
import edu.ucdavis.mw.myinfovault.domain.biosketch.Criterion;
import edu.ucdavis.mw.myinfovault.domain.biosketch.Ruleset;

/**
 * Manual unit test for Criterion and Ruleset in the edu.ucdavis.myinfovault.biosketch package.
 * Each command line argument may be a literal '%' (percent), a <em>value</em>, or <em>field=value</em>.
 * The '%' placeholder 
 * @author Stephen Paulsen
 */
public class TestCriterion
{
    public static void main(String[] args)
    {
        System.out.println("Start");

        Ruleset r = new Ruleset(0, 0);
        r.setOperator(Ruleset.Operator.AND);

        for (String arg : args)
        {
            Criterion c = new Criterion(0);
            if (!arg.equals("%"))       // allow default 'value' if we see the '%' placeholder
            {
                if (arg.contains("="))
                {
                    String[] assign = arg.split("=");
                    c.setField(assign[0]);
                    c.setValue(assign[1]);
                }
                else
                {
                    c.setValue(arg);
                }
            }
            r.addCriterion(c);
        }

        System.out.println(r.toString());

        // Change the operator from 'AND' to 'OR' and produce the output again.
        r.setOperator(Ruleset.Operator.OR);
        System.out.println(r.toString());

        System.out.println("End");
    }
}
