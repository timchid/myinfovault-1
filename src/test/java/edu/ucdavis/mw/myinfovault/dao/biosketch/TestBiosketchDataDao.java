package edu.ucdavis.mw.myinfovault.dao.biosketch;

import java.sql.SQLException;
import java.util.List;

import junit.framework.TestCase;

import org.junit.Ignore;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import edu.ucdavis.mw.myinfovault.domain.biosketch.BiosketchExclude;
import edu.ucdavis.mw.myinfovault.domain.biosketch.BiosketchSection;
import edu.ucdavis.mw.myinfovault.domain.biosketch.BiosketchSectionLimits;
import edu.ucdavis.mw.myinfovault.domain.biosketch.Section;
import edu.ucdavis.myinfovault.designer.Availability;

/**
 * @author dreddy
 * @since MIV 2.1
 */
@Ignore
public class TestBiosketchDataDao extends TestCase
{
    private BiosketchDataDao biosketchDataDao;

    @Override
    protected void setUp() throws Exception
    {
        ClassPathXmlApplicationContext util = new ClassPathXmlApplicationContext(
                new String[] { "/edu/ucdavis/mw/myinfovault/dao/biosketch/test-myinfovault-data.xml" });

        biosketchDataDao = (BiosketchDataDao) util.getBean("biosketchDataDao");
    }

    public void testFindBiosketchExcludeListByBiosketchIdAndUserId()
    {
        try
        {
            List<BiosketchExclude> biosketchExcludeList = biosketchDataDao.findBiosketchExcludeListByBiosketchIdAndUserId(1, 18807);

            for (int i = 0; i < biosketchExcludeList.size(); i++)
            {
                BiosketchExclude biosketchExclude = biosketchExcludeList.get(i);
                System.out.println("BIOSKETCHEXCLUDE - ID is " + biosketchExclude.getId());
                System.out.println("BIOSKETCHEXCLUDE - USERID is " + biosketchExclude.getUserID());
                System.out.println("BIOSKETCHEXCLUDE - BIOSKETCHID is " + biosketchExclude.getBiosketchID());
                System.out.println("BIOSKETCHEXCLUDE - RECTYPE is " + biosketchExclude.getRecType());
                System.out.println("BIOSKETCHEXCLUDE - RECORDID is " + biosketchExclude.getRecordID());
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        catch (Throwable t)
        {
            t.printStackTrace();
            fail(t.getMessage());
        }
    }

    public void testFindBiosketchSectionLimitsByBiosketchType()
    {
        try
        {
            List<BiosketchSectionLimits> biosketchSectionLimitsList = biosketchDataDao.findBiosketchSectionLimitsByBiosketchType(3);
            for (int i = 0; i < biosketchSectionLimitsList.size(); i++)
            {
                BiosketchSectionLimits biosketchSectionLimits = biosketchSectionLimitsList.get(i);
                System.out.println("BIOSKETCHSECTIONLIMITS - ID is " + biosketchSectionLimits.getId());
                System.out.println("BIOSKETCHSECTIONLIMITS - COLLECTIONSID is " + biosketchSectionLimits.getBiosketchType());
                System.out.println("BIOSKETCHSECTIONLIMITS - SECTIONID is " + biosketchSectionLimits.getSectionID());
                System.out.println("BIOSKETCHSECTIONLIMITS - AVAILABILITY is " + biosketchSectionLimits.getAvailability());
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        catch (Throwable t)
        {
            t.printStackTrace();
            fail(t.getMessage());
        }
    }

    public void testFindSection()
    {
        try
        {
            List<Section> sectionList = biosketchDataDao.findSection();
            for (int i = 0; i < sectionList.size(); i++)
            {
                Section section = sectionList.get(i);
                System.out.println("SECTION - ID is " + section.getId());
                System.out.println("SECTION - ID is " + section.getName());
                System.out.println("SECTION - ID is " + section.getRecordName());
                System.out.println("SECTION - ID is " + section.getRecordQuery());
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        catch (Throwable t)
        {
            t.printStackTrace();
            fail(t.getMessage());
        }
    }

    public void testFindBiosketchSectionsByBiosketchId()
    {
        try
        {
            List<BiosketchSection> biosketchSectionList = biosketchDataDao.findBiosketchSectionsByBiosketchId(1);
            for (int i = 0; i < biosketchSectionList.size(); i++)
            {
                BiosketchSection biosketchSection = biosketchSectionList.get(i);
                System.out.println("BIOSKETCHSECTIONLIMITS - ID is " + biosketchSection.getId());
                System.out.println("BIOSKETCHSECTIONLIMITS - BIOSKETCHID is " + biosketchSection.getBiosketchID());
                System.out.println("BIOSKETCHSECTIONLIMITS - SECTIONID is " + biosketchSection.getSectionID());
                System.out.println("BIOSKETCHSECTIONLIMITS - AVAILABILITY is " + biosketchSection.getAvailability());
                System.out.println("BIOSKETCHSECTIONLIMITS - DISPLAY is " + biosketchSection.isDisplay());
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        catch (Throwable t)
        {
            t.printStackTrace();
            fail(t.getMessage());
        }

    }

    public void testInsertBiosketchSection()
    {
        try
        {
            BiosketchSection biosketchSection = new BiosketchSection(0);

            biosketchSection.setBiosketchID(1);
            biosketchSection.setSectionID(1);
            biosketchSection.setAvailability(Availability.valueOf("MANDATORY"));
            biosketchSection.setDisplay(true);

            biosketchDataDao.insertBiosketchSection(biosketchSection, 18807);
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        catch (Throwable t)
        {
            t.printStackTrace();
            fail(t.getMessage());
        }

    }

    public void testUpdateBiosketchSection()
    {
        try
        {
            List<BiosketchSection> sectionsList = biosketchDataDao.findBiosketchSectionsByBiosketchId(4);

            for (int i = 0; i < sectionsList.size(); i++)
            {
                BiosketchSection biosketchSection = sectionsList.get(i);
                biosketchDataDao.updateBiosketchSection(biosketchSection, 18807);
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        catch (Throwable t)
        {
            t.printStackTrace();
            fail(t.getMessage());
        }
    }

    public void testDeleteBiosketchSection()
    {
        try
        {
            biosketchDataDao.deleteBiosketchSection(100);
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        catch (Throwable t)
        {
            t.printStackTrace();
            fail(t.getMessage());
        }
    }
}
