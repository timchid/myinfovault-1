/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: BaseMivJunitTest.java
 */

package edu.ucdavis.mw.myinfovault.common.test;

import org.junit.runner.RunWith;
import org.springframework.mock.web.MockServletContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.ContextHierarchy;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import edu.ucdavis.myinfovault.MIVConfig;


/**
 * Base class for MIV JUnit tests.
 *
 * @author Rick Hendricks
 * @since MIV 5.0.0
 */

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextHierarchy({
    @ContextConfiguration(locations = {"file:src/main/resources/myinfovault-config.xml",
            "file:src/main/resources/SpringBeans.xml",
            "classpath*:edu/ucdavis/iet/commons/ldap/config/iet-commons-ldap.xml",     // classpath*: finds xml config file inside jar
            "file:src/main/resources/user-service-new.xml",
            "file:src/main/resources/myinfovault-data.xml",
            "file:src/main/resources/myinfovault-service.xml"})})


public class BaseMivJunit
{

    protected static void setUp()
    {
        System.out.println("Executing setUp method!");
        if (!MIVConfig.isInitialized())
        {
            MIVConfig.getConfig(new MockServletContext());
        }
    }

}
