/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: FuzzyMatchFilterTest.java
 */

package edu.ucdavis.mw.myinfovault.service;


import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.List;

import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import edu.ucdavis.mw.myinfovault.common.test.BaseMivJunit;
import edu.ucdavis.mw.myinfovault.domain.packet.Packet;
import edu.ucdavis.mw.myinfovault.domain.packet.PacketContent;
import edu.ucdavis.mw.myinfovault.domain.packet.PacketRequest;
import edu.ucdavis.mw.myinfovault.service.packet.PacketService;
import edu.ucdavis.myinfovault.document.Document;

/**
 * JUnit tests for {@link PacketService}.
 *
 * @author Rick Hendricks
 * @since MIV 5.0.0
 */


public class PacketServiceTest extends BaseMivJunit
{
    @BeforeClass
    public static void setUp()
    {
        BaseMivJunit.setUp();
    }

    /**
     * Various packet update tests
     */
    @Ignore
    @Test
    public void updatePacket()
    {
        // Get the packet service
        PacketService packetService = MivServiceLocator.getPacketService();
        assertTrue("Retrieve PacketService: Expected packetService != null.",packetService != null);

        // create a new packet
        Packet packet = new Packet(18635, "New Packet");
        PacketContent item = new PacketContent(packet.getPacketId(), 18635, 1, 1, "display-1");
        packet.addContentItem(item);
        assertTrue("Create Packet "+packet.getPacketId()+": Expected packet != null and item count of 1.", packet != null && packet.getPacketItems().size() == 1);
        packet = packetService.updatePacket(packet, 18635);
        long packetId = packet.getPacketId();

        // update an existing packet
        packet = packetService.getPacket(packetId);
        packet.addContentItem(new PacketContent(packetId, 18635, 1, 2, "display-2"));
        packet.addContentItem(new PacketContent(packetId, 18635, 1, 3, "display-3"));
        packet.removeContentItem(new PacketContent(packetId, 18635, 1, 1, "display-1"));

        packet = packetService.updatePacket(packet, 18635);
        assertTrue("Update A Packet "+packet.getPacketId()+": Expected packet != null and item count of 2.", packet != null && packet.getPacketItems().size() == 2);

        // copy an existing packet
        Packet packetCopy = packetService.copyPacket(packet, 18635);

        // Update an existing packet...no changes
        packetCopy = packetService.updatePacket(packetCopy, 18635);
        assertTrue("Packet" +packetCopy.getPacketId()+" Update...no changes: Expected packet != null and UpdateTime != null.", packetCopy != null && packetService.getPacketUpdateTime(packetCopy.getPacketId()) != null);


        // delete the packets created
   //     assertTrue("Packet "+packet.getPacketId()+" Delete: Expected true.", packetService.deletePacket(packet.getPacketId()));
   //     assertTrue("Packet "+packetCopy.getPacketId()+" Delete: Expected true.", packetService.deletePacket(packetCopy.getPacketId()));
    }

    @Ignore
    @Test
    public void createAndSubmitPacket()
    {
        long dossierId = 30656411;
        int userId = 18635;


        // Get the packet service
        PacketService packetService = MivServiceLocator.getPacketService();
        if (packetService == null) fail("Unable to retrieve the PacketService");

        Packet packet = packetService.getPacket(172);

//        Packet packet = new Packet(userId, "New Packet");
//        packet.addContentItem(new PacketContent(packet.getPacketId(), userId, 6, 287865));
//        packet.addContentItem(new PacketContent(packet.getPacketId(), userId, 6, 287863));

        packet = packetService.updatePacket(packet, userId);

        // Get the packet request
        PacketRequest packetRequest = packetService.getPacketRequest(dossierId);

        List<Document>documents =  packetService.createPacket(packet);
        // Expect 4 documents: Candidate's Statement, Publications, Contributions to JA Works, Combined pdf
        assertTrue("Create Packet "+packet.getPacketId()+": Expected 2 documents", documents.size() == 4);

        packetRequest.setPacketId(packet.getPacketId());
        packetService.updatePacketRequest(packetRequest, userId);

        assertTrue("Submit Packet "+packet.getPacketId()+": Expected 'true'", packetService.submitPacket(packet, packetRequest, userId));

        System.out.println("Packet Submit complete.");
    }

}


