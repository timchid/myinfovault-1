/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: FuzzyMatchFilterTest.java
 */

package edu.ucdavis.mw.myinfovault.service;


import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

/**
 * JUnit tests for {@link FuzzyMatchFilter}.
 *
 * Note that the matching tests are dependent on the settings of the distance and percentageDifference thresholds.
 * The search filter will retrieve the distance and percentage difference thresholds from the mivconfig.properties
 * file if available. Otherwise the values are defaulted to 10 and 50.00 for the distanceThreshold and
 * percentDifferenceThreshold, respectively.
 *
 * The distanceThreshold and percentDifferenceThreshold values may also be overridden in the FuzzyFilterMatch constructor.
 *
 *
 * @author Rick Hendricks
 * @since MIV 4.8.5
 */
public class FuzzyMatchFilterTest
{

    /**
     * Strings to do the fuzzy match against.
     */
    private static final List <String> stringsToMatch = new ArrayList<String>();
    static
    {
        stringsToMatch.add("John Doe");
        stringsToMatch.add("Jonathan Doe");
        stringsToMatch.add("Jonny Doe");
        stringsToMatch.add("Johnny Q Doe");
        stringsToMatch.add("Joey B Donuts");
        stringsToMatch.add("Joe B Donuts");
        stringsToMatch.add("Joseph B Donuts");
        stringsToMatch.add("Joseph B Doughnuts");
        stringsToMatch.add("Joe Doughnuts");
        stringsToMatch.add("Richard M Hendricks");
        stringsToMatch.add("hjkl");
    }


    /**
     * Should be no matches found
     */
    @Test
    public void testNoResultsReturned()
    {
        List<String> matches = new FuzzyMatchFilter<String>("asdf").apply(stringsToMatch);
        assertTrue("No matches found.",matches.isEmpty());
    }

    /**
     * Should only be an exact match returned when thresholds are set to 0 distance and 0 percent difference
     */
    @Test
    public void testExactMatchReturned()
    {
        List<String> matches = new FuzzyMatchFilter<String>("Joe Doughnuts", 0, 0.00).apply(stringsToMatch);
        assertTrue("Exact match found.",matches.size() == 1 && matches.get(0).equals("Joe Doughnuts"));
    }

    /**
     * Should be more than one fuzzy match returned with default 10 and 50 thresholds:
     * Should Match:
     * John Doe
     * Jonathan Doe
     * Jonny Doe
     * Johnny Q Doe
     */
    @Test
    public void testMatchReturned()
    {
        List<String> matches = new FuzzyMatchFilter<String>("Johnny Q Doe").apply(stringsToMatch);
        assertTrue("Matches found.",matches.size() == 4);
    }

    /**
     * Should get a match when thresholds are set at 10 and 50 for the distance and percent difference respectively
     */
    @Test
    public void testMatchReturned1()
    {
        List<String> matches = new FuzzyMatchFilter<String>("Rick Hendricks", 10, 50.00).apply(stringsToMatch);
        assertTrue("Matches found.",matches.size() == 1  && matches.get(0).equals("Richard M Hendricks"));
    }

    /**
     * Should get case insensitive match when thresholds are set at 10 and 50 for the distance and percent difference respectively
     */
    @Test
    public void testMatchReturned2()
    {
        List<String> matches = new FuzzyMatchFilter<String>("rick hendricks", 10, 50.00).apply(stringsToMatch);
        assertTrue("Matches found.",matches.size() == 1  && matches.get(0).equals("Richard M Hendricks"));
    }

}


