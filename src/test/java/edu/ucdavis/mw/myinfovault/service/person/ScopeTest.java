/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: ScopeTest.java
 */

package edu.ucdavis.mw.myinfovault.service.person;

import static org.hamcrest.core.IsNot.not;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import com.google.common.collect.ImmutableMap;

import edu.ucdavis.mw.myinfovault.service.search.MivSearchKeys;

/**
 * JUnit tests for {@link Scope}.
 *
 * @author Craig Gilmore
 * @since MIV 4.8.1
 */
public class ScopeTest
{
    /**
     * Engineering.
     */
    private Scope eng = null;

    /**
     * Engineering - Mechanical and Aerospace
     */
    private Scope eng_aero = null;

    /**
     * Engineering - Computer Science created via {@link Scope#Scope(int, int)}.
     */
    private Scope eng_cs_a = null;

    /**
     * Engineering - Computer Science created via {@link Scope#Scope(String)}.
     */
    private Scope eng_cs_b = null;

    /**
     * Engineering - Computer Science created via {@link Scope#Scope(java.util.Map)}.
     */
    private Scope eng_cs_c = null;

    /**
     * School of Medicine.
     */
    private Scope som_med = null;

    // school and department IDs
    private final static int eng_id = 34;
    private final static int eng_aero_id = 277;
    private final static int eng_cs_id = 275;
    private final static int som_id = 1;
    private final static int som_med_id = 1;

    // scope strings
    private final static String eng_string = "34:ANY";
    private final static String eng_cs_string = "34:275";
    private final static String any_string = "ANY:ANY";
    private final static String no_string = "0:0";

    // scope attribute strings
    private final static String eng_attribute = "SchoolID=34, DepartmentID=-1";
    private final static String eng_cs_attribute = "SchoolID=34, DepartmentID=275";
    private final static String any_attribute = "SchoolID=-1, DepartmentID=-1";
    private final static String no_attribute = "SchoolID=0, DepartmentID=0";

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception
    {
        eng = new Scope(eng_id);
        eng_aero = new Scope(eng_id, eng_aero_id);
        eng_cs_a = new Scope(eng_id, eng_cs_id);
        eng_cs_b = new Scope(eng_cs_string);
        eng_cs_c = new Scope(ImmutableMap.<String, String>builder()
                                         .put(MivSearchKeys.Person.MIV_SCHOOL_CODE, Integer.toString(eng_id))
                                         .put(MivSearchKeys.Person.MIV_DEPARTMENT_CODE, Integer.toString(eng_cs_id))
                                         .build());

        som_med = new Scope(som_id, som_med_id);
    }

    /**
     * Test method for {@link edu.ucdavis.mw.myinfovault.service.person.Scope#clear()}.
     */
    @Test(expected = UnsupportedOperationException.class)
    public void testClear()
    {
        eng_cs_a.clear();
    }

    /**
     * Test method for {@link edu.ucdavis.mw.myinfovault.service.person.Scope#getSchool()}.
     */
    @Test
    public void testGetSchool()
    {
        assertEquals("School ID", eng_id, eng.getSchool());
        assertEquals("School ID", eng_id, eng_cs_a.getSchool());
        assertEquals("School ID", eng_id, eng_cs_b.getSchool());
        assertEquals("School ID", eng_id, eng_cs_c.getSchool());
        assertEquals("School ID", Scope.ANY, Scope.AnyScope.getSchool());
        assertEquals("School ID", Scope.NONE, Scope.NoScope.getSchool());
    }

    /**
     * Test method for {@link edu.ucdavis.mw.myinfovault.service.person.Scope#getDepartment()}.
     */
    @Test
    public void testGetDepartment()
    {
        assertEquals("Department ID", Scope.ANY, eng.getDepartment());
        assertEquals("Department ID", eng_cs_id, eng_cs_a.getDepartment());
        assertEquals("Department ID", eng_cs_id, eng_cs_b.getDepartment());
        assertEquals("Department ID", eng_cs_id, eng_cs_c.getDepartment());
        assertEquals("Department ID", Scope.ANY, Scope.AnyScope.getDepartment());
        assertEquals("Department ID", Scope.NONE, Scope.NoScope.getDepartment());
    }

    /**
     * Test method for {@link edu.ucdavis.mw.myinfovault.service.person.Scope#matches(edu.ucdavis.mw.myinfovault.service.person.Scope)}.
     */
    @Test
    public void testMatchesScope()
    {
        assertTrue("Transitive property must apply for departments of the same scope",
                eng_cs_a.matches(eng_cs_b)
             && eng_cs_b.matches(eng_cs_c)
             && eng_cs_a.matches(eng_cs_c));

        assertTrue("Reflexive property must apply for departments of the same scope",
                   eng_cs_a.matches(eng_cs_b)
                && eng_cs_b.matches(eng_cs_a));

        assertTrue("Reflexive property must apply for schools and their departments",
                   eng.matches(eng_cs_a)
                && eng_cs_a.matches(eng));

        assertTrue("School scope should match itself", eng.matches(eng));
        assertTrue("School scope should match its departments", eng.matches(eng_aero) && eng.matches(eng_cs_a));
        assertTrue("School scope should match 'any scope'", eng.matches(Scope.AnyScope));
        assertFalse("School scope should not match outside department", eng.matches(som_med));
        assertFalse("School scope should not match 'no scope'", eng.matches(Scope.NoScope));
        assertTrue("Department scope should match itself", eng_aero.matches(eng_aero));
        assertTrue("Department scope should match 'any scope'", eng_aero.matches(Scope.AnyScope));
        assertFalse("Department scope should not match another department", eng_aero.matches(eng_cs_a));
        assertFalse("Department scope should not match 'no scope'", eng_aero.matches(Scope.NoScope));
        assertFalse("'No scope' should match no scope", Scope.NoScope.matches(eng));
    }

    /**
     * Test method for {@link edu.ucdavis.mw.myinfovault.service.person.Scope#contains(edu.ucdavis.mw.myinfovault.service.person.Scope)}.
     */
    @Test(expected = UnsupportedOperationException.class)
    public void testContains()
    {
        eng.contains(eng_cs_a);
    }

    /**
     * Test method for {@link edu.ucdavis.mw.myinfovault.service.person.Scope#put(java.lang.String, java.lang.String)}.
     */
    @Test(expected = UnsupportedOperationException.class)
    public void testPutStringString()
    {
        eng_cs_a.put("unimportant", "unimportant");
    }

    /**
     * Test method for {@link edu.ucdavis.mw.myinfovault.service.person.Scope#putAll(java.util.Map)}.
     */
    @Test(expected = UnsupportedOperationException.class)
    public void testPutAllMapOfQextendsStringQextendsString()
    {
        eng_cs_a.putAll(eng_cs_a);
    }

    /**
     * Test method for {@link edu.ucdavis.mw.myinfovault.service.person.Scope#remove(java.lang.Object)}.
     */
    @Test(expected = UnsupportedOperationException.class)
    public void testRemoveObject()
    {
        eng_cs_a.remove("unimportant");
    }

    /**
     * Test method for {@link edu.ucdavis.mw.myinfovault.service.person.Scope#equals(java.lang.Object)}.
     */
    @Test
    public void testEqualsObject()
    {
        assertEquals("Department scope", eng_cs_a, eng_cs_a);
        assertEquals("Department scope", eng_cs_a, eng_cs_b);
        assertEquals("Department scope", eng_cs_a, eng_cs_c);
        assertEquals("Department scope", eng_cs_b, eng_cs_c);

        assertThat("Department scope", eng_aero, not(Scope.AnyScope));
        assertThat("Department scope", eng_aero, not(Scope.NoScope));
        assertThat("Department scope", eng_aero, not(eng));
        assertThat("Department scope", eng_aero, not(som_med));
    }

    /**
     * Test method for {@link edu.ucdavis.mw.myinfovault.service.person.Scope#getAttributes()}.
     */
    @Test
    public void testGetAttributes()
    {
        assertEquals("Department scope attributes", eng_cs_attribute, eng_cs_a.getAttributes());
        assertEquals("Department scope attributes", eng_cs_attribute, eng_cs_b.getAttributes());
        assertEquals("Department scope attributes", eng_cs_attribute, eng_cs_c.getAttributes());
        assertEquals("School scope attributes", eng_attribute, eng.getAttributes());
        assertEquals("Any scope attributes", any_attribute, Scope.AnyScope.getAttributes());
        assertEquals("No scope attributes", no_attribute, Scope.NoScope.getAttributes());
    }

    /**
     * Test method for {@link edu.ucdavis.mw.myinfovault.service.person.Scope#toString()}.
     */
    @Test
    public void testToString()
    {
        assertEquals("Department scope string", eng_cs_string, eng_cs_a.toString());
        assertEquals("Department scope string", eng_cs_string, eng_cs_b.toString());
        assertEquals("Department scope string", eng_cs_string, eng_cs_c.toString());
        assertEquals("School scope string", eng_string, eng.toString());
        assertEquals("Any scope string", any_string, Scope.AnyScope.toString());
        assertEquals("No scope string", no_string, Scope.NoScope.toString());
    }
}
