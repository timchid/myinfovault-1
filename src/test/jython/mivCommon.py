'''
This script contains common functions used by all scripts


Created on Mar 18, 2010
@author: rhendric
'''

from net.grinder.script.Grinder import grinder
from net.grinder.plugin.http import HTTPPluginControl
from HTTPClient import CookieModule
import os.path
import datetime
import time

from java.util import Random
from java.util.regex import Matcher
from java.util.regex import Pattern
from threading import Lock


random = Random()
lock = Lock()       

# Default Max sleep time in ms ?
MAX_SLEEP_TIME = 1000

# Default server URL
SERVER_URL = 'localhost'

# Last Role Change Seconds
LAST_ROLE_CHANGE_SECONDS = 0

# Min Role Change Interval Seconds
MIN_ROLE_CHANGE_INTERVAL = 0

# Last Route Seconds
LAST_ROUTE_SECONDS = 0 

# Min Routing Interval Seconds
MIN_ROUTE_INTERVAL = 0  

# get the logger
log = grinder.getLogger()


 # Common methods for all scripts  
def setTestIncrement(self, increment):
    self.testIncrement = increment
  
def getTestIncrement(self):
    return self.testIncrement
  
def setNextTestNumber(self, nextNumber):
    self.nextTestNumber = nextNumber
  
def getNextTestNumber(self):
    return self.nextTestNumber
  
def setTestSuccess(self, success):
    grinder.getStatistics().getForLastTest().setSuccess(success)
    self.testSuccess = success
  
def getTestSuccess(self):
    return self.testSuccess


def writeToFile(fileHandle,text):
    fileHandle.write(text)

def openTestFile(scriptName):
    filename = grinder.getFilenameFactory().createFilename(
        scriptName, ".html")

    return open(filename, "a")

def closeTestFile(fileHandle):
    fileHandle.close()

def getRedirectURL(resultObject):
    redirectURL = ''
    for h in resultObject.listHeaders():
        if h == 'Location':
            redirectURL = resultObject.getHeader(h)
            break
    return redirectURL             
  
def getFlowExecutionKey(resultObject):
    flowExecutionKey = ''
    effectiveURI = resultObject.getEffectiveURI()  
    queryString = effectiveURI.getQueryString()
    
    params = queryString.split('=')
    count = 0
    for param in params:
        count += 1
        if param == '_flowExecutionKey':
            flowExecutionKey = params[count]
            break
    
    return flowExecutionKey             

def getSessionId():
    return getCookie('JSESSIONID').getValue()

def getCookie(cookieName):
    cookies = CookieModule.listAllCookies(HTTPPluginControl.getThreadHTTPClientContext())
    for cookie in cookies: 
        if cookie.getName() == cookieName:
            break
    return cookie   

def setCookie(cookie):
    CookieModule.addCookie(cookie,HTTPPluginControl.getThreadHTTPClientContext())

def setSearchCriteria(self, text):

    text = ' '.join(text.split());          # remove all white space

    # find the department options
    pattern = Pattern.compile("<select id=\"department\" name=\"department\">.*?</select>");
    options = pattern.matcher(text);
    departments = []
    schools = []
    searchByValues = ["Name","LastName"]
    
    # build array of departments this user is allowed to select from
    if options.find():
        pattern = Pattern.compile("<option value=\"([0-9]+:[0-9]+)\">(.*?)</option>")
        option = pattern.matcher(options.group())
        while option.find():
            departments.append(option.group(1))

    # build array of schools this user is allowed to select from
    pattern = Pattern.compile("<select id=\"school\" name=\"school\">.*?</select>");
    options = pattern.matcher(text);
    if options.find():
        pattern = Pattern.compile("<option value=\"([0-9]+)\">(.*?)</option>")
        option = pattern.matcher(options.group())
        while option.find():
            schools.append(option.group(1))

    # Last name selection options
    lastName = ["All","A","B","C","D","E","F","G","H","I","J","K","L","M",
                "N","O","P","Q","R","S","T","U","V","W","X","Y","Z"]


    self.token_inputName =  ''
    self.token_lname = ''
    self.token_department =  ''
    self.token_school =   ''
    
    # search by options
    if len(departments) > 0:
        searchByValues.append("Department")
        self.token_department = departments[0]  # default
    if len(schools) > 0:
        searchByValues.append("School")
        self.token_schools = schools[0]  #default
    
    self.searchBy = searchByValues[random.nextInt(len(searchByValues))]

    # Get the search by value
    if self.searchBy == 'Name':
        self.token_inputName =  lastName[random.nextInt(len(lastName))][0:1]   # just get single character, avoid "All"
        self.searchValue = self.token_inputName
        self.token_eventId = '_eventId_searchname'
    elif self.searchBy == 'LastName':
        self.token_lname =  lastName[random.nextInt(len(lastName))]
        self.searchValue = self.token_lname
        self.token_eventId = '_eventId_searchlname'
    elif self.searchBy == 'Department':
       self.token_department =  departments[random.nextInt(len(departments))]
       self.searchValue = self.token_department
       self.token_eventId = '_eventId_searchdept'
    elif self.searchBy == 'School':
       self.token_school =  schools[random.nextInt(len(schools))]
       self.searchValue = self.token_school
       self.token_eventId = '_eventId_searchschool'
      
def getRandomLoginUserAndPassword():
    
    # Note that the CAS impersonation server must be used to allow these logins to work

    # The below select may be used to build the login string for the cas impersonation service 
    #select distinct concat('"',login,':',givenname,' ',middlename,' ',surname,':',principalid,':ac297bdc3729064385293750f700cc3e",') as loginString from UserAccount ua, RoleAssignment ra where ua.active=true and ua.userId = ra.userId and ra.roleId in (1,2,4,11)

    mivUsers = [
    "mcbuell:Marci  Buell:55087:ac297bdc3729064385293750f700cc3e",
    "cmblount:Cameron M Blount:194173:ac297bdc3729064385293750f700cc3e",
    "dbunn:Dean  Bunn:12803:ac297bdc3729064385293750f700cc3e",
    "kdchilds:Kobe  Childs-Floyd:52543:ac297bdc3729064385293750f700cc3e",
    "btchriss:Brenda  Chriss:2711:ac297bdc3729064385293750f700cc3e",
    "sally.divecchia:Sally  DiVecchia:1054788:ac297bdc3729064385293750f700cc3e",
    "jdvorak:Jan  Dvorak:1640878:ac297bdc3729064385293750f700cc3e",
    "cdimuro:Chris  diMuro:1701003:ac297bdc3729064385293750f700cc3e",
    "cdoherty:Catherine  Doherty:7387:ac297bdc3729064385293750f700cc3e",
    "etdahl:Erik  Dahl:50051:ac297bdc3729064385293750f700cc3e",
    "julie.beal:Julie Ann Fritz:2122360:ac297bdc3729064385293750f700cc3e",
    "jlgastin:Jennifer  Gastineau:44222:ac297bdc3729064385293750f700cc3e",
    "marlene.goetz:Marlene  Goetz:2106058:ac297bdc3729064385293750f700cc3e",
    "aakader:Adel A Kader:1643453:ac297bdc3729064385293750f700cc3e",
    "volks11:Jennifer  Kellogg:31032:ac297bdc3729064385293750f700cc3e",
    "mmmcnall:Mary  McNally:7404:ac297bdc3729064385293750f700cc3e",
    "adobe:Kathleen  MacColl:33834:ac297bdc3729064385293750f700cc3e",
    "donna.olsson:Donna  Olsson (Watkins):1760205:ac297bdc3729064385293750f700cc3e",
    "deboj:Debbie  Ojakangas:24951:ac297bdc3729064385293750f700cc3e",
    "eprosser:Eric J Prosser:214303:ac297bdc3729064385293750f700cc3e",
    "crichardson:Cathe  Richardson:1649459:ac297bdc3729064385293750f700cc3e",
    "mtilghman:Megan  Rott:2261741:ac297bdc3729064385293750f700cc3e",
    "ajreyes:Abigail J Reyes:149564:ac297bdc3729064385293750f700cc3e",
    "peabud:Stephanie J Smith:33768:ac297bdc3729064385293750f700cc3e",
    "gsanchez:Lupe  Sanchez:31998:ac297bdc3729064385293750f700cc3e",
    "raymond.tai:Raymond  Tai:1512102:ac297bdc3729064385293750f700cc3e",
    "emtani:Ellen  Tani:9090:ac297bdc3729064385293750f700cc3e",
    "jtenma:Joan  Tenma:283774:ac297bdc3729064385293750f700cc3e",
    "jbvanauk:John  Van Auker:139190:ac297bdc3729064385293750f700cc3e",
    "annwild:Anne  Wildermuth:7701:ac297bdc3729064385293750f700cc3e",
    "jan.ilkiw:Jan E Ilkiw:1192068:ac297bdc3729064385293750f700cc3e",
    "carol.erickson:Carol  Erickson:1072806:ac297bdc3729064385293750f700cc3e",
    "james.macdonald:James  MacDonald:1294170:ac297bdc3729064385293750f700cc3e",
    "brwhite:Bruce R. White:34652:ac297bdc3729064385293750f700cc3e",
    "scurrall:Steven C. Currall:280891:ac297bdc3729064385293750f700cc3e",
    "ncrunner:Nichol  Runner:148901:ac297bdc3729064385293750f700cc3e",
    "wtko:Winston  Ko:22346:ac297bdc3729064385293750f700cc3e",
    "hopmans:Jan  Hopmans:15594:ac297bdc3729064385293750f700cc3e",
    "bwinterh:Bruce P Winterhalder:38408:ac297bdc3729064385293750f700cc3e",
    "latorfi:Lissa  Torfi:50566:ac297bdc3729064385293750f700cc3e",
    "mattheis:Jennifer  Mattheis:10058:ac297bdc3729064385293750f700cc3e",
    "biosburn:Bennie  Osburn:12147:ac297bdc3729064385293750f700cc3e",
    "brianmcc:Brian  Mccarthy:296183:ac297bdc3729064385293750f700cc3e",
    "clbray:Curtis J Bray:161264:ac297bdc3729064385293750f700cc3e",
    "rrocchio:Rosemary  Rocchio:290138:ac297bdc3729064385293750f700cc3e",
    "john.pascoe:John  Pascoe:1389408:ac297bdc3729064385293750f700cc3e",
    "mary.delany:Mary E. Delany:2136946:ac297bdc3729064385293750f700cc3e",
    "elise.valenton:Elisa  Valenton:1545564:ac297bdc3729064385293750f700cc3e",
    "timmyvo:Timmy  Vo:175769:ac297bdc3729064385293750f700cc3e",
    "edward.callahan:Edward J. Callahan:973278:ac297bdc3729064385293750f700cc3e",
    "cherie:Cherie  Felsch:50685:ac297bdc3729064385293750f700cc3e",
    "eldavid:Emerson  David:33951:ac297bdc3729064385293750f700cc3e",
    "bewolfe:Beth M. Wolfe:143966:ac297bdc3729064385293750f700cc3e",
    "rieko.ringo:Rieko  Ringo:1439172:ac297bdc3729064385293750f700cc3e",
    "monique.vasquez:Monique M Vasquez:1548138:ac297bdc3729064385293750f700cc3e",
    "camunoz:Carla  Munoz:22505:ac297bdc3729064385293750f700cc3e",
    "julsq:Julie A Quigley:96154:ac297bdc3729064385293750f700cc3e",
    "lyon:Debra  Lyon:25615:ac297bdc3729064385293750f700cc3e",
    "royrai:Gurmeet Roy Rai:6392:ac297bdc3729064385293750f700cc3e",
    "skmuhamm:Saleema  Muhammed:148179:ac297bdc3729064385293750f700cc3e",
    "carkat:Rose Mary Miller:32966:ac297bdc3729064385293750f700cc3e",
    "cchandlr:Carol  Chandler:122732:ac297bdc3729064385293750f700cc3e",
    "kjzbylut:Kristina  Zbylut:7342:ac297bdc3729064385293750f700cc3e",
    "rtporter:Robert T Porter:216585:ac297bdc3729064385293750f700cc3e",
    "mhjose:Mei  Jose:148558:ac297bdc3729064385293750f700cc3e",
    "tina.meagher:Teresa  Meagher:2130082:ac297bdc3729064385293750f700cc3e",
    "szlsawye:Laura E. Sawyer:12628:ac297bdc3729064385293750f700cc3e",
    "cvxiong:Chue  Xiong:196734:ac297bdc3729064385293750f700cc3e",
    "tythibod:Tracey  Thibodeaux:196282:ac297bdc3729064385293750f700cc3e",
    "bobbie.dabis:Bobbie  Dabis:1793667:ac297bdc3729064385293750f700cc3e",
    "swarrena:Susan  Warren-Alef:5340:ac297bdc3729064385293750f700cc3e",
    "creid:Chandra  Reid:1653:ac297bdc3729064385293750f700cc3e",
    "angelor:Olga  Rusev:45797:ac297bdc3729064385293750f700cc3e",
    "teressa.baldwin:Teressa  Baldwin:914076:ac297bdc3729064385293750f700cc3e",
    "priggs:Penny  Riggs:149736:ac297bdc3729064385293750f700cc3e",
    "patti:Patricia  Robinson:21422:ac297bdc3729064385293750f700cc3e",
    "michael.dahmus:Michael  Dahmus:1031622:ac297bdc3729064385293750f700cc3e",
    "kathryn.blaisdell:Kathryn  Blaisdell:2095762:ac297bdc3729064385293750f700cc3e",
    "kemauer:Keith  Mauer:49072:ac297bdc3729064385293750f700cc3e",
    "mary.mccarthy:Mary  McCarthy:2090614:ac297bdc3729064385293750f700cc3e",
    "carrja:Carrie-Jean  Jackson:89592:ac297bdc3729064385293750f700cc3e",
    "radiobox:Lam  Nguyen:96997:ac297bdc3729064385293750f700cc3e",
    "kally.turner:E Kally Turner:1536984:ac297bdc3729064385293750f700cc3e",
    "jeff.austin:Jeff  Austin:1755057:ac297bdc3729064385293750f700cc3e",
    "shnichol:Sheryl  Nichols:16855:ac297bdc3729064385293750f700cc3e",
    "mjwhite:Mary  White:52491:ac297bdc3729064385293750f700cc3e",
    "sallie.green:Sallie  Green:2285811:ac297bdc3729064385293750f700cc3e",
    "hsublett:Hampton  Sublett:132157:ac297bdc3729064385293750f700cc3e",
    "lyeeisbe:Lisa  Yee-Isbell:55294:ac297bdc3729064385293750f700cc3e",
    "kmjjones:Kelly M Jones:239449:ac297bdc3729064385293750f700cc3e",
    "totlkaos:Mandy  Jarvis:33971:ac297bdc3729064385293750f700cc3e",
    "kgerz:Kelly  Wallace:1832277:ac297bdc3729064385293750f700cc3e",
    "aburnham:Amy  Burnham:95935:ac297bdc3729064385293750f700cc3e",
    "kdpearso:Kimberly D Pearson:50080:ac297bdc3729064385293750f700cc3e",
    "valjohn:Valerie L Johnson:130328:ac297bdc3729064385293750f700cc3e",
    "arterry:Alison R Terry:235379:ac297bdc3729064385293750f700cc3e",
    "clcraig:Cindy  Craig:55770:ac297bdc3729064385293750f700cc3e",
    "judy.krueger:Judy  Krueger:1876035:ac297bdc3729064385293750f700cc3e",
    "ez019247:Matt  Jurach:32486:ac297bdc3729064385293750f700cc3e",
    "szsingha:Anita  Singh:9820:ac297bdc3729064385293750f700cc3e",
    "laklkegr:Lesley  Byrns:5160:ac297bdc3729064385293750f700cc3e",
    "sgyonts:Susan  Yonts:50935:ac297bdc3729064385293750f700cc3e",
    "julia.hamilton:Julia  Hamilton:2117212:ac297bdc3729064385293750f700cc3e",
    "retbrown:Tracey  Brown:162830:ac297bdc3729064385293750f700cc3e",
    "jddimitr:Jonathon  Dimitriadis:237311:ac297bdc3729064385293750f700cc3e",
    "mchriste:Melanie  Christensen:41670:ac297bdc3729064385293750f700cc3e",
    "hes:Sandra  Higby:6980:ac297bdc3729064385293750f700cc3e",
    "katiford:Kathleen  Peterson:150419:ac297bdc3729064385293750f700cc3e",
    "kacurley:Karen  Curley:7856:ac297bdc3729064385293750f700cc3e",
    "andrew.jones:Andrew  Jones:1954113:ac297bdc3729064385293750f700cc3e",
    "szarmstr:Debbie  Armstrong:33633:ac297bdc3729064385293750f700cc3e",
    "sarussel:Sue A Russell:17769:ac297bdc3729064385293750f700cc3e",
    "nancy.ignacio:Nancy J Ignacio:1191210:ac297bdc3729064385293750f700cc3e",
    "monica.steinhart:Monica  Steinhart:1499232:ac297bdc3729064385293750f700cc3e",
    "vedye:Victoria  Dye:11883:ac297bdc3729064385293750f700cc3e",
    "rosanna.ullrich:Rosanna  Ullrich:1752483:ac297bdc3729064385293750f700cc3e",
    "mcastle:Michele  Castillo:58176:ac297bdc3729064385293750f700cc3e",
    "gmdayton:Gina  Dayton:150972:ac297bdc3729064385293750f700cc3e",
    "lapetty:Linda A Petty:200439:ac297bdc3729064385293750f700cc3e",
    "cjturk:Christopher Joseph Turk:163994:ac297bdc3729064385293750f700cc3e",
    "stacie.percey:Stacie  Percey:1395414:ac297bdc3729064385293750f700cc3e",
    "mbelyaye:Marina  Belyayeva:188492:ac297bdc3729064385293750f700cc3e",
    "lefta:Suzanne  Papamichail:21225:ac297bdc3729064385293750f700cc3e",
    "reineta:Nita  Puig-Albert:69806:ac297bdc3729064385293750f700cc3e",
    "sjkanoho:Sue  Kanoho:58295:ac297bdc3729064385293750f700cc3e",
    "kim.delaughder:Kimberly  DeLaughder:2081176:ac297bdc3729064385293750f700cc3e",
    "chi.chiang:Chi  Chiang:999018:ac297bdc3729064385293750f700cc3e",
    "jnickell:Jennifer H Nickell:50829:ac297bdc3729064385293750f700cc3e",
    "hlevine:Harold  Levine:39561:ac297bdc3729064385293750f700cc3e",
    "mlbilhei:Marian L Bilheimer:7202:ac297bdc3729064385293750f700cc3e",
    "jfujita:Janice  Fujita:50508:ac297bdc3729064385293750f700cc3e",
    "sweets:Kristin L Britton-Isaacson:149840:ac297bdc3729064385293750f700cc3e",
    "dkkronen:Debra  Kronenberg:21067:ac297bdc3729064385293750f700cc3e",
    "jlshorts:Jennifer Louise Shorts:229837:ac297bdc3729064385293750f700cc3e",
    "vrelmer:Va Shon  Elmer:162817:ac297bdc3729064385293750f700cc3e",
    "lbarnes:Laura J Stansbury:287450:ac297bdc3729064385293750f700cc3e",
    "tfreeman:Trudi  Banko:96952:ac297bdc3729064385293750f700cc3e",
    "lehret:Laura  Leming:2482:ac297bdc3729064385293750f700cc3e",
    "tbtansey:Tina  Tansey:10186:ac297bdc3729064385293750f700cc3e",
    "ptdavis:Pamela  Davis:27776:ac297bdc3729064385293750f700cc3e",
    "jaowens:Jessie Ann Owens:146114:ac297bdc3729064385293750f700cc3e",
    "ecunderw:Emma Clare Underwood:5446:ac297bdc3729064385293750f700cc3e",
    "gurley:Karen  Nofziger:22087:ac297bdc3729064385293750f700cc3e",
    "pafandi:Pausia  Afandi:31332:ac297bdc3729064385293750f700cc3e",
    "sfbahati:Shani  Bahati:75005:ac297bdc3729064385293750f700cc3e",
    "jmjackso:Julie M Jackson:50503:ac297bdc3729064385293750f700cc3e",
    "hamiel:Jane  Hamiel:51992:ac297bdc3729064385293750f700cc3e",
    "ljhutch:Linda J. Hutchinson:57667:ac297bdc3729064385293750f700cc3e",
    "clalberi:Cheri  Albericci:21619:ac297bdc3729064385293750f700cc3e",
    "pjaguile:Patricia  Aguilera:54627:ac297bdc3729064385293750f700cc3e",
    "arriaga:Lillian  Arriaga:28732:ac297bdc3729064385293750f700cc3e",
    "amador:Karla  Amador:158055:ac297bdc3729064385293750f700cc3e",
    "jlallen:Jan Lynn Allen:228724:ac297bdc3729064385293750f700cc3e",
    "joyault:Joy R Ault:227149:ac297bdc3729064385293750f700cc3e",
    "angander:Karen  Anderson:1949:ac297bdc3729064385293750f700cc3e",
    "laurie.finch:Laurie  Boling:1085676:ac297bdc3729064385293750f700cc3e",
    "terri.bradley:Terri  Bradley:953544:ac297bdc3729064385293750f700cc3e",
    "pbbell:Patrick B Bell:22978:ac297bdc3729064385293750f700cc3e",
    "mbiegaj:Mark  Biegaj:55325:ac297bdc3729064385293750f700cc3e",
    "nancy.boyd:Nancy  Boyd:2096620:ac297bdc3729064385293750f700cc3e",
    "susan.brugh:Susan  Brugh:2079460:ac297bdc3729064385293750f700cc3e",
    "crespo:Lisa  Blake:50248:ac297bdc3729064385293750f700cc3e",
    "saboylan:Sharon  Boylan:41792:ac297bdc3729064385293750f700cc3e",
    "jmburse:Janis M Burse:173123:ac297bdc3729064385293750f700cc3e",
    "jbechtel:Julie L Bechtel:197281:ac297bdc3729064385293750f700cc3e",
    "kboerner:Karen  Boerner:51989:ac297bdc3729064385293750f700cc3e",
    "jbsimons:Janet  Brown-Simmons:20557:ac297bdc3729064385293750f700cc3e",
    "stacey.cole:Stacey  Cole:2027977:ac297bdc3729064385293750f700cc3e",
    "peter.cala:Peter  Cala:971562:ac297bdc3729064385293750f700cc3e",
    "michael.condrin:Michael  Condrin:1864881:ac297bdc3729064385293750f700cc3e",
    "cynthia.chinn:Cynthia  Chinn:1001592:ac297bdc3729064385293750f700cc3e",
    "jmcano:Jennifer  Cano:148498:ac297bdc3729064385293750f700cc3e",
    "rubyc:Ruby  Castillo:37871:ac297bdc3729064385293750f700cc3e",
    "sdcarras:Susan  Carrasco:15707:ac297bdc3729064385293750f700cc3e",
    "scontrer:Stephanie  Contreras:5467:ac297bdc3729064385293750f700cc3e",
    "ccrabill:Carol  Crabill:37949:ac297bdc3729064385293750f700cc3e",
    "mcosens:Marguerite  Cosens:266334:ac297bdc3729064385293750f700cc3e",
    "nechapma:Norm  Chapman:239099:ac297bdc3729064385293750f700cc3e",
    "lrcervan:Lisa R Cervantes:240528:ac297bdc3729064385293750f700cc3e",
    "tlcatron:Terry  Catron-Dodkins:4379:ac297bdc3729064385293750f700cc3e",
    "marge:Marge  Callahan:24714:ac297bdc3729064385293750f700cc3e",
    "nancy.deherrera:Nancy  DeHerrera:1927515:ac297bdc3729064385293750f700cc3e",
    "cmderr:Christopher  Derr:29154:ac297bdc3729064385293750f700cc3e",
    "sldragon:Sherri  Dragonetti:145200:ac297bdc3729064385293750f700cc3e",
    "nancyd:Nancy  Dullum:14460:ac297bdc3729064385293750f700cc3e",
    "teague:Judy  Elliott:33098:ac297bdc3729064385293750f700cc3e",
    "lynnette.eldridge:Lynnette  Eldridge:1067658:ac297bdc3729064385293750f700cc3e",
    "paul.fitzgerald:Paul  Fitzgerald:1089108:ac297bdc3729064385293750f700cc3e",
    "cflander:Christine  Flanders:148028:ac297bdc3729064385293750f700cc3e",
    "kafeinst:Kori  Feinstein:54186:ac297bdc3729064385293750f700cc3e",
    "maflores:Marisela  Flores:173141:ac297bdc3729064385293750f700cc3e",
    "sfischer:Shirl  Fischer:23492:ac297bdc3729064385293750f700cc3e",
    "kfrye:Kyle  Frye:133443:ac297bdc3729064385293750f700cc3e",
    "becky.glasgow:Rebecca  Glasgow:1116564:ac297bdc3729064385293750f700cc3e",
    "klgeist:Kerry  Geist:33162:ac297bdc3729064385293750f700cc3e",
    "cynthia.gomez:Cynthia  Gomez:1122570:ac297bdc3729064385293750f700cc3e",
    "marianne.goetze:Marianne  Goetze:1118280:ac297bdc3729064385293750f700cc3e",
    "dgreene:Dyana  Greene:142224:ac297bdc3729064385293750f700cc3e",
    "salarcon:Brenda  Gilday:24662:ac297bdc3729064385293750f700cc3e",
    "mcgonzal:Maricela  Gonzalez:40510:ac297bdc3729064385293750f700cc3e",
    "nggalvez:Nicole G Galvez:284556:ac297bdc3729064385293750f700cc3e",
    "amgillis:Aretha  Gillis:47929:ac297bdc3729064385293750f700cc3e",
    "tania.heta:Tania  Heta:2100052:ac297bdc3729064385293750f700cc3e",
    "robert.hansen:Robert J Hansen:1147452:ac297bdc3729064385293750f700cc3e",
    "claddagh:Maria C. Hayes:37535:ac297bdc3729064385293750f700cc3e",
    "sharon.hein:Sharon  Hein:1166328:ac297bdc3729064385293750f700cc3e",
    "deb:Debra  Heras:4213:ac297bdc3729064385293750f700cc3e",
    "patpat:Patricia  Hunter:1693:ac297bdc3729064385293750f700cc3e",
    "caheintz:Cynthia  Heintz:44894:ac297bdc3729064385293750f700cc3e",
    "jherron:Jessie  Herron:235728:ac297bdc3729064385293750f700cc3e",
    "bhobart:Bethanie  Hobart:241193:ac297bdc3729064385293750f700cc3e",
    "chermes:Christine  Hermes:31403:ac297bdc3729064385293750f700cc3e",
    "ldjordan:Linsey Deann Jordan:97657:ac297bdc3729064385293750f700cc3e",
    "skywatch:Skye E Jura:128980:ac297bdc3729064385293750f700cc3e",
    "julik:Juli  Koeberlein:2034842:ac297bdc3729064385293750f700cc3e",
    "lrkemp:Laura  Kemp:51939:ac297bdc3729064385293750f700cc3e",
    "kitchen:Mary  Kitchen:29438:ac297bdc3729064385293750f700cc3e",
    "pamela.lowart:Pamela  Lowart:1286448:ac297bdc3729064385293750f700cc3e",
    "tlow:Teri H. Low:17122:ac297bdc3729064385293750f700cc3e",
    "aklaviol:Anita  La Violette:21206:ac297bdc3729064385293750f700cc3e",
    "mflee:Michelle F Lee:50523:ac297bdc3729064385293750f700cc3e",
    "llamoree:Lynn L Lamoree:83551:ac297bdc3729064385293750f700cc3e",
    "susieleetai:Susie  Lee-Tai:1829703:ac297bdc3729064385293750f700cc3e",
    "jlearned:Joan  Learned:28498:ac297bdc3729064385293750f700cc3e",
    "meliving:Marnie E Livingston:159249:ac297bdc3729064385293750f700cc3e",
    "lade:Tracy  Lade:27461:ac297bdc3729064385293750f700cc3e",
    "nlimpin:Nenita  Limpin:42593:ac297bdc3729064385293750f700cc3e",
    "smlopez:Susan M Lopez:40835:ac297bdc3729064385293750f700cc3e",
    "elincoln:Elizabeth  Lincoln:128621:ac297bdc3729064385293750f700cc3e",
    "martha.morris:Martha  Morris:2169550:ac297bdc3729064385293750f700cc3e",
    "amueller:Adeline  Mueller:19163:ac297bdc3729064385293750f700cc3e",
    "debbie.morris:Debbie  Morris:2232568:ac297bdc3729064385293750f700cc3e",
    "blythe.myers:Blythe  Myers:1361094:ac297bdc3729064385293750f700cc3e",
    "cmurdock:Carolyn  Murdock:93799:ac297bdc3729064385293750f700cc3e",
    "fmohamud:Fatima  Mohamud:2051145:ac297bdc3729064385293750f700cc3e",
    "pbelleau:Pamela  Mazanet Belleau:94838:ac297bdc3729064385293750f700cc3e",
    "semckee:Sharon  McKee:50094:ac297bdc3729064385293750f700cc3e",
    "smmann:Sherri  Mann:3371:ac297bdc3729064385293750f700cc3e",
    "jand:Janet D. Harlan:20478:ac297bdc3729064385293750f700cc3e",
    "cjmora:Colleen J Mora:196291:ac297bdc3729064385293750f700cc3e",
    "maminch:Mary A Minch:214576:ac297bdc3729064385293750f700cc3e",
    "smcinish:Suzanne  McInish:97160:ac297bdc3729064385293750f700cc3e",
    "dmatsumo:Deb  Matsumoto:55365:ac297bdc3729064385293750f700cc3e",
    "kawow:Korie  Martinez:142523:ac297bdc3729064385293750f700cc3e",
    "lynn.nguyen:Lynn T Nguyen:1371390:ac297bdc3729064385293750f700cc3e",
    "klbrin:Karen  Nelson:13627:ac297bdc3729064385293750f700cc3e",
    "nakashi:Lauren T Nakashima:32617:ac297bdc3729064385293750f700cc3e",
    "wnichols:Wendall  Nicholson:43694:ac297bdc3729064385293750f700cc3e",
    "vmozella:Valerie  Ozella:11720:ac297bdc3729064385293750f700cc3e",
    "dorescan:Don  Orescanin:6804:ac297bdc3729064385293750f700cc3e",
    "rortez:Renee J Ortez-Buie:190284:ac297bdc3729064385293750f700cc3e",
    "smolton:Susan  Olton:22651:ac297bdc3729064385293750f700cc3e",
    "fingers:Nikki  Phipps:37732:ac297bdc3729064385293750f700cc3e",
    "maria.pablo:Maria  Pablo:1382544:ac297bdc3729064385293750f700cc3e",
    "hcpatel:Hemang  Patel:2083750:ac297bdc3729064385293750f700cc3e",
    "szprathe:Julia  Prather:43336:ac297bdc3729064385293750f700cc3e",
    "rpattiso:Robert  Pattison:57019:ac297bdc3729064385293750f700cc3e",
    "mcpeters:Marie C. Peters:22454:ac297bdc3729064385293750f700cc3e",
    "lpetrish:Yelena  Petrishin:148615:ac297bdc3729064385293750f700cc3e",
    "jmfpeter:Janna  Peterson:195437:ac297bdc3729064385293750f700cc3e",
    "lpastore:Lisa  Pastore:25370:ac297bdc3729064385293750f700cc3e",
    "jessers:Jessica  Potts:24500:ac297bdc3729064385293750f700cc3e",
    "faith.fitzgerald:Faith  Fitzgerald:1088250:ac297bdc3729064385293750f700cc3e",
    "anna.raffetto:Anna  Raffetto:1427160:ac297bdc3729064385293750f700cc3e",
    "chitra.rao:Chitra  Rao:1430592:ac297bdc3729064385293750f700cc3e",
    "rosina:Rosina  Robinson:33523:ac297bdc3729064385293750f700cc3e",
    "crifkin:Carol R. Rifkin:95699:ac297bdc3729064385293750f700cc3e",
    "preginelli:Phyllis R Reginelli:1648601:ac297bdc3729064385293750f700cc3e",
    "otto.raabe:Otto G Raabe:2253161:ac297bdc3729064385293750f700cc3e",
    "marina:Marina  Rumiansev:25905:ac297bdc3729064385293750f700cc3e",
    "cyner:Cynthia  Roberts:32913:ac297bdc3729064385293750f700cc3e",
    "lareeves:Lisa  Reevesman:5501:ac297bdc3729064385293750f700cc3e",
    "jrossiot:Julie  Rossi-Otremba:150832:ac297bdc3729064385293750f700cc3e",
    "rogawski:Michael A Rogawski:157863:ac297bdc3729064385293750f700cc3e",
    "djronnin:DeAnn J Ronning:195414:ac297bdc3729064385293750f700cc3e",
    "magic:Sharon  Schauer:49627:ac297bdc3729064385293750f700cc3e",
    "jill.poloske:Jill  Poloske:1409142:ac297bdc3729064385293750f700cc3e",
    "lhsteven:Lisa  Stevenson:18419:ac297bdc3729064385293750f700cc3e",
    "karrin.sawyer:Karrin  Sawyer:1460622:ac297bdc3729064385293750f700cc3e",
    "marnie.sheppard:Marnie  Sheppard:2094904:ac297bdc3729064385293750f700cc3e",
    "kristine.sleuter:Kristine  Sleuter:1768785:ac297bdc3729064385293750f700cc3e",
    "alicia.sheppard:Alicia  Sheppard:1959261:ac297bdc3729064385293750f700cc3e",
    "gjsetka:Gail  Setka:54060:ac297bdc3729064385293750f700cc3e",
    "mstanton:Maureen  Stanton:25460:ac297bdc3729064385293750f700cc3e",
    "szcsmith:Cheryl A. Smith:9126:ac297bdc3729064385293750f700cc3e",
    "csawyers:Christina  Sawyers:2287193:ac297bdc3729064385293750f700cc3e",
    "s9575:Stephanie  Soares:22109:ac297bdc3729064385293750f700cc3e",
    "jesmith:Jane  Smith:16422:ac297bdc3729064385293750f700cc3e",
    "steward:Pauline  Stuart:36817:ac297bdc3729064385293750f700cc3e",
    "birsingh:Satbir K Singh:199419:ac297bdc3729064385293750f700cc3e",
    "asolorzano1:Amira  Solorzano:238252:ac297bdc3729064385293750f700cc3e",
    "mstaubin:Mary Ann  St Aubin:189640:ac297bdc3729064385293750f700cc3e",
    "sstone:Sherri  Stone:58047:ac297bdc3729064385293750f700cc3e",
    "jsauer:Judith  Sauer:50695:ac297bdc3729064385293750f700cc3e",
    "pamtise:Pam  Tise:6824:ac297bdc3729064385293750f700cc3e",
    "txtran:Thoan  Tran:31305:ac297bdc3729064385293750f700cc3e",
    "hto:Hue  To:97293:ac297bdc3729064385293750f700cc3e",
    "vjtibbs:Vicky  Tibbs:131496:ac297bdc3729064385293750f700cc3e",
    "tanguay:Shannon  Tanguay:29650:ac297bdc3729064385293750f700cc3e",
    "bktran:Tina  Tran:30789:ac297bdc3729064385293750f700cc3e",
    "szklt:Kristi  Threlkeld:33083:ac297bdc3729064385293750f700cc3e",
    "claudine:Claudine  Thompson:29065:ac297bdc3729064385293750f700cc3e",
    "erik.wisner:Erik R Wisner:1589322:ac297bdc3729064385293750f700cc3e",
    "joawilli:Jo Ann  Williams:50536:ac297bdc3729064385293750f700cc3e",
    "tyrone11:Tom  Watkins:54118:ac297bdc3729064385293750f700cc3e",
    "mwilburs:Meredith  Wilbur Singh:132017:ac297bdc3729064385293750f700cc3e",
    "lpyen:Lonna  Yen:46149:ac297bdc3729064385293750f700cc3e",
    "myasner:Matthew  Yasner:145378:ac297bdc3729064385293750f700cc3e",
    "ajzabel:Angela J Zabel:150762:ac297bdc3729064385293750f700cc3e",
    "atoering:Amanda J Toering:240972:ac297bdc3729064385293750f700cc3e",
    "geisler:Barbara  Geisler:15715:ac297bdc3729064385293750f700cc3e",
    "mclaughl:Nancy  McLaughlin:25331:ac297bdc3729064385293750f700cc3e",
    "keminer:Kathy  Miner:7337:ac297bdc3729064385293750f700cc3e",
    "csimmons:Cynthia  Simmons:2263457:ac297bdc3729064385293750f700cc3e",
    "crderamo:Carolyn R. D'Eramo:2287141:ac297bdc3729064385293750f700cc3e",
    "imagine7:Kelli  Sholer:6987:ac297bdc3729064385293750f700cc3e",
    "jbcosca:Josephine  Cosca:48934:ac297bdc3729064385293750f700cc3e",
    "cajun:Ronald  Ottman:12944:ac297bdc3729064385293750f700cc3e",
    "perrone:Katherine  Perrone:28500:ac297bdc3729064385293750f700cc3e",
    "revkay:Patricia Kathleen Allen:1094:ac297bdc3729064385293750f700cc3e",
    "slludwig:Shaunna  Ludwig:50869:ac297bdc3729064385293750f700cc3e",
    "bekele:Aklil  Bekele:21947:ac297bdc3729064385293750f700cc3e",
    "eteica:Eteica  Spencer:27600:ac297bdc3729064385293750f700cc3e",
    "browne:Elaine  Brown:29360:ac297bdc3729064385293750f700cc3e",
    "teresita:Tuyet  Hoang:40499:ac297bdc3729064385293750f700cc3e",
    "tafoya:Darla  Tafoya:19786:ac297bdc3729064385293750f700cc3e",
    "cgdufern:Cynthia  Dufern:19422:ac297bdc3729064385293750f700cc3e",
    "kshayden:Kathy  Hayden:6845:ac297bdc3729064385293750f700cc3e",
    "bandit:Mary  Dixon:25352:ac297bdc3729064385293750f700cc3e",
    "lehfeldt:Jennifer Lehfeldt Wyatt:51440:ac297bdc3729064385293750f700cc3e",
    "rgberger:Rachel G. Berger:145265:ac297bdc3729064385293750f700cc3e",
    "cksisson:Christy K Sisson:234510:ac297bdc3729064385293750f700cc3e",
    "orcasisl:Cathy  Miller:4272:ac297bdc3729064385293750f700cc3e",
    "cgilmore:Craig Robert Gilmore:61160:ac297bdc3729064385293750f700cc3e",
    "jkeight:Joy  Keightley:95759:ac297bdc3729064385293750f700cc3e",
    "reaquino:Ruth E. Victor:4170:ac297bdc3729064385293750f700cc3e",
    "pboulos:Paul J Boulos:287909:ac297bdc3729064385293750f700cc3e",
    "cjdiaz:Catherine J Diaz:48778:ac297bdc3729064385293750f700cc3e",
    "callaway:David  Callaway:4616:ac297bdc3729064385293750f700cc3e",
    "jan.penrose:Jan  Penrose:2124934:ac297bdc3729064385293750f700cc3e",
    "dedra:Dedra D Martinez:153386:ac297bdc3729064385293750f700cc3e",
    "ajcaruso:Antionette J Caruso:127454:ac297bdc3729064385293750f700cc3e",
    "cherylr:Cheryl  Richards:335792:ac297bdc3729064385293750f700cc3e",
    "amyd:Amy  Weaver:13553:ac297bdc3729064385293750f700cc3e",
    "mdill:Mick  Dill:98075:ac297bdc3729064385293750f700cc3e",
    "mary.kelly:Mary Jo  Kelly:1216092:ac297bdc3729064385293750f700cc3e",
    "cjklewis:Cynthia  Lewis:56175:ac297bdc3729064385293750f700cc3e",
    "cnickles:Carol Meade Nickles:53627:ac297bdc3729064385293750f700cc3e",
    "ajpretto:Andrew  Prettol:11451:ac297bdc3729064385293750f700cc3e",
    "szshrive:Joel  Shriver:18781:ac297bdc3729064385293750f700cc3e",
    "aghaza:Laura  Aghazadeh:289499:ac297bdc3729064385293750f700cc3e",
    "bllerol:Bette L. Lerol:39936:ac297bdc3729064385293750f700cc3e",
    "kaplowie:Angus  Chang:24971:ac297bdc3729064385293750f700cc3e",
    "kcruit:Kimberly L Reynolds:61888:ac297bdc3729064385293750f700cc3e",
    "ssulliva:Sheryl  Sullivan:143169:ac297bdc3729064385293750f700cc3e",
    "cgispert:Carmen  Gispert:55241:ac297bdc3729064385293750f700cc3e",
    "tlvaldep:Timmy L Valdepena:240974:ac297bdc3729064385293750f700cc3e",
    "cbrunner:Christie  Brunner:59649:ac297bdc3729064385293750f700cc3e",
    "jrsamsel:John  Samsel:12840:ac297bdc3729064385293750f700cc3e",
    "phantem:Chris  Brandt:5286:ac297bdc3729064385293750f700cc3e",
    "mmbrown:Mary  Brown:42702:ac297bdc3729064385293750f700cc3e",
    "au:Laurel  Sorensen:98258:ac297bdc3729064385293750f700cc3e",
    "irwin.liu:Irwin K Liu:1278726:ac297bdc3729064385293750f700cc3e",
    "pbrinck:Peter  Brinckerhoff:38397:ac297bdc3729064385293750f700cc3e",
    "ctagutos:Cherish Tejada Agutos:80177:ac297bdc3729064385293750f700cc3e",
    "mzsanche:Mari  Royer:34699:ac297bdc3729064385293750f700cc3e",
    "kwisdom:Kathleen  Wisdom:129665:ac297bdc3729064385293750f700cc3e",
    "salfaro:Sandra  Alfaro:133773:ac297bdc3729064385293750f700cc3e",
    "fzjanes:Judy  Janes:1351:ac297bdc3729064385293750f700cc3e",
    "keikenba:Kristina  Eikenbary:148946:ac297bdc3729064385293750f700cc3e",
    "kennethburtis:Ken  Burtis:964698:ac297bdc3729064385293750f700cc3e",
    "sand:Sandra  Vaden:17138:ac297bdc3729064385293750f700cc3e",
    "rosanne.aguirre:Rosanne  Aguirre:885762:ac297bdc3729064385293750f700cc3e",
    "mmontelo:Mireya  Montelongo:230804:ac297bdc3729064385293750f700cc3e",
    "geaster:Gail  Easter:149608:ac297bdc3729064385293750f700cc3e",
    "kcolsen:C. Kathy Olsen:22021:ac297bdc3729064385293750f700cc3e",
    "abejones:Abram  Jones:147156:ac297bdc3729064385293750f700cc3e",
    "davwoods:David L Woods:37622:ac297bdc3729064385293750f700cc3e",
    "kanders:Kelly  Anders:20507:ac297bdc3729064385293750f700cc3e",
    "arevalo:Edwin M Arevalo:150241:ac297bdc3729064385293750f700cc3e",
    "howie:Gina  Anderson:11504:ac297bdc3729064385293750f700cc3e",
    "szbekele:Solomon  Bekele:17393:ac297bdc3729064385293750f700cc3e",
    "fzboork:Jo-Anne  Boorkman:24787:ac297bdc3729064385293750f700cc3e",
    "barbara.horwitz:Barbara  Horwitz:1186062:ac297bdc3729064385293750f700cc3e",
    "rhendric:Richard M Hendricks:194039:ac297bdc3729064385293750f700cc3e",
    "szrkorte:Renee  Korte:33077:ac297bdc3729064385293750f700cc3e",
    "katehi:Linda P.B. Katehi:283136:ac297bdc3729064385293750f700cc3e",
    "szblasky:Bobbie  Lasky:17977:ac297bdc3729064385293750f700cc3e",
    "semerson:Shelley  Lopez-Emerson:7667:ac297bdc3729064385293750f700cc3e",
    "clmelendy:Constance  Melendy:1646027:ac297bdc3729064385293750f700cc3e",
    "mnorthup:Mary  Northup:133789:ac297bdc3729064385293750f700cc3e",
    "kcpoole:Kimberley  Poole:50389:ac297bdc3729064385293750f700cc3e",
    "kapullia:Kimberly  Pulliam:87629:ac297bdc3729064385293750f700cc3e",
    "stephenp:Stephen  Paulsen:147436:ac297bdc3729064385293750f700cc3e",
    "ewilson:Everett  Wilson:50407:ac297bdc3729064385293750f700cc3e",
    "pandora:Crystal Y Barber:54389:ac297bdc3729064385293750f700cc3e",
    "wtuck:William  Tuck:156345:ac297bdc3729064385293750f700cc3e",
    "carolyn.borgnino:Carolyn J Borgnino:947538:ac297bdc3729064385293750f700cc3e",
    "itjoycee:Joyce  Johnstone:12544:ac297bdc3729064385293750f700cc3e",
    "binsingh:Binnie  Singh:31798:ac297bdc3729064385293750f700cc3e",
    "smavocet:Sandra  Stewart:18451:ac297bdc3729064385293750f700cc3e",
    "lgjohnst:Lisa  Johnston:54407:ac297bdc3729064385293750f700cc3e"]

    return mivUsers[random.nextInt(len(mivUsers))]
      
def setUserNameAndPassword(self, username, fullname, principalId, password):
    self.username = username
    self.fullname = fullname
    self.principalId = principalId
    self.password = password
      
      
def getSearchResult(self, resultText):
    pattern = Pattern.compile("<div class=\"searchresults\">.*?</div>");
    resultText = ' '.join(resultText.split());          # remove all white space
    searchResult = pattern.matcher(resultText);
    self.dossierSelected = ''  
    self.userSelected = ''  
    self.switchUserSelected = ''  
    
    # If there are search results, select the first item, either a dossier or a user
    if searchResult.find():
       result = resultText[searchResult.start():searchResult.end()]
       result = result.replace("<div class=\"searchresults\">","").replace("</div>","")
       
       # get the result count 
       [text,count] = result.split("=")
    
       # Check for a dossier
       manageDossierPattern = Pattern.compile("<a title=\"Manage Action for .*?\">.*?</a>");
       manageDossierResult = manageDossierPattern.matcher(resultText);
       # Select a random dossier
       if manageDossierResult.find():
          select = random.nextInt(int(count))
          for i in range(select):
              manageDossierResult.find(manageDossierResult.end())
          self.dossierSelected = manageDossierResult.group()
       else: 
           # Check for a user
           manageUserPattern = Pattern.compile("<a title=\"Edit Details For.*?\">.*?</a>");
           manageUserResult = manageUserPattern.matcher(resultText)
           # Select a random user
           if manageUserResult.find():
               select = random.nextInt(int(count))
               for i in range(select):
                   manageUserResult.find(manageUserResult.end())
               self.userSelected = manageUserResult.group()
           else:
                switchUserPattern = Pattern.compile("<a title=\"Switch to.*?\">.*?</a>");
                switchUserResult = switchUserPattern.matcher(resultText)
                if switchUserResult.find():
                    select = random.nextInt(int(count))
                    for i in range(select):
                        switchUserResult.find(switchUserResult.end())
                    self.switchUserSelected = switchUserResult.group()
    else:
       result = 'No items found matching search criteria'
    
    return result
        
def parseTokens(self,text):
    text = ' '.join(text.split());          # remove all white space
    # parse the dossierId, userId, eventId, and flowExecutionKey
    pattern = Pattern.compile("userId=.*?[&|\"]");
    result = pattern.matcher(text)
    if result.find():
        userIdPair = result.group()
        [userIdKey,self.token_userId] = userIdPair[0:len(userIdPair)-1].split("=")
                      
    pattern = Pattern.compile("dossierId=.*?[&|\"]");
    result = pattern.matcher(text)
    if result.find():
        dossierIdPair = result.group()
        [dossierIdKey,self.token_dossierId] = dossierIdPair[0:len(dossierIdPair)-1].split("=")
    
    pattern = Pattern.compile("eventId=.*?[&|\"]");
    result = pattern.matcher(text)
    if result.find():
        eventIdPair = result.group()
        [eventIdKey,self.token__eventId] = eventIdPair[0:len(eventIdPair)-1].split("=")

    pattern = Pattern.compile("id=.*?[&|\"]");
    result = pattern.matcher(text)
    if result.find():
        idPair = result.group()
        [idKey,self.token_id] = idPair[0:len(idPair)-1].split("=")
    
    pattern = Pattern.compile("_flowExecutionKey=.*?[&|\"]");
    result = pattern.matcher(text)
    if result.find():
        flowExecutionKeyPair = result.group()
        [flowExecutionKey,self.token__flowExecutionKey] = flowExecutionKeyPair[0:len(flowExecutionKeyPair)-1].split("=")
        
def setControlVariables():
    controls = grinder.getProperties().getPropertySubset("control")
    global MAX_SLEEP_TIME
    global MIN_ROLE_CHANGE_INTERVAL       
    global MIN_ROUTE_INTERVAL       
    if controls.get('MaxSleepTime') is not None:
        MAX_SLEEP_TIME = int(controls.get('MaxSleepTime'))
    if controls.get('MinRoleChangeInterval') is not None:
        MIN_ROLE_CHANGE_INTERVAL = int(controls.get('MinRoleChangeInterval'))
    if controls.get('MinRouteInterval') is not None:
        MIN_ROUTE_INTERVAL = int(controls.get('MinRouteInterval'))
    logAndPrint("Max Sleep Time Milliseconds=%d" % MAX_SLEEP_TIME)
    logAndPrint("Min Role Change Interval Seconds=%d" % MIN_ROLE_CHANGE_INTERVAL)
    logAndPrint("Min Routing Interval Seconds=%d" % MIN_ROUTE_INTERVAL)

def getMaxSleepTime():
    return MAX_SLEEP_TIME

def setServerUrl():
    server = grinder.getProperties().getPropertySubset("server")
    global SERVER_URL
    if server.get('URL') is not None:
        SERVER_URL = server.get('URL')
    logAndPrint("Server URL=%s" % SERVER_URL)
        
def getServerUrl():
    return SERVER_URL

def sleep():
    sleepTime = random.nextInt(MAX_SLEEP_TIME) 
#    print "Sleeping for "+str(sleepTime/100)+" ms"
    grinder.sleep(sleepTime)

def logMessage(message):
    log.output(message)

def printMessage(message):
    now = datetime.datetime.now() 
    print now.strftime("%Y-%m-%d %H:%M:%S")+" (worker "+str(grinder.getProcessNumber())+" thread "+str(grinder.getThreadNumber())+" run "+str(grinder.getRunNumber())+"): "+message
    
def logAndPrint(message):
    logMessage(message)
    printMessage(message)

def setLastRoleChangeSeconds():
    global LAST_ROLE_CHANGE_SECONDS
    LAST_ROLE_CHANGE_SECONDS = time.clock()
    
def getLastRoleChangeSeconds():
    return LAST_ROLE_CHANGE_SECONDS

def getMinRoleChangeInterval():
    return MIN_ROLE_CHANGE_INTERVAL       

def setLastRouteSeconds():
    global LAST_ROUTE_SECONDS
    LAST_ROUTE_SECONDS = time.clock()
  
def getLastRouteSeconds():
    return LAST_ROUTE_SECONDS

def getMinRouteInterval():
    return MIN_ROUTE_INTERVAL     

def validateRoleChangeLimit():
    lock.acquire()
    try:
        secondsSinceLastRoleChange = time.clock() - getLastRoleChangeSeconds()
        if (getLastRoleChangeSeconds() > 0)  and  (secondsSinceLastRoleChange < getMinRoleChangeInterval()): 
            logMessage = "Role update skipped. Last update was %d secs ago. Max interval for role changes is %d secs." % (secondsSinceLastRoleChange,getMinRoleChangeInterval())
            logAndPrint(logMessage)
            return False
        else: 
            setLastRoleChangeSeconds()
            return True
    finally:
        lock.release()    
             
def validateRouteLimit():
    lock.acquire()
    try:
        secondsSinceLastRoute = time.clock() - getLastRouteSeconds()
        if (getLastRouteSeconds() > 0)  and  (secondsSinceLastRoute < getMinRouteInterval()): 
            logMessage = "Routing skipped. Last route was %d secs ago. Max routing interval is %d secs." % (secondsSinceLastRoute,getMinRouteInterval())
            logAndPrint(logMessage)
            return False
        else:
            setLastRouteSeconds()
            return True
    finally:
        lock.release()    
            
