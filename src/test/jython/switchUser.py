'''
This script exercises the MIV Search User functionality.
Users are searched randomly by Name, LastName, Department, or School.


Created on June 2010
@author: rhendric
'''
import mivCommon
from net.grinder.script import Test
from net.grinder.script.Grinder import grinder
from net.grinder.plugin.http import HTTPPluginControl, HTTPRequest
from HTTPClient import NVPair
connectionDefaults = HTTPPluginControl.getConnectionDefaults()
httpUtilities = HTTPPluginControl.getHTTPUtilities()

from java.util.regex import Matcher
from java.util.regex import Pattern

# To use a proxy server, uncomment the next line and set the host and port.
# connectionDefaults.setProxyServer("localhost", 8001)

# These definitions at the top level of the file are evaluated once,
# when the worker process is started.

connectionDefaults.setUseCookies(1)
connectionDefaults.useContentEncoding = 1
connectionDefaults.setFollowRedirects(0)

# defaults
nextTestNumber = 0
testIncrement = 100
testSuccess = 1

scriptName = 'switchUser'    
serverURL = mivCommon.getServerUrl()

connectionDefaults.defaultHeaders = \
  ( NVPair('Accept-Language', 'en-us,en;q=0.5'),
    NVPair('Accept-Charset', 'ISO-8859-1,utf-8;q=0.7,*;q=0.7'),
    NVPair('Accept-Encoding', 'gzip,deflate'),
    NVPair('User-Agent', 'Mozilla/5.0 (X11; U; Linux x86_64; en-US; rv:1.9.0.2) Gecko/2008092418 CentOS/3.0.2-3.el5.centos Firefox/3.0.2'),
    NVPair('Cache-Control', 'no-cache'), )


headers0= \
  ( NVPair('Accept', 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8'),
    NVPair('Referer', 'https://'+serverURL+'/miv/MIVMain'), )

url0 = 'https://'+serverURL+':443'

class TestRunner:
  """A TestRunner instance is created for each worker thread."""

  def getScriptName(self):
      return scriptName
  
  # A method for each recorded page.
  def selectUser_page(self):
    
    result = self.request101.GET('/miv/SelectUser?_flowId=select-flow')

    if result.getStatusCode() >= 400 and result.getStatusCode() <= 599:
        mivCommon.setTestSuccess(self,0)
    
    # Expecting 302 'Moved Temporarily'
    if result.getStatusCode() == 302:
       self.token__flowExecutionKey = \
       httpUtilities.valueFromLocationURI('_flowExecutionKey') 
       redirectURL = mivCommon.getRedirectURL(result)
       mivCommon.logMessage("Redirecting to: %s" % redirectURL)
       result1 = self.request102.GET(redirectURL)
       if result1.getStatusCode() >= 400 and result1.getStatusCode() <= 599:
           grinder.getStatistics().getForLastTest().setSuccess(0)

       # set the departments/schools allowed to select from for search
       mivCommon.setSearchCriteria(self, result1.getText())  
    
    mivCommon.writeToFile(self.fileHandle,"\nSelectUser_page\n"+result.toString()+result.getText())

  def selectUserSearch_page(self):
        
      logMessage = "SelectUser Searching by "+self.searchBy+": "+self.searchValue
      mivCommon.logAndPrint(logMessage)
    
      request = '/miv/SelectUser' + \
        '?_flowExecutionKey=' + \
        self.token__flowExecutionKey + \
        '&inputName=' + \
        self.token_inputName + \
        '&lname=' + \
        self.token_lname + \
        '&'+self.token_eventId+'=Search+Now!' + \
        '&department=' + \
        self.token_department
              
      if self.token_school != '':
        request = request+'&school=' + self.token_school
    
      # Expecting 302 'Moved Temporarily'
      result = self.request103.GET(request)

      mivCommon.writeToFile(self.fileHandle,"\nSelectUserSearch_page\n"+result.toString()+result.getText())

      if result.getStatusCode() >= 400 and result.getStatusCode() <= 599:
         mivCommon.setTestSuccess(self,0)

      if result.getStatusCode() == 302:
         redirectURL = mivCommon.getRedirectURL(result)
         result1 = self.request104.GET(redirectURL)
         mivCommon.writeToFile(self.fileHandle,"\nSelectUserSearchResults_page\n"+result1.toString()+result1.getText())


      return result1

  def switchUser_page(self):
      if len(self.switchUserSelected) > 0:

       pattern = Pattern.compile(">.*?</a>")
       result = pattern.matcher(self.switchUserSelected)
       self.switchUserName = ''
       if result.find():
           self.switchUserName = result.group().lstrip(">").rstrip("</a>")
        
       mivCommon.parseTokens(self, self.switchUserSelected)    
   
       logMessage = 'Switching to User = '+self.switchUserName
       mivCommon.logAndPrint(logMessage)
       
       result = self.request105.GET('/miv/SelectUser' +
         '?_flowExecutionKey=' +
         self.token__flowExecutionKey +
         '&_eventId=' +
         self.token__eventId +
         '&id=' +
         self.token_id)
       self.token__flowExecutionKey = \
          httpUtilities.valueFromLocationURI('_flowExecutionKey')
          
       if result.getStatusCode() >= 400 and result.getStatusCode() <= 599:
         mivCommon.setTestSuccess(self,0)

       if result.getStatusCode() == 302:
         redirectURL = mivCommon.getRedirectURL(result)
         result1 = self.request106.GET(redirectURL)
         
       return result1
   
  def switchBack_page(self):
       logMessage = 'Switching Back to User = %s (%s / %s)' % (self.fullname,self.username,self.principalId)
       mivCommon.logAndPrint(logMessage)
       
       result = self.request107.GET('/miv/SwitchBack?_flowId=switchback-flow' +
         '&id=' +
         self.token_id)
       self.token__flowExecutionKey = \
          httpUtilities.valueFromLocationURI('_flowExecutionKey')

       if result.getStatusCode() >= 400 and result.getStatusCode() <= 599:
         mivCommon.setTestSuccess(self,0)

       if result.getStatusCode() == 302:
         redirectURL = mivCommon.getRedirectURL(result)
         result1 = self.request108.GET(redirectURL)
         
       return result1


  def __call__(self):
    """This method is called for every run performed by the worker thread."""

    # open a test file
    self.fileHandle = mivCommon.openTestFile(self.getScriptName())

    # defaults
    if self.nextTestNumber is None:
        self.nextTestNumber = nextTestNumber
    
    if self.testIncrement is None:
        self.testIncrement = testIncrement

    self. testSuccess = 1
    
    # If the response back from the server is not one that we except,
    # we want to mark the test as unsuccessful and not include the statistics
    # in the test times. To do this, the delayReports variable can be set to 1.
    # Doing so will delay the reporting back of the statistics until after
    # the test has completed and we have had chance to check its operation.
    # The default is to report back when the test returns control back to
    # the script, i.e. immediately after a test has executed.
    grinder.getStatistics().setDelayReports(1)
 
    # Create an HTTPRequest for each request, then replace the
    # reference to the HTTPRequest with an instrumented version.
    # You can access the unadorned instance using request101.__target__.
    self.nextTestNumber = self.nextTestNumber+self.testIncrement   
    self.request101 = HTTPRequest(url=url0, headers=headers0)
    self.request101 = Test(self.nextTestNumber+1, 'GET SelectUser').wrap(self.request101)
    self.request102 = HTTPRequest(url=url0, headers=headers0)
    self.request102 = Test(self.nextTestNumber+2, 'SelectUser').wrap(self.request102)
    # Replace method with an instrumented version.
    instrumentMethod(Test(self.nextTestNumber, 'selectUser_page'), 'selectUser_page')

    self.nextTestNumber += self.testIncrement
    self.request103 = HTTPRequest(url=url0, headers=headers0)
    self.request103 = Test(self.nextTestNumber+1, 'SelectUserSearchCriteria').wrap(self.request103)
    self.request104 = HTTPRequest(url=url0, headers=headers0)
    self.request104 = Test(self.nextTestNumber+2, 'SelectUserResults').wrap(self.request104)
    # Replace method with an instrumented version.
    instrumentMethod(Test(self.nextTestNumber, 'selectUserSearch_page'), 'selectUserSearch_page')

    self.nextTestNumber += self.testIncrement
    self.request105 = HTTPRequest(url=url0, headers=headers0)
    self.request105 = Test(self.nextTestNumber+1, 'GET Switch User').wrap(self.request105)
    self.request106 = HTTPRequest(url=url0, headers=headers0)
    self.request106 = Test(self.nextTestNumber+2, 'Switch User').wrap(self.request106)
    # Replace method with an instrumented version.
    instrumentMethod(Test(self.nextTestNumber, 'switchUser_page'), 'switchUser_page')

    self.nextTestNumber += self.testIncrement
    self.request107 = HTTPRequest(url=url0, headers=headers0)
    self.request107 = Test(self.nextTestNumber+1, 'GET Switch Back User').wrap(self.request107)
    self.request108 = HTTPRequest(url=url0, headers=headers0)
    self.request108 = Test(self.nextTestNumber+2, 'Switch Back User').wrap(self.request108)
    # Replace method with an instrumented version.
    instrumentMethod(Test(self.nextTestNumber, 'switchBack_page'), 'switchBack_page')

    mivCommon.sleep()  
    self.selectUser_page()

    mivCommon.sleep()  
    result = self.selectUserSearch_page() 
    searchResult = mivCommon.getSearchResult(self,result.getText())
    mivCommon.logAndPrint(searchResult)

    # If a User is selected, switch 
    if len(self.switchUserSelected) > 0:
       self.switchUser_page()
       
       # just switch right back
       self.switchBack_page()    

    scriptResult = "Success"
    if not mivCommon.getTestSuccess(self) :
       scriptResult = "Failed"
    mivCommon.printMessage(self.getScriptName()+' Script Complete: '+ scriptResult)

    mivCommon.closeTestFile(self.fileHandle)    

def instrumentMethod(test, method_name, c=TestRunner):
  """Instrument a method with the given Test."""
  unadorned = getattr(c, method_name)
  import new
  method = new.instancemethod(test.wrap(unadorned), None, c)
  setattr(c, method_name, method)

