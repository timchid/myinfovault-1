/**
 * Copyright
 * Copyright (c) 2007-2008 The Regents of the University of California
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * RESTRICTED RIGHTS LEGEND
 * Use, duplication, or disclosure by the U.S. Government is subject to restrictions
 * as set forth in Subparagraph (c) (1) (ii) of DFARS 252.227-7013 or in FAR
 * 52.227-19, as applicable.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: RTFGenerator.java
 */
package edu.ucdavis.myinfovault.document;

import java.io.File;
import java.io.FileOutputStream;

import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.stream.StreamSource;
import javax.xml.transform.stream.StreamResult;

import edu.ucdavis.myinfovault.XSLTCache;
import edu.ucdavis.myinfovault.RuntimeExec;

/**
 * @deprecated
 * @author rhendric
 *
 */
public class RTFGenerator implements DocumentGenerator
{

    public boolean generate(File xsltFile, File xmlFile, File rtfFile)
    {
        boolean documentCreated = false;
        try
        {
            if (xsltFile.exists() && xmlFile.exists())
            {  
                //Get the source XML.
                Source xmlSource = new StreamSource(xmlFile);
            
                // Get the output fo name
                File outputName = new File(xmlFile.getAbsolutePath()+".fo");
            
                // Transform the XML input to XSL-FO
                StreamResult outputFO = new StreamResult(new FileOutputStream(outputName));

                Transformer transformer = XSLTCache.newTransformer(xsltFile.getAbsolutePath());
                transformer.transform(xmlSource, outputFO);
            
                // Transform the XSL-FO to RTF
                String [] execArray = {System.getProperty("java.home")+"/bin/java",
                                   "-cp",
                                   System.getProperty("catalina.home")+"/webapps/myinfovault/WEB-INF/lib/xfc.jar",
                                   "com.xmlmind.fo.converter.Driver",
                                   outputName.getAbsolutePath(),
                                   rtfFile.getAbsolutePath()};                                   
            
                RuntimeExec.execute(execArray);
            }
            documentCreated = true;

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return documentCreated;
    }
}
