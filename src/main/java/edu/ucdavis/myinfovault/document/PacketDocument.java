/*
 * Copyright © 2007-2010 The Regents of the University of California
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * RESTRICTED RIGHTS LEGEND
 * Use, duplication, or disclosure by the U.S. Government is subject to restrictions
 * as set forth in Subparagraph (c) (1) (ii) of DFARS 252.227-7013 or in FAR
 * 52.227-19, as applicable.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: PacketDocument.java
 */

package edu.ucdavis.myinfovault.document;

import java.io.File;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map;

/**
 * TODO: Add Javadoc comments!
 *
 * @author Rick Hendricks
 * @since MIV 2.?
 */
public class PacketDocument extends Document
{

    /**
     * TODO: Add Javadoc comments
     *
     * @param userId
     * @param collectionId
     * @param documentId
     * @param description
     * @param baseFileName
     * @param dossierFileName
     * @param xsltFile
     * @param xmlFile
     * @param outputFilePath
     * @param uploadFilePath
     * @param concat
     * @param displayContributions
     * @param documentFormats
     * @param packetId
     */
    public PacketDocument(int userId, int collectionId, int documentId, String description, String baseFileName, String dossierFileName, File xsltFile, File xmlFile,
            String outputFilePath, String uploadFilePath, boolean concat,
            boolean displayContributions, Map<DocumentFormat, DocumentGenerator> documentFormats, long packetId)
    {

        super(userId,collectionId,documentId,description,description,baseFileName,dossierFileName,xsltFile,xmlFile,
              outputFilePath, uploadFilePath, concat, displayContributions,
              documentFormats, packetId);
    }

    /**
     * Collect the output document files for this document based on the formats which are defined by
     * iterating through the documentFormats and checking for the existence of the file.
     *
     * Note that when collecting the files, check for the existence of any pre version 2.0 legacy files
     * which may need to be combined into single documents. This is done for PDF format files only.
     *
     */
    @Override
    public boolean collectDocumentFiles()
    {
        boolean rebuildCombinedPdfFile = false;

        this.outputDocuments.clear();

        for (DocumentFormat format : this.documentFormats.keySet())
        {

            String outputFileName = this.getOutputFileName(format);
            String outputFileUrl = this.getOutputFileUrl(format);
            File outputFile = new File(outputFileName);

            // Check for legacy files to combine for PDF format only
            if (format == DocumentFormat.PDF) {
                LinkedHashMap<String, File> fileList = this.getFileList(this.outputFilePath+"/"+format.getFileExtension().toLowerCase()+"/");

                // There will normally only be one file in the list
                if (fileList.size() == 1) {
                    outputFile = (File)(fileList.values().toArray())[0];
                // More than one file indicates this is potentially a post migration set of files
                // where some documents where split into several.
                }
                else {
                    // Combine the documents
                    outputFile = this.combineDocumentsLeftFromMigration(fileList, outputFileName, this.description);
                    // have to rebuild the combined pdf packet file
                    rebuildCombinedPdfFile = true;
                }
            }

            // Validate the file
             boolean success = false;
             if (outputFile.exists()) {
                 if (outputFile.length() > 0)
                 {
                     success = true;
                 }
                 // Validate as a proper PDF file for PDF format
                 if (format == DocumentFormat.PDF) {
                     success = PDFConcatenator.validatePDFFile(outputFile);
                 }
                 // We only add document to the output file list if it actually exists
                 // Files which exist but are zero length or not valid PDF will indicate error (success = false)
                 DocumentFile documentFile = new DocumentFile(success, outputFile, outputFileUrl, format);
                 this.outputDocuments.add(documentFile);
             }
        }
        return rebuildCombinedPdfFile;
    }

    /**
     * getFileList - Get any existing files for this user with the base filename
     * Checks are done for multiple files which may exist for certain
     * publication types If there are multiple files, it indicates that these
     * files are potentially packet files which have not been recreated and must
     * now be combined for viewing. A check is done to see if the files have
     * time stamps times within the millisecond tolerance of each other before
     * combining.
     *
     * @param directory
     * @return LinkedHashMap<String, File> of files which
     */
    private LinkedHashMap<String, File> getFileList(String directory)
    {
        LinkedHashMap<String, File> fileList = new LinkedHashMap<String, File>();
        StringBuilder logMessage = new StringBuilder();
        StringBuilder excluded = new StringBuilder();
        StringBuilder fileName = null;
        File fileObj = null;
        long timestamp = 0;
        long tolerance = 240000; // 4 minute tolerance in milliseconds

        fileName = new StringBuilder(directory).append("/").append(this.baseFileName).append(this.userId).append("_"+this.packetId);
        fileName.append(".pdf");

        // Always add the baseFileName to the list
        fileObj = new File(fileName.toString());
        fileList.put(baseFileName, fileObj);

        // Extending Knowledge
        if (baseFileName.equalsIgnoreCase("PkEkb"))
        {
            if (fileObj.exists())
            {
                timestamp = fileObj.lastModified();
            }
            // Check for Extending Knowledge Workshops
            fileName = new StringBuilder(directory).append("PkEkw").append(this.userId);
            fileName.append(".pdf");
            fileObj = new File(fileName.toString());

            if (fileObj.exists())
            {
                if ((timestamp == 0) || (Math.abs((fileObj.lastModified() - timestamp)) < tolerance))
                {
                    fileList.put("PkEkw", fileObj);
                    if (timestamp == 0)
                    {
                        timestamp = fileObj.lastModified();
                    }
                    else
                    {
                        excluded.append(fileName);
                        excluded.append("\n");
                    }
                }
            }
            // Check for Extending Knowledge Other
            fileName = new StringBuilder(directory).append("PkEko").append(this.userId);
            fileName.append(".pdf");
            fileObj = new File(fileName.toString());

            if (fileObj.exists())
            {
                if ((timestamp == 0) || (Math.abs((fileObj.lastModified() - timestamp)) < tolerance))
                {
                    fileList.put("PkEko", fileObj);
                }
                else
                {
                    excluded.append(fileName);
                    excluded.append("\n");
                }
            }
        }
        // Grants
        else if (baseFileName.equalsIgnoreCase("PkGt"))
        {
            if (fileObj.exists())
            {
                timestamp = fileObj.lastModified();
            }
            // Check for Grants Other
            fileName = new StringBuilder(directory).append("PkOt").append(this.userId);
            fileName.append(".pdf");

            fileObj = new File(fileName.toString());
            if (fileObj.exists())
            {
                if ((timestamp == 0) || (Math.abs((fileObj.lastModified() - timestamp)) < tolerance))
                {
                    fileList.put("PkOt", fileObj);
                }
                else
                {
                    excluded.append(fileName);
                    excluded.append("\n");
                }
            }
        }
        // Teaching
        else if (baseFileName.equalsIgnoreCase("PkTea"))
        {
            if (fileObj.exists())
            {
                timestamp = fileObj.lastModified();
            }
            // Check for Teaching Supplement
            fileName = new StringBuilder(directory).append("PkTea2").append(this.userId);
            fileName.append(".pdf");
            fileObj = new File(fileName.toString());

            if (fileObj.exists())
            {
                if ((timestamp == 0) || (Math.abs((fileObj.lastModified() - timestamp)) < tolerance))
                {
                    fileList.put("PkTea2", fileObj);
                }
                else
                {
                    excluded.append(fileName);
                    excluded.append("\n");
                }
            }
        }
        // Publications
        else if (baseFileName.equalsIgnoreCase("PkPub"))
        {
            if (fileObj.exists())
            {
                timestamp = fileObj.lastModified();
            }
            // Check for Precent Effort
            // fileName = new StringBuilder(directory).append("PkPub2").append(userID);
            // if (suffix != null)
            // {
            //     fileName.append(suffix).append(sequence);
            // }
            // fileName.append(".pdf");
            // fileObj = new File(fileName.toString());
            // if (fileObj.exists())
            // {
            // if ((timestamp == 0) || (Math.abs((fileObj.lastModified() -
            // timestamp)) < tolerance))
            // {
            // fileList.put("PkPub2", fileObj);
            // if (timestamp == 0)
            // {
            // timestamp = fileObj.lastModified();
            // }
            // }
            // else
            // {
            // excluded.append(fileName);
            // excluded.append("\n");
            // }
            // }
            // Check for Contributions only if displayContributions is true
            if (this.displayContributions)
            {
                fileName = new StringBuilder(directory).append("PkPub3").append(this.userId);
                fileName.append(".pdf");
                fileObj = new File(fileName.toString());

                if (fileObj.exists())
                {
                    if ((timestamp == 0) || (Math.abs((fileObj.lastModified() - timestamp)) < tolerance))
                    {
                        fileList.put("PkPub3", fileObj);
                    }
                    else
                    {
                        excluded.append(fileName);
                        excluded.append("\n");
                    }
                }
            }
        }
        // Log entry when there are multiple files
        if (fileList.size() > 1)
        {
            logMessage.append(fileList.size()).append(" files found to concatenate for document type: ").append(baseFileName).append(
                    " User: ").append(this.userId).append(" Files:");
            for (String baseName : fileList.keySet())
            {
                logMessage.append(" ");
                logMessage.append(fileList.get(baseName).getName());
            }
            if (excluded.length() > 0)
            {
                logMessage.append("\n* The following files were excluded:\n");
                logMessage.append(excluded);
            }
            log.info(logMessage.toString());
        }
        return fileList;
    }

    /**
     * Combine any packet documents which have not been recreated since migration
     */
    private File combineDocumentsLeftFromMigration(LinkedHashMap<String,File>fileList, String targetFileName, String targetFileDescription)
    {
        PDFConcatenator concatenator = new PDFConcatenator();
        File targetFile = null;
        LinkedList<String> files = new LinkedList<String>();
        // Use the target file directory for the temp file
        File tempDirectory = new File(new File(targetFileName).getParent());

        // Attempt to combine these separate files.
        for (String fileName : fileList.keySet())
        {
            if (fileName != null)
            {
                files.add(fileList.get(fileName).getAbsolutePath());
            }
        }
        if (files.size() > 0)
        {
            try
            {
                File concatOutputFile = File.createTempFile("concatenationTemp", ".pdf_"+ System.currentTimeMillis(), tempDirectory);
                concatenator.concatenateWithSingleBookmark(files, concatOutputFile, targetFileDescription);
                // concatenation is complete
                if (concatOutputFile.exists() && concatOutputFile.length() > 0) {
                    try
                    {
                        // Rename all files used to build the new output first since one of the names
                        // will be collide with the name of the new output.
                        int count = 1;
                        StringBuilder logMessage = new StringBuilder();
                        logMessage.append("Files concatenated to form new output file ")
                                  .append(targetFileName)
                                  .append(":\n");

                        for (String fileName : fileList.keySet())
                        {
                            if (fileName != null)
                            {
                                try
                                {
                                    File oldFile = fileList.get(fileName);
                                    if (oldFile.exists())
                                    {
                                        String oldName = oldFile.getAbsolutePath() + "_" + count + "_" + System.currentTimeMillis();
                                        targetFile = new File(oldName);
                                        if (!oldFile.renameTo(targetFile))
                                        {
                                            throw new SecurityException("Rename of input to concatenation failed!");
                                        }
                                        count++;
                                        logMessage.append(oldName);
                                        logMessage.append("\n");
                                    }
                                }
                                catch (SecurityException se)
                                {
                                    log.error("Unable to rename concatenated file to " + targetFileName, se);
                                    se.printStackTrace(System.out);
                                }
                            }
                        }

                        // Rename the newly constructed output file here
                        targetFile = new File(targetFileName);
                        if (!concatOutputFile.renameTo(targetFile))
                        {
                            throw new SecurityException("Rename to new output failed!");
                        }
//                        log.logp(Level.INFO, this.getClass().getName(), "combineDocumentsLeftFromMigration", logMessage.toString());
                        log.info(logMessage.toString());
                    }
                    catch (SecurityException se)
                    {
                        log.error("Unable to rename temp output from concatenation to " + targetFileName, se);
                        se.printStackTrace(System.out);
                    }
                }
            }
            catch (IOException ioe)
            {
               log.error("Unable to create temp output file for concatenation: ", ioe);
               ioe.printStackTrace(System.out);
            }
        }
        return targetFile;
    }
}
