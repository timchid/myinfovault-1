/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: BiosketchDocument.java
 */

package edu.ucdavis.myinfovault.document;

import java.io.File;
import java.util.Map;

import edu.ucdavis.mw.myinfovault.domain.biosketch.BiosketchAttributes;
import edu.ucdavis.mw.myinfovault.domain.biosketch.BiosketchData;

/**
 * TODO: javadoc
 *
 * @author Rick Hendricks
 * @since MIV 2.3
 */
public class BiosketchDocument extends Document
{
    private BiosketchData biosketchData = null;
    private Iterable<BiosketchAttributes> biosketchAttributeList = null;
    private boolean bodyFormatting = false;

    /**
     * TODO: javadoc
     *
     * @param userId
     * @param collectionId
     * @param biosketchData
     * @param biosketchAttributeList
     * @param bodyFormatting
     * @param documentId
     * @param description
     * @param title
     * @param baseFileName
     * @param xsltFile
     * @param xmlFile
     * @param outputFilePath
     * @param concatenateUploads
     * @param documentFormats
     */
    public BiosketchDocument(int userId,
                             int collectionId,
                             BiosketchData biosketchData,
                             Iterable<BiosketchAttributes> biosketchAttributeList,
                             boolean bodyFormatting,
                             int documentId,
                             String description,
                             String title,
                             String baseFileName,
                             File xsltFile,
                             File xmlFile,
                             String outputFilePath,
                             boolean concatenateUploads,
                             Map<DocumentFormat, DocumentGenerator> documentFormats)
    {

        super(userId,
              collectionId,
              documentId,
              description,
              title,
              baseFileName,
              xsltFile,
              xmlFile,
              outputFilePath,
              "",
              concatenateUploads,
              false,
              false,
              "",
              documentFormats);

        this.bodyFormatting= bodyFormatting;
        this.biosketchData = biosketchData;
        this.biosketchAttributeList = biosketchAttributeList;
    }

    /**
     * @return if including format options
     */
    public boolean isBodyFormatting()
    {
        return this.bodyFormatting;
    }

    /**
     * @return biosketch data
     */
    public BiosketchData getBiosketchData()
    {
        return biosketchData;
    }

    /**
     * @param biosketchData biosketch data
     */
    public void setBiosketchData(BiosketchData biosketchData)
    {
        this.biosketchData = biosketchData;
    }

    /**
     *  @return biosketch attribute list
     */
    public Iterable<BiosketchAttributes> getBiosketchAttributeList()
    {
        return biosketchAttributeList;
    }

    /**
     * @param biosketchAttributeList biosketch attribute list
     */
    public void setBiosketchAttributeList(Iterable<BiosketchAttributes> biosketchAttributeList)
    {
        this.biosketchAttributeList = biosketchAttributeList;
    }
}
