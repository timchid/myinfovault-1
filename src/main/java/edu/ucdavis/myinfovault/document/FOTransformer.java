/**
 * Copyright
 * Copyright (c) 2007-2008 The Regents of the University of California
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * RESTRICTED RIGHTS LEGEND
 * Use, duplication, or disclosure by the U.S. Government is subject to restrictions
 * as set forth in Subparagraph (c) (1) (ii) of DFARS 252.227-7013 or in FAR
 * 52.227-19, as applicable.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: FOTransformer.java
 */
package edu.ucdavis.myinfovault.document;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.Reader;
import java.io.Writer;

import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerConfigurationException;

import edu.ucdavis.myinfovault.XSLTCache;



/**
 * This class transforms an input XML file content based on the input XSLT style
 * sheet into a newly created output file. The original XML contents are left unaltered.
 * @author rhendric
 */
public class FOTransformer
{
    /**
     * Do the XSL transformation
     * @param xsltFile the XSLT stylesheet file
     * @param xmlFile the XML file
     * @return File the resulting file object containing the transformed output 
     * @throws FileNotFoundException
     * @throws TransformerConfigurationException
     * @throws TransformerException
     */
    public static File transform(File xsltFile, File xmlFile, String outputFileName) throws FileNotFoundException, TransformerConfigurationException,
            TransformerException
    {
        File outputName = null;

        if (xsltFile != null && xmlFile != null && xsltFile.exists() && xmlFile.exists() && (xmlFile.length() > 0))
        {
            // Get the input XML.
            Source xmlSource = new StreamSource(xmlFile);

            // Get the output fo name
            outputName = new File(outputFileName);

            // Transform the XML input
            StreamResult output = new StreamResult(new FileOutputStream(outputName));
            transform(xsltFile, xmlSource, output);
        }
        return outputName;
    }


    public static void transform(File xsltFile, Reader xmlInput, Writer xmlOutput)
        throws TransformerConfigurationException, TransformerException
    {
        if (xmlInput != null && xsltFile != null && xsltFile.exists())
        {
            Source xmlSource = new StreamSource(xmlInput);
            StreamResult output = new StreamResult(xmlOutput);

            transform(xsltFile, xmlSource, output);
        }
    }


    /**
     * @param xsltFile
     * @param xmlSource
     * @param output
     * @throws TransformerConfigurationException
     * @throws TransformerException
     */
    private static void transform(File xsltFile, Source xmlSource, StreamResult output) throws TransformerConfigurationException,
            TransformerException
    {
        Transformer transformer = XSLTCache.newTransformer(xsltFile.getAbsolutePath());
        if (transformer != null)
        {
            transformer.transform(xmlSource, output);
        }
    }
}
