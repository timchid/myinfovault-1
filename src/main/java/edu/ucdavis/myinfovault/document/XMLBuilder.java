/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: XMLBuilder.java
 */

package edu.ucdavis.myinfovault.document;

import java.io.IOException;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.xml.serialize.OutputFormat;
import org.apache.xml.serialize.XMLSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Text;

import edu.ucdavis.mw.myinfovault.domain.annotation.AnnotationLine;
import edu.ucdavis.mw.myinfovault.util.StringUtil;
import edu.ucdavis.mw.myinfovault.util.XmlManager;
import edu.ucdavis.myinfovault.MIVConfig;
import edu.ucdavis.myinfovault.MIVUserInfo;
import edu.ucdavis.myinfovault.PropertyManager;
import edu.ucdavis.myinfovault.data.DataFetcher;
import edu.ucdavis.myinfovault.data.DataMap;
import edu.ucdavis.myinfovault.data.DisplaySection;
import edu.ucdavis.myinfovault.data.DocumentIdentifier;
import edu.ucdavis.myinfovault.data.MIVUtil;
import edu.ucdavis.myinfovault.format.FormatManager;

/**
 * Builds XML for BioSketch documents.
 *
 * @author Lawrence Fyfe
 * @since MIV 2.0
 */
@SuppressWarnings("deprecation") // for imports of OutputFormat and XMLSerializer
public class XMLBuilder
{
    Document document;
    DocumentIdentifier docIdentifier;
    Element root;
    DataMap headerMap;
    DataMap replaceMap;
    ArrayList<String> formatTags = null;
    Map<String, Map<String, String>> sectionHeaderMap;
    String prefix = null;
    String suffix = null;
    String[] nonEscape = null;

    private DataMap fieldPatternMap = null;

    // Default match prefix and suffix in case not found in properties file
    private static final String MATCH_PREFIX = "(?<=\\A|[^\\p{javaUpperCase}\\p{javaLowerCase}0-9])(";
    private static final String MATCH_SUFFIX = ")(?=[^\\p{javaUpperCase}\\p{javaLowerCase}0-9]|\\Z)";


    protected static final Logger log = LoggerFactory.getLogger(XMLBuilder.class);

    // Get any specific properties for the xmlBuilder
    Properties xmlBuidlerProperies = PropertyManager.getPropertySet("xmlbuilder", "config");
    LinkedHashMap<String, String> stripPunctuationFields = new LinkedHashMap<String, String>();
    LinkedHashMap<String, String> publicationsRecordTypeMap = new LinkedHashMap<String, String>();


    /**
     * Constructor to support print/no-print for section headings
     *
     * @param rootName
     * @param fieldPatternMap
     * @param nameMap
     * @param headerMap
     * @param department
     * @param docIdentifier
     */
    public XMLBuilder(String rootName, DataMap fieldPatternMap, DataMap nameMap, Map<String, Map<String, String>> headerMap,
            String department, DocumentIdentifier docIdentifier)
    {
        Element element;
        Text text;

        try
        {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            this.document = builder.newDocument();
            this.docIdentifier = docIdentifier;
            this.fieldPatternMap = fieldPatternMap;
            this.root = this.document.createElement(rootName);
            this.document.appendChild(root);
            this.sectionHeaderMap = headerMap;

            // Initialize maps for filtering input/output
            this.initMaps();

            // Check for name patterns
            if (nameMap != null && nameMap.size() > 0)
            {
                for (String name : nameMap)
                {
                    element = this.document.createElement(name);
                    text = this.document.createTextNode((String) nameMap.get(name));
                    this.root.appendChild(applyFormatting(element, "", text));
                }
            }

            // MIV-2335 - Always include a department element
            element = this.document.createElement("department");
            text = this.document.createTextNode(department != null ? department : "");
            element.appendChild(text);
            this.root.appendChild(element);

            // Get the pattern prefix/suffix
            MIVConfig config = MIVConfig.getConfig();
            this.prefix = config.getProperty("miv-config-pattern-prefix");
            this.suffix = config.getProperty("miv-config-pattern-suffix");
            if (this.prefix == null || this.prefix.length() == 0)
            {
                this.prefix = MATCH_PREFIX;
            }
            if (this.suffix == null || this.suffix.length() == 0)
            {
                this.suffix = MATCH_SUFFIX;
            }

        }
        catch (ParserConfigurationException pce)
        {
            log.error("Unable to create XML document: " + pce.getLocalizedMessage(), pce);
            //pce.printStackTrace();
        }
    }


    /**
     * Initialize maps for filtering input/output
     */
    private void initMaps()
    {
        // Replace strings for output
        replaceMap = new DataMap();
        replaceMap.put("(?i)<br />", "<br/>");
        replaceMap.put("(?i)<hr>", "<hr/>");
        replaceMap.put("(?i)<hr />", "<hr/>");
        replaceMap.put("&nbsp;", "\u00a0");
        replaceMap.put("&(?!amp;|lt;|gt;|quot|#39;)", "&amp;");
        replaceMap.put("<(?!/|\\w)", " &lt; ");
        replaceMap.put("\\s>\\s", " &gt; ");
        replaceMap.put("<ul>\\s*</ul>", " ");
        replaceMap.put("<ol>\\s*</ol>", " ");
        // replaceMap.put("(?i)<br>", "<br/>");
        // replaceMap.put("(?i)</br>", "");
        // replaceMap.put("(?i)<\\\\BR>", "");
        // replaceMap.put("(?i)<hr>", "<hr/>");
        // replaceMap.put("(?i)</HR>", "");
        // replaceMap.put("(?i)<\\\\HR>", "");
        // replaceMap.put("(?i)<\\\\sub>", "</sub>");
        // replaceMap.put("(?i)<\\\\sup>", "</sup>");
        // replaceMap.put("\\r", "");
        // replaceMap.put("<\\d+>", "");
        // replaceMap.put("(?i)<B>", "<b>");
        // replaceMap.put("(?i)</B>", "</b>");
        // replaceMap.put("(?i)<U>", "<u>");
        // replaceMap.put("(?i)</U>", "</u>");
        // replaceMap.put("(?i)<I>", "<i>");
        // replaceMap.put("(?i)</I>", "</i>");
        // replaceMap.put("(?i)<SUB>", "<sub>");
        // replaceMap.put("(?i)</SUB>", "</sub>");
        // replaceMap.put("(?i)<SUP>", "<sup>");
        // replaceMap.put("(?i)</SUP>", "</sup>");

        // replacing empty table columns with space. so the pdf output gets the proper column display.
        replaceMap.put("(<td.*?>)\\s*(</td>)", "$1\u00a0$2");

        // Format tags to check for and handle in input text
        formatTags = new ArrayList<String>();
        formatTags.add("em");
        formatTags.add("strong");
        formatTags.add("u");
        formatTags.add("sub");
        formatTags.add("sup");
        formatTags.add("span");

        // Get the fields from which ending punctuation will be removed
        // String fieldsString =
        //     this.xmlBuidlerProperies.getProperty("removepunctuationfields");
        // String [] fields = fieldsString.split(",");
        // for (String field : fields) {
        //     this.stripPunctuationFields.put(field, field);
        // }
        // Get the punctuation to remove from the fields
        // String punctuationString =
        //     this.xmlBuidlerProperies.getProperty("punctuationvalues");
        // String [] punctuation = punctuationString.split("");
        // for (String punc : punctuation) {
        //     if ((punc != null) && (punc.length() > 0))
        //     {
        //         this.punctuationList.put(punc, punc);
        //     }
        // }

        String nonEscapeElements = this.xmlBuidlerProperies.getProperty("nonescape");
        // Build a map of publication records types from the properties file. The map
        // will contain the value mapped to the property, less the "pubtype-" prefix,
        // and with "-record" appended.
        for (Object objKey : this.xmlBuidlerProperies.keySet())
        {
            String key = (String)objKey;
            if (key.startsWith("pubtype-"))
            {
                String pubType = key.replaceAll("pubtype-","");
                publicationsRecordTypeMap.put(this.xmlBuidlerProperies.getProperty(key),pubType+"-record");
            }
        }

        this.nonEscape = nonEscapeElements.split(",");
    }


    /**
     * Build iterates through the input document map building the XML output
     * document This function is used by the packet building process.
     *
     * @param documentMap
     */
    @SuppressWarnings("unchecked")
    /* for "recordList = (LinkedList)sectionMap.get(...) */
    public void buildPacketXmlDoc(DataMap documentMap)
    {
        DataMap sectionMap = null;
        List<DataMap> recordList = null;
        int count = 1;

        // get any fields/patterns for this document
        for (String sectionName : documentMap)
        {
            addSection(sectionName);

            sectionMap = (DataMap) documentMap.get(sectionName);

            for (String recordName : sectionMap)
            {
                // don't include annotations legends here
                if (!recordName.equalsIgnoreCase(DataFetcher.ANNOTATIONS_LEGENDS))
                {
                    recordList = (List<DataMap>) sectionMap.get(recordName);

                    if (recordList != null)
                    {
                         addRecords(recordList, sectionName, recordName, count);
                    }
                    count = 1;
                }
            }
            // include annotations legends
            addAnnotationsLegends(sectionName,sectionMap);
        }
    }


    /**
     * addRecords - Adds the input recordList for a section and record type to the XML document.
     *
     * @param recordList - DataMap of the input recordList
     * @param sectionName - The section name
     * @param recordName - The record Name
     * @param count - The record count which is used as the record number of the current section
     */
    private void addRecords(List<DataMap> recordList, String sectionName, String recordName, int count)
    {
        // Iterate through the record list
        for (DataMap record : recordList)
        {
            if (addRecord(sectionName, recordName, record, count))
            {
                count++;
            }
        }
    }


    /**
     * Build iterates through the input displayList building the XML output
     * document This function is used by the biosketch building process.
     *
     * @param displayList
     * @param recordNameMap
     * @param mui
     * @param bodyFormatting
     */
    @SuppressWarnings("unchecked")
    /* for "recordList = (LinkedList)sectionMap.get(...) */
    public void buildBiosketchXmlDoc(Iterable<DisplaySection> displayList, DataMap recordNameMap, MIVUserInfo mui,boolean bodyFormatting)
    {
        Map<String, Map<String, String>> sectionMap = null;
        int count = 1;
        DocumentIdentifier originalDocumentIdentifier = this.docIdentifier;


        FormatManager formatting = new FormatManager(mui);
        LinkedHashMap<String, String> tempSectionMap = new LinkedHashMap<String, String>();


        // Iterate through sections and add to the XML document
        for (DisplaySection section : displayList)
        {
            String previousHeader = "";

            // Skip the section if there are no records and the header is set to
            // not display. The header should display when selected to do so, even
            // when no data is present.
            if (section.getRecords().size() == 0 && !section.getShowHeader() && !section.getKey().endsWith("additional"))
            {
                continue;
            }

            // Insure the section is only added once to prevent duplicate sections in the document
            if (!tempSectionMap.containsKey(section.getKey()))
            {
                addSection(section);
                tempSectionMap.put(section.getKey(), section.getHeading());
            }

            if (section.getRecords().size() == 0 && section.getKey().endsWith("additional") && section.getHeading().length()>0)
            {
                String sectionKey = section.getKey();
                Map<String,String>recordTypeMap  = (Map<String,String>)recordNameMap.get(sectionKey);
                DataMap recordMap = new DataMap(recordTypeMap);
               // Get the additional information heading for this record
                recordMap.put("addheader", section.getHeading());

                if (recordMap.containsKey("sequencepreview")) {
                    recordMap.put("sequencepreview", "");
                }
                if (recordMap.containsKey("preview")) {
                    recordMap.put("preview", "");
                }

                // Remove duplicate addheader records to prevent headers from being duplicated in the document
                if (recordMap.get("addheader") != null && (recordMap.containsKey("addheader") && previousHeader.contentEquals((String)recordMap.get("addheader"))))
                {
                    recordMap.put("addheader", "");
                }
                else if (recordMap.containsKey("addheader"))
                {
                    previousHeader = (String)recordMap.get("addheader");
                }
                // Every biosketch is assumed to be displayed, or it would not have been in the input
                recordMap.put("display", "1");

                // Process any markup which may be present in this record
                this.processMarkup (recordTypeMap.get("recordname"), recordMap, formatting);

                if (addRecord(sectionKey, recordTypeMap.get("recordname"), recordMap, count))
                {
                    count++;
                }
                this.docIdentifier = originalDocumentIdentifier;   // Put back originalDocumentIdentifier.
            }
            count = 1;

            sectionMap = section.getRecords();

            for (String recordName : sectionMap.keySet())
            {
                String sectionKey = section.getKey();
                String tableName = null;
                String recordSectionName = null;
                Map<String,String>recordTypeMap  = (Map<String,String>)recordNameMap.get(sectionKey);

                if (recordTypeMap != null)
                {
                    tableName = recordTypeMap.get("tablename");
                    recordSectionName = recordTypeMap.get("recordname");
                }

                DataMap recordMap = new DataMap(sectionMap.get(recordName));

                // This is a hack, but for now its the only way to identify which biosketchrecords
                // are to have formatting applied.
                //
                // If the document id is non-zero check the table from which the data originated for this section.
                // If the table is publicationsummary or presentationsummary, then formatting options are applied to the
                // publication records by setting the document ID to 6 (Publications).
                // If the table is works, events, or reviews, then formatting options are applied to the
                // creative activities records by setting the document ID to 52 (Creative Activities).
                if (this.docIdentifier != DocumentIdentifier.INVALID)
                {
                    if (tableName != null &&
                       (tableName.toLowerCase().contains("publicationsummary") ||
                        tableName.toLowerCase().contains("presentationsummary")))
                    {
                        this.docIdentifier = DocumentIdentifier.PUBLICATIONS;
                    }
                    else if (tableName != null &&
                            (tableName.toLowerCase().contains("works") ||
                             tableName.toLowerCase().contains("events") ||
                             tableName.toLowerCase().contains("publicationevents") ||
                             tableName.toLowerCase().contains("reviews")))
                    {
                        this.docIdentifier = DocumentIdentifier.CREATIVE_ACTIVITIES;
                    }

                    if (bodyFormatting)
                    {
                        this.fieldPatternMap = mui.getUserFieldPatternMap(this.docIdentifier);
                    }
                }

                // When processing publication records, check for the typeid key in the record map.
                // If there, use the section name from the publicationsRecordTypeMap.
                // This mapping exists for situations where multiple publications sections are included under a
                // single section id.
                if (recordSectionName != null && recordSectionName.equalsIgnoreCase("publication-record") &&
                    recordMap.containsKey("typeid") &&
                    publicationsRecordTypeMap.get(recordMap.get("typeid")) != null)
                {
                    recordSectionName = publicationsRecordTypeMap.get(recordMap.get("typeid"));
                }

                // Remove sequencepreview, preview and addheader records when processing additional information
                if (recordSectionName != null && recordSectionName.equalsIgnoreCase("additional-record") &&
                    (recordMap.containsKey("sequencepreview") ||
                    recordMap.containsKey("preview") || recordMap.containsKey("addheader")))
                {

                   // Get the additional information heading for this record
                   recordMap.put("addheader", section.getHeading());

                   if (recordMap.containsKey("sequencepreview")) {
                       recordMap.put("sequencepreview", "");
                   }
                   if (recordMap.containsKey("preview")) {
                       recordMap.put("preview", "");
                   }

                   // Remove duplicate addheader records to prevent headers from being duplicated in the document
                   if (recordMap.get("addheader") != null && (recordMap.containsKey("addheader") && previousHeader.contentEquals((String)recordMap.get("addheader"))))
                   {
                       recordMap.put("addheader", "");
                   }
                   else if (recordMap.containsKey("addheader"))
                   {
                       previousHeader = (String)recordMap.get("addheader");
                   }
                }

                // Every biosketch is assumed to be displayed, or it would not have been in the input
                recordMap.put("display", "1");

                // Process any markup which may be present in this record
                this.processMarkup (recordSectionName, recordMap, formatting);

                if (addRecord(sectionKey, recordSectionName, recordMap, count))
                {
                    count++;
                }
                this.docIdentifier = originalDocumentIdentifier;   // Put back originalDocumentIdentifier.
            }
            count = 1;
        }
    }


    /**
     * @return the string representation of the XML for the document
     */
    public String getXMLString_save()
    {
        /*
         * DOM documents may contain mark-up as data if generated
         * in the WYSIWYG editor. This mark-up is escaped during
         * serialization (as it should be), but needs to be
         * un-escaped for dossier/BioSketch PDF generation.
         */
        return StringEscapeUtils.unescapeHtml(XmlManager.getXmlStr(document));
    }


    public String getXMLString()
    {
        StringWriter writer = new StringWriter();

        try
        {
            OutputFormat format = new OutputFormat(this.document);
            format.setNonEscapingElements(this.nonEscape);
            format.setIndenting(true);

            XMLSerializer serializer = new XMLSerializer(writer, format);
            serializer.asDOMSerializer();
            serializer.serialize(this.document.getDocumentElement());
        }
        catch (IOException ioe)
        {
            log.error("Unable to write XML document: " + ioe.getLocalizedMessage(), ioe);
            ioe.printStackTrace();
        }

        return writer.toString();
    }


    private void addSection(String sectionName)
    {
        Element section = this.document.createElement(sectionName);
        section.setAttribute("id", sectionName);
        section.setIdAttribute("id", true);

        if (this.sectionHeaderMap != null)// && this.sectionHeaderMap.size() > 0)
        {
            if (this.sectionHeaderMap.containsKey(sectionName))
            {
                Map<String, String> sectionMap = sectionHeaderMap.get(sectionName);
                if (sectionMap != null)
                {
                    // newSectionHeading = (String)sectionMap.get("header");
                    String displayHeading = sectionMap.get("display");

                    if ("1".equals(displayHeading))
                    {
                        Element header = this.document.createElement("section-header");
                        Text text = this.document.createTextNode(sectionMap.get("header"));
                        //header.appendChild(text);
                        // section.appendChild(header);
                        section.appendChild(applyFormatting(header, sectionName, text));
                    }
                }
            }
        }
        root.appendChild(section);
    }


    /**
     * Adding annotations legends into section records
     * @param sectionName
     * @param sectionMap
     * <br>Example:
     * <annotations-legends><br>
     *          <legend>* = Publication included in the packet.</legend><br>
     *          <legend>x = Most significant works.</legend><br>
     *          <legend>@ = Refereed.</legend><br>
     * </annotations-legends>
     */
    @SuppressWarnings("unchecked")
    private void addAnnotationsLegends(String sectionName,DataMap sectionMap)
    {
        if(sectionMap.get(DataFetcher.ANNOTATIONS_LEGENDS)!=null)
        {
            List<String> annotationsLegendsList = (List<String>) sectionMap.get(DataFetcher.ANNOTATIONS_LEGENDS);
            Element annotationslegends = this.document.createElement("annotations-legends");
            for (String string : annotationsLegendsList)
            {
                Element legend = this.document.createElement("legend");
                legend.setTextContent(string);
                annotationslegends.appendChild(legend);
            }

            Element parent = this.document.getElementById(sectionName);
            parent.appendChild(annotationslegends);
        }
    }


    private void addSection(DisplaySection displaySection)
    {
        String sectionName = displaySection.getKey();
        Element section = this.document.createElement(sectionName);
        section.setAttribute("id", sectionName);
        section.setIdAttribute("id", true);

        if (this.sectionHeaderMap != null)// && this.sectionHeaderMap.size() >
        {
            if (this.sectionHeaderMap.containsKey(sectionName))
            {
                Map<String, String> sectionMap = sectionHeaderMap.get(sectionName);
                if (sectionMap != null)
                {
                    if (displaySection.getShowHeader())
                    {
                        Element header = this.document.createElement("section-header");
                        Text text = this.document.createTextNode(sectionMap.get("header"));
                        applyFormatting(header, sectionName, text);
                        section.appendChild(header);
                    }
                }
            }
        }
        root.appendChild(section);
    }


    private boolean addRecord(String id, String recordName, DataMap record, int count)
    {
        String displayString;
        boolean display = true;
        Element parent;

        // Should record be displayed?; if no display key is present,then display
        if (record.containsKey("display"))
        {
            displayString = (String) record.get("display");

            if (displayString.equals("0") || displayString.equals("false"))
            {
                display = false;
            }
        }

        if (!display) return false;

        parent = this.document.getElementById(id);

        Element child = addFields(recordName, record, count);

        parent.appendChild(child);
        // String xmlString = getXMLString();
        // System.out.println(xmlString);
        return display;
    }


    @SuppressWarnings("unchecked") // LinkedList<DataMap> cast
    private Element addFields(String recordName, DataMap record, int count)
    {
        Element child;
        Element element;
        Element numberElement;
        Text text;
        String value;
        child = this.document.createElement(recordName);

        text = this.document.createTextNode(Integer.toString(count));
        numberElement = this.document.createElement("number");
        numberElement.appendChild(text);

        child.appendChild(numberElement);

        for (String key : record)
        {
            // Check if this is another hashmap rather than a field
            if (record.get(key) instanceof LinkedHashMap)
            {
                Map<Object,List<DataMap>> nestedRecords = (Map<Object,List<DataMap>>)record.get(key);
                for (Object nestedRecord : nestedRecords.keySet())
                {
                    List<DataMap>fieldDataMaps =  nestedRecords.get(nestedRecord);
                    Iterator<DataMap> it = fieldDataMaps.iterator();
                    int recCount = 1;
                    while (it.hasNext())
                    {
                        child.appendChild(addFields((String)nestedRecord, it.next(), recCount++));
                    }
                }
                continue;
            }
            else if (record.get(key) instanceof AnnotationLine)
            {
                AnnotationLine aLine = (AnnotationLine) record.get(key);

                value = aLine.getLabel();
                text = this.document.createTextNode(MIVUtil.replaceNewline(value));
                // Distinguish at the element level between display before
                // and display after for XSLT Since both display before and
                // after elements actually preceed the record it complicates
                // XSLT handling if they have the same names. i.e. They both
                // are processed when the template is applied for "label"
                // resulting in both labels being printed before the record.
                // MIV-688.
                element = this.document.createElement( aLine.isDisplayBefore() ? "labelbefore" : "labelafter");

                element.setAttribute("rightjustify", String.valueOf(aLine.getRightJustify()));

                /*
                 * TODO: This attribute is actually somewhat redundant now
                 * with the above change and will be removed once confirmed
                 * all label processing is correct.
                 */
                element.setAttribute("displaybefore", String.valueOf(aLine.getDisplayBefore()));

                child.appendChild(applyFormatting(element, key, text));
                continue;
            }

            value = (String) record.get(key);

            if (value == null || value.length() == 0) {
                continue;
            }

            text = this.document.createTextNode(value);
            element = this.document.createElement(key);
            child.appendChild(applyFormatting(element, key, text));
        }
        return child;
    }


    /**
     * To generate fieldkey based on document id.
     * Also try to generate from parentdocumentid if key by documentid not found
     * @param key
     * @return
     */
    private String getFieldKey(String key)
    {
        String fieldKey = new StringBuilder(Integer.toString(this.docIdentifier.getId())).append(":").append(key.toLowerCase()).toString();

        if (this.fieldPatternMap != null && !this.fieldPatternMap.isEmpty() && !this.fieldPatternMap.containsKey(fieldKey))
        {
            fieldKey = new StringBuilder(Integer.toString(this.docIdentifier.getParentDocumentId())).append(":").append(key.toLowerCase()).toString();
        }

        return fieldKey;
    }


    /**
     * Build a map of items within a field to which to apply formatting
     *
     * @param element
     * @param key
     * @param text
     * @return
     */
    private Element applyFormatting(Element element, String key, Text text)
    {
        String fieldKey = getFieldKey(key);

        // Convert any numeric references to character values
        // String textValue = MIVUtil.refToChar(text.getWholeText());
        String textValue = text.getWholeText();
        textValue = swapGreekChars(textValue);
        text.setTextContent(textValue);

        if (this.fieldPatternMap != null && !this.fieldPatternMap.isEmpty() && this.fieldPatternMap.containsKey(fieldKey))
        {
            DataMap fieldMap = (DataMap) this.fieldPatternMap.get(fieldKey);

            for (String field : fieldMap.keySet())
            {
                for (String f : StringUtil.splitToArray(field))
                {
                    DataMap patternMap = (DataMap) fieldMap.get(f);
                    DataMap currentStyleMap = null;

                    // Key into match map. Decimal string in the form "matchStart.matchEnd"
                    if ((patternMap != null) && !patternMap.isEmpty())
                    {
                        // Iterate through the patterns
                        for (String rawPatternValue : patternMap.keySet())
                        {
                            textValue = text.getWholeText();
                            textValue = swapGreekChars(textValue);
                            currentStyleMap = null;

                            if (rawPatternValue != null && rawPatternValue.trim().length() > 0)
                            {
                                // Get the style map for this pattern. Be sure to do this BEFORE
                                // escaping any special characters or numeric entity replacements
                                currentStyleMap = (DataMap) patternMap.get(rawPatternValue);

                                // Escape any markup in this pattern
                                // String matchPattern = MIVUtil.escapeMarkup(rawPatternValue);
                                String matchPattern = swapGreekChars(rawPatternValue);

                                text.setTextContent(highlightPattern(textValue,matchPattern,currentStyleMap));
                            }
                            // No pattern, apply to entire field
                            else
                            {
                                currentStyleMap = (DataMap) patternMap.get(rawPatternValue);
                                text.setTextContent(highlightPattern(textValue,null,currentStyleMap));
                            }
                        }
                    }
                }
            }
        }

        text.setTextContent(locateSymbols(text.getWholeText()));

        // Add all of the elements to the current document
        element = addElements(element, text);

        return element;
    }


    /**
     * wrap all greek symbols with &lt;symbol/&gt; tag
     * @param textValue
     * @return
     */
    private static String locateSymbols(String textValue)
    {
        String patternValue = "\\p{InGreek}+";
        Pattern pattern = Pattern.compile(patternValue);
        Matcher matcher = pattern.matcher(textValue);

        while (matcher.find())
        {
            String replacement = "<symbol>"+matcher.group()+"</symbol>";
            textValue = textValue.replaceAll(matcher.group(), replacement);
        }

        return textValue;
    }


    /**
     * Tag any matches to this pattern found in the input string.
     *
     * @param in
     * @param pattern
     * @param styleMap
     * @return highlight pattern
     */
    public String highlightPattern(String in, String pattern, DataMap styleMap)
    {
        String out = "";

        if (pattern != null && pattern.length() > 0)
        {
            // Build the regex pattern to match on the defined prefix/suffix boundaries
            pattern = this.prefix + Pattern.quote(pattern) + this.suffix;
            String replacement = "<span class=\"" + this.getCssClass(styleMap) + "\">$1</span>";
            out = in.replaceAll(pattern, replacement);
        }
        else
        {
            out = "<span class='" + this.getCssClass(styleMap) + "'>" + in + "</span>";
        }
        return out;
    }


    /**
     * To generate the style css class name based on Bold, Italic and Under rules.
     * @param styleMap
     * @return
     */
    private String getCssClass(DataMap styleMap)
    {
        StringBuilder buf = new StringBuilder();
        buf.append("style");
        buf.append(styleMap.get("bold") != null && styleMap.get("bold").toString().equals("1") ? "B" : "");
        buf.append(styleMap.get("italic") != null && styleMap.get("italic").toString().equals("1") ? "I" : "");
        buf.append(styleMap.get("underline") != null && styleMap.get("underline").toString().equals("1") ? "U" : "");
        return buf.toString();
    }


    /**
     * Create new element nodes for the field and patterns located
     *
     * @param element
     * @param text
     * @return
     */
    private Element addElements(Element element, Text text)
    {
        // Add the Text node to this element after replacing any special tags
        element.appendChild(this.document.createTextNode(replaceTag(text.getData())));

        return element;
    }


    /**
     * @param text
     * @return
     */
    private String replaceTag(String text)
    {
        if (text != null && text.length() > 0)
        {
            for (String key : this.replaceMap)
            {
                String replacement = (String) this.replaceMap.get(key);
                text = text.replaceAll(key, replacement);
//                text = text.replaceAll(key, (String) this.replaceMap.get(key));
            }
            // Replace any CR/LF with space
            text = text.replaceAll("[\\x0d\\x0a]", " ");
            // Replace any remaining non-printable characters with a bullet.
            text = MIVUtil.replaceNonPrintable(text, "\u2022");
        }

        return text;
    }


    /**
     * Temporarily swap these two characters:<br>&nbsp;
     * GREEK LETTER PHI (U+03D5) &#981;<br>&nbsp;
     * GREEK SMALL LETTER PHI (U+03C6) &#966;<br>&nbsp;
     * TODO: (until when?)
     */
    private/* static */String swapGreekChars(String input)
    {
        if (input == null) return "";

        StringBuilder sb = new StringBuilder();
        int len = input.length();
        char replacement;
        for (int i = 0; i < len; i++)
        {
            char ch = replacement = input.charAt(i);

            if (Character.UnicodeBlock.of(ch).equals(Character.UnicodeBlock.GREEK))
            {
                if (ch == 981)
                {
                    replacement = '\u03c6';
                }
                else if (ch == 966)
                {
                    replacement = '\u03d5';
                }
            }
            else
            {
                replacement = ch;
            }
            sb.append(Character.valueOf(replacement));
        }

        return sb.toString();
    }


    /**
     * This class defines the starting and ending positions or range for
     * patterns matched within a record string being processed. MatchRange
     * subclasses BigDecimal and uses the value to the left of the decimal as
     * the starting position for a match, and the value to the right of the
     * decimal as the ending position.
     *
     * MatchRange objects are used as keys in match maps which associate style
     * maps or formatting for the range of characters in the string positions
     * represented by the key. Since the order of finding pattern matches within
     * a string is not necessarily left to right,or from beginning to end, it is
     * necessary to sort the match maps into ascending order once all matching
     * has been completed. Having the BigDecimal superclass as the match map key
     * makes for easy sorting of the match maps keys into a natural order using
     * the TreeMap class.
     *
     * @author rhendric
     */
    class MatchRange extends BigDecimal
    {
        private static final long serialVersionUID = 200803241758L;
        private int start = 0;
        private int end = 0;

        /**
         * Constructor
         *
         * @param startPos
         *            of the match
         * @param endPos
         *            of the match
         */
        MatchRange(int startPos, int endPos)
        {
            super(new StringBuilder(Integer.toString(startPos)).append(".").append(Integer.toString(endPos)).toString());

            this.start = startPos;
            this.end = endPos;
        }

        /**
         * Get the starting position of this match range
         *
         * @return starting position
         */
        public int getStart()
        {
            return start;
        }

        /**
         * Get the ending position of this match range
         *
         * @return ending position
         */
        public int getEnd()
        {
            return end;
        }
    }


    /**
     * Internal class to cache compiled regex patterns. The same patterns are
     * repeatedly compiled during the liftime of the outer class, therefore to
     * increase performance, the compiled patterns are reused.
     *
     * @author rhendric
     */
    public static class PatternCache
    {
        // map patterns to compiled patterns
        private static Map<String, Pattern> cache = new HashMap<String, Pattern>();

        /**
         * Flush all cached patterns from memory, emptying the cache.
         */
        public static synchronized void flushAll()
        {
            cache.clear();
        }

        /**
         * Flush a specific cached pattern from memory.
         *
         * @param regexPattern the pattern to remove
         */
        public static synchronized void flush(String regexPattern)
        {
            cache.remove(regexPattern);
        }

        /**
         * Retrieve a compiled pattern from the cache if present, if not, add
         *
         * @param regexPattern
         *            the pattern to compile.
         * @return a compiled regexPattern.
         */
        public static synchronized Pattern newPattern(String regexPattern)
        {
        //    Logger log = MIVConfig.getConfig().getLogger();

            Pattern compiledPattern = cache.get(regexPattern);

            if (compiledPattern != null)
            {
                log.trace(regexPattern + " retrieved from pattern cache.");
            }

            // create a new entry in the cache if necessary
            if (compiledPattern == null)
            {
                compiledPattern = Pattern.compile(regexPattern);
                cache.put(regexPattern, compiledPattern);
                log.info(regexPattern + " placed in pattern cache.");
            }

            return compiledPattern;
        }
    }


    private void processMarkup (String recordName, DataMap recordMap, FormatManager formatting)
    {
        String rectype = null;
        if (recordName.endsWith("-record"))
        {
            rectype = recordName.substring(0, recordName.length() - 7);
        }

        Properties p = PropertyManager.getPropertySet(rectype, "config");
        @SuppressWarnings({"unchecked"})
        Collection<String> translateFieldsSet = Collections.EMPTY_SET;
        @SuppressWarnings({"unchecked"})
        Collection<String> markupFieldsSet = Collections.EMPTY_SET;

        boolean allowMarkup = Boolean.parseBoolean(p.getProperty("allowmarkup"));
        if (allowMarkup)
        {
            String markupFields = p.getProperty("markupfields");
            markupFieldsSet = markupFields == null ? null : stringToSet(markupFields);
        }
        String translateFields = p.getProperty("translatenewline");

        if (translateFields != null && translateFields.length() > 0)
        {
            translateFieldsSet = translateFields == null ? null : stringToSet(translateFields);
        }

        // Get all String values from the map except notation and footnote
        Map<String, String> tempMap = new LinkedHashMap<>();
        for (String col : recordMap.keySet()) {
            if (recordMap.get(col) instanceof String &&
                    !col.equalsIgnoreCase("footnote") &&
                    !col.equalsIgnoreCase("notation")) {
                tempMap.put(col, (String)recordMap.get(col));
            }
        }
        // Check String values for markup and newlines
        if (!tempMap.isEmpty()) {
            formatting.cleanMarkup(tempMap, markupFieldsSet);
            formatting.translateNewLines(tempMap, translateFieldsSet);
            DataMap cleanMap = new DataMap(tempMap);
            for (String col : cleanMap.keySet()) {
                recordMap.put(col, cleanMap.get(col));
            }
        }
    }


    private Collection<String> stringToSet(String markupFields)
    {
        String[] fields = markupFields.split(",");
        Collection<String> fieldSet = java.util.Arrays.asList(fields);
        return fieldSet;
    }

}
