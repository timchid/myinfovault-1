/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: DocumentGeneratorFactory.java
 */

package edu.ucdavis.myinfovault.document;

import edu.ucdavis.myinfovault.document.XFCGenerator;

/**
 * TODO: javadoc
 * 
 * @author Rick Hendricks
 * @since MIV 2.3
 */
public class DocumentGeneratorFactory
{
    private static final DocumentGeneratorFactory factory = new DocumentGeneratorFactory();
    
    /**
     * Enforcing singleton access. Instantiation outside this class is not allowed.
     */
    private DocumentGeneratorFactory(){}
    
    /**
     * @return document generator factory singleton
     */
    public static DocumentGeneratorFactory getFactory()
    {
        return factory;
    }
    
    /**
     * Get the document generator for the given document format.
     * 
     * @param documentFormat document format associated with a generator
     * @return document generator associated with the given format
     */
    public DocumentGenerator getGenerator(DocumentFormat documentFormat)
    {
        switch (documentFormat)
        {
            case RTF:
            case ODT:
            case WML:
            case DOCX:
                return new XFCGenerator(documentFormat);
            case PDF:
            default:
                return new PDFGenerator();
        }
    }    
}
