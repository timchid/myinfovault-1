/**
 * Copyright
 * Copyright (c) 2007-2008 The Regents of the University of California
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * RESTRICTED RIGHTS LEGEND
 * Use, duplication, or disclosure by the U.S. Government is subject to restrictions
 * as set forth in Subparagraph (c) (1) (ii) of DFARS 252.227-7013 or in FAR
 * 52.227-19, as applicable.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: Builder.java
 */
package edu.ucdavis.myinfovault.document;

import java.io.File;
import java.util.LinkedHashMap;
import java.util.Properties;

import edu.ucdavis.myinfovault.MIVConfig;
import edu.ucdavis.myinfovault.PropertyManager;

/**
 * This is the base class for document building and contains
 * location, format and XML/XSLT information necessary to build a document.
 * @author rhendric
 *
 */
public class Builder
{
    LinkedHashMap<DocumentFormat, DocumentGenerator> documentFormats = new LinkedHashMap<DocumentFormat, DocumentGenerator>();
    private String outputFilePath = null;
    private String uploadFilePath = null;
    private boolean finalizedDocument = false;
    private String xsltFilePath = null;
    private String urlPath = "";
    private String documentType = null;
    private Properties documentProperties = null;
    private int userId;
 //   private String uidDir = "";

    Builder(BuilderDocumentType type)
    {
        this.documentType = type.getDescription();
    }

    /**
     * Initialize the various file paths to be used by the document creation process
     * @param userDir
     */
    public void setFilePaths(File userDir)
    {

        documentProperties = PropertyManager.getPropertySet("document", "config");
        String webAppRoot = MIVConfig.getConfig().getApplicationRoot().getAbsolutePath();

//        // Check for a base directory location document data
//        String baseDocumentLocation = documentProperties.getProperty("baselocation-"+this.documentType);
//        // See if the user ID is to be used in the document output file path. If so, bas location is overridden
//        if (documentProperties.getProperty("location-"+this.documentType+"-byuid","false").equalsIgnoreCase("true") ) {
//            baseDocumentLocation = userDir.getAbsolutePath();
//        }
        String baseDocumentLocation = userDir.getAbsolutePath();

        if (baseDocumentLocation == null || baseDocumentLocation.length() == 0 )
        {
            // Default to web app root if not present
            baseDocumentLocation = webAppRoot;
        }

        // Check for a base directory location document data
        String baseXSLTLocation = documentProperties.getProperty("baselocation-xslt");
        if (baseXSLTLocation == null || baseXSLTLocation.length() == 0 )
        {
            // Default to web app root if not present
            baseXSLTLocation = webAppRoot;
        }

        // Document upload file path relative to the base path.
        String uploadLocation = documentProperties.getProperty("uploadlocation-"+this.documentType);
        if (uploadLocation == null) { uploadLocation = ""; }

        this.uploadFilePath = new File(baseDocumentLocation, uploadLocation).getAbsolutePath();

        // Get the locations for documents
        String location = documentProperties.getProperty("location-"+this.documentType);
        if (location == null) {location = "";}

        // If this is a finalized document, adjust the output location
        if (this.finalizedDocument)
        {
            String locationFinal =  documentProperties.getProperty("location-final-"+this.documentType);
            if (locationFinal == null) { locationFinal = ""; }
            // If this is a finalized document, the location is in a subdirectory relative to the document
            location = location+"/"+locationFinal;
        }

        // Document output file path relative to the base path. These may be stored by user.
        this.outputFilePath = new File(baseDocumentLocation, location).getAbsolutePath();

        this.urlPath = this.outputFilePath;

        // XSLT location relative to base xslt location. Note that this is not currently stored by user
        String xsltLocation =  documentProperties.getProperty("location-xslt-"+this.documentType);
        if (xsltLocation == null)
        {
            xsltLocation = "";
        }
        this.xsltFilePath = new File(baseXSLTLocation, xsltLocation).getAbsolutePath();
    }

    /**
     * Set the document formats which will be used to create the documents.
     * The document formats also define the directory in which a document will be created.
     *
     * Each document format will be created in its own subdirectory under the base output
     * file path. The directories will also be validated and created if not present.
     */
    public void setDocumentFormats()
    {
        String documentFormat = documentProperties.getProperty("format-"+this.documentType);
        if (documentFormat != null)
        {
            String[] formats = documentFormat.split(",");

            DocumentGeneratorFactory generatorFactory = DocumentGeneratorFactory.getFactory();

            for (String formatName : formats)
            {
                DocumentFormat format;
                // = DocumentFormat.valueOf(formatName);
                try {
                    format = DocumentFormat.valueOf(formatName);
                    this.documentFormats.put(format, generatorFactory.getGenerator(format));
                    this.validateDocumentPath(new File(this.outputFilePath, format.getFileExtension()).getAbsolutePath(), true);
                }
                catch (IllegalArgumentException e) {
                    // Got a bad format name; not PDF, RTF, etc
                    // Ignore it and go on to the next format
                    // - should log it too, but there's no log var around. TODO: Add logging later
                }

            }
        }
    }

    /**
     * Validate the input path and create if create flag is true
     * and the path does not exist
     *
     * @param pathStr
     * @param create when set to true the entire path will be created if
     *        it does not already exist
     * @return true if created/exists, false otherwise
     */
    protected boolean validateDocumentPath(String pathStr, boolean create)
    {
        File path = new File(pathStr);
        boolean retVal = true;

        if (!path.isDirectory())
        {
            retVal = false;
            if (create)
            {
                retVal = path.mkdirs();
            }
        }
        return retVal;
    }

    public String getOutputFilePath()
    {
        return outputFilePath;
    }

    public String getUploadFilePath()
    {
        return uploadFilePath;
    }

    public File buildXmlFileObj(String fileName)
    {
        String xmlLocation = documentProperties.getProperty("location-xml");
        File xmlFileObj = new File(new File(this.outputFilePath, xmlLocation), fileName + this.userId + ".xml");
        validateDocumentPath(xmlFileObj.getParent(), true); // validate the path and create if not present
        return xmlFileObj;
    }

    public File buildXsltFileObj(String fileName)
    {
        File xsltFileObj = new File(this.xsltFilePath,fileName+".xslt");
        return xsltFileObj;
    }

    public LinkedHashMap<DocumentFormat, DocumentGenerator> getDocumentFormats()
    {
        return this.documentFormats;
    }

    public void setUserId(int userId)
    {
        this.userId = userId;
    }

    public int getUserId()
    {
        return this.userId;
    }

    public String getDocumentType()
    {
        return this.documentType;
    }


    /**
     * This enum represents the various document types which can be built
     * @author rhendric
     */
    public enum BuilderDocumentType
    {

        PACKET("packet") ,
        DOSSIER("dossier"),
        PREVIEW("preview"),
        BIOSKETCH("biosketch");

        private String description = null;

        private BuilderDocumentType(String description)
        {
            this.description = description;
        }

        String getDescription()
        {
            return this.description;
        }

    }
}

