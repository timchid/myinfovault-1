/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: PDFConcatenator.java
 */

package edu.ucdavis.myinfovault.document;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Logger;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfCopy;
import com.itextpdf.text.pdf.PdfImportedPage;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfSignatureAppearance;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.SimpleBookmark;


/**
 * @author Lawrence Fyfe
 * @since MIV 2.0
 */
public class PDFConcatenator
{
    private static final ThreadLocal<DateFormat> dateFormat =
        new ThreadLocal<DateFormat>() {
            @Override protected DateFormat initialValue() {
                return new SimpleDateFormat("MMMM d, yyyy, h:mm:ss a");
            }
        };
    private static Logger log = Logger.getLogger(PDFConcatenator.class);


    public PDFConcatenator()
    {
        Document.plainRandomAccess = true;
    }

    /**
     * @param fileList of files to concatenate
     * @param outputFile of the concatenation
     */
    public boolean concatenateFiles(List<File> fileList, File outputFile)
    {
        OutputStream opStream = null;
        boolean success = false;

        try {
            opStream = new FileOutputStream(outputFile);

            success = this.concatenateFiles(fileList, opStream);

            try { opStream.flush(); } catch (IOException e) {}
            try { opStream.close(); } catch (IOException e) {}

        }
        catch (FileNotFoundException fnfe) {
            log.error("Unable to create new output file for concatenation: " + outputFile.getName(), fnfe);
        }

        return success;
    }
//    public boolean concatenateFiles(List<File> fileList, File outputFile)
//    {
//        OutputStream opStream = null;
//
//        try
//        {
//            opStream = new FileOutputStream(outputFile);
//        }
//        catch (FileNotFoundException fnfe)
//        {
//            log.error("Unable to create new output file for concatenation: " + outputFile.getName(), fnfe);
//            fnfe.printStackTrace(System.out);
//        }
//
//        // FIXME: Does this ever close the output stream? Looks like it doesn't.
//        return this.concatenateFiles(fileList, opStream);
//    }


    /**
     * TODO: add javadoc
     *
     * @param fileList of files to concatenate
     * @param opStream of the concatenation
     * @return
     */
    public boolean  concatenateFiles(List<File> fileList, OutputStream opStream)
    {
        LinkedList<String> fileNameList = new LinkedList<String>();
        for (File file : fileList)
        {
            fileNameList.add(file.getAbsolutePath());
        }
        return this.concatenate(fileNameList, opStream, null);
    }


    /**
     * @param fileList of files to concatenate
     * @param outputFile of the concatenation
     */
    public boolean  concatenateFiles(LinkedList<String> fileList, File outputFile)
    {
        return this.concatenate(fileList, outputFile, null);
    }


    /**
     * @param fileList of files to concatenate
     * @param outputFile of the concatenation
     * @param bookmarkDescription description to use in place of existing bookmark
     */
    public boolean concatenateWithSingleBookmark(LinkedList<String> fileList, File outputFile, String bookmarkDescription)
    {
        return this.concatenate(fileList, outputFile, bookmarkDescription);
    }


    /**
     * concatenate PDF files and optionally add a descrtiption in place of existing bookmark
     * @param fileList of files to concatenate
     * @param outputFile of the concatenation
     * @param description to use in place of the existing bookmark, when present only the first existing
     *        bookmark will be replace with the description, the others will be ignored and not be present in the
     *        output document.
     *  @return true if successful, false if not
     */
    private boolean concatenate(LinkedList<String> fileList, File outputFile, String description)
    {
        OutputStream opStream = null;
        try
        {
            opStream = new FileOutputStream(outputFile);
        }
        catch (FileNotFoundException fnfe)
        {
            log.error("Unable to create new output file for concatenation: " + outputFile.getName(), fnfe);
            //fnfe.printStackTrace(System.out); // redundant. the logger prints the stack trace to the log file.
        }
        return concatenate(fileList, opStream, description);
    }


    /**
     * concatenate PDF files and optionally add a descrtiption in place of existing bookmark
     * @param fileList of files to concatenate
     * @param outputStream of the concatenation
     * @param description to use in place of the existing bookmark, when present only the first existing
     *        bookmark will be replace with the description, the others will be ignored and not be present in the
     *        output document.
     *  @return true if successful, false if not
     */
    private boolean concatenate(LinkedList<String> fileList, OutputStream outputStream, String description)
    {
        int pageCount;
        Document document = null;
        PdfCopy writer = null;
        PdfImportedPage page;
        int count = 0;
        List<HashMap<String,Object>> allBookmarkList;
        int bookmarkPageOffset = 0;
        boolean success = true;

        try
        {
            if (fileList != null && fileList.size() > 0)
            {
                Document.getVersion();
                allBookmarkList = new ArrayList<HashMap<String,Object>>();
                for (String dataName : fileList)
                {
                    log.debug("Using iText version: " + Document.getVersion());
                    if (!PDFConcatenator.validatePDFFile(dataName))
                    {
                        log.warn(
                                "Input document for concatenation does not exist or contains no data: " +
                                dataName);
                        continue;
                    }
                    try
                    {
                        PdfReader dataReader = new PdfReader(dataName);
                        dataReader.consolidateNamedDestinations();
                        pageCount = dataReader.getNumberOfPages();

                        // Get the bookmarks for this document
                        List<HashMap<String, Object>> bookmarkList = SimpleBookmark.getBookmark(dataReader);

                        // No existing bookmarks...add one which will be the filename itself
                        if (bookmarkList == null || bookmarkList.isEmpty())
                        {
                            bookmarkList = new ArrayList<HashMap<String,Object>>();
                            HashMap<String,Object> bookmarkMap = new HashMap<String,Object>();
                            bookmarkMap.put("Title", new File(dataName).getName());
                            bookmarkMap.put("Action","GoTo");
                            bookmarkMap.put("Page","1 Fit");
                            bookmarkList.add(0,bookmarkMap);
                        }

                        HashMap<String, Object> bookmarkMap = bookmarkList.get(0);
                        // If there is a description present, single bookmark mode is assumed
                        if (description != null && bookmarkMap.containsKey("Title"))
                        {
                            bookmarkMap.put("Title", description);
                            if (allBookmarkList.size() == 0)
                            {
                                allBookmarkList.addAll(bookmarkList);
                            }
                        }
                        else
                        {
                            // Shift pages when multiple bookmarks are present
                            if (bookmarkPageOffset != 0)
                            {
                                SimpleBookmark.shiftPageNumbers(bookmarkList, bookmarkPageOffset, null);
                            }
                            allBookmarkList.addAll(bookmarkList);
                        }

                        // Set the bookmark offset the previous document's page count
                        bookmarkPageOffset += pageCount;

                        // Only create the new document the first time through
                        if (count == 0)
                        {
                            document = new Document(dataReader.getPageSizeWithRotation(1));
                            writer = new PdfCopy(document, outputStream);
                            document.open();
                        }

                        try {
                            for (int i = 1; i <= pageCount; i++)
                            {
                                page = writer.getImportedPage(dataReader, i);
                                writer.addPage(page);
                            }
                        }
                        catch (IllegalArgumentException e) {
                            /* We've been seeing this in the log with a message
                             * "PdfReader not opened with owner password"
                             * Let's find out more about it.
                             */
                            System.out.print  (dateFormat.get().format(new java.util.Date()));
                            System.out.println(" :: PDFConcatenator.concatenate()");
                            System.out.println(" For file [" + dataName + "]");
                            System.out.println(" Metadata " +
                                    (dataReader.isMetadataEncrypted() ? "IS" : "IS NOT") + " encrypted");

                            String msg;

                            int certLevel = dataReader.getCertificationLevel();
                            switch (certLevel)
                            {
                                case PdfSignatureAppearance.NOT_CERTIFIED:
                                    msg = "Not certified";
                                    break;
                                case PdfSignatureAppearance.CERTIFIED_NO_CHANGES_ALLOWED:
                                    msg = "No changes allowed";
                                    break;
                                case PdfSignatureAppearance.CERTIFIED_FORM_FILLING:
                                    msg = "Form filling allowed";
                                    break;
                                case PdfSignatureAppearance.CERTIFIED_FORM_FILLING_AND_ANNOTATIONS:
                                    msg = "Forms and Annotations allowed";
                                    break;
                                default:
                                    msg = "Unknown signature level: " + certLevel;
                                    break;
                            }
                            System.out.println("PDF Certification: " + msg);
                            byte[] userpass = dataReader.computeUserPassword();
                            System.out.println("UserPass: [" + userpass + "]");
                        }
                        writer.freeReader(dataReader);
                        count++;
                    }
                    catch (IOException e)
                    {
                        // Exception occures when there are no pages in the document.
                        // Create the new empty document if needed.
                        // no data in the packet
                        if (count == 0)
                        {
                            log.warn("Input document for concatenation has no pages: " + dataName, e);
                            //e.printStackTrace(System.out);
                            document = new Document();
                            writer = new PdfCopy(document, outputStream);
                            document.open();
                        }
                        count++;
                    }
                }

                if (!allBookmarkList.isEmpty())
                {
                    writer.setOutlines(allBookmarkList);
                }
                try
                {
                    if (document != null) {
                        document.close(); // TODO: does this even throw any Exceptions? The javadoc doesn't mention any.
                    }
                }
                catch (Exception ioe) // FIXME: don't catch plain Exception
                {
                    // Ignore exception here
                }
            }
        }
//        catch (FileNotFoundException fnfe)
//        {
//            log.log(Level.SEVERE, "Unable to create new output file for concatenation: " + outputStream.getName(), fnfe);
//            fnfe.printStackTrace(System.out);
//            success = false;
//        }
        catch (DocumentException de)
        {
            log.error("Unable to initialize PDF document for concatenation", de);
            //de.printStackTrace(System.out);
            success = false;
        }

        return success;
    }


    /**
     * concatenate PDF files and optionally add a descrtiption in place of existing bookmark
     * @param LinkedHashMap of files to concatenate. Key=file description value=filename
     *        The description will be used to replace the first existing bookmark of the file
     *        being concatenated. The resulting document will have a bookmark for file which
     *        was used to assemble the final output.
     * @param outputFile of the concatenation
     */
    public void concatenateFiles(LinkedHashMap<String, String> fileList, File outputFile)
    {
        String dataName = null;
        int pageCount;
        Document document = null;
        PdfCopy writer = null;
        PdfImportedPage page;
        int count = 0;
        ArrayList<HashMap<String,Object>> allBookmarkList;
        int bookmarkPageOffset = 0;
        HashMap<String, Object> bookmarkMap;

        try
        {
            if (fileList != null && fileList.size() > 0)
            {
                allBookmarkList = new ArrayList<HashMap<String,Object>>();
                for (String description : fileList.keySet())
                {
                    dataName = fileList.get(description);
                    if (!PDFConcatenator.validatePDFFile(dataName))
                    {
                        log.warn(
                                "Input document for concatenation does not exist or contains no data: " +
                                dataName);
                        continue;
                    }
                    try
                    {
                        PdfReader dataReader = new PdfReader(dataName);
                        dataReader.consolidateNamedDestinations();
                        pageCount = dataReader.getNumberOfPages();

                        // Get the bookmarks for this document and replace
                        // with the description for this file
                        List<HashMap<String, Object>> bookmarkList = SimpleBookmark.getBookmark(dataReader);
                        if (bookmarkList != null && !bookmarkList.isEmpty())
                        {
                            bookmarkMap = bookmarkList.get(0);
                            if (bookmarkMap.containsKey("Title"))
                            {
                                bookmarkMap.put("Title", description);
                            }

                            if (bookmarkPageOffset != 0)
                            {
                                SimpleBookmark.shiftPageNumbers(bookmarkList, bookmarkPageOffset, null);
                            }
                            allBookmarkList.addAll(bookmarkList);
                        }
                        // Set the bookmark offset the previous documents page count
                        bookmarkPageOffset += pageCount;

                        // Only create the new document the first time through
                        if (count == 0)
                        {
                            document = new Document(dataReader.getPageSizeWithRotation(1));
                            writer = new PdfCopy(document, new FileOutputStream(outputFile));
                            document.open();
                        }
                        for (int i = 1; i <= pageCount; i++)
                        {
                            page = writer.getImportedPage(dataReader, i);
                            writer.addPage(page);
                        }
                        writer.freeReader(dataReader);
                        count++;
                    }
                    catch (IOException e)
                    {
                        // Exception occurs when there are no pages in the document.
                        // Create the new empty document if needed.
                        // no data in the packet
                        if (count == 0)
                        {
                            log.warn("Input document for concatenation has no pages: " + dataName, e);
                            //e.printStackTrace(System.out);
                            document = new Document();
                            writer = new PdfCopy(document, new FileOutputStream(outputFile));
                            document.open();
                        }
                        count++;
                    }
                }

                if (!allBookmarkList.isEmpty())
                {
                    writer.setOutlines(allBookmarkList);
                }

                try
                {
                    document.close(); // TODO: does this even throw any Exceptions? The javadoc doesn't mention any.
                }
                catch (Exception ioe) // FIXME: don't catch plain Exception
                {
                    // Ignore exception here
                }
            }
        }
        catch (FileNotFoundException fnfe)
        {
            log.error("Unable to create new output file for concatenation: " + outputFile.getName(), fnfe);
            //fnfe.printStackTrace(System.out);
        }
        catch (DocumentException de)
        {
            log.error("Unable to initialize PDF document for concatenation to: " + outputFile.getName(), de);
            //de.printStackTrace(System.out);
        }
    }


    /**
     * Validate a PDF file.
     *
     * @param fileName name of the file to validate
     * @return true if valid, false otherwise
     */
    public static boolean validatePDFFile (String fileName)
    {
        boolean isValid = true;
        int pageCount = -1;

        PdfReader dataReader = null;
        try
        {
            dataReader = new PdfReader(fileName);
            pageCount = dataReader.getNumberOfPages();
            if (pageCount <= 0)
            {
                log.warn("PDF File "+fileName+" has a page count of " + pageCount);
                isValid = false;
            }
        }
        catch (IOException ioe)
        {
            log.warn("PDF File "+fileName+" does not exist or is not a valid PDF file; " +
                    ioe + " message: [" + ioe.getMessage() + "] cause: [" + ioe.getCause() + "]");
            isValid = false;

            /* We've been seeing this in the log with a message
             * "PdfReader not opened with owner password"
             * Let's find out more about it.
             */
            System.out.print  (dateFormat.get().format(new java.util.Date()));
            System.out.println(" :: PDFConcatenator.validatePDFFile()");
            System.out.println(" For file [" + fileName + "]");

            if (dataReader == null)
            {
                System.out.println("dataReader is null!");
            }
            else
            {
                System.out.println(" Metadata " +
                        (dataReader.isMetadataEncrypted() ? "IS" : "IS NOT") + " encrypted");

                String msg;

                int certLevel = dataReader.getCertificationLevel();
                switch (certLevel)
                {
                    case PdfSignatureAppearance.NOT_CERTIFIED:
                        msg = "Not certified";
                        break;
                    case PdfSignatureAppearance.CERTIFIED_NO_CHANGES_ALLOWED:
                        msg = "No changes allowed";
                        break;
                    case PdfSignatureAppearance.CERTIFIED_FORM_FILLING:
                        msg = "Form filling allowed";
                        break;
                    case PdfSignatureAppearance.CERTIFIED_FORM_FILLING_AND_ANNOTATIONS:
                        msg = "Forms and Annotations allowed";
                        break;
                    default:
                        msg = "Unknown signature level: " + certLevel;
                    break;
                }
                System.out.println("PDF Certification: " + msg);
                byte[] userpass = dataReader.computeUserPassword();
                System.out.println("UserPass: [" + userpass + "]");
            }
        }

        return isValid;
    }


    /**
     * Validate a PDF file.
     *
     * @param file PDF file to validate
     * @return if valid
     */
    public static boolean validatePDFFile (File file)
    {
        boolean isValid = true;
        int pageCount = -1;

        try
        {
            PdfReader dataReader = new PdfReader(file.getAbsolutePath());
            pageCount = dataReader.getNumberOfPages();
            if (pageCount <= 0)
            {
                log.warn("PDF File " + file.getAbsolutePath() + " has a page count of " + pageCount);
                isValid = false;
            }
        }
        catch (IOException ioe)
        {
            log.warn("PDF File "+file.getAbsolutePath()+" does not exist or is not a valid PDF file.", ioe);
            isValid = false;
        }

        return isValid;
    }


    public static void concatPDFs(List<InputStream> streamOfPDFFiles, OutputStream outputStream, boolean paginate)
    {
        Document document = new Document();
        try {
//            List<InputStream> pdfs = streamOfPDFFiles;
            List<PdfReader> readers = new ArrayList<PdfReader>();
            int totalPages = 0;
//            Iterator<InputStream> iteratorPDFs = pdfs.iterator();
            Iterator<InputStream> iteratorPDFs = streamOfPDFFiles.iterator();

            // Create Readers for the pdfs.
            while (iteratorPDFs.hasNext()) {
                InputStream pdf = iteratorPDFs.next();
                PdfReader pdfReader = new PdfReader(pdf);
                readers.add(pdfReader);
                totalPages += pdfReader.getNumberOfPages();
            }
            // Create a writer for the outputstream
            PdfWriter writer = PdfWriter.getInstance(document, outputStream);

            document.open();
            BaseFont bf = BaseFont.createFont(BaseFont.HELVETICA, "ISO-8859-1", BaseFont.NOT_EMBEDDED);
            PdfContentByte cb = writer.getDirectContent(); // Holds the PDF
            // data

            PdfImportedPage page;
            int currentPageNumber = 0;
            int pageOfCurrentReaderPDF = 0;
            Iterator<PdfReader> iteratorPDFReader = readers.iterator();

            // Loop through the PDF files and add to the output.
            while (iteratorPDFReader.hasNext())
            {
                PdfReader pdfReader = iteratorPDFReader.next();

                // Create a new page in the target for each source page.
                while (pageOfCurrentReaderPDF < pdfReader.getNumberOfPages())
                {
                    document.newPage();
                    pageOfCurrentReaderPDF++;
                    currentPageNumber++;
                    page = writer.getImportedPage(pdfReader, pageOfCurrentReaderPDF);
                    cb.addTemplate(page, 0, 0);

                    // Code for pagination.
                    if (paginate)
                    {
                        cb.beginText();
                        cb.setFontAndSize(bf, 9);
                        cb.showTextAligned(PdfContentByte.ALIGN_CENTER, "" + currentPageNumber + " of " + totalPages, 520, 5, 0);
                        cb.endText();
                    }
                }
                pageOfCurrentReaderPDF = 0;
            }
            outputStream.flush();
            document.close();
            outputStream.close();
        }
        catch (DocumentException de) {
            log.error("Concatenating PDFs failed", de);
        }
        catch (IOException e) {
            // Ignore the IOExceptions from flush() and close()
            //e.printStackTrace();
        }
        finally {
            if (document.isOpen()) document.close();
            if (outputStream != null) try { outputStream.close(); } catch (IOException ioe) {}
        }
    }


    /**
     * Add a single book mark to an existing PDF file.
     *
     * @param inputPdfFile file to which book mark is to be added
     * @param bookmark book mark text
     * @param removeExistingBookmarks remove existing book marks if <code>true</code>
     * @return if successful
     */
    public static boolean addBookmark(File inputPdfFile, String bookmark, boolean removeExistingBookmarks)
    {
        boolean success = true;

        try
        {
            PdfReader reader = new PdfReader(inputPdfFile.getAbsolutePath());
            File outputStampFile = new File(inputPdfFile.getParent(),"stamped.pdf");
            FileOutputStream stampOpstream = new FileOutputStream(outputStampFile.getAbsolutePath());
            PdfStamper stamper = new PdfStamper(reader, stampOpstream);

            // Get the bookmarks for this document
            List<HashMap<String,Object>> bookmarkList = SimpleBookmark.getBookmark(reader);

            // No existing bookmarks...create a list
            if (bookmarkList == null || bookmarkList.isEmpty())
            {
                bookmarkList = new ArrayList<HashMap<String,Object>>();
            }
            if (removeExistingBookmarks)
            {
                bookmarkList.clear();
            }
            HashMap<String, Object> bookmarkMap = new HashMap<String, Object>();
            bookmarkMap.put("Title", bookmark);
            bookmarkMap.put("Action","GoTo");
            bookmarkMap.put("Page","1 Fit");
            bookmarkList.add(0, bookmarkMap);
            stamper.setOutlines(bookmarkList);
            stamper.close();

            // Create temp file to rename the input pdf file to
            File tempFile = File.createTempFile(inputPdfFile.getName(),".tmp",inputPdfFile.getParentFile());
            inputPdfFile.renameTo(tempFile);

            // Rename the stamped output file to the input name
            outputStampFile.renameTo(inputPdfFile);

            // Delete the temp file
            if (tempFile.exists())
            {
                tempFile.delete();
            }
        }
        catch (FileNotFoundException e)
        {
            log.error("Adding a bookmark to PDF File "+inputPdfFile.getAbsolutePath()+" failed.", e);
            success = false;
            //e.printStackTrace();
        }
        catch (DocumentException e)
        {
            log.error("Adding a bookmark to PDF File "+inputPdfFile.getAbsolutePath()+" failed.", e);
            success = false;
            //e.printStackTrace();
        }
        catch (IOException e)
        {
            log.error("Adding a bookmark to PDF File "+inputPdfFile.getAbsolutePath()+" failed.", e);
            success = false;
            //e.printStackTrace();
        }

        return success;
    }
}

