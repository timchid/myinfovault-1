/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: DocumentAccess.java
 */

package edu.ucdavis.myinfovault.document;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.eventbus.Subscribe;

import edu.ucdavis.mw.myinfovault.domain.action.DossierLocation;
import edu.ucdavis.mw.myinfovault.domain.dossier.Dossier;
import edu.ucdavis.mw.myinfovault.events.ConfigurationChangeEvent;
import edu.ucdavis.mw.myinfovault.events.EventDispatcher;
import edu.ucdavis.mw.myinfovault.service.MivServiceLocator;
import edu.ucdavis.mw.myinfovault.service.authorization.AuthorizationService;
import edu.ucdavis.mw.myinfovault.service.authorization.AuthorizationService.Qualifier;
import edu.ucdavis.mw.myinfovault.service.authorization.Permission;
import edu.ucdavis.mw.myinfovault.service.dossier.DossierReviewerDto;
import edu.ucdavis.mw.myinfovault.service.dossier.WorkflowException;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.mw.myinfovault.service.person.MivRole;
import edu.ucdavis.mw.myinfovault.service.person.Scope;
import edu.ucdavis.mw.myinfovault.util.AttributeSet;
import edu.ucdavis.mw.myinfovault.util.StringUtil;


/**
 * Authorizes access to documents.
 *
 * @author Rick Hendricks
 * @since MIV 3.0
 */
public class DocumentAccess
{
    protected final Logger logger = LoggerFactory.getLogger(this.getClass());

    private static final String DEFAULT_CONFIG_FILE = "/documentaccess.properties";
    private static int instanceCount = 0;
    private int instanceNum = 0;

    private final String configFileName;
    private Properties config;
    Map<MivRole, EnumSet<DossierLocation>> permissions = new HashMap<>();


    public DocumentAccess()
    {
        this(DEFAULT_CONFIG_FILE);
    }

    public DocumentAccess(String configFilename)
    {
        this.instanceNum = ++instanceCount;
        logger.info("Creating instance #{} of DocumentAccess", instanceNum);
        this.configFileName = configFilename;
        this.config = loadConfig(this.configFileName);
        if (this.config != null) {
            processConfig(config);
        }
        EventDispatcher.getDispatcher().register(this);
    }


    /**
     * Determine if the user is authorized to view the document.
     *
     * @param userId ID of the user who is accessing the document
     * @param filePath abstract path to file to access
     * @return if authorized to access document
     */
    public boolean isAuthorized(int userId, File filePath)
    {
        /*
         * TODO: In order to implement the individual file viewing permissions, the location will have to be incorporated
         * into the DossierView configuration table. For now we are authorizing all since there is no way to get to a page with
         * links to the individual files without other authorizations being satified.
         */
        return true;
    }


    /**
     * Determine if the role is allowed to view the dossier at its current location
     * @param MivRole
     * @param DossierLocation
     * @return true if this role is authorized to view the Dossier at the current location
     */
    private boolean isAuthorized(MivRole mivRole, Dossier dossier)
    {
        DossierLocation dossierLocation = dossier.getDossierLocation();
        boolean isAllowed = permissions.get(mivRole).contains(dossierLocation);
        logger.info("New configurable dossier view permissions says that {} is {}allowed to view at {}",
                    new Object[] { mivRole, (isAllowed ? "" : "not "), dossierLocation });
        return isAllowed;
    }


    /**
     * Determine if the person is allowed to view a dossier.
     * The qualification is done in two steps
     *    1. Verify that the person has permission to view the dossier based on school and department qualifiers
     *    2. Verify that the dossier is at a location where the role is allowed to view.
     *
     * @param MivPerson
     * @param Dossier
     * @return true if this role is authorized to view the Dossier at the current location
     */
    private boolean isAuthorized(MivPerson mivPerson, Dossier dossier)
    {
        AuthorizationService authorizationService = MivServiceLocator.getAuthorizationService();
        boolean hasPermission = false;

        // Check permission against all departments
        for (Map<String, String> department : dossier.getDepartments())
        {
            // School and department qualifiers
            AttributeSet qualification = new AttributeSet();
            qualification.put(Qualifier.DEPARTMENT, department.get("departmentid"));
            qualification.put(Qualifier.SCHOOL, department.get("schoolid"));
            qualification.put(Qualifier.USERID, dossier.getAction().getCandidate().getUserId()+"");

            // Permission to view the dossier for any one of the departments is sufficient
            if (authorizationService.isAuthorized(mivPerson, Permission.VIEW_DOSSIER, null, qualification)) {
                hasPermission = true;
                break;
            }
        }

        // Person is qualified to view, now check authorization for this role at the dossiers current location
        return hasPermission && this.isAuthorized(this.getEffectiveRole(mivPerson, dossier), dossier);
    }


    /**
     * Determine the assigned effective role for a person.
     * If the primary role of the person is CANDIDATE, then that person can potentially have the DEPT_CHAIR,
     * DEAN or VICEPROVOST roles. If the dossier location corresponds to a location where one of those roles
     * would be in effect for the person, then it is that role which will used to determine the viewing authorization.
     *
     * @param mivPerson
     * @param dossier
     * @return MivRole - the effective role for this person
     */
    private MivRole getEffectiveRole(MivPerson mivPerson, Dossier dossier)
    {
        // If the primary role is CANDIDATE, check if the person also has the DEPT_CHAIR, DEAN or VICEPROVOST roles.
        // If yes and the location corresponds to the location that the secondary role would be in force, then use that role.
        MivRole primaryRoleType = mivPerson.getPrimaryRoleType();
        switch (dossier.getDossierLocation())
        {
            case DEPARTMENT:
                return primaryRoleType == MivRole.CANDIDATE && mivPerson.hasRole(MivRole.DEPT_CHAIR)
                ? MivRole.DEPT_CHAIR : mivPerson.getPrimaryRoleType();

            case SCHOOL:
            case POSTSENATESCHOOL:
            case POSTAPPEALSCHOOL:
                return primaryRoleType == MivRole.CANDIDATE && mivPerson.hasRole(MivRole.DEAN)
                ? MivRole.DEAN : mivPerson.getPrimaryRoleType();

            case VICEPROVOST:
            case POSTSENATEVICEPROVOST:
            case POSTAPPEALVICEPROVOST:
                if (primaryRoleType == MivRole.CANDIDATE && mivPerson.hasRole(MivRole.VICE_PROVOST))
                {
                   return MivRole.VICE_PROVOST;
                }
                if (primaryRoleType == MivRole.CANDIDATE && mivPerson.hasRole(MivRole.PROVOST))
                {
                   return MivRole.PROVOST;
                }
                if (primaryRoleType == MivRole.CANDIDATE && mivPerson.hasRole(MivRole.CHANCELLOR))
                {
                   return MivRole.CHANCELLOR;
                }

            default:
                return primaryRoleType;
        }

    }


    /**
     * Get the Scope for the dossier to be reviewed by this MivPerson.
     * @param mivPerson
     * @param dossier
     * @return Scope - If there are no dossiers for this person a scope of Scope.NONE will be returned.
     */
    public Scope getReviewScope(MivPerson mivPerson, Dossier dossier)
    {
        try
        {
            // Check dossiers to be reviewed by this user
            for (DossierReviewerDto dossierToReview : MivServiceLocator.getDossierService().findDossiersToReview(mivPerson.getUserId()))
            {
                if (dossierToReview.getDossierId() == dossier.getDossierId()
                    && DossierLocation.mapWorkflowNodeNameToLocation(dossierToReview.getReviewLocation()) == dossier.getDossierLocation())
                {
                   return new Scope(dossierToReview.getSchoolId(), dossierToReview.getDepartmentId());
                }
            }
            return Scope.None;
        }
        catch (WorkflowException e)
        {
            logger.warn("Unable to retrieve review scope for user {} for dossierId {}", new Object [] {mivPerson, dossier.getDossierId()});
            return Scope.None;
        }
    }


    /**
     * If the input role is a reviewer role, check authorization by location for that input role,
     * otherwise authorize by the determining the person's effective role
     * @param mivRole
     * @param mivPerson
     * @param dossier
     * @return boolean - true if authorized, otherwise false
     */
    public boolean isAuthorized(MivRole mivRole, MivPerson mivPerson, Dossier dossier)
    {
        // If a reviewer role, authorize the role
        if (mivRole.isReviewerRole())
        {
            return this.isAuthorized(mivRole, dossier);
        }
        return this.isAuthorized(mivPerson, dossier);
    }


    /**
     *
     * @param configFile
     * @return
     */
    private Properties loadConfig(String configFile)
    {
        URL url = this.getClass().getClassLoader().getResource(configFile);
        Properties cfg = new Properties();

        try (FileReader fr = new FileReader(url.getFile())) {
            cfg.load(fr);
        }
        catch (FileNotFoundException e) {
            logger.warn("Config file not found: {}", url.getFile());
            return null;
        }
        catch (IOException e) {
            logger.warn("Failed to read file: {}", url.getFile());
            return null;
        }

        return cfg;
    }

/* This doesn't work — getResourceAsStream caches the file content, so reloading after changing
   the file never sees the changes.  Tried both getClass().getResourceAsStream and
   getClass().getClassLoader().getResourceAsStream.
   It's fine for the initial load, but reloads don't work.

   After first having used the standard getResourceAsStream() which worked fine until I
   added the reload capability, I found this is the only tactic that worked, as suggested
   in a stackoverflow.com discussion I found.

 * This is apparently a problem with the Tomcat custom ClassLoader
   and is allegedly fixed in Tomcat 7.0.40

*/ /*
  (original) private Properties loadConfig()  // no param; originally used instance configFileName directly.
//      InputStream in = this.getClass().getClassLoader().getResourceAsStream(configFileName);
      InputStream in;
//        try
//        {
        //    in = this.getClass().getResourceAsStream(configFileName);
        //    in = this.getClass().getClassLoader().getResource(configFileName).openStream();
            in = this.getClass().getClassLoader().getResourceAsStream(configFileName);
//        }
//        catch (IOException e)
//      {
//            e.printStackTrace();
//            logger.error("Failed to open config file \"{}\"", configFileName, e);
//            return false;
//        }

        if (in != null)
        {
            try
            {
                config = new Properties();
                config.load(in);
                in.close();
            }
            catch (IOException e)
            {
                // Log the error (with/without stack trace?) with the config file name
                logger.error("DocumentAccess configuration error; unable to read config file [" + configFileName + "]", e);
                // Maybe throw an exception here?
                return false;
            }
        }
        else
        {
            return false; // No input stream, we failed to load config.
        }

        return true;
    }
*/


    /**
     * Process the configuration Properties to create a set of per-Role permissions.<br>
     * Each property name (key) must be a valid MIV role enum or it will be logged and skipped.<br>
     * The value for a property must be a single, or comma-seperated list of, valid DossierLocation
     * enum values.<br>
     * @param p
     */
    private void processConfig(Properties p)
    {
        for (Object key : p.keySet())
        {
            String roleName = key.toString().toUpperCase();
            MivRole role;
            try {
                role = MivRole.valueOf(roleName);
            }
            catch (IllegalArgumentException e) {
                logger.warn("Skipping non-existent role [{}]", roleName);
                continue;
            }

            logger.info("Processing role [{}]", role);
            String value = p.getProperty(key.toString());
            if (StringUtils.isEmpty(value)) {
                logger.warn("Skipping role [{}] which has no value assigned", role);
                continue;
            }

            logger.info("Role [{}] has raw value [{}]", role, value);
            EnumSet<DossierLocation> locations;
            if (value.equals("*All*")) {
                locations = EnumSet.allOf(DossierLocation.class);
            }
            else {
                locations = EnumSet.noneOf(DossierLocation.class);
                value = value.toUpperCase();
                String[] locList = StringUtil.splitToArray(value);
                for (String locName : locList)
                {
                    try {
                        DossierLocation location = DossierLocation.valueOf(locName);
                        locations.add(location);
                    }
                    catch (IllegalArgumentException e) {
                        logger.warn("Skipping non-existent location [{}] for [{}]", locName, role);
                    }
                }
            }

            permissions.put(role, locations);
        }
    }


    @Subscribe
    public void configurationChanged(ConfigurationChangeEvent evt)
    {
        logger.info("||----- Reloading options for instance #{}", this.instanceNum);
        this.config = loadConfig(this.configFileName);
        if (this.config != null) {
            processConfig(config);
        }
    }
}
