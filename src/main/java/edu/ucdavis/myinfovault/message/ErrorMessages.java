/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: ErrorMessages.java
 */

package edu.ucdavis.myinfovault.message;

import java.util.ResourceBundle;

import edu.ucdavis.myinfovault.exception.MivSevereApplicationError;

/**
 * Provides access to error messages for {@linkplain MivSevereApplicationError MivSevereApplicationErrors}.
 * Currently, the properties file where the messages are stored is in the package:<br>
 * &nbsp; {@code edu.ucdavis.myinfovault.exception}<br>so the file is:<br>
 * &nbsp; {@code edu/ucdavis/myinfovault/properties/errormessages}.properties
 *
 * @author Stephen Paulsen
 * @since MIV 3.0
 */
public class ErrorMessages extends MessageBundle
{
    private static final String BUNDLE_NAME = "errormessages";
    private static final ResourceBundle RESOURCE_BUNDLE = ResourceBundle.getBundle(BUNDLE_NAME);

    private static MessageBundle instance = new ErrorMessages();


    /** This is a static class that should never be instantiated. */
    private ErrorMessages() { } // don't allow instantiation


    /**
     * Get the single instance of the ErrorMessages object.
     * @return MessageBundle
     */
    public static MessageBundle getInstance()
    {
        return instance;
    }


    @Override
    public ResourceBundle getBundle()
    {
        return RESOURCE_BUNDLE;
    }
}
