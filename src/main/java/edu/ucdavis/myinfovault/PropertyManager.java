/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: PropertyManager.java
 */

package edu.ucdavis.myinfovault;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Logger;

import edu.ucdavis.myinfovault.data.QueryTool;

// SDP 2007-03-15
// May want to change these methods/variables from statics and have MIVConfig manage access
// to an instance of this, e.g. MIVConfig.getConfig().getPropertyManager()
// then clean up some of the existing MIVConfig getProperties(), getConstants() to handle
// all that from here. This class could also manage the "constants" that MIVConfig is doing now.

/**
 * Manage and provide access to the MIV configuration properties set in the mivconfig.properties file.
 * Servlets that are making config options and text strings available to JSPs will use this.
 * Sets of Properties are retrieved by using the Record Type Name and the Property Set Name.
 */
public class PropertyManager implements PropertyChangeListener
{
    private static Logger log = Logger.getLogger("MIV");

    private static Map<String,Map<String,Properties>> allProps = new HashMap<String,Map<String,Properties>>();

    private static PropertyManager instance = null;//new PropertyManager();

    private static Properties tableProperties = null;

    private static boolean listenerAdded = false;


    public PropertyManager()
    {
        log.finest("Creating a PropertyManager object");
        if (allProps == null) {
            allProps = new HashMap<String,Map<String,Properties>>();
        }

        if (tableProperties == null) {
            tableProperties = new Properties();
        }

        if (allProps.isEmpty())
        {
            Properties p = new Properties();
            HashMap<String,Properties> m = new HashMap<String,Properties>();
            // Empty property sets need to exist in the map *before* the defaults are read.
            // It's done this way so that "defaults" can be treated the same as any other set.
            // If a new category (like "labels") is added be sure to add the empty property
            // set with the new name here.
            m.put("config", p);
            m.put("labels", p);
            m.put("tooltips", p);
            m.put("classes", p);
            m.put("strings", p);
            m.put("formats", p);
            m.put("fragments", p);
            m.put("data", p);
            allProps.put("defaults", m);
            initProperties();

            // It's important to register ourself as a listener ONLY when allProps was empty
            System.out.println("PropertyManager adding itself as listener to MIVConfig :: ["+this+"]");
            // Need to protect against double-adding a change listener because
            // loadRecordTypeToTableAssociation() below calls getPropertySet(...)
            // which it SHOULDN'T DO because it is being called BY getPropertySet
            if (!listenerAdded) {
                MIVConfig.getConfig().addPropertyChangeListener(this);
                listenerAdded = true;
            }
            else {
                System.out.println("  The listener was already added.");
            }
        }
    }


    /**
     * Makes the property set that associates record types to table names available.
     * Each property is the record type, with the retrieved value being the table used for that record type.
     * @return a new copy of the table properties.
     */
    public static Properties getTableProperties()
    {
        if (instance == null || instance.isEmpty()) {
            instance = new PropertyManager();
        }
        assert tableProperties != null : "tableProperties is still null!";
        //return new Properties(tableProperties);
        return tableProperties;
    }


    /**
     * Return the Record Type to Formatter Association Properties
     * @return the formater properties.
     */
    public static Properties getFormatterProperties()
    {
        return MIVConfig.getConfig().getFormatterProperties();
    }


    /**
     * Provides a subset of the properties for a particular record type.
     * Supported subsets are<ul>
     * <li>config</li>
     * <li>labels</li>
     * <li>tooltips</li>
     * <li>classes</li>
     * <li>strings</li>
     * <li>formats</li>
     * <li>fragments</li>
     * <li>data</li></ul>
     * @param rectype Name of the record type
     * @param subset Name of the property subset to get.
     * @return A Java {@link java.util.Properties Properties} object containing
     *  the requested subset of properties for the given record type.
     */
    public static Properties getPropertySet(String rectype, String subset)
    {
        assert rectype != null && rectype.length() > 0 : "Record Type cannot be null or empty";
        if (rectype == null || rectype.length() < 1) {
            throw new IllegalArgumentException("Record Type cannot be null or empty");
        }

        String[] parms = { rectype, subset };
        log.entering("PropertyManager", "getPropertySet", parms);
        if (instance == null || instance.isEmpty()) {
            instance = new PropertyManager();
        }
        Map<String,Properties> pset = allProps.get(rectype);

        if (pset == null)
        {
            log.config("PropertyManager: LOADING PROPERTIES for "+rectype+"/"+subset);
            pset = loadProperties(rectype);
            log.fine("  Loaded: ["+pset+"]");
            allProps.put(rectype, pset);
        }
        else
        {
            log.config("PropertyManager: Returning ALREADY LOADED properties for "+rectype+"/"+subset);
        }
        Properties p = pset.get(subset);

        log.exiting("PropertyManager", "getPropertySet", p);
        return p;
    }

    /**
     * @return The property set for record type "default" and subset "strings"
     */
    public static Properties getDefaultProperties()
    {
        return getPropertySet("default", "strings");
    }


    boolean isEmpty()
    {
        return allProps.isEmpty();
    }


    /**
     * Drop all currently loaded and cached properties and start from scratch.
     */
    public static void reload()
    {
        if (instance == null || instance.isEmpty()) {
            instance = new PropertyManager();
        }

        if (allProps != null)
        {
            allProps.clear();
            allProps = null;
        }
        instance = null;
        instance = new PropertyManager();
    }


    private void initProperties()
    {
        //System.out.println("   allProps BEFORE the call ==>> "+allProps);
        log.fine("Init Properties, going to load defaults");
        Map<String,Properties> m = loadProperties("default");
        allProps.put("defaults", m);
        //System.out.println("   allProps AFTER the call ==>> "+allProps);

        // Now load the RecordType to TableName associations
        loadRecordTypeToTableAssociation();
    }


    private void loadRecordTypeToTableAssociation()
    {
        QueryTool qt = MIVConfig.getConfig().getQueryTool();
        String documentQuery = "SELECT DISTINCT RecordName FROM Section";

        List<Map<String,String>> recordNames = qt.getList(documentQuery, (String[])null);

        for (Map<String,String> recordName : recordNames)
        {
            String recType = recordName.get("recordname");
            int index = recType.lastIndexOf('-');
            recType = recType.substring(0, index);

            log.finer("initProperties: getting property set for recType ["+recType+"]");
            Properties configProperties = getPropertySet(recType, "config");

            String tableName = configProperties.getProperty("tablename");
            if (tableName == null) tableName = "";

            //log.finer("tableProperties is ["+tableProperties+"]");
            //log.finer("recType is ["+recType+"]");
            //log.finer("tableName is ["+tableName+"]");

            tableProperties.setProperty(recType, tableName);

            //log.finer("returned from [tableProperties.put(recType, tableName);]");
        }
    }


    /**
     * Fill in the properties objects for the record type passed in with values from the MIVConfig properties
     * that match the provided record type.
     * The properties that are loaded are of the form<pre>
     *   &lt;recordType&gt;-label-&lt;field_name&gt;
     *   &lt;recordType&gt;-tooltip-&lt;field_name&gt;
     *   &lt;recordType&gt;-class-&lt;field_name&gt;
     *   &lt;recordType&gt;-string-&lt;string_name&gt;
     *   &lt;recordType&gt;-config-&lt;item_name&gt;
     *</pre>
     * @param recordType
     */
    private static Map<String,Properties> loadProperties(String recordType)
    {
        log.fine("  PropertyManager - Loading properties for \""+recordType+"\"");
        Map<String,Properties> m = new HashMap<String,Properties>();
        Properties source = MIVConfig.getConfig().getProperties();

        String configPrefix = recordType+"-config-";
        String labelPrefix = recordType+"-label-";
        String tooltipPrefix = recordType+"-tooltip-";
        String classPrefix = recordType+"-class-";
        String stringsPrefix = recordType+"-string-";
        String formatPrefix = recordType+"-format-";
        String fragmentPrefix = recordType+"-fragment-";
        String dataPrefix = recordType+"-data-";

        String sectionPrefix = recordType+"-sectionprefix";
        String subsectionPrefix = "Not Yet Set";

        Properties defaults;
        //System.out.println("allProps: "+allProps);

        Properties configProps = new Properties();
        defaults = allProps.get("defaults").get("config");
        configProps.putAll(defaults);

        Properties labelProps = new Properties();
        defaults = allProps.get("defaults").get("labels");
        labelProps.putAll(defaults);

        Properties tooltipProps = new Properties();
        defaults = allProps.get("defaults").get("tooltips");
        tooltipProps.putAll(defaults);

        Properties stringsProps = new Properties();
        defaults = allProps.get("defaults").get("strings");
        stringsProps.putAll(defaults);

        Properties classProps = new Properties();
        defaults = allProps.get("defaults").get("classes");
        classProps.putAll(defaults);

        Properties formatProps = new Properties();
        defaults = allProps.get("defaults").get("formats");
        formatProps.putAll(defaults);

        Properties fragmentProps = new Properties();
        defaults = allProps.get("defaults").get("fragments");
        fragmentProps.putAll(defaults);

        Properties dataProps = new Properties();
        defaults = allProps.get("defaults").get("data");
        dataProps.putAll(defaults);

        Enumeration<?> names = source.propertyNames();
        String key, value;
        while (names.hasMoreElements())
        {
            String name = (String) names.nextElement();
            if (name.startsWith(sectionPrefix))
            {
                value = source.getProperty(name);
                log.finer(" #***# Got subsection property "+name+" with value \""+value+"\"");
                subsectionPrefix = value;
                continue;
            }

// Moved to below. See the comment there.
//            if (name.startsWith(subsectionPrefix))
//            {
//                value = source.getProperty(name);
//                log.finest("  #***# Putting Value:"+value+"  for "+name+" into STRINGS (*)");
//                stringsProps.put(name, value);
//                continue;
//            }

            if (!name.startsWith(recordType)) {
                // If this property name doesn't start with the record type
                // we're currently loading, skip to the next property name.
                continue;
            }

            if (name.startsWith(configPrefix))
            {
                key = name.replace(configPrefix, "");
                value = source.getProperty(name);
                log.finer("   Putting Value:"+value+"  for "+name+" into CONFIG");
                configProps.setProperty(key, value);
            }
            else if (name.startsWith(labelPrefix))
            {
                key = name.replace(labelPrefix, "");
                value = source.getProperty(name);
                log.finer("   Putting Value:"+value+"  for "+name+" into LABELS");
                labelProps.setProperty(key, value);
            }
            else if (name.startsWith(tooltipPrefix))
            {
                key = name.replace(tooltipPrefix, "");
                value = source.getProperty(name);
                log.finer("   Putting Value:"+value+"  for "+name+" into TOOLTIPS");
                tooltipProps.setProperty(key, value);
            }
            else if (name.startsWith(stringsPrefix))
            {
                key = name.replace(stringsPrefix, "");
                value = source.getProperty(name);
                log.finer("   Putting Value:"+value+"  for "+name+" into STRINGS");
                stringsProps.setProperty(key, value);
            }
            else if (name.startsWith(classPrefix))
            {
                key = name.replace(classPrefix, "");
                value = source.getProperty(name);
                log.finer("   Putting Value:"+value+"  for "+name+" into CLASS");
                classProps.setProperty(key, value);
            }
            else if (name.startsWith(formatPrefix))
            {
                key = name.replace(formatPrefix, "");
                value = source.getProperty(name);
                log.finer("   Putting Value:"+value+"  for "+name+" into FORMATS");
                formatProps.setProperty(key, value);
            }
            else if (name.startsWith(fragmentPrefix))
            {
                key = name.replace(fragmentPrefix, "");
                value = source.getProperty(name);
                log.finer("   Putting Value:"+value+"  for "+name+" into FRAGMEMTS");
                fragmentProps.setProperty(key, value);
            }
            else if (name.startsWith(dataPrefix))
            {
                key = name.replace(dataPrefix, "");
                value = source.getProperty(name);
                log.finer("   Putting Value:"+value+"  for "+name+" into DATA");
                dataProps.setProperty(key, value);
            }
            // We finally test for the subsection prefix here, AFTER all the more specific prefix types,
            // because if the subsection prefix happens to match the record type we'd pick up everything.
            // For example: recordType="service" subsectionPrefix="service"
            // then "service-string-service-board-sectionhead" would be matched by the subsection prefix
            // and be put into the "strings" without first removing the "service-string-" prefix.
            // Many subsection prefixes are plural so do NOT match the record type, for example the
            // journal-records where recordType="journal" uses a subsectionPrefix="journals" so there's
            // no danger of premature matching.
            else if (name.startsWith(subsectionPrefix))
            {
                value = source.getProperty(name);
                log.finer("  #***# Putting Value:"+value+"  for "+name+" into STRINGS (*)");
                stringsProps.put(name, value);
                continue;
            }
            else
            {
                // This property isn't from a known subset, so don't add it to any of our sets
                log.finest("   Skipping Value for "+name+" because it doesn't match any known prefix");
            }
        }//while

        m.put("config",   configProps);
        m.put("labels",   labelProps);
        m.put("tooltips", tooltipProps);
        m.put("strings",  stringsProps);
        m.put("classes",  classProps);
        m.put("formats",  formatProps);
        m.put("fragments",  fragmentProps);
        m.put("data",  dataProps);


        log.fine("  PropertyManager - Done Loading \""+recordType+"\" properties");
        return m;
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt)
    {
        // Testing initially - just report that we got the message
        System.out.println("PropertyManager received a PropertyChangeEvent!");
        System.out.println("  Raw event: ["+evt.toString()+"]");
        System.out.println("\tSource was: " + evt.getSource().getClass().getSimpleName() + " :: " + evt.getSource());
        System.out.println("\tProperty: " + evt.getPropertyName());
        System.out.println("\tOld Value: " + evt.getOldValue());
        System.out.println("\tNew Value: " + evt.getNewValue());

        // Real implementation - call our reload() method (which can be changed to an instance method)
        // so MIVConfig will no longer have to call "PropertyManager.reload()" at line 990
        reload();
    }
}
