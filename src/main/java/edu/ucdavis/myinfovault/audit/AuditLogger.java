/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: AuditLogger.java
 */

package edu.ucdavis.myinfovault.audit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import com.google.common.eventbus.DeadEvent;
import com.google.common.eventbus.Subscribe;

import edu.ucdavis.mw.myinfovault.events.DCEvent;
import edu.ucdavis.mw.myinfovault.events.DelegationAuthorityChangeEvent;
import edu.ucdavis.mw.myinfovault.events.DossierEvent;
import edu.ucdavis.mw.myinfovault.events.DossierRouteEvent;
import edu.ucdavis.mw.myinfovault.events.EventDispatcher;

/**<p>
 * Not really for auditing!</p><p>
 * This is just an experimental central event listener. It may later be converted to use
 * the {@link edu.ucdavis.mw.myinfovault.service.audit.AuditServiceImpl AuditService} and
 * really do auditing, or may become the basis for the user notifications.</p>
 *
 * @author Stephen Paulsen
 *
 */
public class AuditLogger
{
    private static final Logger logger = LoggerFactory.getLogger(AuditLogger.class);
    // SLF4J "Markers" are not supported by Log4j, but are supported by "logback" (http://logback.qos.ch/)
    private static final Marker auditMarker = MarkerFactory.getMarker("|| -----*-----> ");


    public AuditLogger()
    {
        logger.info(auditMarker, " || --------------> AuditLogger being created and registering with the EventDispatcher");
        EventDispatcher.getDispatcher().register(this);
        AuditUtil.getInstance();
    }


    @Subscribe
    public void ReportDossierEvent(DossierEvent event)
    {
        System.out.println();
        System.out.print(" || --------------> ");

        if (event instanceof DossierRouteEvent) {
            DossierRouteEvent dre = (DossierRouteEvent) event;
            logger.info(" The AuditLogger just noticed that User {} routed dossier #{} from {} to {}",
                 new Object[] { event.getActorId(), event.getDossierId(), dre.getFromLocation(), dre.getNewLocation() });
        }
        else {
            logger.info("Tracking a Dossier Event: User {} affected Dossier #{}", event.getActorId(), event.getDossierId());
            logger.info(event.toString());
        }

        System.out.println();
    }


    @Subscribe
    public void DisclosureEventHandler(DCEvent dcEvent)
    {
        logger.info("Got a DCEvent of type {} -- contents: ({})", dcEvent.getClass().getSimpleName(), dcEvent.toString());
    }

    @Subscribe
    public void RafEventHandler(DelegationAuthorityChangeEvent daChangeEvent)
    {
        logger.info("Got a DelegationAuthorityChangeEvent of type {} -- contents: ({})", daChangeEvent.getClass().getSimpleName(), daChangeEvent.toString());
    }

    @Subscribe
    public void ReportOtherEvents(DeadEvent event)
    {
        logger.info("Got another kind of event: {}", event.getEvent());
    }
}
