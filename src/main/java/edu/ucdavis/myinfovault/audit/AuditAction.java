/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: AuditAction.java
 */

package edu.ucdavis.myinfovault.audit;


/**
 * Represents the Audit Actions.
 *
 * @author Pradeep K Haldiya
 * @since MIV 4.0
 * Note: enum AuditAction should match with ActionType Table
 */
public enum AuditAction
{
    AUDIT               (100, "Audit"),
    CREATE              (101, "Create"),
    EDIT                (102, "Edit"),
    DELETE              (103, "Delete"),
    ROUTINES            (104, "Routines"),
    SIGNATURE_REQUEST   (105, "Signature Request");

    private int actionId;
    private String description;


    private AuditAction(int actionId, String description)
    {
        this.actionId = actionId;
        this.description = description;
    }


    /**
     * @return the actionId
     */
    public int getActionId()
    {
        return actionId;
    }


    /**
     * @return the description
     */
    public String getDescription()
    {
        return description;
    }


    /**
     * Returns the enum corresponding to the auditId
     * @param auditId
     * @return AuditAction enum
     */
    public static AuditAction getAuditActionById(int auditId)
    {

        AuditAction[] enumlist = AuditAction.values();
        for (AuditAction action : enumlist)
        {
            if (action.getActionId() == auditId) { return action; }
        }

        return AuditAction.AUDIT;
    }
}
