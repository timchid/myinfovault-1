/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: AuditUtil.java
 */

package edu.ucdavis.myinfovault.audit;



/**
 * FIXME: Missing Javadoc for this class.
 * @author Pradeep K Haldiya
 * @since MIV 4.0
 */
public class AuditUtil
{
    private static AuditUtil auditUtil = new AuditUtil();

    public static AuditUtil getInstance()
    {
        if (auditUtil == null) {
            auditUtil = new AuditUtil();
        }

        return auditUtil;
    }
}
