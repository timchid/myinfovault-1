package edu.ucdavis.myinfovault.htmlcleaner.wysiwyg;

import java.util.Arrays;
import java.util.List;

import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

import edu.ucdavis.myinfovault.htmlcleaner.NodeRenderer;

/**
 * Render a tag along with specified attributes.
 *
 * @author Stephen Paulsen
 * @since MIV 2.0
 */
public class TagAttributeRenderer extends NodeRenderer
{
    private String tagName;
    private List<String> attributeList;


    public TagAttributeRenderer(Node n, String...attributes)
    {
        super(n);
        this.tagName = n.getNodeName();
        this.attributeList = Arrays.asList(attributes);
    }


    public String toString()
    {
        String startTag = makeStartTag();
        String endTag = makeEndTag();
        return startTag + processChildren() + endTag;
    }


    protected String makeStartTag()
    {
        StringBuilder s = new StringBuilder();

        // Start the open tag, but don't close it until attributes have been processed.
        s.append("<").append(this.getTagName());

        if (this.node.hasAttributes())
        {
            NamedNodeMap attrMap = this.node.getAttributes();
            int attrCount = attrMap.getLength();
            for (int i=0; i<attrCount; i++)
            {
                Node attr = attrMap.item(i);
                String attrName = attr.getNodeName();
                        @SuppressWarnings("unused")
                        String attrValue = attr.getNodeValue(); // not used, we want to see it in debugging
                boolean keepAttr = attributeList.isEmpty() || attributeList.contains(attrName);
                if (keepAttr) {
                    s.append(" ").append(attr.toString());
                }
            }
        }

        s.append(">");

        return s.toString();
    }


    protected String makeEndTag()
    {
        return "</" + this.getTagName() + ">";
    }


    String getTagName()
    {
        return this.tagName;
    }
}
