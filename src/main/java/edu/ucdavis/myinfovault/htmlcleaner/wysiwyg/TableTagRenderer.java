package edu.ucdavis.myinfovault.htmlcleaner.wysiwyg;

import org.w3c.dom.Node;

public class TableTagRenderer extends TagAttributeRenderer
{
    public TableTagRenderer(Node n)
    {
        super(n, "width", "border");
    }
}
