package edu.ucdavis.myinfovault.htmlcleaner.wysiwyg;

import org.w3c.dom.Node;

public class ThTagRenderer extends TagAttributeRenderer
{
    public ThTagRenderer(Node n)
    {
        super(n, "width", "valign");
    }
}
