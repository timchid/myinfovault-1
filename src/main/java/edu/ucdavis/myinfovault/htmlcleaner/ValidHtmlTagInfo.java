package edu.ucdavis.myinfovault.htmlcleaner;
public class ValidHtmlTagInfo extends TagInfoRecord{
    private String deptName;
    private String fName;
    private String lName;
    private String school;
    public ValidHtmlTagInfo(String dept,String fName, String lName,String school, String tableName, String recordID, String userID, String fieldName, String tagName, String contents)
    {       
        super(tableName,recordID,userID, fieldName, tagName,contents);  
        this.deptName = dept;
        this.fName = fName;
        this.lName = lName;
        this.school = school;
    }
    public String getDeptName()
    {
        return this.deptName;
    }

    public String getFName()
    {
        return this.fName;
    }
    public String getlName()
    {
        return this.lName;
    }

    public void setDeptName(String deptName)
    {
        this.deptName = deptName;
    }

    public void setFName(String fName)
    {
        this.fName = fName;
    }
    public void setLName(String lName)
    {
        this.lName = lName;
    }
    public String getSchool()
    {
        return this.school;
    }

    public void setSchool(String school)
    {
        this.school = school;
    }
    
}
