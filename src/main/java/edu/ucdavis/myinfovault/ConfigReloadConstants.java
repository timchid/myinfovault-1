/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: ConfigReloadConstants.java
 */

package edu.ucdavis.myinfovault;

/**
 * Config constants to reload MIVConfig
 * @author pradeeph
 * @since MIV 4.4
 */
public enum ConfigReloadConstants
{
    ALL                         ("All"),
    PROPERTIES                  ("Properties"),
    FORMATTER_PROPERTIES        ("FormatterProperties"),
    CONSTANTS                   ("Constants"),
    FORMAT_FIELDS               ("FormatFields"),
//    BUILD_MAPS                  ("buildMaps"),
    USER_SERVICE                ("UserService"),
    GROUP_SERVICE               ("GroupService");

    private String description;

    private ConfigReloadConstants(String description)
    {
        this.description = description;
    }

    /**
     * @return the description
     */
    public String getDescription()
    {
        return description;
    }
}
