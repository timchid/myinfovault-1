package edu.ucdavis.myinfovault.format;

import java.util.Map;

/**
 * @author Binhtri Huynh
 * @since MIV 2.0
 */
public class UniversityExtFormatter extends FormatAdapter implements RecordFormatter
{
    @Override
    public Map<String, String> format(Map<String, String> m)
    {
        this.applySubformats(m);

        StringBuilder preview = new StringBuilder();

        preview
            .append( insertValue("coursenumber", m.get("coursenumber"), "", COMMA_SPACE))
            .append( insertValue("title", m.get("title")))
            .append( insertValue("units", m.get("units"), COMMA_SPACE, " Units"))
            .append( insertValue("duration", m.get("duration"), COMMA_SPACE, ""))
            .append( insertValue("locationdescription", m.get("locationdescription"), COMMA_SPACE, ""))
            ;
        m.put("preview", preview.toString());
        m.put("sequencepreview", preview.toString());

        m.put("year", insertValue("year", m.get("year")));

        return m;
    }
}
