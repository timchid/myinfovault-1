package edu.ucdavis.myinfovault.format;

import java.util.Map;

/**
 * Removes HTML/XML style tags, enclosed in <code>&lt;&hellip;&gt;</code>, from a field or fields.
 *
 * @author Brij Garg
 * @since MIV 2.0
 */
public class EvaluationTagCleaner extends TagCleaner implements RecordFormatter
{
    public EvaluationTagCleaner()
    {
        //DO NOTHING
    }

    public EvaluationTagCleaner(String... fields)
    {
        super(fields);
    }

    @Override
    public Map<String, String> format(Map<String, String> m)
    {
        for (String field : fieldList)
        {
            String oldValue = m.get(field);
            int linkIndex = oldValue.indexOf(URLFormatter.EVALUATIONLINKLABEL);
            if (linkIndex > 0)
            {
                String newValue = oldValue.replaceAll(URLFormatter.EVALUATIONLINKLABEL, "");
                m.put(field, newValue);
            }
        }

        return super.format(m);
    }
}
