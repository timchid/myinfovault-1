package edu.ucdavis.myinfovault.format;

import java.util.Arrays;
import java.util.Collection;
import java.util.Map;
import java.util.logging.Logger;

/**<p>
 * This formatter combines the handling of Volume and Issue formats.
 * The valid formats, where V is the volume and I is the issue, are:<pre>
 *   "V(I):"  - both volume and issue are provided
 *   "V:"     - only a volume is provided
 *   "I:"     - only an issue is provided
 *   ""       - neither volume nor issue is provided</pre>
 * This is necessary to supress the trailing colon when neither is present.</p>
 * <p>TODO: Confirm the format of an Issue alone.
 * In the old MIV system it (sometimes?) showed up as "(I):"</p>
 * <p>This formatter applies sub-formats <em>before</em> its own formatting.</p>
 *
 * @author Stephen Paulsen
 * @since MIV 2.0
 */
public class VolumeIssueFormatter extends FormatAdapter implements RecordFormatter
{
    private static Logger log = Logger.getLogger("MIV");
    private static String altkey = "VolumeIssue";
//    private static String key = "VolumeIssue";
//    private static String[] keys = { altkey, key };
    private static final Collection<String> addedFields = Arrays.asList(new String[] {altkey});


    /**
     * @return The original Map with the new key "VolumeIssue" added.
     */
    @Override
    public Map<String, String> format(Map<String, String> m)
    {
        String volume, issue, combined;

        applySubformats(m);

        volume = m.get("volume");
        issue = m.get("issue");
        boolean hasVolume = volume != null && volume.length() > 0;
        boolean hasIssue  = issue  != null && issue.length() > 0;

        if (hasVolume && hasIssue) {
            //combined = volume + "(" + issue + "):";
            combined = volume + OPEN_PARENTHESIS + issue + CLOSE_PARENTHESIS + COLON;
        }
        else if (hasVolume) {
            //combined = volume + ":";
            combined = volume + COLON;
        }
        else if (hasIssue) {
            //combined = issue + ":";
            combined = OPEN_PARENTHESIS + issue + CLOSE_PARENTHESIS;
        }
        else {  // neither volume nor issue available
            combined = "";
        }

        log.finest("Added combined Volume/Issue string \""+combined+"\" under the key VolumeIssue");
        m.put(altkey, combined);

        return m;
    }

    @Override
    public Iterable<String> getAddedFields()
    {
        return addedFields;
    }
}
