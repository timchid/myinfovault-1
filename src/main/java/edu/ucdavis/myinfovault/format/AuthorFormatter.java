package edu.ucdavis.myinfovault.format;

import java.util.Map;
import java.util.logging.Logger;

/**<p>
 * Format the Authors name string for a publication citation preview.
 * Both the "authors" and the combined "reference" fields are checked.
 * The required trailing punctuation is added if necessary.</p>
 * <p>The original "author" and "reference" are left in the map, and
 * new keys "Author" and "Reference" are added with the formatted
 * version of the data.</p>
 *
 * @author Stephen Paulsen
 * @since MIV 2.0
 */
public class AuthorFormatter extends FormatAdapter implements RecordFormatter
{
    private static Logger log = Logger.getLogger("MIV");
    private static final String[] defaultFields = { "author", "citation", "authors", "reference","creator","creators" };
    private String[] customFields = null;
    private String punctuationChar = PERIOD;


    /**
     * Create an AuthorFormatter to act on the default author fields,
     * using the indicated punctuation string to terminate the fields.
     * @param punctuationChar
     */
    public AuthorFormatter(String punctuationChar)
    {
        this.punctuationChar = punctuationChar;
    }


    /**
     * Create an AuthorFormatter to act on the provided fields,
     * using the indicated punctuation string to terminate the fields.
     * @param punctuationChar
     * @param fields One or more field names to treat as an author name field.
     */
    public AuthorFormatter(String punctuationChar, String... fields)
    {
        customFields = fields;
        this.punctuationChar = punctuationChar;
    }


    /**
     * The AuthorFormatter does not use, and throws away, subformatters.
     */
    @Override
    public RecordFormatter add(RecordFormatter formatter)
    {
        return this;
    }


    /**
     * FIXME: this javadoc is out of date.
     * Format the authors name strings by enclosing the user's name within an
     * HTML <code>&lt;span class="author"&gt;&lt;/span&gt;</code> tag.
     * @return The provided Map, with the "authors" and/or "reference" fields modified.
     */
    @Override
    public Map<String, String> format(Map<String, String> m)
    {
        log.entering(this.getClass().getSimpleName(), "format");
        String orig, current="";

        String[] fields = customFields == null ? defaultFields : customFields;


        for (String fieldname : fields)
        {
            orig = m.get(fieldname);
            if (orig != null && orig.length() > 0)
            {
                current = orig;

                // Handle Punctuation

                // Get just the name part of the string, w/o any <span> tags.
                String namePart = orig;
                while (namePart.matches(".*<span[^>]*>[^<]*</span>.*"))
                {
                    namePart = namePart.replaceAll("<span[^>]*>([^<]*)</span>", "$1");
                }

                // Add the span tag surrounding the complete authors field.
                current = "<span class=\"author\">" + current + "</span>";

                // Then add the punctuation if needed --
                //   don't add a period if there's a dot-dot-dot or CapitalLetter-period
                if ( !punctuationChar.equals(".") ||
                     ( !namePart.matches(".*\\.\\.\\.$") && !namePart.matches(".*[A-Z]\\.$") )
                   )
                {
                    current += punctuationChar;
                }

                m.put(fieldname, current);
            }
        }

        log.exiting(this.getClass().getSimpleName(), "format");
        return m;
    }
}
