package edu.ucdavis.myinfovault.format.wysiwyg;

import org.w3c.dom.Node;

import edu.ucdavis.myinfovault.htmlcleaner.NodeRenderer;

public class ITagRenderer extends NodeRenderer
{
    public ITagRenderer(Node n)
    {
        super(n);
    }

    public String toString()
    {
        return "<em>" + processChildren() + "</em>";
    }
}
