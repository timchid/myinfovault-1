package edu.ucdavis.myinfovault.format.wysiwyg;

import org.w3c.dom.Node;

import edu.ucdavis.myinfovault.htmlcleaner.wysiwyg.SimpleTagRenderer;

public class SubTagRenderer extends SimpleTagRenderer
{
    public SubTagRenderer(Node n)
    {
        super(n);
    }
}
