package edu.ucdavis.myinfovault.format.wysiwyg;

import org.w3c.dom.Node;

import edu.ucdavis.myinfovault.htmlcleaner.wysiwyg.TagAttributeRenderer;

public class ThTagRenderer extends TagAttributeRenderer
{
    public ThTagRenderer(Node n)
    {
        super(n, "width", "valign");
    }
}
