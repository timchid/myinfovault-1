package edu.ucdavis.myinfovault.format.wysiwyg;

import org.w3c.dom.Node;

import edu.ucdavis.myinfovault.htmlcleaner.wysiwyg.SimpleTagRenderer;

public class UlTagRenderer extends SimpleTagRenderer
{
    public UlTagRenderer(Node n)
    {
        super(n);
    }
}
