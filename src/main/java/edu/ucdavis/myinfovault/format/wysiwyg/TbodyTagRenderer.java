package edu.ucdavis.myinfovault.format.wysiwyg;

import org.w3c.dom.Node;

import edu.ucdavis.myinfovault.htmlcleaner.wysiwyg.SimpleTagRenderer;

public class TbodyTagRenderer extends SimpleTagRenderer
{
    public TbodyTagRenderer(Node n)
    {
        super(n);
    }
}
