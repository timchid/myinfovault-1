/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: FormatAdapter.java
 */

package edu.ucdavis.myinfovault.format;

import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.StringTokenizer;

import edu.ucdavis.mw.myinfovault.util.HtmlUtil;
import edu.ucdavis.myinfovault.MIVConfig;
import edu.ucdavis.myinfovault.PropertyManager;

/**<p>
 * An abstract adapter class for creating formatters.
 * Extend this class to create a <code>RecordFormatter</code> and provide an implementation
 * of the {@link RecordFormatter#format(Map) format} method.
 * Override the {@link RecordFormatter#getAddedFields() getAddedFields} method as necessary.</p>
 * <p>This class provides several conveniences that a formatter may want, including an implementation
 * of the {@link RecordFormatter#add(RecordFormatter) add} interface method to add sub-formatters and
 * a simple iteration to apply the sub-formatters.</p>
 * <p>Other methods include testing for empty fields and getting a "field is missing" message.</p>
 */
public abstract class FormatAdapter implements RecordFormatter
{
    protected static final String ERROR_CSS_CLASS = "ERR_MISSING";
    protected static final String FIELD_MISSING_PREFIX = "<span class=\""+ERROR_CSS_CLASS+"\" title=\"This required field is missing\">";
    protected static final String FIELD_MISSING_SUFFIX = "</span>";
    protected static final String FIELD_MISSING = " MISSING";
    protected static final String PUB_GROUP_JOURNALS = "journals";
    protected static final String PUB_GROUP_BOOKS = "books";
    protected static final String PUB_GROUP_PRESENTATIONS = "presentations";

    private static String punctuation = ".,?!\"\'"; // default, if not found in mivconfig.properties
    private static String punctPattern =
        //"\\A.*\\p{Punct}\\z";                 // should the default be all POSIX punctuation...
        "\\A.*[" + punctuation + "]\\z";        // or just our small set?

    private String recType = "";
    private Set<String> requiredFields = null;
    private Set<String> pseudoRequired = null;
    protected List<RecordFormatter> subformatters = new LinkedList<RecordFormatter>();

    static {
        String p = MIVConfig.getConfig().getProperty("xmlbuilder-config-punctuationvalues");
        if (p != null) {
            punctuation = p;
            punctPattern = getPunctuationPattern(punctuation);
        }
    }

    /**
     * To create punctuation pattern
     * @param punctuation : string to get the punctuation values like <b>!?,.</b>
     * @return
     */
    private static String getPunctuationPattern(String punctuation)
    {
        return "\\A.*[" + punctuation + "]\\z";
    }

    /**
     * @return String containing punctuation characters
     */
    public static String getPunctuations()
    {
        return punctuation;
    }

    @Override
    public RecordFormatter add(RecordFormatter formatter)
    {
        subformatters.add(formatter);
        return this;
    }


    @Override
    public Map<String, String> format(Map<String,String> m)
    {
        return this.applySubformats(m);
    }


    public Map<String, String> applySubformats(Map<String, String> m)
    {
        for (RecordFormatter f : subformatters)
        {
            m = f.format(m);
        }
        return m;
    }


    /**
     * Determine whether the provided string ends with punctuation.
     * Characters considered to be punctuation are defined by the property
     * <b>xmlbuilder-config-punctuationvalues</b> in <em>mivconfig.properties</em>
     * The value passed is stripped of any HTML tags and then trimmed before
     * being tested for punctuation.
     * @param field  String or other CharSequence to check for punctuation
     * @return true if the provided text ends with a punctuation character.
     */
    protected boolean hasPunctuation(CharSequence field)
    {
        field = HtmlUtil.removeHTMLTags(field.toString().trim());
        return field.toString().matches(punctPattern);
    }


    /**
     * Determine whether the provided string ends with punctuation.
     * Characters considered to be punctuation
     * @param field  string to check for punctuation
     * @param punctuationvalues  string to get the punctuation values like <b>!?,.</b>
     * @return true if the provided string ends with a punctuation character.
     */
    protected boolean hasPunctuation(CharSequence field, String punctuationvalues)
    {
        field = HtmlUtil.removeHTMLTags(field.toString().trim());
        return field.toString().matches(getPunctuationPattern(punctuationvalues));
    }


    @Override
    @SuppressWarnings({"unchecked"})
    public Iterable<String> getAddedFields()
    {
        return Collections.EMPTY_LIST;
    }


    @Override
    public void addRequiredField(String name)
    {
        if (this.pseudoRequired == null) this.pseudoRequired = new HashSet<String>();
        this.pseudoRequired.add(name);
    }


    static String missing(String label)
    {
        if (label != null) label = label.replaceAll(":", "");
        return FIELD_MISSING_PREFIX + label +  FIELD_MISSING + FIELD_MISSING_SUFFIX;
    }


    String missingField(String field)
    {
        Properties labels = PropertyManager.getPropertySet(getRecType(), "labels");
        String label = labels.getProperty(field);
        if (label != null) {
            label = label.replaceAll(":", "");
        }
        else {
            label = field;
        }

        return FIELD_MISSING_PREFIX + label +  FIELD_MISSING + FIELD_MISSING_SUFFIX;
    }


    /**
     * Tests whether the given String is null or empty.
     * @param s a String to test
     * @return <tt>true</tt> if the String is <code>null</code> or has a 0 length.
     */
    boolean isEmpty(String s)
    {
        return s == null || s.length() <= 0;
    }


    /**
     * Tests whether the given String is null, empty, or blank.
     * @param s a String to test
     * @return <tt>true</tt> if the String is <code>null</code>, has a 0 length, or contains only whitespace.
     */
    boolean isEmptyOrBlank(String s)
    {
        return s == null || s.trim().length() <= 0;
    }


    boolean hasText(String s)
    {
        return !isEmptyOrBlank(s);
    }


    protected String insertValue(String fieldKey, String fieldValue)
    {
        return this.insertValue(fieldKey, fieldValue, "", "", false);
    }


    /**
     * prepare formatted value based on prefix and suffix values
     * @param fieldKey
     * @param fieldValue
     * @param prefix - to add element or tag before the fieldValue
     * @param suffix - to add element or tag after the fieldValue
     */
    protected String insertValue(String fieldKey, String fieldValue, String prefix, String suffix)
    {
        return insertValue(fieldKey, fieldValue, prefix, suffix, false);
    }

// I don't like the addition of the "isMandatory" flag. As we found in 2005-2006, formatting can be complex
// and a simple "skip the punctuation if the field is empty" is rarely sufficient.
// Look at the PeriodicalFormatter and how it has to go back and modify earlier punctuation in some cases.
// This is also why there are sub-formatters. A sub-formatter can be made responsible for a complex
// relationship among several fields, and then make the resulting combination available in the map
// as a pseudo-field.  This is what the VolumeIssue formatter does, and it could probably be expanded
// to do Volume-Issue-Pages.
// It is appropriate for the formatter to be as complex as necessary to implement the formatting rules; this
// addition seems like an over-simplification that is going to miss complex cases of how to punctuate a
// record while adjusting for a series of present-and-empty fields.
    /**
     * Prepare formatted value based on prefix and suffix values.
     *
     * @param fieldKey
     * @param fieldValue
     * @param prefix to add element or tag before the fieldValue
     * @param suffix to add element or tag after the fieldValue
     * @param isMandatory if true then always attach prefix and suffix for field, otherwise only use them if fieldValue has contents.
     * @return formatted value
     */
    protected String insertValue(String fieldKey, String fieldValue, String prefix, String suffix, boolean isMandatory)
    {
        StringBuilder value = new StringBuilder();

        if (prefix == null) prefix = "";
        if (suffix == null) suffix = "";

        if (!isEmptyOrBlank(this.recType) &&            // if the rectype has been set (is not blank)
            isEmptyOrBlank(fieldValue) &&               //  AND this field *is* blank
            isRequired(fieldKey))                       //  AND this field is required
        {
                Properties labels = PropertyManager.getPropertySet(getRecType(), "labels");
                String fieldLabel = labels.getProperty(fieldKey);
                if (fieldLabel == null) fieldLabel = "Required Field";
                value.append(prefix).append(missing(fieldLabel)).append(suffix);
        }
        else if (!isEmptyOrBlank(fieldValue))
        {
            value.append(prefix).append(fieldValue).append(suffix);
        }
        else if (isMandatory)
        {
            value.append(prefix).append(isEmptyOrBlank(fieldValue) ? "" : fieldValue).append(suffix);
        }

        return value.toString();
    }


    /**
     * Check if a given field is required
     * @param field name of the field to check
     * @return true if the field is required
     */
    protected boolean isRequired(String field)
    {
        boolean required = false;
        // See if this field is on the required-fields list
        if (this.requiredFields != null)
        {
            required = requiredFields.contains(field);
        }
        // This field not required yet, are the pseudo-requirements and is the field listed in them?
        if (!required && this.pseudoRequired != null)
        {
            required = pseudoRequired.contains(field);
        }
        return required;
    }


    @Override
    public void setRecType(String rectype)
    {
        this.recType = rectype;
        Properties p = PropertyManager.getPropertySet(getRecType(), "config");
        this.requiredFields = new HashSet<String>();
        String reqFields = p.getProperty("required");
        if (reqFields != null && !("".equals(reqFields)))
        {
            StringTokenizer tokens = new StringTokenizer(p.getProperty("required"), ",");
            while (tokens.hasMoreTokens())
            {
                String token = tokens.nextToken();
                //Since the Required fields are defined within quotes ("reqfield") in the properties file, remove the quote
                //so that it can be compared correctly with the reqired fields.
                token = token.replaceAll("\"", "").trim();
                this.requiredFields.add(token);
            }
        }
    }


    public String getRecType()
    {
        return recType;
    }


    /**
     * To remove trailing comma from preview string and replace with period.
     *
     * @param preview
     * @return String with trailing commas removed
     */
    public String removeTrailingComma(String preview)
    {
        // FIXME: need documentation comment - why is this being trimmed?
        StringBuilder previewBuilder = new StringBuilder(preview/*.toString()*/.trim()); // don't .toString() a String

/*
 * This code was using the length and content of the 'preview' String parameter
 * but was modifying the 'previewBuilder' StringBuilder.  If the 'previewBuilder'
 * had been modified since being created from 'preview' then length could be different.
 * Because 'preview' is trimmed() when creating previewBuilder then previewBuilder
 * could often be shorter than preview. This caused a StringIndexOutOfBoundsException
 * because (preview.length-1) was longer than previewBuilder.length.
 * Performing all tests and operations on previewBuilder ensures that testing and
 * modification is done on the *current* state of the preview.
 */
      //char lastchar = preview.charAt(preview.length() - 1);
        char lastchar = previewBuilder.charAt(previewBuilder.length() - 1);
        if (lastchar == ',')
        {
          //previewBuilder.deleteCharAt(preview.length() - 1);
            previewBuilder.deleteCharAt(previewBuilder.length() - 1);
        }

// Original "if" line, followed by 3 iterations of simplifying it.
//      if (!hasPunctuation(HtmlUtil.removeHTMLTags(preview.toString())))

        // 1st: Don't test 'preview', test 'previewBuilder' which could be different by now, giving this line:
//      if (!hasPunctuation(HtmlUtil.removeHTMLTags(previewBuilder.toString())))

        // 2nd: 'hasPunctuation()' does a 'removeHTMLTags' -- no need to do it here first; shortening to:
//      if ( !hasPunctuation(previewBuilder.toString()) )

        // 3rd: 'hasPunctuation()' accepts a 'CharSequence' and a StringBuilder *is* a CharSequence, so don't need to do a .toString() here.
        if ( !hasPunctuation(previewBuilder) )
        {
            previewBuilder.append(PERIOD);
        }
        return previewBuilder.toString();
    }
}
