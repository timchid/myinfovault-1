package edu.ucdavis.myinfovault.format;

import java.util.List;
import java.util.Map;

import edu.ucdavis.myinfovault.MIVConfig;

/**
 * @author Stephen Paulsen
 * @since MIV 2.0
 */
public class GrantFormatter extends FormatAdapter implements RecordFormatter
{
    public GrantFormatter()
    {
        this.add( new SpanClassFormatter("granttype", "typefield") );
//        this.add( new GrantDateFormatter("startdate") )
//            .add( new GrantDateFormatter("enddate") )
//            ;
    }

    @Override
    public Map<String, String> format(Map<String, String> m)
    {
        //System.out.println("Grant FORMATTER :: running format()");

        // Fix the individual dates first
        this.applySubformats(m);

        StringBuilder preview = new StringBuilder();
        StringBuilder yearfield = new StringBuilder();
        String s, startDate, endDate;

        // Startdate, Enddate or Submitdate
        if ( ! isEmpty(startDate=m.get("startdate")) )
        {
            // put in the start date if not empty
            yearfield .append(startDate);

            // if there's an end date we need a dash, then linebreak if start date or end date is long
            if ( ! isEmptyOrBlank(endDate=m.get("enddate")))
            {
                yearfield .append("-");
                if (startDate.length() > 4 || endDate.length() > 4) {
                    yearfield .append("<br>");
                }
                yearfield .append(endDate);
            }
        }
        else // no startdate, use submitdate if present
        {
            String date;
            date = insertValue("startdate", m.get("submitdate"));
            yearfield .append( isEmpty(date) ? "" : date)
                    ;
        }
        m.put("year", yearfield.toString());

        preview .append( insertValue("granttype", m.get("granttype"),"",COLON + SINGLE_SPACE));

        // Grant number and amount
        preview .append( isEmpty(s=m.get("grantnumber")) ? "" : "Grant #" + s + COMMA_SPACE)
                .append( isEmpty(s=m.get("grantamount")) ? "" : "$" + s + COMMA_SPACE)
                ;

        // Role and Grant title -- This gets ugly.
        // The format for role and title includes the Principal Investigator if you're not the PI
        // - The format is:
        //   Your-Role, Grant-Title, [Principal-Name, '(Principal Investigator)']
        s = m.get("roleid");
        int roleid = Integer.parseInt(s)-1;     // Roles are 1-5 but we're indexing a 0-based array: 0-4
                                                // so Convert from 1-based to 0-based array

        Map<String,List<Map<String,String>>> constants = MIVConfig.getConfig().getConstants();

        //s = constants.get("grantrole").get(roleid).get("name");
        preview .append( insertValue("rolename", m.get("rolename")))
                .append( insertValue("title", m.get("granttitle"), COMMA_SPACE, ""))
                .append( roleid == 0 ? "" :     // remember, a 0 based array
                         isEmpty(s=m.get("principalinvestigator")) ? "" :
                             COMMA_SPACE + s + SINGLE_SPACE +
                             OPEN_PARENTHESIS +
                                 constants.get("grantrole").get(0).get("name") +
                             CLOSE_PARENTHESIS
                                )
                .append( insertValue("grantsource", m.get("grantsource"), COMMA_SPACE, ""))
                .append( insertValue("effort", m.get("effort"), ", Percentage Effort=", "%"))
                //.append( isEmpty(s=m.get("effort")) ? "" : " Percentage Effort="+s+".")
                ;

        m.put("preview", preview.toString());
        m.put("sequencepreview", preview.toString());
        return m;
    }
}
