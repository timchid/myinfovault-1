package edu.ucdavis.myinfovault.format;

import java.util.Map;

/**
 * Extending Knowledge - Meetings
 *
 * @author Stephen Paulsen
 * @since MIV 2.0
 */
public class EKMeetingFormatter extends FormatAdapter implements RecordFormatter
{
    @Override
    public Map<String, String> format(Map<String, String> m)
    {
        this.applySubformats(m);

        StringBuilder preview = new StringBuilder();
        StringBuilder resequencePreview = new StringBuilder();

        // Role (name) is from a drop-down and should always be present.
        preview .append( insertValue("title", m.get("title"), "<span class=\"prefix\">", "</span>, "));
        resequencePreview .append( insertValue("name", m.get("name")))
                .append( insertValue("audience", m.get("audience"), COMMA_SPACE, ""))
                .append( insertValue("location", m.get("location"), COMMA_SPACE, ""))
                .append( insertValue("datespan", m.get("datespan"), COMMA_SPACE, ""))
                .append( insertValue("headcount", m.get("headcount"), COMMA_SPACE, " Attendees"));

        //MIV-1372 checking if the "headcount" too has punctuation to make it consistent with the way the packet
        //puts the punctuation if this field has punctuation in it.
        if (!this.hasPunctuation(resequencePreview.toString()) && !this.hasPunctuation(m.get("headcount"))) {
            resequencePreview.append(PERIOD);
        }

        m.put("sequencepreview", resequencePreview.toString());
        m.put("preview", preview.append(resequencePreview).toString());

        return m;
    }
}
