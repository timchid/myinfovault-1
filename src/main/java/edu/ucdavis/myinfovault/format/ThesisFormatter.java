package edu.ucdavis.myinfovault.format;

import java.util.Map;

/**
 * @author Binhtri Huynh
 * @since MIV 2.0
 */
public class ThesisFormatter extends FormatAdapter implements RecordFormatter
{
    public ThesisFormatter(Iterable<String> names)
    {
        this.add( new ThesisSubFormatter() );
    }

    public ThesisFormatter()
    {
        this(null);
    }

    @Override
    public Map<String, String> format(Map<String, String> m)
    {
        this.applySubformats(m);

        //StringBuilder preview = new StringBuilder();
        StringBuilder resequencePreview = new StringBuilder();
        StringBuilder yearfield = new StringBuilder();
        String startYear,endYear;

        startYear = m.get("startyear");
        endYear = m.get("endyear");
        yearfield.append(insertValue("startyear", startYear));
        yearfield.append(DASH);
        yearfield.append(insertValue("endyear", endYear));
        m.put("year", yearfield.toString());

        //m.put("studentname", insertValue("studentname", m.get("studentname"), "", ""));
        
        //preview.append(m.get("studentname"));
        
        resequencePreview
            .append( insertValue("studentname", m.get("studentname"), SINGLE_SPACE, COMMA_SPACE))
            .append(insertValue(ThesisSubFormatter.COMMITTEEPOSITION, m.get(ThesisSubFormatter.COMMITTEEPOSITION)))
            .append(insertValue(ThesisSubFormatter.DEGREETYPEID, m.get(ThesisSubFormatter.DEGREETYPEID), COMMA_SPACE, ""))
            .append(insertValue(ThesisSubFormatter.DEGREESTATUSID, m.get(ThesisSubFormatter.DEGREESTATUSID), COMMA_SPACE, ""))
            .append(insertValue("position", m.get("position"), COMMA_SPACE, ""))
            ;
        m.put("sequencepreview", resequencePreview.toString());
        m.put("preview", resequencePreview.toString());

        return m;
    }
}
