/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: PublicationEventsFormatter.java
 */

package edu.ucdavis.myinfovault.format;

import java.util.Map;

/**
 * A formatter for PublicationEvents.
 * @author rhendric
 * @since MIV 4.4
 */
public class PublicationEventsFormatter extends PreviewBuilder implements RecordFormatter
{
    /**
     * A formatter for PublicationEvents.
     * @param names An Iterable list of names to highlight in the citation. An empty list is allowed, <code>null</code> is not.
     */
    public PublicationEventsFormatter(Iterable<String> names)
    {
        this.add( new TitleFormatter(PERIOD, PUB_GROUP_JOURNALS) )
            .add( new PublicationNameFormatter() )
            .add( new VolumeIssueFormatter() )
            .add( new CityProvinceCountryFormatter() )
            .add( new URLFormatter(URLFormatter.PreviewType.PUBLICATIONEVENTS).add(new QuoteCleaner("link")) )
            ;
    }

    public PublicationEventsFormatter()
    {
        this(null);
    }

    @Override
    protected String buildPreview(Map<String, String> m)
    {
        this.applySubformats(m);

        StringBuilder preview = new StringBuilder();
        StringBuilder subpreview = new StringBuilder();

        preview
            .append( insertValue("year", m.get("year"), "<div class=\"year\">", "</div>", true) );

        subpreview
            .append( insertValue("type", "Publication", SINGLE_SPACE + "<span class=\"typefield\">" , "</span>"+COLON + SINGLE_SPACE) )
            .append( insertValue("title", m.get("title")) )
            .append( insertValue("publication", m.get("publication"), SINGLE_SPACE, COMMA) )
            .append( insertValue("author", m.get("editors"), SINGLE_SPACE, COMMA_SPACE + "(ed)" + COMMA))
            .append( insertValue("volume", m.get("VolumeIssue"), SINGLE_SPACE, ""));

        if (!isEmpty(m.get("pages")))
        {
            subpreview.append(insertValue("pages", m.get("pages"), SINGLE_SPACE, PERIOD));
        }

        final String publisher = m.get("publisher");
        subpreview
            .append(insertValue("publisher", publisher, SINGLE_SPACE, isEmpty(publisher) ? "" : COMMA_SPACE))
            .append(insertValue("city", m.get("CityProvinceCountry"), SINGLE_SPACE, ""));


        preview
            .append("<div class=\"subrecordentry\">")
            .append(removeTrailingComma(subpreview.toString()))
            .append("</div>")
            //.append("<div class=\"creative-activities-links\">")
            ;

        return preview.toString();
    }

    @Override
    public java.util.Map<String,String> format(java.util.Map<String,String> m)
    {
        String preview = buildPreview(m);

        m.put("preview", preview);
        m.put("sequencepreview", preview);
        return m;
    }
}
