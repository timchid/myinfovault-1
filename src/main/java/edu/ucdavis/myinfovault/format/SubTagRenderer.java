package edu.ucdavis.myinfovault.format;

import org.w3c.dom.Node;
import edu.ucdavis.myinfovault.htmlcleaner.SimpleTagRenderer;

public class SubTagRenderer extends SimpleTagRenderer
{
    public SubTagRenderer(Node n) { super(n); }
}
