package edu.ucdavis.myinfovault.format;

import java.util.Arrays;
import java.util.Collection;
import java.util.Map;
import java.util.logging.Logger;

/**<p>
 * This formatter combines the handling of City Province and Country formats.
 *
 * @author rhendric
 * @since MIV 4.4
 */
public class CityProvinceCountryFormatter extends FormatAdapter implements RecordFormatter
{
    private static Logger log = Logger.getLogger("MIV");
    private static String altkey = "CityProvinceCountry";
    private static final Collection<String> addedFields = Arrays.asList(new String[] {altkey});


    /**
     * @return The original Map with the new key "CityProvincecountry" added.
     */
    @Override
    public Map<String, String> format(Map<String, String> m)
    {
        StringBuilder value = new StringBuilder();

        applySubformats(m);

        // Note that the Country is not currently added to the output string
        if (!isEmptyOrBlank(m.get("city")))
        {
            value.append(insertValue("city", m.get("city"), SINGLE_SPACE, isEmptyOrBlank(m.get("province")) ? PERIOD : COMMA));
        }
        if (!isEmptyOrBlank(m.get("province")))
        {
            value.append(insertValue("province", m.get("province"), SINGLE_SPACE, PERIOD));
        }

        log.finest("Added combined City/Province/Country string \""+value+"\" under the key CityProvinceCountry");
        m.put(altkey, value.toString());
        return m;
    }

    @Override
    public Iterable<String> getAddedFields()
    {
        return addedFields;
    }
}
