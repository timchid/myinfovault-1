package edu.ucdavis.myinfovault.format;

import java.util.Map;

/**
 * A formatter for periodicals, such as journal citations.
 * @author Stephen Paulsen
 * @since MIV 2.0
 */
public class PeriodicalFormatter extends PreviewBuilder implements RecordFormatter
{
    /**
     * A formatter for periodicals, such as journal citations.
     * @param names An Iterable list of names to highlight in the citation. An empty list is allowed, <code>null</code> is not.
     */
    public PeriodicalFormatter(Iterable<String> names)
    {
        this.add( new TitleFormatter(PERIOD, PUB_GROUP_JOURNALS) )
            .add( new PublicationNameFormatter() )
            .add( new VolumeIssueFormatter() )
            .add( new AuthorFormatter(PERIOD, "author", "citation") )
            .add( new URLFormatter(URLFormatter.PreviewType.PUBLICATION).add(new QuoteCleaner("link")))
            ;
    }

    public PeriodicalFormatter()
    {
        this(null);
    }

    @Override
    protected String buildPreview(Map<String, String> m)
    {
        //System.out.println("Periodical FORMATTER :: running format()");
        this.applySubformats(m);
        StringBuilder preview = new StringBuilder();

        m.put("year", insertValue("year", m.get("year")));

        String s = m.get("citation");
        if ( ! isEmpty(s) ) {
            preview.append(s);
        }
        else
        {
            preview
                // Add '.' to author if not there
                //.append( insertValue("author", m.get("author"), "", isEmpty(s=m.get("author")) ? "" : (s.endsWith(PERIOD)||s.endsWith(PERIOD+"</span>")||s.endsWith(PERIOD+"</span></span>") ? SINGLE_SPACE : PERIOD+SINGLE_SPACE) ))
                .append( insertValue("author", m.get("author")))
                .append( insertValue("title", m.get("title"), SINGLE_SPACE, ""))
                .append( insertValue("journal", m.get("journal"), SINGLE_SPACE, COMMA))
                .append( insertValue("volume", m.get("VolumeIssue"), SINGLE_SPACE, ""))
                ;
            // When pages are listed we may have to eliminate trailing punctuation for the case
            // where there's no volume or issue.  This puts just the colon separator, which is how
            // the current system works, but that may have to be revisited.
            int l = preview.length();
            char lastchar = preview.charAt(l-1);

            if (!isEmpty(s=m.get("pages")))
            {
                preview.append(insertValue("pages", m.get("pages"), SINGLE_SPACE, ""));
//                preview.append(PERIOD);
                //String punctuation = isEmpty(m.get("VolumeIssue")) ? COMMA_SPACE : COLON+SINGLE_SPACE;
                //preview.append(punctuation + s);
            }
            else if (!isEmpty(m.get("VolumeIssue")))
            {
                if (lastchar == ':') {
                    preview.deleteCharAt(l-1);
                }
//                preview.append(PERIOD);
            }
            else if (!isEmpty(m.get("journal")))
            {
                if (lastchar == ',') {
                    preview.deleteCharAt(l-1);
                }

            }

            if(!hasPunctuation(preview)){
                preview.append(PERIOD);
            }
        }

        // Finally, add a period at the end of the preview if there's no punctuation at the end.
        //String ch = preview.substring(preview.length()-1);
        //if (! ch.matches("[.,!?]") ) {
        /*if (! hasPunctuation(preview)) {
            preview.append('.');
        }*/

        //MIV - 651 - Add ** IN PRESS ** or ** SUBMITTED ** at the end.
        if (!isEmpty(m.get("statusid")))
        {
            if (m.get("statusid").equals("2"))
            {
                preview.append(" ** IN PRESS ** ");
            }
            else if (m.get("statusid").equals("3"))
            {
                preview.append(" ** SUBMITTED ** ");
            }
        }

        /*if (!isEmpty(m.get("link")))
        {
            preview.append(m.get("link"));
        }*/

        return preview.toString();
    }
}
