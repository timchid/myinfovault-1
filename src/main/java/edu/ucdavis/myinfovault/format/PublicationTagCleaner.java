package edu.ucdavis.myinfovault.format;

import java.util.Map;

/**
 * Removes HTML/XML style tags, enclosed in <code>&lt;&hellip;&gt;</code>, from a field or fields.
 *
 * @author Brij Garg
 * @since MIV 2.0
 */
public class PublicationTagCleaner extends TagCleaner implements RecordFormatter
{
    public PublicationTagCleaner()
    {
        //DO NOTHING
    }

    public PublicationTagCleaner(String... fields)
    {
        super(fields);
    }

    @Override
    public Map<String, String> format(Map<String, String> m)
    {
        for (String field : fieldList)
        {
            String oldValue = m.get(field);
            int linkIndex = oldValue.indexOf(URLFormatter.PUBLICATIONLINKLABEL);
            if (linkIndex > 0)
            {
                String newValue = oldValue.replaceAll(URLFormatter.PUBLICATIONLINKLABEL, "");
                m.put(field, newValue);
            }
        }

        return super.format(m);
    }
}
