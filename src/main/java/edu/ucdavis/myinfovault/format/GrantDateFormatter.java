package edu.ucdavis.myinfovault.format;

import java.util.Map;

/**
 * @author Stephen Paulsen
 * @since MIV 2.0
 */
public class GrantDateFormatter extends FormatAdapter implements RecordFormatter
{
    private String fieldName = null;

    public GrantDateFormatter(String fieldName)
    {
        this.fieldName = fieldName;
        //System.out.println("GrantDateFormatter created with target field==["+this.fieldName+"]");
    }

    @Override
    public Map<String, String> format(Map<String, String> m)
    {
        applySubformats(m);

        String value = m.get(fieldName);
        //System.out.println("GrantDateFormatter got value ["+value+"] for field "+this.fieldName);
        if (! isEmpty(value))
        {
            value = value.replace("00/00/", "");
        }
        m.put(fieldName, value);

        return m;
    }
}
