/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: StudentAdvFormatter.java
 */

package edu.ucdavis.myinfovault.format;

import java.util.Map;

/**
 * @author Binhtri Huynh
 * @since MIV 2.0
 */
public class StudentAdvFormatter extends FormatAdapter implements RecordFormatter
{
    @Override
    public Map<String, String> format(Map<String, String> m)
    {
        this.applySubformats(m);

        String s = "";
        StringBuilder preview = new StringBuilder();


        try {
            String fromYear = m.get("year");
            String toYear = (Integer.parseInt(fromYear) + 1) + "";
            String yearRange = fromYear + DASH + toYear;
            m.put("year", yearRange);
        }
        catch (NumberFormatException nfe) {
            /* do nothing, allow the year field to retain its original value */
        }


        preview.append( insertValue("advise1", s = fixDescription(m.get("advise1"))) );
        preview.append( insertValue("ugnum", fixNumber(m.get("ugnum"))) );
        preview.append( insertValue("advise2", s = fixDescription(m.get("advise2")), COMMA_SPACE, NOTHING) );
        preview.append( insertValue("gnum", fixNumber(m.get("gnum"))) );
        preview.append( insertValue("advise3", s = fixDescription(m.get("advise3")), COMMA_SPACE, NOTHING) );

        if (!isEmpty(s) || !("0".equals(m.get("a3num"))))
        {
            if (isEmpty(s)) {
                preview.append( insertValue("a3num", fixNumber(m.get("a3num")), COMMA_SPACE, NOTHING) );
            }
            else {
                preview.append( insertValue("a3num", fixNumber(m.get("a3num"))));
            }
        }

        preview.append( insertValue("advise4", s = fixDescription(m.get("advise4")), COMMA_SPACE, NOTHING) );

        if (!isEmpty(s) || !("0".equals(m.get("a4num"))))
        {
            if (isEmpty(s)) {
                preview.append( insertValue("a4num", fixNumber(m.get("a4num")), COMMA_SPACE, ""));
            }
            else {
                preview.append( insertValue("a4num", fixNumber(m.get("a4num"))));
            }
        }

        m.put("preview", preview.toString());
        m.put("sequencepreview", preview.toString());
        return m;
    }


    String fixDescription(String d)
    {
        String result = "";

        if (isEmpty(d)) return result;

        d = d.trim();
        if (! isEmpty(d) )
        {
            if (d.endsWith(":"))
            {
                int location = d.lastIndexOf(':');
                d = d.substring(0, location);
            }
            //System.out.println("StudentAdvisingFormatter: modified string=["+s+"]");
            result = d+":";
        }

        return result;
    }


    String fixNumber(String number)
    {
        String result = "";

        number = number.trim();
        if (! isEmpty(number) )
        {
            try {
                int n = Integer.parseInt(number);
                if (n >= 0)
                {
                    result = " ("+n+")";
                }
            }
            catch (NumberFormatException nfe) {
                /* Do nothing, allow result string to remain empty. */
            }
        }

        return result;
    }
}
