package edu.ucdavis.myinfovault.format;

import java.util.Map;
import java.util.Properties;

import edu.ucdavis.myinfovault.PropertyManager;

public class AdditionalInfoHelper extends FormatAdapter
{
    public static String getMissingAdditionalHeaderPreview()
    {
        Properties labels = PropertyManager.getPropertySet("additional", "labels");
        String fieldLabel = labels.getProperty("header");
        return missing(fieldLabel);
    }

    public static void getMissingAdditionalRecordPreview(Map<String, Map<String, String>> m)
    {
        for (String key : m.keySet())
        {
            Map<String,String> record = m.get(key);
            String preview = record.get("preview");
            if (preview == null || "".equals(preview))
            {
                Properties labels = PropertyManager.getPropertySet("additional", "labels");
                String fieldLabel = labels.getProperty("content");
                record.put("preview", missing(fieldLabel));
            }
        }
    }

    public static void formatAdditionalRecordType(Map<String, Map<String, String>> m)
    {
        for (String key : m.keySet())
        {
            Map<String,String> record = m.get(key);
            record.put("sequencepreview", record.get("preview"));
        }
    }
}
