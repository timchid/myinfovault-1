/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: EventsFormatter.java
 */

package edu.ucdavis.myinfovault.format;

import java.util.Map;

/**
 * A formatter for events.
 * @author rhendric
 * @since MIV 4.4
 */
public class EventsFormatter extends PreviewBuilder implements RecordFormatter
{
    public EventsFormatter()
    {
        this.add( new URLFormatter(URLFormatter.PreviewType.EVENTS).add( new QuoteCleaner("link")) )
            .add( new CityProvinceCountryFormatter() )
            .add( new SpanClassFormatter("eventtype", "typefield") )
            ;
    }

    @Override
    public String buildPreview(Map<String, String> m)
    {
        this.applySubformats(m);

        StringBuilder preview = new StringBuilder();
        StringBuilder subpreview = new StringBuilder();

        // Combine the Start and End dates and call the combination "date"
        String date = m.get("eventstartdate");

        final String eventEndDate = m.get("eventenddate");
        if (hasText(eventEndDate))
        {
            date += "&nbsp;to <br>" + eventEndDate;
        }

        preview
            .append( insertValue("date", date, "<div class=\"daterange\">", "</div>", true) );

        subpreview
            .append( insertValue("type", m.get("eventtype"), "", COLON+SINGLE_SPACE) )
            .append( insertValue("eventdescription", m.get("eventdescription")) )
            .append( insertValue("venue", m.get("venue"), COMMA_SPACE, PERIOD ) )
            .append( insertValue("venudescription", m.get("venudescription"), SINGLE_SPACE, PERIOD) )
            .append( insertValue("city", m.get("CityProvinceCountry"), SINGLE_SPACE, "") );

        preview
            .append("<div class=\"subrecordentry\">")
            .append(removeTrailingComma(subpreview.toString()))
            .append("</div>");
            //.append("<div class=\"creative-activities-links\">");

        return preview.toString();
    }

    @Override
    public java.util.Map<String,String> format(java.util.Map<String,String> m)
    {
        String preview = buildPreview(m);

        m.put("preview", preview);
        m.put("sequencepreview", preview);
        return m;
    }
}

