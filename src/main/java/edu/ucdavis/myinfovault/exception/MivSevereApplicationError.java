/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: MivSevereApplicationError.java
 */

package edu.ucdavis.myinfovault.exception;

import edu.ucdavis.myinfovault.message.ErrorMessages;

/**
 * Signals a severe error, usually of the "this can't possibly happen" variety, that
 * will send a user to the error page where they can send an email to the MIV help list.
 * A message <em>must</em> be supplied in a constructor to be displayed in the log and
 * on the error page. The displayed message can be overridden with a friendlier message
 * to show on the error reporting page by using
 * the {@link #setDisplayMessage(CharSequence) setDisplayMessage} call.
 *
 * @author Stephen Paulsen
 * @since MIV 3.0
 */
public class MivSevereApplicationError extends RuntimeException
{
    private static final long serialVersionUID = 200905010933L;

    /** The displayMessage will show in the ReportError page. It may be different than the logged error message. */
    private String displayMessage;


    /**
     * Set the message to be displayed in the error reporting page.
     * The message provided in the constructor will be used unless this method
     * is called to override it.
     *
     * @param message a message to display to the user in the ReportError page.
     */
    public void setDisplayMessage(CharSequence message)
    {
        this.displayMessage = getMessageInternal(message.toString());
    }


    /**
     * Set the message to be displayed in the error reporting page.
     * The message provided in the constructor will be used unless this method
     * is called to override it.
     *
     * @param messageId the <em>Identifier</em> of an externalized message to display to the user
     *  in the ReportError page.
     * @param messageParameters as many parameters as necessary to fill "<code>{0}</code>" style placeholders in the message.
     */
    public void setDisplayMessage(String messageId, Object... messageParameters)
    {
        this.displayMessage = getMessageInternal(messageId, messageParameters);
    }


    /**
     * Return the message currently set to display to the user.
     *
     * @return the message String
     */
    public String getDisplayMessage()
    {
        return this.displayMessage;
    }



    // -----=== "Inherited" Constructors ===----- //


    /**
     * Create a new MivSevereApplicationError with the String specified as an error message.
     *
     * @param message The error message or message Identifier to be logged and displayed for the exception.
     */
    public MivSevereApplicationError(String message)
    {
        super(getMessageInternal(message));
        this.displayMessage = getMessageInternal(message);
    }


    /**
     * Create a new MivSevereApplicationError with the given Exception base cause and detail message.
     *
     * @param message The error message to be logged and displayed for the exception.
     * @param cause The <code>Throwable</code> that was the cause for raising this application error.
     */
    public MivSevereApplicationError(String message, Throwable cause)
    {
        super(getMessageInternal(message), cause);
        this.displayMessage = getMessageInternal(message);
    }


    /**
     * Create a new MivSevereApplicationError with the given detail message,
     * with replaceable parameters substituted using the provided message parameters.
     *
     * @param messageId the <em>Identifier</em> of an externalized message to display to the user in the ReportError page.
     * @param messageParameters as many parameters as necessary to fill "<code>{0}</code>" style placeholders in the message.
     */
    public MivSevereApplicationError(String messageId, Object...messageParameters)
    {
        this(getMessageInternal(messageId, messageParameters));
    }


    /**
     * Create a new MivSevereApplicationError with the given Exception base cause and detail message,
     * with replaceable parameters substituted using the provided message parameters.
     *
     * @param messageId the <em>Identifier</em> of an externalized message to display to the user in the ReportError page.
     * @param cause The <code>Throwable</code> that was the cause for raising this application error.
     * @param messageParameters as many parameters as necessary to fill "<code>{0}</code>" style placeholders in the message.
     */
    public MivSevereApplicationError(String messageId, Throwable cause, Object...messageParameters)
    {
        this(getMessageInternal(messageId, messageParameters), cause);
    }


    /**
     * Retrieves a message from an externalized resource.
     *
     * @param messageId the <em>Identifier</em> of an externalized message to display to the user in the ReportError page.
     * @return the message String from the ResourceBundle
     */
    private static final String getMessageInternal(String messageId)
    {
        String message = ErrorMessages.getInstance().getString(messageId);
        if (message == null) message = messageId;
        return message;
    }


    /**
     * Retrieves a formatted message from an externalized resource.
     *
     * @param messageId the <em>Identifier</em> of an externalized message to display to the user in the ReportError page.
     * @param messageParameters as many parameters as necessary to fill "<code>{0}</code>" style placeholders in the message.
     * @return the message String, with all parameter substitution and formatting complete.
     */
    private static final String getMessageInternal(String messageId, Object[] messageParameters)
    {
        String message = ErrorMessages.getInstance().getString(messageId, messageParameters);
        if (message == null) message = messageId;
        return message;
    }
}
