/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: MIVApplicationException.java
 */

package edu.ucdavis.myinfovault.exception;

import edu.ucdavis.myinfovault.message.ErrorMessages;

/**
 * MIV application checked exception with access to {@link ErrorMessages externalized error messages}.
 * 
 * @author Stephen Paulsen
 * @since MIV 2.0
 */
public class MIVApplicationException extends Exception
{
    private static final long serialVersionUID = 200806171152L;

    /**
     * Constructs a new MIV application exception with null as its detail message.
     * 
     * @see Exception#Exception()
     */
    public MIVApplicationException() {}

    /**
     * Constructs a new MIV application exception with the specified detail message and cause.
     * 
     * @param message detail message
     * @param cause the cause
     * @see Exception#Exception(String, Throwable)
     */
    public MIVApplicationException(String message, Throwable cause)
    {
        super(message, cause);
    }

    /**
     * Constructs a new MIV application exception with the specified detail message.
     * 
     * @param message detail message
     * @see Exception#Exception(String)
     */
    public MIVApplicationException(String message)
    {
        super(message);
    }

    /**
     * Constructs a new MIV application exception with the specified cause and a detail message of cause.toString().
     * 
     * @param cause the cause
     * @see Exception#Exception(Throwable)
     */
    public MIVApplicationException(Throwable cause)
    {
        super(cause);
    }
    
    /**
     * Constructs a new MIV application exception with the specified cause.
     * The detail message is generated from the given message parameters and
     * the {@link ErrorMessages} message pattern mapped to the message ID.
     * 
     * @param messageId the <em>Identifier</em> of an externalized message
     * @param cause cause for raising the exception
     * @param messageParameters parameters associated with the externalized message
     */
    public MIVApplicationException(String messageId, Throwable cause, Object...messageParameters)
    {
        super(ErrorMessages.getInstance().getString(messageId, messageParameters), cause);
    }
}
