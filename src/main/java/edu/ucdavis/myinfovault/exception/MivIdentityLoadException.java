/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: MivIdentityLoadException.java
 */


package edu.ucdavis.myinfovault.exception;

import edu.ucdavis.mw.myinfovault.service.person.MivPerson;

/**
 * Exception to indicate the detail information for a person couldn't
 * be loaded from external source(s).
 *
 * @author Stephen Paulsen
 * @since MIV 3.0
 */
public class MivIdentityLoadException extends MivSevereApplicationError  // RuntimeException
{
    private static final long serialVersionUID = 200907151718L;

    private MivPerson person;

// Removed some constructors when re-parenting this exception
// because MivSevereApplicationError doesn't have them.
// SDP 2009-07-20

//    public MivIdentityLoadException()
//    {
//        super();
//    }

    public MivIdentityLoadException(String message)
    {
        super(message);
    }

//    public MivIdentityLoadException(Throwable cause)
//    {
//        super(cause);
//    }

    public MivIdentityLoadException(String message, Throwable cause)
    {
        super(message, cause);
    }

//    public MivIdentityLoadException(MivPerson person)
//    {
//        super();
//        this.person = person;
//    }

    public MivIdentityLoadException(MivPerson person, String message)
    {
        super(message);
        this.person = person;
    }

//    public MivIdentityLoadException(MivPerson person, Throwable cause)
//    {
//        super(cause);
//        this.person = person;
//    }

    public MivIdentityLoadException(MivPerson person, String message, Throwable cause)
    {
        super(message, cause);
        this.person = person;
    }

    public MivIdentityLoadException(MivPerson person, String messageId, Object...messageParameters)
    {
        super(messageId, messageParameters);
        this.person = person;
    }

    public MivPerson getPerson()
    {
        return this.person;
    }
}
