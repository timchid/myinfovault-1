package edu.ucdavis.myinfovault.designer;

/**
 * FIXME: needs Javadoc! 
 * @author Venkat
 */
public interface DisplayState
{
    public void setCbName(String cbName);
    public String getCbName();

    public void setDisplayStatus(boolean displayStatus);
    public boolean getDisplayStatus();

    public void setDisplayField(String displayField);
    public String getDisplayField();

    public String getRecID();
    /** An alias for #getRecID() */
    public String getID();

    public String getRecType();
}
