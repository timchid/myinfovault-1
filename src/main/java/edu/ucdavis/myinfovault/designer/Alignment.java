package edu.ucdavis.myinfovault.designer;

/**
 * Specifies text alignment for biosketch design.
 * @author Stephen Paulsen
 * @since MIV 2.1
 */
public enum Alignment
{
    LEFT,
    CENTER,
    RIGHT,
    JUSTIFY;
}
