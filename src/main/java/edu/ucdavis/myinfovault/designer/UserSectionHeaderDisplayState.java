package edu.ucdavis.myinfovault.designer;


public class UserSectionHeaderDisplayState extends BaseDisplayState implements HeaderDisplayState
{
    static final String tableName = "USH";
    String sectionHeading;

    public UserSectionHeaderDisplayState(String cbName, String recID, String recType, String displayFieldName, String displayFlag)
    {
        super(cbName, recID, recType, displayFieldName, displayFlag);
    }

    public void setSectionHeading(String sectionHeading)
    {
        this.sectionHeading = sectionHeading;
    }

    public String getSectionHeading()
    {
        return this.sectionHeading;
    }

    public String toString()
    {
        return super.toString() + "," + tableName + "," + sectionHeading;
    }
}
