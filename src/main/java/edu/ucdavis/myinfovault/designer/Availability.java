package edu.ucdavis.myinfovault.designer;

/**
 * A collection of section availability status.
 * @author Brij Garg
 * @since MIV 2.1
 */
public enum Availability
{
    MANDATORY,
    OPTIONAL_ON,
    OPTIONAL_OFF,
    PROHIBITED;
}
