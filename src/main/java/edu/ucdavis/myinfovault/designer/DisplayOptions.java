/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: DisplayOptions.java
 */


package edu.ucdavis.myinfovault.designer;

import java.util.List;

import edu.ucdavis.myinfovault.PropertyManager;
import edu.ucdavis.myinfovault.data.DisplaySection;
import edu.ucdavis.myinfovault.data.DocumentIdentifier;

/**
 * This is simple java class that holds the all the sections in a document along with some available options information.
 * It is used by the dossier design page.
 *
 * @author venkat
 * @since MIV 2.0
 */
public class DisplayOptions
{
    private static final String RECORD_TYPE_GRANT = "grant";
    private static final String RECORD_TYPE_STUDENT_ADVISING = "student-advising";
    private static final String RECORD_TYPE_ADDITIONAL = "additional";

    private String recType;
    private boolean showDisplay = true;
    private boolean showHeaderEditLink = false;
    private boolean showAnnotationLink = false;
    private boolean showGoals = false;
    private boolean printSectionHeader = true;
    private boolean disableSectionHeader = false;
    private String sectionName;
    private DisplaySection displaySection;
    private List<DisplaySection> displaySections;
    private String sectionHeading;
    private String sectionID;
    private boolean showCommitteesHeading = false;


    /**
     * This is for editable section headers. <code>true</code> indicates that the Editable
     * Header check box is checked.
     */
    private boolean displaySectionHeader = true; // TODO: this is always true - is it needed? It's used in design.jsp

    /**
     * This is to indicate Edit Header link and  If we
     * have only one section we don't show section header.
     */
    private boolean showSectionHeader = true;

    /**
     * This is to indicate whether to show check box or not.
     */
    private boolean includeInDossier = false;

    public DisplayOptions(String recType,
                          String sectionName,
                          String sectionHeading,
                          List<DisplaySection> displaySections,
                          DocumentIdentifier docId,
                          int userId,
                          boolean showSectionHeader)
    {
        this.recType = recType;
        this.sectionName = sectionName;
        this.sectionHeading = sectionHeading;
        this.displaySections = displaySections;
        this.showSectionHeader = showSectionHeader;

        // TODO: refactor the if else blocks out
        if (docId == DocumentIdentifier.PUBLICATIONS) // means all publications and presentations
        {
            this.showHeaderEditLink = true;
            this.showAnnotationLink = true;
            this.includeInDossier = true;
        }
        else if (docId == DocumentIdentifier.CREATIVE_ACTIVITIES) // means all creative activities
        {
            this.showAnnotationLink = true;
            this.includeInDossier = true;

            // Description needed for Events and Works
            if (recType.equalsIgnoreCase("events") || recType.equalsIgnoreCase("works"))
            {
                this.sectionHeading =  setRecordDescription(PropertyManager.getPropertySet(recType, "strings").getProperty("description")) + sectionHeading;
            }
        }
        else if (recType.equalsIgnoreCase(RECORD_TYPE_GRANT)) // means grants record
        {
            this.showGoals = true;
        }
        else if (recType.equalsIgnoreCase(RECORD_TYPE_STUDENT_ADVISING)) // means student advising record
        {

        }
        else if (recType.equalsIgnoreCase(RECORD_TYPE_ADDITIONAL)) // means all additional-information records
        {
            this.showHeaderEditLink = true;
            this.includeInDossier = true;
        }
        else if (recType.equalsIgnoreCase(RECORD_TYPE_STUDENT_ADVISING) && docId == DocumentIdentifier.EVALUATIONS) // List of evaluations
        {
          this.showAnnotationLink = true;
        }
        else if (docId == DocumentIdentifier.SERVICE) // means all services
        {
          this.showAnnotationLink = true;
        }
        else if (docId == DocumentIdentifier.EXTENDING_KNOWLEDGE) // means all extending knowledge records
        {
          this.showAnnotationLink = true;
        }
        else if (docId == DocumentIdentifier.SHORT_BIOGRAPHY) // means all short biography records
        {
          this.includeInDossier = true;
        }
    }

    /**
     * To get record description to add in front of section heading
     * @param description
     * @return
     */
    public static String setRecordDescription(String description)
    {
        if(description!=null && description.trim().length()>0)
        {
            description =  "<span class='section-description'>[ "+description + " ]</span> ";
        }
        else
        {
            description = "";
        }
        return description;
    }


    public DisplaySection getDisplaySection()
    {
        return displaySection;
    }

    public String getRecType()
    {
        return recType;
    }

    public boolean getShowAnnotationLink()
    {
        return showAnnotationLink;
    }

    public boolean getShowDisplay()
    {
        return showDisplay;
    }

    public boolean getShowGoals()
    {
        return showGoals;
    }

    public boolean getShowHeaderEditLink()
    {
        return showHeaderEditLink;
    }

    public String getSectionHeading()
    {
        return sectionHeading;
    }

    public boolean getDisplaySectionHeader()
    {
        return displaySectionHeader;
    }

    public boolean getShowSectionHeader()
    {
        return showSectionHeader;
    }

    public String getSectionName()
    {
        return sectionName;
    }

    public boolean getIncludeInDossier()
    {
        return includeInDossier;
    }

    public List<DisplaySection> getDisplaySections()
    {
        return displaySections;
    }

    public void setDisplaySections(List<DisplaySection> displaySections)
    {
        this.displaySections = displaySections;
    }


    public String getSectionID()
    {
        return sectionID;
    }

    public void setSectionID(String sectionID)
    {
        this.sectionID = sectionID;
    }



    public boolean getPrintSectionHeader()
    {
        return printSectionHeader;
    }

    public void setPrintSectionHeader(boolean printSectionHeader)
    {
        // If there are no records for a section show that section as unchecked
        // but if it has record(s) set the value passed as parameter
        int displaySectionCount = this.displaySections.size();
        if (displaySectionCount > 0)
        {
            this.printSectionHeader = printSectionHeader;
        }
        else
        {
            this.printSectionHeader = false;
        }
    }


    public boolean getDisableSectionHeader()
    {
        return this.disableSectionHeader;
    }

    public void setDisableSectionHeader()
    {
        // If there are no records for a section, show that section checkbox as disabled
        // (it applies to publications and additional information)
        int displaySectionCount = this.displaySections.size();
        if (displaySectionCount == 0)
        {
            this.disableSectionHeader = true;
        }
    }


    public boolean getShowCommitteesHeading()
    {
        return showCommitteesHeading;
    }

    public void setShowCommitteesHeading(boolean showCommitteesHeading)
    {
        this.showCommitteesHeading = showCommitteesHeading;
    }

    /**
     * @return Show section if there is data or its an additional information section
     */
    public boolean getShowSection()
    {
        //has data?
        for (DisplaySection displaySection : displaySections)
        {
            if (!displaySection.getRecords().isEmpty())
            {
                return !sectionHeading.isEmpty();
            }
        }

        //else, must be additional type
        return recType.equalsIgnoreCase(RECORD_TYPE_ADDITIONAL);
    }
}
