package edu.ucdavis.myinfovault.designer;

public interface HeaderDisplayState extends DisplayState
{
    public void setSectionHeading(String sectionHeading);
    public String getSectionHeading();
}
