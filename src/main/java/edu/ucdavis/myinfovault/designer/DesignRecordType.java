package edu.ucdavis.myinfovault.designer;

/**
 * Specifies type for biosketch design row.
 * @author Srividya
 * @since MIV 2.1
 */
public enum DesignRecordType
{
    RECORD,
    NODATA,
    HEADING,
    SUBHEADING,
    ATTRIBUTE,
    ADDITIONALHEADER,
    DUMMYHEADING;
}
