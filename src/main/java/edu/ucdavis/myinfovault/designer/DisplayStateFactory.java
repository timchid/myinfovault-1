package edu.ucdavis.myinfovault.designer;


public class DisplayStateFactory
{
    private static final DisplayStateFactory factory = new DisplayStateFactory();

    private static final int REGULAR_RECORDS = 5;

    /* Don't allow creation of other factories */
    private DisplayStateFactory() { super(); }

    public static DisplayStateFactory getFactory()
    {
        return factory;
    }

    public DisplayState getHiddenValueObject(String hiddenValue)
    {
        DisplayState displayState =  null;
        String[] cbValues = hiddenValue.split(",");

        if (cbValues.length == REGULAR_RECORDS)
        {
            displayState = new BaseDisplayState(cbValues[0], cbValues[1], cbValues[2], cbValues[3], cbValues[4]);
        }
        else
        {
            String tableName = cbValues[5];
            if (tableName.equals(AdditionalHeaderDisplayState.tableName))
            {
                displayState = new AdditionalHeaderDisplayState(
                        cbValues[0], cbValues[1], cbValues[2], cbValues[3], cbValues[4]);
            }
            else if (tableName.equals(UserUploadDisplayState.tableName))
            {
                displayState = new UserUploadDisplayState(
                        cbValues[0], cbValues[1], cbValues[2], cbValues[3], cbValues[4]);
            }
            else if (tableName.equals(UserSectionHeaderDisplayState.tableName))
            {
                UserSectionHeaderDisplayState ush = new UserSectionHeaderDisplayState(
                        cbValues[0], cbValues[1], cbValues[2], cbValues[3], cbValues[4]);
                ush.setSectionHeading(cbValues[6]);
                displayState = ush;
            }
        }

        return displayState;
    }
}

