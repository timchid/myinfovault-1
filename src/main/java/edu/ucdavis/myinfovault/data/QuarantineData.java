/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: QuarantineData.java
 */


package edu.ucdavis.myinfovault.data;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.logging.Logger;

import edu.ucdavis.myinfovault.MIVConfig;
import edu.ucdavis.myinfovault.MIVUserInfo;

/**
 * Handle fetching, updating records in the quarantine table.
 *
 * @author rhendric
 * @since MIV 2.0
 * @deprecated Used for v2.x migration and data fixup. No longer needed.
 */
@Deprecated
public class QuarantineData
{
    private static final String QUARANTINE_QUERY_ALL =
        new StringBuilder().append("select q.ID, q.RecordID, q.RecordTableName, q.RecordFieldName, ").
        append("q.DocumentID, q.SectionID, q.Contents, q.Suggestion, q.Fixed, ").
        append("d.Description, s.Name, s.RecordName ").
        append("from Quarantine q, Document d, Section s where ").
        append("q.UserID = ? and q.Fixed = 0 and q.DocumentID = d.ID and q.SectionID = s.ID").toString();

    private MIVUserInfo userInfo = null;
    /** Map quarantine record number to a map of all data in the row. */
    private LinkedHashMap<Integer, LinkedHashMap<Integer, String>> quarantineData = null;
    /** Maps quarantine record number to a map of data to be displayed for a row */
    private LinkedHashMap<Integer, LinkedHashMap<Integer, String>> quarantineDisplayData = null;
    /** Maps the database column name to its column number */
    private LinkedHashMap<String, Integer> quarantineDataColumns = new LinkedHashMap<String, Integer>();
    /** Maps the database column number to the the column title to be displayed */
    private LinkedHashMap<Integer, String> quarantineDisplayColumns = new LinkedHashMap<Integer, String>();
    /** Defines the database columns to be display maps the database column name to the display name for the column. */
    private LinkedHashMap<String, String> displayColumnMap = new LinkedHashMap<String, String>();
    /** Maps packet record number to the quarantine record...one packet record can map to multiple quarantine records */
    private LinkedHashMap<String, ArrayList<Integer>> recordMap = new LinkedHashMap<String, ArrayList<Integer>>();
    /** Maps packet record number to the quarantine record fields...one packet record can map to multiple quarantine record fields */
    private LinkedHashMap<String, ArrayList<String>> recordFieldMap = new LinkedHashMap<String, ArrayList<String>>();
    /**
     * Map of section names mapped to record ID's for the section. This map has a compound key since there may be
     * several of the same sections represented, each with different target records. The key will be
     * made up of the query section name and the record ID in the form "<Name>::<RecordID>"
     */
    private LinkedHashMap<String, String> sectionRecordMap = new LinkedHashMap<String, String>();
    /**
     * Map of section record names mapped to record ID's for the section. This map has a compound key since there may be
     * several of the same sections represented, but each with different fields in the target records. The key will be
     * made up of the query section name and the record field name "<RecordName>::<RecordFieldName>"
     */
    private LinkedHashMap<String, String> sectionFieldMap = new LinkedHashMap<String, String>();
    /** Map of Document ID's and names which have quarantine records for the current user. */
    private LinkedHashMap<String, String> documentIDMap = new LinkedHashMap<String, String>();


    private static Logger log = Logger.getLogger("MIV");
    boolean debug = MIVConfig.getConfig().isDebug();


    public QuarantineData(MIVUserInfo userInfo)
    {
        if (this.userInfo != null && (this.userInfo.getPerson().getUserId() != userInfo.getPerson().getUserId()))
        {
            this.quarantineData = null;
        }
        this.userInfo = userInfo;

        // Map of DB column names to display names. Only columns in the map are placed in the
        // quarantineDisplayData map. Note the order is the display order from left to right
        // in a table.
        this.displayColumnMap.put("Name", "Publication Type");
        this.displayColumnMap.put("RecordFieldName", "Field");
        this.displayColumnMap.put("Contents", "Invalid HTML Code");
        this.displayColumnMap.put("Suggestion", "Replace Invalid HTML Code With This?");
        this.setQuarantineData();
    }

    /**
     * Get the quarantine data for a user.
     * Data is stored in LinkedHashMaps of RecordIDs referencing a LinkedHashMap of column values
     * Two LinkedHashMaps are built, one for all data and one of just data which will may be displayed in
     * a JSP page.
     */
    private void getData()
    {
        int columnCount = 0;
        ResultSet querySet = null;
        PreparedStatement statement = null;
        Connection connection = null;
        ResultSetMetaData metaData = null;

        LinkedHashMap<Integer, String> quarantineRecordMap = null;
        LinkedHashMap<Integer, String> quarantineDisplayRecordMap = null;

        // Maps quarantine record number to a map of data in all columns
        this.quarantineData = new LinkedHashMap<Integer, LinkedHashMap<Integer, String>>();
        // Maps quarantine record number to a map of data in columns to be displayed for a row
        this.quarantineDisplayData = new LinkedHashMap<Integer, LinkedHashMap<Integer, String>>();
        // Maps packet record number to the quarantine record...one packet record can map to multiple quarantine records
        this.recordMap = new LinkedHashMap<String, ArrayList<Integer>>();
        // Maps packet record number to the quarantine record fields...one packet record can map to multiple quarantine record fields
        this.recordFieldMap = new LinkedHashMap<String, ArrayList<String>>();
        // Map of section names mapped to record ID's for the section. This map has a compound key since there may be
        // several of the same sections represented, each with different target records. The key will be
        // made up of the query section name and the record ID in the form "<Name>::<RecordID>"
        this.sectionRecordMap = new LinkedHashMap<String, String>();
        // Map of section record names mapped to record ID's for the section. This map has a compound key since there may be
        // several of the same sections represented, but each with different fields in the target records. The key will be
        // made up of the query section name and the record field name "<RecordName>::<RecordFieldName>"
        this.sectionFieldMap = new LinkedHashMap<String, String>();
        // Map of Document ID's and names which have quarantine records for the current user.
        this.documentIDMap = new LinkedHashMap<String, String>();

        try
        {
            connection = this.userInfo.getRecordHandler().getConnection();
            statement = connection.prepareStatement(QUARANTINE_QUERY_ALL);
            statement.setInt(1, this.userInfo.getPerson().getUserId());
            querySet = statement.executeQuery();
            metaData = querySet.getMetaData();
            columnCount = metaData.getColumnCount();
            // Map of the database column names to the column number
            for (int i = 1; i <= columnCount; i++)
            {
                this.quarantineDataColumns.put(metaData.getColumnName(i), Integer.valueOf(i));
            }
            // Map of the display column names mapped to the column number
            for (String columnName : this.displayColumnMap.keySet())
            {
                Integer columnNumber = this.quarantineDataColumns.get(columnName);
                if (columnNumber != null)
                {
                    this.quarantineDisplayColumns.put(columnNumber, this.displayColumnMap.get(columnName));
                }
            }

            // Put the data into the map
            while (querySet.next())
            {
                quarantineRecordMap = new LinkedHashMap<Integer, String>();
                quarantineDisplayRecordMap = new LinkedHashMap<Integer, String>();
                String sectionKey = null;
                ArrayList <Integer> recList = null;
                ArrayList <String> fieldList = null;
                for (int i = 1; i <= columnCount; i++)
                {
                    if (debug) {
                        Object o = querySet.getObject(i);
                        System.out.println("i (col number): "+i+" class: "+o.getClass().getSimpleName());
                        System.out.println("         value: "+o.toString());
                    }
                    quarantineRecordMap.put(Integer.valueOf(i), querySet.getObject(i).toString());

                    // Map the section name to the record ID for the section
                    if (metaData.getColumnName(i).equals("Name"))
                    {
                        // Build the section key which consists of the query section name and the record ID
                        // in the form "<Name>::<RecordID>". The key points to the packet RecordID
                        // The map is used to quickly know if a record ID in a section has a quarantine record.
                        sectionKey = new StringBuilder(querySet.getObject(i).toString()).
                                     append("::").append(Integer.valueOf(querySet.getInt(2))).toString();
                        this.sectionRecordMap.put(sectionKey, Integer.valueOf(querySet.getInt(2)).toString());
                    }

                    // Map the section record name to the record ID for the section
                    if (metaData.getColumnName(i).equals("RecordName"))
                    {
                        // Build the section record key which consists of the section record name and the field name
                        // in the form "<RecordName>::<RecordFieldName>::<RecordID>" The key points to the packet RecordID
                        // The map is used to find all records in the quarantine table which are in the same document and section.
                        sectionKey = new StringBuilder(querySet.getObject(i).toString()).
                                     append("::").
                                     append(querySet.getString(4)).
                                     append("::").
                                     append(Integer.valueOf(querySet.getInt(2))).toString();
                        this.sectionFieldMap.put(sectionKey, Integer.valueOf(querySet.getInt(2)).toString());
                    }

                    // Map of the documentID and document name. Used at packet generation time determine if a document
                    // can be generated
                    if (metaData.getColumnName(i).equals("Description"))
                    {
                        this.documentIDMap.put(Integer.valueOf(querySet.getInt(5)).toString(), querySet.getString(10));
                    }
                }
                // record map for each recordID
                this.quarantineData.put(Integer.valueOf(querySet.getInt(1)), quarantineRecordMap);

                // Add columns to the display only data map. Do this here to maintain column order.
                for (Integer columnNumber : this.quarantineDisplayColumns.keySet())
                {
                    // Capitalize the first letter of the Name for display
                    String value = quarantineRecordMap.get(columnNumber);
                    if (metaData.getColumnName(columnNumber.intValue()).equals("Name"))
                    {
                        value = new StringBuilder(value.substring(0, 1).toUpperCase()).append(value.substring(1)).toString();
                    }
                    quarantineDisplayRecordMap.put(columnNumber, value);
                }
                this.quarantineDisplayData.put(Integer.valueOf(querySet.getInt(1)), quarantineDisplayRecordMap);
                // Map the RecordID of the packet record to the quarantine records. Note: One RecordID can be referenced
                // by multiple quarantine records.
                String [] recordType = quarantineRecordMap.get(this.quarantineDataColumns.get("RecordName")).split("-");

                String recordKey = quarantineRecordMap.get(this.quarantineDataColumns.get("RecordID"))+recordType[0];
                if (this.recordMap.containsKey(recordKey))
                {
                    recList = this.recordMap.get(recordKey);
                    recList.add(Integer.valueOf(querySet.getInt(1)));
                }
                else
                {
                    recList = new ArrayList<Integer>();
                    recList.add(Integer.valueOf(querySet.getInt(1)));
                    this.recordMap.put(recordKey, recList);
                }

                // Map the RecordID of the packet record to the quarantine records fields. Note: One RecordID can be referenced
                // by multiple quarantine record fields.
                if (this.recordFieldMap.containsKey(recordKey))
                {
                    fieldList = this.recordFieldMap.get(recordKey);
                    fieldList.add(querySet.getString(4));
                }
                else
                {
                    fieldList = new ArrayList<String>();
                    fieldList.add(querySet.getString(4));
                    this.recordFieldMap.put(recordKey, fieldList);
                }

            }

            // For debug
            if (debug) {
                for (String record : recordMap.keySet()) {
                    System.out.println();
                    System.out.println("Record: "+record);
                    ArrayList <Integer> recList = recordMap.get(record);
                    for (Integer qRec : recList) {
                        System.out.println("  QRecord: "+qRec);
                    }
                }
            }

        }
        catch (SQLException e)
        {
            log.finer(new StringBuilder("SQLException: ").
                    append(e.getErrorCode()).toString());
            e.printStackTrace();
        }
        finally
        {
            try {
                if (querySet != null) {
                    querySet.close();
                }
                if (statement != null) {
                    statement.close();
                }
                this.userInfo.getRecordHandler().releaseConnection(connection);
            } catch (SQLException e) {
                //do nothing
            }
        }
    }

    /**
     * Initialize the quarantineData LinkedHashMap if not already done
     */
    private void setQuarantineData()
    {
        if (this.quarantineData == null)
        {
            if (debug) {
                System.out.println("Reloading qdata!");
            }
            this.getData();
        }
        else
        {
            if (debug) {
                System.out.println("Skip Reloading qdata!");
            }
        }
    }

    /**
     * Get the quarantineData LinkedHashMap
     * Contains all data columns for each record
     * @return quarantineData
     */
    public LinkedHashMap<Integer, LinkedHashMap<Integer, String>> getQuarantineData()
    {
        this.setQuarantineData();
        return this.quarantineData;
    }

    /**
     * Get the quarantineDisplayData LinkedHashMap
     * Contains only column data meant for display
     * @return quarantineDisplayData
     */
    public LinkedHashMap<Integer, LinkedHashMap<Integer, String>> getQuarantineDisplayData()
    {
        this.setQuarantineData();
        return this.quarantineDisplayData;
    }

    /**
     * Get the quarantineDataColumns LinkedHashMap
     * Contains column name mapped to the database column number
     * @return quarantineDataColumns
     */
    public LinkedHashMap<String, Integer> getQuarantineDataColumns()
    {
        this.setQuarantineData();
        return this.quarantineDataColumns;
    }

    /**
     * Get the quarantineDisplayColumns LinkedHashMap
     * Contains database column number mapped to the column display name
     * @return quarantineDisplayColumns
     */
    public LinkedHashMap<Integer, String> getQuarantineDisplayColumns()
    {
        this.setQuarantineData();
        return this.quarantineDisplayColumns;
    }

    /**
     * Get the number of quarantine records for the current UserID
     * @return quarantineData.size()
     */
    public int getRecordCount()
    {
        this.setQuarantineData();
        return this.quarantineData.size();
    }

    /**
     * Get the map of section names to packet records
     * @return sectionRecordMap
     */
    public LinkedHashMap<String, String> getSectionRecordMap()
    {
        this.setQuarantineData();
        return this.sectionRecordMap;
    }

    /**
     * Get the map of section names field names to packet records
     * @return sectionFieldMap
     */
    public LinkedHashMap<String, String> getSectionFieldMap()
    {
        this.setQuarantineData();
        return this.sectionFieldMap;
    }

    /**
     * Get the record map of packet records to quarantine records
     * @return recordMap
     */
    public LinkedHashMap<String, ArrayList<Integer>> getRecordMap()
    {
        this.setQuarantineData();
        return this.recordMap;
    }

    /**
     * Get the record map of packet records to quarantine record fields
     * @return recordFieldMap
     */
    public LinkedHashMap<String, ArrayList<String>> getRecordFieldMap()
    {
        this.setQuarantineData();
        return this.recordFieldMap;
    }

    /**
     * Get the map of documentIDs
     * @return documentIDMap
     */
    public LinkedHashMap<String, String> getDocumentIDMap()
    {
        this.setQuarantineData();
        return this.documentIDMap;
    }

    /**
     * Reloads the quarantine data
     */
    public void reloadQuarantineData()
    {
        this.quarantineData = null;
        this.setQuarantineData();
    }

    /**
     * Update all quarantine records for a record type.
     * 
     * @param recordType 
     * @param recordID 
     * @return 
     */
    public int updateAllQrecords(String recordType, String recordID)
    {
        String [] fields = {"Fixed"};
        String [] values = {"1"};
        int recsUpdated = 0;
        for (String section : this.sectionFieldMap.keySet())
        {
            // Section record names have a "-record" suffix, it is
            // only the first part we are interested in here
            if (section.startsWith(recordType) && this.sectionFieldMap.get(section).equals(recordID))
            {
                // Get the packet record associated with this section and field
                String packetRecord = this.sectionFieldMap.get(section);
                // Get the Quarantine records which referenced this packed recordId
                ArrayList <Integer> qRecords = this.recordMap.get(packetRecord+recordType);
                if (qRecords != null)
                {
                    for (Integer qRecord : qRecords)
                    {
                        String logMesg = new StringBuilder("Begin quarantine update: record=").append(qRecord).toString();
                        log.fine(logMesg);
                        recsUpdated += this.userInfo.getRecordHandler().update("Quarantine", qRecord.intValue(), fields, values);
                        logMesg = new StringBuilder(Integer.valueOf(recsUpdated).toString()).append(" records updated!").toString();
                        log.fine(logMesg);
                    }
                    this.reloadQuarantineData();
                }
                else
                {
                    log.finest("  No Qarantine records found to update!");
                }
                // When we've made an update, we are done
                break;
            }
        }
        return recsUpdated;
    }

    /**
     * Update specific quarantine records.
     * 
     * @param qRecords 
     * @param recordHandler 
     * @return 
     */
    public int updateQrecord(ArrayList<Integer> qRecords, RecordHandler recordHandler)
    {
        String[] fields = { "Fixed" };
        String[] values = { "1" };
        int recsUpdated = 0;
        if (qRecords != null)
        {
            System.out.println("qRecords size:" + qRecords.size());
            for (Integer qRecord : qRecords)
            {
                String logMesg = new StringBuilder("Begin quarantine update: record=").append(qRecord).toString();
                log.fine(logMesg);
                recsUpdated += recordHandler.update("Quarantine", qRecord.intValue(), fields, values);
                logMesg = new StringBuilder(Integer.valueOf(recsUpdated).toString()).append(" records updated!").toString();
                log.fine(logMesg);
            }
            this.reloadQuarantineData();
        }
        else
        {
            log.finest("  No Qarantine records found to update!");
        }
        return recsUpdated;
    }

}
