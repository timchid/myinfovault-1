/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: QueryResultMapper.java
 */

package edu.ucdavis.myinfovault.data;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import org.springframework.jdbc.core.RowMapper;

/**
 * Mapper class to provide common RowMappers to store ResultSet data
 * @author Pradeep K Haldiya
 * @since MIV 4.0
 */
public class QueryResultMapper
{
    /**
     * Mapper to store resultset into Map<String,Object>
     */
    public static RowMapper<Map<String,Object>> resultMapper = new RowMapper<Map<String,Object>>() {
        @Override
        public Map<String, Object> mapRow(final ResultSet rs, final int rowNum) throws SQLException
        {
            ResultSetMetaData lResMetadata = rs.getMetaData();
            int liColumnCnt = lResMetadata.getColumnCount();
            Map<String, Object> resultMap = new HashMap<String, Object>();
            for (int licnt = 1; licnt <= liColumnCnt; licnt++)// loop for each column
            {
                String lStrColumnName = lResMetadata.getColumnName(licnt).toLowerCase();
                Object lStrAttrvalue = rs.getObject(licnt);
                resultMap.put(lStrColumnName, lStrAttrvalue);
            }
            return resultMap;
        }
    };
}
