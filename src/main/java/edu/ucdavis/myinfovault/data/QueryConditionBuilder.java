/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: QueryConditionBuilder.java
 */

package edu.ucdavis.myinfovault.data;

import java.util.List;

/**
 * ConditionBuilder class to build the Conditions for SQL queries
 * @author Pradeep K Haldiya
 * @since MIV 4.0
 */
public class QueryConditionBuilder
{
    private final String sql;
    private final int argc;

    public QueryConditionBuilder(final String sql, final int argc)
    {
        this.sql = sql;
        this.argc = argc;
    }

    public String getSql()
    {
        return this.sql;
    }

    /*
     * Ignores extra args, repeats last arg if too few args given
     */
    public void addArgs(final List<String> args, final String... arg)
    {
        // e.g. 3 needed, 1 given
        final int loopIterations = Math.min(arg.length, this.argc);
        // e.g. would have iters = 1
        for (int i = 0; i < loopIterations; i++)
        {
            args.add(arg[i]);
        }
        if (arg.length < this.argc) // e.g. 1 < 3
        {
            // not enough args passed... keep repeating the last arg given
            for (int i = 0; i < this.argc - arg.length; i++)
            {
                args.add(arg[arg.length - 1]);
            }
        }
    }

    @Override
    public String toString()
    {
        return sql;
    }
}
