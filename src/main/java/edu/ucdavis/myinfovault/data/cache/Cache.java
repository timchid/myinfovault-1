/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: Cache.java
 */


package edu.ucdavis.myinfovault.data.cache;

import java.util.Map;



/**
 * TODO: Extract "Cache" as an interface and make this just one implementation.
 * ... Cache, ReadCache, ReadWriteCache
 * ... Lifetime.SHORT : use a WeakHashMap; LifeTime.LONG : use a HashMap
 *<p>
 * This cache has no set capacity and makes no attempt to limit its size, for
 * example by dropping older cached objects (LRU) when nearing cache capacity.
 * Its strategy instead is to hold items for a set amount of time
 *</p>
 *
 *  More javadoc needed.
 *
 * type of the key (K) and type of the cached object (T)
 * e.g. new Cache<Number, Page>
 *
 * @author Stephen Paulsen
 * @param <K> the type of the key used to retrieve items from the cache
 * @param <E> the type of elements held in this cache
 * @since MIV 3.0
 */
public class Cache<K,E>
{
    /** Storage for the cached items. */
    Map<K,CachedObject> cache;
    /** Callback object to load items into the cache. */
    CacheLoader<K,E> loader;
    /** Periodic cleaner to remove old items from the cache. */
    CacheSweeper sweeper;
    /** Lifetime before an item is removed from the cache. */
    long maxAge;

    /* cache hit vs. miss statistics */
    private long cacheHits = 0;
    private long cacheMisses = 0;

    private int count = 0;

    private boolean debug = false;



    /**<p>
     * Create a Cache with a specific Time-To-Live.
     *
     * @param maxAge - maximum amount of time, in milliseconds, that an object may stay in the cache
     * before it is considered "expired"</p>
     * @param sweepTime - period, in milliseconds, that the cache will be swept for expired objects.
     * A time less than 1 ms will use the default period instead.
     * @param loader - a CacheLoader object the caller provides to load objects that are <em>not</em>
     * found in the cache.
     */
    public Cache(long maxAge, long sweepTime, CacheLoader<K,E> loader)
    {
        this.cache = new java.util.concurrent.ConcurrentHashMap<K, CachedObject>();

        this.maxAge = maxAge;
        this.loader = loader;
        this.sweeper = new CacheSweeper(cache, maxAge, sweepTime);

        // Start the cache sweeper as a Daemon thread
        if (sweepTime != 0L)
        {
            String threadName = "CacheCleaner-" + (++count);
            Thread t = new Thread(sweeper, threadName);
            t.setDaemon(true);
            t.start();
        }
    }


    public E get(K key)
    {
        E found = null;

        CachedObject o = this.cache.get(key);

        if (o != null)
        {
            if (o.getAge() > this.maxAge) {
      System.out.println("cache HIT on aged-out item (counting it as a miss)");
                announce("cache HIT on aged-out item (counting it as a miss)");
                this.cache.remove(o);
                o = null;
            }
            else {
                announce("cache HIT!");
                found = o.getCachedItem();
            }
        }

        if (o == null)
        {
            announce("cache MISS");
            this.cacheMisses++;
            found = this.loader.load(key);
            this.cache.put(key, new CachedObject(found));
//            this.sweeper.notify(); // wake up the thread in case it's waiting /****Not Working****/
        }

        return found;
    }


    public E remove(K key)
    {
        E found = null;
        CachedObject o = this.cache.remove(key);
        if (o != null) {
            found = o.getCachedItem();
        }
        return found;
    }


    public void setDebug(boolean debug)
    {
        this.debug = debug;
    }


    public void showCacheStatistics()
    {
        long hits = this.cacheHits * 100L;
        long misses = this.cacheMisses * 100L;
        long total = cacheHits + cacheMisses;
        long pctHit = total > 0 ? hits / total : 0;
        long pctMiss = total > 0 ? misses / total : 0;
        announce("  Cache statistics -- Accesses: "+total+"  Hits: "+cacheHits+" ("+pctHit+"%)  Misses: "+cacheMisses+"("+pctMiss+"%)");
    }


    @Override
    public void finalize() throws Throwable
    {
        announce("--=| FINALIZING CACHE |=--");
        this.showCacheStatistics();
        super.finalize();
    }



    /*
     * Supporting objects
     * - the CachedObject which holds the actual object along with its timestamp.
     * - the CacheSweeper which periodically cleans expired objects from the cache.
     */


    /**
     * A {@link Cache} wraps its contained elements in a CachedObject, which
     * maintains statistical information about the item to allow the cache
     * implementation to make decisions regarding keeping or releasing its
     * contents based on the age, frequency of use, and access time of items.
     */
    private class CachedObject
    {
        private final long createTime = System.currentTimeMillis();
        private long lastAccessTime = createTime;
        private long accessCount = 0L;
        private E cachedThing;


        CachedObject(E o)
        {
            this.cachedThing = o;
        }


        /**
         * Get the age of the cached item.
         * Allows cache implementations to make caching decisions based on age of items.
         *
         * @return the age, in milliseconds since creation, of this cached item.
         */
        long getAge()
        {
            return System.currentTimeMillis() - this.createTime;
        }


        /**
         * Get the last access time of the cached item.
         * Allows cache implementations to make caching decisions based on
         * recent use, as in LRU caches.
         *
         * @return the system time, in milliseconds, when this cached item was last accessed.
         */
        @SuppressWarnings("unused")
        long getLastAccess()
        {
            return this.lastAccessTime;
        }


        /**
         * Get the number of times this cached item has been retrieved.
         * Allows cache implementations to make caching decisions based on
         * frequency of access.
         *
         * @return the count of times this cached item was accessed.
         */
        @SuppressWarnings("unused")
        long getAccessCount()
        {
            return this.accessCount;
        }


        /**
         * Get the actual object being cached.
         * Updates the access time and count.
         *
         * @return the cached element
         */
        E getCachedItem()
        {
            this.lastAccessTime = System.currentTimeMillis();
            accessCount++;
            return this.cachedThing;
        }


        void clear()
        {
            this.cachedThing = null;
            this.accessCount = 0;
            this.lastAccessTime = 0L;
        }


        @Override
        public void finalize()
        {
            this.clear();
        }
    }



    private class CacheSweeper implements Runnable
    {
        private static final long OVERHEAD = 1L;
        private static final long DEFAULT_SWEEP_PERIOD = 1L/*minute*/ * 60L/*seconds per minute*/ * 1000L/*millis per second*/ ;

        /** Reference to the cache that this is sweeping */
        Map<K,CachedObject> cache = null;
        /** Amount of time between sweeps, in milliseconds */
        long sweepPeriod = DEFAULT_SWEEP_PERIOD;
        long maxAge;

        /**
         * Create a cache "sweeper" that will periodically sweep the
         * provided cache and remove any expired objects.
         *
         * @param c The Cache to be swept
         * @param age Maximum amount of time, in milliseconds, that an object may stay in the cache
         * before it is discarded.
         * @param sweepTime Period, in milliseconds, that the cache will be swept for expired objects.
         * A time less than 1 ms will use the default period instead.
         */
        public CacheSweeper(Map<K,CachedObject> c, long age, long sweepTime)
        {
            this.cache = c;
            this.maxAge = age;
            if (sweepTime > 0) this.sweepPeriod = sweepTime; // else use the default
        }

        private boolean sweep()
        {
            if (this.cache.isEmpty()) return false;

            announce("---= * Sweeping Cache:");
            showCacheStatistics();
            int objCount = 0;
            int removeCount = 0;

            for (K key : this.cache.keySet())
            {
                objCount++;
                CachedObject item = this.cache.get(key);
                if (item.getAge() > this.maxAge)
                {
                    //Object o = item.getCachedItem(); // any reason we need to look at the cached object?

                    CachedObject removed = this.cache.remove(key);
                    if (removed == null)
                    {
                        // This should never happen
                        String msg = "Tried to remove an expired item but it wasn't found in the cache!";
                        announce("    !!    " + msg);
                        throw new IllegalStateException(msg);
                    }

                    // Make sure the reference to the real object is dropped immediately
                    // so we don't hold up garbage collection.
                    item.clear();
                    removeCount++;
                }
            }

            announce("---= * Sweeper removed " + removeCount + " of " + objCount + " items from Cache");
            return true;
        }

        @Override
        public void run()
        {
            sleep(sweepPeriod * 3); // sleep before initial start

            for/*ever*/ (;;)
            {
                /*boolean hasObjects =*/ sweep();
                sleep(sweepPeriod);
            /**** Not Working --- needs more thought for wait/notify to stop sweeping when cache is empty
                try {
                    if (hasObjects) this.wait(); // don't need to sweep if there's nothing left in the cache
                }
                catch (InterruptedException e) {
                    // don't care -- do nothing
                }
            ****/
            }
        }

        /** Sleep for the indicated time, and keep re-sleeping if interrupted. */
        private void sleep(long sleepTime)
        {
            long startTime = System.currentTimeMillis();
            long now = startTime;
            long endTime = startTime + sleepTime + OVERHEAD;
            long remainingTime = endTime - now;

            while (remainingTime > 0)
            {
                try {
                    Thread.sleep(remainingTime);
                }
                catch (InterruptedException e) {
                    // ignore it, we always compute remaining time to sleep in the finally block.
                }
                finally {
                    now = System.currentTimeMillis();
                    remainingTime = endTime - now;
                }
            }
        }
    }


    /* allow subclasses to override this */
    void announce(CharSequence msg)
    {
        if (debug) System.out.println(msg);
    }
}
