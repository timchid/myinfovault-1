package edu.ucdavis.myinfovault.data;

import java.util.Map;

/**
 * A section of records to be displayed on a page.
 * This class exists to make a set of records available to a JSP, along with some title/text information.
 *
 * @author Stephen Paulsen
 * @since MIV 2.0
 */
public class DisplaySection
{
    /**
     * The section "key" String. This is the <code>Name</code> field from the Section table.
     */
    private String key = null;

    /**
     * Record number of this section.
     * For "Additional Information" this is the ID of the AdditionalHeader record for the user;
     * for "normal" sections it is the section ID &mdash; the ID from the Section table.
     * It is initialized to negative one (-1) to indicate and invalid record number, but
     * that doesn't imply this DisplaySection is invalid; it might just not correspond to
     * any distinct record.
     */
    private int recordID = -1;

    /**
     * Text of the header to display for this section.
     */
    private String heading = null;

    /**
     * Text of the sub-heading to display for this section.
     * Provided for future use &mdash; we aren't currently (2007-11-06) using subheadings anywhere.
     */
    private String subhead = null;

    /**
     * Indicate whether or not this is one of the editable headers in the system.
     * For Additional Information the headers are always editable.
     */
    private boolean headerEditable = false;

    /**
     * Indicate whether or not this header should be displayed.
     * We always display headers in the UI, but users are allowed to suppress certain headers
     * when printing.
     */
    private boolean showHeader = true;

    /**
     * Style to use for the "Year" field &mdash; if there is a year field.
     * Some year fields are a single year, but some are ranges of years, i.e. "1997-2006"
     */
    private String yearstyle = "year";

    /**
     * The records that make up the content of this section.
     */
    private Map<String,Map<String,String>> records;


    public DisplaySection(String key, String heading, String subhead, Map<String,Map<String,String>> records)
    {
        this(key, heading, subhead, (String)null, records);
    }

    public DisplaySection(String key, String heading, String subhead, Map<String,Map<String,String>> records, String recordID)
    {
        this(key, heading, subhead, records, Integer.parseInt(recordID));
    }

    public DisplaySection(String key, String heading, String subhead, Map<String,Map<String,String>> records, int recordID)
    {
        this(key, heading, subhead, (String) null, records, recordID);
    }

    public DisplaySection(String key, String heading, String subhead, String yearstyle, Map<String,Map<String,String>> records)
    {
        // System.out.println(" ** Creating DisplaySection with key=["+key+"],
        // heading=["+heading+"], subhead=["+subhead+"]");
        this.key = key;
        this.heading = heading;
        this.subhead = subhead;
        if (yearstyle != null && yearstyle.length() > 0) {
            this.yearstyle = yearstyle;
        }
        this.records = records;
    }

    public DisplaySection(String key, String heading, String subhead, String yearstyle, Map<String,Map<String,String>> records, int recordID)
    {
        this(key, heading, subhead, yearstyle, records);
        this.recordID = recordID;
    }

    public DisplaySection setRecords(Map<String,Map<String,String>> records)
    {
        this.records = records;
        return this;
    }

    public Map<String,Map<String,String>> getRecords()
    {
        return this.records;
    }

    public String getKey()
    {
        return this.key;
    }

    public int getRecordNumber()
    {
        return recordID;
    }

    public int getSectionID()
    {
        return recordID;
    }

    public String getHeading()
    {
        return this.heading;
    }
    public void setHeading(String heading)
    {
        this.heading = heading;
    }

    public String getSubHeading()
    {
        return this.subhead;
    }

    /** An alias for {@link #getSubHeading()} */
    public String getSubheading()
    {
        return this.getSubHeading();
    }

    /** Return the CSS style that should be used for "year" fields. */
    public String getYearStyle()
    {
        return this.yearstyle;
    }

    /** Indicate whether or not this header should be editable in the UI. */
    public boolean isHeaderEditable()
    {
        return headerEditable;
    }

    public boolean getShowHeader()
    {
        return showHeader;
    }

    public void setShowHeader(boolean showHeader)
    {
        this.showHeader = showHeader;
    }

    /**
     * Set whether or not the header for this section should be editable.
     * Note package-private, NOT public access! Only the data classes such as SectionFetcher
     * and RecordHandler should be able to toggle the Editable state of the Header.
     * @param headerEditable
     */
    /*package*/ void setHeaderEditable(boolean headerEditable)
    {
        this.headerEditable = headerEditable;
    }

    /**
     * Set whether or not the header for this section should be displayed.
     * Note package-private, NOT public access! Only the data classes such as SectionFetcher
     * and RecordHandler should be able to toggle the Display state of the Header.
     * @param showHeader
     */
    /*package*/ void setHeaderVisible(boolean showHeader)
    {
        this.showHeader = showHeader;
    }

    @Override
    public String toString()
    {
        return "key: " + key +
               ", record number: " + recordID +
               ", heading: " + heading +
               (subhead != null && subhead.length() > 0 ? (", " + subhead) : "") +
               ", year CSS style: " + yearstyle +
               ", record count: " + records.size() +
               ", showHeader: " + showHeader +
               "";
    }
}
