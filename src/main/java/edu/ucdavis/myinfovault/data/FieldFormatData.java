/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: FieldFormatData.java
 */

package edu.ucdavis.myinfovault.data;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.ucdavis.mw.myinfovault.util.StringUtil;
import edu.ucdavis.myinfovault.MIVConfig;
import edu.ucdavis.myinfovault.MIVUserInfo;

/**
 * Handle fetching, updating records in the UserFormatPattern table.
 *
 * @author rhendric
 * @since MIV 2.0
 */
public class FieldFormatData
{
    private static final int PATTERN_IDX = 0;
    private static final int BOLD_IDX = 1;
    private static final int ITALIC_IDX = 2;
    private static final int UNDERLINE_IDX = 3;
    private static final int DATATYPE_IDX = 4;
    private static final int FIELDNAME_IDX = 5;
    private static final int DISPLAY_IDX = 6;
    private static final int DEFAULT_DOCUMENT_ID = 6;

    private static final String DOCUMENT_ID = "documentID";
    private static final String FIELD_NAME = "fieldName";
    public static final String FIELD_DESCRIPTION = "fieldDescription";
    private static final String PATTERN = "pattern";
    private static final String BOLD = "bold";
    private static final String ITALIC = "italic";
    private static final String UNDERLINE = "underline";
    private static final String RECID = "recID";
    private static final String ALL_FIELDS = "All Fields (listed above)";
    // ALLOW_ENTIRE_FIELD indicates whether or not the user will be allowed to select a format for this entire field.
    // A value of true will indicate that this field will be available for selection in the JSP page.
    public static final String ALLOW_ENTIRE_FIELD = "allowEntireField";
    // ALLOW_PATTERN indicates whether or not the user will be allowed to select a format pattern for this field.
    // A value of true will indicate that this field will be available for selection in the JSP page.
    public static final String ALLOW_PATTERN = "allowPattern";

    private static final String USER_FORMAT_PATTERN_QUERY =
            " SELECT ID, UserID, ifNull(Pattern,'') AS Pattern, DocumentID, FieldName, Bold, Italic, Underline" +
            " FROM UserFormatPattern" +
            " WHERE UserID = ? AND DocumentID=?" +
            " ORDER BY DocumentID, FieldName, Pattern"
            ;
//        new StringBuilder()
//        .append("SELECT fp.ID, fp.UserID, fp.Pattern, fp.DocumentID, fp.FieldName, fp.Bold, fp.Italic, fp.Underline")
//        .append(" FROM UserFormatPattern fp")
//        .append(" WHERE fp.UserID = ?")
//        .append(" ORDER BY fp.DocumentID, fp.FieldName, fp.Pattern")
//            .toString();
    // append("where fp.UserID = ? and TypeID = ?").toString();

    private MIVUserInfo userInfo = null;
    private int dataType = 0;
    private static Logger log = LoggerFactory.getLogger(FieldFormatData.class);

    // Map Field name to format and pattern options for the field.
    private LinkedHashMap<String, LinkedHashMap<String, String>> fieldFormatData = null;
    private DataMap fieldPatternMap = null;


    /**
     * Constuctor
     *
     * @param userInfo
     * @param dataType
     */
    public FieldFormatData(MIVUserInfo userInfo, int dataType)
    {
        if (this.userInfo != null && (this.userInfo.getPerson().getUserId() != userInfo.getPerson().getUserId()))
        {
            this.fieldFormatData = null;
        }
        this.userInfo = userInfo; // current user info
        this.dataType = dataType; // data type to retrieve
        this.setFieldFormatData();
    }


    /**
     * Get the publication format data for a user.
     */
    private void getData()
    {
        LinkedHashMap<String, String> formatOptions = null;
        LinkedHashMap<String, LinkedHashMap<String, String>> fieldDataMap = new LinkedHashMap<String, LinkedHashMap<String, String>>();

        ResultSet querySet = null;
        PreparedStatement statement = null;
        Connection connection = null;
        String fieldName = null;
        String formatPattern = null;
        int documentID = 0;

        // Maps field data to a map of formats
        this.fieldFormatData = new LinkedHashMap<String, LinkedHashMap<String, String>>();

        try
        {
            LinkedHashMap<String, LinkedHashMap<String, String>> fieldMap = getFieldMap();
            connection = this.userInfo.getRecordHandler().getConnection();
            statement = connection.prepareStatement(USER_FORMAT_PATTERN_QUERY);
            statement.setInt(1, this.userInfo.getPerson().getUserId());
            statement.setInt(2, this.dataType);
            querySet = statement.executeQuery();
            // Put the data into the map
            while (querySet.next())
            {
                // If the fieldName is null, it is the Author Field
                fieldName = (String) querySet.getObject("FieldName");
                if (fieldName == null || fieldName.trim().length() == 0)
                {
                    fieldName = "author";
                }

                // If the documentID is 0, default to 6 (publications) for now
                documentID = querySet.getInt("DocumentID");
                if (documentID <= 0)
                {
                    documentID = DEFAULT_DOCUMENT_ID;
                }

                formatPattern = querySet.getString("Pattern");

                // Don't add to the map if no pattern and field is not to be formatted
                if (fieldMap.get(fieldName.toLowerCase()).get(ALLOW_ENTIRE_FIELD).equalsIgnoreCase("false") &&
                        (formatPattern == null || formatPattern.length() == 0))
                {
                    continue;
                }

                // Don't add to the map if there is a pattern and pattern is not to be formatted
                if (fieldMap.get(fieldName.toLowerCase()).get(ALLOW_PATTERN).equalsIgnoreCase("false") &&
                        (formatPattern != null && formatPattern.length() > 0))
                {
                    continue;
                }

                formatOptions = new LinkedHashMap<String, String>();
                formatOptions.put(DOCUMENT_ID, Integer.toString(documentID));
                formatOptions.put(FIELD_NAME, fieldName.toLowerCase());
                if (fieldName.equalsIgnoreCase("all"))
                {
                    formatOptions.put(FIELD_DESCRIPTION, ALL_FIELDS);
                }
                else
                {
                    formatOptions.put(FIELD_DESCRIPTION, fieldMap.get(fieldName.toLowerCase()).get(FIELD_DESCRIPTION));
                }

                // Handle any of these special characters which may be present
                // in the pattern
//                formatPattern = formatPattern.replaceAll("\u201c", "&#8220;").
//                replaceAll("\u201d", "&#8221;").
//                replaceAll("\u2022", "&#8226;").
//                replaceAll("\u2020", "&#8224;").
//                replaceAll("\u2122", "&#8482;").
//                replaceAll("\u20ac", "&#8364;").
//                replaceAll("\u0178", "&#376;");

//                formatPattern = MIVUtil.greekToRef(formatPattern);

                // Escape any markup wich may be present in the pattern.
                formatPattern = MIVUtil.escapeMarkup(formatPattern);
                formatOptions.put(PATTERN, formatPattern);
                formatOptions.put(BOLD, Integer.toString(querySet.getInt("Bold")));
                formatOptions.put(ITALIC, Integer.toString(querySet.getInt("Italic")));
                formatOptions.put(UNDERLINE, Integer.toString(querySet.getInt("Underline")));
                formatOptions.put(RECID, Integer.toString(querySet.getInt("ID")));

                // build a temp map of fields without patterns for check
                String pattern = formatOptions.get(PATTERN);
                if (pattern == null || pattern.trim().length() == 0)
                {
                    fieldDataMap.put(formatOptions.get(FIELD_NAME), formatOptions);
                }
                else
                {
                    // If the field has a pattern, add to the map instance map
                    this.fieldFormatData.put(formatOptions.get(RECID), formatOptions);
                }
            }

            // Iterate through the field data map just built for two reasons.
            // 1. Build a data map in field order as defined by the fieldMap.
            // 2. Make sure all fields are represented in the map.
            // Fields to which patterns are applied are optional, however
            // all other fields must be represented in the map which will
            // be available to the JSP page.
            int recCount = 0;
            // Iterate through the field map.
            for (String field : fieldMap.keySet())
            {
                // If an allowed field is not there, one needs to be present without a pattern
                // This is done so that the field will be represented on the JSP
                // page...a placeholder.
                if (!fieldDataMap.containsKey(field) && fieldMap.get(field).get(ALLOW_ENTIRE_FIELD).equalsIgnoreCase("true"))
                {
                    formatOptions = new LinkedHashMap<String, String>();
                    formatOptions.put(DOCUMENT_ID, String.valueOf(dataType));
                    formatOptions.put(FIELD_NAME, field.toLowerCase());
                    formatOptions.put(FIELD_DESCRIPTION, fieldMap.get(field.toLowerCase()).get(FIELD_DESCRIPTION));
                    formatOptions.put(PATTERN, "");
                    formatOptions.put(BOLD, "0");
                    formatOptions.put(ITALIC, "0");
                    formatOptions.put(UNDERLINE, "0");
                    formatOptions.put(RECID, "new" + recCount++);
                }
                else
                {
                    formatOptions = fieldDataMap.get(field);
                }
                // Add the formatoptions for this field to the map
                if (formatOptions != null) {
                    this.fieldFormatData.put(formatOptions.get(RECID), formatOptions);
                }
            }
        }
        catch (SQLException e)
        {
//            log.finer(new StringBuilder("SQLException: ").append(e.getErrorCode()).toString());
            log.trace("SQLException: " + e.getErrorCode(), e);
//            e.printStackTrace();
        }
        finally
        {
            try
            {
                if (querySet != null)
                {
                    querySet.close();
                }
                if (statement != null)
                {
                    statement.close();
                }
                this.userInfo.getRecordHandler().releaseConnection(connection);
            }
            catch (SQLException e)
            {
                // do nothing
            }
        }
    }
    // getData()


    /**
     * Build a hash map of field patterns from the UserFormatPattern table. The
     * top level key in the hash is the DocumentID:FieldName which is mapped to
     * the various patterns to match against a field. A field may have more than
     * one pattern and each pattern will have a map of style attributes which
     * are to be applied to the pattern for the designated field. If there is no
     * pattern present, the style attributes apply to the entire field rather
     * than a pattern within the field.
     *
     * DocumentID : Field FieldName Pattern Style
     */
    public void buildFieldPatternMap()
    {
        LinkedHashMap<String, String> formatOptions = null;

        // Map of fields by document type to patterns and formats
        this.fieldPatternMap = new DataMap();

        for (String recID : this.fieldFormatData.keySet())
        {
            // Skip "new" records. They only exist as placeholders
            if (recID.startsWith("new"))
            {
                continue;
            }
            formatOptions = this.fieldFormatData.get(recID);
            addToFieldMap(formatOptions);

            // Have to handle "contributor" identically to "author" so add an
            // entry to the field pattern map for the contributor with the
            // author pattern fields...if the author field exists.
            String documentID = formatOptions.get(DOCUMENT_ID);
            DataMap fields = (DataMap) this.fieldPatternMap.get(documentID + ":" + "author");
            if (fields != null)
            {
                this.fieldPatternMap.put(documentID + ":" + "contributor", fields);
            }
        }
        // Sort the pattern maps for each field
        for (String type : this.fieldPatternMap.keySet())
        {
            DataMap fields = (DataMap) this.fieldPatternMap.get(type);
            for (String field : fields.keySet())
            {
                DataMap patterns = (DataMap) fields.get(field);
                fields.put(field, sortFieldPatterns(patterns));
            }
        }

        if (log.isDebugEnabled())
        {
            System.out.println("FieldFormatData: buildFieldPatternMap");
            for (String type : this.fieldPatternMap.keySet())
            {
                System.out.println("DocumentType: " + type);
                DataMap fields = (DataMap) this.fieldPatternMap.get(type);
                for (String field : fields.keySet())
                {
                    System.out.println(" Field: " + field);
                    DataMap patterns = (DataMap) fields.get(field);
                    for (String pat : patterns.keySet())
                    {
                        System.out.println("  Pattern: " + pat);
                        DataMap formats = (DataMap) patterns.get(pat);
                        for (String format : formats.keySet())
                        {
                            System.out.println("   Format: " + format + "=" + formats.get(format));
                        }
                    }
                }
            }
        }// debug
    }
    // buildFieldPatternMap()


    /**
     * Add a field to the field pattern map
     *
     * @param formatOptions
     */
    private void addToFieldMap(LinkedHashMap<String, String> formatOptions)
    {
        LinkedHashMap<String, LinkedHashMap<String, String>> fieldMap = getFieldMap();
        DataMap fields = null;
        DataMap patterns = null;

        // get the current patternType key: DocumentID:FieldName
        String fieldName = formatOptions.get(FIELD_NAME);
        String documentID = formatOptions.get(DOCUMENT_ID);
        String pattern = formatOptions.get(PATTERN);

        // New style map for each record
        // Get the pattern and format options into the style map
        // for this record
        DataMap styleMap = new DataMap();
        styleMap.put(BOLD, formatOptions.get(BOLD));
        styleMap.put(ITALIC, formatOptions.get(ITALIC));
        styleMap.put(UNDERLINE, formatOptions.get(UNDERLINE));

        // If all fields, loop through add a mapping for each field
        if (fieldName.equalsIgnoreCase("all"))
        {
            for (String field : fieldMap.keySet())
            {
                for (String f : StringUtil.splitToArray(field))
                {
                    formatOptions.put(FIELD_NAME, f);
                    addToFieldMap(formatOptions);
                }
            }
        }
        // Add the pattern to the map for the current field
        else
        {
            String patternTypeKey = "";
            for (String field : StringUtil.splitToArray(fieldName))
            {
                patternTypeKey = new StringBuilder(documentID).append(":").append(field).toString();

                // If the field is not in the map, add it
                if (!this.fieldPatternMap.containsKey(patternTypeKey))
                {
                    // A new field and pattern map for this data type
                    fields = new DataMap();
                    patterns = new DataMap();
                    fields.put(field, patterns);
                    this.fieldPatternMap.put(patternTypeKey, fields);
                }

                // Get the field map for this field
                fields = (DataMap) this.fieldPatternMap.get(patternTypeKey);
                patterns = (DataMap) fields.get(field);
                // Map the pattern to the style to be applied
                // Note that no pattern indicates the styles
                // are to be applied to the entire field
                if (pattern == null || pattern.trim().length() == 0)
                {
                    pattern = "";
                }

                // Apply the pattern map to the current field
                if (patterns.containsKey(pattern))
                {
                    // If the pattern is already there, merge the style maps
                    DataMap currentStyleMap = (DataMap) patterns.get(pattern);
                    for (String currentStyle : currentStyleMap.keySet())
                    {
                        String value = (String) styleMap.get(currentStyle);
                        // Only set value would be change to on ("1")
                        if (value.equals("1"))
                        {
                            currentStyleMap.put(currentStyle, value);
                        }
                    }
                }
                else
                {
                    patterns.put(pattern, styleMap);
                }
            }
        }
    }
    // addToFieldMap()


    /**
     * Sort the patterns for each field, longest pattern first
     *
     * @param unsortedPatternMap
     * @return sortedPatternMap
     */
    private DataMap sortFieldPatterns(DataMap unsortedPatternMap)
    {
        ArrayList<String> patternList = new ArrayList<String>();
        DataMap sortedPatternMap = new DataMap();
        for (String pat : unsortedPatternMap.keySet())
        {
            patternList.add(pat);
        }

        // Sort the array by the lengths of the strings
        Comparator<String> comp = new Comparator<String>() {
            @Override
            public int compare(String s1, String s2)
            {
                return s2.length() - s1.length();
            }
        };
        Collections.sort(patternList, comp);

        // Rebuild the map
        for (String pat : patternList)
        {
            sortedPatternMap.put(pat, unsortedPatternMap.get(pat));
        }

        return sortedPatternMap;
    }


    /**
     * Get the fields available to format into a comma delimited string. The
     * "key" is actual field name which is mapped to the field description for
     * display in a pulldown list.
     */
    public String getSelectionFields()
    {
        LinkedHashMap<String, LinkedHashMap<String, String>> fieldMap = getFieldMap();
        StringBuilder fields = new StringBuilder("-- Select A Field --,");

        for (String field : fieldMap.keySet())
        {
            if (fieldMap.get(field).get(ALLOW_PATTERN).equalsIgnoreCase("true"))
            {
                fields.append(fieldMap.get(field).get(FIELD_DESCRIPTION)).append(",");
            }
        }
//        if (fields.toString().split(",").length > 1)
//        {
//            fields.append(ALL_FIELDS);
//        }
//        else
//        {
            fields = fields.deleteCharAt(fields.length()-1);
//        }
        return fields.toString();
    }


    /**
     * Initialize the fieldFormatData if not already done
     */
    private void setFieldFormatData()
    {
        if (this.fieldFormatData == null)
        {
            log.debug("Reloading field format data!");
            this.getData();
            this.buildFieldPatternMap();
        }
        else
        {
            log.debug("Skip field format data!");
        }
    }




    /**
     * Initialize the map of field names. The order set here is maintained when
     * the descriptions are displayed on the JSP page.
     * @return
     */
    private LinkedHashMap<String, LinkedHashMap<String, String>> getFieldMap()
    {
        // Initialize the field Map
        if (getFormatFieldsMapByDataTypeId(this.dataType) != null)
        {
            return getFormatFieldsMapByDataTypeId(this.dataType);
        }
        else
        {
            return new LinkedHashMap<String, LinkedHashMap<String, String>>();
        }
    }
    // setFieldMap()


    /**
     * To get the FieldMap from MIVConfig's FormatFieldsMap
     * @param dataType
     * @return
     */
    public static LinkedHashMap<String, LinkedHashMap<String, String>> getFormatFieldsMapByDataTypeId(int dataType)
    {
        LinkedHashMap<Integer, LinkedHashMap<String, LinkedHashMap<String, String>>> formatFieldsMap = MIVConfig.getConfig().getFormatFieldsMap();
        if (formatFieldsMap != null && formatFieldsMap.get(dataType) != null)
        {
            return formatFieldsMap.get(dataType);
        }

        return null;
    }


    /**
     * Get the fieldFormatData LinkedHashMap
     *
     * @return fieldFormatData
     */
    public LinkedHashMap<String, LinkedHashMap<String, String>> getFieldFormatData()
    {
        this.setFieldFormatData();
        return this.fieldFormatData;
    }


    /**
     * Get the fieldPattermMap LinkedHashMap
     *
     * @return fieldPatternMap
     */
    public DataMap getFieldPatternMap()
    {
        this.setFieldFormatData();
        return this.fieldPatternMap;
    }


    /**
     * Reloads the field format data
     */
    public void reloadFieldFormatData()
    {
        this.fieldFormatData = null;
        this.setFieldFormatData();
    }


    /**
     * TODO - This function is pretty ugly and needs refactoring where possible
     *
     * @param fieldData 
     * @param dataType 
     * @return 
     *
     */
    public int update(LinkedHashMap<String, String> fieldData, String dataType)
    {
        // Get a record handler
        RecordHandler rh = this.userInfo.getRecordHandler();
        String[] fields = { "Pattern", "Bold", "Italic", "Underline", "DocumentID", "FieldName", "Display" };
        String[] values = new String[fields.length];
        int recsUpdated = -1;    // changes to save
        LinkedHashMap<String, LinkedHashMap<String, String>> dupCheckMap = new LinkedHashMap<String, LinkedHashMap<String, String>>();
        LinkedHashMap<String, String> formatMap = new LinkedHashMap<String, String>();
        boolean delete = false;
        String fieldName = null;
        String formatPattern = null;
        String bold = null;
        String italic = null;
        String underline = null;
        String record = null;
        int fieldCntr = 0;

        LinkedHashMap<String, LinkedHashMap<String, String>> fieldMap = getFieldMap();

        // Format options are for fields are in the form df1:bold
        // Where the value on the left of the colon is the field
        // field designator ("df1") and the value on the right is the
        // the specific formatting option/pattern ("bold")
        // Note that the designator without a value is the field name

        // Get all of the field designators
        // df1 - dfn for data fields
        // pf1 - pfn for pattern fields
        LinkedHashMap<String, String> fieldParamNames = new LinkedHashMap<String, String>();
        for (String key : fieldData.keySet())
        {
            // Split out the field designator
            String[] field = key.split(":");
            if (!fieldParamNames.containsKey(field[0]))
            {
                fieldParamNames.put(field[0], field[0]);
            }
        }

        // Iterate through each field parameter
        // The designator without a value is the field name
        for (String rec : fieldParamNames.keySet())
        {
            delete = false;
            // Build the key for each of the parameters the record id
            String recIDkey = new StringBuilder(rec).append(":recID").toString();
// FIXME: Why isn't recIDkey etc. simply done as a String concatenation?
//   e.g.   String recIDkey = rec + ":recID";
            String patternKey = new StringBuilder(rec).append(":pattern").toString();
            String boldKey = new StringBuilder(rec).append(":bold").toString();
            String italicKey = new StringBuilder(rec).append(":italic").toString();
            String underlineKey = new StringBuilder(rec).append(":underline").toString();
            String dupKey = null;

            // Get each of the parameter values
            fieldName = fieldData.get(rec + ":");
            formatPattern = fieldData.get(patternKey);
            bold = fieldData.get(boldKey);
            italic = fieldData.get(italicKey);
            bold = fieldData.get(boldKey);
            underline = fieldData.get(underlineKey);
            record = fieldData.get(recIDkey);

            fieldName = (fieldName == null) ? Integer.toString(fieldCntr++) : fieldName.trim();
            formatPattern = (formatPattern == null) ? "" : formatPattern.trim();

            // Convert Greek characters to numeric entities
            //formatPattern = MIVUtil.charToRef(formatPattern);

            formatPattern = MIVUtil.replaceNumericReferences(formatPattern);

            dupKey = new StringBuilder(fieldName.trim()).append(":").append(formatPattern.trim()).toString();

            // If this is a pattern field "pf" with no pattern, delete
            if (formatPattern.length() == 0 && rec.toLowerCase().startsWith("pf"))
            {
                delete = true;
            }
            // If there are no formatting options, don't save/delete.
            else if (bold == null && italic == null && underline == null)
            {
                delete = true;
            }

            // Check for duplicate fields with the same pattern
            if (dupCheckMap.containsKey(dupKey))
            {
                formatMap = dupCheckMap.get(dupKey);
                String tempRecord = formatMap.get("record");
                // See if there is already a record associated and use it
                if (tempRecord != null && !tempRecord.startsWith("new"))
                {
                    if (record != null && !record.startsWith("new"))
                    {
                        // If the current field has a record associated,  delete
                        recsUpdated = rh.delete("UserFormatPattern", Integer.parseInt(record));
                    }
                    record = formatMap.get("record");    // get the record
                    delete = formatMap.get("delete").equalsIgnoreCase("yes");// ? true : false;    // get the current delete flag for the record
                }
            }
            else
            {
                // Add the dup key for the next time through
                formatMap = new LinkedHashMap<String, String>();
                dupCheckMap.put(dupKey, formatMap);
            }

            formatMap.put("delete", delete ? "yes" : "no");
            formatMap.put("record", record);
            formatMap.put(FIELD_NAME, fieldName);
            formatMap.put(PATTERN, formatPattern);
            if (bold != null)
            {
                formatMap.put(BOLD, bold);
            }
            if (italic != null)
            {
                formatMap.put(ITALIC, italic);
            }
            if (underline != null)
            {
                formatMap.put(UNDERLINE, underline);
            }
        }

        for (String fieldKey : dupCheckMap.keySet())
        {
            formatMap = dupCheckMap.get(fieldKey);
            delete = formatMap.get("delete").equalsIgnoreCase("yes") ? true : false;
            record = formatMap.get("record");
            fieldName = formatMap.get(FIELD_NAME);
            formatPattern = formatMap.get(PATTERN);
            bold = formatMap.get(BOLD);
            italic = formatMap.get(ITALIC);
            underline = formatMap.get(UNDERLINE);
            // Delete flag set for new record...skip
            if (delete && (record == null || record.startsWith("new")))
            {
                continue;
            }


            if (!delete)
            {
                // Check that the column name is returned in the fieldMap
                // If it's not there, then the description or "select" was
                // returned, get the column name associated with
                // the description. This happens for records with fields
                // selected from the pull down list.
                // The description is used in the pull down, but we store the
                // actual column name in the database.
                //
                if (fieldName.toLowerCase().equalsIgnoreCase(ALL_FIELDS))
                {
                    fieldName = "all";
                }
                else if (!fieldMap.containsKey(fieldName.toLowerCase()))
                {
                    delete = true;
                    for (String field : fieldMap.keySet())
                    {
                        if (fieldMap.get(field).get(FIELD_DESCRIPTION).equalsIgnoreCase(fieldName))
                        {
                            fieldName = field;
                            delete = false;
                        }
                    }
                }

                // Set the values to be updated
                values[PATTERN_IDX] = formatPattern;
                values[BOLD_IDX] = (bold != null) ? "1" : "0";
                values[ITALIC_IDX] = (italic != null) ? "1" : "0";
                values[UNDERLINE_IDX] = (underline != null) ? "1" : "0";
                values[DATATYPE_IDX] = dataType;
                values[FIELDNAME_IDX] = fieldName.toLowerCase();
                values[DISPLAY_IDX] = "1"; // Default for display
            }

            // Update or delete an existing record
            if (record != null && !record.startsWith("new"))
            {
                if (delete)
                {
                    recsUpdated = rh.delete("UserFormatPattern", Integer.parseInt(record));
                }
                else
                {
                    // Check if any fields in this record were updated
                    LinkedHashMap<String, String>fieldRecord = this.fieldFormatData.get(record);
                    if (!fieldRecord.get(FIELD_NAME).equals(values[FIELDNAME_IDX]) ||
                        !fieldRecord.get(PATTERN).equals(MIVUtil.escapeMarkup(values[PATTERN_IDX])) ||
                        !fieldRecord.get(BOLD).equals(values[BOLD_IDX])       ||
                        !fieldRecord.get(ITALIC).equals(values[ITALIC_IDX])   ||
                        !fieldRecord.get(UNDERLINE).equals(values[UNDERLINE_IDX]))
                    {
                        if(StringUtils.isEmpty(values[PATTERN_IDX]))
                        {
                            values[PATTERN_IDX] = null;
                        }
                        recsUpdated = rh.update("UserFormatPattern", Integer.parseInt(record), fields, values);
                    }
                }
            }
            else
            {
                // New record
                if (!delete)
                {
                    if (StringUtils.isEmpty(values[PATTERN_IDX]))
                    {
                        values[PATTERN_IDX] = null;
                    }
                    recsUpdated = rh.insert("UserFormatPattern", fields, values);
                }
            }
        }
        if (recsUpdated > 0)
        {
            this.reloadFieldFormatData();
        }
        return recsUpdated;
    }
    // update()
}
