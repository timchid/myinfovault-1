/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: EvaluationValidation.java
 */
package edu.ucdavis.myinfovault.data.validation;

import java.util.Map;

import edu.ucdavis.mw.myinfovault.valuebeans.ResultVO;
import edu.ucdavis.myinfovault.data.Validator;

/**
 * To provide validations to List of Evaluations application.<br><br>
 *
 * (SDP) <strong>I don't understand the point of this custom validation class.
 * It validates two fields,</strong> <u>Enrollment</u> and <u>Responses</u>, <strong>checking
 * only for a value greater than zero and limiting length to four,
 * <em>which can be done with standard validation</em> in the config file,
 * using</strong> <code>int:0,length:4</code> <strong>just as is done here.</strong><br>
 *
 * @author Pradeep Haldiya
 * @since MIV 4.6.2
 */
public class EvaluationValidation extends BaseValidation
{
    /**
     * Validate total enrollment
     * @param enrollment
     * @param reqData
     * @return
     */
    public ResultVO validateEnrollment(String enrollment, Map<String, String[]> reqData)
    {
        Validator validator = new Validator(reqData);
        validator.validate("enrollment", enrollment, "int:0,length:4");

        if (!validator.isValid())
        {
            return new ResultVO(false, validator.getErrorMessageList());
        }

        return new ResultVO(true).setValue(enrollment != null && enrollment.length() > 0 ? enrollment : null);
    }

    /**
     * Validate total responses
     * @param totalResponses
     * @param reqData
     * @return
     */
    public ResultVO validateResponses(String totalResponses, Map<String, String[]> reqData)
    {
        Validator validator = new Validator(reqData);
        validator.validate("responsetotal", totalResponses, "int:0,length:4");

        if (!validator.isValid())
        {
            return new ResultVO(false, validator.getErrorMessageList());
        }

        return new ResultVO(true).setValue(totalResponses != null && totalResponses.length() > 0 ? totalResponses : null);
    }

}
