package edu.ucdavis.myinfovault.data;

//import java.util.Date;
import java.util.List;

/**
 * This is simple java class the store all the personal information, which contains address, phone, email, url.
 * this custom object contains lists of two other custom objects ,Address and Phone object, which are used to store
 * part of personal information.   
 * 
 * @author Venkat Vegesna
 */
public class PersonalData
{
    private String displayName;
    private String employeeID;
    private List<Address> addresses;   // we use this addresses list to store various kinds of address. 
    private List<Phone> phonesList; // we  use this phones list store various phone numbers.
    //private Phone mobilePhone;
    private String website;
    private String websiteRecID;
    private String email;
    private String emailRecID;
    private String birthDate;

    /* *** CITIZENSHIP RELATED INFORMATION ********************* */
    private boolean fUSCitizen = true;
    private String dateEntered;
    private String visaType;
    private int recordID = 0;

    public PersonalData()
    {
        // do nothing, allow recordID to be 0 to indicate a new record
    }

    public PersonalData(int recordID)
    {
        this.recordID = recordID;
    }

/*    public Phone getMobilePhone()
    {
        return mobilePhone;
    }

    public void setMobilePhone(Phone mobilePhone)
    {
        this.mobilePhone = mobilePhone;
    }
*/
    public String getBirthDate()
    {
        return birthDate;
    }

    public void setBirthDate(String dob)
    {
        if (dob != null) dob = dob.replaceAll("\"", "&quot;");
        this.birthDate = dob;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        if (email != null) email = email.replaceAll("\"", "&quot;");
        this.email = email;
    }

    public String getEmployeeID()
    {
        return employeeID;
    }

    public void setEmployeeID(String employeeID)
    {
        this.employeeID = employeeID;
    }

    public String getDisplayName()
    {
        return displayName;
    }

    public void setDisplayName(String name)
    {
        if (name != null) name = name.replaceAll("\"", "&quot;");
        this.displayName = name;
    }

    public String getWebsite()
    {
        return website;
    }

    public void setWebsite(String url)
    {
        if (url != null) url = url.replaceAll("\"", "&quot;");
        this.website = url;
    }

    public String getDateEntered()
    {
        return dateEntered;
    }

    public void setDateEntered(String dateEntered)
    {
        if (dateEntered != null) dateEntered = dateEntered.replaceAll("\"", "&quot;");
        this.dateEntered = dateEntered;
    }

    public int getRecordID()
    {
        return recordID;
    }

// Don't allow setting the record ID --
// it's either zero (new record), or set with the constructor (existing record), but
// once set it's not allowed to be changed.
//    public void setRecordID(int personalInfoRecordID)
//    {
//        this.recordID = personalInfoRecordID;
//    }

    public boolean isUSCitizen()
    {
        return fUSCitizen;
    }

    public void setUSCitizen(boolean isUSCitizen)
    {
        this.fUSCitizen = isUSCitizen;
    }

    public void setVisaType(String visaType)
    {
        if (visaType != null) visaType = visaType.replaceAll("\"", "&quot;");
        this.visaType = visaType;
    }

    public String getVisaType()
    {
        return visaType;
    }

    public String getEmailRecID()
    {
        return emailRecID;
    }

    public void setEmailRecID(String emailRecID)
    {
        this.emailRecID = emailRecID;
    }

    public String getWebsiteRecID()
    {
        return websiteRecID;
    }

    public void setWebsiteRecID(String urlRecID)
    {
        this.websiteRecID = urlRecID;
    }

    public List<Address> getAddresses()
    {
        return addresses;
    }

    public void setAddresses(List<Address> addresses)
    {
        this.addresses = addresses;
    }

    public List<Phone> getPhonesList()
    {
        return phonesList;
    }

    public void setPhonesList(List<Phone> phonesList)
    {
        this.phonesList = phonesList;
    }

}
