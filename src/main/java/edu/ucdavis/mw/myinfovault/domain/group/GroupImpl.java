/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: GroupImpl.java
 */

package edu.ucdavis.mw.myinfovault.domain.group;

import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import org.springframework.util.StringUtils;

import edu.ucdavis.mw.myinfovault.service.person.AssignedRole;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.mw.myinfovault.util.StringUtil;

/**
 * The MIV implementation of a group.
 *
 * @author Craig Gilmore
 * @since 4.3
 */
public class GroupImpl implements Serializable, Comparable<Group>, Group
{
    private static final long serialVersionUID = 201108051200L;

    private int id;
    private String name;
    private String description;
    private GroupType type;
    private MivPerson creator;
    private Date created;
    private Date updated;
    private boolean readOnly;
    private Boolean hidden;//needs to be three state as this is allowed to be initially null
    private boolean confidential;
    private boolean active;
    private Set<AssignedRole> assignedRoles = new HashSet<AssignedRole>();
    private Set<MivPerson> inactive = new HashSet<MivPerson>();
    private SortedSet<MivPerson> members = new TreeSet<MivPerson>(new MemberComparator());

    /**
     * Create a new group object with default values.
     *
     * @param creator The MivPerson that is creating the group
     * @throws IllegalArgumentException
     */
    public GroupImpl(MivPerson creator) throws IllegalArgumentException
    {
        this("",
             "",
             GroupType.GROUP,
             creator,
             false,
             null,
             false,
             true,
             Collections.<AssignedRole>emptyList(),
             Collections.<MivPerson>emptyList(),
             Collections.<MivPerson>emptyList());
    }

    /**
     * Create a group object.
     *
     * @param name The name given to the group
     * @param description The description given to the group
     * @param type The type assigned to the group
     * @param creator The MivPerson that originally created the group
     * @param readOnly Can the group be edited by someone other than the creator?
     * @param hidden Can the group be viewed by someone other than the owner? Can be <code>null</code> for new groups
     * @param confidential May the group's membership be viewed by someone other than the creator?
     * @param active Can the group be used?
     * @param assignedRoles The role-scopes of which a user must have at least one in order to access the group
     * @param inactive The subset of MivPersons in the group that are not active
     * @param members The collection of MivPersons that comprise the group
     * @throws IllegalArgumentException
     */
    private GroupImpl(String name,
                     String description,
                     GroupType type,
                     MivPerson creator,
                     boolean readOnly,
                     Boolean hidden,
                     boolean confidential,
                     boolean active,
                     Collection<AssignedRole> assignedRoles,
                     Collection<MivPerson> inactive,
                     Collection<MivPerson> members)
    throws IllegalArgumentException
    {
        this(0,
             name,
             description,
             type,
             creator,
             null,
             null,
             readOnly,
             hidden,
             confidential,
             active,
             assignedRoles,
             inactive,
             members);
    }

    /**
     * Create a group object.
     *
     * @param id The record ID of the group
     * @param name The name given to the group
     * @param description The description given to the group
     * @param type The type assigned to the group
     * @param creator The MivPerson that originally created the group
     * @param created The date-time the group was created
     * @param updated The date-time the group was last updated
     * @param readOnly Can the group be edited by someone other than the creator?
     * @param hidden Can the group be viewed by someone other than the owner? Can be <code>null</code> for new groups
     * @param confidential May the group's membership be viewed by someone other than the creator?
     * @param active Can the group be used?
     * @param assignedRoles The role-scopes of which a user must have at least one in order to access the group
     * @param inactive The subset of MivPersons in the group that are not active
     * @param members The collection of MivPersons that comprise the group
     * @throws IllegalArgumentException
     */
    public GroupImpl(int id,
                     String name,
                     String description,
                     GroupType type,
                     MivPerson creator,
                     Date created,
                     Date updated,
                     boolean readOnly,
                     Boolean hidden,
                     boolean confidential,
                     boolean active,
                     Collection<AssignedRole> assignedRoles,
                     Collection<MivPerson> inactive,
                     Collection<MivPerson> members)
    throws IllegalArgumentException
    {
        //populate group attributes
        this.id = id;
        this.name = name;
        this.description = description;
        this.type = type;
        this.creator = creator;
        this.created = created;
        this.updated = updated;
        this.readOnly = readOnly;
        this.hidden = hidden;
        this.confidential = confidential;
        this.active = active;

        //populate group roles and members unless given null
        if (assignedRoles != null) this.assignedRoles.addAll(assignedRoles);
        if (inactive != null) this.inactive.addAll(inactive);
        if (members != null) this.members.addAll(members);

        //enforce that inactive must be a subset of members
        this.inactive.retainAll(this.members);

        //enforce that review groups may not be readOnly
        this.readOnly = this.readOnly
                     && this.type == GroupType.GROUP;

        //enforce that review groups may not be private
        if (this.hidden != null)
        {
            this.hidden = this.hidden
                       && this.type == GroupType.GROUP;
        }

        //check group parameters
        if (this.name == null
         || this.description == null
         || this.type == null
         || this.creator == null
         || this.name.length() > 25
         || !StringUtils.hasText(this.description) && this.description.length() > 100)
        {
            throw new IllegalArgumentException("The following group attributes may not be null: " +
                                               "name, description, type, creator, inactive, members, assigned roles" + this);
        }

        //check group parameters for existing groups
        if ( this.id > 0
           && ( this.hidden == null
             || this.created == null
             || this.updated == null))
        {
            throw new IllegalArgumentException("The following group attributes may not be null for existing groups: " +
                                               "hidden, created, updated." + this);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getId()
    {
        return id;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setId(int id)
    {
        this.id = id;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getName()
    {
        return name;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setName(String name)
    {
        this.name = name;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getDescription()
    {
        return description;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setDescription(String description)
    {
        this.description = description;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public GroupType getType()
    {
        return type;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setType(GroupType type)
    {
        this.type = type;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MivPerson getCreator()
    {
        return creator;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCreator(MivPerson creator)
    {
        this.creator = creator;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Date getCreated()
    {
        return created;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCreated(Date created)
    {
        this.created = created;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Date getUpdated()
    {
        return updated;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setUpdated(Date updated)
    {
        this.updated = updated;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isReadOnly()
    {
        return readOnly;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setReadOnly(boolean readOnly)
    {
        this.readOnly = readOnly;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Boolean getHidden()
    {
        return hidden;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setHidden(Boolean hidden)
    {
        this.hidden = hidden;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isConfidential()
    {
        return confidential;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setConfidential(boolean confidential)
    {
        this.confidential = confidential;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isActive()
    {
        return active;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setActive(boolean active)
    {
        this.active = active;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Set<AssignedRole> getAssignedRoles()
    {
        return assignedRoles;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setAssignedRoles(Set<AssignedRole> assignedRoles)
    {
        this.assignedRoles = assignedRoles;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Set<MivPerson> getInactive()
    {
        return inactive;
    }

    @Override
    public Map<Integer, MivPerson> getInactiveMap()
    {
        Map<Integer, MivPerson> inactiveMap = new HashMap<Integer, MivPerson>();

        for (MivPerson inactivePerson : inactive)
        {
            inactiveMap.put(inactivePerson.getUserId(), inactivePerson);
        }

        return inactiveMap;
    }

    /*
     * Necessary for JSON serialization performed by
     * org.springframework.web.servlet.view.json.writer.sojo.SojoJsonStringWriter
     * {@see /config/json-views.xml}
     */
    @SuppressWarnings("unused")
    private void setInactive(Set<MivPerson> inactive)
    {
        this.inactive = inactive;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SortedSet<MivPerson> getMembers()
    {
        return members;
    }

    @Override
    public Map<Integer, MivPerson> getMemberMap()
    {
        Map<Integer, MivPerson> memberMap = new HashMap<Integer, MivPerson>();

        for (MivPerson member : members)
        {
            memberMap.put(member.getUserId(), member);
        }

        return memberMap;
    }

    /*
     * Necessary for JSON serialization performed by
     * org.springframework.web.servlet.view.json.writer.sojo.SojoJsonStringWriter
     * {@see /config/json-views.xml}
     */
    @SuppressWarnings("unused")
    private void setMembers(SortedSet<MivPerson> members)
    {
        this.members = members;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Group copy(GroupType type, Set<AssignedRole> assignedRoles)
    {
        return new GroupImpl(name,
                             description,
                             type != null ? type : this.type,//set new type if not null
                             creator,
                             readOnly,
                             hidden,
                             confidential || hidden != null && hidden,//private groups become confidential
                             active,
                             assignedRoles != null ? assignedRoles : this.assignedRoles,//set new assigned roles if not null
                             inactive,
                             members);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object ob)
    {
        //quick optimization
        if (this == ob) return true;
        if (!(ob instanceof GroupImpl)) return false;
        GroupImpl group = (GroupImpl) ob;

        return type == group.getType()//both are the same type
            && (id > 0 || group.getId() > 0)//at least one has an ID is greater than zero
            && id == group.getId()//equal if IDs match
            || StringUtil.nullComparator.compare(description, group.getDescription()) == 0//OR, description matches
            && name.equals(group.getName())//name matches
            && members.equals(group.getMembers());//same members
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode()
    {
        int hash = 1;

        //corresponds to the conditions of equals
        if (id > 0)
        {
            hash = hash * 31 + id;
        }
        else
        {
            if (StringUtils.hasText(description))
            {
                hash = hash * 31 + description.hashCode();
            }

            hash = hash * 31 + name.hashCode();
            hash = hash * 31 + members.hashCode();
        }

        return hash;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int compareTo(Group group)
    {
        /*
         * An ID comparison is enough if both are the same
         * type and at least one ID is greater than zero
         */
        if (type == group.getType()
         && (id > 0 || group.getId() > 0))
        {
            return id - group.getId();
        }

        // return description comparison if not equal
        int comparison = StringUtil.nullComparator.compare(description, group.getDescription());
        if (comparison != 0) return comparison;

        // Return name comparison if not equal
        comparison = name.compareTo(group.getName());
        if (comparison != 0) return comparison;

        // Return members comparison if not equal
        comparison = members.hashCode() - group.getMembers().hashCode();
        if (comparison != 0) return comparison;

        // Should be equal then
        assert equals(group) : "compareTo is inconsistent with equal";

        return 0;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();

        //group attributes
        builder.append("\n*** GROUP ***")
               .append("\nID:\t\t").append(id)
               .append("\nName:\t\t").append(name)
               .append("\nDescription:\t").append(description)
               .append("\nType:\t\t").append(type)
               .append("\nCreator:\t").append(creator)
               .append("\nCreated:\t").append(created)
               .append("\nUpdated:\t").append(updated)
               .append("\nReadOnly:\t").append(readOnly)
               .append("\nPrivate:\t").append(hidden)
               .append("\nConfidential:\t").append(confidential)
               .append("\nActive:\t\t").append(active);

        /*
         * list assigned roles
         */
        builder.append("\nAssigned Roles:");

        if (assignedRoles.isEmpty())
        {
            builder.append("\t[None]");
        }
        else
        {
            for (AssignedRole assignedRole : assignedRoles)
            {
                builder.append("\n\t\t* ")
                       .append(assignedRole);
            }
        }

        /*
         * list group members
         */
        builder.append("\nGroup Members: ");

        if (members.isEmpty())
        {
            builder.append("\t[None]");
        }
        else
        {
            for (MivPerson member : members)
            {
                builder.append("\n\t\t* ")
                       .append(inactive.contains(member) ? "[inactive] " : "")
                       .append(member);
            }
        }

        builder.append("\n*************");

        return builder.toString();
    }
}
