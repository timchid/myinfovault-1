/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: DossierActionType.java
 */

package edu.ucdavis.mw.myinfovault.domain.action;

import static edu.ucdavis.mw.myinfovault.domain.action.DossierDelegationAuthority.NON_REDELEGATED;
import static edu.ucdavis.mw.myinfovault.domain.action.DossierDelegationAuthority.REDELEGATED;

import java.util.Arrays;

/**
 * This class represents the available dossier workflow action types.
 *
 * @author Rick Hendricks
 * @author Craig Gilmore
 * @since MIV 3.0
 */
public enum DossierActionType
{
    /**
     * Appointment. May be redelegated(default) or non-redelegated.
     */
    APPOINTMENT(21, "New Appointment", "Appointment", REDELEGATED, NON_REDELEGATED),

    /**
     * Endowed Chair Appointment. May be redelegated or non-redelegated.
     */
    APPOINTMENT_ENDOWED_CHAIR(3, "Endowed Chair Appointment", "Endowed Chair Review", NON_REDELEGATED, REDELEGATED),

    /**
     * .  Endowed Professorship Appointment. May be non-redelegated only.
     */
    APPOINTMENT_ENDOWED_PROFESSORSHIP(4, "Endowed Professorship Appointment", "Endowed Chair Review", NON_REDELEGATED),

    /**
     * Endowed Specialist in CE Appointment.  May be non-redelegated only.
     */
    APPOINTMENT_ENDOWED_SPECIALIST(5, "Endowed Specialist in CE Appointment", "Endowed Chair Review", NON_REDELEGATED),

    /**
     * Initial Continuing Appointment.  May be non-redelegated only.
     */
    APPOINTMENT_INITIAL_CONTINUING(6, "Initial Continuing Appointment", "Continuing Appointment", NON_REDELEGATED),

    /**
     * Appointment via change in Department.  May be non-redelegated only.
     */
    APPOINTMENT_CHANGE_DEPARTMENT(7, "Appointment via change in Department", "Appointment", NON_REDELEGATED),

    /**
     * Appointment via change in Title.  May be redelegated or non-redelegated.
     */
    APPOINTMENT_CHANGE_TITLE(8, "Appointment via change in Title", "Appointment", NON_REDELEGATED, REDELEGATED),

    /**
     * Appraisal.  May be redelegated or non-redelegated.
     */
    APPRAISAL(9, "Appraisal", "Appraisal", NON_REDELEGATED, REDELEGATED),

    /**
     * 1st Year Deferral.  May be redelegated only.
     */
    DEFERRAL_FIRST_YEAR(10, "1st Year Deferral", "Deferral", REDELEGATED, NON_REDELEGATED),

    /**
     * 2nd Year Deferral.  May be redelegated only.
     */
    DEFERRAL_SECOND_YEAR(11, "2nd Year Deferral", "Deferral", REDELEGATED, NON_REDELEGATED),

    /**
     * 3rd Year Deferral.  May be non-redelegated only.
     */
    DEFERRAL_THIRD_YEAR(12, "3rd Year Deferral", "Deferral", NON_REDELEGATED),

    /**
     * 4th Year Deferral.  May be non-redelegated only.
     */
    DEFERRAL_FOURTH_YEAR(13, "4th Year Deferral", "Deferral", NON_REDELEGATED),

    /**
     * Emeritus Status.  May be non-redelegated only.
     */
    EMERITUS_STATUS(14, "Emeritus Status", "Emeritus", NON_REDELEGATED),

    /**
     * Five Year Review.  May be non-redelegated only.
     */
    FIVE_YEAR_REVIEW(15, "Five Year Review", "Five Year Review", NON_REDELEGATED),

    /**
     * Five Year Review Department Chair Review.  May be non-redelegated only.
     */
    FIVE_YEAR_REVIEW_DEPARTMENT_CHAIR(16, "Five Year Review - Department Chair Review", "Chair Review", NON_REDELEGATED),

    /**
     * Merit.  May be redelegated or non-redelegated.
     */
    MERIT(1, "Merit", "Merit", NON_REDELEGATED, REDELEGATED),

    /**
     * Promotion.  May be redelegated or non-redelegated.
     */
    PROMOTION(2, "Promotion", "Promotion", NON_REDELEGATED, REDELEGATED),

    /**
     * Reappointment.  May be redelegated only.
     */
    REAPPOINTMENT(17, "Reappointment: Pre-Six Unit 18", "Reappointment", REDELEGATED, NON_REDELEGATED),

    /**
     * Endowed Chair Reappointment.  May be non-redelegated only.
     */
    REAPPOINTMENT_ENDOWED_CHAIR(18, "Endowed Chair Reappointment", "Endowed Chair Review", NON_REDELEGATED),

    /**
     * Endowed Professorship Reappointment.  May be non-redelegated only.
     */
    REAPPOINTMENT_ENDOWED_PROFESSORSHIP(19, "Endowed Professorship Reappointment", "Endowed Chair Review", NON_REDELEGATED),

    /**
     * Endowed Specialist in CE Reappointment.  May be non-redelegated only.
     */
    REAPPOINTMENT_ENDOWED_SPECIALIST(20, "Endowed Specialist in CE Reappointment", "Endowed Chair Review", NON_REDELEGATED);

    private int actionTypeId=0;
    private String description;
    private String edmsDocumentKey;
    private DossierDelegationAuthority[] validDelegationAuthorities;

    /**
     * Initialize action types.
     *
     * @param actionTypeId id for the action type
     * @param description description for the letter type
     * @param edmsDocumentKey for the letter type
     * @param validDelegationAuthorities valid delegation authorities for the action type (first is considered the default)
     * @throws IllegalArgumentException if no associated delegation authorities have been defined
     */
    private DossierActionType(int actionTypeId, String description, String edmsDocumentKey,
                              DossierDelegationAuthority...validDelegationAuthorities) throws IllegalArgumentException
    {
        this.actionTypeId = actionTypeId;
        this.description = description;
        this.validDelegationAuthorities = validDelegationAuthorities;
        this.edmsDocumentKey = edmsDocumentKey;

        if (validDelegationAuthorities.length < 1)
        {
            throw new IllegalArgumentException(description + " must have at least one associated delegation authority");
        }
    }

    /**
     * @return id for the action type
     */
    public int getActionTypeId()
    {
        return this.actionTypeId;
    }

    /**
     * @return description for the letter type
     */
    public String getDescription()
    {
        return description;
    }

    /**
     * @return edmsDocumentKey for the letter type
     */
    public String getEDMSDocumentKey()
    {
        return edmsDocumentKey;
    }

    /**
     * @return valid delegation authorities for the action type
     */
    public DossierDelegationAuthority[] getValidDelegationAuthorities()
    {
        return validDelegationAuthorities;
    }

    /**
     * @return valid delegation authorities for the action type
     */
    public DossierDelegationAuthority getDefaultDelegationAuthority()
    {
        return validDelegationAuthorities[0];//first valid one defined
    }

    /**
     * @param delegationAuthority delegation authority to validate
     * @return if the given delegation authority is valid for this action type
     */
    public boolean isValid(DossierDelegationAuthority delegationAuthority)
    {
        return Arrays.asList(getValidDelegationAuthorities()).contains(delegationAuthority);
    }

    /**
     * Returns the enum corresponding to the id
     * @param id
     * @return null if not match otherwise matched DossierActionType
     */
    public static DossierActionType valueOf(int id)
    {
        DossierActionType[] enumlist = DossierActionType.values();
        for (DossierActionType actionType : enumlist)
        {
            if (actionType.getActionTypeId() == id) { return actionType; }
        }

        return null;
    }
}
