/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: DCBo.java
 */

package edu.ucdavis.mw.myinfovault.domain.disclosurecertificate;

import java.io.Serializable;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.springframework.util.StringUtils;

import edu.ucdavis.mw.myinfovault.domain.action.StatusType;
import edu.ucdavis.mw.myinfovault.domain.action.Title;
import edu.ucdavis.mw.myinfovault.domain.dossier.Dossier;
import edu.ucdavis.mw.myinfovault.domain.raf.AcademicActionBo;
import edu.ucdavis.mw.myinfovault.domain.raf.RafBo;
import edu.ucdavis.mw.myinfovault.domain.signature.SignableDocument;
import edu.ucdavis.mw.myinfovault.service.MivServiceLocator;
import edu.ucdavis.mw.myinfovault.service.signature.SignatureStatus;
import edu.ucdavis.mw.myinfovault.util.MivDocument;
import edu.ucdavis.mw.myinfovault.util.StringUtil;
import edu.ucdavis.myinfovault.document.DocumentFormat;

/**
 * Disclosure Certificate business object.
 *
 * @author Jed Whitten
 * @author Craig Gilmore
 * @author Pradeep Haldiya (MIV v4.7.1)
 * @since MIV 3.0
 */
public class DCBo extends SignableDocument implements Serializable
{
    private static final long serialVersionUID = -1737933765408912603L;

    /**
     * Format used in {@link #toString()}
     */
    private static final String TO_STRING = "\n\t*** Disclosure Certificate ***" +
                                            "\n\tarchived:\t{0}" +
                                            "\n\trevised:\t{1}" +
                                            "\n{2}";

    /**
     * DC PDF filename.
     */
    private static final String DC_FILE = "{0}_{1,number,#}_{2,number,#}." + DocumentFormat.PDF.getFileExtension();

    /**
     * DC PDF filename for drafts with no associated signature/request.
     */
    private static final String ADDED_DC_FILE = "added_" + DC_FILE;
    private static final int DEFAULT_DC_ID = 0;

    private String candidateName = null;
    private String schoolName = null;
    private String departmentName = null;
    private List<Material> materials = new ArrayList<Material>();
    private List<Rank> ranks = new ArrayList<Rank>();
    private boolean revised = false;
    private Date enteredDate;
    private boolean actionAppraisal = false;
    private boolean actionAcceleration = false;
    private boolean actionDeferral = false;
    private boolean actionOther = false;
    private String additionalInfo = StringUtil.EMPTY_STRING;
    private String changesAdditions = StringUtil.EMPTY_STRING;
    private boolean archived = false;

    /**
     * Create a disclosure certificate from a database record.
     *
     * @param id
     * @param dossier
     * @param schoolId
     * @param departmentId
     * @param candidateName
     * @param schoolName
     * @param departmentName
     * @param revised
     * @param enteredDate
     * @param actionAppraisal
     * @param actionAcceleration
     * @param actionDeferral
     * @param actionOther
     * @param additionalInfo
     * @param changesAdditions
     * @param materials
     * @param ranks
     * @param archived
     */
    public DCBo(int id,
                Dossier dossier,
                int schoolId,
                int departmentId,
                String candidateName,
                String schoolName,
                String departmentName,
                boolean revised,
                Date enteredDate,
                boolean actionAppraisal,
                boolean actionAcceleration,
                boolean actionDeferral,
                boolean actionOther,
                String additionalInfo,
                String changesAdditions,
                List<Material> materials,
                List<Rank> ranks,
                boolean archived)
    {
        super(dossier, id, getDocumentType(dossier, schoolId, departmentId), schoolId, departmentId);

        this.materials = materials;
        this.ranks = ranks;
        this.candidateName = candidateName;
        this.schoolName = schoolName;
        this.departmentName = departmentName;
        this.revised = revised;
        this.enteredDate = enteredDate;
        this.actionAppraisal = actionAppraisal;
        this.actionAcceleration = actionAcceleration;
        this.actionDeferral = actionDeferral;
        this.actionOther = actionOther;
        this.additionalInfo = additionalInfo;
        this.changesAdditions = changesAdditions;
        this.archived = archived;
    }

    /**
     * Create a new disclosure certificate.
     *
     * @param dossier
     * @param schoolId
     * @param departmentId
     */
    public DCBo(Dossier dossier,
                int schoolId,
                int departmentId)
    {
        super(dossier, DEFAULT_DC_ID, getDocumentType(dossier, schoolId, departmentId), schoolId, departmentId);

        // load RAF present/proposed rank/step
        loadRAF();
    }

    /**
     * Disclosure certificate shallow copy constructor.
     *
     * Copies are constructed with:
     * <ul>
     * <li>Default record ID</li>
     * <li>Latest available candidate name</li>
     * <li>Latest available school name</li>
     * <li>Latest available department name</li>
     * <li>Latest RAF present/proposed ranks and steps</li>
     * <li>{@link #revised} set <code>true</code></li>
     * <li>{@link #archived} set <code>false</code></li>
     * </ul>
     *
     * @param dc disclosure certificate to copy.
     */
    private DCBo(DCBo dc)
    {
        // create new DC
        this(dc.getDossier(), dc.getSchoolId(), dc.getDepartmentId());

        // revised by default
        this.revised = true;

        // constructed copy is not archived
        this.archived = false;

        // copy over from given DC
        this.enteredDate = dc.getEnteredDate();
        this.actionAppraisal = dc.isActionAppraisal();
        this.actionAcceleration = dc.isActionAcceleration();
        this.actionDeferral = dc.isActionDeferral();
        this.actionOther = dc.isActionOther();
        this.additionalInfo = dc.getAdditionalInfo();
        this.changesAdditions = dc.getChangesAdditions();
        this.materials = dc.getMaterials();
    }

    /**
     * Load CDC with latest RAF present/proposed rank/step.
     */
    private void loadRAF()
    {
        // clear current ranks
        this.ranks.clear();

        // build rank and step from the legacy RAF
        if (getDossier().isRecommendedActionFormPresent())
        {
            RafBo raf = MivServiceLocator.getRafService().getBo(getDossier());

            // Compute RankAndSteps from RafBo
            Iterator<edu.ucdavis.mw.myinfovault.domain.raf.Rank> present = raf.getStatus(StatusType.PRESENT).getRanks().iterator();
            Iterator<edu.ucdavis.mw.myinfovault.domain.raf.Rank> proposed = raf.getStatus(StatusType.PROPOSED).getRanks().iterator();

            while (present.hasNext() || proposed.hasNext())
            {
                this.ranks.add(new Rank(present.hasNext() ? present.next() : null,
                                        proposed.hasNext() ? proposed.next() : null));
            }
        }
        // build rank and step from the "new RAF"
        else if (getDossier().isAcademicActionFormPresent())
        {
            AcademicActionBo aa = MivServiceLocator.getAcademicActionService().getBo(getDossier());

            Iterator<Title> present = aa.getTitles(StatusType.PRESENT, getScope()).iterator();
            Iterator<Title> proposed = aa.getTitles(StatusType.PROPOSED, getScope()).iterator();

            while (present.hasNext() || proposed.hasNext())
            {
                this.ranks.add(new Rank(present.hasNext() ? present.next() : null,
                                        proposed.hasNext() ? proposed.next() : null));
            }
        }
    }

    /**
     * @return dossier candidate name
     */
    public String getCandidateName()
    {
        return StringUtils.hasText(candidateName)
             ? candidateName// name preserved from database
             : getDossier().getAction().getCandidate().getDisplayName();// latest from dossier
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.domain.signature.SignableDocument#getSchoolName()
     */
    @Override
    public String getSchoolName()
    {
        return StringUtils.hasText(schoolName)
             ? schoolName// name preserved from database
             : super.getSchoolName();// latest from MIV config
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.domain.signature.SignableDocument#getDepartmentName()
     */
    @Override
    public String getDepartmentName()
    {
        return StringUtils.hasText(departmentName)
             ? departmentName// name preserved from database
             : super.getDepartmentName();// latest from MIV config
    }

    /**
     * @return if revised has been selected
     */
    public boolean isRevised()
    {
       return revised;
    }

    /**
     * @param revised if revised has been selected
     * @return self-reference for chaining
     */
    public DCBo setRevised(boolean revised)
    {
       this.revised = revised;
       return this;
    }

    /**
     * @return dossier entered date
     */
    @Deprecated
    public Date getEnteredDate()
    {
    	return enteredDate;
    }

    /**
     * @return if the DC action type is an appraisal
     */
    @Deprecated
    public boolean isActionAppraisal()
    {
        return actionAppraisal;
    }

    /**
     * @return if the DC action type is an acceleration
     */
    @Deprecated
    public boolean isActionAcceleration()
    {
        return actionAcceleration;
    }

    /**
     * @return if the DC action type is a deferral
     */
    @Deprecated
    public boolean isActionDeferral()
    {
        return actionDeferral;
    }

    /**
     * @return if the DC action type is an 'other'
     */
    @Deprecated
    public boolean isActionOther()
    {
        return actionOther;
    }

    /**
     * @return additional information for the candidate
     */
    public String getAdditionalInfo()
    {
        return additionalInfo;
    }

    /**
     * @param additionalInfo additional information for candidate
     */
    public void setAdditionalInfo(String additionalInfo)
    {
        this.additionalInfo = additionalInfo;
    }

    /**
     * @return comment changes to the DC since last signing
     */
    public String getChangesAdditions()
    {
        return changesAdditions;
    }

    /**
     * @param changesAdditions comment changes to the DC since last signing
     */
    public void setChangesAdditions(String changesAdditions)
    {
        this.changesAdditions = changesAdditions;
    }

    /**
     * @return materials reviewed by the candidate
     */
    @Deprecated
    public List<Material> getMaterials()
    {
        return materials;
    }

    /**
     * @return present and proposed rank and steps
     */
    public List<Rank> getRanks()
    {
        return ranks;
    }

    /**
     * @return if the DC has been archived
     */
    public boolean isArchived()
    {
        return archived;
    }

    /**
     * @param archived if the DC has been archived
     * @return self-reference for chaining
     */
    public DCBo setArchived(boolean archived)
    {
        this.archived = archived;
        return this;
    }

	/*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.domain.signature.SignableDocument#getFilenamePattern()
     */
    @Override
    protected String getFilenamePattern()
    {
        return getSignatureStatus() == SignatureStatus.MISSING ? ADDED_DC_FILE : DC_FILE;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        return MessageFormat.format(TO_STRING, archived, revised, super.toString());
    }

    /**
     * @return Shallow copy of DC
     */
    public DCBo copy()
    {
    	// using copy constructor
        return new DCBo(this);
    }

    /**
     * @return DC with refreshed RAF data
     */
    public DCBo refresh()
    {
        loadRAF();
        return this;
    }

    /**
     * Get the disclosure certificate document type for the given dossier and scope.
     *
     * @param dossier DC dossier
     * @param schoolId DC school ID
     * @param departmentId DC department ID
     * @return signable document type
     */
    private static MivDocument getDocumentType(Dossier dossier, int schoolId, int departmentId)
    {
        return dossier.getPrimarySchoolId() == schoolId
            && dossier.getPrimaryDepartmentId() == departmentId
             ? MivDocument.DISCLOSURE_CERTIFICATE
             : MivDocument.JOINT_DISCLOSURE_CERTIFICATE;
    }
}
