/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: DepartmentTypeFilter.java
 */

package edu.ucdavis.mw.myinfovault.domain.raf;

import edu.ucdavis.mw.myinfovault.service.SearchFilterAdapter;

/**
 * Filters for ranks that are blank (contain no data).
 *
 * @author Craig Gilmore
 * @since 4.3
 */
public class RankFilter extends SearchFilterAdapter<Rank>
{
    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.SearchFilter#include(java.lang.Object)
     */
    @Override
    public boolean include(Rank item)
    {
        return item.isBlank();
    }
}
