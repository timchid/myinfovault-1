/*

 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: AcademicActionBo.java
 */

package edu.ucdavis.mw.myinfovault.domain.raf;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;

import edu.ucdavis.mw.myinfovault.domain.action.AppointmentDetails;
import edu.ucdavis.mw.myinfovault.domain.action.Assignment;
import edu.ucdavis.mw.myinfovault.domain.action.StatusType;
import edu.ucdavis.mw.myinfovault.domain.action.Title;
import edu.ucdavis.mw.myinfovault.domain.dossier.Dossier;
import edu.ucdavis.mw.myinfovault.service.person.Appointment;
import edu.ucdavis.mw.myinfovault.service.person.Scope;

/**
 * AcademicActionBO
 * @author rhendric
 *
 */
public class AcademicActionBo implements Serializable
{
    private static final long serialVersionUID = 7339609714816423770L;

    private final Dossier dossier;

    private Map<StatusType, List<Assignment>> assignmentStatuses;
    private final AppointmentDetails apptDetails;

    /**
     * @param dossier
     * @param apptDetails
     * @param assignmentStatuses
     */
    public AcademicActionBo(Dossier dossier,
                            AppointmentDetails apptDetails,
                            Map<StatusType, List<Assignment>> assignmentStatuses)
    {
        this.dossier = dossier;
        this.assignmentStatuses = assignmentStatuses;
        this.apptDetails = apptDetails;
    }

    /**
     * Create a new recommended academic action for the given dossier.
     *
     * @param dossier
     */
    public AcademicActionBo(Dossier dossier)
    {
        this.dossier = dossier;
        this.apptDetails = new AppointmentDetails();

        List<Assignment> presentAssignments = new ArrayList<Assignment>();
        List<Assignment> proposedAssignments = new ArrayList<Assignment>();

        Collection<Appointment> candidateAppointments = dossier.getAction().getCandidate().getAppointments();

        /*
         * Populate academic action assignments with current candidate appointments (if any).
         */
        if (!candidateAppointments.isEmpty())
        {
            // current number of candidate appointments
            BigDecimal appointmentCount = new BigDecimal(candidateAppointments.size());

            /*
             * Divide total percent of time (100%) among all appointments. Primary
             * percent of time rounded up so that the percentages sum to 100%.
             */
            BigDecimal percentOfTime = BigDecimal.ONE.divide(appointmentCount, 2, RoundingMode.DOWN);
            BigDecimal primaryPercentOfTime = BigDecimal.ONE.divide(appointmentCount, 2, RoundingMode.UP);

            // present and proposed assignments default to candidate appointments
            for (Appointment appointment : candidateAppointments)
            {
                presentAssignments.add(new Assignment(new ArrayList<Title>(),
                                                      appointment.isPrimary(),
                                                      appointment.getScope(),
                                                      appointment.isPrimary() ? primaryPercentOfTime : percentOfTime));

                proposedAssignments.add(new Assignment(new ArrayList<Title>(),
                                                       appointment.isPrimary(),
                                                       appointment.getScope(),
                                                       appointment.isPrimary() ? primaryPercentOfTime : percentOfTime));
            }
        }

        this.assignmentStatuses = ImmutableMap.of(StatusType.PRESENT, presentAssignments,
                                                  StatusType.PROPOSED, proposedAssignments);
    }

    /**
     * @return dossier to which this academic action belongs
     */
    public Dossier getDossier()
    {
        return this.dossier;
    }

    /**
     * @return logical iterable of both present and proposed assignments
     */
    public List<Assignment> getAssignments()
    {
        List<Assignment> present = assignmentStatuses.get(StatusType.PRESENT);

        return Lists.newArrayList(Iterables.concat(present != null ? present : Collections.<Assignment>emptyList(),
                                                   assignmentStatuses.get(StatusType.PROPOSED)));
    }


    /**
     * @return
     */
    public Map<StatusType, List<Assignment>> getAssignmentStatuses()
    {
        return this.assignmentStatuses;
    }

    /**
     * @param assignmentStatuses
     */
    public void setAssignmentStatuses(Map<StatusType, List<Assignment>> assignmentStatuses)
    {
        this.assignmentStatuses = assignmentStatuses;
    }

    /**
     * @return
     */
    public AppointmentDetails getApptDetails()
    {
        return this.apptDetails;
    }

    /**
     * Get the first assignment matching the given status type and scope.
     *
     * @param type status type
     * @param scope school and department scope
     * @return matching assignment
     */
    public Assignment getAssignment(StatusType type, Scope scope)
    {
        // search all assignments of the given status type
        for (Assignment assignment : assignmentStatuses.get(type))
        {
            // given scope matches assignment
            if (assignment.getScope().matches(scope)) return assignment;
        }

        // not found
        return null;
    }

    /**
     * Get the titles for the assignment matching the given status type and scope.
     *
     * @param type status type
     * @param scope school and department scope
     * @return matching assignment titles
     */
    public List<Title> getTitles(StatusType type, Scope scope)
    {
        Assignment assignment = getAssignment(type, scope);

        return assignment != null ?  assignment.getTitles() : Collections.<Title>emptyList();
    }
}
