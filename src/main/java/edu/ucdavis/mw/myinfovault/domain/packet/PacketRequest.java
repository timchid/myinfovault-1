/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: PacketRequest.java
 */

package edu.ucdavis.mw.myinfovault.domain.packet;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.ucdavis.mw.myinfovault.domain.dossier.Dossier;
import edu.ucdavis.mw.myinfovault.service.person.MivRole;

/**
 * This class represents a PacketRequest
 * @author rhendric
 *
 */
public class PacketRequest
{

    private Dossier dossier;
    private int requestId;
    private long packetId;
    private Date requestTime;
    private Date submitTime;

    private static Logger logger = LoggerFactory.getLogger(PacketRequest.class);

    /**
     * Construct a new packet request
     * @param Dossier
     */
    public PacketRequest(Dossier dossier)
    {
        this(dossier, 0);
    }

    /**
     * Construct a packet request from input parameters
     * @param dossier
     * @param packetId
     */
    public PacketRequest(Dossier dossier, int packetId)
    {
        this(dossier, packetId, 0);
    }

    /**
     * Construct a packet request from input parameters
     * @param dossier
     * @param packetId
     * @param requestId
     */
    public PacketRequest(Dossier dossier, int packetId, int requestId)
    {
        this(dossier, packetId, requestId, null, null);
    }

    /**
     * Construct a packet request from input parameters
     * @param dossier
     * @param packetId
     * @param requestId
     * @param InsertTime of the packet request
     */
    public PacketRequest(Dossier dossier, int packetId, int requestId, Date inserttime, Date updatetime)
    {
        this.dossier = dossier;
        this.packetId = packetId;
        this.requestId = requestId;
        this.requestTime = inserttime;
        this.submitTime = updatetime;
    }

    public boolean isSubmitted()
    {
        return packetId != 0;
    }

    /**
     * Get the userId for this packet request
     * @return userId
     */
    public int getUserId()
    {
        return dossier.getAction().getCandidate().getUserId();
    }

    /**
     * Get the requestId associated with this packet request
     * @return int requestId
     */
    public int getRequestId()
    {
        return this.requestId;
    }

    /**
     * Set the request ID
     * @param requestId
     */
    public void setRequestId(int requestId)
    {
        this.requestId = requestId;
    }

    /**
     * Get the packetId
     * @return long packetId
     */
    public long getPacketId()
    {
        return this.packetId;
    }

    /**
     * Set the packetId
     * @param packetId
     */
    public void setPacketId(long packetId)
    {
        this.packetId = packetId;
    }

    /**
     * Get the Dossier associated with this packet request
     * @return Dossier
     */
    public Dossier getDossier()
    {
        return this.dossier;
    }


    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        return String.format("PacketRequest #%s for Dossier #%s, has packetId #%s", requestId, dossier.getDossierId(), packetId);
    }

    /**
     * @return the requestTime
     */
    public Date getRequestTime()
    {
        return requestTime;
    }

    /**
     * @return the submitTime
     */
    public Date getSubmitTime()
    {
        return submitTime;
    }

    /**
     * @return A URL linking to the Candidate's view of the PDF.
     */
    public String getSubmittedPacketUrl()
    {
        if (this.isSubmitted())
        {
            Dossier dossier = this.getDossier();
            return dossier.getDossierPdfUrl(MivRole.CANDIDATE, dossier.getPrimarySchoolId(), dossier.getPrimaryDepartmentId());
        }

        return null;
    }
}
