/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: DecisionRoleFilter.java
 */

package edu.ucdavis.mw.myinfovault.domain.signature;

import edu.ucdavis.mw.myinfovault.service.SearchFilterAdapter;
import edu.ucdavis.mw.myinfovault.service.person.MivRole;

/**
 * Filters for signatures that a user with the given role is allowed to sign.
 *
 * @author Craig Gilmore
 * @since 4.6.2.2
 */
public class DocumentRoleFilter extends SearchFilterAdapter<MivElectronicSignature>
{
    private final MivRole role;

    /**
     * Creates the document role filter.
     *
     * @param role capacity by which signatures will be filtered
     */
    public DocumentRoleFilter(MivRole role)
    {
        this.role = role;
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.SearchFilter#include(java.lang.Object)
     */
    @Override
    public boolean include(MivElectronicSignature item)
    {
        return item.getDocument().getDocumentType().getAllowedSigner() == role;
    }
}
