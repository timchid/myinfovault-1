/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: SalaryPeriod.java
 */

package edu.ucdavis.mw.myinfovault.domain.action;

/**
 * {@link Title} salary period. Either monthly or hourly.
 *
 * @author Craig Gilmore
 * @since MIV 4.8
 */
public enum SalaryPeriod
{
    /**
     * Salary period is monthly.
     */
    MONTHLY("Monthly", "Monthly Salary", "month"),

    /**
     * Salary period is hourly.
     */
    HOURLY("Hourly", "Hourly Rate", "hour");

    private final String label;
    private final String description;
    private final String term;

    /**
     * @param label salary period display label
     * @param description more verbose salary period description
     */
    private SalaryPeriod(String label, String description, String term)
    {
        this.label = label;
        this.description = description;
        this.term = term;
    }

    /**
     * @return salary period display label
     */
    public String getLabel()
    {
        return label;
    }

    /**
     * @return more verbose salary period description
     */
    public String getDescription()
    {
        return description;
    }

    /**
     * @return the term used to refer to the period
     */
    public String getTerm()
    {
        return term;
    }
}
