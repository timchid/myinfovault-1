/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: DossierReviewType.java
 */

package edu.ucdavis.mw.myinfovault.domain.dossier;

/**
 * This class represents the available dossier review types.
 * 
 * @author Rick Hendricks
 * @since MIV 3.4
 */
public enum DossierReviewType
{
    /**
     * CRC review is required.
     */
    CRC_REVIEW ("CRC Required");

    private final String description;

    /**
     * Initialize the description for the review type.
     * 
     * @param description description for the review type
     */
    private DossierReviewType(String description)
    {
        this.description = description;
    }

    /**
     * @return description for the review type
     */
    public String getDescription() 
    {
        return this.description;
    }
}
