/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: PacketContent.java
 */

package edu.ucdavis.mw.myinfovault.domain.packet;

import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;

import edu.ucdavis.mw.myinfovault.util.StringUtil;

/**
 * This class represents the content of a Packet created by the Candidate.
 * @author rhendric
 *
 * @since MIV 5.0
 *
 */
@XmlRootElement(name="packetitem")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class PacketContent
{
    private int id;
    private long packetId;
    private Integer userId;
    private int sectionId;
    private int recordId;
    private String displayParts;

    public PacketContent()
    {
        //used by spring mvc for databinding
    }

    /**
     * Construct a PacketContent object for insertion into the database
     *
     * @param packetId - packetId to which this content belongs
     * @param userId - owner if the packet content record
     * @param sectionId - sectionId from the Section table referenced by this content item
     * @param recordId - recordId within the section referenced by this content item
     */
    public PacketContent(long packetId, int userId, int sectionId, int recordId)
    {
        this(0, packetId, userId, sectionId, recordId, null);
    }

    /**
     * Construct a PacketContent object for insertion into the database
     *
     * @param packetId - packetId to which this content belongs
     * @param userId - owner if the packet content record
     * @param sectionId - sectionId from the Section table referenced by this content item
     * @param recordId - recordId within the section referenced by this content item
     * @param displayParts - Comma delimited list of textual boolean values which indicate
     *                       the display state (true/false) of the section record itself
     *                       and any associated "contribution" items.
     *                       As an example, you may want to display the main publication record
     *                       for and entry but suppress the display of the contribution record
     *                       on the "Display Contributions to Jointly Authored Works" document.
     */
    public PacketContent(long packetId, int userId, int sectionId, int recordId, String displayParts)
    {
        this(0, packetId, userId, sectionId, recordId, displayParts);
    }

    /**
     * Construct a PacketContent object - Used when retrieving an item from the database.
     * @param id - Id of those PacketContent record
     * @param packetId - packetId to which this content belongs
     * @param userId - owner if the packet content record
     * @param sectionId - sectionId from the Section table referenced by this content item
     * @param recordId - recordId within the section referenced by this content item
     * @param displayParts - Comma delimited list of textual boolean values which indicate
     *                       the display state (true/false) of the section record itself
     *                       and any associated "contribution" items.
     *                       As an example, you may want to display the main publication record
     *                       for and entry but suppress the display of the contribution record
     *                       on the "Display Contributions to Jointly Authored Works" document.    */
    public PacketContent(int id, long packetId, int userId, int sectionId, int recordId, String displayParts)
    {
        this.id = id;
        this.packetId = packetId;
        this.userId = userId;
        this.sectionId = sectionId;
        this.recordId = recordId;
        this.displayParts = displayParts;
    }

    /**
     * @param id
     */
    public void setId(int id)
    {
        this.id = id;
    }

    /**
     * @return id
     */
    @JsonIgnore
    public int getId()
    {
        return this.id;
    }

    /**
     * @param packetId
     */
    public void setPacketId(Long packetId)
    {
        this.packetId = packetId;
    }

    /**
     * @return packetId
     */
    @JsonIgnore
    public Long getPacketId()
    {
        return this.packetId;
    }

    /**
     * @return userId
     */
    @JsonIgnore
    public Integer getUserId()
    {
        return this.userId;
    }

    /**
     * @return displayParts
     */
    public String getDisplayParts()
    {
        return this.displayParts;
    }

    @JsonIgnore
    public String[] getPartsArray()
    {
        return StringUtil.splitToArray(this.displayParts);
    }

    /**
     * @return sectionId
     */
    public int getSectionId()
    {
        return this.sectionId;
    }

    /**
     * @return recordId
     */
    public int getRecordId()
    {
        return this.recordId;
    }

    /**
     * Get the key for this content item
     * @return String
     */
    @JsonIgnore
    public String getContentKey()
    {
        return PacketContentKey.build(this.packetId,this.recordId,this.sectionId);
    }

    public static class PacketContentKey
    {
        static public String build(long packetId, int recordId, int sectionId)
        {
            return Long.toString(packetId)+":"+Integer.toString(recordId)+":"+Integer.toString(sectionId);
        }
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(Integer userId)
    {
        this.userId = userId;
    }

    /**
     * @param sectionId the sectionId to set
     */
    public void setSectionId(int sectionId)
    {
        this.sectionId = sectionId;
    }

    /**
     * @param recordId the recordId to set
     */
    public void setRecordId(int recordId)
    {
        this.recordId = recordId;
    }

    /**
     * @param displayParts the displayParts to set
     */
    public void setDisplayParts(String displayParts)
    {
        this.displayParts = displayParts;
    }
}
