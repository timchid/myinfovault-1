/**
 * Copyright
 * Copyright (c) University of California, Davis, 2009
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * RESTRICTED RIGHTS LEGEND
 * Use, duplication, or disclosure by the U.S. Government is subject to restrictions
 * as set forth in Subparagraph (c) (1) (ii) of DFARS 252.227-7013 or in FAR
 * 52.227-19, as applicable.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: ElectronicSignatureImpl.java
 */

package edu.ucdavis.mw.myinfovault.domain.signature;

import java.nio.charset.StandardCharsets;
import java.util.Date;

import edu.ucdavis.mw.myinfovault.util.HashingUtil;



/**
 *<p>
 * To sign something, first create an ElectronicSignature object with an <em>owner</em> ID.
 * This is the owner of the signature; the person called upon to sign some data. The signature
 * is then applied to the data by invoking the {@link #sign(byte[], String)} method or one of
 * the other signing convenience methods. When signed, the ID of the <em>signer</em> is recorded,
 * which may be different from the owner due to delegation of signing authority.
 *</p>
 * @author Stephen Paulsen
 * @since MIV 3.0
 */
public class ElectronicSignatureImpl extends ElectronicSignatureBase implements ElectronicSignature
{
    /** The current version of the electronic signature. */
    private static final String CURRENT_VERSION = "1.0";
    /** Version number of this class, so we can change algorithms or key-construction strategy, but still be able to work with old signatures. */
    protected String compatibleVersion = CURRENT_VERSION;

    /** Indicate whether this ElectronicSignature has been used to sign something. */
    protected transient boolean signed = false;  // we could just use the presence or absence of 'signature' (!=null) instead of this boolean

    /** Generated signature */
    protected byte[] signature = null;

    private transient boolean debug = false;

    /**
     * Create a signature object "owned" by the specified user/person.
     *
     * @param requestedSigner Identifier of the <em>owner</em> of this signature
     */
    public ElectronicSignatureImpl(String requestedSigner)
    {
        super(requestedSigner);
    }

    /**
     * No-arg constructor for sub-class serialization
     */
    protected ElectronicSignatureImpl(){}


    /**
     * Not public used only internally as a test case.
     * @param signerId person actually signing the document, which could be different from the requested signer.
     */
    private void sign(String signerId)
    {
        sign((byte[])null, signerId);
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.domain.signature.ElectronicSignature#sign(java.lang.String, java.lang.String)
     */
    @Override
    public void sign(String document, String signerId)
    {
        byte[] docBytes = null;
        if (document != null)
        {
            docBytes = document.getBytes(StandardCharsets.UTF_8);
        }
        sign(docBytes, signerId);
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.domain.signature.ElectronicSignature#sign(byte[], java.lang.String)
     */
    @Override
    public void sign(byte[] document, String signerId)
    {
    	// Sign only if not signed or is invalid signature
        if (!signed || !this.isValid(document))
        {
            this.signer = signerId;
            this.signingDate = new Date();
            this.signature = HashingUtil.computeHashAsBytes(getSigningKey(document), digestType);

            signed = true;

            if (debug)
            {
                System.out.printf("sign(doc, \"%1$s\") computed signature of length %2$d:%n", signerId, this.signature.length);
                for (byte b : this.signature) {
                    System.out.printf(" %1$02x", b);
                }
                System.out.println();
            }
        }
        else
        {
            throw new SigningException("already signed on " + this.signingDate + " by identity \"" + this.signer + "\"");
        }
    }


    /* *
     * We could extend this class further so it could sign a file
     * @param document
     * @param signerId
     * /
    public void sign(java.io.File document, long signerId)
    {
        //...
    }*
    /


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.domain.signature.ElectronicSignature#isValid(java.lang.String)
     */
    @Override
    public boolean isValid(String document)
    {
        byte[] docBytes = null;
        if (document != null)
        {
            docBytes = document.getBytes(StandardCharsets.UTF_8);
        }
        return isValid(docBytes);
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.domain.signature.ElectronicSignature#isValid(byte[])
     */
    @Override
    public boolean isValid(byte[] document)
    {
        return this.signed
            && this.signature != null
            && HashingUtil.bytesToHashCode(signature).equals(
                   HashingUtil.computeHash(getSigningKey(document), digestType));
    }


    /* *
     * We could extend this class further so it could sign a file
     * @param document -
     * /
    public boolean isValid(java.io.File document)
    {
        //...
    }
    */


    /**
     * Produce a "signing key" that will be hashed by the hash algorithm.
     *
     * @param document The document, or representation of the document this signature applies to or will apply to.
     * @return byte array signing key
     */
    protected byte[] getSigningKey(byte[] document)
    {
        // Retrieve the version 1 signing key. If and when the key changes this method should be refactored
        // to call, for example, getSigningKeyVersion1() or getSigningKeyVersion2() depending on the version
        // indicated and compatibility between versions.
        byte[] key = null;
        if (this.compatibleVersion.startsWith("1.")) {
            key = getSigningKeyVersion1(document);
        }
        else {
            throw new SigningException("unsupported signature version: " + this.compatibleVersion);
        }
        return key;
    }


    /**
     * Implementation of {@linkplain #getSigningKey(byte[]) getSigningKey} for version 1.0 of electronic signatures.
     * The key string (actually a byte array) is made up of pieces separated by underscores. These pieces are<ul>
     * <li>the document</li>
     * <li>the time when the document was signed, and</li>
     * <li>the identifier of the person signing the document</li>
     * </ul> that is, <code>[document]_[timestamp]_[signer]</code>
     * @param document The document, or representation of the document this signature applies to or will apply to.
     * @see #getSigningKey(byte[])
     */
    private byte[] getSigningKeyVersion1(byte[] document)
    {
        String keystring = this.signingDate != null ? "_" + this.signingDate.getTime() + "_" + this.signer : "";
        byte[] stringbytes = null;

        stringbytes = keystring.getBytes(StandardCharsets.UTF_8);

        int documentLength = document != null ? document.length : 0;
        int keylength = stringbytes.length + documentLength;
        byte[] key = new byte[ keylength ];
        if (document != null) {
            System.arraycopy(document, 0, key, 0, documentLength);
        }
        System.arraycopy(stringbytes, 0, key, documentLength, stringbytes.length);

        return key;
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public String toString()
    {
        return "signed: " + this.signed + "\tidentity: " + this.signer + "\tdate: " + this.signingDate;
    }


    /**
     * Command line main program to test the class.
     *
     * @param args command-line arguments
     */
    public static void main(String[] args)
    {
        ElectronicSignature sig = new ElectronicSignatureImpl("18099");

        System.out.println("Before signing:");
        showSignature(sig, "RAF");

        sig.sign("RAF", "LisaO'J");
        System.out.println();

        System.out.println("After signing:");
        showSignature(sig, "RAF");

        // Cause the SigningException
        try {
            sig.sign("RAF", new Integer(42).toString());
            System.out.println("Test failed: an exception should have been thrown");
        }
        catch (SigningException e) {
            System.out.println("Test passed: exception thrown as expected:");
            System.out.println("SigningException [" + e.getLocalizedMessage() + "]");
        }
        System.out.println();


        // Test passing null as the document

        // Test sign(id) which implies a null document
        ElectronicSignatureImpl signull = new ElectronicSignatureImpl("0");
        try {
            signull.sign("18099");
            System.out.println("Signull 1:");
            showSignature(signull, null);
        }
        catch (Exception e) {
            System.out.println(e.getClass().getSimpleName() + " [" + e.getMessage() + "] when testing signull1");
            e.printStackTrace();
        }

        // Test sign( (String) null, id ) with an explicitly null String
        signull = new ElectronicSignatureImpl("0");
        try {
            signull.sign((String)null, "18099");
            System.out.println("Signull 2:");
            showSignature(signull, null);
        }
        catch (Exception e) {
            System.out.println(e.getClass().getSimpleName() + " [" + e.getMessage() + "] when testing signull2");
            e.printStackTrace();
        }

        // Test sign( (byte[]) null, id ) with an explicitly null byte-array
        signull = new ElectronicSignatureImpl("0");
        try {
            signull.sign((byte[])null, "18099");
            System.out.println("Signull 3:");
            showSignature(signull, null);
        }
        catch (Exception e) {
            System.out.println(e.getClass().getSimpleName() + " [" + e.getMessage() + "] when testing signull3");
            e.printStackTrace();
        }

        // Test attempting to verify (isValid) a signature that hasn't been used to sign anything.
        ElectronicSignature unsigned = new ElectronicSignatureImpl("18099");
        try {
            System.out.println("Unsigned signature:");
            if ( ! unsigned.isValid("This sig hasn't signed anything and should be invalid.") ) {
                System.out.println("Test passed");
                showSignature(unsigned, null);
            }
            else {
                System.out.println("Test failed");
            }
        }
        catch (Exception e) {
            System.out.println(e.getClass().getSimpleName() + " [" + e.getMessage() + "] when testing unsigned signature");
            e.printStackTrace();
        }
    }


    /**
     * Write information about the signature to System.out. Used only to help test the class.
     * @param sig A signature to display.
     * @param doc The document the signature applies to.
     */
    private static final void showSignature(ElectronicSignature sig, String doc)
    {
        String signer = sig.getSigner();
        Date d = sig.getSigningDate();

        System.out.println(" toString... " + sig);
        System.out.println(" retrieved values:");
        System.out.println("   date=" + d);
        System.out.println("   signer=" + signer);
        System.out.println(" Valid: " + sig.isValid(doc));
        System.out.println();
    }

}
