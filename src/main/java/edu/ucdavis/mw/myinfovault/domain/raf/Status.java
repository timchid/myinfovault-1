/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: Status.java
 */

package edu.ucdavis.mw.myinfovault.domain.raf;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import edu.ucdavis.mw.myinfovault.domain.action.StatusType;

/**
 * Recommended action form status.
 *
 * @author Craig Gilmore
 * @since MIV 3.0
 */
public class Status implements Serializable
{
    private static final long serialVersionUID = 200904101607L;//-1737933765408912603L;

    private final StatusType statusType;
    private int id;
    private List<Rank> ranks;
    private int insertUserId;
    private java.sql.Date insertTimestamp;

    /**
     * Create a status object form a database record.
     *
     * @param id
     * @param statusType
     * @param ranks
     * @param insertUserId
     * @param insertTimestamp
     */
    public Status(int id,
                  StatusType statusType,
                  List<Rank> ranks,
                  int insertUserId,
                  java.sql.Date insertTimestamp)
    {
        this.id = id;
        this.statusType = statusType;
        this.ranks = ranks;
        this.insertUserId = insertUserId;
        this.insertTimestamp = insertTimestamp;
    }

    /**
     * Create a new status of the given type.
     *
     * @param statusType status type, either PRESENT or PROPOSED
     */
    public Status(StatusType statusType)
    {
        this(0,
             statusType,
             new ArrayList<Rank>(5),
             0,
             null);
    }

    /**
     * @return RAF status record ID
     */
    public int getId()
    {
        return id;
    }

    /**
     * @param id RAF status record ID
     */
    public void setId(int id)
    {
        this.id = id;
    }

    /**
     * @return RAF status type, either PRESENT or PROPOSED
     */
    public StatusType getStatusType()
    {
        return statusType;
    }

    /**
     * @return list of status rank and steps.
     */
    public List<Rank> getRanks()
    {
        return ranks;
    }

    /**
     * @return ID of the originally creating user
     */
    public int getInsertUserId()
    {
        return insertUserId;
    }

    /**
     * @return time the status was originally created
     */
    public java.sql.Date getInsertTimestamp()
    {
        return insertTimestamp;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        StringBuilder status = new StringBuilder();

        for (Iterator<Rank> it = ranks.iterator();it.hasNext();)
        {
            status.append(it.next().getRankAndStep());

            // append delimiter if there's a next element
            if (it.hasNext()) status.append("; ");
        }

        return status.toString();
    }
}
