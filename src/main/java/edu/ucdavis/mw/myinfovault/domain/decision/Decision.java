/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: Decision.java
 */

package edu.ucdavis.mw.myinfovault.domain.decision;

import java.io.File;
import java.io.Serializable;
import java.text.MessageFormat;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import edu.ucdavis.mw.myinfovault.domain.action.Assignment;
import edu.ucdavis.mw.myinfovault.domain.action.DossierActionType;
import edu.ucdavis.mw.myinfovault.domain.action.StatusType;
import edu.ucdavis.mw.myinfovault.domain.action.Title;
import edu.ucdavis.mw.myinfovault.domain.dossier.Dossier;
import edu.ucdavis.mw.myinfovault.domain.signature.SignableDocument;
import edu.ucdavis.mw.myinfovault.dossier.attributes.DossierAppointmentAttributeKey;
import edu.ucdavis.mw.myinfovault.dossier.attributes.DossierAttribute;
import edu.ucdavis.mw.myinfovault.service.MivServiceLocator;
import edu.ucdavis.mw.myinfovault.service.ScopeFilter;
import edu.ucdavis.mw.myinfovault.service.SearchFilter;
import edu.ucdavis.mw.myinfovault.service.dossier.DossierFileDto;
import edu.ucdavis.mw.myinfovault.service.person.Scope;
import edu.ucdavis.mw.myinfovault.util.MivDocument;
import edu.ucdavis.mw.myinfovault.util.StringUtil;
import edu.ucdavis.myinfovault.document.DocumentFormat;

/**
 * Signable Dean/Vice-Provost decision.
 *
 * @author Craig Gilmore
 * @since MIV 4.7
 */
public class Decision extends SignableDocument implements Serializable
{
    private static final long serialVersionUID = 201302131504L;

    /**
     * Decision choices available for appraisal actions.
     */
    private static final DecisionType[] APPRAISAL_CHOICES = {
        DecisionType.POSITIVE_APPRAISAL,
        DecisionType.POSITIVE_GUARDED_APPRAISAL,
        DecisionType.GUARDED_APPRAISAL,
        DecisionType.GUARDED_NEGATIVE_APPRAISAL,
        DecisionType.NEGATIVE_APPRAISAL};

    /**
     * Decision choices available for five year review actions.
     */
    private static final DecisionType[] FIVE_YEAR_REVIEW_CHOICES = {
        DecisionType.SATISFACTORY_REVIEW,
        DecisionType.SATISFACTORY_REVIEW_ADVANCEMENT,
        DecisionType.UNSATISFACTORY_REVIEW};

    /**
     * Decision choices available for five year, department chair review actions.
     */
    private static final DecisionType[] FIVE_YEAR_REVIEW_DEPARTMENT_CHAIR_CHOICES = {
        DecisionType.REAPPOINTMENT_APPROVED,
        DecisionType.REAPPOINTMENT_DENIED,
        DecisionType.OTHER};

    /**
     * Decision choices available for most actions.
     */
    private static final DecisionType[] DEFAULT_CHOICES = {
        DecisionType.APPROVED,
        DecisionType.DENIED,
        DecisionType.OTHER};

    /**
     * Format used in {@link #toString()}
     */
    private static final String TO_STRING = "\n*** Decision ***" +
                                            "\ndecisionType: {0}" +
                                            "\tcontraryToComittee: {1}" +
                                            "\tcomments: {2}" +
                                            "\n{3}";

    /**
     * Filename pattern for the decision PDF.
     */
    protected static final String DECISION_FILENAME = "{0}_{1,number,#}_{2,number,#}." + DocumentFormat.PDF.getFileExtension();

    private DecisionType decisionType = DecisionType.NONE;
    private boolean contraryToCommittee = false;
    private String comments = StringUtil.EMPTY_STRING;

    /**
     * Create decision from database record.
     *
     * @param dossier dossier to which the decision belongs
     * @param documentId record ID of the document to which this decision belongs
     * @param documentType signable document type
     * @param decisionType decision made by the dean/vice-post
     * @param contraryToCommittee if the decision was contrary to the committee
     * @param comments comments associated with the decision
     * @param schoolId ID of the school to which the department belongs
     * @param departmentId ID of the department to which the decision belongs
     */
    public Decision(Dossier dossier,
                    int documentId,
                    MivDocument documentType,
                    DecisionType decisionType,
                    boolean contraryToCommittee,
                    String comments,
                    int schoolId,
                    int departmentId)
    {
        super(dossier, documentId, documentType, schoolId, departmentId);

        this.decisionType = decisionType;
        this.contraryToCommittee = contraryToCommittee;
        this.comments = comments;
    }

    /**
     * Create a new decision.
     *
     * @param dossier dossier to which the decision belongs
     * @param documentType signable document type
     * @param schoolId ID of the school to which the department belongs
     * @param departmentId ID of the department to which the decision belongs
     */
    public Decision(Dossier dossier,
                    MivDocument documentType,
                    int schoolId,
                    int departmentId)
    {
        super(dossier, 0, documentType, schoolId, departmentId);
    }

    /**
     * @return type of decision made
     */
    public DecisionType getDecisionType()
    {
        return decisionType;
    }

    /**
     * @param decisionType type of decision made
     */
    public void setDecisionType(DecisionType decisionType)
    {
        this.decisionType = decisionType;
    }

    /**
     * @return if the decision made is contrary to the committee recommendation
     */
    public boolean isContraryToCommittee()
    {
        return contraryToCommittee;
    }

    /**
     * @param contraryToCommittee if the decision made is contrary to the committee recommendation
     */
    public void setContraryToCommittee(boolean contraryToCommittee)
    {
        this.contraryToCommittee = contraryToCommittee;
    }

    /**
     * @return Dean/Vice Provost decision/recommendation comments
     */
    public String getComments()
    {
        return comments;
    }

    /**
     * @param comments Dean/Vice Provost decision/recommendation comments
     */
    public void setComments(String comments)
    {
        this.comments = comments;
    }

    /**
     * @return wet signature for this decision
     */
    public File getWetSignature()
    {
        DossierAttribute dossierAttribute = getDossier().getAttributesByAppointment(
                                                             new DossierAppointmentAttributeKey(getSchoolId(),
                                                                                                getDepartmentId()))
                                                        .getAttribute(getDocumentType().getWetSignatureAttributeName());
        if (dossierAttribute != null)
        {
            Iterator<DossierFileDto> attributeDocuments = dossierAttribute.getDocuments().iterator();

            if (attributeDocuments.hasNext())
            {
                return attributeDocuments.next().getFilePath();
            }
        }

        return null;// none found
    }

    /**
     * @return if the decision type is valid for this decision
     */
    public boolean isValid()
    {
        return Arrays.asList(getDecisionTypes()).contains(getDecisionType());
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.domain.signature.SignableDocument#getDocumentType()
     */
    @Override
    public MivDocument getDocumentType()
    {
        /*
         * Provost tenure document types depend on the decision type chosen.
         * Tenure DENIED is a recommendation; a decision otherwise.
         */
        if (super.getDocumentType() == MivDocument.PROVOSTS_TENURE_DECISION
         || super.getDocumentType() == MivDocument.PROVOSTS_TENURE_RECOMMENDATION)
        {
            return decisionType == DecisionType.DENIED
                 ? MivDocument.PROVOSTS_TENURE_RECOMMENDATION
                 : MivDocument.PROVOSTS_TENURE_DECISION;
        }
        else if (super.getDocumentType() == MivDocument.PROVOSTS_TENURE_APPEAL_DECISION
              || super.getDocumentType() == MivDocument.PROVOSTS_TENURE_APPEAL_RECOMMENDATION)
        {
            return decisionType == DecisionType.DENIED
                 ? MivDocument.PROVOSTS_TENURE_APPEAL_RECOMMENDATION
                 : MivDocument.PROVOSTS_TENURE_APPEAL_DECISION;
        }

        // else, return the predetermined type
        return super.getDocumentType();
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.domain.signature.SignableDocument#getFilenamePattern()
     */
    @Override
    protected String getFilenamePattern()
    {
        return DECISION_FILENAME;
    }

    /**
     * @return description of this decision type made
     */
    public String getDecisionTypeDescription()
    {
        return getDecisionType().getDescription(getDocumentType());
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        return MessageFormat.format(TO_STRING, decisionType,
                                    contraryToCommittee, comments, super.toString());
    }

    /**
     * @return colon delimited list of present assignment titles
     */
    public String getPresent()
    {
        return getDetail(StatusType.PRESENT);
    }

    /**
     * @return colon delimited list of proposed assignment titles
     */
    public String getProposed()
    {
        return getDetail(StatusType.PROPOSED);
    }

    /**
     * @param type present or proposed status
     * @return colon delimited list of assignment titles for the given status
     */
    public String getDetail(StatusType type)
    {
        if (this.getDossier().getAction().getActionType()
                == DossierActionType.APPOINTMENT
                || this.getDossier().isAcademicActionFormPresent())
        {
            List<Assignment> assignments = MivServiceLocator.getAcademicActionService()
                                                            .getBo(getDossier())
                                                            .getAssignmentStatuses()
                                                            .get(type);

            if (assignments == null) return StringUtil.EMPTY_STRING;

            if (getDocumentType() == MivDocument.JOINT_DEANS_RECOMMENDATION)
            {
                SearchFilter<Assignment> filter = new ScopeFilter<Assignment>(getSchoolId(), getDepartmentId()) {
                    @Override
                    protected Scope getScope(Assignment item) {
                        return item.getScope();
                    }
                };

                assignments = filter.apply(assignments);
            }

            StringBuilder builder = new StringBuilder();

            for (Assignment assignment : assignments)
            {
                for (Title title : assignment.getTitles())
                {
                    if (builder.length() > 0) builder.append("; ");

                    builder.append(title.getDescription())
                           .append(" Step: ")
                           .append(title.getStep() == null || title.getStep().getValue() == null ? "NA" : title.getStep());
                }
            }

            return builder.toString();
        }
        else
        {
            return MivServiceLocator.getRafService()
                                    .getBo(getDossier())
                                    .getStatus(type).toString();
        }
    }


    /**
     * Get the valid decision choices for this decision.
     *
     * @return valid decision types
     */
    public DecisionType[] getDecisionTypes()
    {
        switch (getDossier().getAction().getActionType())
        {
            case APPRAISAL:
                return APPRAISAL_CHOICES;
            case FIVE_YEAR_REVIEW:
                return FIVE_YEAR_REVIEW_CHOICES;
            case FIVE_YEAR_REVIEW_DEPARTMENT_CHAIR:
                return  FIVE_YEAR_REVIEW_DEPARTMENT_CHAIR_CHOICES;
            default:
                return  DEFAULT_CHOICES;
        }
    }
}
