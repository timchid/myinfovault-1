/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: DomainObject.java
 */


package edu.ucdavis.mw.myinfovault.domain;

import org.springframework.beans.factory.InitializingBean;

/**
 * Base Domain Object
 * @author Brij Garg
 * @since MIV 2.1
 */
public abstract class DomainObject implements InitializingBean
{
    /** The record ID from the database, or 0 (zero) for a newly created object not yet stored in the DB */
    protected int id = 0;
    private int userId = -1;

    /**
     * No-arg constructor for de-serialization to use.
     * <strong>Don't use this constructor programatically!</strong>
     * "Protected" so subclasses can access it but the world can't.
     */
    protected DomainObject() {}

    public DomainObject(int id)
    {
        this.id = id;
    }

    public DomainObject(int id, int userId)
    {
        this.id = id;
        this.userId = userId;
    }

    /**
     * Invoked by a BeanFactory after it has set all bean properties supplied
     */
    public void afterPropertiesSet() throws Exception
    {
        //  If required, perform initialization or validation here
    }

    /**
     * @return the id
     */
    public int getId()
    {
        return id;
    }

    /**
     * Setter for the database record ID number. The ID can only be set once, when this object
     * represents a new record and therefore has an ID of zero. An existing ID > 0 can not be
     * changed by this setter.
     * @param id Identifier number of the newly saved object.
     */
    public void setId(int id)
    {
        if (this.id > 0) {
            throw new IllegalArgumentException("The object ID can only be set when it is zero");
        }

        this.id = id;
    }


    public int clearId()
    {
        int oldId = this.id;
        this.id = 0;
        return oldId;
    }

    public void setUserId(int userId)
    {
        if (this.id > 0 && this.userId != userId) {
            throw new IllegalArgumentException("The user ID can only be set when it is zero");
        }

        this.userId = userId;
    }

    /**
     * @return the userID
     */
    public int getUserID()
    {
        return userId;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode()
    {
//        final int prime = 31;
//        int result = 1;
//        result = prime * result + userID;
//        return result;
        return super.hashCode();
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj)
    {
        if (this == obj) return true;
        if (obj == null) return false;
        if (!(obj instanceof DomainObject)) return false;
        return true;
    }
}
