package edu.ucdavis.mw.myinfovault.domain.biosketch;

import edu.ucdavis.mw.myinfovault.domain.DomainObject;

/**
 * Domain Object representing a Section
 * @author Brij Garg
 * @since MIV 2.1
 */
public class Section extends DomainObject
{
    public Section(int id)
    {
        super(id);
        // TODO Auto-generated constructor stub
    }

    private String name;
    private String recordName;
    private String recordQuery;

    /**
     * @return the name
     */
    public String getName()
    {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name)
    {
        this.name = name;
    }

    /**
     * @return the recordName
     */
    public String getRecordName()
    {
        return recordName;
    }

    /**
     * @param recordName the recordName to set
     */
    public void setRecordName(String recordName)
    {
        this.recordName = recordName;
    }

    /**
     * @return the recordQuery
     */
    public String getRecordQuery()
    {
        return recordQuery;
    }

    /**
     * @param recordQuery the recordQuery to set
     */
    public void setRecordQuery(String recordQuery)
    {
        this.recordQuery = recordQuery;
    }

}
