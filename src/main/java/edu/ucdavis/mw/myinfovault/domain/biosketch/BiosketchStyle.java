package edu.ucdavis.mw.myinfovault.domain.biosketch;

import edu.ucdavis.mw.myinfovault.domain.DomainObject;
import edu.ucdavis.myinfovault.designer.Alignment;

/**
 * Domain Object representing Biosketch Styles
 * @author Brij Garg
 * @since MIV 2.1
 */
public class BiosketchStyle extends DomainObject
{

    private String styleName;
    private int pageWidth;
    private int pageHeight;
    private int marginTop;
    private int marginRight;
    private int marginBottom;
    private int marginLeft;
    private int titleFontID;
    private int titleSize;
    private Alignment titleAlign = Alignment.CENTER;
    private boolean titleBold;
    private boolean titleItalic;
    private boolean titleUnderline;
    private boolean titleRules;
    private boolean titleEditable;
    private Alignment displayNameAign = Alignment.CENTER;
    private int headerFontID;
    private int headerSize;
    private Alignment headerAlign = Alignment.LEFT;
    private boolean headerBold;
    private boolean headerItalic;
    private boolean headerUnderline;
    private int headerIndent;
    private int bodyFontID;
    private int bodySize;
    private int bodyIndent;
    private boolean bodyFormatting;
    private boolean footerOn;
    private boolean footerPageNumbers;
    private boolean footerRules;
    private int citationFormatID;

    /**
     * @param userId 
     */
    public BiosketchStyle(int id, int userId)
    {
        super(id, userId);
    }

    /**
     * @param userId
     * @param base
     */
    public BiosketchStyle(int userId, BiosketchStyle base)
    {
        super(0, userId);
        this.styleName = base.getStyleName();
        this.pageWidth = base.getPageWidth();
        this.pageHeight = base.getPageHeight();
        this.marginTop = base.getMarginTop();
        this.marginRight = base.getMarginRight();
        this.marginBottom = base.getMarginBottom();
        this.marginLeft = base.getMarginLeft();
        this.titleFontID = base.getTitleFontID();
        this.titleSize = base.getTitleSize();
        this.titleAlign = base.getTitleAlign();
        this.titleBold = base.isTitleBold();
        this.titleItalic = base.isTitleItalic();
        this.titleUnderline = base.isTitleUnderline();
        this.titleRules = base.isTitleRules();
        this.titleEditable = base.isTitleEditable();
        this.displayNameAign = base.getDisplayNameAign();
        this.headerFontID = base.getHeaderFontID();
        this.headerSize = base.getHeaderSize();
        this.headerAlign = base.getHeaderAlign();
        this.headerBold = base.isHeaderBold();
        this.headerItalic = base.isHeaderItalic();
        this.headerUnderline = base.isHeaderUnderline();
        this.headerIndent = base.getHeaderIndent();
        this.bodyFontID = base.getBodyFontID();
        this.bodySize = base.getBodySize();
        this.bodyIndent = base.getBodyIndent();
        this.bodyFormatting = base.isBodyFormatting();
        this.footerOn = base.isFooterOn();
        this.footerPageNumbers = base.isFooterPageNumbers();
        this.footerRules = base.isFooterRules();
        this.citationFormatID = base.getCitationFormatID();
    }

    /**
     * @return the bodyFontID
     */
    public int getBodyFontID()
    {
        return bodyFontID;
    }

    /**
     * @param bodyFontID
     *            the bodyFontID to set
     */
    public void setBodyFontID(int bodyFontID)
    {
        this.bodyFontID = bodyFontID;
        if (this.id < 0)
        {
            this.id = 0;
        }
    }

    /**
     * @return the bodyFormatting
     */
    public boolean isBodyFormatting()
    {
        return bodyFormatting;
    }

    /**
     * @param bodyFormatting
     *            the bodyFormatting to set
     */
    public void setBodyFormatting(boolean bodyFormatting)
    {
        this.bodyFormatting = bodyFormatting;
        if (this.id < 0)
        {
            this.id = 0;
        }
    }

    /**
     * @return the bodyIndent
     */
    public int getBodyIndent()
    {
        return bodyIndent;
    }

    /**
     * @param bodyIndent
     *            the bodyIndent to set
     */
    public void setBodyIndent(int bodyIndent)
    {
        this.bodyIndent = bodyIndent;
        if (this.id < 0)
        {
            this.id = 0;
        }
    }

    /**
     * @return the bodySize
     */
    public int getBodySize()
    {
        return bodySize;
    }

    /**
     * @param bodySize
     *            the bodySize to set
     */
    public void setBodySize(int bodySize)
    {
        this.bodySize = bodySize;
        if (this.id < 0)
        {
            this.id = 0;
        }
    }

    /**
     * @return the footerOn
     */
    public boolean isFooterOn()
    {
        return footerOn;
    }

    /**
     * @param footerOn
     *            the footerOn to set
     */
    public void setFooterOn(boolean footerOn)
    {
        this.footerOn = footerOn;
        if (this.id < 0)
        {
            this.id = 0;
        }
    }

    /**
     * @return the footerPageNumbers
     */
    public boolean isFooterPageNumbers()
    {
        return footerPageNumbers;
    }

    /**
     * @param footerPageNumbers
     *            the footerPageNumbers to set
     */
    public void setFooterPageNumbers(boolean footerPageNumbers)
    {
        this.footerPageNumbers = footerPageNumbers;
        if (this.id < 0)
        {
            this.id = 0;
        }
    }

    /**
     * @return the footerRules
     */
    public boolean isFooterRules()
    {
        return footerRules;
    }

    /**
     * @param footerRules
     *            the footerRules to set
     */
    public void setFooterRules(boolean footerRules)
    {
        this.footerRules = footerRules;
        if (this.id < 0)
        {
            this.id = 0;
        }
    }

    /**
     * @return the headerAlign
     */
    public Alignment getHeaderAlign()
    {
        return headerAlign;
    }

    /**
     * @param headerAlign
     *            the headerAlign to set
     */
    public void setHeaderAlign(Alignment headerAlign)
    {
        this.headerAlign = headerAlign;
        if (this.id < 0)
        {
            this.id = 0;
        }
    }

    /**
     * @return the headerFontID
     */
    public int getHeaderFontID()
    {
        return headerFontID;
    }

    /**
     * @param headerFontID
     *            the headerFontID to set
     */
    public void setHeaderFontID(int headerFontID)
    {
        this.headerFontID = headerFontID;
        if (this.id < 0)
        {
            this.id = 0;
        }
    }

    /**
     * @return the headerIndent
     */
    public int getHeaderIndent()
    {
        return headerIndent;
    }

    /**
     * @param headerIndent
     *            the headerIndent to set
     */
    public void setHeaderIndent(int headerIndent)
    {
        this.headerIndent = headerIndent;
        if (this.id < 0)
        {
            this.id = 0;
        }
    }

    /**
     * @return the headerSize
     */
    public int getHeaderSize()
    {
        return headerSize;
    }

    /**
     * @param headerSize
     *            the headerSize to set
     */
    public void setHeaderSize(int headerSize)
    {
        this.headerSize = headerSize;
        if (this.id < 0)
        {
            this.id = 0;
        }
    }

    /**
     * @return the marginBottom
     */
    public int getMarginBottom()
    {
        return marginBottom;
    }

    /**
     * @param marginBottom
     *            the marginBottom to set
     */
    public void setMarginBottom(int marginBottom)
    {
        this.marginBottom = marginBottom;
        if (this.id < 0)
        {
            this.id = 0;
        }
    }

    /**
     * @return the marginLeft
     */
    public int getMarginLeft()
    {
        return marginLeft;
    }

    /**
     * @param marginLeft
     *            the marginLeft to set
     */
    public void setMarginLeft(int marginLeft)
    {
        this.marginLeft = marginLeft;
        if (this.id < 0)
        {
            this.id = 0;
        }
    }

    /**
     * @return the marginRight
     */
    public int getMarginRight()
    {
        return marginRight;
    }

    /**
     * @param marginRight
     *            the marginRight to set
     */
    public void setMarginRight(int marginRight)
    {
        this.marginRight = marginRight;
        if (this.id < 0)
        {
            this.id = 0;
        }
    }

    /**
     * @return the marginTop
     */
    public int getMarginTop()
    {
        return marginTop;
    }

    /**
     * @param marginTop
     *            the marginTop to set
     */
    public void setMarginTop(int marginTop)
    {
        this.marginTop = marginTop;
        if (this.id < 0)
        {
            this.id = 0;
        }
    }

    /**
     * @return the pageHeight
     */
    public int getPageHeight()
    {
        return pageHeight;
    }

    /**
     * @param pageHeight
     *            the pageHeight to set
     */
    public void setPageHeight(int pageHeight)
    {
        this.pageHeight = pageHeight;
        if (this.id < 0)
        {
            this.id = 0;
        }
    }

    /**
     * @return the pageWidth
     */
    public int getPageWidth()
    {
        return pageWidth;
    }

    /**
     * @param pageWidth
     *            the pageWidth to set
     */
    public void setPageWidth(int pageWidth)
    {
        this.pageWidth = pageWidth;
        if (this.id < 0)
        {
            this.id = 0;
        }
    }

    /**
     * @return the styleName
     */
    public String getStyleName()
    {
        return styleName;
    }

    /**
     * @param styleName
     *            the styleName to set
     */
    public void setStyleName(String styleName)
    {
        this.styleName = styleName;
        if (this.id < 0)
        {
            this.id = 0;
        }
    }

    /**
     * @return the titleAlign
     */
    public Alignment getTitleAlign()
    {
        return titleAlign;
    }

    /**
     * @param titleAlign
     *            the titleAlign to set
     */
    public void setTitleAlign(Alignment titleAlign)
    {
        this.titleAlign = titleAlign;
        if (this.id < 0)
        {
            this.id = 0;
        }
    }

    /**
     * @return the titleFontID
     */
    public int getTitleFontID()
    {
        return titleFontID;
    }

    /**
     * @param titleFontID
     *            the titleFontID to set
     */
    public void setTitleFontID(int titleFontID)
    {
        this.titleFontID = titleFontID;
        if (this.id < 0)
        {
            this.id = 0;
        }
    }

    /**
     * @return the titleRules
     */
    public boolean isTitleRules()
    {
        return titleRules;
    }

    /**
     * @param titleRules
     *            the titleRules to set
     */
    public void setTitleRules(boolean titleRules)
    {
        this.titleRules = titleRules;
        if (this.id < 0)
        {
            this.id = 0;
        }
    }

    /**
     * @return the titleEditable
     */
    public boolean isTitleEditable()
    {
        return titleEditable;
    }

    /**
     * @param titleEditable if title is editable
     */
    public void setTitleEditable(boolean titleEditable)
    {
        this.titleEditable = titleEditable;
        if (this.id < 0)
        {
            this.id = 0;
        }
    }

    /**
     * @return the titleSize
     */
    public int getTitleSize()
    {
        return titleSize;
    }

    /**
     * @param titleSize
     *            the titleSize to set
     */
    public void setTitleSize(int titleSize)
    {
        this.titleSize = titleSize;
        if (this.id < 0)
        {
            this.id = 0;
        }
    }

    /**
     * @return the citationFormatID
     */
    public int getCitationFormatID()
    {
        return citationFormatID;
    }

    /**
     * @param citationFormatID
     *            the citationFormatID to set
     */
    public void setCitationFormatID(int citationFormatID)
    {
        this.citationFormatID = citationFormatID;
        if (this.id < 0)
        {
            this.id = 0;
        }
    }

    /**
     * @return the displayNameAlign
     */
    public Alignment getDisplayNameAign()
    {
        return displayNameAign;
    }

    /**
     * @param displayNameAign
     *            the displayNameAlign to set
     */
    public void setDisplayNameAign(Alignment displayNameAign)
    {
        this.displayNameAign = displayNameAign;
    }

    /**
     * @return the headerBold
     */
    public boolean isHeaderBold()
    {
        return headerBold;
    }

    /**
     * @param headerBold
     *            the headerBold to set
     */
    public void setHeaderBold(boolean headerBold)
    {
        this.headerBold = headerBold;
    }

    /**
     * @return the headerItalic
     */
    public boolean isHeaderItalic()
    {
        return headerItalic;
    }

    /**
     * @param headerItalic
     *            the headerItalic to set
     */
    public void setHeaderItalic(boolean headerItalic)
    {
        this.headerItalic = headerItalic;
    }

    /**
     * @return the headerUnderLine
     */
    public boolean isHeaderUnderline()
    {
        return headerUnderline;
    }

    /**
     * @param headerUnderline
     *            the headerUnderline to set
     */
    public void setHeaderUnderline(boolean headerUnderline)
    {
        this.headerUnderline = headerUnderline;
    }

    /**
     * @return the titleBold
     */
    public boolean isTitleBold()
    {
        return titleBold;
    }

    /**
     * @param titleBold
     *            the titleBold to set
     */
    public void setTitleBold(boolean titleBold)
    {
        this.titleBold = titleBold;
    }

    /**
     * @return the titleItalic
     */
    public boolean isTitleItalic()
    {
        return titleItalic;
    }

    /**
     * @param titleItalic
     *            the titleItalic to set
     */
    public void setTitleItalic(boolean titleItalic)
    {
        this.titleItalic = titleItalic;
    }

    /**
     * @return the titleUnderline
     */
    public boolean isTitleUnderline()
    {
        return titleUnderline;
    }

    /**
     * @param titleUnderline
     *            the titleUnderlline to set
     */
    public void setTitleUnderline(boolean titleUnderline)
    {
        this.titleUnderline = titleUnderline;
    }
}
