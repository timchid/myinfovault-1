package edu.ucdavis.mw.myinfovault.domain.biosketch;

import edu.ucdavis.mw.myinfovault.domain.DomainObject;

/**
 * Domain Object representing Biosketch Limits
 * @author Brij Garg
 * @since MIV 2.1
 */
public class BiosketchFormatLimits extends DomainObject
{
    private int biosketchType;
    private int pageWidthMin;
    private int pageWidthMax;
    private int pageHeightMin;
    private int pageHeightMax;
    private int marginMin;
    private int marginMax;
    private int headerSizeMin;
    private int headerSizeMax;
    private int headerIndentMin;
    private int headerIndentMax;
    private int bodySizeMin;
    private int bodySizeMax;
    private int bodyIndentMin;
    private int bodyIndentMax;
   
    public BiosketchFormatLimits(int id)
    {
        super(id);
    }

    /**
     * @return the bodyIndentMax
     */
    public int getBodyIndentMax()
    {
        return bodyIndentMax;
    }

    /**
     * @return the bodyIndentMin
     */
    public int getBodyIndentMin()
    {
        return bodyIndentMin;
    }

    /**
     * @return the bodySizeMax
     */
    public int getBodySizeMax()
    {
        return bodySizeMax;
    }

    /**
     * @return the bodySizeMin
     */
    public int getBodySizeMin()
    {
        return bodySizeMin;
    }

    /**
     * @return the biosketchType
     */
    public int getBiosketchType()
    {
        return biosketchType;
    }

    /**
     * @return the headerIndentMax
     */
    public int getHeaderIndentMax()
    {
        return headerIndentMax;
    }

    /**
     * @return the headerIndentMin
     */
    public int getHeaderIndentMin()
    {
        return headerIndentMin;
    }

    /**
     * @return the headerSizeMax
     */
    public int getHeaderSizeMax()
    {
        return headerSizeMax;
    }

    /**
     * @return the headerSizeMin
     */
    public int getHeaderSizeMin()
    {
        return headerSizeMin;
    }

    /**
     * @return the marginMax
     */
    public int getMarginMax()
    {
        return marginMax;
    }

    /**
     * @return the marginMin
     */
    public int getMarginMin()
    {
        return marginMin;
    }

    /**
     * @return the pageHeightMax
     */
    public int getPageHeightMax()
    {
        return pageHeightMax;
    }

    /**
     * @return the pageHeightMin
     */
    public int getPageHeightMin()
    {
        return pageHeightMin;
    }

    /**
     * @return the pageWidthMax
     */
    public int getPageWidthMax()
    {
        return pageWidthMax;
    }

    /**
     * @return the pageWidthMin
     */
    public int getPageWidthMin()
    {
        return pageWidthMin;
    }


}
