package edu.ucdavis.mw.myinfovault.domain.biosketch;

import edu.ucdavis.mw.myinfovault.domain.DomainObject;

/**
 * @author Brij Garg
 * @since MIV 2.1
 */
public class Fonts extends DomainObject
{

    private String name;
    private String font;
    
    /**
     * @param id
     */
    public Fonts(int id)
    {
        super(id);
    }

    /**
     * @return the font
     */
    public String getFont()
    {
        return font;
    }

    /**
     * @return the name
     */
    public String getName()
    {
        return name;
    }

}
