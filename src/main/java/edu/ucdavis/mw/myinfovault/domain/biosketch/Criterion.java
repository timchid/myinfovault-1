package edu.ucdavis.mw.myinfovault.domain.biosketch;

import edu.ucdavis.mw.myinfovault.domain.DomainObject;

/**
 * A search condition
 * @author Stephen Paulsen
 * @since MIV 2.1
 */
public class Criterion extends DomainObject
{
    public enum RelOp {
        NE("!="), LT("<"), LE("<="), EQ("="), GE(">="), GT(">");

        private String opSymbol = "??";

        private RelOp(String op)
        {
            this.opSymbol = op;
        }

        /** Get the SQL / C / Java etc relational operator string. */
        public String relopString()
        {
            return this.opSymbol;
        }
    }

    /* 'n' and 'counter' are for testing and debug only,
     * to give 'value' an initial value.
     */
    static private int n = 0;
    private int counter = ++n;

    private String field = "field" + this.counter;
    private RelOp operator = RelOp.EQ;
    private String value = "" + n;
    private int rulesetID;

    public Criterion()
    {
        this(0);
    }

    public Criterion(int id)
    {
        super(id);
    }

    public Criterion setField(String field)
    {
        this.field = field;
        return this;
    }

    public String getField()
    {
        return this.field;
    }

    public Criterion setOperator(RelOp op)
    {
        this.operator = op;
        return this;
    }

    public RelOp getOperator()
    {
        return operator;
    }

    public Criterion setValue(String s)
    {
        this.value = s;
        return this;
    }

    public String getValue()
    {
        return value;
    }

    @Override
    public String toString()
    {
        return this.field + " " + this.operator.relopString() + " " + "'" + this.value + "'";
    }

    public int getRulesetID()
    {
        return rulesetID;
    }

    public void setRulesetID(int rulesetID)
    {
        this.rulesetID = rulesetID;
    }
}
