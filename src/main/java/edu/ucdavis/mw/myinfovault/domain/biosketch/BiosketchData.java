package edu.ucdavis.mw.myinfovault.domain.biosketch;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import edu.ucdavis.mw.myinfovault.domain.DomainObject;
import edu.ucdavis.myinfovault.data.DisplaySection;

/**
 * Domain Object containing the biosketch data (sections/records) required for Design My Biosketch and Create My Biosketch
 * @author Brij Garg
 * @since MIV 2.1
 */
public class BiosketchData extends DomainObject
{
    // BiosketchExclude is required to evaluate those records which the user has opted not to include in the packet
    private List<BiosketchExclude> biosketchExcludeList = null;

    //TODO MIV 2.0 uses DisplaySection to display a Section with set of records on the page
    // In MIV 2.1 we might end up using the same object or create a new one.
    private List<DisplaySection> displaySectionList = null;
    
    private Map<String,Map<String,String>> sectionHeaderMap = null;

    public BiosketchData(int id, int userID)
    {
        //id is the biosketch id
        super(id, userID);
    }

    /**
     * @return the biosketch id
     */
    public int getBiosketchId()
    {
        return getId();
    }

    /**
     * @return the Biosketch Exclude List
     */
    public List<BiosketchExclude> getBiosketchExcludeList()
    {
        return biosketchExcludeList;
    }

    /**
     * @param biosketchExcludeList the Biosketch Exclude List
     */
    public void setBiosketchExcludeList(Collection<BiosketchExclude> biosketchExcludeList)
    {
        if (biosketchExcludeList instanceof List)
        {
            this.biosketchExcludeList = (List<BiosketchExclude>) biosketchExcludeList;
        }
        else
        {
            this.biosketchExcludeList = new ArrayList<BiosketchExclude>(biosketchExcludeList);
        }
    }

    /**
     * @return the Display Section List
     */
    public Iterable<DisplaySection> getDisplaySectionList()
    {
        return displaySectionList;
    }

    /**
     * @param displaySectionList the Display Section List
     */
    public void setDisplaySectionList(List<DisplaySection> displaySectionList)
    {
        this.displaySectionList = displaySectionList;
    }

    public Map<String, Map<String, String>> getSectionHeaderMap()
    {
        return sectionHeaderMap;
    }

    public void setSectionHeaderMap(Map<String, Map<String, String>> sectionHeaderMap)
    {
        this.sectionHeaderMap = sectionHeaderMap;
    }
}
