/**
 * Copyright © The Regents of the University of California, Davis campus. 
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: MivApplicationContext.java
 */
package edu.ucdavis.mw.myinfovault.system;

import org.springframework.context.ApplicationContext; 


/**
 * This class provides application-wide access to the Spring ApplicationContext.
 * The ApplicationContext is injected by the class "ApplicationContextProvider".
 *
 * Example call:
 * MivApplicationContext.getApplicationContext().getBean("myBeanName")
 */

public class MivApplicationContext { 
	private static ApplicationContext ctx; 
	/**
	 * Injected from the class "ApplicationContextProvider" which is automatically
	 * loaded during Spring-Initialization.
	 */ 
	public static void setApplicationContext(ApplicationContext applicationContext) { 
		ctx = applicationContext; 
	} 
	/**
	 * Get access to the Spring ApplicationContext from everywhere in your Application.
	 *
	 * @return
	 */ 
	public static ApplicationContext getApplicationContext() { 
		return ctx; 
	} 
}  
