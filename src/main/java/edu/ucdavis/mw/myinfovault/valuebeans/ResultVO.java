/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: ResultVO.java
 */

package edu.ucdavis.mw.myinfovault.valuebeans;

import java.util.Arrays;
import java.util.List;

/**
 * Hold the results of request execution.
 *
 * @author pradeeph
 * @since MIV 4.4
 */
public class ResultVO
{
    /**
     * Request execution result default value.
     */
    public static final String DEFAULT_VALUE="[null]";
    private boolean success = false;
    private List<String> messages = null;
    private List<String> errorfields = null;
    private String id = null;
    private String value = DEFAULT_VALUE;
    private long timestamp = -1;


    /**
     * Basic result constructor.
     *
     * @param success indicate success success (true) or failure (false)
     */
    public ResultVO(boolean success)
    {
        this(success, null, null, null, DEFAULT_VALUE);
    }

    /**
     * Constructor with the a result message.
     *
     * @param success indicate success success (true) or failure (false)
     * @param messages
     */
    public ResultVO(boolean success, String message)
    {
        this(success, Arrays.asList(message), null, null, DEFAULT_VALUE);
    }

    /**
     * Constructor with the a result message and a error field.
     *
     * @param success indicate success success (true) or failure (false)
     * @param messages
     * @param field
     */
    public ResultVO(boolean success, String message, String field)
    {
        this(success, Arrays.asList(message), Arrays.asList(field), null, DEFAULT_VALUE);
    }


    /**
     * Constructor with the list of result messages.
     *
     * @param success indicate success success (true) or failure (false)
     * @param messages
     */
    public ResultVO(boolean success, List<String> messages)
    {
        this(success, messages, null, null, DEFAULT_VALUE);
    }

    /**
     * Constructor with the list of result messages and list of error fields.
     *
     * @param success indicate success success (true) or failure (false)
     * @param messages
     * @param errorfields
     */
    public ResultVO(boolean success, List<String> messages, List<String> errorfields)
    {
        this(success, messages, errorfields, null, DEFAULT_VALUE);
    }


    /**
     * Use this constructor only when you need to modify an error field ID or are sending a result ID.
     *
     * @param success indicate success success (true) or failure (false)
     * @param messages
     * @param errorfields
     * @param id
     */
    public ResultVO(boolean success, List<String> messages, List<String> errorfields, String id)
    {
        this(success, messages, errorfields, id, DEFAULT_VALUE);
    }


    /**
     * Use this constructor only when you need to modify an error field ID, are sending a result ID,
     * or sending modified values.
     * This is useful when validating date fields and sending back sql safe format as a result value.
     *
     * @param success indicate success success (true) or failure (false)
     * @param messages
     * @param errorfields
     * @param id
     * @param value
     */
    public ResultVO(boolean success, List<String> messages, List<String> errorfields, String id, String value)
    {
        this.success = success;
        this.messages = messages;
        this.errorfields = errorfields;
        this.id = id;
        this.value = value;
        this.timestamp = System.currentTimeMillis();
    }

    /**
     * Set the value
     *
     * @param value
     * @return self reference for chained invocation
     */
    public ResultVO setValue(String value)
    {
        this.value = value;
        return this;
    }


    /**
     * @return the success
     */
    public boolean isSuccess()
    {
        return success;
    }

    /**
     * @return the messages
     */
    public List<String> getMessages()
    {
        return messages;
    }

    /**
     * @return the errorfields
     */
    public List<String> getErrorfields()
    {
        return errorfields;
    }

    /**
     * @return the id
     */
    public String getId()
    {
        return id;
    }

    /**
     * @return the value
     */
    public String getValue()
    {
        return value;
    }

    /**
     * @return the timestamp
     */
    public long getTimestamp()
    {
        return timestamp;
    }


    /**
     * Produce a JSON string to use in AJAX responses.
     */
    @Override
    public String toString()
    {
        return "{success:" + this.success + ", id:" + this.id + ", timestamp:" + this.timestamp + "}";
    }

}
