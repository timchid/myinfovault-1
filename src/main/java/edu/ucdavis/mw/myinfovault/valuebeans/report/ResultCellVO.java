/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: ResultCellVO.java
 */

package edu.ucdavis.mw.myinfovault.valuebeans.report;

/**
 * Value Object to hold the required data for cell of report result.
 *
 * @author Pradeep K Haldiya
 * @since MIV 4.4.2
 */
public class ResultCellVO
{
    private String value = "";
    private boolean linkRequired = false;
    private String linkUrl = "";
    private String linkTitle = "";


    /**
     * Restrict to use no argument constructor
     */
    ResultCellVO(){}

    /**
     * TODO: add Javadoc
     * 
     * @param value
     */
    public ResultCellVO(String value)
    {
        this.value = value;
    }

    /**
     * Constructor to create result's record cell
     * @param value : value of the cell
     * @param linkUrl : provide the partial url link and _flowExecutionKey added by page to the link.
     * @param linkTitle
     */
    public ResultCellVO(String value,String linkUrl,String linkTitle)
    {
        this.value = value;

        if(linkUrl!=null)
        {
            this.linkRequired = true;
            this.linkUrl = linkUrl;
            this.linkTitle = linkTitle;
        }
    }

    /**
     * @return the value
     */
    public String getValue()
    {
        return value;
    }

    /**
     * @return the linkRequired
     */
    public boolean isLinkRequired()
    {
        return linkRequired;
    }

    /**
     * @return the linkUrl
     */
    public String getLinkUrl()
    {
        return linkUrl;
    }

    /**
     * @return the linkTitle
     */
    public String getLinkTitle()
    {
        return linkTitle;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        String display = "{value:" + this.value + ", linkRequired:" + this.linkRequired;
        if (this.linkUrl != null && this.linkUrl.trim().length() > 0)
        {
            display += ", linkUrl:" + this.linkUrl;
        }
        if (this.linkTitle != null && this.linkTitle.trim().length() > 0)
        {
            display += ", linkTitle:" + this.linkTitle;
        }
        display += "}";

        return display;
    }
}
