/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: ValidateResultVO.java
 */

package edu.ucdavis.mw.myinfovault.valuebeans;

import java.util.List;

/**
 * FIXME: Missing Javadoc
 *
 * @author pradeeph
 * @since MIV 4.4
 */
public class ValidateResultVO extends ResultVO
{
    String[][] validateParameters = null;

    /**
     * TODO: add Javadoc
     * @param errors
     * @param errorfields
     */
    public ValidateResultVO(List<String> errors, List<String> errorfields)
    {
        super(false, errors, errorfields);
    }

    /**
     * TODO: add Javadoc
     * @param validateParameters
     */
    public ValidateResultVO(String[][] validateParameters)
    {
        super(true);
        this.validateParameters = validateParameters;
    }


    /**
     * @return the validateParameters
     */
    public String[][] getValidateParameters()
    {
        return validateParameters;
    }

}
