/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: DossierEvent.java
 */


package edu.ucdavis.mw.myinfovault.events;

/**
 * @author Stephen Paulsen
 *
 */
public abstract class DossierEvent extends MivEvent
{
    private final long dossierId;

    public DossierEvent(final long dossierId, final String actorId)
    {
        super(actorId);
        this.dossierId = dossierId;
    }

    public long getDossierId()
    {
        return this.dossierId;
    }
}
