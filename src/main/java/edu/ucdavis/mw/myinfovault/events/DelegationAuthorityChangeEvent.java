package edu.ucdavis.mw.myinfovault.events;

import edu.ucdavis.mw.myinfovault.domain.action.DossierActionType;
import edu.ucdavis.mw.myinfovault.domain.action.DossierDelegationAuthority;
import edu.ucdavis.mw.myinfovault.domain.dossier.Dossier;
import edu.ucdavis.mw.myinfovault.service.MivServiceLocator;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.mw.myinfovault.service.person.UserService;
import edu.ucdavis.myinfovault.MIVUserInfo;

public class DelegationAuthorityChangeEvent extends DossierEvent
{
    private MivPerson candidate;
    private DossierDelegationAuthority fromDelegationAuthority;
    private DossierDelegationAuthority toDelegationAuthority;
    private DossierActionType fromActionType;
    private DossierActionType toActionType;
    private MIVUserInfo targetMivUserInfo;
    private MIVUserInfo loggedInMivUserInfo;

    public DelegationAuthorityChangeEvent(Dossier dossierBefore,
                                          Dossier dossierAfter,
                                          MivPerson targetPerson,
                                          MivPerson loggedInPerson)
    {
        super(dossierBefore.getDossierId(), Integer.toString(targetPerson.getUserId()));

        UserService userService = MivServiceLocator.getUserService();

        this.targetMivUserInfo = userService.getMivUserByPersonUuid(targetPerson.getPersonId());
        this.loggedInMivUserInfo = userService.getMivUserByPersonUuid(loggedInPerson.getPersonId());
        this.loggedInMivUserInfo = userService.getMivUserByPersonUuid(loggedInPerson.getPersonId());

        this.candidate = dossierBefore.getCandidate();
        this.fromDelegationAuthority = dossierBefore.getAction().getDelegationAuthority();
        this.toDelegationAuthority = dossierAfter.getAction().getDelegationAuthority();
        this.fromActionType = dossierBefore.getAction().getActionType();
        this.toActionType = dossierAfter.getAction().getActionType();
    }

    public DossierDelegationAuthority getFromDelegationAuthority()
    {
        return fromDelegationAuthority;
    }

    public DossierDelegationAuthority getToDelegationAuthority()
    {
        return toDelegationAuthority;
    }

    public DossierActionType getFromActionType()
    {
        return fromActionType;
    }

    public DossierActionType getToActionType()
    {
        return toActionType;
    }

    public MIVUserInfo getTargetMivUserInfo()
    {
        return targetMivUserInfo;
    }

    public MIVUserInfo getLoggedInMivUserInfo()
    {
        return loggedInMivUserInfo;
    }

    @Override
    public String toString()
    {
        StringBuilder sb = new StringBuilder();

        sb.append("PersonId: ").append(this.getActorId());
        sb.append(" changed delegation authority for ")
          .append(this.candidate.getDisplayName())
          .append(" dossier ")
          .append(this.getDossierId())
          .append(" from ")
          .append(this.getFromDelegationAuthority())
          .append("-")
          .append(this.getFromActionType())
          .append(" to ")
          .append(this.getToDelegationAuthority())
          .append("-")
          .append(this.getToActionType())
          ;
        return sb.toString();
    }
}
