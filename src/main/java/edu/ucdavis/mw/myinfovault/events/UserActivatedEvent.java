/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: UserActivatedEvent.java
 */

package edu.ucdavis.mw.myinfovault.events;

/**
 * TODO: Add Javadoc
 * @author stephenp
 *
 */
public class UserActivatedEvent extends UserActivationEvent
{
    /**
     * @param actorId
     * @param affectedUserId
     */
    public UserActivatedEvent(String actorId, String affectedUserId)
    {
        super(actorId, affectedUserId, true);
    }
}
