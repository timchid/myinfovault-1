/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: UUID.java
 */


package edu.ucdavis.mw.myinfovault.events2;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.Arrays;

import edu.ucdavis.myinfovault.exception.MivSevereApplicationError;


/**
 * Wrap com.eaio.uuid.UUID to provide a getBytes() method.
 * But see also http://commons.apache.org/sandbox/commons-id/apidocs/org/apache/commons/id/uuid/UUID.html
 * which has a getRawBytes() method.
 *
 * @author Stephen Paulsen
 * @since MIV 4.7.2
 */
public class UUID extends com.eaio.uuid.UUID
{
    public UUID()
    {
        super();
    }

    public UUID(long time, long clockSeqAndNode)
    {
        super(time, clockSeqAndNode);
    }

    public UUID(UUID u)
    {
        super(u.time, u.clockSeqAndNode);
    }

    public UUID(CharSequence s)
    {
        super(s);
    }


    public byte[] getBytes()
    {
        byte[] bytes = null;
        byte[] result = null;

        try
        {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            ObjectOutputStream out = new ObjectOutputStream(bos);
            out.reset();
            bos.reset();

            this.writeExternal(out);
            out.flush();

            bytes = bos.toByteArray();
            // Using writeExternal adds stream header bytes. Drop them to get the actual UUID bytes.
            result = Arrays.copyOfRange(bytes, 2, bytes.length);

            out.close();
            bos.close();
        }
        catch (IOException e)
        {
            // Because the output streams don't actually perform I/O this should not be possible.
            throw new MivSevereApplicationError("UUID output stream failure!", e);
        }

        return result;//bytes;
    }
}
