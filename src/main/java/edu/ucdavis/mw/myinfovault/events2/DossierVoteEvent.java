/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: DossierVoteEvent.java
 */

package edu.ucdavis.mw.myinfovault.events2;

import edu.ucdavis.mw.myinfovault.domain.action.DossierLocation;
import edu.ucdavis.mw.myinfovault.domain.dossier.Dossier;
import edu.ucdavis.mw.myinfovault.dossier.attributes.DossierAttributeStatus;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;

/**
 * TODO: add javadoc
 *
 * @author Rick Hendricks
 * @since MIV 4.7.2
 */
public class DossierVoteEvent extends DossierEventStub implements DossierEvent, VoteEvent
{
    private static final long serialVersionUID = 201308071500L;

    private final DossierLocation voteLocation;
    private final DossierAttributeStatus voteStatus;

    /**
     * TODO: add javadoc
     *
     * @param realPerson
     * @param dossier
     * @param voteLocation
     * @param voteStatus
     */
    public DossierVoteEvent(final MivPerson realPerson,
                            final Dossier dossier,
                            final DossierLocation voteLocation,
                            final DossierAttributeStatus voteStatus)
    {
        super(realPerson, dossier);

        this.voteLocation = voteLocation;
        this.voteStatus = voteStatus;
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.events2.VoteEvent#getVoteStatus()
     */
    @Override
    public DossierAttributeStatus getVoteStatus()
    {
        return this.voteStatus;
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.events2.VoteEvent#getVoteLocation()
     */
    @Override
    public DossierLocation getVoteLocation()
    {
        return this.voteLocation;
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.events2.MivEventStub#getEventTitle()
     */
    @Override
    public String getEventTitle()
    {
        return "Dossier Vote Event";
    }
}
