/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: ErrorEvent.java
 */


package edu.ucdavis.mw.myinfovault.events2;

/**
 * TODO: Add Javadoc
 *
 * @author Rick Hendricks
 * @since MIV 4.7.2
 */
@LoggableEvent(EventCategory.ERROR)
public interface ErrorEvent extends DossierEvent
{
    Exception getException();
}
