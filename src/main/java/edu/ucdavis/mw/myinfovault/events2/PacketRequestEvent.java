/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: PacketRequestEvent.java
 */


package edu.ucdavis.mw.myinfovault.events2;

import edu.ucdavis.mw.myinfovault.domain.dossier.Dossier;
import edu.ucdavis.mw.myinfovault.domain.packet.PacketRequestActionType;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;


/**
 * PacketRequestEvent event is posted when a packet is requested for a dossier.
 *
 * @author Rick Hendricks
 * @since MIV 5.0
 */
public class PacketRequestEvent extends PacketEventStub
{

    private static final long serialVersionUID = 201503301320L;


    /**
     * @param realPerson
     * @param dossier
     */
    public PacketRequestEvent(MivPerson realPerson, Dossier dossier)
    {
        super(realPerson, dossier, PacketRequestActionType.REQUESTED);
    }

    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.events2.MivEventStub#getEventTitle()
     */
    @Override
    public String getEventTitle()
    {
        return "Packet Request Event";
    }

}
