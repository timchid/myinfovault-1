/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: DossierReturnEvent.java
 */


package edu.ucdavis.mw.myinfovault.events2;

import edu.ucdavis.mw.myinfovault.domain.action.DossierLocation;
import edu.ucdavis.mw.myinfovault.domain.dossier.Dossier;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;


/**
 * Exactly the same as a {@link DossierRouteEvent} but implies routing to a previous node.
 *
 * @author Stephen Paulsen
 * @since MIV 4.7.2
 */
public class DossierReturnEvent extends DossierRouteEvent implements DossierEvent, RouteEvent
{
    private static final long serialVersionUID = 201307311025L;

    /**
     * TODO: add Javadoc
     *
     * @param realPerson
     * @param dossier
     * @param from
     * @param to
     */
    public DossierReturnEvent(final MivPerson realPerson, final Dossier dossier, final DossierLocation from, final DossierLocation to)
    {
        super(realPerson, dossier, from, to);
    }

    /**
     * TODO: add Javadoc
     *
     * @param parentEvent
     * @param realPerson
     * @param dossier
     * @param from
     * @param to
     */
    public DossierReturnEvent(final MivEvent parentEvent, final MivPerson realPerson, final Dossier dossier, final DossierLocation from, final DossierLocation to)
    {
        super(parentEvent, realPerson, dossier, from, to);
    }


    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.events2.DossierRouteEvent#getEventTitle()
     */
    @Override
    public String getEventTitle()
    {
        return "Dossier Return Event";
    }
}
