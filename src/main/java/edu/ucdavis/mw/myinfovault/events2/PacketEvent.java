/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: PacketRequest.java
 */


package edu.ucdavis.mw.myinfovault.events2;

import edu.ucdavis.mw.myinfovault.domain.packet.PacketRequestActionType;

/**
 * PacketEvent interface.
 * Right now there are only REQUESTED and SUMBITTED events, but more may be added as needed.
 *
 * @author Rick Hendricks
 * @since MIV 5.0
 */
@LoggableEvent(EventCategory.PACKET)
public interface PacketEvent extends DossierEvent
{

    /**
     * @return the action taken on the packet - request, submit, etc.
     */
    public PacketRequestActionType getAction();

}
