/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: DisclosureRequestDeleteEvent.java
 */

package edu.ucdavis.mw.myinfovault.events2;

import edu.ucdavis.mw.myinfovault.domain.disclosurecertificate.DCBo;
import edu.ucdavis.mw.myinfovault.domain.signature.MivElectronicSignature;

/**
 * TODO: add Javadoc
 *
 * @author Jacob Saporito
 * @since MIV 4.8.4
 */
public class DisclosureRequestDeleteEvent extends DisclosureSignatureEvent
{
    private static final long serialVersionUID = 201408071123L;

    /**
     * TODO: add Javadoc
     *
     * @param event real event initiator
     * @param signature
     */
    public DisclosureRequestDeleteEvent(MivEvent event, MivElectronicSignature signature)
    {
        super(event, event.getRealPerson(), signature);

        // must set changes/additions as comments
        this.setComments(((DCBo)signature.getDocument()).getChangesAdditions());
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.events2.MivEventStub#getEventTitle()
     */
    @Override
    public String getEventTitle()
    {
        return "Disclosure Signature Request Deletion";
    }
}
