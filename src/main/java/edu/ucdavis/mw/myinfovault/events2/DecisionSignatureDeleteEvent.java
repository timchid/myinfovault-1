/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: DecisionSignatureDeleteEvent.java
 */


package edu.ucdavis.mw.myinfovault.events2;

import edu.ucdavis.mw.myinfovault.domain.signature.MivElectronicSignature;

/**
 * A signature signed on a decision was deleted.
 * Inherits most of its methods from the parent {@link DecisionSignatureEvent}.
 *
 * @author Stephen Paulsen
 * @since MIV 4.7.2
 */
public class DecisionSignatureDeleteEvent extends DecisionSignatureEvent implements DossierEvent, DecisionEvent, SignatureEvent
{
    private static final long serialVersionUID = 201308191710L;

    /**
     * @param parent
     * @param userId
     * @param signature
     */
    public DecisionSignatureDeleteEvent(MivEvent parent, MivElectronicSignature signature)
    {
        super(parent, parent.getRealPerson(), signature);
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.events2.MivEventStub#getEventTitle()
     */
    @Override
    public String getEventTitle()
    {
        return "Decision Signature Delete";
    }

}
