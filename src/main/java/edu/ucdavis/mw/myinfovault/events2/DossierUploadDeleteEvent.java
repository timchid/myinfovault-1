/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: DossierUploadDeleteEvent.java
 */

package edu.ucdavis.mw.myinfovault.events2;

import edu.ucdavis.mw.myinfovault.domain.dossier.Dossier;
import edu.ucdavis.mw.myinfovault.service.dossier.UploadDocumentDto;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;

/**
 * An event to indicate a document upload was deleted.
 *
 * @author Craig Gilmore
 * @since MIV 4.7.2
 */
public class DossierUploadDeleteEvent extends DossierUploadEvent implements DossierEvent, UploadEvent
{
    private static final long serialVersionUID = 201308221203L;

    /**
     * Create an upload delete event.
     *
     * @param realPerson
     * @param dossier
     * @param uploadDocument
     */
    public DossierUploadDeleteEvent(MivPerson realPerson,
                                    Dossier dossier,
                                    UploadDocumentDto uploadDocument)
    {
        this(null,
                realPerson,
             dossier,
             uploadDocument);
    }

    /**
     * Create an upload delete event as a result of the given parent event.
     *
     * @param parent
     * @param realPerson
     * @param dossier
     * @param uploadDocument
     */
    public DossierUploadDeleteEvent(MivEvent parent,
                                    MivPerson realPerson,
                                    Dossier dossier,
                                    UploadDocumentDto uploadDocument)
    {
        super(parent,
              realPerson,
              dossier,
              uploadDocument);
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.events2.DossierUploadEvent#getEventTitle()
     */
    @Override
    public String getEventTitle()
    {
        return "Dossier Upload Delete";
    }
}
