/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: MivEvent.java
 */

package edu.ucdavis.mw.myinfovault.events2;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import edu.ucdavis.mw.myinfovault.service.person.MivPerson;

/**
 * Represents an event occurring during runtime in MyInfoVault.
 *
 * @author Stephen Paulsen
 * @since MIV 4.7.2
 */
public interface MivEvent extends Serializable
{
    /**
     * @return unique event ID
     */
    public UUID getEventId();

    /**
     * @return event title description
     */
    public String getEventTitle();

    /**
     * @return time at which this event occurred
     */
    public Date getEventTime();

    /**
     * @return event that caused this event
     */
    public MivEvent getParentEvent();

    /**
     * @return categories belonging to this event
     */
    public Set<EventCategory> getCategories();

    /**
     * Gets the identifier for the person who caused this event to occur. Even though an MIV
     * user ID is a number, a String is returned that could be a non-user, such as "SYSTEM".
     *
     * @return identifier of the real event initiator
     */
    public String getRealUserId();

    /**
     * Gets the real, logged-in MIV person who initiated the event
     * or <code>null</code> for non-user initiated event.
     *
     * @return the real event initiator
     */
    public MivPerson getRealPerson();

    /**
     * @param shadowPerson the proxied event initiator
     * @return this event (for chaining)
     */
    public MivEvent setShadowPerson(MivPerson shadowPerson);

    /**
     * Gets the identifier of a person who is being proxied by the logged-in "real" user.
     *
     * @return identifier of the proxied event initiator
     */
    public String getShadowUserId();

    /**
     * Gets the proxied, switched-to MIV person who initiated the event
     * or <code>null</code> for non-user or non-proxy initiated event.
     *
     * @return the proxied event initiator
     */
    public MivPerson getShadowPerson();

    /**
     * @param comments
     * @return this event (for chaining)
     */
    public MivEvent setComments(String comments);

    /**
     * @return system commentary on this event (used for logging)
     */
    public String getComments();
}
