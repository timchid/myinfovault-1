/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: DossierUploadEvent.java
 */


package edu.ucdavis.mw.myinfovault.events2;

import edu.ucdavis.mw.myinfovault.domain.dossier.Dossier;
import edu.ucdavis.mw.myinfovault.service.dossier.UploadDocumentDto;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;


/**
 * An event to indicate a Document was uploaded.
 *
 * @author Rick Hendricks
 * @since MIV 4.7.2
 */
public class DossierUploadEvent extends DossierEventStub implements DossierEvent, UploadEvent
{
    private static final long serialVersionUID = 201308121532L;

    private UploadDocumentDto uploadDocument;

    /**
     * TODO: add Javadoc
     *
     * @param realPerson
     * @param dossier
     * @param uploadDocument
     */
    public DossierUploadEvent(final MivPerson realPerson, final Dossier dossier, UploadDocumentDto uploadDocument)
    {
        this(null, realPerson, dossier, uploadDocument);
    }

    /**
     * TODO: add Javadoc
     *
     * @param parent
     * @param realPerson
     * @param dossier
     * @param uploadDocument
     */
    public DossierUploadEvent(final MivEvent parent, final MivPerson realPerson, final Dossier dossier, UploadDocumentDto uploadDocument)
    {
        super(parent, realPerson, dossier);

        this.uploadDocument = uploadDocument;
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.events2.MivEventStub#getEventTitle()
     */
    @Override
    public String getEventTitle()
    {
        return "Dossier Upload Event";
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.events2.UploadEvent#getUploadDocument()
     */
    @Override
    public UploadDocumentDto getUploadDocument()
    {
        return uploadDocument;
    }
}
