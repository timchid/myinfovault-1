/**
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: DossierDocumentTest.java
 */

package edu.ucdavis.mw.myinfovault.test;


public class DossierDocumentTest
{
//
////    private IdentityService identityService = null;
//    DossierService dossierService = null;
//
//    public DossierDocumentTest ()
//    {
////        this.identityService = KIMServiceLocator.getIdentityService();
//        this.dossierService = MivServiceLocator.getDossierService();
//    }
//
//    public Dossier initiateDossier(String loginId, String dossierDescription, DossierDelegationAuthority delegationAuthority,
//            DossierActionType dossierAction, String workflowAnnotation) throws WorkflowException
//    {
//        return dossierService.initiateDossierRouting(loginId, dossierDescription, delegationAuthority, dossierAction, null,
//                workflowAnnotation);
//    }
//
//    public Dossier approveDossier(String loginId, Long dossierId, String annotation) throws WorkflowException
//    {
//        return dossierService.approveDossier(loginId, dossierId, annotation);
//    }
//
//    public Dossier cancelDossier(String loginId, Long dossierId, String annotation) throws WorkflowException
//    {
//        return dossierService.cancelDossier(loginId, dossierId, annotation);
//    }
//
//    public Dossier routeDossier(String loginId, Long dossierId, String annotation) throws WorkflowException
//    {
//        return dossierService.routeDossier(loginId, dossierId, annotation);
//    }
//
//    public Dossier returnDossierToPreviousNode(String loginId, Long dossierId, String annotation) throws WorkflowException
//    {
//        dossierService.returnDossierToPreviousNode(loginId, dossierId, annotation);
//        return dossierService.getDossierAndLoadAllData(dossierId);
//    }
//
//    public Dossier getDossier(Long dossierId) throws WorkflowException
//    {
//        return dossierService.getDossierAndLoadAllData(dossierId);
//    }
//
//    public List <Long> findActionListDossiersByPrincipalId(String loginId) throws WorkflowException
//    {
//        return dossierService.findActionListDossiersByPrincipalId(loginId);
//    }
//
//    public List <Dossier> getDossierByPrincipalId(String loginId) throws WorkflowException, IllegalStateException
//    {
//        return dossierService.getDossierByPrincipalId(loginId);
//    }
//
//    public List <Dossier> getDossiersByLocation(String loginId, DossierLocation location) throws WorkflowException
//    {
////        return dossierService.getDossiersByWorkflowLocation(convertPrincipalNameToPrincipalId(loginName), location);
//        return dossierService.getDossiersByWorkflowLocation(loginId, location);
//    }
//
//    public List<Dossier> findDossiersBySchoolDepartmentAndLocation(String loginId, int schoolId, int departmentId, DossierLocation location)
//            throws WorkflowException
//    {
//        return dossierService.getDossiersBySchoolDepartmentAndLocation(loginId, schoolId, departmentId, location);
//    }
//
//    public List <Dossier> getDossiersScopedByName(String loginId) throws WorkflowException
//    {
////        return dossierService.getDossiers(convertPrincipalNameToPrincipalId(loginName));
//        return dossierService.getDossiers(loginId);
//    }
//
//    public Dossier routeDossierToLocation(String loginId, Long dossierId, DossierLocation location, String annotation) throws WorkflowException
//    {
//        dossierService.routeDossierToNode(loginId, dossierId, location, annotation);
//        return dossierService.getDossierAndLoadAllData(dossierId);
//    }

}


