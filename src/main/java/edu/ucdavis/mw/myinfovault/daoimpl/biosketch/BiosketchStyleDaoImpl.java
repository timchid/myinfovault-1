package edu.ucdavis.mw.myinfovault.daoimpl.biosketch;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import edu.ucdavis.mw.myinfovault.dao.biosketch.BiosketchStyleDao;
import edu.ucdavis.mw.myinfovault.domain.biosketch.BiosketchStyle;
import edu.ucdavis.myinfovault.designer.Alignment;

/**
 * @author dreddy
 * @since MIV 2.1
 */
public class BiosketchStyleDaoImpl implements BiosketchStyleDao
{
    private static final Log logger = LogFactory.getLog(BiosketchStyleDaoImpl.class);
    private JdbcTemplate jdbcTemplate = new JdbcTemplate();

    /**
     * @param template
     */
    public void setJdbcTemplate(JdbcTemplate template)
    {
        this.jdbcTemplate = template;
    }

    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.biosketch.BiosketchStyleDao#findDefaultBiosketchStyle(int)
     * returns the default biosketchstyle for a particular biosketchType
     */
    @Override
    public BiosketchStyle findDefaultBiosketchStyle(int biosketchType) throws SQLException
    {
        logger.debug("Entering findDefaultBiosketchStyle() ... ");

        final String sql = "SELECT * FROM BiosketchStyle WHERE ID = ?";
        Object[] params = { new Integer(biosketchType) };
        // Retrieve query results from database
        BiosketchStyle biosketchStyle = jdbcTemplate.queryForObject(sql, params, biosketchstylemapper);

        logger.debug("Exiting findDefaultBiosketchStyle() ... ");
        return biosketchStyle;
    }

    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.biosketch.BiosketchStyleDao#findBiosketchStyleById(int)
     * returns the biosketchstyle associated with a particualr styleid
     */
    @Override
    public BiosketchStyle findBiosketchStyleById(int styleID) throws SQLException
    {
        logger.debug("Entering findBiosketchStyleById() ... ");

        final String sql = "SELECT * FROM BiosketchStyle WHERE ID = ?";
        Object[] params = { new Integer(styleID) };
        // Retrieve query results from database
        BiosketchStyle biosketchStyle = jdbcTemplate.queryForObject(sql, params, biosketchstylemapper);

        logger.debug("Exiting findBiosketchStyleById() ... ");
        return biosketchStyle;
    }

    /**
     * maps the biosketchstyle result set to biosketchstyle domain object
     */
    private RowMapper<BiosketchStyle> biosketchstylemapper = new RowMapper<BiosketchStyle>() {
        @Override
        public BiosketchStyle mapRow(ResultSet rs, int rowNum) throws SQLException
        {
            logger.debug("Entering mapRow() ... ");

            BiosketchStyle biosketchStyle = new BiosketchStyle(rs.getInt("ID"), rs.getInt("UserID"));
            biosketchStyle.setStyleName(rs.getString("StyleName"));
            biosketchStyle.setPageWidth(rs.getInt("PageWidth"));
            biosketchStyle.setPageHeight(rs.getInt("PageHeight"));
            biosketchStyle.setMarginTop(rs.getInt("MarginTop"));
            biosketchStyle.setMarginRight(rs.getInt("MarginRight"));
            biosketchStyle.setMarginBottom(rs.getInt("MarginBottom"));
            biosketchStyle.setMarginLeft(rs.getInt("MarginLeft"));
            biosketchStyle.setTitleFontID(rs.getInt("TitleFontID"));
            biosketchStyle.setTitleSize(rs.getInt("TitleSize"));
            String titleAlign = rs.getString("TitleAlign");
            biosketchStyle.setTitleAlign(Alignment.valueOf(titleAlign));
            biosketchStyle.setTitleBold(rs.getBoolean("TitleBold"));
            biosketchStyle.setTitleItalic(rs.getBoolean("TitleItalic"));
            biosketchStyle.setTitleUnderline(rs.getBoolean("TitleUnderline"));
            biosketchStyle.setTitleRules(rs.getBoolean("TitleRules"));
            biosketchStyle.setTitleEditable(rs.getBoolean("TitleEditable"));
            String displayNameAlign = rs.getString("DisplaynameAlign");
            biosketchStyle.setDisplayNameAign(Alignment.valueOf(displayNameAlign));
            biosketchStyle.setHeaderFontID(rs.getInt("HeaderFontID"));
            biosketchStyle.setHeaderSize(rs.getInt("HeaderSize"));
            String headerAlign = rs.getString("HeaderAlign");
            biosketchStyle.setHeaderAlign(Alignment.valueOf(headerAlign));
            biosketchStyle.setHeaderBold(rs.getBoolean("HeaderBold"));
            biosketchStyle.setHeaderItalic(rs.getBoolean("HeaderItalic"));
            biosketchStyle.setHeaderUnderline(rs.getBoolean("HeaderUnderline"));
            biosketchStyle.setHeaderIndent(rs.getInt("HeaderIndent"));
            biosketchStyle.setBodyFontID(rs.getInt("BodyFontID"));
            biosketchStyle.setBodySize(rs.getInt("BodySize"));
            biosketchStyle.setBodyIndent(rs.getInt("BodyIndent"));
            biosketchStyle.setBodyFormatting(rs.getBoolean("BodyFormatting"));
            biosketchStyle.setFooterOn(rs.getBoolean("FooterOn"));
            biosketchStyle.setFooterPageNumbers(rs.getBoolean("FooterPageNumbers"));
            biosketchStyle.setFooterRules(rs.getBoolean("FooterRules"));
            biosketchStyle.setCitationFormatID(rs.getInt("CitationFormatID"));

            logger.debug("Exiting mapRow() ... ");
            return biosketchStyle;
        }
    };

    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.biosketch.BiosketchStyleDao#insertBiosketchStyle(edu.ucdavis.mw.myinfovault.domain.biosketch.BiosketchStyle)
     * inserts the biosketchstyle record and fetches the biosketchstyle id
     */
    @Override
    public int insertBiosketchStyle(BiosketchStyle biosketchStyle) throws SQLException
    {
        logger.debug("Entering insertBiosketchStyle() ... ");

        final String INSERT_BIOSKETCHSTYLE_SQL = "INSERT INTO BiosketchStyle(UserID, StyleName, PageWidth, PageHeight, MarginTop, MarginRight, MarginBottom, MarginLeft,"
                + "TitleFontID, TitleSize, TitleAlign, TitleBold, TitleItalic, TitleUnderline, TitleRules, TitleEditable, DisplaynameAlign, HeaderFontID, HeaderSize,"
                + "HeaderAlign, HeaderBold, HeaderItalic, HeaderUnderline, HeaderIndent, BodyFontID,BodySize, BodyIndent, BodyFormatting, FooterOn, FooterPageNumbers,"
                + "FooterRules, CitationFormatID, InsertTimestamp, InsertUserID)"
                + "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

        // final int ID = (biosketchStyle.getId() == 3) ? 0 : biosketchStyle.getId();
        final int userID = biosketchStyle.getUserID();
        final String styleName = biosketchStyle.getStyleName();
        final int pageWidth = biosketchStyle.getPageWidth();
        final int pageHeight = biosketchStyle.getPageHeight();
        final int marginTop = biosketchStyle.getMarginTop();
        final int marginRight = biosketchStyle.getMarginRight();
        final int marginBottom = biosketchStyle.getMarginBottom();
        final int marginLeft = biosketchStyle.getMarginLeft();
        final int titleFontID = biosketchStyle.getTitleFontID();
        final int titleSize = biosketchStyle.getTitleSize();
        final String titleAlign = biosketchStyle.getTitleAlign().toString();
        final Boolean titleBold = biosketchStyle.isTitleBold();
        final Boolean titleItalic = biosketchStyle.isTitleItalic();
        final Boolean titleUnderline = biosketchStyle.isTitleUnderline();
        final Boolean titleRules = biosketchStyle.isTitleRules();
        final Boolean titleEditable = biosketchStyle.isTitleEditable();
        final String displayNameAlign = biosketchStyle.getDisplayNameAign().toString();
        final int headerFontID = biosketchStyle.getHeaderFontID();
        final int headerSize = biosketchStyle.getHeaderSize();
        final String headerAlign = biosketchStyle.getHeaderAlign().toString();
        final Boolean headerBold = biosketchStyle.isHeaderBold();
        final Boolean headerItalic = biosketchStyle.isHeaderItalic();
        final Boolean headerUnderline = biosketchStyle.isHeaderUnderline();
        final int headerIndent = biosketchStyle.getHeaderIndent();
        final int bodyFontID = biosketchStyle.getBodyFontID();
        final int bodySize = biosketchStyle.getBodySize();
        final int bodyIndent = biosketchStyle.getBodyIndent();
        final Boolean bodyFormatting = biosketchStyle.isBodyFormatting();
        final Boolean footerOn = biosketchStyle.isFooterOn();
        final Boolean footerPageNumbers = biosketchStyle.isFooterPageNumbers();
        final Boolean footerRules = biosketchStyle.isFooterRules();
        final int citationFormatId = biosketchStyle.getCitationFormatID();
        final Date insertTimeStamp = new Date();
        final int insertUserID = biosketchStyle.getUserID();

        KeyHolder keyHolder = new GeneratedKeyHolder();
        // inserts the record and collects the id
        jdbcTemplate.update(new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(Connection connection) throws SQLException
            {
                connection.setAutoCommit(true);
                PreparedStatement ps = connection.prepareStatement(INSERT_BIOSKETCHSTYLE_SQL, new String[] { "id" });
                ps.setInt(1, userID);
                ps.setString(2, styleName);
                ps.setInt(3, pageWidth);
                ps.setInt(4, pageHeight);
                ps.setInt(5, marginTop);
                ps.setInt(6, marginRight);
                ps.setInt(7, marginBottom);
                ps.setInt(8, marginLeft);
                ps.setInt(9, titleFontID);
                ps.setInt(10, titleSize);
                ps.setString(11, titleAlign);
                ps.setBoolean(12, titleBold);
                ps.setBoolean(13, titleItalic);
                ps.setBoolean(14, titleUnderline);
                ps.setBoolean(15, titleRules);
                ps.setBoolean(16, titleEditable);
                ps.setString(17, displayNameAlign);
                ps.setInt(18, headerFontID);
                ps.setInt(19, headerSize);
                ps.setString(20, headerAlign);
                ps.setBoolean(21, headerBold);
                ps.setBoolean(22, headerItalic);
                ps.setBoolean(23, headerUnderline);
                ps.setInt(24, headerIndent);
                ps.setInt(25, bodyFontID);
                ps.setInt(26, bodySize);
                ps.setInt(27, bodyIndent);
                ps.setBoolean(28, bodyFormatting);
                ps.setBoolean(29, footerOn);
                ps.setBoolean(30, footerPageNumbers);
                ps.setBoolean(31, footerRules);
                ps.setInt(32, citationFormatId);
                ps.setTimestamp(33, new Timestamp(insertTimeStamp.getTime()));
                ps.setInt(34, insertUserID);

                return ps;
            }
        }, keyHolder);

        System.out.println("auto generated styleID is " + keyHolder.getKey().intValue());
        logger.debug("Exiting insertBiosketchStyle() ... ");
        return (keyHolder.getKey().intValue());
    }

    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.biosketch.BiosketchStyleDao#updateBiosketchStyle(edu.ucdavis.mw.myinfovault.domain.biosketch.BiosketchStyle)
     * updates the biosketchstyle record
     */
    @Override
    public void updateBiosketchStyle(BiosketchStyle biosketchStyle) throws SQLException
    {
        logger.debug("Entering updateBiosketchStyle() ... ");

        final String UPDATE_BIOSKETCHSTYLE_SQL = "UPDATE BiosketchStyle SET StyleName=?, PageWidth=?, PageHeight=?, MarginTop=?,"
                + "MarginRight=?, MarginBottom=?, MarginLeft=?,TitleFontID=?,TitleSize=?, TitleAlign=?, TitleBold=?, TitleItalic=?,"
                + "TitleUnderline=?, TitleRules=?, TitleEditable=?, DisplaynameAlign=?, HeaderFontID=?, HeaderSize=?, HeaderAlign=?,"
                + "HeaderBold=?, HeaderItalic=?, HeaderUnderline=?, HeaderIndent=?, BodyFontID=?, BodySize=?, BodyIndent=?, BodyFormatting=?,"
                + "FooterOn=?, FooterPageNumbers=?, FooterRules=?, CitationFormatID=?, UpdateTimestamp=?, UpdateUserID=? WHERE ID=?";

        final String styleName = biosketchStyle.getStyleName();
        final int pageWidth = biosketchStyle.getPageWidth();
        final int pageHeight = biosketchStyle.getPageHeight();
        final int marginTop = biosketchStyle.getMarginTop();
        final int marginRight = biosketchStyle.getMarginRight();
        final int marginBottom = biosketchStyle.getMarginBottom();
        final int marginLeft = biosketchStyle.getMarginLeft();
        final int titleFontID = biosketchStyle.getTitleFontID();
        final int titleSize = biosketchStyle.getTitleSize();
        final String titleAlign = biosketchStyle.getTitleAlign().toString();
        final Boolean titleBold = biosketchStyle.isTitleBold();
        final Boolean titleItalic = biosketchStyle.isTitleItalic();
        final Boolean titleUnderline = biosketchStyle.isTitleUnderline();
        final Boolean titleRules = biosketchStyle.isTitleRules();
        final Boolean titleEditable = biosketchStyle.isTitleEditable();
        final String displayNameAlign = biosketchStyle.getDisplayNameAign().toString();
        final int headerFontID = biosketchStyle.getHeaderFontID();
        final int headerSize = biosketchStyle.getHeaderSize();
        final String headerAlign = biosketchStyle.getHeaderAlign().toString();
        final Boolean headerBold = biosketchStyle.isHeaderBold();
        final Boolean headerItalic = biosketchStyle.isHeaderItalic();
        final Boolean headerUnderline = biosketchStyle.isHeaderUnderline();
        final int headerIndent = biosketchStyle.getHeaderIndent();
        final int bodyFontID = biosketchStyle.getBodyFontID();
        final int bodySize = biosketchStyle.getBodySize();
        final int bodyIndent = biosketchStyle.getBodyIndent();
        final Boolean bodyFormatting = biosketchStyle.isBodyFormatting();
        final Boolean footerOn = biosketchStyle.isFooterOn();
        final Boolean footerPageNumbers = biosketchStyle.isFooterPageNumbers();
        final Boolean footerRules = biosketchStyle.isFooterRules();
        final int citationFormatId = biosketchStyle.getCitationFormatID();
        final Date updateTimeStamp = new Date();
        final int updateUserID = biosketchStyle.getUserID();
        final int ID = biosketchStyle.getId();

        // updates the biosketchstyle record in the database
        jdbcTemplate.update(new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(Connection connection) throws SQLException
            {
                connection.setAutoCommit(true);
                PreparedStatement ps = connection.prepareStatement(UPDATE_BIOSKETCHSTYLE_SQL);
                ps.setString(1, styleName);
                ps.setInt(2, pageWidth);
                ps.setInt(3, pageHeight);
                ps.setInt(4, marginTop);
                ps.setInt(5, marginRight);
                ps.setInt(6, marginBottom);
                ps.setInt(7, marginLeft);
                ps.setInt(8, titleFontID);
                ps.setInt(9, titleSize);
                ps.setString(10, titleAlign);
                ps.setBoolean(11, titleBold);
                ps.setBoolean(12, titleItalic);
                ps.setBoolean(13, titleUnderline);
                ps.setBoolean(14, titleRules);
                ps.setBoolean(15, titleEditable);
                ps.setString(16, displayNameAlign);
                ps.setInt(17, headerFontID);
                ps.setInt(18, headerSize);
                ps.setString(19, headerAlign);
                ps.setBoolean(20, headerBold);
                ps.setBoolean(21, headerItalic);
                ps.setBoolean(22, headerUnderline);
                ps.setInt(23, headerIndent);
                ps.setInt(24, bodyFontID);
                ps.setInt(25, bodySize);
                ps.setInt(26, bodyIndent);
                ps.setBoolean(27, bodyFormatting);
                ps.setBoolean(28, footerOn);
                ps.setBoolean(29, footerPageNumbers);
                ps.setBoolean(30, footerRules);
                ps.setInt(31, citationFormatId);
                ps.setTimestamp(32, new Timestamp(updateTimeStamp.getTime()));
                ps.setInt(33, updateUserID);
                ps.setInt(34, ID);

                return ps;
            }
        });

        logger.debug("Exiting updateBiosketchStyle() ... ");
    }

    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.biosketch.BiosketchStyleDao#deleteBiosketchStyle(int)
     * deletes the biosketchstyle record
     */
    @Override
    public void deleteBiosketchStyle(int styleID) throws SQLException
    {
        logger.debug("Entering deleteBiosketchStyle() ... ");

        final String DELETE_BIOSKETCHSTYLE_SQL = "DELETE FROM BiosketchStyle WHERE ID=?";

        final int ID = styleID;

        // deletes the biosketchstyle record from the database
        jdbcTemplate.update(new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(Connection connection) throws SQLException
            {
                connection.setAutoCommit(true);
                PreparedStatement ps = connection.prepareStatement(DELETE_BIOSKETCHSTYLE_SQL);
                ps.setInt(1, ID);
                return ps;
            }
        });

        logger.debug("Exiting deleteBiosketchStyle() ... ");
    }
}
