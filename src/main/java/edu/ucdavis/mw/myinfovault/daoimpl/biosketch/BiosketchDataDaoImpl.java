package edu.ucdavis.mw.myinfovault.daoimpl.biosketch;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCallback;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;

import edu.ucdavis.mw.myinfovault.dao.biosketch.BiosketchDataDao;
import edu.ucdavis.mw.myinfovault.domain.biosketch.BiosketchExclude;
import edu.ucdavis.mw.myinfovault.domain.biosketch.BiosketchSection;
import edu.ucdavis.mw.myinfovault.domain.biosketch.BiosketchSectionLimits;
import edu.ucdavis.mw.myinfovault.domain.biosketch.Section;
import edu.ucdavis.myinfovault.designer.Availability;

/**
 * @author dreddy
 * @since MIV 2.1
 */
public class BiosketchDataDaoImpl implements BiosketchDataDao
{
    private static final Log logger = LogFactory.getLog(BiosketchDataDaoImpl.class);
    private JdbcTemplate jdbcTemplate = new JdbcTemplate();

    /**
     * @param template
     */
    public void setJdbcTemplate(JdbcTemplate template)
    {
        this.jdbcTemplate = template;
    }

    /*  public BiosketchData findBiosketchData(Biosketch biosketch) throws SQLException
     {
     Iterable<BiosketchSection> biosketchSectionList = biosketch.getBiosketchSections();

     // sectionIds contains comma separated list of sections which user has
     // opted to include in packet
     StringBuilder sectionIds = new StringBuilder();
     for (BiosketchSection biosketchSection : biosketchSectionList)
     {
     boolean displaySection = biosketchSection.isDisplay();
     // only get biosketch data for those sections which user has opted
     // to include in packet
     if (displaySection)
     {
     if (!"".equals(sectionIds.toString()))
     {
     sectionIds.append(",");
     }
     sectionIds.append(biosketchSection.getSectionID());
     }
     }

     BiosketchData biosketchData = new BiosketchData(biosketch.getId(), biosketch.getUserID());
     if (!"".equals(sectionIds.toString()))
     {
     //biosketch.setBiosketchData(biosketchData);
     List<DesignDocument> designDocuments = collectDesignData(biosketch, sectionIds.toString());

     //biosketchData.setDisplaySectionList(designDocuments);
     }
     return biosketchData;
     } */

    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.biosketch.BiosketchDataDao#findBiosketchExcludeListByBiosketchIdAndUserId(int, int)
     * returns the list of biosketchExclude associated with a particualr biosketch for a user
     */
    @Override
    public List<BiosketchExclude> findBiosketchExcludeListByBiosketchIdAndUserId(int biosketchId, int userId) throws SQLException
    {
        logger.debug("Entering findBiosketchExcludeListByBiosketchIdAndUserId() ... ");

        final String sql = "SELECT * FROM BiosketchExclude WHERE BiosketchID = ? AND UserID=?";
        Object[] params = { new Integer(biosketchId), new Integer(userId) };
        // Retrieve query results from database
        List<BiosketchExclude> biosketchExcludeList = jdbcTemplate.query(sql, params, biosketchexcludemapper);

        logger.debug("Exiting findBiosketchExcludeListByBiosketchIdAndUserId() ... ");
        return biosketchExcludeList;
    }


    /**
     * maps the BiosketchExclude result set to BiosketchExclude domain object
     */
    private RowMapper<BiosketchExclude> biosketchexcludemapper = new RowMapper<BiosketchExclude>() {
        @Override
        public BiosketchExclude mapRow(ResultSet rs, int rowNum) throws SQLException
        {
            logger.debug("Entering mapRow() ... ");

            BiosketchExclude biosketchExclude = new BiosketchExclude(rs.getInt("ID"), rs.getInt("UserID"));
            biosketchExclude.setBiosketchID(rs.getInt("BiosketchID"));
            biosketchExclude.setRecordID(rs.getInt("RecordID"));
            biosketchExclude.setRecType(rs.getString("RecType"));

            logger.debug("Exiting mapRow() ... ");
            return biosketchExclude;
        }
    };


    /**
     * Inserts the list if BiosketchExclude objects to the db for the respective user.
     * @param excludeList BiosketchExclude objects list
     * @param userID UserID
     */
    @Override
    public void insertBiosketchExcludeBatch(final List<BiosketchExclude> excludeList, final int userID) throws SQLException
    {
        logger.debug("Entering insertBiosketchExclude() ... ");
        final Date insertTimeStamp = new Date();
        final Date updateTimeStamp = new Date();
        final String INSERT_SQL = "INSERT INTO BiosketchExclude(BiosketchID,RecordID, RecType, UserID, InsertTimestamp, InsertUserID, UpdateTimestamp, UpdateUserID) VALUES (?,?,?,?,?,?,?,?)";

        jdbcTemplate.execute(INSERT_SQL, new PreparedStatementCallback<int[]>() {
            @Override
            public int[] doInPreparedStatement(PreparedStatement ps) throws SQLException
            {
                Connection conn = ps.getConnection();
                boolean autoCommit = conn.getAutoCommit();
                conn.setAutoCommit(true);
                for (BiosketchExclude exclude : excludeList)
                {
                    ps.setInt(1, exclude.getBiosketchID());
                    ps.setInt(2, exclude.getRecordID());
                    ps.setString(3, exclude.getRecType());
                    ps.setInt(4, exclude.getUserID());
                    ps.setTimestamp(5, new Timestamp(insertTimeStamp.getTime()));
                    ps.setInt(6, userID);
                    ps.setTimestamp(7, new Timestamp(updateTimeStamp.getTime()));
                    ps.setInt(8, userID);
                    ps.addBatch();
                }
                int[] i = ps.executeBatch();
                conn.setAutoCommit(autoCommit);
                logger.info("Number of BiosketchExclude records added: " + i);
                return i;
            }
        });

        logger.debug("Exiting insertBiosketchExclude() ... ");
    }


    /**
     * Deletes the list of BiosketchExclude objects from the database
     * @param excludeList List of BiosketchExclude objects to be deleted
     */
    @Override
    public void deleteBiosketchExcludeBatch(final List<BiosketchExclude> excludeList) throws SQLException
    {

        logger.debug("Entering deleteBiosketchExcludeBatch() ... ");
        final String DELETE_BIOSKETCHEXCLUDE_SQL = "DELETE FROM BiosketchExclude WHERE ID=?";
        jdbcTemplate.execute(DELETE_BIOSKETCHEXCLUDE_SQL, new PreparedStatementCallback<int[]>() {
            @Override
            public int[] doInPreparedStatement(PreparedStatement ps) throws SQLException
            {
                Connection conn = ps.getConnection();
                boolean autoCommit = conn.getAutoCommit();
                conn.setAutoCommit(true);
                for (BiosketchExclude exclude : excludeList)
                {
                    ps.setInt(1, exclude.getId());
                    ps.addBatch();
                }
                int[] i = ps.executeBatch();
                conn.setAutoCommit(autoCommit);
                logger.info("Number of BiosketchExclude records deleted: " + i);
                return i;
            }
        });

        logger.debug("Exiting deleteBiosketchExcludeBatch() ... ");
    }


    /**
     * @see edu.ucdavis.mw.myinfovault.dao.biosketch.BiosketchDataDao#findBiosketchSectionLimitsByBiosketchType(int)
     * returns the list of biosketchsectionlimits for a particular biosketchtype also gives a section with a sectionID of -1
     */
    @Override
    public List<BiosketchSectionLimits> findBiosketchSectionLimitsByBiosketchType(int biosketchType) throws SQLException
    {
        logger.debug("Entering findBiosketchSectionLimitsByBiosketchType() ... ");

        final String sql = "SELECT * FROM BiosketchSectionLimits WHERE BiosketchType = ?";
        Object[] params = { new Integer(biosketchType) };
        // Retrieve query results from database
        List<BiosketchSectionLimits> biosketchSectionLimitsList = jdbcTemplate.query(sql, params, biosketchsectionlimitsmapper);

        logger.debug("Exiting findBiosketchSectionLimitsByBiosketchType() ... ");
        return biosketchSectionLimitsList;
    }


    /**
     * maps the BiosketchSectionLimits result set to BiosketchSectionLimits
     * domain object
     */
    private RowMapper<BiosketchSectionLimits> biosketchsectionlimitsmapper = new RowMapper<BiosketchSectionLimits>() {
        @Override
        public BiosketchSectionLimits mapRow(ResultSet rs, int rowNum) throws SQLException
        {
            logger.debug("Entering mapRow() ... ");

            BiosketchSectionLimits biosketchSectionLimits = new BiosketchSectionLimits(rs.getInt("ID"));
            biosketchSectionLimits.setBioksetchType(rs.getInt("BiosketchType"));
            biosketchSectionLimits.setSectionID(rs.getInt("SectionID"));
            String availability = rs.getString("Availability");
            biosketchSectionLimits.setAvailability(Availability.valueOf(availability));
            biosketchSectionLimits.setSectionName(rs.getString("SectionName"));
            biosketchSectionLimits.setMainSection(rs.getBoolean("IsMainSection"));
            biosketchSectionLimits.setInSelectList(rs.getBoolean("InSelectList"));
            biosketchSectionLimits.setMayHideHeader(rs.getBoolean("MayHideHeader"));
            biosketchSectionLimits.setDisplayInBiosketch(rs.getInt("DisplayInBiosketch"));
            logger.debug("Exiting mapRow() ... ");
            return biosketchSectionLimits;
        }
    };


    /**
     * @see edu.ucdavis.mw.myinfovault.dao.biosketch.BiosketchDataDao#findSection()
     * returns a list of all the sections
     */
    @Override
    public List<Section> findSection() throws SQLException
    {
        logger.debug("Entering findSection() ... ");

        final String sql = "SELECT * FROM Section";
        // Retrieve query results from database
        List<Section> sectionList = jdbcTemplate.query(sql, sectionmapper);

        logger.debug("Exiting findSection() ... ");
        return sectionList;
    }


    /**
     * Maps the Section result set to Section domain object
     */
    private RowMapper<Section> sectionmapper = new RowMapper<Section>() {
        @Override
        public Section mapRow(ResultSet rs, int rowNum) throws SQLException
        {
            logger.debug("Entering mapRow() ... ");

            Section section = new Section(rs.getInt("ID"));
            section.setName(rs.getString("Name"));
            section.setRecordName(rs.getString("RecordName"));
            section.setRecordQuery(rs.getString("SelectFields"));

            logger.debug("Exiting mapRow() ... ");
            return section;
        }
    };


    /**
     * @see edu.ucdavis.mw.myinfovault.dao.biosketch.BiosketchDataDao#findBiosketchSectionsByBiosketchId(int)
     * @return a list of biosketchsections associated with a particular biosketchid
     */
    @Override
    public List<BiosketchSection> findBiosketchSectionsByBiosketchId(int biosketchId) throws SQLException
    {
        logger.debug("Entering findBiosketchSectionsByBiosketchId() ... ");

        final String sql = "SELECT * FROM BiosketchSection WHERE BiosketchID = ? ";
        Object[] params = { new Integer(biosketchId) };
        // Retrieve query results from database
        List<BiosketchSection> biosketchSectionList = jdbcTemplate.query(sql, params, biosketchsectionmapper);

        logger.debug("Exiting findBiosketchSectionsByBiosketchId() ... ");
        return biosketchSectionList;
    }


    /**
     * maps the BiosketchSection result set to BiosketchSection domain object
     */
    private RowMapper<BiosketchSection> biosketchsectionmapper = new RowMapper<BiosketchSection>() {
        @Override
        public BiosketchSection mapRow(ResultSet rs, int rowNum) throws SQLException
        {
            logger.debug("Entering mapRow() ... ");

            BiosketchSection biosketchSection = new BiosketchSection(rs.getInt("ID"));
            biosketchSection.setBiosketchID(rs.getInt("BiosketchID"));
            biosketchSection.setSectionID(rs.getInt("SectionID"));
            biosketchSection.setSectionName(rs.getString("SectionName"));
            biosketchSection.setMainSection(rs.getBoolean("IsMainSection"));
            biosketchSection.setInSelectList(rs.getBoolean("InSelectList"));
            biosketchSection.setMayHideHeader(rs.getBoolean("MayHideHeader"));
            biosketchSection.setDisplayInBiosketch(rs.getInt("DisplayInBiosketch"));
            String availability = rs.getString("Availability");
            biosketchSection.setAvailability(Availability.valueOf(availability));
            biosketchSection.setDisplay(rs.getBoolean("Display"));
            biosketchSection.setDisplayHeader(rs.getBoolean("DisplayHeader"));

            logger.debug("Exiting mapRow() ... ");
            return biosketchSection;
        }
    };


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.biosketch.BiosketchDataDao#insertBiosketchSection(edu.ucdavis.mw.myinfovault.domain.biosketch.BiosketchSection, int)
     * inserts the biosketchsection associated with a biosketch
     */
    @Override
    public void insertBiosketchSection(BiosketchSection biosketchSection, int userID) throws SQLException
    {
        logger.debug("Entering insertBiosketchSection() ... ");

        final String INSERT_BIOSKETCHSECTION_SQL = "INSERT INTO BiosketchSection(BiosketchID, SectionID, SectionName, IsMainSection, InSelectList, MayHideHeader, DisplayInBiosketch, Availability, Display, DisplayHeader,"
                + "InsertTimestamp, InsertUserID) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)";

        final int biosketchID = biosketchSection.getBiosketchID();
        final int sectionID = biosketchSection.getSectionID();
        final String sectionName = biosketchSection.getSectionName().toString();
        final Boolean isMainSection = biosketchSection.isMainSection();
        final Boolean inSelectList = biosketchSection.isInSelectList();
        final Boolean mayHideHeader = biosketchSection.isMayHideHeader();
        final int displayInBiosketch=biosketchSection.getDisplayInBiosketch();
        final String availability = biosketchSection.getAvailability().toString();
        final Boolean display = biosketchSection.isDisplay();
        final Boolean displayHeader = biosketchSection.isDisplayHeader();
        final Date insertTimeStamp = new Date();
        final int insertUserID = userID;

        // inserts the biosketchsection into the database
        jdbcTemplate.update(new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(Connection connection) throws SQLException
            {
                connection.setAutoCommit(true);
                PreparedStatement ps = connection.prepareStatement(INSERT_BIOSKETCHSECTION_SQL);
                ps.setInt(1, biosketchID);
                ps.setInt(2, sectionID);
                ps.setString(3, sectionName);
                ps.setBoolean(4, isMainSection);
                ps.setBoolean(5, inSelectList);
                ps.setBoolean(6, mayHideHeader);
                ps.setInt(7, displayInBiosketch);
                ps.setString(8, availability);
                ps.setBoolean(9, display);
                ps.setBoolean(10, displayHeader);
                ps.setTimestamp(11, new Timestamp(insertTimeStamp.getTime()));
                ps.setInt(12, insertUserID);
                return ps;
            }
        });

        logger.debug("Exiting insertBiosketchSection() ... ");
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.biosketch.BiosketchDataDao#updateBiosketchSection(edu.ucdavis.mw.myinfovault.domain.biosketch.BiosketchSection, int)
     * updates the biosketchsection record
     */
    @Override
    public void updateBiosketchSection(BiosketchSection biosketchSection, int userID) throws SQLException
    {
        logger.debug("Entering updateBiosketchSection() ... ");

        final String UPDATE_BIOSKETCHSECTION_SQL = "UPDATE BiosketchSection SET SectionName=?,IsMainSection=?,InSelectList=?,MayHideHeader=?, DisplayInBiosketch=?, Availability=?, Display=?, DisplayHeader=?,"
                + "UpdateTimestamp=?, UpdateUserID=? WHERE ID=? and BiosketchID=?";

        final String availability = biosketchSection.getAvailability().toString();
        final Boolean display = biosketchSection.isDisplay();
        final Boolean displayHeader = biosketchSection.isDisplayHeader();
        final String sectionName = biosketchSection.getSectionName().toString();
        final Boolean isMainSection = biosketchSection.isMainSection();
        final Boolean inSelectList = biosketchSection.isInSelectList();
        final Boolean mayHideHeader = biosketchSection.isMayHideHeader();
        final int displayInBiosketch=biosketchSection.getDisplayInBiosketch();
        final Date updateTimeStamp = new Date();
        final int updateUserID = userID;
        final int ID = biosketchSection.getId();
        final int biosketchID = biosketchSection.getBiosketchID();

        // updates the biosketchsection record in the database
        jdbcTemplate.update(new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(Connection connection) throws SQLException
            {
                connection.setAutoCommit(true);
                PreparedStatement ps = connection.prepareStatement(UPDATE_BIOSKETCHSECTION_SQL);
                ps.setString(1, sectionName);
                ps.setBoolean(2, isMainSection);
                ps.setBoolean(3, inSelectList);
                ps.setBoolean(4, mayHideHeader);
                ps.setInt(5, displayInBiosketch);
                ps.setString(6, availability);
                ps.setBoolean(7, display);
                ps.setBoolean(8, displayHeader);
                ps.setTimestamp(9, new Timestamp(updateTimeStamp.getTime()));
                ps.setInt(10, updateUserID);
                ps.setInt(11, ID);
                ps.setInt(12, biosketchID);

                return ps;
            }
        });

        logger.debug("Exiting updateBiosketchSection() ... ");
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.biosketch.BiosketchDataDao#deleteBiosketchSection(edu.ucdavis.mw.myinfovault.domain.biosketch.BiosketchSection)
     * deletes all the biosketchsection records associated with a biosketch
     */
    @Override
    public void deleteBiosketchSection(int biosketchId) throws SQLException
    {
        logger.debug("Entering deleteBiosketchSection() ... ");

        final String DELETE_BIOSKETCHSECTION_SQL = "DELETE FROM BiosketchSection WHERE BiosketchID=?";

        final int biosketchID = biosketchId;

        // deletes the biosketchsection records from the database
        jdbcTemplate.update(new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(Connection connection) throws SQLException
            {
                connection.setAutoCommit(true);
                PreparedStatement ps = connection.prepareStatement(DELETE_BIOSKETCHSECTION_SQL);
                ps.setInt(1, biosketchID);
                return ps;
            }
        });

        logger.debug("Exiting deleteBiosketchSection() ... ");
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.biosketch.BiosketchDataDao#deleteBiosketchExclude(int)
     * deletes all the biosketchexclude records associated with a biosketch
     */
    @Override
    public void deleteBiosketchExclude(int biosketchId) throws SQLException
    {
        logger.debug("Entering deleteBiosketchExclude() ... ");

        final String DELETE_BIOSKETCHEXCLUDE_SQL = "DELETE FROM BiosketchExclude WHERE BiosketchID=?";

        final int biosketchID = biosketchId;

        // deletes the biosketchexclude records from the database
        jdbcTemplate.update(new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(Connection connection) throws SQLException
            {
                connection.setAutoCommit(true);
                PreparedStatement ps = connection.prepareStatement(DELETE_BIOSKETCHEXCLUDE_SQL);
                ps.setInt(1, biosketchID);
                return ps;
            }
        });

        logger.debug("Exiting deleteBiosketchExclude() ... ");
    }
}
