package edu.ucdavis.mw.myinfovault.daoimpl.biosketch;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import edu.ucdavis.mw.myinfovault.dao.biosketch.PersonalDao;
import edu.ucdavis.mw.myinfovault.domain.biosketch.Personal;


/**<p>
 * FIXME: This, and maybe others of the DaoImpl classes, need work!
 *        The various <code>find[X]by[Y]</code> methods below each do a DB query
 *        and return a SINGLE FIELD. Isn't that the point of using domain objects
 *        with DAOs? To avoid these repeated queries to the DB?</p>
 *     <p>The error messages in those routines are also atrocious.
 *        Besides being ungrammatical and uppercased, they all say that "TOO MANY"
 *        items were found, when the common case is actually that <strong>no</strong>
 *        records were returned.</p>
 *     <p>It's also valid, for example, to allow more than one address of a given type.
 *        A personal can legitimately have more than one office address, and the PersonalDao
 *        interface and this implementation of it should allow for that.</p>
 *
 * @author dreddy
 * @since MIV 2.1
 */
public class PersonalDaoImpl implements PersonalDao
{
    private static final Log logger = LogFactory.getLog(PersonalDaoImpl.class);
    private JdbcTemplate jdbcTemplate = new JdbcTemplate();

    /**
     * @param template
     */
    public void setJdbcTemplate(JdbcTemplate template)
    {
        this.jdbcTemplate = template;
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.myinfovault.biosketch.dao.PersonalDao#findById(int)
     */
    @Override
    public Personal findById(int userId)
    {
        logger.debug("Entering findById() ... ");

        String sql = "SELECT * FROM UserPersonal WHERE UserID = ?";
        Object[] params = { userId };
        Personal personal = null;
        // Retrieve query results from database
        try
        {
            personal = jdbcTemplate.queryForObject(sql, params, personalmapper);
        }
        catch (IncorrectResultSizeDataAccessException ex)
        {
            //no record found
            logger.error("PERSON NOT FOUND ID: " + userId);
        }

        logger.debug("Exiting findById() ... ");
        if (personal != null)
        {
            personal.setPermanentAddress(findAddressByTypeId(userId, 1));
            personal.setCurrentAddress(findAddressByTypeId(userId, 2));
            personal.setOfficeAddress(findAddressByTypeId(userId, 3));
            personal.setPermanentPhone(findPhoneByTypeId(userId, 1));
            personal.setHomePhone(findPhoneByTypeId(userId, 3));
            personal.setOfficePhone(findPhoneByTypeId(userId, 5));
            personal.setCellPhone(findPhoneByTypeId(userId, 7));
            personal.setPermanentFax(findPhoneByTypeId(userId, 2));
            personal.setOfficeFax(findPhoneByTypeId(userId, 6));
            personal.setCurrentFax(findPhoneByTypeId(userId, 4));
            personal.setEmail(findEmailByUserId(userId));
            personal.setWebsite(findUrlByUserId(userId));
        }

        return personal;
    }


    /**
     *
     */
    private RowMapper<Personal> personalmapper = new RowMapper<Personal>() {
        @Override
        public Personal mapRow(ResultSet rs, int rowNum) throws SQLException
        {
            logger.debug("Entering mapRow() ... ");
            Personal personal = new Personal();

            personal.setUserId(rs.getString("UserID"));
            personal.setDisplayName(rs.getString("DisplayName"));
            personal.setBirthDate(rs.getString("BirthDate"));
            personal.setCitizen(rs.getBoolean("Citizen"));
            personal.setVisaType(rs.getString("VisaType"));
            personal.setDateEntry(rs.getString("DateEntered"));

            logger.debug("Exiting mapRow() ... ");

            return personal;
        }
    };


    @Override
    public String findEmailByUserId(int userId)
    {
        String sql = "SELECT Value FROM UserEmail WHERE UserID = ?";
        Object[] params = { new Integer(userId) };
        String email = null;
        try {
            email = jdbcTemplate.queryForObject(sql, params, String.class);
        }
        catch (IncorrectResultSizeDataAccessException ex) {
            //no record found
//            logger.error("TOO MANY PERSON EMAIL FOUND ID: "+userId);

            // no record found is ok, but too many is currently a problem
            if (ex.getActualSize() > 1) {
                logger.warn("Too many email records found for UserID=" + userId + "\n  " + ex.getLocalizedMessage());
            }
        }

        return email;
    }


    @Override
    public String findUrlByUserId(int userId)
    {
        String sql = "SELECT Value FROM UserLink WHERE UserID = ?";
        Object[] params = {  new Integer(userId) };
        String url = null;
        try {
            url = jdbcTemplate.queryForObject(sql, params, String.class);
        }
        catch (IncorrectResultSizeDataAccessException ex) {
            //no record found
//            logger.error("TOO MANY PERSON URL FOUND ID: "+userId);

            // no record found is ok, but too many is currently a problem
            if (ex.getActualSize() > 1) {
                logger.warn("Too many URL records found for UserID=" + userId + "\n  " + ex.getLocalizedMessage());
            }
        }
        return url;
    }


    @Override
    public String findAddressByTypeId(int userId, int typeId)
    {
        //AddressType.Description as addresstype,AddressType.ID as addressTypeID,UserAddress.ID,Street1,Street2,City,State,ZipCode
        //UserID = ? and UserAddress.TypeID = AddressType.ID
        String address = null;
        String sql = "SELECT * FROM UserAddress WHERE UserID = ? AND TypeID = ?";
        Object[] params = { new Integer(userId), new Integer(typeId) };
        // Retrieve query results from database
        try {
            address = jdbcTemplate.queryForObject(sql, params, addressMapper);
        }
        catch (IncorrectResultSizeDataAccessException ex) {
            //no record found
//            logger.error("TOO MANY PERSON Address FOUND ID: "+userId);

            // no record found is ok, but too many is currently a problem
            if (ex.getActualSize() > 1) {
                logger.warn("Too many address records found with type=" + typeId + " for UserID=" + userId + "\n  " + ex.getLocalizedMessage());
            }
        }

        return address;
    }


    private RowMapper<String> addressMapper = new RowMapper<String>() {
        @Override
        public String mapRow(ResultSet rs, int rowNum) throws SQLException
        {
            StringBuffer sb = new StringBuffer();
            if (rs.getString("Street1").length() > 0)
            {
                sb.append(rs.getString("Street1"));
            }
            if (rs.getString("Street2").length() > 0)
            {
                sb.append(',').append(' ').append(rs.getString("Street2"));
            }
            //sb.append(rs.getString("Street1")).append(',').append(' ');
            if (rs.getString("City").length() > 0)
            {
                sb.append(',').append(' ').append(rs.getString("City"));
            }
            if (rs.getString("State").length() > 0)
            {
                sb.append(',').append(' ').append(rs.getString("State"));
            }
            if (rs.getString("ZipCode").length() > 0)
            {
                sb.append(',').append(' ').append(rs.getString("ZipCode"));
            }

            return sb.toString();
        }
    };


    @Override
    public String findPhoneByTypeId(int userId, int typeId)
    {
        String sql = "SELECT Number FROM UserPhone" +
                     " WHERE UserPhone.UserID = ? AND UserPhone.TypeID = ?";
        String phone = null;
        Object[] params = { new Integer(userId), new Integer(typeId) };
        // Retrieve query results from database
        //Map<String,String> phone = (Map<String,String>)jdbcTemplate.queryForObject(sql, params, phoneMapper);
        try {
            phone = jdbcTemplate.queryForObject(sql, params, java.lang.String.class);
        }
        catch (IncorrectResultSizeDataAccessException ex) {
//            logger.error("TOO MANY PERSON phone FOUND ID: " + userId);

            // no record found is ok, but too many is currently a problem
            if (ex.getActualSize() > 1) {
                logger.warn("Too many phone records found with type=" + typeId + " for UserID=" + userId + "\n  " + ex.getLocalizedMessage());
            }
        }

        return phone;
    }
}
