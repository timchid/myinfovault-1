/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: DossierAppointmentAttributes.java
 */

package edu.ucdavis.mw.myinfovault.dossier.attributes;

import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import edu.ucdavis.myinfovault.MIVConfig;

/**
 * This class represents attributes for a dossier appointment.
 *
 * @author Rick Hendricks
 * @since 3.0
 */
public class DossierAppointmentAttributes implements Serializable
{
    private static final long serialVersionUID = 201112291028L;

    private Map<String, String> departmentMap = null;
    private boolean isComplete = false;
    private Map<String, DossierAttribute>attributes = new LinkedHashMap<String, DossierAttribute>();

    /**
     * Create attributes for a dossier appointment.
     *
     * @param schoolId appointment school ID
     * @param deptId appointment department ID
     * @param isPrimary if this represents a primary appointment
     */
    public DossierAppointmentAttributes(int schoolId, int deptId, boolean isPrimary)
    {
        this.setDepartmentMap(schoolId, deptId);
        this.setPrimary(isPrimary);
    }

    /**
     * @return map of dossier attribute data by the attribute name
     * @see MIVConfig#getMap(String)
     */
    public Map<String, String> getDepartmentMap()
    {
        return departmentMap;
    }

    /**
     * Initialize the department map for the given school and department IDs.
     *
     * @param schoolId appointment school ID
     * @param deptId appointment department ID
     */
    private void setDepartmentMap(int schoolId, int deptId)
    {
        Map<String, Map<String, String>> deptMap = MIVConfig.getConfig().getMap("departments");

        String key = schoolId + ":" + deptId;
        Map<String, String> found = deptMap.get(key);

        this.departmentMap = new HashMap<String, String>();
        if (found == null) {
            Logger.getLogger(this.getClass()).warn(
                    "DossierAppointmentAttributes.setDepartmentMap: null entry from departments for key [" + key + "]");
        }
        else {
            this.departmentMap.putAll(found);
        }
        //this.departmentMap = new HashMap<String, String>(MIVConfig.getConfig().getMap("departments").get(schoolId+":"+deptId));
    }

    /**
     * @return map of all {@link DossierAttribute dossier attributes} stored for the dossier appointment.
     */
    public Map<String, DossierAttribute> getAttributes()
    {
        return attributes;
    }

    /**
     * Get the dossier attribute with the given name.
     *
     * @param attributeName dossier attribute name
     * @return dossier attribute with the given name
     */
    public DossierAttribute getAttribute(String attributeName)
    {
        for (String key : getAttributes().keySet())
        {
            DossierAttribute attribute = getAttributes().get(key);

            if (attribute.getAttributeName().equalsIgnoreCase(attributeName))
            {
                return attribute;
            }
        }

        return null;//no attribute with the given name
    }

    /**
     * @return appointment school ID
     */
    public String getSchoolId()
    {
        return this.departmentMap.get("schoolid");
    }

    /**
     * @return appointment department ID
     */
    public String getDeptId()
    {
        return this.departmentMap.get("departmentid");
    }

    /**
     * @param isPrimary if this represents a primary appointment
     */
    private void setPrimary(boolean isPrimary)
    {
        departmentMap.put("primary", Boolean.toString(isPrimary));
    }

    /**
     * @return if this represents a primary appointment
     */
    public boolean isPrimary()
    {
        return Boolean.parseBoolean(departmentMap.get("primary"));
    }

    /**
     * @param isComplete
     */
    public void setComplete(boolean isComplete)
    {
        this.isComplete = isComplete;
    }

    /**
     * @return
     */
    public boolean isComplete()
    {
        return this.isComplete;
    }

    /**
     * @return appointment school description
     */
    public String getSchoolDescription()
    {
        return this.departmentMap.get("school");
    }

    /**
     * @return appointment department description
     */
    public String getDepartmentDescription()
    {
        return this.departmentMap.get("description");
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        return String.format("[%s %s - %s, complete: %s]", isPrimary() ? "Primary" : "Joint",
                getSchoolDescription(), getDepartmentDescription(), isComplete());
    }
}
