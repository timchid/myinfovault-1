package edu.ucdavis.mw.myinfovault.mapping.biosketch;


import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import edu.ucdavis.mw.myinfovault.domain.biosketch.Biosketch;
import edu.ucdavis.mw.myinfovault.domain.biosketch.BiosketchAttributes;
import edu.ucdavis.mw.myinfovault.domain.biosketch.BiosketchExclude;
import edu.ucdavis.mw.myinfovault.domain.biosketch.BiosketchExclude.ExcludeStatus;
import edu.ucdavis.mw.myinfovault.domain.biosketch.BiosketchSection;
import edu.ucdavis.mw.myinfovault.web.spring.biosketch.BiosketchDesignCommand;
import edu.ucdavis.mw.myinfovault.web.spring.biosketch.DesignRecord;
import edu.ucdavis.myinfovault.PropertyManager;
import edu.ucdavis.myinfovault.data.DisplaySection;
import edu.ucdavis.myinfovault.data.MIVUtil;
import edu.ucdavis.myinfovault.designer.DesignRecordType;
import edu.ucdavis.myinfovault.format.QuoteCleaner;


public class BiosketchDesignMapper
{
    /**
     * Method maps the command object of the design page to the Biosketch object by updating the attribute list,sections,exclude list
     * @param biosketchDesignCommand
     * @param biosketch
     * @return Biosketch
     */
    public Biosketch mapToBiosketch(BiosketchDesignCommand biosketchDesignCommand, Biosketch biosketch)
    {
        List<DesignRecord> displayList = biosketchDesignCommand.getRecordList();
        List<BiosketchExclude> excludeList = biosketch.getBiosketchData().getBiosketchExcludeList();
        Iterable<BiosketchAttributes> attributeList = biosketch.getBiosketchAttributes();
        Iterable<BiosketchSection> bioSectionList = biosketch.getBiosketchSections();

        //walk thru the displayList(DesignRecord) and set the corresponding properties in the biosketch
        for (DesignRecord ds : displayList)
        {
            switch (ds.getType())
            {
                case HEADING:
                case SUBHEADING:
                case DUMMYHEADING:
                    //based on the displayable flag set the displayHeader of the corresponding BiosketchSection
                    if (!ds.isDisplayable() || ds.isDisabled()) continue;
                    int sectionID = ds.getId();
                    for (BiosketchSection section : bioSectionList)
                    {
                        if (section.getId() != sectionID) continue;
                        section.setDisplayHeader(ds.isDisplay());
                        break;
                    }
                    break;

                case ADDITIONALHEADER:
                case RECORD:
                    //set the excludeList
                    BiosketchExclude exclude = new BiosketchExclude(ds.getRecType(), 0, biosketch.getUserID());
                    exclude.setRecordID(ds.getId());
                    exclude.setBiosketchID(biosketch.getId());
                    exclude.setStatus(ExcludeStatus.EXISTING);
                    boolean found = false;
                    for (BiosketchExclude e : excludeList)
                    {
                        if (e.equals(exclude))
                        {
                            if (ds.isDisplay())
                            {
                                // remove it
                                e.setStatus(ExcludeStatus.DELETED);
                            }
                            else
                            {
                                // tag it as same
                                e.setStatus(ExcludeStatus.EXISTING);
                            }
                            found = true;
                            break;
                        }
                    }
                    if (!found && !ds.isDisplay())
                    {
                        // add it
                        exclude.setStatus(ExcludeStatus.ADDED);
                        excludeList.add(exclude);
                    }
                    break;

                case ATTRIBUTE:
                    //set the display flags in the attribute list
                    for (BiosketchAttributes attr : attributeList)
                    {
                        if ( ! (attr.getId() == ds.getId() && attr.getName().equals(ds.getRecType())) ) continue;
                        attr.setDisplay(ds.isDisplay());
                        break;
                    }
                    break;
                default: break;
            }
        }

        return biosketch;
    }//end of mapToBiosketch



    /**
     * Method maps the biosketch object to BiosketchDesignCommand(the command class of the design page)
     * @param biosketch
     * @param biosketchDesignCommand
     * @param biosketchType
     * @return BiosketchDesignCommand
     */
    public BiosketchDesignCommand mapToBiosketchDesignCommand(Biosketch biosketch, BiosketchDesignCommand biosketchDesignCommand, String biosketchType)
    {
        QuoteCleaner quoteCleaner =  new QuoteCleaner();
        biosketchDesignCommand.setBiosketchName(quoteCleaner.format(MIVUtil.escapeMarkup(biosketch.getName())));
        if(biosketchType.equals("cv"))
        {
            biosketchDesignCommand.setRecordList(getCVDesignRecords(biosketch));
        }
        else if(biosketchType.equals("nih"))
        {
            biosketchDesignCommand.setRecordList(getNIHDesignRecords(biosketch));
        }
        return biosketchDesignCommand;
    }



    /**
     * Method processes the Biosketch Sections to return the data required for the Design page
     * @param biosketch Biosketch object for which the design records need to be retrieved.
     * @return List<DesignRecord> List of design records to be displayed on the NIH Design page.
     */
    private List<DesignRecord> getNIHDesignRecords(Biosketch biosketch)
    {
        List<DesignRecord> displayList = new LinkedList<DesignRecord>();
        Properties pstr = PropertyManager.getPropertySet("nihheaders", "strings");
        // List of display sections
        Iterable<DisplaySection> displaySections = biosketch.getBiosketchData().getDisplaySectionList();
        // List of excluded records
        List<BiosketchExclude> excludeList = biosketch.getBiosketchData().getBiosketchExcludeList();
        // List of sections to be displayed(BiosketchSections)
        Iterable<BiosketchSection> bioSectionList = biosketch.getBiosketchSections();
        // List of attributes
        Iterable<BiosketchAttributes> attributeList = biosketch.getBiosketchAttributes();
        boolean first = true;
        for (BiosketchSection section: bioSectionList)
        {
            String sectionName = section.getSectionName();
            //Always process the attribute list as the first section in the page
            if (first)
            {
                buildAttributeSection(displayList, pstr, attributeList, section);
                first = false;
                continue;
            }
            //if the display flag is false or if the section is not part of the biosketch continue--------NIH headers ALWAYS display!
            if(/*!section.isDisplay() ||*/ section.getDisplayInBiosketch() != 0) continue;
            //Check if it is a dummy section
            if(sectionName.endsWith("-header") && pstr.getProperty(sectionName + "-dummyheading") != null)
            {
                //Add dummy section record and continue
                DesignRecord dummyHeading = new DesignRecord(section.isMayHideHeader(),DesignRecordType.DUMMYHEADING);
                dummyHeading.setId(section.getId());
                dummyHeading.setRecType(section.getSectionName());
                if (section.isMayHideHeader()) {
                    dummyHeading.setDisplay(section.isDisplayHeader());
                }
                dummyHeading.setPreview(pstr.getProperty(sectionName+"-dummyheading"));
                displayList.add(dummyHeading);
                continue;
            }
            //Check if it is a main Section
            if (section.isMainSection() && pstr.getProperty(sectionName+"-heading") != null)
            {

                DesignRecord mainSection = new DesignRecord(section.isMayHideHeader(),DesignRecordType.HEADING);
                mainSection.setId(section.getId());
                mainSection.setRecType(sectionName);
                if (section.isMayHideHeader()) {
                    mainSection.setDisplay(section.isDisplayHeader());
                }
                mainSection.setPreview(pstr.getProperty(sectionName+"-heading"));
                displayList.add(mainSection);
            }
            //get the subheading if any
            if(pstr.getProperty(sectionName+"-subheading") != null)
            {
                DesignRecord subSection = new DesignRecord(section.isMayHideHeader(),DesignRecordType.SUBHEADING);
                subSection.setId(section.getId());
                subSection.setRecType(section.getSectionName());
                if (section.isMayHideHeader()) {
                    subSection.setDisplay(section.isDisplayHeader());
                }
                subSection.setPreview(pstr.getProperty(sectionName+"-subheading"));
                displayList.add(subSection);
            }
            //Check if there is a matching displaySection(records for that type)
            //find it in the displaysectionlist
            List<DisplaySection> dsList = findDisplaySection(section,displaySections);
            if(dsList.size()>0)
            {
                //displaysection found
                for (DisplaySection ds : dsList)
                {
                    if(ds.getRecords().size() > 0)
                    {
                        //BiosketchExclude excludeTest = new BiosketchExclude("", 0, 0);
                        Map<String, Map<String, String>> records = ds.getRecords();
                        for (String recordID : records.keySet())
                        {
                            DesignRecord record = new DesignRecord(true,DesignRecordType.RECORD);
                            record.setYear(records.get(recordID).get("year"));
                            record.setPreview(records.get(recordID).get("preview"));
                            record.setRecType(sectionName);
                            record.setId(Integer.parseInt(recordID));
                            //set display flag from excludelist
                            record.setDisplay(!isExcluded(excludeList, sectionName, recordID));
                            displayList.add(record);
                        }
                    }
                }//end for each display section
            }//end if displaySection found
        } // for each section

        return displayList;
    }//end of getNIHDesignRecords



    /**
     * Method processes the Biosketch Sections to return the data required for the Design page
     * @param biosketch Biosketch object for which the design records need to be retrieved.
     * @return List<DesignRecord> List of design records to be displayed on the CV Design page.
     */
    public List<DesignRecord> getCVDesignRecords(Biosketch biosketch)
    {
        //List od design records to be populated
        List<DesignRecord> displayList = new LinkedList<DesignRecord>();
        Properties pstr = PropertyManager.getPropertySet("cvheaders", "strings");
        // List of display sections
        Iterable<DisplaySection> displaySections = biosketch.getBiosketchData().getDisplaySectionList();
        // List of excluded records
        List<BiosketchExclude> excludeList = biosketch.getBiosketchData().getBiosketchExcludeList();
        // List of sections to be displayed(BiosketchSections)
        Iterable<BiosketchSection> bioSectionList = biosketch.getBiosketchSections();
        Map<String,Map<String,String>> sectionHeaderMap = biosketch.getBiosketchData().getSectionHeaderMap();
        // List of attributes
        Iterable<BiosketchAttributes> attributeList = biosketch.getBiosketchAttributes();
        /* Always process the first section as attribute section.
         * For each BiosketchSection-Check if it is a dummy section header and add the corresponding DesignRecord
         * If not a dummy section header-find if the section has matching display section(s).
         * If there is/are matching display section(s)-check if the section has isMainSection=true, create a record for the heading
         * Get the subheading for the section either from the properties file(PRE-DEFINED for NIH) or get it from the 'heading'
         * variable of the corresponding DisplaySection.
         * Get the records under the section.
         *
         */
        DesignRecord previousHeading = null;
        boolean headingDisplay = false;
        boolean first = true;
        boolean hasData = false;
        for (BiosketchSection section: bioSectionList)
        {
            String sectionName = section.getSectionName();
            if(displayList.size()>0)
            {
                if(displayList.get(displayList.size()-1).getType().equals(DesignRecordType.DUMMYHEADING))
                {
                    //store the section heading in this variable to enable it later if there is data present in
                    //this section. CLEARING this variable as soon as a new DUMMYHEADING is created.
                    previousHeading = displayList.get(displayList.size()-1);
                }
            }
            //Always process the attribute list as the first section in the page
            if (first)
            {
                buildAttributeSection(displayList, pstr, attributeList, section);
                if(!section.isDisplay())
                {
                    DesignRecord record = new DesignRecord();
                    record.setType(DesignRecordType.HEADING);
                    record.setId(section.getId());
                    record.setRecType(section.getSectionName());
                    record.setDisplayable(section.isMayHideHeader());
                    record.setDisplay(false);
                    record.setDisabled(true);
                    record.setPreview(pstr.getProperty("attribute-heading"));
                    displayList.add(record);
                }
                else
                {
                    hasData = true;
                }
                first = false;
                //hasData = true;
                continue;
            }
            //if the display flag is false or if the section is not part of the biosketch continue
            if(section.getDisplayInBiosketch() != 0) continue;

            DesignRecord psuedoHeading = null;
            //Check if it is a dummy section
            if(sectionName.endsWith("-header") && pstr.getProperty(sectionName + "-dummyheading") != null)
            {   //Add dummy section record and continue
                psuedoHeading = new DesignRecord(section.isMayHideHeader(),DesignRecordType.DUMMYHEADING);
                psuedoHeading.setId(section.getId());
                psuedoHeading.setRecType(section.getSectionName());
                if (section.isMayHideHeader())
                {
                    //disable it initially and enable it based on whether there is data in the section or not!
                    psuedoHeading.setDisabled(true);
                }
                //store the value of the displayHeader to set it back in it when it is enabled.
                headingDisplay = section.isDisplayHeader();
                psuedoHeading.setPreview(pstr.getProperty(sectionName+"-dummyheading"));
                displayList.add(psuedoHeading);
                //clear the previous heading
                previousHeading=null;
                continue;
            }
            DesignRecord sectionHeading = new DesignRecord(section.isMayHideHeader(),DesignRecordType.SUBHEADING);
            //Check if it is a main Section/Solitary section
            if (section.isMainSection() && pstr.getProperty(sectionName+"-heading") != null)
            {
                sectionHeading.setType(DesignRecordType.HEADING);
                sectionHeading.setId(section.getId());
                sectionHeading.setRecType(sectionName);
                if (section.isMayHideHeader()) {
                    sectionHeading.setDisplay(section.isDisplayHeader());
                }
                sectionHeading.setPreview(pstr.getProperty(sectionName+"-heading"));
                displayList.add(sectionHeading);
            }
            //get the subheading if any
            else if(pstr.getProperty(sectionName+"-subheading") != null)
            {
                sectionHeading.setId(section.getId());
                sectionHeading.setRecType(section.getSectionName());
                if (section.isMayHideHeader()) {
                    sectionHeading.setDisplay(section.isDisplayHeader());
                }
                sectionHeading.setPreview(pstr.getProperty(sectionName+"-subheading"));
                displayList.add(sectionHeading);
            }
            //Any other section
            else if(sectionHeaderMap.get(sectionName) != null)
            {
                //sectionHeaderMap.g
                sectionHeading.setId(section.getId());
                sectionHeading.setRecType(section.getSectionName());
                if (section.isMayHideHeader()) {
                    sectionHeading.setDisplay(section.isDisplayHeader());
                }
                sectionHeading.setPreview(sectionHeaderMap.get(sectionName).get("header"));
                displayList.add(sectionHeading);
            }


            //Check if there is a matching displaySection(records for that type)
            //find it in the displaysectionlist
            List<DisplaySection> dsList = findDisplaySection(section,displaySections);
            if(!section.isDisplay() || dsList.size()==0)
            {
                //sectionHeading.setDisplay(false);
                sectionHeading.setDisabled(true);
                continue;
            }
            //get the section heading and if dsList=0 set displayHeader=0
            else
            {
                //enable the BLUE/Psuedoheading if the current section is not a mainsection
                if(previousHeading != null && !section.isMainSection())
                {
                    previousHeading.setDisabled(false);
                    previousHeading.setDisplay(headingDisplay);
                }
                hasData = true;
                //displaysection found
                for (DisplaySection ds : dsList)
                {
                    //if section subheading not found in the properties file get it from the display section
                    if(ds.getRecordNumber()>0 && ds.getHeading()!= null)
                    {
                        DesignRecord subSection = new DesignRecord(section.isMayHideHeader(),DesignRecordType.SUBHEADING);
                        subSection.setType(DesignRecordType.ADDITIONALHEADER);
                        subSection.setId(ds.getRecordNumber());
                        subSection.setDisplay(!isExcluded(excludeList, sectionName+"-header", String.valueOf(ds.getRecordNumber())));
                        subSection.setRecType(sectionName+"-header");
                        subSection.setPreview(ds.getHeading());
                        displayList.add(subSection);
                    }
                    if(ds.getRecords().size() > 0)
                    {
                        //BiosketchExclude excludeTest = new BiosketchExclude("", 0, 0);
                        Map<String, Map<String, String>> records = ds.getRecords();
                        for (String recordID : records.keySet())
                        {
                            DesignRecord record = new DesignRecord(true,DesignRecordType.RECORD);
                            record.setYear(records.get(recordID).get("year"));
                            record.setPreview(records.get(recordID).get("preview"));
                            record.setRecType(sectionName);
                            record.setId(Integer.parseInt(recordID));
                            //set display flag from excludelist
                            record.setDisplay(!isExcluded(excludeList, sectionName, recordID));
                            displayList.add(record);
                        }
                    }
                }//end for each display section
            }//end if displaySection found

        } // for each section
        if (!hasData)
        {
            displayList.clear();
            DesignRecord noDataInd = new DesignRecord(false, DesignRecordType.NODATA);
            noDataInd.setPreview("No data has been selected.");
            displayList.add(noDataInd);
        } // no Data to be displayed

        return displayList;
    }//end of getCVDesignRecords



    /**
     * Method finds and returns the list of displaysections which match the section passed
     * @param bioSection
     * @param displaySections
     * @return List of DisplaySections corresponding to the BiosketechSection passed
     */
    private List<DisplaySection> findDisplaySection(BiosketchSection bioSection, Iterable<DisplaySection> displaySections)
    {
        List<DisplaySection> ds = new LinkedList<DisplaySection>();
        for (DisplaySection section : displaySections)
        {
            if (section.getKey().equals(bioSection.getSectionName()))
            {
                ds.add(section);
                if(!bioSection.getSectionName().endsWith("-additional")) break;
            }
        }
        return ds;
    }

    /**
     * Method returns if the a particular record is in the exclude list of the biosketch.
     * @param excludeList
     * @param recType
     * @param recordID
     * @return
     */
    private boolean isExcluded(List<BiosketchExclude> excludeList, String recType, String recordID)
    {
        BiosketchExclude excludeTest = new BiosketchExclude("", 0, 0);
        excludeTest.setRecType(recType);
        excludeTest.setRecordID(Integer.parseInt(recordID));
        return excludeList.contains(excludeTest);
    }
    /**
     * @param displayList
     * @param pstr
     * @param attributeList
     */
    private void buildAttributeSection(List<DesignRecord> displayList, Properties pstr, Iterable<BiosketchAttributes> attributeList, BiosketchSection section)
    {
        if(!section.isDisplay()) return;
        DesignRecord record = new DesignRecord();
        record.setType(DesignRecordType.HEADING);
        record.setId(section.getId());
        record.setRecType(section.getSectionName());
        record.setDisplayable(section.isMayHideHeader());
        record.setDisplay(section.isDisplayHeader());
        record.setPreview(pstr.getProperty("attribute-heading"));
        displayList.add(record);

        for (BiosketchAttributes attribute : attributeList)
        {
            DesignRecord rec = new DesignRecord();
            String attributeName = pstr.getProperty("attribute-"+attribute.getName());
            rec.setType(DesignRecordType.ATTRIBUTE);
            rec.setId(attribute.getId());
            rec.setRecType(attribute.getName());
            rec.setDisplayable(true);
            rec.setDisplay(attribute.isDisplay());
            rec.setAttributeName(attributeName);
            rec.setPreview(MIVUtil.escapeMarkup(attribute.getValue()));
            //rec.setPreview(attribute.getName()+": "+attribute.getValue());
            displayList.add(rec);
        }
    }
}
