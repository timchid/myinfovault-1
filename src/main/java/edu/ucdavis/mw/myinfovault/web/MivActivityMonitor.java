/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: MivActivityMonitor.java
 */


package edu.ucdavis.mw.myinfovault.web;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.Callable;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * TODO: Add Javadoc
 * @author Stephen Paulsen
 *
 */
public class MivActivityMonitor
{
    private static Logger logger = LoggerFactory.getLogger(MivActivityMonitor.class);

    private static int instanceCount = 0;
    private final int instanceNum;

    private static final MivActivityMonitor singleton = new MivActivityMonitor();

    private static final long EXPIRE_TIME        = TimeUnit.HOURS.toMillis(1L);   // Clear idle activity after 1 hour.
    private static final long LOGOUT_EXPIRE_TIME = TimeUnit.MINUTES.toMillis(5L); // Clear logout notice after 5 minutes.

    private static final long DELAY_TIME = TimeUnit.MINUTES.toMillis(15L);     // Delay for 15 minutes before starting.
    private static final long REPEAT_TIME = TimeUnit.MINUTES.toMillis(5L);     // Run every 5 minutes / Wait 5 minutes between runs.
    private static final long INCREMENT_TIME = TimeUnit.MINUTES.toMillis(3L);  // Add 3 minutes to wait every time there's nothing to do.
    private static final long  MAX_WAIT_TIME = TimeUnit.MINUTES.toMillis(20L); // Never wait longer than 20 minutes.

    private static final String LOGOUT_MESSAGE = "logging out";

    private ScheduledExecutorService cleaner;

    private boolean stopped = false;


    /**
     * Private constructor so only the singleton can be created.
     */
    private MivActivityMonitor()
    {
        synchronized (this.getClass()) {
            this.instanceNum = ++instanceCount;
        }
        this.cleaner = Executors.newScheduledThreadPool(1);

        final Callable<Void> call = new Callable<Void>() {
            private long repeatTime = REPEAT_TIME;
            @Override
            public Void call() throws Exception
            {
                // Per http://code.nomad-labs.com/2011/12/09/mother-fk-the-scheduledexecutorservice/
                // Catch *any* error that occurs so the task doesn't get terminated by the Executor.
                try
                {
                    logger.debug("Monitor #{} Running Cleanup", instanceNum);
                    long now = new Date().getTime();

                    // When nothing is in the queue increase the wait time.
                    if (events.size() == 0) {
                        if (repeatTime < MAX_WAIT_TIME) {
                            repeatTime += INCREMENT_TIME;
                        }
                    }
                    else {
                        repeatTime = REPEAT_TIME;
                    }

                    for (Iterator<Entry<String, Activity>> iter = events.entrySet().iterator(); iter.hasNext();)
                    {
                        Entry<String, Activity> entry = iter.next();
                        Activity a = entry.getValue();
                        long age = now - a.getWhen().getTime();
                        logger.debug("Monitor #{} Checking activity [{}] age is {} ms ({} max)", new Object[] { instanceNum, a, age, EXPIRE_TIME });
                        if ( age > EXPIRE_TIME ||
                            (age > LOGOUT_EXPIRE_TIME && LOGOUT_MESSAGE.equals(a.getWhat())) )
                        {
                            iter.remove();
                            logger.debug("Monitor #{} removed old activity [{}]", instanceNum, a);
                        }
                    }
/*
                    for (String key : events.keySet())
                    {
                        Activity a = events.get(key);
                        long age = now - a.getWhen().getTime();
                        logger.debug("Monitor #{} Checking activity [{}] age is {} ms ({} max)", new Object[] { instanceNum, a, age, EXPIRE_TIME });
                        if ( age > EXPIRE_TIME ||
                            (age > LOGOUT_EXPIRE_TIME && LOGOUT_MESSAGE.equals(a.getWhat())) )
                        {
                            events.remove(key);
                            logger.debug("Monitor #{} removed old activity [{}]", instanceNum, a);
                        }
                    }
*/
                }
                catch (Throwable t)
                {
                    logger.warn("Activity cleaner suppressed an error:", t);
                }

                logger.debug("Waiting {} minutes before next run.",
                            TimeUnit.MINUTES.convert(repeatTime, TimeUnit.MILLISECONDS));

                cleaner.schedule(this, repeatTime, TimeUnit.MILLISECONDS); // Schedule the next run of the cleaner.
                return null;
            }
        };

        this.cleaner.schedule(call, DELAY_TIME, TimeUnit.MILLISECONDS);

        logger.info("Activity Monitor #{} Created ({})", instanceNum, this.toString());
        logger.debug("Created from (stack trace):\n", new Exception("Object Creation Trace").fillInStackTrace());
    }


    public static MivActivityMonitor getMonitor()
    {
        return singleton;
    }


    public synchronized boolean isRunning()
    {
        return ! this.stopped;
    }
    public synchronized boolean isStopped()
    {
        return this.stopped;
    }

    public synchronized void shutdown()
    {
        if (! stopped)
        {
            logger.info("MivActivityMonitor #{} SHUTDOWN called ({})", instanceNum, this.toString());
            this.cleaner.shutdownNow();
            this.stopped = true;
        }
    }


    private Map<String, Activity> events = new HashMap<String, Activity>();

    public void logLogin(String user)
    {
        Activity a = events.get( user );
        if (a == null) {
            a = new Activity(user);
            events.put(user, a);
        }
        else {
            a.setActivity("logged in again");
        }
    }


    /**
     *
     * @param user
     */
    public void logLogout(String user)
    {
        this.logActivity(user, LOGOUT_MESSAGE);
        // schedule to remove this Activity from the events hash in 4-5 minutes.
    }


    /**
     *
     * @param user
     * @param activity
     */
    public void logActivity(String user, String activity)
    {
        Activity a = events.get( user );
        if (a == null) {
            a = new Activity(user);
            events.put(user, a);
        }
        // We may be asked, by the filter, to log activity for someone who is not done logging in yet.
        // They won't have an entry in the event table, so just skip it if we get a null.
        a.setActivity( cleanPath(activity) );
    }


    public List<Activity> getActivity()
    {
        List<Activity> results = new ArrayList<Activity>( events.values() );
        Collections.sort( results );
        return results;
    }


    // A lot of what we log are GET url requests.
    // Clean them up to shorten them and toss out things like the flow key.
    private String cleanPath(final String path)
    {
        // _flowExecutionKey=_c2456F0A5-C252-AC09-1F82-4135B07813AE_kE432E5E6-D68F-8BAA-33BA-D4A17B61AAF6
        // _flowId=openactions-flow
        String newPath = executionKeyPattern.matcher(path).replaceAll("");
        if (! path.equals(newPath)) {
            newPath = newPath.replaceFirst("\\?$", ""); // drop any left-over trailing '?'
            logger.debug("cleanPath changed [{}] to [{}]", path, newPath);
        }
        return newPath;
    }
    private static final Pattern executionKeyPattern = Pattern.compile("&?_flowExecutionKey=_[0-9A-Za-z_-]+");


    public class Activity implements Comparable<Activity>
    {
        private final String user;
        private long when;
        private String lastActivity;

        public Activity(String user)
        {
            this.user = user;
            this.when = System.currentTimeMillis();
            this.lastActivity = "logged in";
        }

        Activity setActivity(String what)
        {
            this.lastActivity = what;
            this.when = System.currentTimeMillis();

            return this;
        }

        public String getWho()
        {
            return this.user;
        }
        public String getWhat()
        {
            return this.lastActivity;
        }
        public Date getWhen()
        {
            return new java.util.Date(this.when);
        }

        @Override
        public String toString()
        {
            return getWhen() + " : " + getWho() + " : " + getWhat();
        }

        @Override
        public int compareTo(Activity other)
        {
            if (this == other) return 0;
            return this.getWhen().compareTo(other.getWhen());
        }
    }
}
