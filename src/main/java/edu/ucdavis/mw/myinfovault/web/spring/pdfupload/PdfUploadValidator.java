/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: PdfUploadValidator.java
 */

package edu.ucdavis.mw.myinfovault.web.spring.pdfupload;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.itextpdf.text.pdf.PdfReader;

import edu.ucdavis.mw.myinfovault.domain.dossier.Dossier;
import edu.ucdavis.mw.myinfovault.service.MivServiceLocator;
import edu.ucdavis.mw.myinfovault.service.dossier.DossierService;
import edu.ucdavis.mw.myinfovault.service.dossier.UploadDocumentDto;
import edu.ucdavis.mw.myinfovault.service.dossier.WorkflowException;
import edu.ucdavis.myinfovault.MIVConfig;
import edu.ucdavis.myinfovault.data.MIVUtil;


/**
 * @author Rick Hendricks
 * @since MIV 3.0
 */
public class PdfUploadValidator implements Validator
{
    private static final Logger logger = Logger.getLogger(PdfUploadValidator.class);

    /*
     * (non-Javadoc)
     * @see org.springframework.validation.Validator#supports(java.lang.Class)
     */
    @Override
    public boolean supports(@SuppressWarnings("rawtypes") Class clazz)
    {
        boolean rc = clazz.equals(PdfUploadForm.class);
        return rc;
    }


    /*
     * (non-Javadoc)
     * @see org.springframework.validation.Validator#validate(java.lang.Object, org.springframework.validation.Errors)
     */
    @Override
    public void validate(Object target, Errors errors)
    {
        PdfUploadForm upload = (PdfUploadForm) target;
        CommonsMultipartFile file = upload.getUploadFileName();
        String filename = file.getOriginalFilename();

        if(upload.isYearField())
        {
        	edu.ucdavis.myinfovault.data.Validator validator = new edu.ucdavis.myinfovault.data.Validator(new HashMap<String, String[]>());
            validator.validate("year", upload.getYear(), "required,year:Y+1");

            if (!validator.isValid())
            {
            	String errorMessage = "";

                for(String msg: validator.getErrorMessageList())
                {
                    if(errorMessage.trim().length() > 0)
                    {
                        errorMessage += ", ";
                    }

                    errorMessage += msg;
                }

                errors.reject("", "Year : "+errorMessage);
            }
        }

        // No file; form was submitted without a file in the field.
        if (filename.length() <= 0) {
            errors.reject("", "Upload PDF File : No upload file name was provided.");
            return;
        }

        // Check for max uploads for this document type only if doing an upload for a dossier.
        // Non dossier upload max count is checked prior to calling upload. Zero indicates no limit.
        if (upload.getMaxUploads() > 0 && upload.getDossierId() > 0)
        {
            try
            {
                DossierService dossierService = MivServiceLocator.getDossierService();
                Dossier dossier = dossierService.getDossier(upload.getDossierId());
                List <UploadDocumentDto> uploadDocumentDtoList = dossierService.getUploadFileDtoByTypeUserSchoolAndDepartment(upload.getDossierId(), upload.getDocumentId(), dossier.getAction().getCandidate().getUserId(), upload.getSchoolId(), upload.getDepartmantId());

                if (uploadDocumentDtoList.size() >= upload.getMaxUploads())
                {
                    errors.reject("", "Maximum upload count of " + upload.getMaxUploads() + " has been reached for this document type.");
                    // Set the file to null to avoid errors when spring tries to desierialize the file object and delete the file
                    upload.setUploadFileName(null);
                    return;
                }
            }
            catch (WorkflowException e)
            {
                logger.warn("Unable to determine upload count for dossier "+upload.getDossierId()+ ", upload file "+upload.getUploadFileDescription());
                e.printStackTrace();
            }
        }

        // If upload is redactable, make sure either redacted or non-redacted is selected
        if (upload.isRedactable() && upload.getLetterType() == null)
        {
            errors.reject("", "Either \"Redacted\" or \"Non-Redacted\" must be selected.");
            upload.setUploadFileName(null);     // Clear the file name so Apache commons upload does not attempt "clean-up"
        }

        // Default to 10mb
        int maxUploadBytes = 10000000;

        // Get the maximum upload from the config properties in K and convert to bytes
        String maxUploadSizeStr = MIVConfig.getConfig().getProperty("default-config-pdfupload-maxsize");
        if (maxUploadSizeStr != null)
        {
            try {
                maxUploadBytes = Integer.parseInt(maxUploadSizeStr) * 1000;
            }
            catch (NumberFormatException nfe) {
                // Ignore - default is set above
            }
        }

        // Check for 10mb max size
        if (file.getSize() > maxUploadBytes)
        {
            String msg = MIVUtil.escapeMarkup("The uploaded document \"" + filename +
                    "\" is too large. There is a " + maxUploadBytes / 1000000 + " MB size limit.");
            logger.warn("Rejecting file: " + msg);
            errors.reject("", msg);
            upload.setUploadFileName(null);     // Clear the file name so Apache commons upload does not attempt "clean-up"
        }

        // If there are no errors, attempt to open the file with the PDF Reader and check for encryption
        if (!errors.hasErrors())
        {
            // If the PDF reader cannot open this file, it is either not available or not a PDF file.
            try
            {
                PdfReader dataReader = new PdfReader(file.getBytes());
                // MIV-5339 - Do not allow encrypted uploads
                if (dataReader.isEncrypted())
                {
                    String msg = MIVUtil.escapeMarkup("The upload document \"" + filename + "\" is encrypted and cannot be uploaded.");
                    logger.warn("Rejecting file: " + msg);
                    errors.reject("", msg);
                    upload.setUploadFileName(null);     // Clear the file name so Apache commons upload does not attempt "clean-up"
                }
                dataReader.close();
            }
            catch (IOException e)
            {
                String msg = MIVUtil.escapeMarkup("The upload document \"" + filename + "\" is not a valid PDF file.");
                logger.warn("Rejecting file: " + msg, e);
                errors.reject("", msg);
                upload.setUploadFileName(null);     // Clear the file name so Apache commons upload does not attempt "clean-up"
            }
        }else{
        	upload.setUploadFileName(null); // Clear the file name so Apache commons upload does not attempt "clean-up"
        	return;
        }
    }
}
