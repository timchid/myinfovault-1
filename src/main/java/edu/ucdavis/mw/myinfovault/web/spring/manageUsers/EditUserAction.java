/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: EditUserAction.java
 */

package edu.ucdavis.mw.myinfovault.web.spring.manageUsers;

import org.springframework.webflow.execution.Event;
import org.springframework.webflow.execution.RequestContext;

import edu.ucdavis.mw.myinfovault.service.authorization.AuthorizationService;
import edu.ucdavis.mw.myinfovault.service.authorization.Permission;
import edu.ucdavis.mw.myinfovault.service.person.UserService;
import edu.ucdavis.mw.myinfovault.web.spring.commons.MivFormAction;

/**
 * Webflow form action for editing users.
 *
 * @author Craig Gilmore
 * @since MIV 4.8
 */
public class EditUserAction extends MivFormAction
{
    private static final String[] breadcrumbs = {"Edit a User's Account: Search", "Search Results"};

    /**
     * Create edit user form action bean.
     *
     * @param userService Spring-injected user service
     * @param authService Spring-injected authorization service
     */
    public EditUserAction(UserService userService,
                         AuthorizationService authService)
    {
        super(userService, authService);
    }

    /**
     * Checks if user has general permission to edit a user.
     *
     * @param context Request context from webflow
     * @return boolean status, success or error
     */
    public Event hasPermission(RequestContext context)
    {
        return authorizationService.hasPermission(getShadowPerson(context),
                                                  Permission.EDIT_USER,
                                                  null)
             ? allowed()
             : denied();
    }

    /**
     * Add the bread crumb for the previous page in this flow.
     *
     * @param context Webflow request context
     */
    public void addBreadcrumb(RequestContext context)
    {
        addBreadcrumbs(context, breadcrumbs[0]);
    }

    /**
     * Add bread crumbs for edit user as parent flow
     *
     * @param context Webflow request context
     */
    public void addBreadcrumbs(RequestContext context)
    {
        addBreadcrumbs(context, breadcrumbs);
    }

    /**
     * remove bread crumbs for edit user as parent flow
     *
     * @param context Webflow request context
     */
    public void removeBreadcrumbs(RequestContext context)
    {
        removeBreadcrumbs(context, breadcrumbs.length);
    }
}
