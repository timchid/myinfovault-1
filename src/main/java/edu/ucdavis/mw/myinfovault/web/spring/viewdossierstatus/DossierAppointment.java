/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: DossierAppointment.java
 */


package edu.ucdavis.mw.myinfovault.web.spring.viewdossierstatus;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * TODO: Add Javadoc comments!
 *
 * @author Mary Northup
 * @since MIV 3.0
 */
public class DossierAppointment implements Serializable
{
    private static final long serialVersionUID = 200907231135L;

    private boolean isPrimary = true;
    private boolean isUpdate = false;  // for open actions: if the viewer matches primary or joint this dictates which they can Update
    private boolean isComplete = false;
    private String processMsg = null;
    private String appointmentDescription = null;
    private boolean showJointSendBackLink = false;
    private boolean isJointComplete = false;
    private String jointWaitingMsg = null;
    private int schoolId = 0;
    private int deptId = 0;
    private List<DossierAction> actionList = new ArrayList<DossierAction>();

    public void setPrimary(boolean isPrimary)
    {
        this.isPrimary = isPrimary;
    }
    public boolean getIsPrimary()
    {
        return this.isPrimary;
    }
    public boolean isPrimary()
    {
        return this.isPrimary;
    }

    public void setUpdate(boolean isUpdate)
    {
        this.isUpdate = isUpdate;
    }
    public boolean getIsUpdate()
    {
        return this.isUpdate;
    }
    public boolean isUpdate()
    {
        return this.isUpdate;
    }

    public void setComplete(boolean isComplete)
    {
        this.isComplete = isComplete;
        this.processMsg = isComplete ? "Completed" : "In Progress";
    }
    public boolean getIsComplete()
    {
        return this.isComplete();
    }
    public boolean isComplete()
    {
        return this.isComplete;
    }

// This is entirely determined by the isComplete flag, so don't let callers set it.
// Set it internally when setting the completion flag.
//    public void setProcessMsg(String processMsg)
//    {
//        this.processMsg = processMsg;
//    }
    public String getProcessMsg()
    {
        return this.processMsg;
    }

    public void setAppointmentDescription(String appointmentDescription)
    {
        this.appointmentDescription = appointmentDescription;
    }
    public String getAppointmentDescription()
    {
        return this.appointmentDescription;
    }

    public void setShowJointSendBackLink(boolean showJointSendBackLink)
    {
        this.showJointSendBackLink = showJointSendBackLink;
    }
    public boolean getShowJointSendBackLink()
    {
        return this.showJointSendBackLink;
    }

    public void setSchoolId(int schoolId)
    {
        this.schoolId = schoolId;
    }
    public int getSchoolId()
    {
        return this.schoolId;
    }

    public void setDeptId(int deptId)
    {
        this.deptId = deptId;
    }
    public int getDeptId()
    {
        return this.deptId;
    }

    public List<DossierAction> getActionList()
    {
        return this.actionList;
    }
    public void addAction(DossierAction dossierAction)
    {
        this.actionList.add(dossierAction);
    }

    public void setJointWaitingMsg(String jointWaitingMsg)
    {
        this.jointWaitingMsg = jointWaitingMsg;
    }
    public String getJointWaitingMsg()
    {
        return jointWaitingMsg;
    }

    public void setJointComplete(boolean isJointComplete)
    {
        this.isJointComplete = isJointComplete;
    }
    public boolean isJointComplete()
    {
        return isJointComplete;
    }
    public boolean getIsJointComplete()
    {
        return isJointComplete;
    }

}
