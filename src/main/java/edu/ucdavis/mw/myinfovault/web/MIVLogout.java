/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: MIVLogout.java
 */

package edu.ucdavis.mw.myinfovault.web;

import java.net.MalformedURLException;
import java.net.URL;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import edu.ucdavis.myinfovault.MIVConfig;
import edu.ucdavis.myinfovault.MIVSession;
import edu.ucdavis.myinfovault.data.QueryTool;


public class MIVLogout extends HttpServlet
{
    public static final long serialVersionUID = 1L;
    ServletConfig config = null;
    //String loginURL = "";
    String logoutPath = null;
    String timeoutPath = null;
    MivActivityMonitor am;

    @Override
    public void init(ServletConfig config)
    throws ServletException
    {
        this.config = config;
        //loginURL = config.getInitParameter("LoginURL");

        logoutPath = config.getInitParameter("LogoutPath");//LogoutPath
        if (logoutPath == null) logoutPath = "/";
        if (!logoutPath.startsWith("/"))
        {
            logoutPath = "/" + logoutPath;
        }

        timeoutPath = config.getInitParameter("TimeoutPath");//LogoutPath
        if (timeoutPath == null) timeoutPath = "/";
        if (!timeoutPath.startsWith("/"))
        {
            timeoutPath = "/" + timeoutPath;
        }

        am = MivActivityMonitor.getMonitor();
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response)
    {
        //String logoutURL = "https://secureweb.ucdavis.edu:443/form-auth/logout?";
        //String servername = request.getServerName();
        URL loginTargetUrl = null;

        String timoutparm = request.getParameter("timeout");
        try
        {
            String targetPath = timoutparm == null ? logoutPath : timeoutPath;
            loginTargetUrl = new URL("http", request.getServerName(), targetPath);
        }
        catch (MalformedURLException e1)
        {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }

        try
        {
            HttpSession session = request.getSession(false);
            System.out.println("|| -- Logout: timeout var is " + (timoutparm == null ? "" : "not ") + "null");
            System.out.println("|| -- Logout: http session is " + (session == null ? "" : "not ") + "null");
            MIVSession mivSession = MIVSession.getSession(request);
            System.out.println("|| -- Logout: MIV session is " + (mivSession == null ? "" : "not ") + "null");
            String loginName = session.getAttribute("loginName").toString();
            System.out.println("|| -- Logout: loginName==" + loginName);
            if (mivSession != null)
            {
                edu.ucdavis.myinfovault.MIVUser user = mivSession.getUser();
                if (user != null)
                {
                    String query = loginName == null || loginName.trim().length() == 0 ?
                            "UPDATE UserLogin SET LogoutTimestamp=current_timestamp WHERE UserID=?" :
                            "UPDATE UserLogin SET LogoutTimestamp=current_timestamp WHERE UserID=? AND Login=?";
                    QueryTool qt = MIVConfig.getConfig().getQueryTool();
                    qt.runQuery(query, null, "" + user.getUserID(), loginName);
                    am.logLogout(loginName);
                    user.release();
                }
                else {
                    System.out.println("Can't release MIVUser because it's null!");
                }
            }
            else
            {
                System.out.println("Can't release MIVSession because it's null!");
            }
            try { Thread.sleep(1000); } catch (InterruptedException ie) {}
            session.invalidate();

            System.out.println("MIVLogout about to redirect");
            //response.sendRedirect(logoutURL + loginURL);
            //response.sendRedirect(logoutURL + loginTargetUrl.toString());
            response.sendRedirect(loginTargetUrl.toString());
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response)
    {
        doGet(request, response);
    }
}
