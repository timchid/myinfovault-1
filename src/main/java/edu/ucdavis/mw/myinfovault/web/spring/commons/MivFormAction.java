/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: MivFormAction.java
 */

package edu.ucdavis.mw.myinfovault.web.spring.commons;

import java.text.MessageFormat;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Stack;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.binding.convert.ConversionException;
import org.springframework.validation.Errors;
import org.springframework.webflow.action.FormAction;
import org.springframework.webflow.context.servlet.ServletExternalContext;
import org.springframework.webflow.core.collection.ParameterMap;
import org.springframework.webflow.execution.Event;
import org.springframework.webflow.execution.RequestContext;

import edu.ucdavis.mw.myinfovault.domain.dossier.Dossier;
import edu.ucdavis.mw.myinfovault.service.authorization.AuthorizationService;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.mw.myinfovault.service.person.UserService;
import edu.ucdavis.mw.myinfovault.util.StringUtil;
import edu.ucdavis.myinfovault.MIVConfig;
import edu.ucdavis.myinfovault.MIVSession;
import edu.ucdavis.myinfovault.MIVUser;
import edu.ucdavis.myinfovault.PropertyManager;
import edu.ucdavis.myinfovault.exception.MivSevereApplicationError;


/**
 * This class extends the Spring FormAction class for the purpose of initializing
 * some common MIV variables used throughout MIV web flows.
 * The variables are initialized in the setupForm method which must be invoked
 * in the initial action-state of the webflow.
 *
 * @author Rick Hendricks
 * @author Craig Gilmore
 * @author Stephen Paulsen
 * @since 3.0
 */
public abstract class MivFormAction extends FormAction
{
    private static final String PARAMETER_CONVERSION_FAILIURE = "Possible hacking attempt! Unable to convert '{0}' parameter to type '{1}'; continuing with default value '{2}'";

    protected final String className = this.getClass().getSimpleName();

    /**
     * MIV form action logger.
     */
    protected final Logger logger = LoggerFactory.getLogger(this.getClass());

    private final static String BREADCRUMBS = "breadcrumbs";
    protected final static String DOSSIER = "dossier";
    private final static String DOSSIERID = "dossierId";
    private final static String SAVE_STATE = "flowState";
    private final static String DENIED = "denied";
    private final static String ALLOWED = "allowed";
    private final static String AJAX_CONTENT_TYPE = "application/x-www-form-urlencoded";

    /**
     * Default properties subset key.
     */
    protected final static String DEFAULT_SUBSET = "strings";

    /**
     * Spring injected user service.
     */
    protected UserService userService;

    /**
     * Spring injected authorization service.
     */
    protected AuthorizationService authorizationService;

    /**
     * Create the MIV form action bean for the references to the user
     * service and given authorization service.
     *
     * @param userService Spring injected user service
     * @param authorizationService Spring injected authorization service
     */
    public MivFormAction(UserService userService, AuthorizationService authorizationService)
    {
        this.userService = userService;
        this.authorizationService = authorizationService;
    }

    /* (non-Javadoc)
     * @see org.springframework.webflow.action.FormAction#setupForm(org.springframework.webflow.execution.RequestContext)
     * This overridden method invokes the initFormInternal(RequestContext context) method which initializes the commonly
     * used variables.
     */
    /**<p>
     * In MIV, do not override Spring's <code>setupForm(context)</code> method.
     * Instead, implement <code>initFormAction(context)</code> to override
     * the do-nothing implementation in this superclass.</p>
     * <p>
     * This <code>setupForm(context)</code> in the MivFormAction superclass
     * will eventually be made <code>final</code> so it <em>can't</em> be
     * overridden, and <code>initFormAction(context)</code> will be made
     * <code>abstract</code> so it <em>must</em> be implemented.</p>
     */
    @Override
    public final Event setupForm(RequestContext context) throws Exception
    {
        Event e = super.setupForm(context);

        initFormAction(context);

        // Initialize the breadcrumbs
        if (!(context.getFlowScope().get(BREADCRUMBS) instanceof Stack))
        {
            this.clearBreadcrumbs(context);
        }

        return e;
    }


// TODO - Implement this at some point after the 5.0 release...
//    /**
//     * Override this method to do your form setup.
//     * <strong>Don't</strong> write your own "setupForm" or custom setup &mdash; e.g. "setupSearchForm()"
//     * This method should eventually be made abstract to force subclasses to implement it.
//     *
//     * @param context Webflow request context
//     * @return Webflow event status
//     */
//    public abstract Event initFormAction(final RequestContext context);

    /**
     * Override this method to do your form setup.
     * <strong>Don't</strong> write your own "setupForm" or custom setup &mdash; e.g. "setupSearchForm()"
     * This method should eventually be made abstract to force subclasses to implement it.
     *
     * @param context Webflow request context
     * @return Webflow event status
     */
    public /*abstract*/ Event initFormAction(final RequestContext context)
    {
        logger.debug("\n!! ------------ !! ------------> The class [" + this.className + "] has not overridden the method initFormAction()  !!\n");

        return success();
    }

    /*
     * (non-Javadoc)
     * @see org.springframework.webflow.action.FormAction#getFormObject(org.springframework.webflow.execution.RequestContext)
     * This overridden method catches the thrown exception of the super.
     */
    @Override
    public Object getFormObject(RequestContext context)
    {
        try
        {
            final Object formObject = super.getFormObject(context);
            return formObject;
        }
        catch (Exception exception)
        {
            MivSevereApplicationError err = null;
            if (exception instanceof MivSevereApplicationError) {
                err = (MivSevereApplicationError) exception;
            }
            else {
                err = new MivSevereApplicationError("errors.action.form", exception);
            }
            throw err;
        }
    }


    /*
     * (non-Javadoc)
     * @see org.springframework.webflow.action.FormAction#getFormErrors(org.springframework.webflow.execution.RequestContext)
     * This overridden method catches the thrown exception of the super.
     */
    @Override
    public Errors getFormErrors(RequestContext context)
    {
        try
        {
            return super.getFormErrors(context);
        }
        catch (Exception e)
        {
            throw new MivSevereApplicationError("errors.action.form", e);
        }
    }


    /**
     * Gets the current webflow view/action state ID for the current context.
     *
     * @param context Webflow request context
     * @return The ID of the current state in the webflow context
     */
    public String getStateId(final RequestContext context)
    {
        return context.getCurrentState().getId();
    }


    /**
     * Loads the {@link #DEFAULT_SUBSET} from the properties file into request scope using
     * the subset name as the key. A property with key 'flowId-viewStateId-strings-whatever'
     * in mivConfig.properties will be available in the view as 'strings.whatever'.
     *
     * @param context Webflow request context
     */
    public void loadProperties(final RequestContext context)
    {
        loadProperties(context, DEFAULT_SUBSET);
    }


    /**
     * Loads the given subsets from the properties file into request scope using the
     * subset name as the key. A property with key 'flowId-viewStateId-subset-whatever'
     * in mivConfig.properties will be available in the view as 'subset.whatever'.
     *
     * @param context Webflow request context
     * @param subsets property subsets to load into the request scope
     */
    protected void loadProperties(final RequestContext context, final String...subsets)
    {
        for (String subset : subsets)
        {
            loadProperties(context,
                           subset,
                           getProperties(getFlowId(context), getStateId(context), subset));
        }
    }


    /**
     * Loads the {@link #DEFAULT_SUBSET} from the properties keyed by the current flow ID
     * and given state ID into request scope using the subset name as the key. A property
     * with key 'flowId-viewStateId-strings-whatever' for the property set will be available
     * in the view as 'strings.whatever'.
     *
     * @param stateId Alternative flow state ID to use other than one from the webflow context
     * @param context Webflow request context
     */
    protected void loadProperties(String stateId, final RequestContext context)
    {
        loadProperties(context,
                       DEFAULT_SUBSET,
                       getProperties(getFlowId(context), stateId, DEFAULT_SUBSET));
    }


    /**
     * Loads the given properties into request scope mapped to the given
     * key. A property with key 'flowId-viewStateId-strings-whatever' in
     * the property set will be available in the view as 'strings.whatever'.
     *
     * @param context Webflow request context
     * @param key request scope map key
     * @param properties Set of properties to put in the request
     */
    private void loadProperties(final RequestContext context, String key, Properties properties)
    {
        context.getRequestScope().put(key, properties);
    }


    /**
     * Get a set of properties keyed by the given flow and state ID in the given subset.
     *
     * @param flowId Alternative flow ID to use other than one from the webflow context
     * @param stateId Alternative flow state ID to use other than one from the webflow context
     * @param subset properties subset key
     * @return Properties table for the current state
     */
    private Properties getProperties(String flowId, String stateId, String subset)
    {
        return PropertyManager.getPropertySet(flowId + "-" + stateId, subset);
    }


    /**
     * Add bread crumbs to the top of the stack.
     *
     * @param context Webflow request context
     * @param newBreadcrumbs bread crumbs to add the stack
     */
    protected void addBreadcrumbs(final RequestContext context, String...newBreadcrumbs)
    {
        Stack<String> breadcrumbs = getBreadcrumbs(context);

        // Add new bread crumbs
        for (String breadcrumb : newBreadcrumbs)
        {
            breadcrumbs.push(breadcrumb.toString());
        }
    }


    /**
     * Remove the given number bread crumbs from the top of stack.
     *
     * @param context Webflow request context
     * @param popNumber number of bread crumbs to remove from the top of the stack
     */
    public void removeBreadcrumbs(final RequestContext context, final int popNumber)
    {
        Stack<String> breadcrumbs = getBreadcrumbs(context);

        // Remove last 'popNumber' of elements
        for (int i=0; i < popNumber && !breadcrumbs.empty(); i++)
        {
            breadcrumbs.pop();
        }
    }


    /**
     * Pop a bread crumb from the top of the stack.
     *
     * @param context Webflow request context
     */
    public void removeBreadcrumb(RequestContext context)
    {
        removeBreadcrumbs(context, 1);
    }


    /**
     * Remove all bread crumbs from the stack.
     *
     * @param context Webflow request context
     */
    public void clearBreadcrumbs(final RequestContext context)
    {
        getBreadcrumbs(context).clear();
    }


    /**
     * Retrieve a stack bread crumbs from flow scope with the most recent bread
     * crumb on the top of the stack.
     *
     * @param context Web flow request context
     * @return Stack of bread crumbs for the flow scope
     */
    @SuppressWarnings("unchecked")
    public Stack<String> getBreadcrumbs(RequestContext context)
    {
        Stack<String> breadcrumbs = (Stack<String>) context.getFlowScope().get(BREADCRUMBS);

        //create new bread crumb stack if DNE
        if (breadcrumbs == null)
        {
            breadcrumbs = new Stack<String>();

            context.getFlowScope().put(BREADCRUMBS, breadcrumbs);
        }

        return breadcrumbs;
    }


    /**
     * Saves the current flow state in the flow scope
     *
     * @param context Webflow request context
     * @return Web flow event status
     */
    public Event saveState(final RequestContext context)
    {
        context.getFlowScope().put(SAVE_STATE, getStateId(context));

        return success();
    }


    /**
     * Saves the dossierId from the RequestParameters into the flowScope
     *
     * @param context Webflow request context
     * @return Web Flow event status
     */
    public Event saveDossierId(final RequestContext context)
    {
    	Long dossierId = context.getRequestParameters().getLong(DOSSIERID);
    	context.getFlowScope().put(DOSSIERID, dossierId);

    	return success();
    }


    /**
     * Short cut to obtain the MIV session in the session map of the external servlet context.
     *
     * @param context Webflow request context
     * @return The MyInfoVault session object
     */
    public MIVSession getMivSession(final RequestContext context)
    {
        // MIV-4073 ... If we decide we want to create the session if it's missing
        //              this next line ought to do it. Running with it on my machine
        //              for a while.  If you find this uncommented, ask Stephen if
        //              it was a mistake to commit it.
        MIVSession mivSession = MIVSession.getSession(getRequest(context), true);

        // This should never happen because we should have created a session if there wasn't one
        if (mivSession == null)
        {
            throw new MivSevereApplicationError("MIV was unable to find your session. You may have to logout and login again.");
        }

        return mivSession;
    }


    /**
     * Get the MIV <strong>Logged-in</strong> user from the session for this context.
     *
     * @param context Web flow request context
     * @return MIV user object
     */
    public MIVUser getUser(final RequestContext context)
    {
        return getMivSession(context).getUser();
    }


    /**
     * Get the shadow, switched-to person from the session context.
     *
     * @param context Webflow request context
     * @return MIV target person
     */
    public MivPerson getShadowPerson(final RequestContext context)
    {
        return getUser(context).getTargetUserInfo().getPerson();
    }


    /**
     * Get the real, logged-in person from the session context.
     *
     * @param context Webflow request context
     * @return MIV real person
     */
    public MivPerson getRealPerson(final RequestContext context)
    {
        return getUser(context).getUserInfo().getPerson();
    }


    /**
     * Get the {@link MIVConfig} constants map.
     *
     * @return MIV application "constants" loaded from the database
     * @see MIVConfig#getConstants()
     */
    public Map<String, List<Map<String, String>>> getConstants()
    {
        return MIVConfig.getConfig().getConstants();
    }


    /**
     * Short cut to obtain the external servlet context.
     *
     * @param context Webflow request context
     * @return The external servlet context
     */
    protected ServletExternalContext getServletContext(final RequestContext context)
    {
        return (ServletExternalContext) context.getExternalContext();
    }


    /**
     * Short cut to obtain the request in the current context.
     *
     * @param context Webflow request context
     * @return the HTTP servlet request for the current context
     */
    protected HttpServletRequest getRequest(RequestContext context)
    {
        return (HttpServletRequest) getServletContext(context).getNativeRequest();
    }


    /**
     * Returns the ID of the inner most web flow active in the current context.
     *
     * @param context Webflow request context
     * @return ID of the current web flow
     */
    public String getFlowId(final RequestContext context)
    {
        return context.getActiveFlow().getId();
    }


    /**
     * Returns the top level or root web flow in the current flow execution context.
     *
     * @param context Webflow request context
     * @return ID of the root web flow
     */
    public String getRootFlowId(final RequestContext context)
    {
        return context.getFlowExecutionContext().getDefinition().getId();
    }


    /**
     * Get the MIV standard "permission denied" Event object.
     *
     * @return a "denied" result event.
     */
    public Event denied()
    {
        return getEvent(DENIED);
    }


    /**
     * Log reason denied and return denied event.
     *
     * @param logPattern log message pattern
     * @param parameters objects injected into the log pattern
     * @return Spring WebFlow denied event
     */
    public Event denied(String logPattern, Object...parameters)
    {
        logger.warn(MessageFormat.format("DENIED: " + logPattern, parameters));

        return denied();
    }


    /**
     * Get the MIV standard "permission allowed" Event object.
     *
     * @return an "allowed" result event.
     */
    public Event allowed()
    {
        return getEvent(ALLOWED);
    }


    /**
     * Get the MIV standard Event object from the factory for a
     * given string. This is similar to the Spring standard
     * <code>success()</code> and <code>error()</code> methods.
     *
     * @param name the name of the event to create and return
     * @return a result event for the given string.
     * @see org.springframework.webflow.action.AbstractAction#success()
     * @see org.springframework.webflow.action.AbstractAction#error()
     */
    protected Event getEvent(final String name)
    {
        return getEventFactorySupport().event(this, name);
    }


    /**
     * Short cut to obtain the map of URL parameters in the current context.
     *
     * @param context Webflow request context
     * @return URL parameter map
     */
    public ParameterMap getParameters(RequestContext context)
    {
        return getServletContext(context).getRequestParameterMap();
    }


    /**
     * Gets the value for the given parameter from the URL parameter
     * map. The value converted to the type of the given default value
     * or the default value is returned.
     *
     * @param context Webflow request context
     * @param parameter The key of the value to get from the parameter map
     * @param defaultValue Returned if conversion fails or doesn't exist
     * @return A value converted to the type of the given default value
     */
    @SuppressWarnings("unchecked")
    protected <T> T getParameterValue(RequestContext context, final String parameter, final T defaultValue)
    {
        try
        {
            return getParameters(context).get(parameter,
                                              (Class<T>) defaultValue.getClass(),
                                              defaultValue);
        }
        catch(ConversionException e)
        {
            logger.error(MessageFormat.format(PARAMETER_CONVERSION_FAILIURE,
                                              parameter,
                                              defaultValue.getClass(),
                                              defaultValue), e);
        }

        return defaultValue;
    }


    /**
     * Gets the value for the given parameter from the URL parameter
     * map. The value converted to the {@link Enum} type of the given default value
     * or the default Enum value is returned.
     *
     * @param context Webflow request context
     * @param parameter The key of the value to get from the parameter map
     * @param defaultValue Returned if conversion fails or doesn't exist
     * @return A value converted to the Enum type of the given default Enum value
     */
    @SuppressWarnings("unchecked")
    protected <T extends Enum<T>> T getParameterValue(RequestContext context, final String parameter, final T defaultValue)
    {
        try
        {
            String value = getParameters(context).get(parameter);

            if (value != null)
            {
                 return (T) Enum.valueOf(defaultValue.getClass(), value);
            }
        }
        catch(IllegalArgumentException e)
        {
            logger.warn(MessageFormat.format(PARAMETER_CONVERSION_FAILIURE,
                                             parameter,
                                             defaultValue.getClass(),
                                             defaultValue), e);
        }

        return defaultValue;
    }


    /**
     * Gets the String value for the given parameter from the URL parameter map.
     *
     * @param context Webflow request context
     * @param parameter The key of the value to get from the parameter map
     * @return String converted value for the given parameter or empty string if conversion fails
     */
    protected String getParameterValue(RequestContext context, final String parameter)
    {
        return getParameterValue(context, parameter, StringUtil.EMPTY_STRING);
    }


    /**
     * Does the request for the current context have an AJAX content type?
     *
     * @param context Webflow request context
     * @return <code>true</code> if the request is an AJAX one, <code>false</code> otherwise
     */
    protected boolean isAJAXRequest(RequestContext context)
    {
        return AJAX_CONTENT_TYPE.equalsIgnoreCase(getRequest(context).getContentType());
    }


    /**
     * Get the dossier from conversation scope via the "dossier" attribute.
     *
     * @param context Web flow request context
     * @return dossier or <code>null</code> if DNE
     */
    protected Dossier getDossier(RequestContext context)
    {
        return (Dossier) context.getConversationScope().get(DOSSIER);
    }


    /* Standard methods overridden only to allow logging and breakpoints to be set -- start */
    /*
    @Override
    protected void initAction()
    {
        logger.debug("   --------------------->   [initAction (no-args)] called in [" + this.className + "]");
        super.initAction();
    }

    @Override
    public Event bind(RequestContext context) throws Exception
    {
        logger.debug("[bind] called in [" + this.className + "]");
        Event e = super.bind(context);
        return e;
    }

    @Override
    public Event bindAndValidate(RequestContext context) throws Exception
    {
        logger.debug("[bindAndValidate] called in [" + this.className + "]");
        Event e = super.bindAndValidate(context);
        return e;
    }

    @Override
    protected void doBind(RequestContext context, DataBinder binder) throws Exception
    {
        logger.debug("[doBind] called in [" + this.className + "]");
        super.doBind(context, binder);
    }

    @Override
    protected void doValidate(RequestContext context, Object formObject, Errors errors) throws Exception
    {
        logger.debug("[doValidate] called in [" + this.className + "]");
        super.doValidate(context, formObject, errors);
    }

    @Override
    public org.springframework.validation.Validator getValidator()
    {
        logger.debug("[getValidator] called in [" + this.className + "]");
        org.springframework.validation.Validator v = super.getValidator();
        return v;
    }

    @Override
    protected Event doPreExecute(RequestContext context) throws Exception
    {
        logger.info("[doPreExecute] called in [" + this.className + "]");
        Event e = super.doPreExecute(context);
        return e;
    }

    @Override
    protected void doPostExecute(RequestContext context) throws Exception
    {
        logger.info("[doPostExecute] called in [" + this.className + "]");
        super.doPostExecute(context);
    }
    */
    /* Standard methods overridden only to allow logging and breakpoints to be set -- end */
}
