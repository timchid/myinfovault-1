/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: EditValidator.java
 */

package edu.ucdavis.mw.myinfovault.web.spring.manageUsers;

import java.util.List;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import edu.ucdavis.mw.myinfovault.service.person.MivRole;
import edu.ucdavis.mw.myinfovault.service.person.Scope;

/**
 * TODO: rewrite this completely for the 4.0 replacement form
 * Validates the manage users add form
 * @author Craig Gilmore
 * @author Stephen Paulsen
 * @since MIV 3.0
 */
public class EditValidator implements Validator
{
    @SuppressWarnings("rawtypes")
    @Override
    public boolean supports(Class clazz)
    {
        return EditUserForm.class.isAssignableFrom(clazz);
    }


    @Override
    public void validate(Object obj, Errors errors)
    {
        EditUserForm editForm = (EditUserForm) obj;

        // Check that there is an miv role selected
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "selectedRole", "MIV Role is required");

        boolean isFaculty = editForm.getSelectedRole().getCategory() == MivRole.Category.FACULTY;

        // This was ridiculous; non-numeric code:code couldn't have been passed.
//        // valid department identifier format
//        String[] deptCode = {};
//        boolean isPrimaryDepartment = true;
//        for (String department : addForm.getDepartments())
//        {
//            deptCode = department.split(":");// split school and dept code into array
//
//            // more than 2 ids or invalid integers
//            if (deptCode.length != 2 || !TempUtility.isInteger(deptCode[0]) || !TempUtility.isInteger(deptCode[1]))
//            {
//                if (isPrimaryDepartment)
//                {
//                    errors.reject(null, "Primary School/College Department is required"); // blank or invalid
//                }
//                else if (!department.equals(""))
//                {
//                    errors.reject(null, "Joint School/College Department contains an invalid value"); // invalid
//                }
//            }
//
//            isPrimaryDepartment = false;
//        }

        // Check that there is at least one assignment made with a scope that is not Scope.None
        AssignmentLine top = editForm.getHomeDepartment();
        if (top == null || top.getScope().equals(Scope.None))
        {
            List<AssignmentLine> lines = editForm.getFormLines();
            int assignmentCount = 0;
            int candidateCount = 0;

            for (AssignmentLine al : lines)
            {
                if (isFaculty && al.getCandidate()) {
                    candidateCount++;
                }
                if (! al.getScope().equals(Scope.None)) {
                    assignmentCount++;
                }
            }
            if (assignmentCount == 0) {
                errors.reject(null, "At least one School/College - Department must be assigned");
            }
            else if (isFaculty && candidateCount == 0) {
                errors.reject(null, "At least one \"Candidate\" checkbox must be selected ");
            }
        }

        // email address
        if (!editForm.getAvailableEmails().contains(editForm.getSelectedEmail()))
        {
            errors.reject(null, "Invalid email selection");
        }

   //     int emailSelected = editForm.getSelectedEmail(); // ??
        // Email is an 'int' selection, not a String, and can't be unset because it's a radio button choice.
        // Don't need to validate it.
//        // email address
//        String email = addForm.getEmailAddress();
//        if (StringUtils.isBlank(email) || !(TempUtility.isInteger(email) && Integer.parseInt(email) >= 0))
//        {
//            errors.reject(null, "Email Address is required");
//        }
    }
}
