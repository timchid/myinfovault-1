/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: MivPropertyEditors.java
 */

package edu.ucdavis.mw.myinfovault.web.spring.commons;

import java.beans.PropertyEditor;
import java.beans.PropertyEditorSupport;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.Date;

import org.apache.commons.lang.StringUtils;

import edu.ucdavis.mw.myinfovault.domain.action.DossierActionType;
import edu.ucdavis.mw.myinfovault.domain.action.DossierDelegationAuthority;
import edu.ucdavis.mw.myinfovault.domain.action.Step;
import edu.ucdavis.mw.myinfovault.service.person.MivRole;
import edu.ucdavis.mw.myinfovault.service.person.Scope;
import edu.ucdavis.mw.myinfovault.util.StringUtil;
import edu.ucdavis.myinfovault.data.DateParser;

/**
 * Defines common {@link PropertyEditor} classes for use with Spring data binding.
 *
 * @author Craig Gilmore
 * @since MIV 4.6.2
 */
public class MivPropertyEditors
{
    /**
     * Handles conversion between {@link BigDecimal} and {@link String}.
     *
     * @author Craig Gilmore
     * @since MIV 4.6.2
     */
    public static class BigDecimalPropertyEditor extends PropertyEditorSupport
    {
        /*
         * (non-Javadoc)
         * @see java.beans.PropertyEditorSupport#setAsText(java.lang.String)
         */
        @Override
        public void setAsText(String value) throws IllegalArgumentException
        {
            try
            {
                setValue(StringUtils.isNotBlank(value)
                       ? StringUtil.parseBigDecimal(value)
                       : BigDecimal.ZERO);//consider empty/null as zero
            }
            catch (ParseException e)
            {
                throw new IllegalArgumentException("Given value is not a proper number", e);
            }
        }
    }

    /**
     * Handles conversion of {@link String} to {@link Step}.
     *
     * @author Craig Gilmore
     * @since MIV 4.8.4.1
     */
    public static class StepPropertyEditor extends PropertyEditorSupport
    {
        /*
         * (non-Javadoc)
         * @see java.beans.PropertyEditorSupport#setAsText(java.lang.String)
         */
        @Override
        public void setAsText(String value) throws IllegalArgumentException
        {
            setValue(new Step(value));
        }
    }

    /**
     * Handles conversion between {@link Date} and {@link String}.
     *
     * @author Pradeep K Haldiya
     * @since MIV 4.7.1.1
     */
    public static class DatePropertyEditor extends PropertyEditorSupport
    {
        /*
         * (non-Javadoc)
         * @see java.beans.PropertyEditorSupport#setAsText(java.lang.String)
         */
        @Override
        public void setAsText(String value) throws IllegalArgumentException
        {
            if (StringUtils.isNotBlank(value))
            {
                Date date = DateParser.parse(value, /*strict=*/true);
                if (date == null) {
                    throw new IllegalArgumentException("\"" + value + "\" is not a valid date");
                }
                setValue(date);
            }
        }
    }

    /**
     * Handles conversion between {@link Boolean} and {@link String}.
     *
     * @author Rick Hendricks
     * @since MIV 4.8.5
     */
    public static class BooleanPropertyEditor extends PropertyEditorSupport {

        private Boolean value = false;

        /**
         * @param value - the default value to use when none supplied
         */
        public BooleanPropertyEditor(Boolean value)
        {
            this.value = value;
        }

        @Override
        public String getAsText() {
           return getValue().toString();
        }

        @Override
        public void setAsText(String text) throws IllegalArgumentException {
            setValue(StringUtils.isEmpty(text) ? value : new Boolean(text));
        }
    }

    /**
     * Handles conversion between {@link Enum} and {@link String}.
     *
     * @author Rick Hendricks
     * @since MIV 4.8.5
     */
    public static class EnumPropertyEditor<T extends Enum<T>> extends PropertyEditorSupport {
        private Class<T> clazz;

        public EnumPropertyEditor(Class<T> clazz) {
            this.clazz = clazz;
        }

        @Override
        public String getAsText() {
            return (getValue() == null ? "" : ((Enum<?>) getValue()).name());
        }

        @Override
        public void setAsText(String value) throws IllegalArgumentException {
            setValue(Enum.valueOf(clazz, value));
        }
    }

    /**
     * Handles conversion between {@link DossierDelegationAuthority} and {@link String}.
     *
     * @author Stephen Paulsen
     * @since MIV 4.0
     */
    public static class DelegationAuthorityPropertyEditor extends PropertyEditorSupport
    {
        /*
         * (non-Javadoc)
         * @see java.beans.PropertyEditorSupport#setAsText(java.lang.String)
         */
        @Override
        public void setAsText(String value) throws IllegalArgumentException
        {
            setValue(DossierDelegationAuthority.valueOf(value).getDescription());
        }
    }

    /**
     * Handles conversion between {@link DossierActionType} and {@link String}.
     *
     * @author Stephen Paulsen
     * @since MIV 4.0
     */
    public static class ActionTypePropertyEditor extends PropertyEditorSupport
    {
        /*
         * (non-Javadoc)
         * @see java.beans.PropertyEditorSupport#setAsText(java.lang.String)
         */
        @Override
        public void setAsText(String value) throws IllegalArgumentException
        {
            setValue(DossierActionType.valueOf(value).getDescription());
        }
    }

    /**
     * Handles conversion between {@link MivRole} and {@link String}.
     *
     * @author Stephen Paulsen
     * @since MIV 4.0
     */
    public static class RolePropertyEditor extends PropertyEditorSupport
    {
        /*
         * (non-Javadoc)
         * @see java.beans.PropertyEditorSupport#setAsText(java.lang.String)
         */
        @Override
        public void setAsText(String value) throws IllegalArgumentException
        {
            setValue(MivRole.valueOf(value));
        }
    }

    /**
     * Handles conversion between {@link Scope} and {@link String}.
     *
     * @author Stephen Paulsen
     * @since MIV 4.0
     */
    public static class ScopePropertyEditor extends PropertyEditorSupport
    {
        /*
         * (non-Javadoc)
         * @see java.beans.PropertyEditorSupport#setAsText(java.lang.String)
         */
        @Override
        public void setAsText(String value) throws IllegalArgumentException, NumberFormatException
        {
            setValue(StringUtils.isNotBlank(value)
                   ? new Scope(value)
                   : Scope.NoScope);
        }
    }

    /**
     * Handles conversion between String and Integer. Any non-parsable integer is bound as zero.
     *
     * @author Craig Gilmore
     * @since MIV 4.8.3
     */
    public static class IntegerPropertyEditor extends PropertyEditorSupport
    {
        /*
         * (non-Javadoc)
         * @see java.beans.PropertyEditorSupport#setAsText(java.lang.String)
         */
        @Override
        public void setAsText(String value)
        {
            try
            {
                setValue(Integer.parseInt(value));
            }
            catch (NumberFormatException e)
            {
                setValue(0);
            }
        }
    }
}
