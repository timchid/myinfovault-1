package edu.ucdavis.mw.myinfovault.web.spring.biosketch;

/**
 * Command Object representing a BiosketchCriteria
 * @author dreddy
 * @since MIV 2.1
 */
public class CriteriaCommand extends BaseCommand
{
    private String startYear;
    private String endYear;
    //private String presentYear="1";
    private String field = "Year";

    /**
     * @return
     */
    public String getField()
    {
        return field;
    }

    /**
     * @param field
     */
    public void setField(String field)
    {
        this.field = field;
    }

    /**
     * @return
     */
    public String getStartYear()
    {
        return startYear;
    }

    /**
     * @param startYear
     */
    public void setStartYear(String startYear)
    {
        this.startYear = startYear;
    }

    /**
     * @return
     */
    public String getEndYear()
    {
        return endYear;
    }

    /**
     * @param endYear
     */
    public void setEndYear(String endYear)
    {
        this.endYear = endYear;
    }

    /**
     * @return
     */
   /* public String getPresentYear()
    {
        return presentYear;
    }

    /**
     * @param presentYear
     *//*
    public void setPresentYear(String presentYear)
    {
        this.presentYear = presentYear;
    }*/
}
