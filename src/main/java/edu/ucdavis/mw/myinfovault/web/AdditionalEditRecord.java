/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: AdditionalEditRecord.java
 */

package edu.ucdavis.mw.myinfovault.web;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import edu.ucdavis.mw.myinfovault.util.HtmlUtil;
import edu.ucdavis.mw.myinfovault.valuebeans.ResultVO;
import edu.ucdavis.myinfovault.MIVConfig;
import edu.ucdavis.myinfovault.MIVUserInfo;
import edu.ucdavis.myinfovault.PropertyManager;
import edu.ucdavis.myinfovault.data.MIVUtil;
import edu.ucdavis.myinfovault.data.QueryTool;
import edu.ucdavis.myinfovault.data.RecordHandler;
import edu.ucdavis.myinfovault.exception.MivSevereApplicationError;
import edu.ucdavis.myinfovault.format.FormatManager;
import edu.ucdavis.myinfovault.htmlcleaner.HtmlCleaner;
import edu.ucdavis.myinfovault.htmlcleaner.InvalidMarkupException;
import edu.ucdavis.myinfovault.http.MivRequestAdapter;

/**
 * FIXME: Needs Javadoc
 * @author venkatv
 * @since MIV v2.0
 */
public class AdditionalEditRecord extends MIVServlet
{
    private static final long serialVersionUID = 200710031107L;
    private HtmlCleaner htmlCleaner = new HtmlCleaner("edu.ucdavis.myinfovault.format.wysiwyg");


    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException
    {
        MIVUserInfo mui = mivSession.get().getUser().getTargetUserInfo();

        // We escape any HTML markup which may be present in the input parameters
        // to guard against cross-site scripting (XSS) attacks

        String recordType = req.getParameter("rectype");
        recordType = MIVUtil.sanitize(recordType);

        String sectionName = req.getParameter("sectionname");
        sectionName = MIVUtil.sanitize(sectionName);

        String subType = req.getParameter("subtype");
        subType = MIVUtil.sanitize(subType);

        String documentID = req.getParameter("documentid");
        // Make sure the documentID is a valid integer...validation is in place of sanitizing here
        try {
            if (documentID != null) {
                Integer.parseInt(documentID);
            }
        }
        catch (NumberFormatException nfe) {
//            showError("Document ID \"" + MIVUtil.sanitize(documentID) + "\" is not a valid number in AdditionalInformation", req, res);
            throw new MivSevereApplicationError("errors.additionalinformation.documentid", nfe, MIVUtil.sanitize(documentID));
//            return;
        }

        String target = "EditRecord";
        Properties p = null;

        // this request adapter is created for adding new parameters and deleting unwanted parameters
        // from the request object and we pass this request object to editrecord.
        MivRequestAdapter newreq = new MivRequestAdapter(req, req.getParameterMap());
        RecordAction actionInfo = getRecordAction(req, recordType);


/* --- Deleting a Header (and all records under it) --- */
        if (actionInfo.action == Action.DELETING_HEADER) // changed to this; next round, use a case statement.
        {
            deleteSection(actionInfo, mui, req, res);
        }
/* --- Deleting one Additional Info record --- */
        else if (actionInfo.action == Action.DELETING_RECORD) // changed to this; next round, use a case statement.
        {
            deleteRecord(actionInfo, mui, req, res);
        }
/* --- Not deleting :: either Editing or Adding --- */
        else
        {
            Map<String,String> record = null;
            Map<String,String> headerRecord = null;

            p = PropertyManager.getPropertySet(actionInfo.recType, "config");

            // this editorform contains the name of the jsp to which we need to send the response.
            String editorForm = p.getProperty("editorpage");
            req.setAttribute("editorpage", editorForm);

            // addHeaderInd==true indicates that we are adding a new Heading for AdditionalInformation.
// ADD Header
            if (actionInfo.action == Action.ADDING_HEADER) // changed to this; next round, use a case statement.
            {
                headerRecord = addSection(mui, actionInfo);
            }
            // addHeaderInd==false means we are editing record, or editing record or editing header.
// EDIT something
            else if (actionInfo.action == Action.EDITING_HEADER || actionInfo.action == Action.EDITING_RECORD)
            {
                RecordHandler rh = mui.getRecordHandler();
                String tableName = p.getProperty("tablename");
                record = rh.get(tableName, Integer.parseInt(actionInfo.recordID));
// EDIT record
                if (actionInfo.action == Action.EDITING_RECORD)
                {
                    String headerID = "";
                    String content = "";
                    String recID = "";
                    String updateTimestamp = "";
                    if (record != null && record.size() > 0)
                    {
                        // EditRecord is now escaping the content; don't do it here anymore. SDP 2007-12-21
                        content = record.get("content");
                        //content = MIVUtil.escapeWysiwygEdit(record.get("content"));
                        recID = record.get("id");
                        headerID = record.get("headerid");
                        updateTimestamp = record.get("updatetimestamp");
                    }
                    QueryTool qt = MIVConfig.getConfig().getQueryTool();
                    final String query = "SELECT ID AS HeaderID, Value AS Heading FROM AdditionalHeader WHERE ID = ? AND UserID = ?";
                    String[] queryArgs = { headerID, Integer.toString(mui.getPerson().getUserId()) };

                    List<Map<String, String>> records = qt.getList(query, queryArgs);

                    if (records != null && records.size() > 0)
                    {
                        headerRecord = records.get(0);
                    }
                    if (headerRecord != null)
                    {
                        FormatManager fm = mui.getSectionFetcher().getFormatManager();
                        fm.applyFormats("additionalheader", headerRecord);
                        headerRecord.put("id", recID);
                        headerRecord.put("content", content);
                        headerRecord.put("updatetimestamp", updateTimestamp);
                    }
                }
// EDIT Header
                else if (actionInfo.action == Action.EDITING_HEADER)
                {
                    headerRecord = record;
                }
            }
// ADD Record
            else if (actionInfo.action == Action.ADDING_RECORD) // changed to this; next round, use a case statement.
            {
                headerRecord = addRecord(mui, actionInfo, headerRecord);
            }

            // If we are editing or adding an AdditionalInformation record
            //    headerRecord contains headerid, heading, content and additionalInformation id.
            // If we are adding or editing a Heading headerRecord contains only headerid and heading.
            req.setAttribute("record", headerRecord);
            newreq.addParam("documentid", documentID);
            newreq.addParam("sectionname", sectionName);
            newreq.addParam("subtype", subType);
            newreq.addParam("rectype", actionInfo.recType);

            dispatch(target, newreq, res);
        }
    }


    /**
     * @param mui
     * @param actionInfo
     * @return
     */
    private Map<String, String> addSection(MIVUserInfo mui, RecordAction actionInfo)
    {
        Map<String, String> record;
        Map<String, String> headerRecord;
        RecordHandler rh = mui.getRecordHandler();
        // FIXME: WTF? if we're _ADDING_ a header we won't ever _get_ a header here!
        record = rh.get("AdditionalHeader", Integer.parseInt(actionInfo.recordID));
        headerRecord = record;

        FormatManager fm = mui.getSectionFetcher().getFormatManager();
        fm.applyFormats("additionalheader", headerRecord);
        String headerId = headerRecord.get("id");
        String heading = headerRecord.get("value");

        headerRecord.remove("id");
        headerRecord.remove("value");
        headerRecord.put("headerid", headerId);
        headerRecord.put("heading", heading);

        return headerRecord;
    }


    /**
     * @param mui
     * @param actionInfo
     * @param headerRecord
     * @return
     */
    private Map<String, String> addRecord(MIVUserInfo mui, RecordAction actionInfo, Map<String, String> headerRecord)
    {
        String headerID = actionInfo.recordID;
        String recID = "";
        String content = "";
        QueryTool qt = MIVConfig.getConfig().getQueryTool();

        final String query =
            "SELECT ID AS HeaderID, Value AS Heading" +
            " FROM AdditionalHeader" +
            " WHERE ID=? AND UserID=?";
        String[] params = { headerID+"", mui.getPerson().getUserId()+"" };

        //log.fine("AddtlInfo - addRecord: headerID is " + headerID);
        logger.debug("AddtlInfo - addRecord: headerID is {}", headerID);

        List<Map<String, String>> records = qt.getList(query, params);

        if (records != null)
        {
            logger.debug("AddtlInfo - addRecord: size of record list found: {}", records.size());
            if (records.size() > 0)
            {
                headerRecord = records.get(0);
                logger.debug("AddtlInfo - addRecord: record is [{}]",headerRecord);
            }
        }
        else
        {
            logger.warn("AddtlInfo - addRecord: no header records found for header ID={} when trying to add a record", headerID);
        }

        FormatManager fm = mui.getSectionFetcher().getFormatManager();
        fm.applyFormats("additionalheader", headerRecord);
        String heading = headerRecord.get("heading");
        headerRecord.put("heading", heading);

        headerRecord.put("id", recID);
        headerRecord.put("content", content);

        return headerRecord;
    }


    /**
     * @param actionInfo
     * @param mui
     * @param res
     * @param subType
     */
    private void deleteRecord(RecordAction actionInfo, MIVUserInfo mui, HttpServletRequest req,HttpServletResponse res)
    {
        String key = actionInfo.recordID;
        RecordHandler rh = mui.getRecordHandler();
        int deleteCount = rh.delete("AdditionalInformation", Integer.parseInt(key));

        String subType = req.getParameter("subtype");
        subType = MIVUtil.sanitize(subType);

        String addRecType = actionInfo.recType + "-" + subType;
        Properties pstr = PropertyManager.getPropertySet(addRecType, "strings");
        String where = (String)pstr.get("pagehead");

        try
        {
            Map<String, String[]> reqData = getRequestMap(req);

            String deleteMessage = "Record" + (deleteCount == 1 ? "" : "s") + " deleted from " + where;

            if (isAjaxRequest())
            {
                PrintWriter pw = res.getWriter();
                res.setContentType("text/html");
                /*out.print("<div class=\"completionmessage\">" +
                deleteCount + " record" + (deleteCount == 1 ? "" : "s") +
                " deleted from Additional Information</div>");*/

                pw.print(deleteMessage);
            }
            else
            {
                String recordType = (reqData.get("rectype") != null ? reqData.get("rectype")[0] : null);
                String documentID = (reqData.get("documentid") != null ? reqData.get("documentid")[0] : null);

                if (documentID == null || documentID.trim().length() == 0)
                {
                    documentID = (reqData.get("DocumentID") != null ? reqData.get("DocumentID")[0] : null);
                }

                documentID = MIVUtil.sanitize(documentID);

                req.setAttribute("recordTypeName", recordType);

                // Additional Information Related and this documentid is used when we add a new Heading.
                // These are passed straight through to the JSPs
                req.setAttribute("documentid", documentID);
                String sectionName = MIVUtil.sanitize(req.getParameter("sectionname"));

                subType = subType != null ? subType : "";
                documentID = documentID != null ? documentID : "";
                sectionName =  sectionName !=null ? sectionName : "";


                logger.debug("redirecting to list page");
                if (recordType.equals("additionalheader"))
                {
                    recordType = "additional";
                }

                StringBuilder redirectPath = new StringBuilder(req.getContextPath());
                redirectPath.append("/ItemList?type=").append(recordType);

                if (subType != null && !("".equals(subType)))
                {
                    redirectPath.append("&subtype=").append(subType);
                }

                if (sectionName != null && !("".equals(sectionName)))
                {
                    redirectPath.append("&sectionname=").append(sectionName);
                }

                redirectPath.append("&documentid=").append(documentID);

                redirectPath.append("&success=").append(true);
                redirectPath.append("&CompletedMessage=").append(deleteMessage);

                logger.debug("redirectPath :: {}", redirectPath.toString());

                res.setContentType("text/html");
                res.sendRedirect(redirectPath.toString());

                return;
            }

        }
        catch (IOException ioe)
        {
            logger.warn("Tried to write output to xmlhttprequest", ioe);
        }
    }


    private static final String documentQuery = "SELECT ID FROM AdditionalInformation WHERE HeaderID=?";
    /**
     * @param actionInfo
     * @param res
     * @param subType TODO
     * @param rh
     * @throws NumberFormatException
     */
    private void deleteSection(RecordAction actionInfo, MIVUserInfo mui, HttpServletRequest req, HttpServletResponse res) throws NumberFormatException
    {
        String responseMsg = "Header and corresponding records have been deleted from ";
        String key = actionInfo.recordID;
        int deleteCount = 0;

        QueryTool qt = MIVConfig.getConfig().getQueryTool();
//        final String documentQuery = "SELECT ID FROM AdditionalInformation WHERE HeaderID=?";
        String[] params = { key };

        List<Map<String, String>> infoRecords = qt.getList(documentQuery, params);

        RecordHandler rh = mui.getRecordHandler();
        for (Map<String, String> infoRecord : infoRecords)
        {
            String id = infoRecord.get("id");
            deleteCount += rh.delete("AdditionalInformation", Integer.parseInt(id));
        }

        deleteCount += rh.delete("AdditionalHeader", Integer.parseInt(key));

        //ServletOutputStream out = null;

        String subType = req.getParameter("subtype");
        subType = MIVUtil.sanitize(subType);

        try
        {
            String addRecType = actionInfo.recType + "-" + subType;
            Properties pstr = PropertyManager.getPropertySet(addRecType, "strings");
            String where = (String)pstr.get("pagehead");

            if (deleteCount == 0)
            {
                responseMsg = "Header and corresponding records could not be deleted from ";
            }
            responseMsg += where;

            Map<String, String[]> reqData = getRequestMap(req);

            if (isAjaxRequest())
            {
                PrintWriter pw = res.getWriter();
                res.setContentType("text/html");
                pw.print(responseMsg);
            }
            else
            {
                String recordType = (reqData.get("rectype") != null ? reqData.get("rectype")[0] : null);
                String documentID = (reqData.get("documentid") != null ? reqData.get("documentid")[0] : null);

                if( documentID == null || documentID.trim().length()==0)
                {
                    documentID = (reqData.get("DocumentID") != null ? reqData.get("DocumentID")[0] : null);
                }

                documentID = MIVUtil.sanitize(documentID);

                req.setAttribute("recordTypeName", recordType);

                // Additional Information Related and this documentid is used when we add a new Heading.
                // These are passed straight through to the JSPs
                req.setAttribute("documentid", documentID);
                String sectionName = MIVUtil.sanitize(req.getParameter("sectionname"));

                subType = subType != null ? subType : "";
                documentID = documentID != null ? documentID : "";
                sectionName =  sectionName !=null ? sectionName : "";

                logger.debug("redirecting to list page");
                if (recordType.equals("additionalheader"))
                {
                    recordType = "additional";
                }

                StringBuilder redirectPath = new StringBuilder(req.getContextPath());
                redirectPath.append("/ItemList?type=").append(recordType);

                if (subType != null && !("".equals(subType)))
                {
                    redirectPath.append("&subtype=").append(subType);
                }

                if (sectionName != null && !("".equals(sectionName)))
                {
                    redirectPath.append("&sectionname=").append(sectionName);
                }

                redirectPath.append("&documentid=").append(documentID);

//                if (deleteCount == 0) {
//                    redirectPath.append("&success=").append(false);
//                }
//                else {
//                    redirectPath.append("&success=").append(true);
//                }
                redirectPath.append("&success=").append(deleteCount > 0);

                redirectPath.append("&CompletedMessage=").append(responseMsg);

                logger.debug("redirectPath :: {}", redirectPath.toString());

                res.setContentType("text/html");
                res.sendRedirect(redirectPath.toString());

                return;
            }
        }
        catch (IOException ioe)
        {
            logger.warn("Tried to write output to xmlhttprequest", ioe);
        }
    }


    /**
     * Gets the data from the page, creates new request parameters that matches
     * with database fields' name and remove the old unwanted request
     * parameters. and dispatches the request to EditRecord's doPost() method.
     *
     * @param req
     * @param res
     */
    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException
    {
        Map<String, String[]> reqData = getRequestMap(req, false);

        MivRequestAdapter newreq = new MivRequestAdapter(req, reqData);


        //String documenID = req.getParameter("documentid");
        String documenID = (reqData.get("documentid") != null ? reqData.get("documentid")[0] : null);//reqData.get("documentid");
        String recordType = (reqData.get("rectype") != null ? reqData.get("rectype")[0] : null);

        if (recordType.equals("additionalheader"))
        {
            newreq.addParam("rectype", recordType);
            newreq.addParam("recid", (reqData.get("headerid") != null ? reqData.get("headerid")[0] : null)); //(String) reqData.get("headerid"));
            newreq.addParam("value", (reqData.get("value") != null ? reqData.get("value")[0] : null)); //(String) reqData.get("value"));
            newreq.addParam("display", "1");
            newreq.addParam("DocumentID", documenID);
            newreq.addParam("subtype", (reqData.get("subtype") != null ? reqData.get("subtype")[0] : null));//(String) reqData.get("subtype"));
            newreq.removeParam("header");
            newreq.removeParam("headerid");
            newreq.removeParam("documentid");
        }
        else if (recordType.equals("additional"))// if Additional Info Record
        {
            String content = (reqData.get("content") != null ? reqData.get("content")[0] : null);//reqData.get("content");

            String sectionname = (reqData.get("sectionname") != null ? reqData.get("sectionname")[0] : null);
            sectionname = MIVUtil.sanitize(sectionname);

            try
            {
                content = htmlCleaner.removeTags(content);
            }
            catch (InvalidMarkupException e)
            {
                logger.info("InvalidMarkupException while handling additional information content [{}]", content);
                logger.info(e.getMessage());
            }

            newreq.addParam("content", content);
            newreq.addParam("rectype", recordType);
            newreq.addParam("recid", (reqData.get("recordid") != null ? reqData.get("recordid")[0] : null));//(String) reqData.get("recordid"));
            newreq.addParam("display", "1");
            newreq.addParam("sectionname", sectionname);//(String) reqData.get("sectionname"));
            newreq.addParam("recid", (reqData.get("recordid") != null ? reqData.get("recordid")[0] : null));//(String) reqData.get("recordid"));
            newreq.addParam("heading", (reqData.get("heading") != null ? reqData.get("heading")[0] : null));
            newreq.removeParam("recordid");
        }

        String target = "EditRecord";
        dispatch(target, newreq, res);
    }


    public RecordAction getRecordAction(HttpServletRequest req, String recordType)
    {
        @SuppressWarnings({"unchecked"}) // http parameter maps are always "key=value" with String keys.
        Set<String> params = (req.getParameterMap()).keySet();

        RecordAction foundAction = null;
        String recordID = "";
        Action action = Action.UNKNOWN; // don't need to set this every time

        String keyParam = null;
        for (String p : params)
        {
            keyParam = p;
            // if record number matches DH[0-9] pattern means we are deleting a Heading.
            if (p.matches("DH[0-9]+"))
            {
                action = Action.DELETING_HEADER;
                break;
            }
            else if (p.matches("D[0-9]+"))
            {
                action = Action.DELETING_RECORD;
                break;
            }
            else if (p.matches("E00[0-9]+"))
            {
                action = Action.ADDING_HEADER;
                break;
            }
            // if record number matches EH[0-9] pattern means we are editing a header.
            else if (p.matches("EH[0-9]+"))
            {
                action = Action.EDITING_HEADER;
                break;
            }
            // if record number matches E[0-9] pattern means we are editing an informationRecord.
            else if (p.matches("E[0-9]+"))
            {
                action = Action.EDITING_RECORD;
                break;
            }
            // if record number matches AR[0-9] pattern means we are adding an informationRecord to a additionalHeading.
            else if (p.matches("AR[0-9]+"))
            {
                action = Action.ADDING_RECORD;
                break;
            }
        }

        if (action != Action.UNKNOWN) {
            recordID = keyParam.replaceFirst("[a-zA-Z]*", ""); // only need to set this in if(action!=
            foundAction = new RecordAction(recordID, recordType, action);
        }

        return foundAction;
    }

    /**
     * Validate Content.
     *
     * @param content
     * @param reqData
     * @return validation result
     */
    public ResultVO validateContent(String content, Map<String, String[]> reqData)
    {
        content = HtmlUtil.filterWysiwygContent(content);

        if(StringUtils.isEmpty(content))
        {
            return new ResultVO(false, "This field is required.");
        }

        return new ResultVO(true, null, null, null, content);
    }

    private static enum Action {
        UNKNOWN, ADDING_HEADER, EDITING_HEADER, DELETING_HEADER, ADDING_RECORD, EDITING_RECORD, DELETING_RECORD
    };
    private class RecordAction
    {
        String recordID = null;
        String recType = null;
        Action action = Action.UNKNOWN;

        /**
         * @param recordID
         * @param recordType
         * @param action
         */
        public RecordAction(String recordID, String recordType, Action action)
        {
            this.recordID = recordID;
            this.recType = recordType;
            this.action = action;
            // Temporary for compatibility with existing logic...
            // Set the indicator flags based on the action.
            // This will let us use the new constructor with the old flag tests.
            switch (action)
            {
                case ADDING_HEADER:
                    this.recType = "additionalheader";
                    break;
                case EDITING_HEADER:
                    this.recType = "additionalheader";
                    // don't set any indicators
                    break;
                case DELETING_HEADER:
                    this.recType = "additionalheader";
                    break;
                case ADDING_RECORD:
                    break;
                case EDITING_RECORD:
                    // don't set any indicators
                    break;
                case DELETING_RECORD:
                    break;
            }
        }

        @Override
        public String toString()
        {
            return this.action + " " + this.recType + " #" + this.recordID;
        }
    }
}
