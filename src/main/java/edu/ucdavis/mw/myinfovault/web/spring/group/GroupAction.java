/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: GroupAction.java
 */

package edu.ucdavis.mw.myinfovault.web.spring.group;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;

import org.springframework.validation.Errors;
import org.springframework.webflow.execution.Event;
import org.springframework.webflow.execution.RequestContext;

import edu.ucdavis.mw.myinfovault.domain.group.Group;
import edu.ucdavis.mw.myinfovault.domain.group.GroupImpl;
import edu.ucdavis.mw.myinfovault.domain.group.GroupType;
import edu.ucdavis.mw.myinfovault.events.EventDispatcher;
import edu.ucdavis.mw.myinfovault.events.GroupDeleteEvent;
import edu.ucdavis.mw.myinfovault.events.GroupPersistEvent;
import edu.ucdavis.mw.myinfovault.service.authorization.AuthorizationService;
import edu.ucdavis.mw.myinfovault.service.authorization.Permission;
import edu.ucdavis.mw.myinfovault.service.dossier.DossierService;
import edu.ucdavis.mw.myinfovault.service.group.GroupService;
import edu.ucdavis.mw.myinfovault.service.group.GroupTypeFilter;
import edu.ucdavis.mw.myinfovault.service.group.GroupViewFilter;
import edu.ucdavis.mw.myinfovault.service.person.AssignedRole;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.mw.myinfovault.service.person.MivRole;
import edu.ucdavis.mw.myinfovault.service.person.UserService;
import edu.ucdavis.mw.myinfovault.web.spring.assignment.AssignmentAction;
import edu.ucdavis.mw.myinfovault.web.spring.search.SearchStrategy;

/**
 * Controller for the manage groups spring webflow.
 *
 * @author Craig Gilmore
 * @since 4.3
 */
public class GroupAction extends AssignmentAction
{
    //URL parameter key
    private final static String GROUPID_PARAM = "groupId";

    //scope parameter key
    private static final String RESULTS_PARAM = "results";

    //assigned roles map keys
    private static final String DISABLED = "disabled";
    private static final String CHECKED = "checked";
    private static final String VALUE = "value";

    /**
     * Create the group webflow form action bean.
     *
     * @param searchStrategy
     * @param authorizationService
     * @param userService
     * @param dossierService
     * @param groupService
     */
    public GroupAction(SearchStrategy searchStrategy,
                       AuthorizationService authorizationService,
                       UserService userService,
                       DossierService dossierService,
                       GroupService groupService)
    {
        super(searchStrategy,
              authorizationService,
              userService,
              dossierService,
              groupService);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean canAccess(RequestContext context)
    {
        return authorizationService.hasPermission(getShadowPerson(context),
                                                  Permission.VIEW_GROUP,
                                                  null);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean canView(RequestContext context)
    {
        return canDo(context,
                     Permission.VIEW_GROUP);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean canEdit(RequestContext context)
    {
        return canDo(context,
                     Permission.EDIT_GROUP);
    }

    //does acting user have permission is this context
    private boolean canDo(RequestContext context,
                          String permission)
    {
        return isGroupAuthorized(getShadowPerson(context),
                                 getFormObject(context).getGroup(),
                                 permission);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Event addBreadcrumb(RequestContext context)
    {
        //is this flow a group view or edit?
        Boolean isEdit = context.getFlowScope().getBoolean(ISEDIT_PARAM);

        Group group = getFormObject(context).getGroup();

        //add group view/edit/add/menu bread crumb
        addBreadcrumbs(context,
                       isEdit == null//root flow if isView boolean DNE
                     ? "Manage Groups"
                     : (
                           group.getId() > 0//edit or view state if group ID is natural
                         ? (
                               isEdit//flow event determines text
                             ? "Edit"
                             : "View"
                           ) + " Group: " + group.getName()
                         : "Add a group"
                       ));

        return success();
    }

    /**
     * Loads the MIV group form object for the group ID from the URL.
     *
     * @param context Webflow request context
     * @return The GroupForm object
     */
    @Override
    public Object createFormObject(RequestContext context) throws Exception
    {
        int groupId = getGroupId(context);

        MivPerson actingPerson = getShadowPerson(context);

        return new GroupForm(
                        groupId > 0
                        ? groupService.getGroup(groupId)
                        : new GroupImpl(actingPerson),
                        actingPerson);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Event save(RequestContext context)
    {
        //get form object
        GroupForm form = getFormObject(context);

        /*
         * Add assigned group membership (and those inactive) to
         * the assigned a group may not contain other groups.
         */
        for (Group assignedGroup : form.getAssignedGroups())
        {
            form.getAssignedPeople().addAll(assignedGroup.getMembers());
            form.getInactivePeople().addAll(assignedGroup.getInactive());
        }

        //all members of these groups are now already assigned individually
        form.getAssignedGroups().clear();

        try
        {
            return groupService.persist(getShadowPerson(context),
                                        form.getGroup())

                 ? success()//group successfully persisted
                 : error();//persist failed or had no effect
        }
        catch(SecurityException e)
        {
            return denied();//user unauthorized
        }
        finally
        {
            EventDispatcher.getDispatcher().post(
                new GroupPersistEvent(getRealPerson(context).getUserId(),
                                      form.getGroup().getId()));
        }
    }

    /**
     * Deletes an MIV group from the form in the current context.
     *
     * @param context Webflow request context
     * @return Webflow event status
     */
    public Event delete(RequestContext context)
    {
        try
        {
            return groupService.delete(getShadowPerson(context),
                                       getGroupId(context)) != null
                 ? success()//group successfully deleted
                 : error();//delete failed or had no effect
        }
        catch(SecurityException e)
        {
            return denied();//user unauthorized
        }
        finally
        {
            EventDispatcher.getDispatcher().post(
                new GroupDeleteEvent(getRealPerson(context).getUserId(),
                                     getGroupId(context)));
        }
    }

    /**
     * Put the list of groups visible to the acting person in the request scope.
     *
     * @param context Webflow request context
     * @return Webflow event status
     */
    @SuppressWarnings("unchecked")
    public Event getGroups(RequestContext context)
    {
        context.getRequestScope().put(RESULTS_PARAM,
                                      groupService.getGroups(new GroupTypeFilter(GroupType.GROUP),
                                                             new GroupViewFilter(getShadowPerson(context))));

        return success();
    }

    //get the group ID from the URL
    private int getGroupId(RequestContext context)
    {
        return getParameterValue(context, GROUPID_PARAM, 0);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected List<Object> getAvailable(RequestContext context)
    {
        SortedSet<Group> availableGroups = super.getAvailableGroups(context);

        //remove the group to which we are assigning from the available groups
        availableGroups.remove(getFormObject(context).getGroup());

        //combine into one list all available groups and people
        List<Object> available = new ArrayList<Object>();
        available.addAll(getAvailablePeople(context));
        available.addAll(availableGroups);

        return available;
    }

    /**
     * Parse checked assigned roles and associate them with the
     * group and update the assignment from bulk arrays in form.
     *
     * {@inheritDoc}
     */
    @Override
    public Event bind(RequestContext context) throws Exception
    {
        //pass off to super for bulk user additions/removals
        Event e = super.bind(context);

        //get form object
        GroupForm form = getFormObject(context);

        //update assigned roles if group is not hidden
        if (form.getGroup().getHidden() != null
         && !form.getGroup().getHidden())
        {
            //bind checked assigned roles
            for (MivRole role : form.getAssignedRoles().keySet())
            {
                for (Map<String,Object> assignment : form.getAssignedRoles().get(role))
                {
                    //disabled if not null and parses true to boolean
                    boolean isDisabled = assignment.get(DISABLED) != null
                                      && Boolean.parseBoolean(assignment.get(DISABLED).toString());

                    //ignore disabled check boxes; the acting user is not associated with them may not effect changes
                    if (!isDisabled)
                    {
                        AssignedRole assignedRole = (AssignedRole) assignment.get(VALUE);

                        //checked if not null and parses true to boolean
                        boolean isChecked = assignment.get(CHECKED) != null
                                         && Boolean.parseBoolean(assignment.get(CHECKED).toString());

                        //add if checked
                        if (isChecked)
                        {
                            form.getGroup().getAssignedRoles().add(assignedRole);
                        }
                        //remove otherwise
                        else
                        {
                            form.getGroup().getAssignedRoles().remove(assignedRole);
                        }
                    }
                }
            }
        }

        //If the remove all flag is not set, activate/deactivate selected people
        if (!form.isRemoveAll())
        {
            //add/remove selected users to/from the inactive collection in the form
            form.getInactivePeople().addAll(userService.getPeopleByMivId(form.getDeactivatePeople()));
            form.getInactivePeople().removeAll(userService.getPeopleByMivId(form.getActivatePeople()));

            //inactive set should only retain assigned people
            form.getInactivePeople().retainAll(form.getAssignedPeople());
        }

        //reset form fields
        form.reset();

        return e;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Event bindAndValidate(RequestContext context) throws Exception
    {
        Event event = bind(context);

        //repository of errors for this form
        Errors errors = getFormErrors(context);

        //validate search criteria
        ((GroupValidator) getValidator()).validateSearchCriteria(getFormObject(context),
                                                                 errors);
        //errors were found
        if (errors.hasErrors())
        {
            return error();
        }

        return event;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public GroupForm getFormObject(RequestContext context)
    {
        return (GroupForm) super.getFormObject(context);
    }
}
