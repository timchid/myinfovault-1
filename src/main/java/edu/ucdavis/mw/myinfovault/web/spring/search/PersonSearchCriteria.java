/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: PersonSearchCriteria.java
 */

package edu.ucdavis.mw.myinfovault.web.spring.search;

import java.util.List;

import edu.ucdavis.mw.myinfovault.service.person.MivPerson;

/**
 * WTF is this class for? Is this the form backing object for de/re-activate?
 *
 * @author Leo Golubev
 * @since MIV 3.0
 */
public class PersonSearchCriteria extends SearchCriteria
{

    private static final long serialVersionUID = -1737933765408912603L;

    private List<MivPerson> filteredList, changeList, failureList;

    //list of all persons - MIV members
    public List<MivPerson> getFilteredList()
    {
        return filteredList;
    }

    public void setFilteredList(List<MivPerson> filteredList)
    {
        this.filteredList = filteredList;
    }

    //list of all changed active flag persons - MIV members
    public List<MivPerson> getChangeList()
    {
        return changeList;
    }

    public void setChangeList(List<MivPerson> changeList)
    {
        this.changeList = changeList;
    }
    
    //list of all failed changed active flag persons - MIV members
    public List<MivPerson> getFailureList()
    {
        return failureList;
    }

    public void setFailureList(List<MivPerson> failureList)
    {
        this.failureList = failureList;
    }
}
