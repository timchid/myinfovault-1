/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: ConvertAppointee.java
 */


package edu.ucdavis.mw.myinfovault.web;

import java.io.IOException;
import java.sql.Types;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;

import edu.ucdavis.mw.myinfovault.service.MivServiceLocator;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.mw.myinfovault.service.person.MivRole;
import edu.ucdavis.mw.myinfovault.service.person.UserService;
import edu.ucdavis.myinfovault.MIVConfig;
import edu.ucdavis.myinfovault.MIVUser;
import edu.ucdavis.myinfovault.data.UCDLdap;


/**
 * Short-term: Servlet to convert an Appointee to a Candidate.
 * Note this does no error checking for non-numeric or non-existent userIds,
 * login names, or missing LDAP data. As a short-term solution it counts on
 * being called correctly and only when appropriate.
 *
 * @author Stephen Paulsen
 * @since MIV 4.8.2
 */
public class ConvertAppointee extends MIVServlet
{
    private static final long serialVersionUID = 201404211105L;

    private static final UserService userService = MivServiceLocator.getUserService();


    /**
     * Roles authorized to convert an Appointee to a Candidate.<br>
     * This may be changed to <code>authorizedRoles = { MivRole.SYS_ADMIN, MivRole.VICE_PROVOST_STAFF };</code>
     */
    private static final MivRole[] authorizedRoles = { MivRole.SYS_ADMIN, MivRole.VICE_PROVOST_STAFF };

    /* LDAP attribute strings used for information lookup. */
    private static final String LDAP_EMPLOYEE_ID = "employeeNumber";
    private static final String LDAP_PERSON_UUID = "ucdPersonUUID";
    private static final String LDAP_USER_EMAIL = "mail";
    private static final String LDAP_UID = "uid";


    private static String targetPage = "/WEB-INF/jsp/convertappointee.jsp";


    /**
     * Handle the request for the page.
     * Returns a 403 "Forbidden" response if the user is not authorized for this action.
     */
    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse res)
        throws ServletException, IOException
    {
        if (! isAuthorized( mivSession.get().getUser() )) {
            res.sendError(HttpServletResponse.SC_FORBIDDEN/*, "some additional custom message"*/);
            return;
        }

        req.setAttribute("status", "fetch");
        dispatch(targetPage, req, res);
    }


    /**
     * Handle the form POST data, gathering the form content and LDAP information,
     * and converting the given Appointee to a Candidate.
     * Returns a 403 "Forbidden" response if the user is not authorized for this action.
     */
    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse res)
        throws ServletException, IOException
    {
        MIVUser user = mivSession.get().getUser();

        String status = "confirm";
//        boolean error = false;
        String message = null;

        Map<String, Object> out = null;

        if (! isAuthorized( user )) {
            res.sendError(HttpServletResponse.SC_FORBIDDEN);
            return;
        }

        String userId = req.getParameter("userId"); // MIV user ID number
        String empnum = req.getParameter("empnum"); // Employee ID number

        // Check for missing parameters
        if (StringUtils.isBlank(userId) || StringUtils.isBlank(empnum)) {
//            error = true;
            status = "caution";
            message = "User ID and Employee Number are required.";
            reject(req, res, userId, empnum, message);
            return;
        }

        // Check for 9-digit employee ID number
        if (! empnum.matches("\\d{9}")) {
            status = "caution";
            message = "Employee ID must be a 9 digit number";
            reject(req, res, userId, empnum, message);
        }


        MivPerson person = null;

        // Get the user with userId — reject if not-a-number...
        try {
            int mivUserId = Integer.parseInt(userId);
            person = userService.getPersonByMivId(mivUserId);
        }
        catch (NumberFormatException e) {
            reject(req, res, userId, empnum, "MIV User ID must be numeric");
            return;
        }

        // ... and reject if there's no person with that userId
        if (person == null) {
            reject(req, res, userId, empnum, "There is no user with User ID " + userId);
            return;
        }

        // Check that userId is an Appointee
        if (person.getPrimaryRoleType() != MivRole.APPOINTEE) {
            reject(req, res, userId, empnum, "Only an Appointee can be converted - user " + userId +
                   " is a " + person.getPrimaryRoleType().getShortDescription());
            return;
        }


        logger.info("Converting Appointee to Candidate: got request for UserId {} with Employee Number {}", userId, empnum);

        Map<String,String> info = getLdapInfo(empnum);
        String login = info.get(LDAP_UID);
        String personUUID = info.get(LDAP_PERSON_UUID);
        String employeeID = info.get(LDAP_EMPLOYEE_ID);
        String email = info.get(LDAP_USER_EMAIL);

        if (StringUtils.isBlank(personUUID)
         || StringUtils.isBlank(login)
         || StringUtils.isBlank(employeeID)
         || StringUtils.isBlank(email))
        {
            reject(req, res, userId, empnum, "Couldn't find LDAP info for Employee Number \"" + empnum + "\"");
            return;
        }


        out = update(user.getTargetUserInfo().getPerson().getUserId(),
                     userId, login, personUUID, employeeID, email);

        message = parseOutput(out);

        req.setAttribute("status", status);
        req.setAttribute("pagemessage", message != null ? message : out);
        if (! "confirm".equals(status)) {
            req.setAttribute("userId", userId);
            req.setAttribute("empnum", empnum);
        }

        dispatch(targetPage, req, res);
    }


    private void reject(HttpServletRequest req, HttpServletResponse res, String userId, String empnum, String message)
            throws ServletException, IOException
    {
        logger.info("Rejecting user #\"{}\" / employee ID \"{}\" with message [{}]",
                    new String[] { userId, empnum, message });

        req.setAttribute("status", "caution"); // "caution" is not a great status, but makes it easy to use the existing CSS class.
        req.setAttribute("pagemessage", message);
        req.setAttribute("userId", userId);
        req.setAttribute("empnum", empnum);

        dispatch(targetPage, req, res);
    }


    /**
     * Call the SQL stored procedure to convert an Appointee to a Candidate
     * @param updateUser MIV UserId of the person running the conversion.
     * @param convertUser UserId of the person being converted
     * @param uid Login (aka Uid) of the person being converted
     * @param personUUID ucdPersonUUID (aka MothraID) of the person being converted
     * @param employeeId EmployeeId (aka employeeNumber) of the person being converted
     * @param email Email address of the person being converted
     */
    private Map<String, Object> update(int updateUser, String convertUser, String uid, String personUUID, String employeeId, String email)
    {
        final Object[] params = new Object[] { updateUser, convertUser, uid, personUUID, employeeId, email };

        logger.info("call transition_appointee({}, {}, {}, {}, {}, {})", params);

        SimpleJdbcCall proc = new SimpleJdbcCall(MIVConfig.getConfig().getDataSource()).withProcedureName("TRANSITION_APPOINTEE");
        proc.setFunction(false);

        logger.info("Going to call stored procedure: {}", proc.getProcedureName());

        SqlParameterSource in = new MapSqlParameterSource()
            .addValue("inUpdateUser", updateUser, Types.INTEGER)
            .addValue("inUserId", convertUser, Types.INTEGER)
            .addValue("inUid", uid, Types.VARCHAR)
            .addValue("inPersonUUID", personUUID, Types.VARCHAR)
            .addValue("inEmployeeId", employeeId, Types.VARCHAR)
            .addValue("inEmail", email, Types.VARCHAR);

        Map<String, Object> out = proc.execute(in);

        logger.info("Result of execute TRANSITION_APPOINTEE:\n{}", out);

        // Return some sort of results so maybe the caller can use them for something.
        return out;
    }


    /**
     * Get the information required for conversion from LDAP
     *
     * @param empnum Employee ID Number of the person for whom to fetch LDAP information.
     * @return a Map of attribute-to-value pairs of Strings.
     */
    private Map<String,String> getLdapInfo(String empnum)
    {
        UCDLdap.Provider provider = UCDLdap.Provider.PEOPLE;
        UCDLdap dir = new UCDLdap(provider);
        Map<String, String> entry = dir.search("(employeeNumber=" + empnum + ")", LDAP_UID, LDAP_PERSON_UUID, LDAP_EMPLOYEE_ID, LDAP_USER_EMAIL);
        dir.close();

        return entry;
    }


//    @SuppressWarnings("unchecked")
    private String parseOutput(Map<String, Object> o)
    {
/* --- Sample output
{
  #result-set-1=[{inUserId=21122, inUid=gribble, inPersonUUID=00022746}],
  #result-set-2=[ {ID=21512, UserID=21122, RoleID=6, MivCode=CANDIDATE, Scope=SchoolID=38, DepartmentID=313, PrimaryRole=true},
                  {ID=21513, UserID=21122, RoleID=6, MivCode=CANDIDATE, Scope=SchoolID=1, DepartmentID=1, PrimaryRole=true},
                  {ID=21524, UserID=21122, RoleID=7, MivCode=MIV_USER, Scope=SchoolID=38, DepartmentID=313, PrimaryRole=false}
                ],
  #update-count-1=0
}
*/
/*        @SuppressWarnings("unused")
        Object r1 = o.get("#result-set-1");
        Object r2 = o.get("#result-set-2"); //ArrayList
        List<Object> roles = (List<Object>) r2;
        StringBuilder sb = new StringBuilder("<strong>Updated Role Data:</strong><br>");
        for (Object role : roles)
        {
            sb.append(role.toString()).append("<br>");
        }
        return sb.toString();//r2.toString();
*/
        return "Conversion has completed";
    }


    /**
     * Simple check to see if the user has a role that is authorized to convert Appointees to Candidates.
     * To be replaced later by the AuthorizationService and a Permission.
     * @param user MIVUser to check
     * @return <code>true</code> iff the user is authorized based on their roles, <code>false</code> otherwise.
     */
    private boolean isAuthorized(MIVUser user)
    {
        return user.getUserInfo().getPerson().hasRole( authorizedRoles );
    }
}
