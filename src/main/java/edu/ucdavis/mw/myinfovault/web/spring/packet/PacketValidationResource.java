/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: PacketValidationResource.java
 */

package edu.ucdavis.mw.myinfovault.web.spring.packet;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlRootElement;

import org.springframework.hateoas.ResourceSupport;

import edu.ucdavis.mw.myinfovault.domain.packet.Packet;
import edu.ucdavis.mw.myinfovault.domain.packet.PacketContent;
import edu.ucdavis.mw.myinfovault.service.MivServiceLocator;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.mw.myinfovault.service.person.UserService;
import edu.ucdavis.mw.myinfovault.web.ValidationErrorResource;
import edu.ucdavis.myinfovault.data.DataFetcher;

/**
 * FIXME: Needs javadoc
 * @author japorito
 * @since 5.0
 */
@XmlRootElement(name="error")
public class PacketValidationResource extends ValidationErrorResource
{
    private static final String PACKET_ID_MISMATCH = "That packet cannot be saved with the specified ID.";
    private static final String WRONG_PACKET_USER = "The user id specified in the request is not the owner of the packet";
    private static final String USER_UNKNOWN = "Server is unable to determine who the packet belongs to. Specify userId.";
    private static final String USER_DOESNT_EXIST = "The specified user does not exist in MIV.";
    private static final String EMPTY_PACKET = "Packets must have at least one piece of data selected for inclusion.";
    private static final String PKT_OWNER_NOT_ITEM_OWNER = "Packet contents do not belong to the packet owner.";
    private static final String INVALID_DISPLAY_PARTS = "{0} is not a valid display field.";

    private static final UserService userService = MivServiceLocator.getUserService();

    private static final List<String> validDisplayParts = new ArrayList<String>();
    static {
        validDisplayParts.add("display");
        validDisplayParts.add("displaycontribution");
        validDisplayParts.add("displaydescription");
    }


    public PacketValidationResource(Packet originalPacket, Packet newPacket)
    {
        //When not a new packet creation, do some additional validation to check
        //that the important parts of the request and the packet are consistent with
        //the original.
        if (!originalPacket.getPacketId().equals(newPacket.getPacketId())) {
            this.addError(PACKET_ID_MISMATCH);
        }

        if (!originalPacket.getUserId().equals(newPacket.getUserId()))
        {
            this.addError(WRONG_PACKET_USER);
        }

        this.validate(newPacket);
    }


    public PacketValidationResource(Packet newPacket)
    {
        this.validate(newPacket);
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.web.ValidationErrorResource#validate(org.springframework.hateoas.ResourceSupport)
     */
    @Override
    public Boolean validate(ResourceSupport resource)
    {
        Packet packet = (Packet) resource;

        if (packet.getUserId() == null)
        {
            this.addError(USER_UNKNOWN);
        }

        if (packet.getPacketItemsMap() == null || packet.getPacketItemsMap().isEmpty())
        {
            this.addError(EMPTY_PACKET);
        }

        MivPerson packetOwner = userService.getPersonByMivId(packet.getUserId());
        if (packetOwner == null)
        {
            this.addError(USER_DOESNT_EXIST);
        }
        else if (packet.getPacketItemsMap() != null)
        {
            validatePacketItems(packetOwner, packet.getPacketItems());
        }

        this.valid = this.errors.isEmpty();

        return this.valid;
    }


    public void validatePacketItems(MivPerson packetOwner, Collection<PacketContent> packetItems)
    {
        DataFetcher dataFetcher = new DataFetcher( userService.getMivUserById(packetOwner.getUserId()) );

        Map<Integer, List<Map<String, Object>>> includedSections = new HashMap<>();
        List<Map<String, Object>> sectionData;
        for (PacketContent item : packetItems)
        {
            for (String displayPart : item.getDisplayParts().split(","))
            {
                if (!validDisplayParts.contains(displayPart))
                {
                    this.addError(MessageFormat.format(INVALID_DISPLAY_PARTS, displayPart));
                }
            }


            boolean userOwnsItem = false;
            sectionData = includedSections.get(item.getSectionId());
            if (sectionData == null)
            {
                sectionData = dataFetcher.getSectionById(item.getSectionId());
                includedSections.put(item.getSectionId(), sectionData);
            }

            for (Map<String, Object> dataRecord : sectionData)
            {
                if ((Integer) dataRecord.get("ID") == item.getRecordId())
                {
                    userOwnsItem = true;
                    break;
                }
            }

            if (!userOwnsItem)
            {
                this.addError(PKT_OWNER_NOT_ITEM_OWNER);
                break;
            }
        }
    }
}
