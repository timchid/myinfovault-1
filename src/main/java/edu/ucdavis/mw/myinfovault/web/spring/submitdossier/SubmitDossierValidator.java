/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: SubmitDossierValidator.java
 */

package edu.ucdavis.mw.myinfovault.web.spring.submitdossier;

import org.springframework.validation.Errors;

import edu.ucdavis.mw.myinfovault.web.spring.action.AcademicActionValidator;

/**
 * Validates the forms extending {@link SubmitDossierForm}.
 *
 * @author Pradeep K Haldiya
 * @since MIV 4.8
 */
public class SubmitDossierValidator extends AcademicActionValidator
{
    /*
     * (non-Javadoc)
     * @see org.springframework.validation.Validator#supports(java.lang.Class)
     */
    @Override
    public boolean supports(@SuppressWarnings("rawtypes") Class clazz)
    {
        return SubmitDossierForm.class.isAssignableFrom(clazz);
    }

    /*
     * (non-Javadoc)
     * @see org.springframework.validation.Validator#validate(java.lang.Object, org.springframework.validation.Errors)
     */
    @Override
    public void validate(Object obj, Errors errors)
    {
        SubmitDossierForm form = ((SubmitDossierForm) obj);

        /*
         * Action type and delegation of authority
         */
        validateAction(form.getActionType(), form.getDelegationAuthority(), errors);

        /*
         * Academic action effective and retroactive dates
         */
        validateDates(form, errors);
    }
}
