package edu.ucdavis.mw.myinfovault.web.spring.biosketch;

/**
 * Command Object representing a Biosketch
 * @author dreddy
 * @since MIV 2.1
 *
 * Updated: Pradeep on 08/03/2012
 */
public class BiosketchCommand extends BaseCommand
{
    private String name;
    private String title;
    private String biosketchType;
    /********* Biosketch Style *********/
    private String styleName;
    private String pageWidth;
    private String pageHeight;
    private String marginTop;
    private String marginRight;
    private String marginBottom;
    private String marginLeft;
    private String titleFontID;
    private String titleSize;
    private String titleAlign;
    private String titleBold;
    private String titleItalic;
    private String titleUnderline;
    private String titleRules;
    private String titleEditable;
    private String displayNameAlign;
    private String headerFontID;
    private String headerSize;
    private String headerAlign;
    private String headerBold;
    private String headerItalic;
    private String headerUnderline;
    private String headerIndent;
    private String bodyFontID;
    private String bodySize;
    private String bodyIndent;
    private String bodyFormatting;
    private String footerOn;
    private String footerPageNumbers;
    private String footerRules;
    /********* Biosketch Attributes *********/
    private String programDirectorName;
    private String programDirectorNameSequence;
    private String programDirectorNameDisplay;
    private String fullName;
    private String fullNameSequence;
    private String fullNameDisplay;
    private String eraUserName;
    private String eraUserSequence;
    private String eraUserDisplay;
    private String positionTitle1;
    private String positionTitle1Sequence;
    private String positionTitle1Display;
    private String positionTitle2;
    private String positionTitle2Sequence;
    private String positionTitle2Display;

    private BiosketchAttributesCommand biosketchAttributes =  new BiosketchAttributesCommand();

    /**
     * @return
     */
    public BiosketchAttributesCommand getBiosketchAttributes()
    {
        return biosketchAttributes;
    }

    /**
     * @param biosketchAttributes
     */
    public void setBiosketchAttributes(BiosketchAttributesCommand biosketchAttributes)
    {
        this.biosketchAttributes = biosketchAttributes;
    }

    /**
     * @return
     */
    public String getName()
    {
        return name;
    }

    /**
     * @param name
     */
    public void setName(String name)
    {
        this.name = name;
    }

    /**
     * @return
     */
    public String getTitle()
    {
        return title;
    }

    /**
     * @param title
     */
    public void setTitle(String title)
    {
        this.title = title;
    }



    /**
     * @return
     */
    public String getBodyFontID()
    {
        return bodyFontID;
    }

    /**
     * @param bodyFontID
     */
    public void setBodyFontID(String bodyFontID)
    {
        this.bodyFontID = bodyFontID;
    }

    /**
     * @return
     */
    public String getBodyFormatting()
    {
        return bodyFormatting;
    }

    /**
     * @param bodyFormatting
     */
    public void setBodyFormatting(String bodyFormatting)
    {
        this.bodyFormatting = bodyFormatting;
    }

    /**
     * @return
     */
    public String getBodyIndent()
    {
        return bodyIndent;
    }

    /**
     * @param bodyIndent
     */
    public void setBodyIndent(String bodyIndent)
    {
        this.bodyIndent = bodyIndent;
    }

    /**
     * @return
     */
    public String getBodySize()
    {
        return bodySize;
    }

    /**
     * @param bodySize
     */
    public void setBodySize(String bodySize)
    {
        this.bodySize = bodySize;
    }

    /**
     * @param footerOn
     */
    public void setFooterOn(String footerOn)
    {
        this.footerOn = footerOn;
    }

    /**
     * @return
     */
    public String getFooterPageNumbers()
    {
        return footerPageNumbers;
    }

    /**
     * @param footerPageNumbers
     */
    public void setFooterPageNumbers(String footerPageNumbers)
    {
        this.footerPageNumbers = footerPageNumbers;
    }

    /**
     * @return
     */
    public String getFooterRules()
    {
        return footerRules;
    }

    /**
     * @param footerRules
     */
    public void setFooterRules(String footerRules)
    {
        this.footerRules = footerRules;
    }

    /**
     * @return
     */
    public String getHeaderAlign()
    {
        return headerAlign;
    }

    /**
     * @param headerAlign
     */
    public void setHeaderAlign(String headerAlign)
    {
        this.headerAlign = headerAlign;
    }

    /**
     * @return
     */
    public String getHeaderFontID()
    {
        return headerFontID;
    }

    /**
     * @param headerFontID
     */
    public void setHeaderFontID(String headerFontID)
    {
        this.headerFontID = headerFontID;
    }

    /**
     * @return
     */
    public String getHeaderIndent()
    {
        return headerIndent;
    }

    /**
     * @param headerIndent
     */
    public void setHeaderIndent(String headerIndent)
    {
        this.headerIndent = headerIndent;
    }

    /**
     * @return
     */
    public String getHeaderSize()
    {
        return headerSize;
    }

    /**
     * @param headerSize
     */
    public void setHeaderSize(String headerSize)
    {
        this.headerSize = headerSize;
    }

    /**
     * @return
     */
    public String getMarginBottom()
    {
        return marginBottom;
    }

    /**
     * @param marginBottom
     */
    public void setMarginBottom(String marginBottom)
    {
        this.marginBottom = marginBottom;
    }

    /**
     * @return
     */
    public String getMarginLeft()
    {
        return marginLeft;
    }

    /**
     * @param marginLeft
     */
    public void setMarginLeft(String marginLeft)
    {
        this.marginLeft = marginLeft;
    }

    /**
     * @return
     */
    public String getMarginRight()
    {
        return marginRight;
    }

    /**
     * @param marginRight
     */
    public void setMarginRight(String marginRight)
    {
        this.marginRight = marginRight;
    }

    /**
     * @return
     */
    public String getMarginTop()
    {
        return marginTop;
    }

    /**
     * @param marginTop
     */
    public void setMarginTop(String marginTop)
    {
        this.marginTop = marginTop;
    }

    /**
     * @return
     */
    public String getPageHeight()
    {
        return pageHeight;
    }

    /**
     * @param pageHeight
     */
    public void setPageHeight(String pageHeight)
    {
        this.pageHeight = pageHeight;
    }

    /**
     * @return
     */
    public String getPageWidth()
    {
        return pageWidth;
    }

    /**
     * @param pageWidth
     */
    public void setPageWidth(String pageWidth)
    {
        this.pageWidth = pageWidth;
    }

    /**
     * @return
     */
    public String getStyleName()
    {
        return styleName;
    }

    /**
     * @param styleName
     */
    public void setStyleName(String styleName)
    {
        this.styleName = styleName;
    }

    /**
     * @return
     */
    public String getTitleAlign()
    {
        return titleAlign;
    }

    /**
     * @param titleAlign
     */
    public void setTitleAlign(String titleAlign)
    {
        this.titleAlign = titleAlign;
    }

    /**
     * @return
     */
    public String getTitleEditable()
    {
        return titleEditable;
    }

    /**
     * @param titleEditable
     */
    public void setTitleEditable(String titleEditable)
    {
        this.titleEditable = titleEditable;
    }

    /**
     * @return
     */
    public String getTitleFontID()
    {
        return titleFontID;
    }

    /**
     * @param titleFontID
     */
    public void setTitleFontID(String titleFontID)
    {
        this.titleFontID = titleFontID;
    }

    /**
     * @return
     */
    public String getTitleRules()
    {
        return titleRules;
    }

    /**
     * @param titleRules
     */
    public void setTitleRules(String titleRules)
    {
        this.titleRules = titleRules;
    }

    /**
     * @return
     */
    public String getTitleSize()
    {
        return titleSize;
    }

    /**
     * @param titleSize
     */
    public void setTitleSize(String titleSize)
    {
        this.titleSize = titleSize;
    }

    /**
     * @return
     */
    public String getFooterOn()
    {
        return footerOn;
    }

    public String getDisplayNameAlign()
    {
        return displayNameAlign;
    }

    public void setDisplayNameAlign(String displayNameAlign)
    {
        this.displayNameAlign = displayNameAlign;
    }

    public String getHeaderBold()
    {
        return headerBold;
    }

    public void setHeaderBold(String headerBold)
    {
        this.headerBold = headerBold;
    }

    public String getHeaderItalic()
    {
        return headerItalic;
    }

    public void setHeaderItalic(String headerItalic)
    {
        this.headerItalic = headerItalic;
    }

    public String getHeaderUnderline()
    {
        return headerUnderline;
    }

    public void setHeaderUnderline(String headerUnderline)
    {
        this.headerUnderline = headerUnderline;
    }

    public String getTitleBold()
    {
        return titleBold;
    }

    public void setTitleBold(String titleBold)
    {
        this.titleBold = titleBold;
    }

    public String getTitleItalic()
    {
        return titleItalic;
    }

    public void setTitleItalic(String titleItalic)
    {
        this.titleItalic = titleItalic;
    }

    public String getTitleUnderline()
    {
        return titleUnderline;
    }

    public void setTitleUnderline(String titleUnderline)
    {
        this.titleUnderline = titleUnderline;
    }


    public String getEraUserDisplay()
    {
        return eraUserDisplay;
    }
    public void setEraUserDisplay(String eraUserDisplay)
    {
        this.eraUserDisplay = eraUserDisplay;
    }
    public String getEraUserName()
    {
        return eraUserName;
    }
    public void setEraUserName(String eraUserName)
    {
        this.eraUserName = eraUserName;
    }
    public String getEraUserSequence()
    {
        return eraUserSequence;
    }
    public void setEraUserSequence(String eraUserSequence)
    {
        this.eraUserSequence = eraUserSequence;
    }
    public String getFullName()
    {
        return fullName;
    }
    public void setFullName(String fullName)
    {
        this.fullName = fullName;
    }
    public String getFullNameDisplay()
    {
        return fullNameDisplay;
    }
    public void setFullNameDisplay(String fullNameDisplay)
    {
        this.fullNameDisplay = fullNameDisplay;
    }
    public String getFullNameSequence()
    {
        return fullNameSequence;
    }
    public void setFullNameSequence(String fullNameSequence)
    {
        this.fullNameSequence = fullNameSequence;
    }

    public String getProgramDirectorName()
    {
        return programDirectorName;
    }
    public void setProgramDirectorName(String programDirectorName)
    {
        this.programDirectorName = programDirectorName;
    }
    public String getProgramDirectorNameDisplay()
    {
        return programDirectorNameDisplay;
    }
    public void setProgramDirectorNameDisplay(String programDirectorNameDisplay)
    {
        this.programDirectorNameDisplay = programDirectorNameDisplay;
    }
    public String getProgramDirectorNameSequence()
    {
        return programDirectorNameSequence;
    }
    public void setProgramDirectorNameSequence(String programDirectorNameSequence)
    {
        this.programDirectorNameSequence = programDirectorNameSequence;
    }

    public String getPositionTitle1()
    {
        return positionTitle1;
    }
    public void setPositionTitle1(String positionTitle1)
    {
        this.positionTitle1 = positionTitle1;
    }
    public String getPositionTitle1Display()
    {
        return positionTitle1Display;
    }
    public void setPositionTitle1Display(String positionTitle1Display)
    {
        this.positionTitle1Display = positionTitle1Display;
    }
    public String getPositionTitle1Sequence()
    {
        return positionTitle1Sequence;
    }
    public void setPositionTitle1Sequence(String positionTitle1Sequence)
    {
        this.positionTitle1Sequence = positionTitle1Sequence;
    }
    public String getPositionTitle2()
    {
        return positionTitle2;
    }
    public void setPositionTitle2(String positionTitle2)
    {
        this.positionTitle2 = positionTitle2;
    }
    public String getPositionTitle2Display()
    {
        return positionTitle2Display;
    }
    public void setPositionTitle2Display(String positionTitle2Display)
    {
        this.positionTitle2Display = positionTitle2Display;
    }
    public String getPositionTitle2Sequence()
    {
        return positionTitle2Sequence;
    }
    public void setPositionTitle2Sequence(String positionTitle2Sequence)
    {
        this.positionTitle2Sequence = positionTitle2Sequence;
    }

    public BiosketchType getBiosketchType()
    {
        return BiosketchType.convert(biosketchType);
    }

    public void setBiosketchType(String biosketchType)
    {
        this.biosketchType = biosketchType;
    }
}
