/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: package-info.java
 */
//   see http://www.intertech.com/Blog/whats-package-info-java-for/
/**
 * Classes used for the Spring based web UI for Packet management.
 */
package edu.ucdavis.mw.myinfovault.web.spring.packet;