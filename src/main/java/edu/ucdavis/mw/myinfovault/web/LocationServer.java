/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: LocationServer.java
 */

package edu.ucdavis.mw.myinfovault.web;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.MediaType;

import org.springframework.util.StringUtils;

import edu.ucdavis.mw.myinfovault.dao.location.LocationDao;
import edu.ucdavis.mw.myinfovault.dao.location.LocationDaoImpl;
import edu.ucdavis.mw.myinfovault.util.StringUtil;
import edu.ucdavis.mw.myinfovault.valuebeans.ResultVO;
import edu.ucdavis.myinfovault.data.MIVUtil;

/**
 * Get countries, provinces, cities information
 * @author pradeeph
 * @since MIV v4.4
 */
public class LocationServer extends MIVServlet
{
    private static final long serialVersionUID = 201204191428L;
    private LocationDao locDao = new LocationDaoImpl();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        String requestType = req.getParameter("requestType");
        logger.debug("requestType :: {}", requestType);

        if (requestType == null || requestType.length() == 0) {
            // No requestType provided - return nothing
            return;
        }

        List<Map<String, String>> resultList = null;

        String countryCode = MIVUtil.sanitize(req.getParameter("country"));
        String province = req.getParameter("province");
        String city = req.getParameter("city");

        /*System.out.println("countryCode :: "+countryCode);
        System.out.println("province :: "+province);
        System.out.println("city :: "+city);*/

        logger.debug("countryCode :: {}", countryCode);

        if (StringUtils.hasText(countryCode))
        {
            if (requestType.equalsIgnoreCase("provincesbycountrycode"))
            {
                    resultList = locDao.suggestProvinces(countryCode,province);
            }
            else if (requestType.equalsIgnoreCase("citiesbycountrycode"))
            {
                    resultList = locDao.getCitiesByCountryCode(countryCode);
            }
            else if (requestType.equalsIgnoreCase("citiesbyprovinceandcountrycode"))
            {
                    resultList = locDao.suggestCities(countryCode,province,city);
            }
        }


        ServletOutputStream os = resp.getOutputStream();
        resp.setContentType(isAjaxRequest() ? MediaType.APPLICATION_JSON : MediaType.TEXT_PLAIN);
        /*System.out.println("resultList :: "+resultList);*/

        if (resultList == null) {
            // something's wrong - no handler configured + no default handler
            os.println("[ ]");
            return;
        }

        final String result = StringUtil.toJson(resultList);
        logger.debug("StringUtil.toJson(resultList) produced:\n{}", result);
        os.println(result);
    }


    /**
     * to validate location combinations
     * @param dummy
     * @param reqData
     * @return
     */
    public ResultVO validateLocation(String dummy, Map <String, String[]> reqData)
    {
        String country = (reqData.get("country") != null ? reqData.get("country")[0] : null);
        String province = (reqData.get("province") != null ? reqData.get("province")[0] : null);
        String city = (reqData.get("city") != null ? reqData.get("city")[0] : null);

        boolean hasCountry = (country != null && country.trim().length() > 0 && !country.trim().equals("0"));
        boolean hasProvince = (province != null && province.trim().length() > 0 && !province.trim().equals("0"));
        boolean hasCity = (city != null && city.trim().length() > 0 && !city.trim().equals("0"));

        if ( !hasCountry )
        {
            country = null;
        }

        // If nothing is selected no need to validate
        if ( !hasCountry && !hasProvince && !hasCity )
        {
            return new ResultVO(true).setValue(country);
        }

        // Country must be required to pick province
        if ( !hasCountry && hasProvince )
        {
            return new ResultVO(false, "This field is required to combine with state&#47;province.", "country");
        }
        else if( hasCountry && hasProvince )
        {
            ResultVO resultVO = validateProvince(province,reqData);
            if(!resultVO.isSuccess())
            {
                // Invalid province no need to check combination.
                return new ResultVO(true).setValue(country);
            }
        }
        // Country must be required to pick city
        else if ( !hasCountry && hasCity )
        {
            return new ResultVO(false, "This field is required to combine with city.", "country");
        }
        else if ( hasCountry && hasCity )
        {
            ResultVO resultVO = validateCity(city,reqData);
            if(!resultVO.isSuccess())
            {
                // Invalid province no need to check combination.
                return new ResultVO(true).setValue(country);
            }
        }
        /*// Province must be required to pick city
        else if ( !hasProvince && hasCity )
        {
            return new ResultVO(false, "This field is required.", "province");
        }*/

        // validate the combinations
        if (!locDao.validateLocation(country,province,city))
        {
            return new ResultVO(false,
                                ((province != null && province.trim().length() > 0 && !province.trim().equals("0"))?
                                        "Invalid combination of country&#44; state&#47;province and city.":
                                        "Invalid combination of country and city."),
                                "country");
        }

        return new ResultVO(true).setValue(country);
    }

    /**
     * to validate province or state
     * @param province
     * @param reqData
     * @return
     */
    public ResultVO validateProvince(String province, Map <String, String[]> reqData)
    {
        if (province == null || province.trim().length() == 0 || province.trim().equals("0"))
        {
            return new ResultVO(true).setValue(null);
        }

        String country = (reqData.get("country") != null ? reqData.get("country")[0] : "");
        String stateByName = locDao.getValidStateByNameAndCountryCode(province,country);

        if (stateByName != null && stateByName.trim().length() > 0)
        {
            return new ResultVO(true).setValue(stateByName);
        }
        else
        {
            return new ResultVO(false, "Invalid combination of country and state&#47;province.", "province");
        }
    }

    /**
     * to validate city
     * @param city
     * @param reqData
     * @return
     */
    public ResultVO validateCity(String city, Map <String, String[]> reqData)
    {
        if (city == null || city.trim().length() == 0 || city.trim().equals("0"))
        {
            return new ResultVO(true).setValue(null);
        }

        String country = (reqData.get("country") != null ? reqData.get("country")[0] : "");
        String cityByName = locDao.getValidCityByNameAndCountryCode(city,country);

        if (cityByName != null && cityByName.trim().length() > 0)
        {
            return new ResultVO(true).setValue(cityByName);
        }
        else
        {
            return new ResultVO(false, "Invalid combination of country and city.", "city");
        }
    }

}
