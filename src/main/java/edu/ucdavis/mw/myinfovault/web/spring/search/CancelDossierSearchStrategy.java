/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: CancelDossierSearchStrategy.java
 */

package edu.ucdavis.mw.myinfovault.web.spring.search;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import edu.ucdavis.mw.myinfovault.domain.action.DossierLocation;
import edu.ucdavis.mw.myinfovault.domain.dossier.Dossier;
import edu.ucdavis.mw.myinfovault.service.dossier.DossierAppointmentType;
import edu.ucdavis.mw.myinfovault.service.dossier.WorkflowException;
import edu.ucdavis.mw.myinfovault.service.person.MivRole;
import edu.ucdavis.myinfovault.MIVUser;

/**
 * TODO: javadoc
 * 
 * @author Rick Hendricks
 * @since MIV ?
 */
public class CancelDossierSearchStrategy extends  ManageOpenActionSearchStrategy
{
    /**
     * Get the Dossiers that are at the given location for the given user.
     *
     * @param user who will be accessing the dossiers
     * @param location dossier location
     * @return List of MivActionList items
     */
    @Override
    public List<MivActionList> getDossiersAtLocation(MIVUser user, DossierLocation location)
    {
        // Only the Vice Provost Staff role will have any items returned
        return user.getTargetUserInfo().getPerson().hasRole(MivRole.VICE_PROVOST_STAFF)
             ? super.getDossiersAtLocation(user, location)//vp staff
             : Collections.<MivActionList>emptyList();//not vp staff, return empty list
    }

    /**
     * Get all the dossiers/actions by specified criteria
     * 
     * @param criteria from search form containing the search "criteria" requested
     * @param user MIVUser requesting the search
     * @return List of MivActionList objects
     */
    public List<MivActionList> dossierSearch(SearchCriteria criteria, MIVUser user, List<Dossier>dossiers) throws WorkflowException
    {
        List<MivActionList> allDossiers = super.dossierSearch(criteria, user, dossiers);
        
        //remove the joint appointments from the display
        List<MivActionList> removeJoint = new ArrayList<MivActionList>();
        for (MivActionList action : allDossiers)
        {
            if (action.getAppointmentType().equals(DossierAppointmentType.JOINT.shortDescription))
            {
                removeJoint.add(action);
            }
        }
        allDossiers.removeAll(removeJoint);
        
        return allDossiers;
        
    }
}
