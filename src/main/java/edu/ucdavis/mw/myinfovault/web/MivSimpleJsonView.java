/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: MivSimpleJsonView.java
 */
package edu.ucdavis.mw.myinfovault.web;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.web.servlet.View;
import org.springframework.webflow.engine.RequestControlContext;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;


/**
 * Serializes the model to JSON and outputs it as the HttpServletResponse.
 *
 * @author Jacob Saporito
 * @since 4.8.6
 */
public class MivSimpleJsonView implements View
{
    private static final Set<Class<?>> unserializableClasses = new HashSet<Class<?>>(Arrays.asList(BeanPropertyBindingResult.class,
                                                                                                   RequestControlContext.class));

    /* (non-Javadoc)
     * @see org.springframework.web.servlet.View#getContentType()
     */
    @Override
    public String getContentType()
    {
        return "application/json";
    }

    /* (non-Javadoc)
     * @see org.springframework.web.servlet.View#render(java.util.Map, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    @Override
    public void render(Map<String, ?> model, HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);

        HashMap<String, Object> modelCopy = new HashMap<>();

        for (String key : model.keySet())
        {
            Object value = model.get(key);
            if (!isClassUnserializable(value.getClass()))
            {
                modelCopy.put(key, value);
            }
        }

        String json = mapper.writeValueAsString(modelCopy);
        response.getOutputStream().print(json);

        return;
    }

    /**
     * Checks clazz against a list of known problematic classes/interfaces to see if we should strip it out before serialization.
     *
     * @param clazz The class of the object that we want to see if we can serialize.
     * @return Whether class is unserializable (and thus should be filtered out)
     */
    private boolean isClassUnserializable(Class<?> clazz)
    {
        boolean interfaceUnserializable = false;

        for (Class<?> interfaze : clazz.getInterfaces())
        {
            if (unserializableClasses.contains(interfaze))
            {
                interfaceUnserializable = true;
                break;
            }
        }

        return unserializableClasses.contains(clazz) || interfaceUnserializable;
    }

}
