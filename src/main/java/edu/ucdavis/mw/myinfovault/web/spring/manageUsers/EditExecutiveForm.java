/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: EditExecutiveForm.java
 */

package edu.ucdavis.mw.myinfovault.web.spring.manageUsers;

import java.io.Serializable;
import java.util.List;

import edu.ucdavis.mw.myinfovault.service.person.MivPerson;

/**
 * TODO: add java doc
 *
 * @author Mary Northup
 * @since MIV 4.8.2
 */
public class EditExecutiveForm implements Serializable
{
    private static final long serialVersionUID = 201403251309L;

    /*
     * Executive persons displayed on the form.
     */
    private MivPerson viceProvost = null;
    private MivPerson provost = null;
    private MivPerson chancellor = null;

    /*
     * The following IDs are sent to and from the page for person selection.
     */
    private int viceProvostID = 0;
    private int provostID = 0;
    private int chancellorID = 0;

    /**
     * Create executive form bean.
     *
     * @param viceProvosts users with the vice provost role
     * @param provosts users with the provost role
     * @param chancellors users with the chancellor role
     */
    public EditExecutiveForm(List<MivPerson> viceProvosts,
                             List<MivPerson> provosts,
                             List<MivPerson> chancellors)
    {
        // if at least on vice provost
        if (viceProvosts.size() > 0)
        {
            this.viceProvost = viceProvosts.get(0);
            this.viceProvostID = this.viceProvost.getUserId();
        }

        // if at least one provost
        if (provosts.size() > 0)
        {
            this.provost = provosts.get(0);
            this.provostID = this.provost.getUserId();
        }

        // if at least on chancellor
        if (chancellors.size() > 0)
        {
            this.chancellor = chancellors.get(0);
            this.chancellorID = this.chancellor.getUserId();
        }
    }

    /**
     * @return Vice Provost user ID
     */
    public int getViceProvostID()
    {
        return this.viceProvostID;
    }

    /**
     * @return Provost user ID
     */
    public int getProvostID()
    {
        return this.provostID;
    }

    /**
     * @return Chancellor user ID
     */
    public int getChancellorID()
    {
        return this.chancellorID;
    }

    /**
     * @param viceProvostID Vice Provost user ID
     */
    public void setViceProvostID(int viceProvostID)
    {
        this.viceProvostID = viceProvostID;
    }

    /**
     * @param provostID Provost user ID
     */
    public void setProvostID(int provostID)
    {
        this.provostID = provostID;
    }

    /**
     * @param chancellorID Chancellor user ID
     */
    public void setChancellorID(int chancellorID)
    {
        this.chancellorID = chancellorID;
    }

    /**
     * @param viceProvost Vice Provost person
     */
    public void setViceProvost(MivPerson viceProvost)
    {
        this.viceProvost = viceProvost;
    }

    /**
     * @param provost Provost person
     */
    public void setProvost(MivPerson provost)
    {
        this.provost = provost;
    }

    /**
     * @param chancellor Chancellor person
     */
    public void setChancellor(MivPerson chancellor)
    {
        this.chancellor = chancellor;
    }

    /**
     * @return Vice Provost person
     */
    public MivPerson getViceProvost()
    {
        return viceProvost;
    }

    /**
     * @return Provost person
     */
    public MivPerson getProvost()
    {
        return provost;
    }

    /**
     * @return Chancellor person
     */
    public MivPerson getChancellor()
    {
        return chancellor;
    }
}
