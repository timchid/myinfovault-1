/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: SignatureAction.java
 */

package edu.ucdavis.mw.myinfovault.web.spring.signature;

import org.springframework.webflow.execution.RequestContext;

import edu.ucdavis.mw.myinfovault.domain.signature.MivElectronicSignature;
import edu.ucdavis.mw.myinfovault.events.EventDispatcher;
import edu.ucdavis.mw.myinfovault.events.SignatureEvent;
import edu.ucdavis.mw.myinfovault.service.authorization.AuthorizationService;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.mw.myinfovault.service.person.UserService;
import edu.ucdavis.mw.myinfovault.service.signature.SigningService;
import edu.ucdavis.mw.myinfovault.web.spring.commons.MivFormAction;

/**
 * Webflow form action for electronic signatures.
 *
 * @author Craig Gilmore
 * @since 3.0
 */
public abstract class SignatureAction extends MivFormAction
{
    /**
     * Spring injected signature service.
     */
    protected SigningService signingService;

    /**
     * Create the signature action form bean.
     *
     * @param userService Spring injected user service
     * @param authorizationService Spring injected authorization service
     * @param signingService Spring injected signature service
     */
    public SignatureAction(UserService userService,
                           AuthorizationService authorizationService,
                           SigningService signingService)
    {
        super(userService, authorizationService);

        this.signingService = signingService;
    }

    /**
     * Sign a signable document.
     *
     * @param realPerson real person siging
     * @param signature MIV signature to sign
     * @return if signed successfully
     */
    protected boolean sign(MivPerson realPerson, MivElectronicSignature signature)
    {
        // sign, persist document
        signingService.sign(signature, realPerson.getUserId());

        // if signature is signed and valid
        if (signature.isValid())
        {
            // log signature
            logger.info("_DC_AUDIT" + signature);

            // post signature event
            EventDispatcher.getDispatcher().post(new SignatureEvent(signature));

            return true;// success
        }

        return false;// neither signed nor valid
    }

    /**
     * Add signature action bread crumb.
     *
     * @param context Webflow request context
     */
    public void addBreadcrumb(RequestContext context)
    {
        SignatureForm form = getFormObject(context);

        addBreadcrumbs(context, (form.getSignature().isSigned() ? "Edit " : "Add ") + form.getSignature().getDocument().getDocumentType().getDescription());
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.web.spring.commons.MivFormAction#getFormObject(org.springframework.webflow.execution.RequestContext)
     */
    @Override
    public SignatureForm getFormObject(RequestContext context)
    {
        return (SignatureForm) super.getFormObject(context);
    }
}
