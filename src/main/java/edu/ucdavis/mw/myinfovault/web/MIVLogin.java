/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: MIVLogin.java
 */

package edu.ucdavis.mw.myinfovault.web;

import java.io.IOException;
import java.text.MessageFormat;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.ucdavis.mw.myinfovault.service.MivServiceLocator;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.myinfovault.MIVSession;
import edu.ucdavis.myinfovault.MIVUser;


/**
 * Login Servlet
 *
 */
public class MIVLogin extends HttpServlet // this intentionally does _not_ extend MIVServlet
{
    private static final long serialVersionUID = 200805231650L;

    private static final Logger logger = LoggerFactory.getLogger(MIVLogin.class);

    private static final String LOGGING_ON = "Logged on {0} with roles {1} [primary = {2}]\n\tUser-Agent: [{3}]";
    private MivActivityMonitor am;


    @Override
    public void init(ServletConfig config) throws ServletException
    {
        am = MivActivityMonitor.getMonitor();
        super.init(config);
    }


    /**
     * Get the MIVSession object from the request.
     * Redirect to the main MIV page after making sure a session is created.
     */
    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException
    {
        // Get a new MIVSession
        MIVSession session = MIVSession.getSession(request, true);

        final MIVUser user = session.getUser();
        final MivPerson loginPerson = user.getUserInfo().getPerson();

// SDP 2015-06-26 Moved 'freshen' to be done at login
        MivServiceLocator.getUserService().freshen( loginPerson, /*force=*/true );


        // log the logging-on
        logger.info(MessageFormat.format(LOGGING_ON,
                                         loginPerson,
                                         loginPerson.getAssignedRoles(),
                                         loginPerson.getPrimaryRoles().get(0),
                                         request.getHeader("User-Agent")));
        logger.info("Client IP address: {}", request.getRemoteAddr());

// SDP 2012-06-08 Added experimental session timeout handler
        SessionTimeoutHandler handler = new SessionTimeoutHandler(user.getUserId(), user.getLoginName());
        request.getSession().setAttribute("theUserTimeoutHandler", handler);
        am.logLogin( user.getLoginName() );

        // Redirect the response to MIVMain
        try {
            response.sendRedirect(request.getContextPath() + "/MIVMain");
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException
    {
        doGet(request, response);
    }
}
