/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: AcademicActionForm.java
 */

package edu.ucdavis.mw.myinfovault.web.spring.action;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.EnumSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.util.AutoPopulatingList;
import org.springframework.util.AutoPopulatingList.ElementInstantiationException;

import edu.ucdavis.mw.myinfovault.domain.PolarResponse;
import edu.ucdavis.mw.myinfovault.domain.action.AppointmentDuration;
import edu.ucdavis.mw.myinfovault.domain.action.Assignment;
import edu.ucdavis.mw.myinfovault.domain.action.DossierActionType;
import edu.ucdavis.mw.myinfovault.domain.action.DossierDelegationAuthority;
import edu.ucdavis.mw.myinfovault.domain.action.DossierLocation;
import edu.ucdavis.mw.myinfovault.domain.action.SalaryPeriod;
import edu.ucdavis.mw.myinfovault.domain.action.StatusType;
import edu.ucdavis.mw.myinfovault.domain.action.Title;
import edu.ucdavis.mw.myinfovault.domain.dossier.Dossier;
import edu.ucdavis.mw.myinfovault.domain.raf.AcademicActionBo;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.mw.myinfovault.service.person.Scope;
import edu.ucdavis.mw.myinfovault.util.DateUtil;
import edu.ucdavis.mw.myinfovault.util.StringUtil;
import edu.ucdavis.myinfovault.data.DateParser;

/**
 * Academic action form backing.
 *
 * @author Craig Gilmore
 * @since MIV 4.7.1
 */
public class AcademicActionForm implements Serializable
{
    private static final long serialVersionUID = 201305171336L;

    private static final ThreadLocal<DateFormat> dateFormat =
            new ThreadLocal<DateFormat>() {
                @Override protected DateFormat initialValue() {
                    return new SimpleDateFormat("yyyy-MM-dd");
                }
            };

    private DossierLocation state = null;

    private MivPerson candidate = null;
    private DossierActionType actionType = null;
    private DossierDelegationAuthority delegationAuthority = null;
    private Date dateEffectiveDate = DateUtil.getAcademicYear().getTime();
    private Date dateRetroactiveDate = null;
    private Date dateEndDate = null;
    private String effectiveDate;
    private String retroactiveDate;
    private String endDate;
    private int accelerationYears;

    /*
     * New appointment fields.
     */
    private int mivUserID = 0;
    private String ldapPersonUUID = StringUtil.EMPTY_STRING;
    private String login = StringUtil.EMPTY_STRING;
    private String givenName = StringUtil.EMPTY_STRING;
    private String middleName = StringUtil.EMPTY_STRING;
    private String surname = StringUtil.EMPTY_STRING;
    private PolarResponse currentEmployee = PolarResponse.NA;
    private PolarResponse representedEmployee = PolarResponse.NA;
    private PolarResponse unionNoticeRequired = PolarResponse.NA;
    private PolarResponse laborRelationsNotified = PolarResponse.NA;

    private final AssignmentFactory factory = new AssignmentFactory();

    private Map<StatusType, List<Assignment>> assignmentStatuses = new LinkedHashMap<StatusType, List<Assignment>>(2, 1f)
    {
        private static final long serialVersionUID = 201310291459L;
        {
            put(StatusType.PRESENT, new AutoPopulatingList<Assignment>(factory));
            put(StatusType.PROPOSED, new AutoPopulatingList<Assignment>(factory));
        }
    };

    private final static EnumSet<DossierActionType> appointeeActionTypes = EnumSet.of(DossierActionType.APPOINTMENT);
    private final static EnumSet<DossierActionType> candidateActionTypes = EnumSet.complementOf(appointeeActionTypes);

    /**
     * Create academic action form backing instance.
     *
     * @param actionType action type
     * @param delegationAuthority delegation of authority
     */
    private AcademicActionForm(DossierActionType actionType, DossierDelegationAuthority delegationAuthority)
    {
        this.actionType = actionType;
        this.delegationAuthority = delegationAuthority;
    }

    /**
     * Create a new, blank academic action form backing instance.
     *
     * @param actionType initial action type
     */
    public AcademicActionForm(DossierActionType actionType)
    {
        this(actionType, actionType.getDefaultDelegationAuthority());

        /*
         * Initial blank assignments.
         */
        assignmentStatuses.get(StatusType.PRESENT).add(factory.createElement(0));
        assignmentStatuses.get(StatusType.PROPOSED).add(factory.createElement(0));
    }

    /**
     * Create an academic action form backing instance for an existing dossier.
     *
     * @param dossier dossier from which the academic action form is loaded. Must not be <code>null</code>.
     */
    protected AcademicActionForm(Dossier dossier)
    {
        this(dossier.getAction().getActionType(), dossier.getAction().getDelegationAuthority());

        this.state = dossier.getDossierLocation();
        this.candidate = dossier.getAction().getCandidate();
        this.dateRetroactiveDate = dossier.getAction().getRetroactiveDate();
        this.retroactiveDate = this.dateRetroactiveDate == null ? "" : dateFormat.get().format(this.dateRetroactiveDate);
        this.dateEndDate = dossier.getAction().getEndDate();
        this.endDate = this.dateEndDate == null ? StringUtils.EMPTY : dateFormat.get().format(this.dateEndDate);
        this.dateEffectiveDate = dossier.getAction().getEffectiveDate();
        this.effectiveDate = this.dateEffectiveDate == null ? "" : dateFormat.get().format(this.dateEffectiveDate);
        this.accelerationYears = dossier.getAction().getAccelerationYears();
        this.mivUserID = dossier.getAction().getCandidate().getUserId();
        this.ldapPersonUUID = dossier.getAction().getCandidate().getPersonId();
        // FIXME: person.getPrincipal() is documented to return NULL in some cases. The next line may throw a NullPointerException.
        this.login = dossier.getAction().getCandidate().getPrincipal().getPrincipalName();
        this.givenName = dossier.getAction().getCandidate().getGivenName();
        this.middleName = dossier.getAction().getCandidate().getMiddleName();
        this.surname = dossier.getAction().getCandidate().getSurname();
    }

    /**
     * Create an academic action form backing instance for an existing academic action.
     *
     * @param aaBo academic action business object
     */
    public AcademicActionForm(AcademicActionBo aaBo)
    {
        this(aaBo.getDossier());

        // Get the appointment details
        this.currentEmployee = aaBo.getApptDetails().getCurrentEmployee();
        this.representedEmployee = aaBo.getApptDetails().getRepresented();
        this.unionNoticeRequired = aaBo.getApptDetails().getNoticeToUnion();
        this.laborRelationsNotified = aaBo.getApptDetails().getNoticeToLaborRelations();

        // Get the AssignmentStatuses
        for (StatusType statusType : aaBo.getAssignmentStatuses().keySet())
        {
            List<Assignment> currentAssignments = aaBo.getAssignmentStatuses().get(statusType);

            if (currentAssignments.isEmpty())
            {
                // There must be at least one present status assignment, though it may be hidden on the form
                assignmentStatuses.get(statusType).add(factory.createElement(0));
            }
            else
            {
                // change title lists for all assignments to auto-populating
                for (Assignment assignment : currentAssignments)
                {
                    if (assignment.getTitles().isEmpty())
                    {
                        // There must be at least one title for each assignment
                        // Default title percent of time to match parent assignment
                        assignment.getTitles().add(new Title(assignment.getPercentOfTime()));
                    }

                    assignment.setTitles(new AutoPopulatingList<Title>(assignment.getTitles(), Title.class));
                }

                assignmentStatuses.get(statusType).addAll(currentAssignments);
            }
        }
    }

    /**
     * @return if the academic action is at the vice provost office
     */
    public boolean isViceProvost()
    {
        return DossierLocation.mapLocationToOffice(state) == DossierLocation.VICEPROVOST;
    }

    /**
     * @return if the academic action is at the school office
     */
    public boolean isSchool()
    {
        return DossierLocation.mapLocationToOffice(state) == DossierLocation.SCHOOL;
    }

    /**
     * @return if the academic action is in the department state or a new action
     */
    public boolean isDepartment()
    {
        return getState() == null || DossierLocation.DEPARTMENT == getState();
    }

    /**
     * @return if the academic action is in the packetrequest state or a new action
     */
    public boolean isPacketRequest()
    {
        return getState() == null || DossierLocation.PACKETREQUEST == getState();
    }

    /**
     * @return state of the academic action (dossier location)
     */
    public DossierLocation getState()
    {
        return state;
    }

    /**
     * @return candidate for this academic action
     */
    public MivPerson getCandidate()
    {
        return candidate;
    }

    /**
     * @param candidate candidate for this academic action
     */
    public void setCandidate(MivPerson candidate)
    {
        this.candidate = candidate;
    }

    /**
     * @return action type
     */
    public DossierActionType getActionType()
    {
        return actionType;
    }

    /**
     * @param actionType action type
     */
    public void setActionType(DossierActionType actionType)
    {
        this.actionType = actionType;
    }

    /**
     * @return delegation of authority
     */
    public DossierDelegationAuthority getDelegationAuthority()
    {
        return delegationAuthority;
    }

    /**
     * @param delegationAuthority delegation of authority
     */
    public void setDelegationAuthority(DossierDelegationAuthority delegationAuthority)
    {
        this.delegationAuthority = delegationAuthority;
    }

    /**
     * @return effective date
     */
    public String getEffectiveDate()
    {
        return this.effectiveDate;
    }

    /**
     * @return effective date
     */
    public Date getEffectiveDateAsDate()
    {
        return this.dateEffectiveDate;
    }

    /**
     * @param effectiveDate effective date
     */
    public void setEffectiveDate(String effectiveDate)
    {
        /*Date potentialDate = DateParser.parse(effectiveDate);
        System.out.println("setting effective date to string ["+effectiveDate+"] which gives a Date of ["+potentialDate+"]");*/

        this.effectiveDate = effectiveDate.trim();
        if (this.effectiveDate.length() > 0) {
            this.dateEffectiveDate = DateParser.parse(effectiveDate);
            if (this.dateEffectiveDate != null) {
                this.effectiveDate = dateFormat.get().format(this.dateEffectiveDate);
            }
        }
    }

    /**
     * @return retroactive date
     */
    public String getRetroactiveDate()
    {
        return this.retroactiveDate;
    }

    /**
     * @return retroactive date
     */
    public Date getRetroactiveDateAsDate()
    {
        return this.dateRetroactiveDate;
    }

    /**
     * @param retroactiveDate retroactive date
     */
    public void setRetroactiveDate(String retroactiveDate)
    {
        /*Date potentialDate = DateParser.parse(retroactiveDate);
        System.out.println("setting retroactive date to string ["+retroactiveDate+"] which gives a Date of ["+potentialDate+"]");*/

        this.retroactiveDate = retroactiveDate.trim();
        this.dateRetroactiveDate = DateParser.parse(retroactiveDate);
        if (this.dateRetroactiveDate != null) {
            this.retroactiveDate = dateFormat.get().format(this.dateRetroactiveDate);
        }
    }

    /**
     * @return action ending date
     */
    public String getEndDate()
    {
        return this.endDate;
    }

    /**
     * @return action ending date
     */
    public Date getEndDateAsDate()
    {
        return this.dateEndDate;
    }

    /**
     * @param endDate action ending date
     */
    public void setEndDate(String endDate)
    {
        this.endDate = endDate.trim();
        this.dateEndDate = DateParser.parse(endDate);
        if (this.dateEndDate != null) {
            this.endDate = dateFormat.get().format(this.dateEndDate);
        }
    }

    /**
     * @return ID of the MIV user to bind as candidate
     */
    public int getMivUserID()
    {
        return mivUserID;
    }

    /**
     * @return acceleration years
     */
    public int getAccelerationYears()
    {
        return this.accelerationYears;
    }

    /**
     * @param accelerationYears acceleration years
     */
    public void setAccelerationYears(int accelerationYears)
    {
        this.accelerationYears = accelerationYears;
    }

    /**
     * @param mivUserID ID of the MIV user to bind as candidate
     */
    public void setMivUserID(int mivUserID)
    {
        this.mivUserID = mivUserID;
    }

    /**
     * @return LDAP person UUID
     */
    public String getLdapPersonUUID()
    {
        return ldapPersonUUID;
    }

    /**
     * @param ldapPersonUUID LDAP person UUID
     */
    public void setLdapPersonUUID(String ldapPersonUUID)
    {
        this.ldapPersonUUID = ldapPersonUUID;
    }

    /**
     * @return given name of the new person to bind as candidate
     */
    public String getGivenName()
    {
        return givenName;
    }

    /**
     * @param givenName given name of the new person to bind as candidate
     */
    public void setGivenName(String givenName)
    {
        this.givenName = givenName;
    }

    /**
     * @return middle name of the new person to bind as candidate
     */
    public String getMiddleName()
    {
        return middleName;
    }

    /**
     * @param middleName middle name of the new person to bind as candidate
     */
    public void setMiddleName(String middleName)
    {
        this.middleName = middleName;
    }

    /**
     * @return surname of the new person to bind as candidate
     */
    public String getSurname()
    {
        return surname;
    }

    /**
     * @param surname surname of the new person to bind as candidate
     */
    public void setSurname(String surname)
    {
        this.surname = surname;
    }

    /**
     * @return response to "is candidate a current employee?"
     */
    public PolarResponse getCurrentEmployee()
    {
        return currentEmployee;
    }

    /**
     * @param currentEmployee response to "is candidate a current employee?"
     */
    public void setCurrentEmployee(PolarResponse currentEmployee)
    {
        this.currentEmployee = currentEmployee;
    }

    /**
     * @return response to "is candidate a represented employee?"
     */
    public PolarResponse getRepresentedEmployee()
    {
        return representedEmployee;
    }

    /**
     * @param representedEmployee response to "is candidate a represented employee?"
     */
    public void setRepresentedEmployee(PolarResponse representedEmployee)
    {
        this.representedEmployee = representedEmployee;
    }

    /**
     * @return response to "is a union notice required?"
     */
    public PolarResponse getUnionNoticeRequired()
    {
        return unionNoticeRequired;
    }

    /**
     * @param unionNoticeRequired response to "is a union notice required?"
     */
    public void setUnionNoticeRequired(PolarResponse unionNoticeRequired)
    {
        this.unionNoticeRequired = unionNoticeRequired;
    }

    /**
     * @return response to "has labor relations been notified?"
     */
    public PolarResponse getLaborRelationsNotified()
    {
        return laborRelationsNotified;
    }

    /**
     * @param laborRelationsNotified response to "has labor relations been notified?"
     */
    public void setLaborRelationsNotified(PolarResponse laborRelationsNotified)
    {
        this.laborRelationsNotified = laborRelationsNotified;
    }

    /**
     * @return map with present and proposed assignments for this academic action
     */
    public Map<StatusType, List<Assignment>> getAssignmentStatuses()
    {
        return this.assignmentStatuses;
    }

    /**
     * @return the array of allowed dossier action types
     */
    public EnumSet<DossierActionType> getDossierActionTypes()
    {
        /*
         * If action is Appointment then only Appointment is available,
         * otherwise everything except appointment is available.
         */
        return this.actionType == DossierActionType.APPOINTMENT
             ? AcademicActionForm.appointeeActionTypes
             : AcademicActionForm.candidateActionTypes;
    }

    /**
     * @return the array of allowed dossier delegation authority types
     */
    public DossierDelegationAuthority[] getDossierDelegationAuthorities()
    {
        return this.actionType.getValidDelegationAuthorities();
    }

    /**
     * @return allowed polar responses
     */
    public EnumSet<PolarResponse> getPolarResponses()
    {
        return EnumSet.of(PolarResponse.YES, PolarResponse.NO);
    }

    /**
     * @return allowed appointment durations
     */
    public EnumSet<AppointmentDuration> getAppointmentDurations()
    {
        return EnumSet.complementOf(EnumSet.of(AppointmentDuration.NA));
    }

    /**
     * @return allowed salary periods
     */
    public EnumSet<SalaryPeriod> getSalaryPeriods()
    {
        return EnumSet.allOf(SalaryPeriod.class);
    }

    /**
     * Builds new {@link Assignment} objects with {@link AutoPopulatingList} of appointment titles.
     *
     * @author Craig Gilmore
     * @since MIV 4.8
     */
    private class AssignmentFactory implements AutoPopulatingList.ElementFactory<Assignment>, Serializable
    {
        private static final long serialVersionUID = 201310071357L;

        /*
         * (non-Javadoc)
         * @see org.springframework.util.AutoPopulatingList.ElementFactory#createElement(int)
         */
        @Override
        public Assignment createElement(int index) throws ElementInstantiationException
        {
            // new assignment must have at least one title
            // Default title percent of time to match parent assignment
            List<Title> titles = new ArrayList<Title>(Collections.singleton(new Title(BigDecimal.ONE)));

            return new Assignment(new AutoPopulatingList<Title>(titles, Title.class),
                                  index <= 0,
                                  Scope.NoScope,
                                  BigDecimal.ONE);
        }

    }

    /**
     * @return the login
     */
    public String getLogin()
    {
        return login;
    }

    /**
     * @param login the login to set
     */
    public void setLogin(String login)
    {
        this.login = login;
    }
}
