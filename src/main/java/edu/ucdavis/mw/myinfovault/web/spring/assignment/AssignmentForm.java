/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: AssignmentForm.java
 */

package edu.ucdavis.mw.myinfovault.web.spring.assignment;

import java.io.Serializable;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import org.apache.commons.lang.ArrayUtils;

import edu.ucdavis.mw.myinfovault.domain.group.Group;
import edu.ucdavis.mw.myinfovault.domain.group.GroupComparator;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.mw.myinfovault.web.spring.search.SearchCriteria;

/**
 * Supports the MivPerson assignment aspect of the group and assign reviewer form backing objects.
 *
 * @author Craig Gilmore
 * @since 4.3
 */
public abstract class AssignmentForm extends SearchCriteria implements Serializable
{
    private static final long serialVersionUID = 201110031608L;

    private SortedSet<MivPerson> assignedPeople;
    private Set<MivPerson> inactivePeople;
    private SortedSet<Group> assignedGroups;
    private SortedSet<MivPerson> availablePeople;
    private SortedSet<Group> availableGroups;
    private MivPerson actingPerson;
    private int[] removePeople = {};
    private int[] addPeople = {};
    private int[] removeGroups = {};
    private int[] addGroups = {};
    private String[] activateGroups = {};
    private String[] deactivateGroups = {};
    private boolean removeAll = false;

    /**
     * Create form with initially assigned/inactive people
     * and the acting person for this assignment.
     *
     * @param assignedPeople Set of people initially assigned
     * @param inactivePeople Set of people initially inactive
     * @param actingPerson The switched-to acting person
     */
    public AssignmentForm(SortedSet<MivPerson> assignedPeople,
                          Set<MivPerson> inactivePeople,
                          MivPerson actingPerson)
    {
        this(assignedPeople,
             inactivePeople,
             new TreeSet<Group>(new GroupComparator()),
             actingPerson);
    }

    /**
     * Create form with initially assigned/inactive people/groups
     * and the acting person for this assignment.
     *
     * @param assignedPeople Set of people initially assigned
     * @param inactivePeople Set of people initially inactive
     * @param assignedGroups Set of groups initially assigned
     * @param actingPerson The switched-to acting person
     */
    public AssignmentForm(SortedSet<MivPerson> assignedPeople,
                          Set<MivPerson> inactivePeople,
                          SortedSet<Group> assignedGroups,
                          MivPerson actingPerson)
    {
        this.assignedPeople = assignedPeople;
        this.inactivePeople = inactivePeople;
        this.assignedGroups = assignedGroups;
        this.actingPerson = actingPerson;
    }

    /**
     * Get the set of people assigned in the form.
     *
     * @return Assigned people
     */
    public SortedSet<MivPerson> getAssignedPeople()
    {
        return assignedPeople;
    }

    /**
     * Get the set of people assigned, but inactive in the form.
     *
     * @return Assigned inactive people
     */
    public Set<MivPerson> getInactivePeople()
    {
        return inactivePeople;
    }


    /**
     * Get the set of groups assigned in the form.
     *
     * @return Assigned groups
     */
    public SortedSet<Group> getAssignedGroups()
    {
        return assignedGroups;
    }

    /**
     * Get the set of groups assigned in the form.
     *
     * @return Assigned groups
     */
    public SortedSet<MivPerson> getAvailablePeople()
    {
        return availablePeople;
    }

    /**
     * Set the set of available people in the form.
     *
     * @param availablePeople
     */
    public void setAvailablePeople(SortedSet<MivPerson> availablePeople)
    {
        this.availablePeople = availablePeople;
    }

    /**
     * Get the set of available groups in the form.
     *
     * @return availableGroups
     */
    public SortedSet<Group> getAvailableGroups()
    {
        return availableGroups;
    }

    /**
     * Set the set of available groups in the form
     *
     * @param availableGroups
     */
    public void setAvailableGroups(SortedSet<Group> availableGroups)
    {
        this.availableGroups = availableGroups;
    }

    /**
     * Get the person acting on this assignment.
     *
     * @return The acting MIV person
     */
    public MivPerson getActingPerson()
    {
        return actingPerson;
    }

    /**
     * Get user IDs of people to remove from the assigned collection.
     *
     * @return User IDs to remove
     */
    public int[] getRemovePeople()
    {
        return removePeople;
    }

    /**
     * Set the user IDs of people to remove from the assigned collection.
     *
     * @param removePeople user IDs to remove
     */
    public void setRemovePeople(int[] removePeople)
    {
        this.removePeople = removePeople;
    }

    /**
     * Get user IDs of people to add to the assigned collection.
     *
     * @return User IDs to add
     */
    public int[] getAddPeople()
    {
        return addPeople;
    }

    /**
     * Set the user IDs of people to add to the assigned collection.
     *
     * @param addPeople user IDs to add
     */
    public void setAddPeople(int[] addPeople)
    {
        this.addPeople = addPeople;
    }

    /**
     * Get group IDs of groups to remove from the assigned collection.
     *
     * @return Group IDs to remove
     */
    public int[] getRemoveGroups()
    {
        return removeGroups;
    }

    /**
     * Set the group IDs of groups to remove from the assigned collection.
     *
     * @param removeGroups group IDs to remove
     */
    public void setRemoveGroups(int[] removeGroups)
    {
        this.removeGroups = removeGroups;
    }

    /**
     * Get group IDs of groups whose members are to be added to the assigned collection.
     *
     * @return Group IDs to add
     */
    public int[] getAddGroups()
    {
        return addGroups;
    }

    /**
     * Set the group IDs of groups whose members are to be added to the assigned collection.
     *
     * @param addGroups group IDs to add
     */
    public void setAddGroups(int[] addGroups)
    {
        this.addGroups = addGroups;
    }

    /**
     * Get group and user IDs of people in groups to activate in the assigned collection.
     *
     * @return Array of strings in the form "<GROUPID>:<USERID>" corresponding
     *         to an assigned group and one of its members
     */
    public String[] getActivateGroups()
    {
        return activateGroups;
    }

    /**
     * Set group and user IDs of people in groups to activate in the assigned collection.
     *
     * @param activateGroups Array of strings in the form "<GROUPID>:<USERID>" corresponding
     *                       to an assigned group and one of its members
     */
    public void setActivateGroups(String[] activateGroups)
    {
        this.activateGroups = activateGroups;
    }

    /**
     * Get group and user IDs of people in groups to deactivate in the assigned collection.
     *
     * @return Array of strings in the form "<GROUPID>:<USERID>" corresponding
     *         to an assigned group and one of its members
     */
    public String[] getDeactivateGroups()
    {
        return deactivateGroups;
    }

    /**
     * Set group and user IDs of people in groups to deactivate in the assigned collection.
     *
     * @param deactivateGroups strings in the form "<GROUPID>:<USERID>" corresponding
     * to an assigned group and one of its members
     */
    public void setDeactivateGroups(String[] deactivateGroups)
    {
        this.deactivateGroups = deactivateGroups;
    }

    /**
     * Should all assigned be removed?
     *
     * @return <code>true</code> if all assigned should be removed, <code>false</code> otherwise
     */
    public boolean isRemoveAll()
    {
        return removeAll;
    }

    /**
     * Set if all assigned should be removed.
     *
     * @param removeAll <code>true</code> if all assigned should be removed, <code>false</code> otherwise
     */
    public void setRemoveAll(boolean removeAll)
    {
        this.removeAll = removeAll;
    }

    /**
     * Resets the fields for binding assignment changes.
     */
    public void reset()
    {
        removePeople = ArrayUtils.EMPTY_INT_ARRAY;
        addPeople = ArrayUtils.EMPTY_INT_ARRAY;
        removeGroups = ArrayUtils.EMPTY_INT_ARRAY;
        addGroups = ArrayUtils.EMPTY_INT_ARRAY;
        activateGroups = ArrayUtils.EMPTY_STRING_ARRAY;
        deactivateGroups = ArrayUtils.EMPTY_STRING_ARRAY;
        removeAll = false;
    }

}
