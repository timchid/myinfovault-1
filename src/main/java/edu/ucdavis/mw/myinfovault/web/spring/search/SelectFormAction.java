/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: SelectFormAction.java
 */

package edu.ucdavis.mw.myinfovault.web.spring.search;

import javax.servlet.http.HttpServletRequest;

import org.springframework.util.StringUtils;
import org.springframework.webflow.execution.Event;
import org.springframework.webflow.execution.RequestContext;

import edu.ucdavis.mw.myinfovault.service.authorization.AuthorizationService;
import edu.ucdavis.mw.myinfovault.service.authorization.AuthorizationService.Qualifier;
import edu.ucdavis.mw.myinfovault.service.authorization.Permission;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.mw.myinfovault.service.person.UserService;
import edu.ucdavis.mw.myinfovault.util.AttributeSet;
import edu.ucdavis.myinfovault.MIVUser;
import edu.ucdavis.myinfovault.MIVUserInfo;
import edu.ucdavis.myinfovault.data.MIVUtil;

/**
 * Contains the methods used to support various actions executed in the web flow.
 *
 * @author Mary Northup
 * @since MIV 3.0
 */
public class SelectFormAction extends SearchFormAction
{
    /**
     *
     * @param searchStrategy
     * @param authorizationService
     * @param userService
     */
    public SelectFormAction(SearchStrategy searchStrategy,
                            AuthorizationService authorizationService,
                            UserService userService)
    {
        super(searchStrategy, authorizationService, userService);
    }

    /**
     * Determine if the user is authorized to use the select user's account page.
     *
     * @param context the context associated with the current web flow.
     * @return WebFlow event status, either success or failure
     */
    @Override
    public Event checkAuthorization(RequestContext context)
    {
        logger.trace("entering " + this.className + ".checkAuthorization");

        MivPerson actor = this.getShadowPerson(context);

        if (authorizationService.hasPermission(actor, Permission.SWITCH_USER, null))
        {
            return success();
        }

        System.out.println("we have unauthorized access");
        logger.warn("User \"" + actor.toString() +  "\" attempted to access MIV Select User page");
        return error();
    }


    /**
     * {@inheritDoc}
     */
    @Override
    boolean isSearchAuthorized(MivPerson person, AttributeSet details, AttributeSet scope)
    {
        return authorizationService.isAuthorized(person, Permission.SWITCH_USER, details, scope);
    }


    /**
     * Used to switch to a new (selected) userId or to return to (switchBack) the original userId.
     * Prior to switching the method verifies that the user has the appropriate AccessLvl to switch to
     * the selected user.  Only by tampering with the request parameters could a user manage to request switching
     * to an unauthorized user, so we return an error and the Web Flow will display an Access Denied page.
     *
     * @param context the context associated with the current web flow.
     * @return either success or failure, used by web flow to control the flow execution.
     */
    public Event switchUser(RequestContext context)
    {
        MIVUser mivUser = this.getUser(context);

        MIVUserInfo currentMui = mivUser.getUserInfo();
        MIVUserInfo targetMui  = mivUser.getTargetUserInfo();

        // If the user is already switched then switch back
        if (targetMui != currentMui)
        {
            mivUser.switchto(null);
            return success();
        }

        HttpServletRequest req = this.getRequest(context);
//        FlowExecutionContext flowContext = context.getFlowExecutionContext();
//        String myStateId = flowContext.getActiveSession().getState().getId();

        // The selected user's MIV User ID number
        String switchTargetParam = MIVUtil.sanitize(req.getParameter("id"));

        if (! StringUtils.hasText(switchTargetParam))
        {
            return error();  // Transitions to CheckAuthorization
        }

        int targetUserID = Integer.MIN_VALUE;

        try {
            targetUserID = Integer.parseInt(switchTargetParam);
        }
        catch (NumberFormatException e) {
            String msg = "Possible Hacking Attempt! Switch-to user ID is not numeric: " + switchTargetParam + "\n" +
                         "   Current Logged-in user: " + currentMui.getDisplayName() +
                         "(" + currentMui.getPerson().getUserId() + ")\n";
            logger.warn(msg);
            mivUser.switchto(null);
        }

        //
        // Now we're ready to try switching to another user
        //
        MivPerson user = mivUser.getUserInfo().getPerson();
        MivPerson target = userService.getPersonByMivId(targetUserID);


        // Check that the target user exists

        if (target == null)
        {
            // bail, not a valid user ID number
            String msg = "Possible Hacking Attempt! User trying to switch to an unknown user:\n" +
                         "   Current Logged-in user: " + user.getDisplayName() +
                         "(" + user.getUserId() + "),  Role: " + user.getPrimaryRoleType() +
                         "   Target User Id: " + targetUserID + "\n" +
                         "";
            logger.warn(msg);
            return error();
        }

        // Check if target person details can be loaded
        if (!target.loadDetail())
        {
            // Put error message in flow scope; picked up by setErrorMessage method
            context.getFlowScope().put("selectErrorMessage", "<strong>"+target.getSortName()+"</strong> is no longer a UC Davis affiliate");
            return no();
        }

        AttributeSet scope = new AttributeSet();
        scope.put(Qualifier.USERID, String.valueOf(target.getUserId()));
        boolean switchAllowed = authorizationService.isAuthorized(user, Permission.SWITCH_USER, null, scope);

        if (!switchAllowed)
        {
            String msg = "Possible Hacking Attempt! User trying to switch to a disallowed person:\n" +
            " Current Logged-in user: " + user.getDisplayName() + "(" + user.getUserId() + ")\n" +
            "   Role: " + user.getPrimaryRoleType() + /*",  Level: " + currentMui.getAccess() +*/ "\n" +
            "   School:" + user.getPrimaryAppointment().getScope().getSchool() +
            "   Dept:" + user.getPrimaryAppointment().getScope().getDepartment() + "\n" +
            " Target User: " + target.getDisplayName() + "(" + target.getUserId() + ")\n" +
            "   Role: " + target.getPrimaryRoleType() + "\n" +
            "   School:" + target.getPrimaryAppointment().getScope().getSchool() +
            "   Dept:" + target.getPrimaryAppointment().getScope().getDepartment() + "\n" +
            "\n" +
            "";
            logger.warn(msg);
            return error();
        }

        mivUser.switchto(targetUserID);

        // put "target" or "targetMui" in MRU
        logger.info("Switching to user {} - displayName : {} \t fullName : {}",
                    new Object[] { targetMui.getPerson().getUserId(), targetMui.getDisplayName(), targetMui.getFullName() }
                    );
        this.getMivSession(context).updateMRU(target);

        return success();
    }


    /**
     * Set error message to request scope if exists in flow scope.
     *
     * @param context WebFlow request context
     * @return WebFlow event status
     */
    public Event setErrorMessage(RequestContext context)
    {
        Object errorMessage = context.getFlowScope().get("selectErrorMessage");

        if (errorMessage != null) {
            context.getRequestScope().put("selectErrorMessage", errorMessage);
        }

        return success();
    }


    /**
     * Add the bread crumb for the previous page in this flow.
     *
     * @param context Webflow request context
     */
    public void addBreadcrumb(RequestContext context)
    {
        addBreadcrumbs(context, "Select a User's Account: Search");
    }

}
