/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: PacketManagementController.java
 */


package edu.ucdavis.mw.myinfovault.web.spring.packet;

import java.sql.Timestamp;
import java.text.MessageFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import edu.ucdavis.mw.myinfovault.domain.packet.Packet;
import edu.ucdavis.mw.myinfovault.domain.packet.PacketRequest;
import edu.ucdavis.mw.myinfovault.service.MivServiceLocator;
import edu.ucdavis.mw.myinfovault.service.packet.PacketService;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.mw.myinfovault.util.StringUtil;
import edu.ucdavis.mw.myinfovault.web.ControllerBase;
import edu.ucdavis.myinfovault.MIVConfig;
import edu.ucdavis.myinfovault.MIVSession;
import edu.ucdavis.myinfovault.MIVUser;
import edu.ucdavis.myinfovault.document.Document;
import edu.ucdavis.myinfovault.document.DocumentFile;
import edu.ucdavis.myinfovault.document.DocumentFormat;


/**
 * Handles user packet management requests.
 * @author Stephen Paulsen
 * @since MIV 5.0
 */
@Controller
public class PacketManagementController extends ControllerBase
{
    /*
     * Mapping paths for
     *  - /miv/packet
     *  - /miv/packet/add
     *  - /miv/packet/edit/{nnnn}       !! Couldn't get this to work.
     *  - /miv/packet/edit?p={nnnn}
     */

    /**
     * Servlet context relative URL to the user's packet management page.
     */
    private static final String PACKET_URL = "/packet";

    /**
     * URL to the create-new-packet controller.
     */
    private static final String ADD_URL = PACKET_URL + "/add";

    private static final String SUBMIT_URL = PACKET_URL + "/submit";

    /**
     * URL to edit an existing packet, with the packetId.
     */
    @SuppressWarnings("unused")
    private static final String EDIT_URL = PACKET_URL + "/edit";
//  private static final String EDIT_URL = PACKET_URL + "/edit" + "/{packetId}"; // I wanted path: /miv/packet/edit/1234 but
                                                                                 // had to settle for: /miv/packet/edit?p=1234

    private static final String DESIGN_URL = "/packet/design";

    private static final String ERROR_VIEW = "helloWorld"; // TODO: Need a real error page. Maybe it's the same packets.jsp showing a message.
    private static final String PREVIEW_VIEW = "packet_preview";

    private static final String MESSAGE_KEY = "message";

    private static final Logger logger = LoggerFactory.getLogger(PacketManagementController.class);

    private final PacketService packetService;

    private static final String BREADCRUMB_GROUP = "packetmanagement";

    /**
     * Comparator to sort packets into order of Most Recently Modified first.
     */
    private static final Comparator<Packet> recentFirst = new Comparator<Packet>()
    {
        @Override
        public int compare(Packet p1, Packet p2)
        {
            return p2.getModifiedTime().compareTo(p1.getModifiedTime()); // reverse compare to get highest (newest) first.
        }
    };

    /**
     * Create a Packet Management Controller with the given Packet Service.
     */
    @Autowired
    public PacketManagementController(final PacketService packetService)
    {
        System.out.println("Inside PacketManagementController constructor, setting packetService. That was easy with @AutoWired.");
        this.packetService = packetService;
    }

    /**
     *
     * @param req
     * @param model
     * @param redirectAttrs
     * @param message
     * @return the View name used to display the packet list/management page.
     */
    @RequestMapping(value=PACKET_URL, method=RequestMethod.GET)
    public String showPackets(
                              final HttpServletRequest req,
                              final ModelMap model,
                              final RedirectAttributes redirectAttrs,
                              @ModelAttribute(MESSAGE_KEY) final String message)
    {
        final MIVSession session = MIVSession.getSession(req);
        final MIVUser currentUser = session.getUser();

        addBreadcrumb("Manage My Packets", BREADCRUMB_GROUP, req);

        logger.debug("'showPacket' on path {}", req.getRequestURI());
        logger.debug("Current user is {}", currentUser.getUserInfo());
        logger.debug("Proxied user is {}", currentUser.getTargetUserInfo());
        //logger.debug("Flash Attributes are: {}", redirectAttrs.getFlashAttributes());
        //logger.debug("Model Attributes are: {}", model);

        final int packetUserId = currentUser.getTargetUserInfo().getPerson().getUserId();

        final List<Packet> packets = packetService.getPackets(packetUserId);
        Collections.sort(packets, recentFirst);
        model.addAttribute("packets", packets);

        // Add packet requests in addition to list of your packets.
        List<PacketRequest> requests = packetService.getOpenPacketRequests(packetUserId);
        model.addAttribute("requests", requests);

        // Make the any-data-was-last-updated timestamp available to the page       //    SELECT UpdateTimestamp FROM MasterTimestamp WHERE UserID=?
        List<Map<String, String>> timeRec = MIVConfig.getConfig().getQueryTool().getList("SELECT * FROM MasterTimestamp WHERE UserID=?", packetUserId+"");
        Timestamp t = Timestamp.valueOf( timeRec.get(0).get("updatetimestamp") );
        model.addAttribute("dtime", t.getTime()/*lastUpdate.getTime()*/);


        String scrollTop = (String) model.get("scrollTop");
        String scrollLeft = (String) model.get("scrollLeft");
        logger.debug("Should scroll browser to Top:{} Left:{}", scrollTop, scrollLeft);

        return "packets";
    }



    /**
     *
     * @param req
     * @param model
     * @param redirectAttrs
     * @param packetName
     * @return
     */
    @RequestMapping(value=ADD_URL, method={ RequestMethod.POST, RequestMethod.GET })
    public ModelAndView addPacket(
                                  final HttpServletRequest req,
                                  final ModelMap model,
                                  final RedirectAttributes redirectAttrs,
                                  @RequestParam(value="packetname", required=true) final String packetName
                                  )
    {
        final MIVUser currentUser = MIVSession.getSession(req).getUser();
        final int proxiedId = currentUser.getTargetUserInfo().getPerson().getUserId();

        logger.debug("running addPacket at the URI {}", req.getRequestURI());

        String dest_url = null;  // Allows me to pick one in the debugger.
        dest_url = DESIGN_URL;
        //  dest_url = PACKET_URL;

        model.remove("authHeader");

        final ModelAndView mav = new ModelAndView(ERROR_VIEW, model);

        if (StringUtils.isBlank(packetName))
        {
            logger.warn("packetName is null, empty, or all whitespace: [{}]", packetName);
            addMessage(model, "The Packet name can not be blank.");
            redirectAttrs.addFlashAttribute(MESSAGE_KEY, model.get(MESSAGE_KEY));
            mav.setView(new RedirectView(PACKET_URL, true));
            return mav;
        }

        String name = packetName.trim();
        logger.debug("Packet Name to create is [{}]", name);
        addMessage(model, "New packet name is ["+name+"]");

        Packet newPacket = new Packet(proxiedId, name);
        logger.info("New packet object is [{}]", newPacket);
        addMessage(model, "New packet object is: ["+newPacket+"]");

        redirectAttrs.addFlashAttribute("packetid", newPacket.getPacketId());
        redirectAttrs.addFlashAttribute("packet", newPacket);
        redirectAttrs.addFlashAttribute("new_packet", newPacket);
        redirectAttrs.addFlashAttribute(MESSAGE_KEY, model.get(MESSAGE_KEY));

        logger.debug("redirectAttrs: {}", redirectAttrs);

        mav.setView(new RedirectView(dest_url, true));

        return mav;
    }



    /**
     *
     * @param req
     * @param model
     * @param redirectAttrs
     * @param packetId
     * @return
     */
    @RequestMapping(value=PACKET_URL, method=RequestMethod.POST, params="edit")
    public ModelAndView editPacket(
                                   final HttpServletRequest req,
                                   final ModelMap model,
                                   final RedirectAttributes redirectAttrs,
                                   @RequestParam(value="edit", required=true) final String packetId
                                   )
    {
        final MIVUser currentUser = MIVSession.getSession(req).getUser();
        final int proxiedId = currentUser.getTargetUserInfo().getPerson().getUserId();

        model.remove("authHeader");

        final ModelAndView mav = new ModelAndView(ERROR_VIEW, model); // This should be an error view, or permission denied

        final Packet packet = validatePacket(model, proxiedId, packetId);
        if (packet != null)
        {
            logger.info("Allowing edit of packet [{}] by user {} while proxying user {}",
                        new Object[] { packetId, currentUser.getUserId(), proxiedId });
            redirectAttrs.addFlashAttribute("packetid", packet.getPacketId());
            redirectAttrs.addFlashAttribute("packet", packet);

            mav.setView(new RedirectView(DESIGN_URL, true));
        }

        return mav;
    }


    /**
     * Duplicates a Packet.<br>
     * Makes the original Packet available to the next view as <em>"packet"</em> and
     * the newly created duplicate packet as <em>"new_packet"</em>.
     *
     * @param req standard HttpServletRequest
     * @param model
     * @param redirectAttrs
     * @param packetId ID of the Packet to be duplicated.
     * @param top current browser top scroll position. (optional)
     * @param left current browser left scroll position. (optional)
     * @return ModelAndView to show next.
     */
    @RequestMapping(value="/packet", method=RequestMethod.POST, params="duplicate")
    public ModelAndView duplicatePacket(
                                final HttpServletRequest req,
                                final ModelMap model,
                                final RedirectAttributes redirectAttrs,
                                @RequestParam(value="duplicate", required=true) final String packetId,
                                @RequestParam(value="scrollTop", required=false) final String top,
                                @RequestParam(value="scrollLeft", required=false) final String left
                                )
    {
        final MIVUser currentUser = MIVSession.getSession(req).getUser();
        final int proxiedId = currentUser.getTargetUserInfo().getPerson().getUserId();

        final ModelAndView mav = new ModelAndView(ERROR_VIEW, model);

        addBreadcrumb("Duplicate a Packet",  BREADCRUMB_GROUP, req);

        final Packet packet = validatePacket(model, proxiedId, packetId);
        if (packet != null)
        {
            final Packet copiedPacket = packetService.copyPacket(packet, proxiedId);

            redirectAttrs.addFlashAttribute("packet", packet);          // original packet
            redirectAttrs.addFlashAttribute("new_packet", copiedPacket);// newly duplicated packet

            String message = MessageFormat.format("Duplicated packet &ldquo;{0}&rdquo; as new packet &ldquo;{1}&rdquo;",
                                                  packet.getPacketName(), copiedPacket.getPacketName());
            addMessage(model, message);

            logger.debug("Passing on top:{} and left:{}", top, left);
            redirectAttrs.addFlashAttribute("scrollTop", top);
            redirectAttrs.addFlashAttribute("scrollLeft", left);

            redirectAttrs.addFlashAttribute(MESSAGE_KEY, model.get(MESSAGE_KEY));

            mav.setView(new RedirectView(PACKET_URL, true));
        }

        return mav;
    }




    /**
     * Handle a request to preview a packet.<br>
     * Builds the packet files then cleans up the list to seperate the combined view from the individual files.
     * Puts the resulting documents in the Model.
     *
     * @param req
     * @param model
     * @param redirectAttrs
     * @param packetId ID of the Packet to be viewed.
     * @return name of the view to use
     */
    @RequestMapping(value="/packet/preview/{packetId}", method=RequestMethod.GET)
    public String previewPacket(
                                final HttpServletRequest req,
                                final ModelMap model,
                                final RedirectAttributes redirectAttrs,
                                @PathVariable("packetId") final String packetId)
    {
        final MIVUser currentUser = MIVSession.getSession(req).getUser();
        final int proxiedId = currentUser.getTargetUserInfo().getPerson().getUserId();

        List<Document> documentTypes = null;

        addBreadcrumb("Packet Preview",  BREADCRUMB_GROUP, req);

        logger.debug("'previewPacket' on path {} where packetId={}", req.getRequestURI(), packetId);
        model.addAttribute("message", "/packet/preview/" + packetId + " has invoked previewPacket");

//        final ModelAndView mav = new ModelAndView(ERROR_VIEW, model); // This should be an error view, or permission denied

        final Packet packet = validatePacket(model, proxiedId, packetId);
        if (packet != null)
        {
            documentTypes = MivServiceLocator.getPacketService().previewPacket(packet);

            // Get the combined packet document and place in a separate attribute, removing it from the rest of the list.
            Document removeCombinedDocument = null;
            for (Document document : documentTypes)
            {
                List<DocumentFile> documentFiles = document.getOutputDocuments();
                for (DocumentFile documentFile : documentFiles)
                {
                    if (documentFile.getDocumentFormat() == DocumentFormat.COMBINED_PDF)
                    {
                        removeCombinedDocument = document;
                        model.addAttribute("combinedPacketDocument", documentFile);
                        break;
                    }
                }
            }
            if (removeCombinedDocument != null)
            {
                documentTypes.remove(removeCombinedDocument);
            }


// Do I want to Redirect (add flash attrs) or just display a View (add plain attrs) ??
            redirectAttrs.addFlashAttribute("document_types", documentTypes);
            redirectAttrs.addFlashAttribute("hasDocuments", !documentTypes.isEmpty());

            model.addAttribute("packet", packet);
            model.addAttribute("document_types", documentTypes);
            model.addAttribute("hasDocuments", !documentTypes.isEmpty());

//            //mav.setViewName("packetPreview.jsp?action=View");
//            View x = new RedirectView("/jsp/packetPreview.jsp", true);
//            //x = new org.springframework.web.servlet.view.V
//            mav.setView(x);
            return PREVIEW_VIEW;
        }

        //return mav;
        return ERROR_VIEW;
    }



    /**
     * Handle a request to Delete a packet.<br>
     * Requires a confirmation parameter be sent with value "true".
     * This parameter should be added dynamically in response to a user pressing a "Confirm" button.
     *
     * @param req standard HttpServletRequest
     * @param model
     * @param redirectAttrs
     * @param packetId ID of the Packet to be deleted.
     * @param confirm
     * @return ModelAndView to show next.
     */
    @RequestMapping(value="/packet", method=RequestMethod.POST, params="delete")
    public ModelAndView deletePacket(
                                     final HttpServletRequest req,
                                     final ModelMap model,
                                     final RedirectAttributes redirectAttrs,
                                     @RequestParam(value="delete", required=true) final String packetId,
                                     @RequestParam(value="confirm", required=false, defaultValue="false") final String confirm
                                     )
    {
        final MIVUser currentUser = MIVSession.getSession(req).getUser();
        final int proxiedId = currentUser.getTargetUserInfo().getPerson().getUserId();

        model.remove("authHeader");

        addBreadcrumb("Delete a Packet",  BREADCRUMB_GROUP, req);


        final ModelAndView mav = new ModelAndView(ERROR_VIEW, model); // This should be an error view, or permission denied

        logger.info("confirm param value is [{}]", confirm);
        boolean confirmed = Boolean.parseBoolean(confirm);

        final Packet packet = validatePacket(model, proxiedId, packetId);

        if (packet != null)
        {
            logger.info("We have{} been confirmed to delete packet [{}]", confirmed ? "" : " NOT", packet);

            if (confirmed)
            {
                logger.debug("deletePacket is going to delete packet #{}", packet.getPacketId());
                boolean deleted = packetService.deletePacket(packet.getPacketId());

                String message = MessageFormat.format("Your packet named &quot;{1}&quot; has{0} been deleted.", deleted ? "" : " NOT", packet.getPacketName());
                addMessage(model, message);
                redirectAttrs.addFlashAttribute(MESSAGE_KEY, model.get(MESSAGE_KEY));

                mav.setView(new RedirectView(PACKET_URL, true));
            }
            else // We were asked to delete, but no confirmation was supplied
            {
                logger.info("deletePacket is not going to delete packet [{}], because value of confirm is [{}]", packet, confirmed);
                // Now what?
                //   Redirect back to PACKET_URL anyway but with an in-page confirmation set up?

                // This is just sending a message, and is temporary (as of Jul 6 2015)
                String message = MessageFormat.format(
                                      "Your packet named &quot;{0}&quot; has not been deleted. No confirmation was received.<br>" +
                                      "Please contact the <a href=\"mailto:miv-help@ucdavis.edu\">MIV help desk</a> for assistance.",
                                      packet.getPacketName());
                addMessage(model, message);
                redirectAttrs.addFlashAttribute(MESSAGE_KEY, model.get(MESSAGE_KEY));

                mav.setView(new RedirectView(PACKET_URL, true));
            }
        }

        return mav;

    }


    @RequestMapping(value=SUBMIT_URL, method=RequestMethod.POST)
    public ModelAndView submitPacket(final Long packetId,
                                     final Long dossierId,
                                     final ModelMap model,
                                     final HttpServletRequest req,
                                     final RedirectAttributes redirectAttrs)
    {
        MivPerson person = MIVSession.getSession(req).getUser().getTargetUserInfo().getPerson();
        Packet packet = validatePacket(model, person.getUserId(), packetId.toString());

        model.remove("authHeader");

        addBreadcrumb("Submit a Packet",  BREADCRUMB_GROUP, req);


        final ModelAndView mav = new ModelAndView(ERROR_VIEW, model);

        if (packet != null)
        {
            PacketRequest packetRequest = packetService.getPacketRequest(dossierId);

            if (packetRequest.getUserId() == person.getUserId())
            {
                boolean submitted = packetService.submitPacket(packet, packetRequest, person.getUserId());

                String message = MessageFormat.format("Your packet named &quot;{1}&quot; has{0} been submitted.", submitted ? "" : " NOT", packet.getPacketName());
                addMessage(model, message);
                redirectAttrs.addFlashAttribute(MESSAGE_KEY, model.get(MESSAGE_KEY));

                mav.setView(new RedirectView(PACKET_URL, true));
            }
            else
            {
                //add error message
                String msg = MessageFormat.format("Packet Request [{0}] belongs to user {1}, NOT {2}",
                                                  packetRequest.getPacketId(),
                                                  packetRequest.getUserId(),
                                                  person.getUserId());
                logger.warn(msg);
                addMessage(model, msg);
            }
        }

        return mav;
    }



    /**
     *
     * @param model
     * @param userId
     * @param packetId
     * @return
     */
    private Packet validatePacket(final ModelMap model, final int userId, final String packetId)
    {
        long pkid = 0L;
        Packet packet = null;

        // Packet ID has to be numeric
        try {
            pkid = Long.parseLong(packetId);
        }
        catch (NumberFormatException e) {
            String msg = MessageFormat.format("Packet ID [{0}] is not numeric.", packetId);
            logger.warn(msg);
            addMessage(model, msg);
            return null;
        }


        // Packet ID must refer to an existing packet
        packet = packetService.getPacket(pkid);

        if (packet == null)
        {
            String msg = MessageFormat.format("There is no packet with ID #{0}", Long.toString(pkid));
            logger.warn(msg);
            addMessage(model, msg);
            return null;
        }


        // Packet must belong to the person trying to manipulate it.
        logger.debug("Got packet [{}] from packetId {}", packet, pkid);
        if (packet.getUserId() != userId) {
            String msg = MessageFormat.format("Packet #{0} belongs to user {1}, NOT user {2}!",
                                              pkid, Long.toString(packet.getUserId()), Integer.toString(userId));
            logger.error(msg);
            addMessage(model, msg);
            return null;
        }


        return packet;
    }



    /**
     * Add a message to the model.<br>
     * Appends the message given to the existing message, if there is one.
     * @param model
     * @param message
     */
    private void addMessage(final ModelMap model, final String message)
    {
        if (message == null) { // nothing to add
            logger.warn("Tried to add a null message to the model.");
            return;
        }

        String curMsg = (String) model.get(MESSAGE_KEY);
        if (curMsg == null) curMsg = StringUtil.EMPTY_STRING;

        StringBuilder sb = new StringBuilder(curMsg);
        if (sb.length() > 0) sb.append("<br>");
        sb.append(message);
        model.addAttribute(MESSAGE_KEY, sb.toString().trim());

        logger.debug("message is now [{}]", model.get(MESSAGE_KEY));
    }


/* Could we use a Validator to deal with some of the form POST data?
    public class NameValidator implements Validator
    {
        @Override
        public boolean supports(Class<?> clazz)
        {
            // Auto-generated method stub
            return true;
        }

        @Override
        public void validate(Object target, Errors errors)
        {
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, field, errorCode)

        }

    }
*/
}
