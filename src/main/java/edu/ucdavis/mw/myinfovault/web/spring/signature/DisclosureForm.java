/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: DisclosureForm.java
 */

package edu.ucdavis.mw.myinfovault.web.spring.signature;

import java.io.Serializable;

import edu.ucdavis.mw.myinfovault.domain.disclosurecertificate.DCBo;
import edu.ucdavis.mw.myinfovault.domain.signature.MivElectronicSignature;
import edu.ucdavis.myinfovault.document.PathConstructor;

/**
 * Form backing for the signing a disclosure signature action.
 *
 * @author Craig Gilmore
 * @since MIV 4.7
 */
public class DisclosureForm extends SignatureForm implements Serializable
{
    private static final long serialVersionUID = 2012113016120L;

    private static final PathConstructor pathConstructor = new PathConstructor();
    
    /**
     * Create the form backing for the given disclosure certificate and signature/request.
     *
     * @param signature signature/request
     */
    DisclosureForm(MivElectronicSignature signature)
    {
        super(signature);
    }

    /**
     * @return URL to the disclosure PDF
     */
    public String getDisclosureUrl()
    {
        return pathConstructor.getUrlFromPath(getSignature().<DCBo>getDocument().getDossierFile());
    }
}
