
package edu.ucdavis.mw.myinfovault.web.spring.search;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import edu.ucdavis.mw.myinfovault.domain.dossier.Dossier;
import edu.ucdavis.mw.myinfovault.dossier.attributes.DossierAttributeStatus;

/**
 * Contains the fields, getters, and setters for Open Action results list. added fields, getters, and setters for Open
 * Action Report results list.
 *
 * @author Mary Northup
 * @since MIV 3.0
 */
public class MivActionList implements Serializable
{
    private static final long serialVersionUID = 200906111719L;

    private final Dossier dossier;

    private int userId = 0;
    private long dossierId = 0;
    private String givenName = "";
    private String surname = "";
    private String sortName = "";
    private String displayName = "";
    private String schoolName = "";
    private String departmentName = "";
    private String appointmentType = "";
    private String delegationAuthority;
    private String actionType;
    private String roleType;
    private String reviewCommittees;
    private String submitDate = null;
    private String lastRoutedDate = null;
    private String completedDate = null;
    private String archiveDate = null;
    private String actionLocation = "";
    private String locationDescription = "";
    private String dossierDescription = "";
    private int schoolId = 0;
    private int departmentId = 0;
    private String dossierUrl = "";
    /* added for MIV Open Actions Report */
    private String jointStatus = "";
    private DossierAttributeStatus reviewStatus = DossierAttributeStatus.CLOSE;
    private String disclosureStatus = "";
    private String decisionStatus = "";
    private String releasedStatus = "";
    private String locationStatus = "";
    private String decisionType = "";
    private String decisionDate = "";
    private String releasedTo = "";
    private String recommendationOrDecision = "";
    private boolean executiveFinalDecision = false;
    private List<String> reviewGroups = new ArrayList<String>();

    /**
     * Create an action list item.
     *
     * @param dossier dossier from which item information is taken
     */
    public MivActionList(Dossier dossier)
    {
        this.dossier = dossier;
    }

    /**
     * @return dossier this action list item represents
     */
    public Dossier getDossier()
    {
        return dossier;
    }

    public int getUserId()
    {
        return userId;
    }


    public void setUserId(int userId)
    {
        this.userId = userId;
    }

    public String getRoleType()
    {
        return roleType;
    }


    public void setRoleType(String roleType)
    {
        this.roleType = roleType;
    }


    public long getDossierId()
    {
        return dossierId;
    }


    public void setDossierId(long dossierId)
    {
        this.dossierId = dossierId;
    }


    public String getGivenName()
    {
        return givenName;
    }


    public void setGivenName(String givenName)
    {
        this.givenName = givenName;
    }


    public String getSurname()
    {
        return surname;
    }


    public void setSurname(String surname)
    {
        this.surname = surname;
    }


    public String getSortName()
    {
        return this.sortName;
    }


    public void setSortName(String sortName)
    {
        this.sortName = sortName;
    }


    public String getDisplayName()
    {
        return displayName;
    }


    public void setDisplayName(String displayName)
    {
        this.displayName = displayName;
    }


    public String getSchoolName()
    {
        return schoolName;
    }


    public void setSchoolName(String schoolName)
    {
        this.schoolName = schoolName;
    }


    public String getDepartmentName()
    {
        return departmentName;
    }


    public void setDepartmentName(String departmentName)
    {
        this.departmentName = departmentName;
    }


    public String getAppointmentType()
    {
        return appointmentType;
    }


    public void setAppointmentType(String appointmentType)
    {
        this.appointmentType = appointmentType;
    }


    public String getDelegationAuthority()
    {
        return delegationAuthority;
    }


    public void setDelegationAuthority(String delegationAuthority)
    {
        this.delegationAuthority = delegationAuthority;
    }


    public String getActionType()
    {
        return actionType;
    }


    public void setActionType(String actionType)
    {
        this.actionType = actionType;
    }


    public String getSubmitDate()
    {
        return submitDate;
    }


    public void setSubmitDate(String submitDate)
    {
        this.submitDate = submitDate;
    }


    public String getLastRoutedDate()
    {
        return lastRoutedDate;
    }


    public void setLastRoutedDate(String lastRoutedDate)
    {
        this.lastRoutedDate = lastRoutedDate;
    }


    public String getCompletedDate()
    {
        return completedDate;
    }


    public void setCompletedDate(String completedDate)
    {
        this.completedDate = completedDate;
    }


    public String getArchiveDate()
    {
        return archiveDate;
    }


    public void setArchiveDate(String archiveDate)
    {
        this.archiveDate = archiveDate;
    }


    public String getActionLocation()
    {
        return actionLocation;
    }


    public void setActionLocation(String actionLocation)
    {
        this.actionLocation = actionLocation;
    }


    public String getLocationDescription()
    {
        return locationDescription;
    }


    public void setLocationDescription(String locationDescription)
    {
        this.locationDescription = locationDescription;
    }


    public String getDossierDescription()
    {
        return dossierDescription;
    }


    public void setDossierDescription(String dossierDescription)
    {
        this.dossierDescription = dossierDescription;
    }


    public int getSchoolId()
    {
        return schoolId;
    }


    public void setSchoolId(int schoolId)
    {
        this.schoolId = schoolId;
    }


    public int getDepartmentId()
    {
        return departmentId;
    }


    public void setDepartmentId(int departmentId)
    {
        this.departmentId = departmentId;
    }


    public String getDossierUrl()
    {
        return this.dossierUrl;
    }


    public void setDossierUrl(String dossierUrl)
    {
        this.dossierUrl = dossierUrl;
    }


    /* added for MIV Open Actions Report */
    public String getJointStatus()
    {
        return this.jointStatus;
    }


    public void setJointStatus(String jointStatus)
    {
        this.jointStatus = jointStatus;
    }


    public DossierAttributeStatus getReviewStatus()
    {
        return reviewStatus;
    }


    public void setReviewStatus(DossierAttributeStatus reviewStatus)
    {
        // review status may only be opened or closed
        this.reviewStatus = reviewStatus != DossierAttributeStatus.OPEN
                ? DossierAttributeStatus.CLOSE
                : DossierAttributeStatus.OPEN;
    }


    public String getDisclosureStatus()
    {
        return this.disclosureStatus;
    }


    public void setDisclosureStatus(String disclosureStatus)
    {
        this.disclosureStatus = disclosureStatus;
    }


    public String getDecisionStatus()
    {
        return this.decisionStatus;
    }


    public void setDecisionStatus(String decisionStatus)
    {
        this.decisionStatus = decisionStatus;
    }


    public String getDecisionType()
    {
        return decisionType;
    }


    public void setDecisionType(String decisionType)
    {
        this.decisionType = decisionType;
    }

    public String getReviewCommittees()
    {
        return reviewCommittees;
    }


    public void setReviewCommittees(String reviewCommittees)
    {
        this.reviewCommittees = reviewCommittees;
    }

    public String getReleasedStatus()
    {
        return this.releasedStatus;
    }


    public void setReleasedStatus(String releasedStatus)
    {
        this.releasedStatus = releasedStatus;
    }


    public String getLocationStatus()
    {
        return locationStatus;
    }


    public void setLocationStatus(String locationStatus)
    {
        this.locationStatus = locationStatus;
    }


    @Override
    public String toString()
    {
        return this.getDisplayName() + " " + this.getActionType() + " at " + this.getActionLocation();
    }


    public String getDecisionDate()
    {
        return decisionDate;
    }


    public void setDecisionDate(String decitionDate)
    {
        this.decisionDate = decitionDate;
    }


	public List<String> getReviewGroups() {
		return reviewGroups;
	}


	public void setReviewGroups(List<String> reviewGroups) {
		this.reviewGroups = reviewGroups;
	}

	public void addReviewGroup(String reviewGroup) {
		this.reviewGroups.add(reviewGroup);
	}

	public String getReleasedTo() {
		return releasedTo;
	}

	public void setReleasedTo(String releasedTo) {
		this.releasedTo = releasedTo;
	}

	public boolean hasExecutiveFinalDecision() {
		return executiveFinalDecision;
	}

	public void setExecutiveFinalDecision(boolean executiveFinalDecision) {
		this.executiveFinalDecision = executiveFinalDecision;
	}

	public String getRecommendationOrDecision() {
		return recommendationOrDecision;
	}

	public void setRecommendationOrDecision(String recommendationOrDecision) {
		this.recommendationOrDecision = recommendationOrDecision;
	}
}
