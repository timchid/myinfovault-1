/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: CreateActionController.java
 */
package edu.ucdavis.mw.myinfovault.web.spring.submitdossier;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import edu.ucdavis.mw.myinfovault.domain.action.DossierDelegationAuthority;
import edu.ucdavis.mw.myinfovault.domain.dossier.Dossier;
import edu.ucdavis.mw.myinfovault.service.MivServiceLocator;
import edu.ucdavis.mw.myinfovault.service.dossier.DossierService;
import edu.ucdavis.mw.myinfovault.service.dossier.WorkflowException;
import edu.ucdavis.mw.myinfovault.service.person.AssignedRole;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.mw.myinfovault.service.person.MivRole;
import edu.ucdavis.mw.myinfovault.web.ControllerBase;
import edu.ucdavis.mw.myinfovault.web.ForbiddenException;
import edu.ucdavis.myinfovault.MIVSession;
import edu.ucdavis.myinfovault.PropertyManager;
import edu.ucdavis.myinfovault.exception.MivSevereApplicationError;

/**
 * Serves the create action form and its confirmation page.
 *
 * @author japorito
 * @since 5.0
 */
@Controller
public class CreateActionController extends ControllerBase
{
    private static final Properties strings_new = PropertyManager.getPropertySet("createaction-mvc-newAction", "strings");
    private static final Properties tooltips_new = PropertyManager.getPropertySet("createaction-mvc-newAction", "tooltips");
    private static final Properties strings_confirm = PropertyManager.getPropertySet("createaction-mvc-actionConfirmation", "strings");
    private static final Properties tooltips_confirm = PropertyManager.getPropertySet("createaction-mvc-actionConfirmation", "tooltips");
    private static final DossierService dossierService = MivServiceLocator.getDossierService();
    private static final ThreadLocal<DateFormat> dossierDateFormat =
        new ThreadLocal<DateFormat>() {
            @Override protected DateFormat initialValue() {
                return new SimpleDateFormat("MM/dd/yy, h:mm a");
            }
        };

    @RequestMapping(value="/createAction")
    public ModelAndView newAction(HttpServletRequest req, @ModelAttribute("authHeader") String authHeader)
    {
        MIVSession sess = MIVSession.getSession(req);
        SubmitDossierForm form = new SubmitDossierForm();
        final int submitterId = sess.getUser().getTargetUserInfo().getPerson().getUserId();

        ModelAndView mav = new ModelAndView("createaction", "form", form);
        mav.addObject("authHeader", authHeader);
        mav.addObject("submitterId", submitterId);
        mav.addObject("strings", strings_new);
        mav.addObject("tooltips", tooltips_new);
        mav.addObject("delegations", DossierDelegationAuthority.values());

        return mav;
    }

    @RequestMapping(value="/createAction/confirm")
    public ModelAndView actionConfirm(@RequestParam(value="dossier") Long dossierId, HttpServletRequest req)
    {
        MivPerson shadowPerson = MIVSession.getSession(req).getUser().getTargetUserInfo().getPerson();

        try
        {
            Dossier dossier = dossierService.getDossier(dossierId);
            MivPerson dossierPerson = dossier.getAction().getCandidate();
            Boolean inScope = false;

            for (AssignedRole role : shadowPerson.getAssignedRoles()) {
                if (role.getRole().getCategory() == MivRole.Category.STAFF &&
                    role.getScope().matches(dossierPerson.getPrimaryAppointment().getScope()))
                {
                    inScope = true;
                }
            }

            if (!inScope)
            {
                throw new ForbiddenException();
            }

            String submitDate = "Submitted " + dossierDateFormat.get().format(dossier.getSubmittedDate().getTime());

            ModelAndView mav = new ModelAndView("actionconfirmation");

            mav.addObject("dossierId", dossier.getDossierId());
            mav.addObject("dossierDescription", dossier.getAction().getDescription());
            mav.addObject("candidate", dossier.getAction().getCandidate().getDisplayName());
            mav.addObject("departments", dossier.getDepartmentList());
            mav.addObject("submitDate", submitDate);

            mav.addObject("strings", strings_confirm);
            mav.addObject("tooltips", tooltips_confirm);

            return mav;
        }
        catch (WorkflowException wfe)
        {
            throw new MivSevereApplicationError("Failed loading newly created dossier", wfe);
        }
    }

}
