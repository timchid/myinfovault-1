/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: ReportSearchStrategy.java
 */
package edu.ucdavis.mw.myinfovault.web.spring.report;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import edu.ucdavis.mw.myinfovault.service.MivServiceLocator;
import edu.ucdavis.mw.myinfovault.service.SearchFilter;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.mw.myinfovault.service.person.UserService;
import edu.ucdavis.mw.myinfovault.valuebeans.report.ReportVO;
import edu.ucdavis.mw.myinfovault.web.spring.search.SearchCriteria;
import edu.ucdavis.myinfovault.MIVUser;

/**
 * This class defines all of the currently required user report search methods.
 * The UserRoleSearchStrategy class implements all methods in the interface.
 *
 * @author Pradeep K Haldiya
 * @since MIV 4.0
 */
public interface ReportSearchStrategy
{
    /**
     * FIXME: never used
     */
    public UserService userService = MivServiceLocator.getUserService();
    
    /**
     * FIXME: never used
     */
    public Logger logger = Logger.getLogger(ReportSearchStrategy.class);

    /**
     * Basic search for users by their name with no SearchFilter applied.
     * 
     * @param mivUser the target user doing the search
     * @param criteria the search criteria to use
     * @return ReportVO objects
     */
    public ReportVO getUsersByName(MIVUser mivUser, SearchCriteria criteria);

    /**
     * Basic search for users by their name.
     * 
     * @param actor The target user doing the search
     * @param criteria The search criteria to use
     * @param s The SearchFilter to further filter the results
     * @return A list of Map
     */
    public List<Map<String, Object>> getUsersByName(MivPerson actor, SearchCriteria criteria, SearchFilter<MivPerson> s);

    /**
     * Basic search for users by their department with no SearchFilter applied.
     * 
     * @param mivUser The target user doing the search
     * @param criteria The search criteria to use
     * @return ReportVO objects
     */
    public ReportVO getUsersByDepartment(MIVUser mivUser, SearchCriteria criteria);

    /**
     * Basic search for users by their department.
     * 
     * @param actor The target user doing the search
     * @param criteria The search criteria to use
     * @param s The SearchFilter to further filter the results
     * @return A list of Map
     */
    public List<Map<String, Object>> getUsersByDepartment(MivPerson actor, SearchCriteria criteria, SearchFilter<MivPerson> s);

    /**
     * Basic search for users by their school with no SearchFilter applied.
     * 
     * @param mivUser The target user doing the search
     * @param criteria The search criteria to use
     * @return ReportVO objects
     */
    public ReportVO getUsersBySchool(MIVUser mivUser, SearchCriteria criteria);

    /**
     * Basic search for users by their school.
     * 
     * @param actor The target user doing the search
     * @param criteria The search criteria to use
     * @param s The SearchFilter to further filter the results
     * @return A list of Map
     */
    public List<Map<String, Object>> getUsersBySchool(MivPerson actor, SearchCriteria criteria, SearchFilter<MivPerson> s);

}
