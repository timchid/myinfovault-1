/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: DownloadValidator.java
 */

package edu.ucdavis.mw.myinfovault.web.spring.publications;

import org.springframework.util.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

/**
 * Validates the publication download form.
 *
 * @author Craig Gilmore
 * @since MIV 3.9.1
 */
public class DownloadValidator implements Validator
{
    /*
     * (non-Javadoc)
     * @see org.springframework.validation.Validator#supports(java.lang.Class)
     */
    @Override
    public boolean supports(@SuppressWarnings("rawtypes") Class clazz)
    {
        return clazz.equals(DownloadForm.class);
    }

    /*
     * (non-Javadoc)
     * @see org.springframework.validation.Validator#validate(java.lang.Object, org.springframework.validation.Errors)
     */
    @Override
    public void validate(Object target, Errors errors)
    {
        DownloadForm form = (DownloadForm) target;

        if (!StringUtils.hasText(form.getSurname()))
        {
            errors.reject(null, "Last name is required");
        }

        if (!StringUtils.hasText(form.getGivenInitial()))
        {
            errors.reject(null, "First initial is required");
        }
        else if (!Character.isLetter(form.getGivenInitial().charAt(0)))
        {
            errors.reject(null, "First initial must be a letter");
        }

        if (StringUtils.hasText(form.getMiddleInitial()) && !Character.isLetter(form.getMiddleInitial().charAt(0)))
        {
            errors.reject(null, "Middle initial must be a letter");
        }
    }
}
