/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: AssignReviewersSearchStrategy.java
 */

package edu.ucdavis.mw.myinfovault.web.spring.search;

import java.util.List;

import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.mw.myinfovault.service.person.MivRole;
import edu.ucdavis.myinfovault.MIVUser;

/**
 * Implement the strategy to be used when searching for users to
 * include in a group. Group members must be active candidates.
 *
 * @author Craig Gilmore
 * @since 4.3
 */
public class GroupMembersSearchStrategy extends DefaultSearchStrategy
{
    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.web.spring.search.DefaultSearchStrategy#getUsersByName(edu.ucdavis.myinfovault.MIVUser, edu.ucdavis.mw.myinfovault.web.spring.search.SearchCriteria)
     */
    @Override
    public List<MivPerson> getUsersByName(MIVUser user,
                                          SearchCriteria criteria)
    {
        return super.getUsersByName(user,
                                    getCriteria(criteria));
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.web.spring.search.DefaultSearchStrategy#getUsersByDepartment(edu.ucdavis.myinfovault.MIVUser, edu.ucdavis.mw.myinfovault.web.spring.search.SearchCriteria)
     */
    @Override
    public List<MivPerson> getUsersByDepartment(MIVUser user,
                                                SearchCriteria criteria)
    {
        return super.getUsersByDepartment(user,
                                          getCriteria(criteria));
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.web.spring.search.DefaultSearchStrategy#getUsersBySchool(edu.ucdavis.myinfovault.MIVUser, edu.ucdavis.mw.myinfovault.web.spring.search.SearchCriteria)
     */
    @Override
    public List<MivPerson> getUsersBySchool(MIVUser user,
                                            SearchCriteria criteria)
    {
        return super.getUsersBySchool(user,
                                      getCriteria(criteria));
    }

    //criteria used in both search methods
    private SearchCriteria getCriteria(SearchCriteria criteria)
    {
        //user must be a candidate
        criteria.setRole(MivRole.CANDIDATE);

        return criteria;
    }
}
