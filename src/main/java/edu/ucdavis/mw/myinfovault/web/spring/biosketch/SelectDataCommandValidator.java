/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: SelectDataCommandValidator.java
 */

package edu.ucdavis.mw.myinfovault.web.spring.biosketch;

import java.util.Calendar;
import java.util.GregorianCalendar;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import edu.ucdavis.myinfovault.data.MIVUtil;

public class SelectDataCommandValidator implements Validator
{
    @Override
    public boolean supports(Class clazz)
    {
        return clazz.equals(SelectDataCommand.class);
    }

    /* (non-Javadoc)
     * @see org.springframework.validation.Validator#validate(java.lang.Object, org.springframework.validation.Errors)
     */
    @Override
    public void validate(Object command, Errors errors)
    {
        SelectDataCommand selectDataCommand = (SelectDataCommand) command;
        String startYear = selectDataCommand.getStartYear();
        String endYear = selectDataCommand.getEndYear();

        // Clear the start or end year if not valid; this avoids XSS attacks.

        if (! validateYear(startYear, "year", "From year", errors)) {
            selectDataCommand.setStartYear(
                        MIVUtil.escapeMarkup(startYear)
            );
        }

        if (! validateYear(endYear, "year", "To year", errors)) {
            selectDataCommand.setEndYear(
                        MIVUtil.escapeMarkup(endYear)
            );
        }
    }


    /**
     * Validate a year field. To be valid the field contents must be:
     * <ul><li>Numeric only</li><li>Within the range 1900 &lt;= year &lt;= current year</li></ul>
     * @param year Value of the field to be validated
     * @param fieldName Name of the field being validated
     * @param fieldTitle Title of the field, to display in the error message
     * @param errors The <em>Spring</em> errors object to fill in if the field is invalid
     * @return <code>true</code> if the year is valid
     */
    private boolean validateYear(String year, String fieldName, String fieldTitle, Errors errors)
    {
        boolean valid = true;
        Calendar cal = new GregorianCalendar();
        int currentYear = cal.get(Calendar.YEAR);

        if ((year != null && year.length() > 0))
        {
            boolean isNumeric = year.matches("^[-+]?\\d+(\\.\\d+)?$");
            if (isNumeric)
            {
                int numericYear = 0;
                try {numericYear = Integer.parseInt(year); }
                    catch (NumberFormatException e) { /* ignore exception, allow numericYear to remain==0 */ }

                if (numericYear < 1900 || numericYear > currentYear)
                {
                    valid = false;
                    errors.reject(fieldName, fieldTitle + " must be from 1900 to " + currentYear);
                }
            }
            else
            {
                valid = false;
                errors.reject(fieldName, fieldTitle + " must be numeric");
            }
        }

        return valid;
    }


    /*
     * @param year
     * @param errors
     *
    private boolean validateStartYear(String year, Errors errors)
    {
        boolean valid = true;
        Calendar cal = new GregorianCalendar();
        int currentYear = cal.get(Calendar.YEAR);

        if ((year != null && year.length() > 0))
        {
            boolean isNumeric = year.matches("^[-+]?\\d+(\\.\\d+)?$");
            if (isNumeric)
            {
                if (year.length() == 4)
                {
                    int numericYear = Integer.parseInt(year);
                    if (numericYear < 1900 || numericYear > currentYear)
                    {
                        valid = false;
                        errors.reject("year", "From year must be from 1900 to " + currentYear);
                    }
                }
                else
                {
                    valid = false;
                    errors.reject("year", "From year must be from 1900 to " + currentYear);
                }
            }
            else
            {
                valid = false;
                errors.reject("year", "From year must be numeric");
            }
        }

        return valid;
    }


    private boolean validateEndYear(String year, Errors errors)
    {
        boolean valid = true;
        Calendar cal = new GregorianCalendar();
        int currentYear = cal.get(Calendar.YEAR);

        if ((year != null && year.length() > 0))
        {
            boolean isNumeric = year.matches("^[-+]?\\d+(\\.\\d+)?$");
            if (isNumeric)
            {
                if (year.length() == 4)
                {
                    int numericYear = Integer.parseInt(year);
                    if (numericYear < 1900 || numericYear > currentYear)
                    {
                        valid = false;
                        errors.reject("year", "To year must be from 1900 to " + currentYear);
                    }
                }
                else
                {
                    valid = false;
                    errors.reject("year", "To year must be from 1900 to " + currentYear);
                }
            }
            else
            {
                valid = false;
                errors.reject("year", "To year must be numeric");
            }
        }

        return valid;
    }
    */
}

