/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: RafAction.java
 */

package edu.ucdavis.mw.myinfovault.web.spring.raf;

import java.math.BigDecimal;
import java.util.Date;

import org.springframework.beans.PropertyEditorRegistry;
import org.springframework.webflow.execution.Event;
import org.springframework.webflow.execution.RequestContext;

import edu.ucdavis.mw.myinfovault.domain.action.AcademicAction;
import edu.ucdavis.mw.myinfovault.domain.raf.Rank;
import edu.ucdavis.mw.myinfovault.domain.raf.RankFilter;
import edu.ucdavis.mw.myinfovault.domain.raf.Status;
import edu.ucdavis.mw.myinfovault.events2.AcademicActionEvent;
import edu.ucdavis.mw.myinfovault.events2.EventDispatcher2;
import edu.ucdavis.mw.myinfovault.service.SearchFilter;
import edu.ucdavis.mw.myinfovault.service.authorization.AuthorizationService;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.mw.myinfovault.service.person.UserService;
import edu.ucdavis.mw.myinfovault.service.raf.RafService;
import edu.ucdavis.mw.myinfovault.web.spring.commons.MivFormAction;
import edu.ucdavis.mw.myinfovault.web.spring.commons.MivPropertyEditors;

/**
 * Webflow form action for the recommended action form.
 *
 * @author Craig Gilmore
 * @since 3.0
 */
public class RafAction extends MivFormAction
{
    private final RafService rafService;

    /**
     * Create the RAF spring form action bean.
     *
     * @param userService Spring injected user service
     * @param authorizationService Spring injected authorization service
     * @param rafService Spring injected RAF service
     */
    public RafAction(UserService userService,
                     AuthorizationService authorizationService,
                     RafService rafService)
    {
        super(userService, authorizationService);

        this.rafService = rafService;
    }


    /*
     * (non-Javadoc)
     * @see org.springframework.webflow.action.FormAction#createFormObject(org.springframework.webflow.execution.RequestContext)
     */
    @Override
    protected Object createFormObject(RequestContext context)
    {
        return new RafForm(rafService.getBo(getDossier(context)));
    }


    /**
     * Persists RAF business object to database and creates the PDF form.
     *
     * @param context Request context from webflow
     * @return Event status, success or error
     */
    public Event saveFormObject(RequestContext context)
    {
        RafForm form = getFormObject(context);

        try
        {
            // bind action from form to RAF's dossier reference
            form.getRaf().getDossier().setAction(new AcademicAction(form.getCandidate(),
                                                                    form.getActionType(),
                                                                    form.getDelegationAuthority(),
                                                                    form.getEffectiveDateAsDate(),
                                                                    form.getRetroactiveDateAsDate(),
                                                                    form.getEndDateAsDate(),
                                                                    form.getAccelerationYears()));
        }
        catch (IllegalArgumentException e)
        {
            this.getFormErrors(context).rejectValue(null, e.getMessage());

            return error();
        }

        /*
         * Filter out all ranks that contain no data.
         */
        SearchFilter<Rank> rankFilter = new RankFilter();
        for (Status status : form.getRaf().getStatuses())
        {
            status.getRanks().removeAll(rankFilter.apply(status.getRanks()));
        }

        MivPerson realPerson = getRealPerson(context);

        // save RAF, update dossier/DC, and create PDFs
        AcademicAction formerAction = rafService.saveRaf(form.getRaf(),
                                                         getShadowPerson(context),
                                                         realPerson);

        EventDispatcher2.getDispatcher().post(
            new AcademicActionEvent(realPerson,
                                    form.getRaf().getDossier(),
                                    formerAction));

        return success();
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.web.spring.commons.MivFormAction#getFormObject(org.springframework.webflow.execution.RequestContext)
     */
    @Override
    public RafForm getFormObject(RequestContext context)
    {
        return (RafForm) super.getFormObject(context);
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.web.spring.commons.MivFormAction#loadProperties(org.springframework.webflow.execution.RequestContext)
     */
    @Override
    public void loadProperties(RequestContext context)
    {
        super.loadProperties(context, DEFAULT_SUBSET, "labels", "tooltips");
    }

    /*
     * (non-Javadoc)
     * @see org.springframework.webflow.action.FormAction#registerPropertyEditors(org.springframework.beans.PropertyEditorRegistry)
     */
    @Override
    protected void registerPropertyEditors(PropertyEditorRegistry registry)
    {
        registry.registerCustomEditor(Date.class, new MivPropertyEditors.DatePropertyEditor());
        registry.registerCustomEditor(BigDecimal.class, new MivPropertyEditors.BigDecimalPropertyEditor());
    }
}
