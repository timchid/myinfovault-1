/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: SearchCriteria.java
 */

package edu.ucdavis.mw.myinfovault.web.spring.search;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * This class/formObject is used by openactions report search page(s) to collect the criteria entered for a search. The
 * text is used on the page to show checkboxes.
 *
 * @author Mary Northup
 * @since MIV 4.5
 */
public class SearchCriteriaCheckboxes implements Serializable
{
    private static final long serialVersionUID = 200903181723L;
    private static final String TOSTRING_FORMAT = "SearchCriteriaCheckboxes [rptType=%s, rptSearch=%s, checkboxes=%s]";

    private String rptType = "";
    private String rptSearch = "";
    private List<CheckboxLine> checkboxes = new ArrayList<CheckboxLine>();


    public String getRptType()
    {
        return rptType;
    }


    public void setRptType(String rptType)
    {
        this.rptType = rptType;
    }


    public String getRptSearch()
    {
        return rptSearch;
    }


    public void setRptSearch(String rptSearch)
    {
        this.rptSearch = rptSearch;
    }


    public void setCheckboxes(List<CheckboxLine> checkboxes)
    {
        this.checkboxes = checkboxes;
    }


    public List<CheckboxLine> getCheckboxes()
    {
        return checkboxes;
    }


    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        return String.format(TOSTRING_FORMAT,
                             rptType, rptSearch, checkboxes);
    }


    protected static SearchCriteriaCheckboxes getDepartmentCriteria()
    {
        SearchCriteriaCheckboxes criterialist = new SearchCriteriaCheckboxes();
        List<CheckboxLine> results = new ArrayList<CheckboxLine>();
        results.add(new CheckboxLine("OPR", "Open Packet Requests.",
                "Open Packet Requests"));
        results.add(new CheckboxLine("DC", "Open actions at the Department awaiting a signed disclosure.",
                "Awaiting signed disclosure"));
        results.add(new CheckboxLine("OR", "Open actions at the Department with an open review period.", "Open Reviews"));
        results.add(new CheckboxLine("IJA", "Open actions at the Department with incomplete joint appointments.",
                "Incomplete Joint Appointments"));
        results.add(new CheckboxLine("Ready",
                "Open actions at the Department that are available to send to the School/College.",
                "Available to send to School/College"));
        criterialist.setCheckboxes(results);
        criterialist.setRptType("searchdept");
        criterialist.setRptSearch("search1");

        return criterialist;
    }


    protected static SearchCriteriaCheckboxes getSchoolCriteria()
    {
        SearchCriteriaCheckboxes criterialist = new SearchCriteriaCheckboxes();
        List<CheckboxLine> results = new ArrayList<CheckboxLine>();
        results.add(new CheckboxLine("OR", "Open actions at the School/College with an open review period.",
                "Open Reviews"));
        results.add(new CheckboxLine("Dean",
                "Open actions at the School/College released to the dean without a dean's signature.",
                "Without dean's signature"));
        results.add(new CheckboxLine("IJA", "Open actions at the School/College with incomplete joint appointments.",
                "Incomplete Joint Appointments"));
        results.add(new CheckboxLine("Ready",
                "Open actions at the School/College that are available to send to the next step.",
                "Available to send along"));
        criterialist.setCheckboxes(results);
        criterialist.setRptType("searchschool");
        criterialist.setRptSearch("search2");

        return criterialist;
    }


    protected static SearchCriteriaCheckboxes getSenateCriteria()
    {
        SearchCriteriaCheckboxes criterialist = new SearchCriteriaCheckboxes();
        List<CheckboxLine> results = new ArrayList<CheckboxLine>();
        results.add(new CheckboxLine("ORS", "Open actions at Senate with an open review period.",
                "Open Reviews Senate"));
        results.add(new CheckboxLine("ORF", "Open actions at Federation with an open review period.",
                "Open Reviews Federation"));
        results.add(new CheckboxLine("ORSF", "Open actions at Senate/Federation with an open review period.",
                "Open Reviews Senate/Federation"));
        results.add(new CheckboxLine("ReadySenate", "Open actions at Senate that are completed.",
                "Complete at Senate location"));
        results.add(new CheckboxLine("ReadyFed", "Open actions at Federation that are completed.",
                "Complete at CRC location"));
        results.add(new CheckboxLine("ReadySenateFed", "Open actions at Senate/Federation that are completed.",
                "Complete at Senate/Federation location"));
        results.add(new CheckboxLine("AllSenate", "Open actions at Senate.",
                "Actions at Senate location"));
        results.add(new CheckboxLine("AllFed", "Open actions at Federation.",
                "Actions at Federation location"));
        results.add(new CheckboxLine("AllSenateFed", "Open actions at Senate/Federation.",
                "Actions at Senate/Federation location"));
        criterialist.setCheckboxes(results);
        criterialist.setRptType("searchsenate");
        criterialist.setRptSearch("search4");

        return criterialist;
    }


    protected static SearchCriteriaCheckboxes getAcademicCriteria()
    {
        SearchCriteriaCheckboxes criterialist = new SearchCriteriaCheckboxes();
        List<CheckboxLine> results = new ArrayList<CheckboxLine>();
        results.add(new CheckboxLine("OPR", "Open Packet Requests.",
                "Open Packet Requests"));
        results.add(new CheckboxLine("OR", "Open actions at Academic Affairs with an open review period.",
                "Open Reviews"));
        results.add(new CheckboxLine("VP",
                "Open actions at Academic Affairs released without a final decision.",
                "Without final decision"));
        results.add(new CheckboxLine("Audit", "Open actions at the School/College that are complete.",
                "Complete at the School/College"));
        results.add(new CheckboxLine("Ready",
                "Open actions at Academic Affairs with a final decision.",
                "With final decision"));
        criterialist.setCheckboxes(results);
        criterialist.setRptType("searchacademic");
        criterialist.setRptSearch("search3");

        return criterialist;
    }

}
