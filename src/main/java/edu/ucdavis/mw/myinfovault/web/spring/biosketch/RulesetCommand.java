package edu.ucdavis.mw.myinfovault.web.spring.biosketch;

/**
 * Command Object representing a BiosketchRuleset
 * @author dreddy
 * @since MIV 2.1
 */
public class RulesetCommand extends BaseCommand
{
    private CriteriaCommand criteria = new CriteriaCommand();

    /**
     * @return
     */
    public CriteriaCommand getCriteria()
    {
        return criteria;
    }

    /**
     * @param criteria
     */
    public void setCriteria(CriteriaCommand criteria)
    {
        this.criteria = criteria;
    }
}
