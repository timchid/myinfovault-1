/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: SwitchUserSearchStrategy.java
 */

package edu.ucdavis.mw.myinfovault.web.spring.search;

import java.util.ArrayList;
import java.util.List;

import edu.ucdavis.mw.myinfovault.util.AttributeSet;

import edu.ucdavis.mw.myinfovault.service.MivServiceLocator;
import edu.ucdavis.mw.myinfovault.service.authorization.AuthorizationService;
import edu.ucdavis.mw.myinfovault.service.authorization.AuthorizationService.Qualifier;
import edu.ucdavis.mw.myinfovault.service.authorization.Permission;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.mw.myinfovault.service.person.MivRole;
import edu.ucdavis.myinfovault.MIVUser;

/**
 * Implements the strategy to be used when searching for users to switch to.
 * The search strategy is determined by the role of the searching user as follows:
 * Only Department Admins, School Admins, MIV Admins and OCP Admins are allowed to switch to users.
 * Users being switched to must "below" the role of the person switching.
 * Switching is done within defined scopes:
 * <ul>
 * <li>{@link MivRole#VICE_PROVOST_STAFF} may switch to anyone</li>
 * <li>{@link MivRole#SCHOOL_STAFF} may switch to anyone at a role below them in their own school</li>
 * <li>{@link MivRole#DEPT_STAFF} may switch to anyone at a role below them in their own department</li>
 * <li>{@link MivRole#OCP_STAFF} may switch only to the Provost or Chancellor roles</li>
 * </ul>
 *
 * @author Rick Hendricks
 * @since MIV 4.0
 */
public class SwitchUserSearchStrategy extends DefaultSearchStrategy
{
    /**
     * Gets a list of users by name to switch to.
     * Overridden basic search for users by name in order to apply a
     * SelectFilter which will eliminate the logged in user and target user from the results as
     * well as further scoping the results to only include persons the current user is allowed to
     * switch to via the authorizedPersons helper method.
     *
     * @param user The target user doing the search
     * @param criteria - The search criteria to use
     * @return - A scoped list of MivPerson objects
     */
    @Override
    public List<MivPerson> getUsersByName(MIVUser user, SearchCriteria criteria)
    {
        MivPerson actor = user.getTargetUserInfo().getPerson();
        List<MivPerson>people = this.getUsersByName(actor, criteria, this.getSelectFilter(user));
        return this.authorizedPersons(actor, people);
    }

    /**
     * Gets a list of users by department to switch to.
     * Overridden basic search for users by department in order to apply a
     * SelectFilter which will eliminate the logged in user and target user from the results as
     * well as further scoping the results to only include persons the current user is allowed to
     * switch to via the authorizedPersons helper method.
     *
     * @param user The target user doing the search
     * @param criteria - The search criteria to use
     * @return A scoped list of MivPerson objects
     */
    @Override
    public List<MivPerson> getUsersByDepartment(MIVUser user, SearchCriteria criteria)
    {
        MivPerson actor = user.getTargetUserInfo().getPerson();
        List<MivPerson>people = this.getUsersByDepartment(actor, criteria, this.getSelectFilter(user));
        return this.authorizedPersons(actor, people);
    }

    /**
     * Gets a list of users by school to switch to.
     * Overridden basic search for users by school in order to apply a
     * SelectFilter which will eliminate the logged in user and target user from the results as
     * well as further scoping the results to only include persons the current user is allowed to
     * switch to via the authorizedPersons helper method.
     *
     * @param user The target user doing the search
     * @param criteria The search criteria to use
     * @return A scoped list of MivPerson objects
     */
    @Override
    public List<MivPerson> getUsersBySchool(MIVUser user, SearchCriteria criteria)
    {
        MivPerson actor = user.getTargetUserInfo().getPerson();
        List<MivPerson>people = this.getUsersBySchool(actor, criteria, this.getSelectFilter(user));
        return this.authorizedPersons(actor, people);
    }

    /**
     * Helper method to authorize that target user is allowed to switch to
     * each MivPerson in the input list. Those MivPersons to which the user is not authorized to switch
     * will be eliminated from the returned list.
     *
     * @param mivPerson The target user
     * @param people The list of MivPerson objects to be authorized
     * @return A list of MivPerson objects to which the target user is allowed to switch.
     */
    private List<MivPerson> authorizedPersons(MivPerson mivPerson, List<MivPerson> people)
    {
        List<MivPerson> scopedPeople = new ArrayList<MivPerson>(people.size());
        AuthorizationService authorizationService = MivServiceLocator.getAuthorizationService();

        for (MivPerson p : people)
        {
            // principal qualifier
            AttributeSet qualification = new AttributeSet();
            qualification.put(Qualifier.USERID, String.valueOf(p.getUserId()));

            if (authorizationService.isAuthorized(mivPerson, Permission.SWITCH_USER, null, qualification))
            {
                scopedPeople.add(p);
            }
        }

        return scopedPeople;
    }
}
