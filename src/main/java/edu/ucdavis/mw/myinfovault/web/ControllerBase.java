/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: ControllerBase.java
 */
package edu.ucdavis.mw.myinfovault.web;

import java.util.HashMap;
import java.util.Map;

import javax.security.auth.login.CredentialException;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections.map.LinkedMap;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ModelAttribute;

import edu.ucdavis.mw.myinfovault.service.apientity.ApiAuthorizerFactory;
import edu.ucdavis.mw.myinfovault.service.apientity.ApiSecurityScheme;
import edu.ucdavis.mw.myinfovault.service.apientity.InternalApiAuthorizer;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.myinfovault.MIVSession;

/**
 * @author japorito
 *
 */
public abstract class ControllerBase
{
    protected final org.slf4j.Logger logger = LoggerFactory.getLogger(this.getClass());

    //protected static Map <String, LinkedMap>breadcrumbMap = new HashMap<String, LinkedMap>();

    // Breadcrumb map by session ID
    protected static Map <String, Map<String, LinkedMap>>breadcrumbSessionMap = new HashMap<String, Map<String, LinkedMap>>();

    @ModelAttribute("authHeader")
    public String getAuthHeader(HttpServletRequest req) throws CredentialException
    {
        MivPerson shadowPerson = MIVSession.getSession(req).getUser().getTargetUserInfo().getPerson();

        //Casted to internal API authorizer for access to the getAuthHeader method.
        InternalApiAuthorizer authorizer = (InternalApiAuthorizer) ApiAuthorizerFactory.getAuthorizer(
                                                "miv-frontend/" + shadowPerson.getUserId(),
                                                ApiSecurityScheme.INTERNAL);

        return authorizer.getAuthHeader();
    }

    /**
     * Add/Remove breadcrumbs as needed for the current page
     *
     * @param label - The label to be used for the breadcrumb
     * @param group - The group is used to segregate breadcrumbs such that
     *                breadcrumbs for a page would only redirect to other pages
     *                within the group.
     * @param req - The request object
     * @return
     */
    public LinkedMap addBreadcrumb(String label, String group,  HttpServletRequest req)
    {

        // Get the breadcrumbs for this sessiom
        if (!breadcrumbSessionMap.containsKey(req.getSession().getId()))
        {
            breadcrumbSessionMap.put(req.getSession().getId(), new HashMap<String, LinkedMap>());
        }

        Map <String, LinkedMap>breadcrumbMap = breadcrumbSessionMap.get(req.getSession().getId());


        // Get the breadcrumbs for this sessions group
        if(!breadcrumbMap.containsKey(group))
        {
            breadcrumbMap.put(group, new LinkedMap());
        }

        LinkedMap breadcrumbs = breadcrumbMap.get(group);

        // See if the breadcrumb label for the current page is present
        if (breadcrumbs.containsKey(label))
        {
            // If breadcrumb label for the current page is present, remove all breadcrumbs which follow it
            while (!breadcrumbs.lastKey().equals(label))
            {
                breadcrumbs.remove(breadcrumbs.lastKey());
            }
        }
        else
        {
            breadcrumbs.put(label, req.getRequestURL().toString());
        }

        // Remove last entry since it is for the current page
        LinkedMap currentPageBreadcrumbs = new LinkedMap(breadcrumbs);
        currentPageBreadcrumbs.remove(currentPageBreadcrumbs.lastKey());

        // Set the page title for the current page to the label of the last breadcrumb
        req.getSession().setAttribute("pageTitle", breadcrumbs.lastKey());
        req.getSession().setAttribute("breadcrumbs", currentPageBreadcrumbs);

        return currentPageBreadcrumbs;
    }

    /**
     * Remove breadcrumbs for a session
     *
     * @param sessionId - The session id for the breadcrumbs to remove
     */
    public static void removeBreadcrumbs(String sessionId)
    {
        breadcrumbSessionMap.remove(sessionId);
    }

}
