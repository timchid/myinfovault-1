/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: ActionApiController.java
 */
package edu.ucdavis.mw.myinfovault.web.spring.action;

import java.util.EnumSet;

import javax.security.auth.login.CredentialException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.hateoas.ResourceSupport;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import edu.ucdavis.mw.myinfovault.domain.action.AcademicAction;
import edu.ucdavis.mw.myinfovault.domain.dossier.Dossier;
import edu.ucdavis.mw.myinfovault.events2.AcademicActionEvent;
import edu.ucdavis.mw.myinfovault.events2.DossierRouteEvent;
import edu.ucdavis.mw.myinfovault.events2.EventDispatcher2;
import edu.ucdavis.mw.myinfovault.service.MivServiceLocator;
import edu.ucdavis.mw.myinfovault.service.apientity.ApiAuthorizer;
import edu.ucdavis.mw.myinfovault.service.apientity.ApiAuthorizerFactory;
import edu.ucdavis.mw.myinfovault.service.apientity.ApiPermission;
import edu.ucdavis.mw.myinfovault.service.apientity.ApiResourceType;
import edu.ucdavis.mw.myinfovault.service.apientity.ApiScope;
import edu.ucdavis.mw.myinfovault.service.dossier.DossierService;
import edu.ucdavis.mw.myinfovault.service.dossier.WorkflowException;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.mw.myinfovault.service.person.MivRole;
import edu.ucdavis.mw.myinfovault.service.person.Scope;
import edu.ucdavis.mw.myinfovault.service.person.UserService;
import edu.ucdavis.mw.myinfovault.web.ApiControllerBase;


/**
 * Controller for the API for managing Academic Actions.
 *
 * @author japorito
 * @since 5.0.0
 */
@RestController
public class ActionApiController extends ApiControllerBase
{
    DossierService dossierService = MivServiceLocator.getDossierService();
    UserService userService = MivServiceLocator.getUserService();

    protected final Logger logger = LoggerFactory.getLogger(this.getClass());
    protected static final EnumSet<MivRole> EDITOR_ROLES = EnumSet.of(MivRole.SYS_ADMIN,
                                                                      MivRole.DEPT_STAFF,
                                                                      MivRole.SCHOOL_STAFF,
                                                                      MivRole.VICE_PROVOST_STAFF,
                                                                      MivRole.API_ENTITY);


    @RequestMapping(value="/api/action", method={RequestMethod.PUT, RequestMethod.POST})
    public ResponseEntity<? extends ResourceSupport> createAction(@RequestBody ActionResource action,
                                                                  @RequestHeader(value="Authorization", required=true) String authHeader) throws CredentialException
    {
        ApiAuthorizer auth = ApiAuthorizerFactory.getAuthorizer(authHeader);

        MivPerson actionUser = userService.getPersonByMivId(action.getUserId());

        if (actionUser == null)
        {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }

        Scope primary = actionUser.getPrimaryAppointment().getScope();

        ApiScope scopeRequired = new ApiScope(ApiResourceType.ACTION,
                                              primary.getSchool(),
                                              primary.getDepartment(),
                                              ApiScope.ANY,
                                              EnumSet.of(ApiPermission.CREATE));

        if (!auth.permits(authHeader, scopeRequired, EDITOR_ROLES))
        {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }

        ActionValidationResource validation = new ActionValidationResource(action);

        if (validation.isValid())
        {
            MivPerson submitUser = userService.getPersonByMivId(action.getSubmitterId());

            //something else will need to be done when we implement action proposals.
            if (!action.getIsProposal()) {
                // Initiate/approve the dossier in workflow
                try
                {
                    AcademicAction newAction = new AcademicAction(actionUser,
                                                                  action.getActionType(),
                                                                  action.getDelegationAuthority(),
                                                                  action.getEffectiveDate(),
                                                                  action.getAccelerationYears());

                    Dossier dossier = null;

                    // This dossier is not currently in workflow so we must initiate
                    dossier = dossierService.initiateDossierRouting(newAction, actionUser);

                    action.setDossierId(dossier.getDossierId());

                    // post academic action event
                    EventDispatcher2.getDispatcher().post(new AcademicActionEvent(submitUser, dossier));

                    //post dossier route event
                    EventDispatcher2.getDispatcher().post(new DossierRouteEvent(
                            submitUser,
                            dossier,
                            dossierService.getInitialWorkflowNode(dossier).getNodeLocation(),
                            dossierService.getCurrentWorkflowNode(dossier).getNodeLocation())
                                          .setShadowPerson(actionUser)
                                          .setComments("Initial Submission"));
                }
                catch (WorkflowException wfe)
                {
                    logger.error("Unable to route dossier for " + actionUser.getPersonId(), wfe);
                    return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
                }
            }

            return new ResponseEntity<ActionResource>(action, HttpStatus.CREATED);
        }
        else
        {
            return new ResponseEntity<ActionValidationResource>(validation, HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value="/api/action", method=RequestMethod.GET)
    public ResponseEntity<ActionResource> blankAction()
    {
        return new ResponseEntity<ActionResource>(new ActionResource(), HttpStatus.OK);
    }
}
