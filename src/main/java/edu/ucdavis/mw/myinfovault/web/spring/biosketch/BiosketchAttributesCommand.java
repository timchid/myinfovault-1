package edu.ucdavis.mw.myinfovault.web.spring.biosketch;

/**
 * Command Object representing a BiosketchAttributes
 * @author dreddy
 * @since MIV 2.1
 */
public class BiosketchAttributesCommand extends BaseCommand
{
    private String programDirectorName;    
    private String programDirectorNameSequence;
    private String programDirectorNameDisplay;
    private String fullName;    
    private String fullNameSequence;
    private String fullNameDisplay;
    private String eraUserName;    
    private String eraUserSequence;
    private String eraUserDisplay;
    private String positionTitle1;    
    private String positionTitle1Sequence;
    private String positionTitle1Display;
    private String positionTitle2;    
    private String positionTitle2Sequence;
    private String positionTitle2Display;
    
    public String getEraUserDisplay()
    {
        return eraUserDisplay;
    }
    public void setEraUserDisplay(String eraUserDisplay)
    {
        this.eraUserDisplay = eraUserDisplay;
    }
    public String getEraUserName()
    {
        return eraUserName;
    }
    public void setEraUserName(String eraUserName)
    {
        this.eraUserName = eraUserName;
    }
    public String getEraUserSequence()
    {
        return eraUserSequence;
    }
    public void setEraUserSequence(String eraUserSequence)
    {
        this.eraUserSequence = eraUserSequence;
    }
    public String getFullName()
    {
        return fullName;
    }
    public void setFullName(String fullName)
    {
        this.fullName = fullName;
    }
    public String getFullNameDisplay()
    {
        return fullNameDisplay;
    }
    public void setFullNameDisplay(String fullNameDisplay)
    {
        this.fullNameDisplay = fullNameDisplay;
    }
    public String getFullNameSequence()
    {
        return fullNameSequence;
    }
    public void setFullNameSequence(String fullNameSequence)
    {
        this.fullNameSequence = fullNameSequence;
    }
    
    public String getProgramDirectorName()
    {
        return programDirectorName;
    }
    public void setProgramDirectorName(String programDirectorName)
    {
        this.programDirectorName = programDirectorName;
    }
    public String getProgramDirectorNameDisplay()
    {
        return programDirectorNameDisplay;
    }
    public void setProgramDirectorNameDisplay(String programDirectorNameDisplay)
    {
        this.programDirectorNameDisplay = programDirectorNameDisplay;
    }
    public String getProgramDirectorNameSequence()
    {
        return programDirectorNameSequence;
    }
    public void setProgramDirectorNameSequence(String programDirectorNameSequence)
    {
        this.programDirectorNameSequence = programDirectorNameSequence;
    }
    
    public String getPositionTitle1()
    {
        return positionTitle1;
    }
    public void setPositionTitle1(String positionTitle1)
    {
        this.positionTitle1 = positionTitle1;
    }
    public String getPositionTitle1Display()
    {
        return positionTitle1Display;
    }
    public void setPositionTitle1Display(String positionTitle1Display)
    {
        this.positionTitle1Display = positionTitle1Display;
    }
    public String getPositionTitle1Sequence()
    {
        return positionTitle1Sequence;
    }
    public void setPositionTitle1Sequence(String positionTitle1Sequence)
    {
        this.positionTitle1Sequence = positionTitle1Sequence;
    }
    public String getPositionTitle2()
    {
        return positionTitle2;
    }
    public void setPositionTitle2(String positionTitle2)
    {
        this.positionTitle2 = positionTitle2;
    }
    public String getPositionTitle2Display()
    {
        return positionTitle2Display;
    }
    public void setPositionTitle2Display(String positionTitle2Display)
    {
        this.positionTitle2Display = positionTitle2Display;
    }
    public String getPositionTitle2Sequence()
    {
        return positionTitle2Sequence;
    }
    public void setPositionTitle2Sequence(String positionTitle2Sequence)
    {
        this.positionTitle2Sequence = positionTitle2Sequence;
    }
}
