/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: ArchiveDossiersForm.java
 */

package edu.ucdavis.mw.myinfovault.web.spring.archivedossiers;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * WebFlow form backing for {@link ArchiveDossiersAction}.
 * 
 * @author Rick Hendricks
 * @since MIV 3.1
 */
public class ArchiveDossiersForm implements Serializable
{
    private static final long serialVersionUID = 890796184476944451L;

    private List<String> toArchive = new ArrayList<String>();

    /**
     * @return the dossiers numbers to archive
     */
    public List<String> getToArchive()
    {
        return this.toArchive;
    }

    /**
     * @param toArchive dossier numbers to archive
     */
    public void setToArchive(List<String> toArchive)
    {
        this.toArchive = toArchive;
    }

    /**
     * Clear list of dossier numbers
     */
    public void clearToArchive()
    {
        this.toArchive.clear();
    }
}
