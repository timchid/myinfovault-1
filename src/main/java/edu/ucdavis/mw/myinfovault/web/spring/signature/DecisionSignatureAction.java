/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: DecisionSignatureAction.java
 */

package edu.ucdavis.mw.myinfovault.web.spring.signature;

import edu.ucdavis.mw.myinfovault.util.AttributeSet;
import org.springframework.webflow.execution.Event;
import org.springframework.webflow.execution.RequestContext;

import edu.ucdavis.mw.myinfovault.domain.decision.Decision;
import edu.ucdavis.mw.myinfovault.domain.dossier.Dossier;
import edu.ucdavis.mw.myinfovault.domain.signature.MivElectronicSignature;
import edu.ucdavis.mw.myinfovault.events2.DecisionSignatureEvent;
import edu.ucdavis.mw.myinfovault.events2.EventDispatcher2;
import edu.ucdavis.mw.myinfovault.service.authorization.AuthorizationService;
import edu.ucdavis.mw.myinfovault.service.authorization.AuthorizationService.PermissionDetail;
import edu.ucdavis.mw.myinfovault.service.authorization.Permission;
import edu.ucdavis.mw.myinfovault.service.decision.DecisionService;
import edu.ucdavis.mw.myinfovault.service.dossier.DossierService;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.mw.myinfovault.service.person.MivRole;
import edu.ucdavis.mw.myinfovault.service.person.Scope;
import edu.ucdavis.mw.myinfovault.service.person.UserService;
import edu.ucdavis.mw.myinfovault.service.signature.SigningService;
import edu.ucdavis.mw.myinfovault.util.MivDocument;
import edu.ucdavis.myinfovault.exception.MivSevereApplicationError;

/**
 * Webflow form action for electronic signatures.
 *
 * @author Craig Gilmore
 * @since 3.0
 */
public class DecisionSignatureAction extends SignatureAction
{
    // flow and request scope parameter keys
    private static final String DOSSIER_PARAM = "dossier";
    private static final String SCHOOL_PARAM = "schoolId";
    private static final String DEPARTMENT_PARAM = "departmentId";
    private static final String ATTRIBUTE_PARAM = "attributeName";

    private final DecisionService decisionService;
    private final DossierService dossierService;

    /**
     * Create the decision action form bean.
     *
     * @param userService Spring injected user service
     * @param authorizationService Spring injected authorization service
     * @param signingService Spring injected signature service
     * @param decisionService Spring injected decision service
     * @param dossierService Spring injected dossier service
     */
    public DecisionSignatureAction(UserService userService,
                                   AuthorizationService authorizationService,
                                   SigningService signingService,
                                   DecisionService decisionService,
                                   DossierService dossierService)
    {
        super(userService,
              authorizationService,
              signingService);

        this.decisionService = decisionService;
        this.dossierService = dossierService;
    }

    /**
     * Allowed to view the decision in the given request context?
     *
     * @param context Web flow request context
     * @return Web flow event status, allowed or denied
     */
    public Event canView(RequestContext context)
    {
        return isAuthorized(context, Permission.VIEW_DECISION);
    }

    /**
     * Allowed to sign the decision in the given request context?
     *
     * @param context Web flow request context
     * @return Web flow event status, allowed or denied
     */
    public Event canSign(RequestContext context)
    {
        return isAuthorized(context, Permission.SIGN_DECISION);
    }

    /**
     * Authorized for the given permission in the given request context?
     *
     * @param context Web flow request context
     * @param permission decision permission
     * @return Web flow event status, allowed or denied
     */
    public Event isAuthorized(RequestContext context, String permission)
    {
        AttributeSet permissionDetails = new AttributeSet();
        permissionDetails.put(PermissionDetail.TARGET_ID, Integer.toString(getFormObject(context).getSignature().getDocument().getDocumentId()));
        permissionDetails.put(PermissionDetail.ACTOR_ID, Integer.toString(getRealPerson(context).getUserId()));

        // is authorized?
        return authorizationService.isAuthorized(getShadowPerson(context),
                                                 permission,
                                                 permissionDetails,
                                                 null)
             ? allowed()
             : denied();
    }

    /**
     * Is the user authorized to upload a decision wet signature?
     *
     * @param context Web flow request context
     * @return Web flow event status, either allowed or denied
     */
    public Event canUpload(RequestContext context)
    {
        DecisionForm form = (DecisionForm) getFormObject(context);

        AttributeSet permissionDetails = new AttributeSet();
        permissionDetails.put(PermissionDetail.TARGET_ID, form.getSignature().getRequestedSigner());
        permissionDetails.put(PermissionDetail.ACTOR_ID, Integer.toString(getRealPerson(context).getUserId()));

        return authorizationService.isAuthorized(this.getShadowPerson(context),
                                                 Permission.UPLOAD_DECISION,
                                                 permissionDetails,
                                                 null)
             ? allowed()
             : denied();
    }

    /**
     * Get the signature/request corresponding to the given values.
     *
     * @param dossier
     * @param schoolId
     * @param departmentId
     * @param requestedSigner
     * @param documentType
     * @param realPerson
     * @return MIV signature/request
     */
    private MivElectronicSignature getSignature(Dossier dossier,
                                                int schoolId,
                                                int departmentId,
                                                MivPerson requestedSigner,
                                                MivDocument documentType,
                                                MivPerson realPerson)
    {
        MivElectronicSignature signature = signingService.getSignature(dossier,
                                                                       schoolId,
                                                                       departmentId,
                                                                       documentType,
                                                                       requestedSigner.getUserId());

        /*
         * Re-request signature for the dean/VP/candidate as they may have
         * been granted the role after the initial signature requests (MIV-3161).
         */
        if (signature == null)
        {
            // request decision signatures
            for (MivElectronicSignature requested : decisionService.requestSignatures(dossier,
                                                                                      documentType,
                                                                                      schoolId,
                                                                                      departmentId,
                                                                                      realPerson))
            {
                // return the requested signature if belonging to the requested signer
                if (requested.getRequestedSignerId() == requestedSigner.getUserId())
                {
                    return requested;
                }
            }

            // no signature exists for the given criteria
            throw new MivSevereApplicationError("errors.signature.request", documentType,
                                                                            requestedSigner,
                                                                            dossier.getDossierId(),
                                                                            new Scope(schoolId, departmentId));
        }

        return signature;
    }

    /*
     * (non-Javadoc)
     * @see org.springframework.webflow.action.FormAction#createFormObject(org.springframework.webflow.execution.RequestContext)
     */
    @Override
    public Object createFormObject(RequestContext context)
    {
        MivPerson realPerson = getRealPerson(context);

        Dossier dossier = (Dossier) context.getConversationScope().get(DOSSIER_PARAM);

        int schoolId = Integer.parseInt(context.getFlowScope().getString(SCHOOL_PARAM));
        int departmentId = Integer.parseInt(context.getFlowScope().getString(DEPARTMENT_PARAM));
        MivDocument documentType = MivDocument.get(context.getFlowScope().getString(ATTRIBUTE_PARAM));

        MivPerson executiveUser = getShadowPerson(context);

        /*
         * Get the real executive user if an OCP admin
         */
        if (executiveUser.hasRole(MivRole.OCP_STAFF))
        {
            executiveUser = userService.getUsers(documentType.getAllowedSigner()).iterator().next();
        }

        // signature/request for the given dossier, scope, user, and document type
        MivElectronicSignature signature = getSignature(dossier,
                                                        schoolId,
                                                        departmentId,
                                                        executiveUser,
                                                        documentType,
                                                        realPerson);

        return new DecisionForm(signature, realPerson);
    }

    /**
     * Sign the dean/vice-provost decision/recommendation.
     *
     * @param context Webflow request context
     * @return Web flow event status, success or error
     */
    public Event sign(RequestContext context)
    {
        MivElectronicSignature signature = getFormObject(context).getSignature();

        /*
         * Decision type must be valid for the decisions action and document type.
         */
        if (!signature.<Decision>getDocument().isValid())
        {
            throw new MivSevereApplicationError("errors.decision.signature", signature.getDocument());
        }

        EventDispatcher2.getDispatcher().post(new DecisionSignatureEvent(getRealPerson(context), signature)
        .setShadowPerson(getShadowPerson(context)));

        return sign(getRealPerson(context), signature)
             ? success()
             : error();
    }

    /**
     * Remove the wet signature upload file.
     *
     * @param context Web flow request context
     */
    public void removeWetSignature(RequestContext context)
    {
        Decision decision = ((DecisionForm) getFormObject(context)).getDecision();

        dossierService.deleteUploadFile(decision.getDossier(),
                                        decision.getSchoolId(),
                                        decision.getDepartmentId(),
                                        decision.getDocumentType().getWetSignatureAttributeName(),
                                        0,
                                        getRealPerson(context),
                                        getShadowPerson(context));
    }

    /**
     * Add bread crumb for decision signature as parent flow
     *
     * @param context Web flow request context
     */
    @Override
    public void addBreadcrumb(RequestContext context)
    {
        DecisionForm form = (DecisionForm) getFormObject(context);

        addBreadcrumbs(context, "Sign " + form.getDecision().getDocumentType().getDescription());
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.web.spring.commons.MivFormAction#loadProperties(org.springframework.webflow.execution.RequestContext)
     */
    @Override
    public void loadProperties(RequestContext context)
    {
        super.loadProperties(context, DEFAULT_SUBSET, "labels", "tooltips");
    }
}
