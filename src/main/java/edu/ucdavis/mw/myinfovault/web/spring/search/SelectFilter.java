/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: SelectFilter.java
 */

package edu.ucdavis.mw.myinfovault.web.spring.search;

import edu.ucdavis.mw.myinfovault.service.SearchFilterAdapter;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.myinfovault.MIVUser;

/**
 * Filter for the "select a users account" searches, removes current user from the results
 * AND remove target user from the results.
 *
 * @author Mary Northup
 * @since 3.0
 */
public class SelectFilter extends SearchFilterAdapter<MivPerson>
{
    private int userId = 0;
    private int targetUserId = 0;

    public SelectFilter(MIVUser user)
    {
        this(user.getUserId(), user.getTargetUserInfo().getPerson().getUserId());
    }

    public SelectFilter(int userId, int targetUserId)
    {
        this.userId = userId;
        this.targetUserId = targetUserId;
    }

    @Override
    public boolean include(MivPerson item)
    {
        return (userId == 0 && targetUserId == 0)
            || (item.getUserId() != userId && item.getUserId() != targetUserId);
    }
}
