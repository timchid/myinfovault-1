package edu.ucdavis.mw.myinfovault.web.spring.biosketch;

import edu.ucdavis.myinfovault.designer.DesignRecordType;

/**
 * Utility class representing a Design record in the command class
 * @author svidya
 *
 */
public class DesignRecord
{
    //indicates the value of checkbox
    private boolean display;
    //indicates is it is displayed in the biosketch document(to show/not show the checkbox)
    private boolean displayable;
    private boolean disabled;
    //indicates the type of record:"record","heading","subheading","dummyheading","attribute"   
    private DesignRecordType type;
    //the value(preview) to be put in the row
    private String preview;
    private String attributeName;
    
    private String year;
    private int id;
    private String recType;
    public DesignRecord()
    {
        super();
    }
    public DesignRecord(boolean displayable, DesignRecordType type)
    {
        super();
        this.displayable = displayable;
        this.type = type;
    }
    public boolean isDisplay()
    {
        return display;
    }
    public void setDisplay(boolean display)
    {
        this.display = display;
    }
    public boolean isDisplayable()
    {
        return displayable;
    }
    public void setDisplayable(boolean displayable)
    {
        this.displayable = displayable;
    }
    public DesignRecordType getType()
    {
        return type;
    }
    public void setType(DesignRecordType type)
    {
        this.type = type;
    }
    public String getPreview()
    {
        return preview;
    }
    public void setPreview(String preview)
    {
        this.preview = preview;
        
        if(preview != null && preview.equals("Honors and Awards"))
        {
        	this.preview = preview;
        }
    }
    public int getId()
    {
        return id;
    }
    public void setId(int id)
    {
        this.id = id;
    }
    public String getYear()
    {
        return year;
    }
    public void setYear(String year)
    {
        this.year = year;
    }
    public String getRecType()
    {
        return recType;
    }
    public void setRecType(String recType)
    {
        this.recType = recType;
    }
    public String getAttributeName()
    {
        return attributeName;
    }
    public void setAttributeName(String attributeName)
    {
        this.attributeName = attributeName;
    }
    
    public boolean isDisabled()
    {
        return disabled;
    }
    public void setDisabled(boolean isDisabled)
    {
        this.disabled = isDisabled;
    }
    
    
}
