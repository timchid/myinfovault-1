/**
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: ViewDossierSnapshotAction.java
 */

package edu.ucdavis.mw.myinfovault.web.spring.viewdossiersnapshots;

import java.text.MessageFormat;
import java.util.Collections;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.webflow.core.collection.MutableAttributeMap;
import org.springframework.webflow.execution.Event;
import org.springframework.webflow.execution.RequestContext;

import edu.ucdavis.mw.myinfovault.domain.action.DossierLocation;
import edu.ucdavis.mw.myinfovault.domain.dossier.Dossier;
import edu.ucdavis.mw.myinfovault.service.authorization.AuthorizationService;
import edu.ucdavis.mw.myinfovault.service.authorization.AuthorizationService.Qualifier;
import edu.ucdavis.mw.myinfovault.service.authorization.Permission;
import edu.ucdavis.mw.myinfovault.service.dossier.DossierService;
import edu.ucdavis.mw.myinfovault.service.dossier.WorkflowException;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.mw.myinfovault.service.person.MivRole;
import edu.ucdavis.mw.myinfovault.service.person.UserService;
import edu.ucdavis.mw.myinfovault.util.AttributeSet;
import edu.ucdavis.mw.myinfovault.web.spring.search.ManageOpenAction;
import edu.ucdavis.mw.myinfovault.web.spring.search.MivActionList;
import edu.ucdavis.mw.myinfovault.web.spring.search.SearchCriteria;
import edu.ucdavis.mw.myinfovault.web.spring.search.SearchStrategy;
import edu.ucdavis.myinfovault.MIVUser;

/**
 * TODO: Add Javadoc
 *
 * @author Rick Hendricks
 *
 */
public class ViewDossierSnapshotAction extends ManageOpenAction
{
    private final Logger logger = Logger.getLogger(this.getClass());
    private static final String AUTH_LOG_PATTERN = "User {0} ({1}) is AUTHORIZED to {2}";
    private static final String NOT_AUTH_LOG_PATTERN = "User {0} ({1}) is ** NOT ** AUTHORIZED to {2}";
    private static final String[] archiveBreadcrumbs = {"View Dossier Archive: Search","Search Results","Archived Dossier"};
    private static final String[] snapshotBreadcrumbs = {"View Dossier Snapshots: Search","Search Results","Dossier Snapshots"};

    /**
     * Creates the form action bean for viewing dossier snapshots.
     *
     * @param searchStrategy Spring-injected search strategy
     * @param authorizationService Spring-injected  service
     * @param userService Spring-injected user service
     * @param dossierService Spring-injected dossier service
     */
    public ViewDossierSnapshotAction(SearchStrategy searchStrategy,
                                     AuthorizationService authorizationService,
                                     UserService userService,
                                     DossierService dossierService)
    {

        super(searchStrategy,
                authorizationService,
                userService,
                dossierService);

    }

    /**
     * Checks if user is authorized to view archives. All roles
     * except DEPT_ASSISTANT are allowed view various archives.
     *
     * @param context Request context from webflow
     * @return Event, success or error
     */
    public Event isAuthorized(RequestContext context)
    {
        MivPerson currentPerson = getShadowPerson(context);

        // Get primary role for current person
        MivRole primaryRole = currentPerson.getPrimaryRoleType();

        // Set permission based on whether or not we are viewing regular snapshots or archives
        // If not viewing snapshots, get the archive viewing permission based on the role of the current person
        String permission = context.getActiveFlow().getId().equalsIgnoreCase("viewdossiersnapshots-flow")
                          ? Permission.VIEW_SNAPSHOTS
                          : getPermission(primaryRole);

        AttributeSet qualification = new AttributeSet();
        qualification.put(Qualifier.USERID,
                          Integer.toString(currentPerson.getUserId()));

        boolean isAuthorized = authorizationService.hasPermission(currentPerson,
                                                                  permission,
                                                                  qualification);

        // log authentication result
        logger.info(MessageFormat.format(isAuthorized ? AUTH_LOG_PATTERN : NOT_AUTH_LOG_PATTERN,
                                         currentPerson.getDisplayName(),
                                         currentPerson.getPersonId(),
                                         permission));

        // return error if not authorized
        if (!isAuthorized) return error();

        // If this user is a candidate, the only archives viewable are his/her own, unless they
        // are a DEAN or DEPT_CHAIR or VICE_PROVOST, therefore we can skip the search criteria and display only
        // archives belonging to the candidate
        if (primaryRole == MivRole.CANDIDATE
         && !currentPerson.hasRole(MivRole.DEAN, MivRole.DEPT_CHAIR, MivRole.VICE_PROVOST))
        {
            /*
             * TODO: is this necessary? setupForm method (already called at this point and
             * has created the SearchCriteria object as the form backing for this action
             */
            SearchCriteria searchCriteria = new SearchCriteria();
            searchCriteria.setInputName(currentPerson.getDisplayName());
            context.getFlowScope().put("searchCriteria", searchCriteria);

            //return "skipSearchCriteria" webflow event
            return getEvent("skipSearchCriteria");
        }

        return success();
    }

    /**
     * Get the dossier snapshots for the selected user and dossier.
     *
     * @param context Request context from webflow
     * @return Event, success or error
     */
    public Event getSnapshots (RequestContext context)
    {
        MutableAttributeMap<Object> flowScope = context.getFlowScope();

        String userName = getParameterValue(context, "userName");

        try
        {
            Dossier dossier = dossierService.getDossier(
                                  getParameterValue(context, "dossierId", 0L));

            // See if this dossier was created prior to Jan 19, 2010. There were no snapshots produced for locations where the dossier
            // was routed  prior to that date
            flowScope.put("routedBefore20100119",
                          SnapshotValidator.isPriorToSnapshotsProducedStartDate(dossier.getSubmittedDate()));

            context.getConversationScope().put("dossier", dossier);  // Want the dossier to be available to any sub-flow

            // Get the snapshots available to this user based on the searchStrategy
            flowScope.put("snapshots",
                          searchStrategy.getSnapshots(getShadowPerson(context), dossier));



        }
        catch (WorkflowException e)
        {
            logger.info("Unable to retrieve dossier snapshots for user " + userName, e);

            return error();
        }

        return success();
    }

    /**
     * Get dossiers for which there are snapshots.
     *
     * @param context Webflow request context
     * @return Webflow event status
     */
    public Event getDossiersForSnapshots(RequestContext context)
    {
        //real logged in MIV user
        MIVUser realUser = getUser(context);

        try
        {
            //get dossier snapshots for the target user
            //set results in request scope
            setResults(context,
                       searchStrategy.dossierSearch(getFormObject(context),
                                                    realUser,
                                                    dossierService.getDossiersForSnapshots(
                                                        realUser.getTargetUserInfo().getPerson())));
        }
        catch (WorkflowException e)
        {
            logger.warn("Unable to retrieve dossier snapshots for " + realUser.getTargetUserInfo().getDisplayName(), e);

            return error();
        }

        return success();
    }

    /**
     * Get inprocess dossiers for a specific candidate in order to retrieve the
     * candidate snapshots.
     *
     * @param context Webflow request context
     * @return Webflow event status
     */
    public Event getCandidateSnapshot(RequestContext context)
    {
        //real logged in MIV user
        MIVUser realUser = getUser(context);

        try
        {
            //get dossier snapshots for the target user
            //set results in request scope
            setResults(context,  searchStrategy.dossierSearch(getFormObject(context),
                                                    realUser,
                                                    dossierService.getDossierByUser(realUser.getTargetUserInfo().getPerson())));
        }
        catch (WorkflowException e)
        {
            logger.warn("Unable to retrieve dossier snapshots for " + realUser.getTargetUserInfo().getDisplayName(), e);

            return error();
        }

        return success();
    }


    /**
     * Get archived dossier snapshots.
     *
     * @param criteria Qualifies the search results
     * @param user Scoped by user
     * @return List of MivActionList objects
     */
     public List<MivActionList> getArchivedDossiers(SearchCriteria criteria, MIVUser user)
     {
         // list all users archived dossier
         if (criteria.isActiveOnly())
         {
             criteria.setActiveOnly(false);
         }

         List<Dossier> archivedDossiers = null;

         try
         {
            // search by school and department
            if (criteria.getDeptSchoolId() > -1
             && criteria.getDepartmentId() > -1)
            {
                archivedDossiers = dossierService.getArchivedDossiersBySchoolAndDepartment(user.getTargetUserInfo().getPerson(),
                                                                                           criteria.getDeptSchoolId(),
                                                                                           criteria.getDepartmentId());
            }
            // search by school
            else if (criteria.getSchoolId() > -1)
            {
                archivedDossiers = dossierService.getArchivedDossiersBySchoolAndDepartment(user.getTargetUserInfo().getPerson(),
                                                                                           criteria.getSchoolId(),
                                                                                           0);
            }
            // search by name and location
            else
            {
                archivedDossiers = dossierService.getDossiersByUserNameSearchCriteriaAndLocation(
                                       user.getTargetUserInfo().getPerson(),
                                       criteria,
                                       DossierLocation.ARCHIVE);
            }

            return searchStrategy.dossierSearch(criteria, user, archivedDossiers);
        }
        catch (WorkflowException e)
        {
            logger.warn("Unable to retrieve archived dossiers for " + user.getTargetUserInfo().getDisplayName(), e);
        }

         // no results
         return Collections.emptyList();
    }

    /**
     * Get the permission based on role.
     *
     * @param role MivRole with which permission is associated
     * @return permission Permission for the given role
     */
    private String getPermission(MivRole role)
    {
        switch(role)
        {
            case SYS_ADMIN:
            case VICE_PROVOST_STAFF:
            case VICE_PROVOST:
                return Permission.VIEW_FULL_ARCHIVES;
            case DEPT_STAFF:
            case SCHOOL_STAFF:
                return Permission.VIEW_ADMIN_ARCHIVES;
            case CANDIDATE:
            default:
                return Permission.VIEW_CANDIDATE_ARCHIVES;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SearchCriteria getFormObject(RequestContext context)
    {
        return (SearchCriteria) super.getFormObject(context);
    }

    /**
     * Add bread crumbs for manage open action as parent flow.
     *
     * @param context Webflow request context
     */
    @Override
    public void addBreadcrumbs(RequestContext context)
    {
        addBreadcrumbs(context, context.getActiveFlow().getId().equalsIgnoreCase("viewdossiersnapshots-flow")
                ? snapshotBreadcrumbs
                : archiveBreadcrumbs);
    }

    /**
     * remove bread crumbs for manage open action as parent flow.
     *
     * @param context Webflow request context
     */
    @Override
    public void removeBreadcrumbs(RequestContext context)
    {
        removeBreadcrumbs(context, (context.getActiveFlow().getId().equalsIgnoreCase("viewdossiersnapshots-flow")
                ? snapshotBreadcrumbs
                : archiveBreadcrumbs).length);
    }

}
