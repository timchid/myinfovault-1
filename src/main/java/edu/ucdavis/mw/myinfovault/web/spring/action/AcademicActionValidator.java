/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: AcademicActionValidator.java
 */

package edu.ucdavis.mw.myinfovault.web.spring.action;

import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.springframework.validation.Errors;

import edu.ucdavis.mw.myinfovault.domain.PolarResponse;
import edu.ucdavis.mw.myinfovault.domain.action.AppointmentDuration;
import edu.ucdavis.mw.myinfovault.domain.action.Assignment;
import edu.ucdavis.mw.myinfovault.domain.action.DossierActionType;
import edu.ucdavis.mw.myinfovault.domain.action.DossierDelegationAuthority;
import edu.ucdavis.mw.myinfovault.domain.action.DossierLocation;
import edu.ucdavis.mw.myinfovault.domain.action.SalaryPeriod;
import edu.ucdavis.mw.myinfovault.domain.action.StatusType;
import edu.ucdavis.mw.myinfovault.domain.action.Title;
import edu.ucdavis.mw.myinfovault.service.person.Scope;
import edu.ucdavis.mw.myinfovault.util.MathUtil;
import edu.ucdavis.mw.myinfovault.util.SelectedFilter;
import edu.ucdavis.mw.myinfovault.util.StringUtil;
import edu.ucdavis.myinfovault.MIVConfig;
import edu.ucdavis.myinfovault.PropertyManager;
import edu.ucdavis.myinfovault.data.Validator;

/**
 * Validates the forms extending {@link AcademicActionForm}.
 *
 * @author Craig Gilmore
 * @since MIV 4.8
 */
public class AcademicActionValidator implements org.springframework.validation.Validator
{
    /*
     * Error message format patterns.
     */
    private static final String ERROR_REQUIRED = "{0}: Required";
    private static final String ERROR_INVALID = "{0}: Invalid";
    private static final String ERROR_ILLEGAL = "{0}: Illegal for {2} {1}";
    private static final String ERROR_EMPTY = "{0}: Must have at least one";
    private static final String ERROR_RANGE_CURRENCY = "{0}: Must be at least {1,number,currency} and no greater than {2,number,currency}";
    private static final String ERROR_ANNUAL_SALARY = "{0}: Must be a whole dollar amount";
    private static final String ERROR_RANGE_INT = "{0}: Must be between {1,number,#} and {2,number,#}, inclusive";
    private static final String ERROR_RANGE_DATE = "{0}: Must be between {1,number,#} and {2,date,MMMM d, YYYY}, inclusive";
    private static final String ERROR_MINIMUM_DATE = "{0}: Must be greater than {1,date,MMMM d, YYYY}";
    private static final String ERROR_INVALID_DATE = "{0} - invalid date: \"{1}\"";
    private static final String ERROR_FOUR_DIGITS = "{0}: Must be four digits";
    private static final String ERROR_MINIMUM = "{0}: Must be at least {1,number,percent}";
    private static final String ERROR_MAXIMUM = "{0}: Must not exceed {1,number,percent}";
    private static final String DUPLICATE_DEPARTMENTS = "{0}: Must be unique.";
    private static final String ERROR_WITHOUT_SALARY = "{0}: Percent Of Time must be zero for WOS (Without Salary).";
    private static final String ERROR_DIVISIBLE = "{0}: Must be divisible by {1,number,0.0}";
    private static final String ERROR_MAXIMUM_DECIMAL = "{0}: Must not exceed {1,number,#0.0}";

    /*
     * Maximum and minimum bounds.
     */
    protected static final BigDecimal MIN_PERIOD_SALARY = BigDecimal.ZERO;
    protected static final BigDecimal MAX_PERIOD_SALARY = new BigDecimal("99999.99");
    protected static final BigDecimal MAX_ANNUAL_SALARY = new BigDecimal("999999");
    private static final int MAX_YEAR = Calendar.getInstance().get(Calendar.YEAR) + 2;
    private static final int MIN_YEAR = 1900;
    private static final BigDecimal MAX_PERCENT = BigDecimal.ONE;
    private static final BigDecimal MIN_PERCENT = BigDecimal.ZERO;
    private static final int MAX_QUARTERS = 24;
    private static final int MAX_ACCELERATION_YEARS = 20;
    private static final int MAX_RANKSTEP_YEARS = 50;
    private static final int MINIMUM = 0;

    /*
     * Field labels.
     */
    private static final String CURRENT_EMPLOYEE;
    private static final String REPRESENTED_EMPLOYEE;
    private static final String UNION_NOTICE_REQUIRED;
    private static final String LABOR_RELATIONS_NOTIFIED;
    private static final String DEPARTMENT;
    private static final String PERCENT_OF_TIME;
    private static final String WITHOUT_SALARY;
    private static final String RANK_AND_TITLE;
    private static final String TITLE_CODE;
    private static final String STEP;
    private static final String APPOINTMENT_DURATION;
    private static final String SALARY_PERIOD;
    private static final String ANNUAL_SALARY;
    private static final String ACTION_TYPE;
    private static final String DELGATION_AUTHORITY;
    private static final String EFFECTIVE_DATE;
    private static final String RETROACTIVE_DATE;
    private static final String END_DATE;
    private static final String PRIMARY_APPOINTMENT;
    private static final String JOINT_APPOINTMENT;
    private static final String APPOINTMENT_TITLE;
    private static final String STATUS_ASSIGNMENTS;
    private static final String ASSIGNMENT_TITLES;
    private static final String QUARTER_COUNT;
    private static final String YEARS_AT_RANK;
    private static final String YEARS_AT_STEP;
    private static final String ACCELERATION_YEARS;

    static {
        Properties labels = PropertyManager.getPropertySet("editAcademicAction-flow-form", "labels");

        CURRENT_EMPLOYEE = labels.getProperty("currentEmployee");
        REPRESENTED_EMPLOYEE = labels.getProperty("representedEmployee");
        UNION_NOTICE_REQUIRED = labels.getProperty("unionNoticeRequired");
        LABOR_RELATIONS_NOTIFIED = labels.getProperty("laborRelationsNotified");
        DEPARTMENT = labels.getProperty("department");
        PERCENT_OF_TIME = labels.getProperty("percentOfTime");
        WITHOUT_SALARY =  labels.getProperty("withoutSalary");
        RANK_AND_TITLE = labels.getProperty("rankAndTitle");
        TITLE_CODE = labels.getProperty("code");
        STEP = labels.getProperty("step");
        APPOINTMENT_DURATION = labels.getProperty("appointmentDuration");
        SALARY_PERIOD = labels.getProperty("salaryPeriod");
        ANNUAL_SALARY = labels.getProperty("annualSalary");
        ACTION_TYPE = labels.getProperty("actionType");
        DELGATION_AUTHORITY = labels.getProperty("delegationAuthority");
        EFFECTIVE_DATE = labels.getProperty("effectiveDate");
        RETROACTIVE_DATE = labels.getProperty("retroactiveDate");
        END_DATE = labels.getProperty("endDate");
        PRIMARY_APPOINTMENT = labels.getProperty("primaryAppointment");
        JOINT_APPOINTMENT = labels.getProperty("jointAppointment");
        APPOINTMENT_TITLE = labels.getProperty("title");
        STATUS_ASSIGNMENTS = "Appointments";
        ASSIGNMENT_TITLES = "Titles";
        QUARTER_COUNT = labels.getProperty("quarterCount");
        YEARS_AT_RANK = labels.getProperty("yearsAtRank");
        YEARS_AT_STEP = labels.getProperty("yearsAtStep");
        ACCELERATION_YEARS = labels.getProperty("accelerationYears");
    }

    /**
     * Field label separator for fields with multiple labels.
     */
    private static final String SEPARATOR = ", ";

    /**
     * Exactly one half of {@link BigDecimal#ONE}.
     */
    private static final BigDecimal ONE_HALF = new BigDecimal(".5");

    /**
     * Exactly one hundred.
     */
    private static final BigDecimal ONE_HUNDRED = new BigDecimal(100);

    private final Map<String, Map<String, String>> departments = MIVConfig.getConfig().getMap("departments");

    /*
     * (non-Javadoc)
     * @see org.springframework.validation.Validator#supports(java.lang.Class)
     */
    @Override
    public boolean supports(@SuppressWarnings("rawtypes") Class clazz)
    {
        return AcademicActionForm.class.isAssignableFrom(clazz);
    }

    /*
     * (non-Javadoc)
     * @see org.springframework.validation.Validator#validate(java.lang.Object, org.springframework.validation.Errors)
     */
    @Override
    public void validate(Object obj, Errors errors)
    {
        AcademicActionForm form = ((AcademicActionForm) obj);

        /*
         * Action type and delegation of authority
         */
        validateAction(form.getActionType(), form.getDelegationAuthority(), errors);

        /*
         * Validate the acceleration years - Only for non-new appointments
         */
        if (form.getActionType() != DossierActionType.APPOINTMENT)
        {
            validateAccelerationYears(form.getAccelerationYears(), errors);
        }

        /*
         * Academic action effective and retroactive dates
         */
        validateDates(form, errors);

        /*
         * Assignment validation
         */
        if (form.getActionType() != DossierActionType.APPOINTMENT || form.getCurrentEmployee() == PolarResponse.YES)
        {
            /*
             * Present assignments.
             */
            validateAssignments(form.getActionType(),
                                StatusType.PRESENT,
                                form.getAssignmentStatuses().get(StatusType.PRESENT),
                                errors);
        }
        /*
         * Proposed assignments.
         */
        validateAssignments(form.getActionType(),
                            StatusType.PROPOSED,
                            form.getAssignmentStatuses().get(StatusType.PROPOSED),
                            errors);

        /*
         * New appointment validation
         */
        if (form.getActionType() == DossierActionType.APPOINTMENT)
        {
            /*
             * New appointment questions
             */
            if (form.getCurrentEmployee() == PolarResponse.NA)
            {
                reject(errors, ERROR_REQUIRED, CURRENT_EMPLOYEE);
            }
            else if (form.getCurrentEmployee() == PolarResponse.YES)
            {
                if (form.getRepresentedEmployee() == PolarResponse.NA)
                {
                    reject(errors, ERROR_REQUIRED, REPRESENTED_EMPLOYEE);
                }
                else if (form.getRepresentedEmployee() == PolarResponse.YES)
                {
                    if (form.getUnionNoticeRequired() == PolarResponse.NA)
                    {
                        reject(errors, ERROR_REQUIRED, UNION_NOTICE_REQUIRED);
                    }
                    else if (form.getUnionNoticeRequired() == PolarResponse.YES)
                    {
                        if (form.getLaborRelationsNotified() == PolarResponse.NA)
                        {
                            reject(errors, ERROR_REQUIRED, LABOR_RELATIONS_NOTIFIED);
                        }
                    }
                }
            }
        }
    }

    /**
     * Validate the action.
     *
     * @param actionType action type
     * @param delegationAuthority delegation of authority
     * @param errors form errors
     */
    protected void validateAction(DossierActionType actionType,
                                  DossierDelegationAuthority delegationAuthority,
                                  Errors errors)
    {
        if (actionType == null)
        {
            reject(errors, ERROR_REQUIRED, ACTION_TYPE);
        }
        else if (delegationAuthority == null)
        {
            reject(errors, ERROR_REQUIRED, DELGATION_AUTHORITY);
        }
        else if (!actionType.isValid(delegationAuthority))
        {
            reject(errors, ERROR_ILLEGAL, DELGATION_AUTHORITY, ACTION_TYPE, actionType.getDescription());
        }
    }

    /**
     * Validate the acceleration years
     *
     * @param accelerationYears
     * @param errors
     */
    protected void validateAccelerationYears(int accelerationYears, Errors errors)
    {
        if (accelerationYears < MINIMUM || accelerationYears > MAX_ACCELERATION_YEARS)
        {
            reject(errors, ERROR_RANGE_INT, ACCELERATION_YEARS, MINIMUM, MAX_ACCELERATION_YEARS);
        }
    }

    /**
     * Validate the effective and retroactive date for the action in the given state.
     *
     * @param form academic action form
     * @param errors form errors
     */
    protected void validateDates(AcademicActionForm form, Errors errors)
    {
        /*
         * Effective date must not be missing.
         */
        if (StringUtils.isBlank(form.getEffectiveDate()))
        {
            reject(errors, ERROR_REQUIRED, EFFECTIVE_DATE);
        }
        /*
         * Effective date must not be invalid.
         */
        else if (form.getEffectiveDateAsDate() == null)
        {
            reject(errors, ERROR_INVALID_DATE, EFFECTIVE_DATE, form.getEffectiveDate());
        }
        else
        {
            Calendar effectiveDate = Calendar.getInstance();
            effectiveDate.setTime(DateUtils.truncate(form.getEffectiveDateAsDate(), Calendar.DATE));

            int effectiveYear = effectiveDate.get(Calendar.YEAR);

            /*
             * Check to make sure the effective year entered is
             * between the minimum year and next year, inclusive.
             */
            if (effectiveYear < MIN_YEAR || effectiveYear > MAX_YEAR)
            {
                reject(errors, ERROR_RANGE_INT, EFFECTIVE_DATE, MIN_YEAR, MAX_YEAR);
            }


            /*
             * Validate end date if not blank.
             */
            if (StringUtils.isNotBlank(form.getEndDate()))
            {
                /*
                 * End date must not be invalid.
                 */
                if (form.getEndDateAsDate() == null)
                {
                    reject(errors, ERROR_INVALID_DATE, END_DATE, form.getEndDate());
                }
                else
                {
                    // Set the end date and compare to effective date
                    Calendar endDate = Calendar.getInstance();
                    endDate.setTime(DateUtils.truncate(form.getEndDateAsDate(), Calendar.DATE));

                    /*
                     * Make sure the end date is greater than the effective date.
                     */
                    if (!endDate.after(effectiveDate))
                    {
                        reject(errors, ERROR_MINIMUM_DATE, END_DATE, form.getEffectiveDateAsDate());
                    }
                }
            }

            /*
             * Validate retroactive date only if not blank and action is in the Vice Provost state.
             */
            if (StringUtils.isNotBlank(form.getRetroactiveDate())
             && (
                     form.getState() == DossierLocation.VICEPROVOST
                  || form.getState() == DossierLocation.POSTSENATEVICEPROVOST))
            {
                /*
                 * Retroactive date must not be invalid.
                 */
                if (form.getRetroactiveDateAsDate() == null)
                {
                    reject(errors, ERROR_INVALID_DATE, RETROACTIVE_DATE, form.getRetroactiveDate());
                }
                else
                {
                    // Set the retroactive date and compare to effective date
                    Calendar retroactiveDate = Calendar.getInstance();
                    retroactiveDate.setTime(DateUtils.truncate(form.getRetroactiveDateAsDate(), Calendar.DATE));

                    /*
                     * Make sure the retroactive date year is greater than minimum year
                     * and not after the effective date year.
                     */
                    effectiveDate.add(Calendar.DAY_OF_MONTH, -1);
                    if ((retroactiveDate.get(Calendar.YEAR) < MIN_YEAR) || retroactiveDate.after(effectiveDate))
                    {
                        reject(errors, ERROR_RANGE_DATE, RETROACTIVE_DATE, MIN_YEAR, effectiveDate.getTime());
                    }
                }
            }
        }
    }


    private void validateAssignments(DossierActionType actionType,
                                     StatusType status,
                                     List<Assignment> assignments,
                                     Errors errors)
    {
        /*
         * Get the list of assignments NOT marked for removal.
         *
         * Note that the assignments are being dumped into a new list before filtering.
         * SearchFilterAdapter is unable to create a new instance of AutoPopulatingList.
         */
        List<Assignment> assignmentsRetained = (new SelectedFilter<Assignment>()).not().apply(new ArrayList<Assignment>(assignments));

        String statusLabel = status.getDescription() + SEPARATOR;

        /*
         * Assignments
         */
        if (assignmentsRetained.isEmpty())
        {
            reject(errors, ERROR_EMPTY, statusLabel + STATUS_ASSIGNMENTS);
        }
        else
        {
            BigDecimal totalAssignmentPercentOfTime = BigDecimal.ZERO;
            Set<Scope> uniqueAssignmentScopes = new HashSet<Scope>();

            for (int i = 0; i < assignmentsRetained.size(); i++)
            {
                Assignment assignment = assignmentsRetained.get(i);

                String assignmentLabel = statusLabel + getLabel(i, assignment);

                totalAssignmentPercentOfTime = totalAssignmentPercentOfTime.add(assignment.getPercentOfTime());

                /*
                 * Assignment scope (appointment)
                 */
                if (departments.get(assignment.getScope().toString()) ==  null)
                {
                    reject(errors, ERROR_INVALID, assignmentLabel + DEPARTMENT);
                }
                else if (!uniqueAssignmentScopes.add(assignment.getScope())) // check for duplicate assignment scope
                {
                    reject(errors, DUPLICATE_DEPARTMENTS, assignmentLabel + DEPARTMENT);
                }

                /*
                 * Assignment percent of time
                 */
                if (assignment.getPercentOfTime().compareTo(MIN_PERCENT) < 0)
                {
                    reject(errors, ERROR_MINIMUM, assignmentLabel + PERCENT_OF_TIME, MIN_PERCENT);
                }

                // get the list of titles NOT marked for removal
                List<Title> titlesRetained = (new SelectedFilter<Title>()).not().apply(new ArrayList<Title>(assignment.getTitles()));

                /*
                 * Assignment titles
                 */
                if (titlesRetained.isEmpty())
                {
                    reject(errors, ERROR_EMPTY, assignmentLabel + ASSIGNMENT_TITLES);
                }
                else
                {
                    BigDecimal totalTitlePercentOfTime = BigDecimal.ZERO;

                    for (int j = 0; j < titlesRetained.size(); j++)
                    {
                        Title title = titlesRetained.get(j);

                        String titleLabel = assignmentLabel + getLabel(j, title);

                        totalTitlePercentOfTime = totalTitlePercentOfTime.add(title.getPercentOfTime());

                        /*
                         * Title description
                         */
                        if (StringUtils.isBlank(title.getDescription()))
                        {
                            reject(errors, ERROR_REQUIRED, titleLabel + RANK_AND_TITLE);
                        }

                        /*
                         * Title code
                         */
                        if (!Validator.isInteger(title.getCode())
                         || title.getCode().trim().length() != 4)
                        {
                            reject(errors, ERROR_FOUR_DIGITS, titleLabel + TITLE_CODE);
                        }

                        /*
                         * Title step
                         */
                        if (title.getStep() != null && title.getStep().getValue() != null)
                        {
                            /*
                             * Only whole number increments are allowed for new appointments; one-half for all other actions.
                             */
                            BigDecimal allowedIncrement = actionType == DossierActionType.APPOINTMENT ? BigDecimal.ONE : ONE_HALF;

                            if (!MathUtil.isDivisibleBy(title.getStep().getValue(), allowedIncrement))
                            {
                                reject(errors, ERROR_DIVISIBLE, titleLabel + STEP, allowedIncrement);
                            }

                            /*
                             * The maximum step is the lowest three-digit number (100) less the allowed increment.
                             */
                            BigDecimal maximumStep = ONE_HUNDRED.subtract(allowedIncrement);

                            if (title.getStep().getValue().compareTo(maximumStep) > 0)
                            {
                                reject(errors, ERROR_MAXIMUM_DECIMAL, titleLabel + STEP, maximumStep);
                            }
                        }

                        /*
                         * Title percent of time
                         */
                        if (title.getPercentOfTime().compareTo(MIN_PERCENT) < 0)
                        {
                            reject(errors, ERROR_MINIMUM, titleLabel + PERCENT_OF_TIME, MIN_PERCENT);
                        }

                        /*
                         *  Title percent of time must be zero for WOS
                         */
                        if (title.isWithoutSalary() && title.getPercentOfTime().compareTo(BigDecimal.ZERO) != 0)
                        {
                            reject(errors, ERROR_WITHOUT_SALARY, titleLabel + WITHOUT_SALARY);
                        }

                        /*
                         * Title appointment duration
                         *
                         * Ignored for the present status of appointment actions.
                         */
                        if ((actionType != DossierActionType.APPOINTMENT || status != StatusType.PRESENT)
                                && (title.getAppointmentDuration() == null || title.getAppointmentDuration() == AppointmentDuration.NA))
                        {
                            reject(errors, ERROR_REQUIRED, titleLabel + APPOINTMENT_DURATION);
                        }

                        /*
                         * Years at Rank and Step
                         *
                         * Ignored for Proposed and Appointments
                         */
                        if (actionType != DossierActionType.APPOINTMENT && status != StatusType.PROPOSED)
                        {
                            /*
                             * Years at rank and step are replaced with "quarter count" for REAPPOINTMENT actions.
                             */
                            if (actionType == DossierActionType.REAPPOINTMENT ||
                                actionType == DossierActionType.APPOINTMENT_INITIAL_CONTINUING)
                            {
                                if (title.getYearsAtRank() < MINIMUM || title.getYearsAtRank() > MAX_QUARTERS)
                                {
                                    reject(errors, ERROR_RANGE_INT, titleLabel + QUARTER_COUNT, MINIMUM, MAX_QUARTERS);
                                }
                            }
                            else
                            {
                                if (title.getYearsAtRank() < MINIMUM || title.getYearsAtRank() > MAX_RANKSTEP_YEARS)
                                {
                                    reject(errors, ERROR_RANGE_INT, titleLabel + YEARS_AT_RANK, MINIMUM, MAX_RANKSTEP_YEARS);
                                }

                                if (title.getYearsAtStep() < MINIMUM || title.getYearsAtStep() > MAX_RANKSTEP_YEARS)
                                {
                                    reject(errors, ERROR_RANGE_INT, titleLabel + YEARS_AT_STEP, MINIMUM, MAX_RANKSTEP_YEARS);
                                }
                            }
                        }

                        /*
                         * Title salary period
                         */
                        if (title.getSalaryPeriod() == null)
                        {
                            reject(errors, ERROR_REQUIRED, titleLabel + SALARY_PERIOD);
                        }
                        else
                        {
                            /*
                             * Title salary or rate
                             */
                            if (title.getPeriodSalary() == null)
                            {
                                reject(errors, ERROR_REQUIRED, titleLabel + title.getSalaryPeriod().getDescription());
                            }
                            else if (title.getPeriodSalary().compareTo(MAX_PERIOD_SALARY) > 0
                                  || title.getPeriodSalary().compareTo(MIN_PERIOD_SALARY) < 0)
                            {
                                reject(errors, ERROR_RANGE_CURRENCY, titleLabel + title.getSalaryPeriod().getDescription(), MIN_PERIOD_SALARY, MAX_PERIOD_SALARY);
                            }

                            /*
                             * Title annual salary
                             */
                            if (title.getSalaryPeriod() ==  SalaryPeriod.MONTHLY)
                            {
                                if (title.getAnnualSalary() == null
                                 || title.getAnnualSalary().compareTo(BigDecimal.ZERO) < 0 )
                                {
                                    reject(errors, ERROR_REQUIRED, titleLabel + ANNUAL_SALARY, ERROR_REQUIRED);
                                }

                                try
                                {
                                    // check for nonzero fractional part
                                    int annualSalary = title.getAnnualSalary().intValueExact();

                                    /*
                                     * Minimum annual salary is one; zero if WOS.
                                     */
                                    BigDecimal minAnnualSalary = title.isWithoutSalary() ? BigDecimal.ZERO : BigDecimal.ONE;

                                    /*
                                     * Annual salary may not be:
                                     *  - greater than maximum
                                     *  - less than zero
                                     *  - less than minimum when not WOS
                                     */
                                    if (annualSalary > MAX_ANNUAL_SALARY.intValue() || annualSalary < minAnnualSalary.intValue())
                                    {
                                        reject(errors, ERROR_RANGE_CURRENCY, titleLabel + ANNUAL_SALARY, minAnnualSalary, MAX_ANNUAL_SALARY);
                                    }
                                }
                                catch (ArithmeticException e)
                                {
                                    reject(errors, ERROR_ANNUAL_SALARY, titleLabel + ANNUAL_SALARY);
                                }
                            }
                        }
                    }

                    /*
                     * Total percent of time for this assignments titles
                     */
                    if (totalTitlePercentOfTime.compareTo(assignment.getPercentOfTime()) > 0)
                    {
                        reject(errors, ERROR_MAXIMUM, assignmentLabel + ASSIGNMENT_TITLES + StringUtil.SPACE + PERCENT_OF_TIME, assignment.getPercentOfTime());
                    }
                }
            }

            /*
             * Total percent of time for assignments
             */
            if (totalAssignmentPercentOfTime.compareTo(MAX_PERCENT) > 0)
            {
                //reject(errors, ERROR_MAXIMUM, statusLabel + STATUS_ASSIGNMENTS + StringUtil.SPACE + PERCENT_OF_TIME, MAX_PERCENT);
                reject(errors, "The total percentage of time in all departments cannot exceed {0,number,percent}", MAX_PERCENT);
            }
        }
    }


    /**
     * Get the label to prepend to error messages on assignment fields.
     *
     * @param index assignment index within its status
     * @param assignment present or proposed assignment
     * @return assignment error message label
     */
    private String getLabel(int index, Assignment assignment)
    {
        StringBuilder builder = new StringBuilder();

        String scopeDescription = assignment.getScopeDescription();

        if (StringUtils.isBlank(scopeDescription))
        {
            if (assignment.isPrimary())
            {
                builder.append(PRIMARY_APPOINTMENT);
            }
            else
            {
                builder.append(JOINT_APPOINTMENT).append(StringUtil.SPACE).append(index);
            }
        }
        else
        {
            builder.append(scopeDescription);
        }

        builder.append(SEPARATOR);

        return builder.toString();
    }


    /**
     * Get the label to prepend to error messages on assignment title fields.
     *
     * @param index title index within its assignment
     * @param title assignment title
     * @return title error message label
     */
    private String getLabel(int index, Title title)
    {
        StringBuilder builder = new StringBuilder();

        if (StringUtils.isBlank(title.getDescription()))
        {
            builder.append(APPOINTMENT_TITLE).append(StringUtil.SPACE).append(index + 1);
        }
        else
        {
            builder.append(title.getDescription());
        }

        builder.append(SEPARATOR);

        return builder.toString();
    }


    /**
     * Add formatted error message to form validation errors.
     *
     * @param errors form validation errors
     * @param pattern error message pattern
     * @param arguments error message pattern arguments
     */
    private void reject(Errors errors, String pattern, Object...arguments)
    {
        errors.reject(null, MessageFormat.format(pattern, arguments));
    }
}
