package edu.ucdavis.mw.myinfovault.web.spring.sandbox;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class SandboxController
{
    /**
    * Respond with the widget sandbox;
    *
    * @return sandbox for playing with widgets
    */
    @RequestMapping(method=RequestMethod.GET, value="/sandbox")
    public ModelAndView getSandbox()
    {
        return new ModelAndView("widget-sandbox");
    }
}
