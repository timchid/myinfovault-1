/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: PacketRequestAction.java
 */

package edu.ucdavis.mw.myinfovault.web.spring.packetrequest;

import java.text.DateFormat;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.mail.MessagingException;

import org.springframework.beans.PropertyEditorRegistry;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.util.StringUtils;
import org.springframework.webflow.execution.Event;
import org.springframework.webflow.execution.RequestContext;

import edu.ucdavis.mw.myinfovault.domain.action.DossierActionType;
import edu.ucdavis.mw.myinfovault.domain.dossier.Dossier;
import edu.ucdavis.mw.myinfovault.domain.packet.PacketRequestActionType;
import edu.ucdavis.mw.myinfovault.dossier.attributes.DossierAppointmentAttributes;
import edu.ucdavis.mw.myinfovault.dossier.attributes.DossierAttributeStatus;
import edu.ucdavis.mw.myinfovault.events2.DossierErrorEvent;
import edu.ucdavis.mw.myinfovault.events2.EventDispatcher2;
import edu.ucdavis.mw.myinfovault.events2.MivEvent;
import edu.ucdavis.mw.myinfovault.events2.PacketRequestCancellationEvent;
import edu.ucdavis.mw.myinfovault.events2.PacketRequestEvent;
import edu.ucdavis.mw.myinfovault.service.authorization.AuthorizationService;
import edu.ucdavis.mw.myinfovault.service.authorization.AuthorizationService.Qualifier;
import edu.ucdavis.mw.myinfovault.service.authorization.Permission;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.mw.myinfovault.service.person.UserService;
import edu.ucdavis.mw.myinfovault.util.AttributeSet;
import edu.ucdavis.mw.myinfovault.web.spring.commons.MivFormAction;
import edu.ucdavis.mw.myinfovault.web.spring.commons.MivPropertyEditors;
import edu.ucdavis.myinfovault.MIVConfig;
import edu.ucdavis.myinfovault.MivServerAwareMailMessage;

/**
 * Used from Manage Open Actions to create and mail Packet Request.
 *
 * @since MIV 5.0
 */
public class PacketRequestAction extends MivFormAction
{
    private static final ThreadLocal<DateFormat> df =
        new ThreadLocal<DateFormat>() {
        @Override protected DateFormat initialValue() {
            return new SimpleDateFormat("yyyy-MM-dd");
        }
    };

    private static final String BREADCRUMB_PATTERN = "Packet Request for {0}.";

    private static final String LOG_PATTERN = "Packet Request email was sent for dossier #{0,number,#} belonging to {1}/{2,number,#}";

    private static final String REQUEST_MESSAGE_PATTERN = "You are receiving this notification from the {0} because your packet data is being requested to initiate your {1} action."+
            "Please follow these steps to submit your packet:\n\n"+
            "* Log in to MyInfoVault at {2}\n"+
            "* Select \"My Packet Requests\" under the “My Dossier” heading, or select \"Manage My Packets\" located under the top navigation menu \"My Packets\"\n"+
            "* Respond to the Packet Request by selecting the appropriate packet from the drop down menu within the Packet Request and select \"Submit\"\n\n"+
            "If you feel you have received this message in error or have questions, please contact your department administrator or the MIV Project Team at miv-help@ucdavis.edu.";

    private static final String CANCEL_REQUEST_MESSAGE_PATTERN = "You are receiving this notification from the {0} because the packet request " +
            "for your {1} action has been canceled and there is no action required by you at this time. "+
            "Your department administrator will request your packet again at a later date, if required.\n\n" +
            "If you feel you have received this message in error or have questions, please contact your " +
            "department administrator or the MIV Project Team at miv-help@ucdavis.edu.";

    private static final String ADDITIONAL_PATTERN = "\n\n\n[Additional Information]\n\n{0}";

    private static final String REQUEST_MAIL_SUBJECT = "Your packet is being requested for your {0} action.";

    private static final String CANCEL_REQUEST_MAIL_SUBJECT = "The packet request for your {0} action has been canceled.";

    /**
     * Create the Packet Request spring form action bean.
     *
     * @param userService Spring-injected user service
     * @param authorizationService Service Spring-injected authorization service
     * @param packetService Spring-injected packet service
     */
    public PacketRequestAction(UserService userService,
                    AuthorizationService authorizationService)
    {
        super(userService, authorizationService);

        EventDispatcher2.getDispatcher().register(this);
    }


    /**
     * Checks if user is authorized to edit this disclosure certificate.
     *
     * @param context Request context from webflow
     * @return Event status, success or error
     */
    public Event isAuthorized(RequestContext context)
    {
        Dossier dossier = getFormObject(context).getDossier();

        AttributeSet qualification = new AttributeSet();

        qualification.put(Qualifier.SCHOOL,
                          Integer.toString(dossier.getPrimarySchoolId()));

        qualification.put(Qualifier.DEPARTMENT,
                          Integer.toString(dossier.getPrimaryDepartmentId()));

        return authorizationService.isAuthorized(getShadowPerson(context),
                                                 Permission.PACKET_REQUEST,
                                                 null,
                                                 qualification)
             ? success()
             : denied();
    }

    public Event isEmailRequired(RequestContext context)
    {
        Dossier dossier = (Dossier) context.getConversationScope().get("dossier");
        if (dossier.getAction().getActionType() == DossierActionType.APPOINTMENT)
        {
            context.getFlashScope().put("skipNotification", true);
            return getEventFactorySupport().event(this, "skipEmail");
        }

        return success();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PacketRequestForm createFormObject(RequestContext context)
    {
        Dossier dossier = (Dossier) context.getConversationScope().get("dossier");
        PacketRequestActionType requestAction = (PacketRequestActionType) context.getConversationScope().get("requestAction");

        // Default to packet request
        String message = REQUEST_MESSAGE_PATTERN;
        String subject = REQUEST_MAIL_SUBJECT;

        if (requestAction == PacketRequestActionType.CANCELED)
        {
            message = CANCEL_REQUEST_MESSAGE_PATTERN;
            subject = CANCEL_REQUEST_MAIL_SUBJECT;
        }

        int schoolId = getParameterValue(context, "schoolId", 0);
    	int departmentId = getParameterValue(context, "departmentId", 0);

    	DossierAppointmentAttributes apptAttr =
    	        new DossierAppointmentAttributes(schoolId, departmentId, true);

        // prepare email message
        String mailMessage = MessageFormat.format(message,
                                              StringUtils.hasText(apptAttr.getDepartmentDescription())
                                            ? apptAttr.getDepartmentDescription().toUpperCase() + " department"
                                            : apptAttr.getSchoolDescription().toUpperCase(),
                                            dossier.getAction().getFullDescription(), MIVConfig.getConfig().getServer());
        /*
         * Load PacketRequest form data.
         */
        return new PacketRequestForm(dossier,
                          getRealPerson(context),
                          getShadowPerson(context),
                          MessageFormat.format(subject, dossier.getAction().getFullDescription()),
                          mailMessage);
    }

    /**
     * Request/Cancel the packet request and send the candidate an email notification.
     *
     * @param context Request context from webflow
     * @return Event status, success or error
     */
    public Event requestPacket(RequestContext context)
    {
        PacketRequestForm packetRequestForm = getFormObject(context);

        PacketRequestActionType requestAction = (PacketRequestActionType) context.getConversationScope().get("requestAction");

        MivPerson realPerson = getRealPerson(context);
        MivPerson shadowPerson = getShadowPerson(context);
        Dossier dossier = getDossier(context);

        String message = packetRequestForm.getEmailMessage();

        // user has added text to the additional information text area
        if (StringUtils.hasText(packetRequestForm.getEmailBody()))
        {
            message += MessageFormat.format(ADDITIONAL_PATTERN, packetRequestForm.getEmailBody());
        }

        // send email and post the packet request event
        String sentTo = null;
        try
        {
            // Skip email for New Appointment actions
            if (dossier.getAction().getActionType() != DossierActionType.APPOINTMENT)
            {
                MivServerAwareMailMessage email = new MivServerAwareMailMessage(packetRequestForm.getDossier().getAction().getCandidate(),
                        getShadowPerson(context),
                        getRealPerson(context),
                        packetRequestForm.getEmailSubject());
                // add CC address if available
                if (StringUtils.hasText(packetRequestForm.getEmailCc()))
                {
                    email.setCc(packetRequestForm.getEmailCc());
                }

                // update TO address if something other than the default was entered
                if (!packetRequestForm.getEmailTo().equals(packetRequestForm.getEmailToDefault()))
                {
                    email.setTo(packetRequestForm.getEmailTo());
                }
                sentTo = email.getTo();

                packetRequestForm.setEmailSent(email.send(message));
            }

            // Post the packet request/cancellation event
            switch (requestAction)
            {
                case REQUESTED:
                    dossier.setAttributeStatus("packet_request", DossierAttributeStatus.REQUESTED, dossier.getPrimarySchoolId() , dossier.getPrimaryDepartmentId());
                    EventDispatcher2.getDispatcher().post(
                            new PacketRequestEvent(realPerson,
                                    dossier).
                                    setComments("Packet Request for Candidate "+dossier.getAction().getCandidate().getDisplayName()+" - "+dossier.getAction().getFullDescription()).
                                    setShadowPerson(shadowPerson));
                    break;
                case CANCELED:
                    dossier.setAttributeStatus("packet_request", DossierAttributeStatus.CANCEL, dossier.getPrimarySchoolId() , dossier.getPrimaryDepartmentId());
                    EventDispatcher2.getDispatcher().post(
                            new PacketRequestCancellationEvent(realPerson,
                                    dossier).
                                    setComments("Canceled Packet Request for Candidate "+dossier.getAction().getCandidate().getDisplayName()+" - "+dossier.getAction().getFullDescription()).
                                    setShadowPerson(shadowPerson));
                    break;
                default:
            }
        }
        catch (MessagingException e)
        {
            logger.error("Failed to send packet request email", e);

            getFormErrors(context).reject(null, "Failed to send packet request email to one or more recipients. Make sure the email address(es) are correct.");

            MivEvent dossierErrorEvent =
                    new DossierErrorEvent((MivEvent)context.getFlowScope().get("parentEvent"), getDossier(context), e)
            .setShadowPerson(getShadowPerson(context))
            .setComments("Failed to send packet request email to "
                    +sentTo);
            EventDispatcher2.getDispatcher().post(dossierErrorEvent);

            return error();
        }

        // log packet request mail sent
        if (logger.isInfoEnabled())
        {
            logger.info(MessageFormat.format(LOG_PATTERN,
                    packetRequestForm.getDossier().getDossierId(),
                    packetRequestForm.getDossier().getAction().getCandidate().getDisplayName(),
                    packetRequestForm.getDossier().getAction().getCandidate().getUserId()));
        }

        return success();
    }

    /*
     * (non-Javadoc)
     * @see org.springframework.webflow.action.FormAction#registerPropertyEditors(org.springframework.beans.PropertyEditorRegistry)
     */
    @Override
    protected void registerPropertyEditors(PropertyEditorRegistry registry)
    {
        registry.registerCustomEditor(DossierActionType.class, new MivPropertyEditors.ActionTypePropertyEditor());
        registry.registerCustomEditor(Date.class, new CustomDateEditor(df.get(), false));
        registry.registerCustomEditor(PacketRequestActionType.class, new MivPropertyEditors.EnumPropertyEditor<>(PacketRequestActionType.class));
    }

    /**
     * Add bread crumb for packet request form.
     *
     * @param context Webflow request context
     */
    public void addBreadcrumbs(RequestContext context)
    {
        Dossier dossier = getFormObject(context).getDossier();

        addBreadcrumbs(context,
                       MessageFormat.format(BREADCRUMB_PATTERN,
                                            dossier.getAction().getFullDescription()));
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.web.spring.commons.MivFormAction#getFormObject(org.springframework.webflow.execution.RequestContext)
     */
    @Override
    public PacketRequestForm getFormObject(RequestContext context)
    {
        return (PacketRequestForm) super.getFormObject(context);
    }
}
