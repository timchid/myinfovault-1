/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: CreateUserPackets.java
 */

package edu.ucdavis.mw.myinfovault.web.util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import edu.ucdavis.mw.myinfovault.dao.annotation.AnnotationDao;
import edu.ucdavis.mw.myinfovault.domain.annotation.Annotation;
import edu.ucdavis.mw.myinfovault.domain.annotation.AnnotationLine;
import edu.ucdavis.mw.myinfovault.domain.dossier.Dossier;
import edu.ucdavis.mw.myinfovault.domain.packet.Packet;
import edu.ucdavis.mw.myinfovault.domain.packet.PacketContent;
import edu.ucdavis.mw.myinfovault.service.MivServiceLocator;
import edu.ucdavis.mw.myinfovault.service.dossier.DossierService;
import edu.ucdavis.mw.myinfovault.service.dossier.WorkflowException;
import edu.ucdavis.mw.myinfovault.service.packet.PacketService;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.mw.myinfovault.service.person.MivRole;
import edu.ucdavis.mw.myinfovault.service.person.Scope;
import edu.ucdavis.mw.myinfovault.service.person.UserService;
import edu.ucdavis.mw.myinfovault.util.DateUtil;
import edu.ucdavis.mw.myinfovault.web.MIVServlet;
import edu.ucdavis.myinfovault.MIVConfig;
import edu.ucdavis.myinfovault.MIVUserInfo;
import edu.ucdavis.myinfovault.data.DataFetcher;
import edu.ucdavis.myinfovault.data.DataMap;
import edu.ucdavis.myinfovault.data.QueryTool;
import edu.ucdavis.myinfovault.document.Builder.BuilderDocumentType;
import edu.ucdavis.myinfovault.document.Document;
import edu.ucdavis.myinfovault.document.PacketBuilder;


/**
 *This servlet creates packets for MIV users for the MIV 5.0 conversion.
 *
 *If the user has an action currently in process, the Packet will be named for the name of the in process action.
 *If there is no action in process for the user, the Packet will be named "Template"
 *
 *This is a one off conversion, so there is no menu option, invoke with ../miv/util/BuildUserPackets
 *
 * @author Rick Hendricks
 * @since MIV 5.0
 */
public class CreateUserPackets extends MIVServlet
{
    private static final long serialVersionUID = 1L;

    Logger logger = Logger.getLogger(this.getClass());

    private static final Map<String, Map<String, String>> sectionidByNameMap = MIVConfig.getConfig().getMap("sectionidbyname");
    private static final DossierService dossierService = MivServiceLocator.getDossierService();
    private static final PacketService packetService = MivServiceLocator.getPacketService();
    private static final UserService userService = MivServiceLocator.getUserService();
    private static final AnnotationDao annotationDao = (AnnotationDao) MivServiceLocator.getBean("annotationDao");
    private static final QueryTool qt = MIVConfig.getConfig().getQueryTool();
    private static MivPerson loggedInPerson;


    @Override
    public void init(ServletConfig config)
        throws ServletException
    {
        super.init(config);
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response)
    {
        ServletOutputStream out = null;
        loggedInPerson =
                MivServiceLocator.getUserService().getPersonByMivId(mivSession.get().getUser().getTargetUserInfo().getLoginUser().getUserId());

        try
        {
            out = response.getOutputStream();
            response.setContentType("text/html");

            prologue(out);
            if (! loggedInPerson.hasRole(MivRole.SYS_ADMIN))
            {
                out.println("<p>You are not authorized to use this feature.</p>");
                epilogue(out);
                return;
            }

            out.println("<h2>Create an initial Packet for user(s)</h2>");
            out.println("<p>Create an intial Packet for a user(s).</p>");
            out.println("<p>Enter the user ids for which to build a packet..</p>");
            out.println("<div id='queryform'>");
            out.println(" <form name='dossierquery' method='GET' action='CreateUserPackets'>");
            out.println("  <input type='checkbox' name='doAll' value='doAll'>Process all eligable users<br><br>");
            out.println("  <label for='userid'>User ID(s): </label>");
            out.println("  <textarea name='userids' id='userids' rows='25' cols='90' wrap='soft'/></textarea>");
            out.println("  <input type='submit' value='Create'>");

            out.println(" </form>\r\n</div><!--queryform-->");

            out.println("<div id=\"results\">");

            String message = null;
            String userIds = request.getParameter("userids");
            String doAll = request.getParameter("doAll");

            List<MivPerson>persons = new ArrayList<>();

            if (doAll != null)
            {
                userIds = null;
                persons = userService.getUsersByRole(MivRole.CANDIDATE, Scope.AnyScope);
                persons.addAll(userService.getUsersByRole(MivRole.APPOINTEE, Scope.AnyScope));
            }

            if (userIds != null && !userIds.isEmpty())
            {
                for (String userId : Arrays.asList(userIds.replaceAll(","," ").replaceAll("\\s+"," ").replaceAll(" ",",").split(",")))
                {
                    MivPerson person = userService.getPersonByMivId(Integer.parseInt(userId));
                    if (person != null)
                    {
                        persons.add(person);
                        continue;
                    }
                    logger.info("Unable to locate userId: "+userId);
                    out.println("Unable to locate userId: "+userId+"</p>");
                }
            }

            if (persons.isEmpty())
            {
                out.println("<p>No users entered to process.</p>");
                epilogue(out);
                return;
            }

            out.println("<p>Processing "+persons.size()+" users.</p>");

            int processed = 0;

            for (MivPerson person : persons)
            {

                message = "Processing user "+(++processed)+" of "+persons.size()+" - "+person.getDisplayName()+" ("+person.getUserId()+"/"+person.getPrimaryRoleType()+")";
                logger.info("\n\n"+message);
                out.println("<hr><p>"+message+"</p>");

                // Get the user ID
                if (person != null )
                {
                    if (!person.isActive())
                    {
                        message = "***** Packet creation skipped for inactive user "+person.getDisplayName();
                        logger.error(message);
                        out.println("<p>"+message+"</p>");
                        continue;
                    }

                    if (!packetService.getPackets(person.getUserId()).isEmpty())
                    {
                        message = "***** User "+person.getDisplayName()+ " already has a packet...skipping";
                        logger.error(message);
                        out.println("<p>"+message+"</p>");
                        continue;
                    }

                    List<Dossier> dossiers = new ArrayList<>();
                    try
                    {
                        dossiers = dossierService.getDossier(person, true);
                    }
                    catch (IllegalStateException | WorkflowException e)
                    {
                        message = "***** Unable to retrieve dossier for " + person.getDisplayName();
                        logger.error(message,e);
                        out.println("<p>"+message+"</p>");
                        continue;
                    }

                    // Should only be one or none in process
                    boolean isTemplate = dossiers.isEmpty();

                    message ="Creating "+(isTemplate ? "TEMPLATE":dossiers.get(0).getAction().getDescription()+" ("+dossiers.get(0).getLocation()+")") +" Packet for user "+person.getDisplayName();
                    logger.info(message);
                    out.println(message+"<br>");

                    String packetName = isTemplate ? "Template Packet" : dossiers.get(0).getAction().getDescription();

                    // Create the packet object for the user.
                    Packet packet = this.createPacket(new Packet(person.getUserId(), packetName), person, isTemplate);

                    if (packet == null)
                    {
                        message = "**** Packet creation failed for "+person.getDisplayName();
                        logger.error(message);
                        out.println("<p>"+message+"</p>");
                        continue;
                    }

                    packet = packetService.updatePacket(packet, loggedInPerson.getUserId());

                    if (packet == null || packet.getPacketId() <= 0)
                    {
                        message = "**** Packet update failed for "+person.getDisplayName();
                        logger.error(message);
                        out.println("<p>"+message+"</p>");
                        continue;
                    }

                    message = "Packet '"+packet.getPacketName()+"' (ID="+packet.getPacketId()+") created: "+packet.getPacketItems().size()+" content items";
                    logger.info(message);
                    out.println(message+"<br>");

                    message = "Processing annotations...";
                    logger.info(message);
                    out.println(message+"<br>");
                    boolean annotationSuccess = this.updateAnnotations(packet, isTemplate);
                    message = "Annotations processing completed "+(annotationSuccess ? "without errors" : "WITH ERRORS");
                    logger.info(message);
                    out.println(message+"<br>");

                    message  = packetName+" packet creation complete for "+person.getDisplayName();
                    logger.info(message);
                    out.println(message+"<br>");

                }
            }
            epilogue(out);
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    void prologue(ServletOutputStream out)
    {
        try {
            out.println("<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01//EN\" \"http://www.w3.org/TR/html4/strict.dtd\">");
            out.println("<html>\r\n<head>");
            out.println(" <title>Create Packets</title>");
            out.println(" <link rel='stylesheet' type='text/css' href='../myinfovault.css'>");
            out.println(" <link rel='shortcut icon' href='../images/favicon.ico' type='image/x-icon'>");
            out.println(" <style>");
            out.println("  div.resultTable {");
            out.println("    margin-bottom: 1.5em;");
            out.println("  }");
            out.println(" label {display:block;}");
            out.println(" textarea { display:block;}");
            out.println(" </style>");
            out.println("</head>\r\n<body>");
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    void epilogue(ServletOutputStream out)
    {
        try {
            out.println("</body>\r\n</html>");
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }


    int beginTable(ServletOutputStream out, String... columns)
        throws IOException
    {
        int colcount = 0;
        out.println("\n<table border=\"1\" cellpadding=\"3\" cellspacing=\"0\">");
        out.print("  <thead>\n    <tr>");
        for (String s : columns)
        {
            out.print("<th>" + s + "</th>");
            colcount++;
        }
        out.println("</tr>\n  </thead>\n  <tbody>");
        return colcount;
    }

    void finishTable(ServletOutputStream out) throws IOException
    {
        out.println("  </tbody>\n</table>\n");
    }

    private boolean updateAnnotations(Packet packet, boolean isTemplate)
    {

        Map<String, Annotation>annotationMap = annotationDao.getAnnotations(packet.getUserId(), packet.getPacketId());
        boolean success = true;

        for (String key : annotationMap.keySet())
        {
            StringBuilder packetNotations = new StringBuilder();
            Annotation annotation = annotationMap.get(key);

            if (annotation.getNotation() != null)
            {
                packetNotations.append(annotation.getNotation().contains("*") ? "*":"" ).append(annotation.getNotation().contains("x") ? "x":"" );
            }

            if (packetNotations.length() > 0)
            {
                Annotation packetAnnotation = new Annotation(0,
                        annotation.getUserId(),
                        annotation.getSectionBaseTable(),
                        annotation.getRecordId(),
                        packet.getPacketId(),
                        null,
                        packetNotations.toString(),
                        true,//annotation.getDisplay(),
                        new ArrayList<AnnotationLine>());

                success = annotationDao.insertAnnotation(packetAnnotation, loggedInPerson.getUserId());

                logger.info(success
                        ? "Inserted a new PACKET annotation: '"+packetAnnotation.getNotation()+"'"
                        : "FAILED to insert a new PACKET annotation: '"+packetAnnotation.getNotation()+"' - Master Annotation record update skipped.");

                if (success)
                {
                    // Now update the master annotation if needed
                    String masterNotations = annotation.getNotation().replace("*", "").replace("x", "");
                    if (!masterNotations.equals(annotation.getNotation()))
                    {
                        String[] annotationUpdateFields = { "RecordID", "SectionBaseTable", "Footnote",
                                                            "Notation", "Display", "UpdateTimestamp", "UpdateUserID"};
                        String[] values = { Integer.toString(annotation.getRecordId()), annotation.getSectionBaseTable(), annotation.getFootnote(),
                                            masterNotations, "1", DateUtil.getSqlSafeCurrentDateTime(), Integer.toString(loggedInPerson.getUserId()) };
                        int updateCount = qt.update("Annotations", annotation.getId(), annotationUpdateFields, values);
                        logger.info("Update of MASTER annotation record "+annotation.getRecordId()+" from '"+annotation.getNotation()+"' to '"+masterNotations+"' "+(updateCount > 0 ? "successful" : "FAILED"));
                    }
                }
            }
        }
        return success;

    }

    private Packet createPacket(Packet packet, MivPerson person, boolean isTemplate)
    {
        // Get the DataFetcher for this user

        MIVUserInfo mui = userService.getMivUserByPrincipalName(person.getPrincipal().getPrincipalName());
        if (mui == null)
        {
            logger.error("Unable to retrieve MIVUserInfo for "+person.getDisplayName());
            return null;
        }

        DataFetcher df = new DataFetcher(mui);

        if (df == null)
        {
            logger.error("Unable to retrieve DataFetcher for "+person.getDisplayName());
            return null;
        }

        // Get the documents from the master packet
        PacketBuilder packetBuilder = new PacketBuilder(BuilderDocumentType.PACKET, packet, person);
        Map<Document,DataMap>documents = packetBuilder.getDocuments();

        // Iterate through the documents and add to the packet
        for (Document document : documents.keySet())
        {
            DataMap dataMap = df.fetch(document.getDocumentId(), packet.getPacketId());

            for (String sectionName : dataMap.keySet())
            {
                DataMap recordMap = (DataMap) dataMap.get(sectionName);

                String sectionId = sectionidByNameMap.get(sectionName).get("id");

                for (String recordName : recordMap.keySet())
                {
                    // Skips annotations-legends records
                    if (!(recordMap.get(recordName) instanceof LinkedList))
                    {
                        continue;
                    }

                    @SuppressWarnings("unchecked")
                    List<DataMap>recordList = (List<DataMap>) recordMap.get(recordName);

                    for (DataMap record : recordList)
                    {
                        StringBuilder displayParts = new StringBuilder();
                        String recordId = (String) record.get("id");

                        // If we are processing a additional header record, skip it
                        if (record.containsKey("addheader"))
                        {
                            continue;
                        }

                        // If we are processing a additional information record, set the sectionId = 0
                        int sectionIdNum =  record.containsKey("addcontent") ? 0 : Integer.parseInt(sectionId);

//                        if (isTemplate)
//                        {
//                            displayParts.append(record.containsKey("display") ? "display" : "")
//                            .append((record.containsKey("displaycontribution") && (record.get("contribution") != null || record.get("significance") != null) ) ? (displayParts.length() > 0 ? "," : "")+"displaycontribution" : "")
//                            .append((record.containsKey("displaydescription") && record.get("description") != null ) ? (displayParts.length() > 0 ? "," : "")+"displaydescription" : "");
//                        }
//                        else
//                        {
                            displayParts.append(record.get("display") == null ? "" : record.get("display").equals("true") ? "display" :"")
                            .append(record.get("displaycontribution") == null  ? "" : (record.get("displaycontribution").equals("true") && (record.get("contribution") != null || record.get("significance") != null)) ? (displayParts.length() > 0 ? "," : "")+"displaycontribution" :"")
                            .append(record.get("displaydescription") == null ? "" : (record.get("displaydescription").equals("true") &&  record.get("description") != null ) ? (displayParts.length() > 0 ? "," : "")+"displaydescription" :"");
//                        }
                        // Only add a packet item if there is something set to display
                        if (displayParts.length() > 0)
                        {
                            packet.addContentItem(new PacketContent(packet.getPacketId(), person.getUserId(),  sectionIdNum, Integer.parseInt(recordId), displayParts.toString()));
                        }
                    }
                }
            }
        }
        return packet;
    }
}
