/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: SessionTimeoutHandler.java
 */

package edu.ucdavis.mw.myinfovault.web;

import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This is currently experimental only.
 * From "How can my application get to know when a HttpSession is removed (when it time-outs)?"
 * at http://www.jguru.com/faq/view.jsp?EID=12141
 *
 * According to the answerer, Kent Johnson, at the above linked question...
 * Create one of these objects when a person logs in, in {@link edu.ucdavis.mw.myinfovault.web.MIVLogin MIVLogin}
 * and add it to the session as a session variable.
 * When the session times out, the "valueUnbound" method will be called.
 * Do whatever is important there.
 *
 * Note that HttpSessionBindingListener is really more general than that. If you use this
 * as a key/value pair and change the value, valueUnbound and valueBound
 *
 * @author Stephen Paulsen
 *
 */
public class SessionTimeoutHandler implements HttpSessionBindingListener
{
    private static final Logger logger = LoggerFactory.getLogger(SessionTimeoutHandler.class);

    private final int userId;
    private final String login;
    private final String info;

    public SessionTimeoutHandler(int userId, String login)
    {
        this.userId = userId;
        this.login = login;
        this.info = login + "/" + userId;
    }

    /* (non-Javadoc)
     * @see javax.servlet.http.HttpSessionBindingListener#valueBound(javax.servlet.http.HttpSessionBindingEvent)
     */
    @Override
    public void valueBound(HttpSessionBindingEvent event)
    {
        // Don't care about this for this purpose. Do Nothing.
        String nameOfAttribute = event.getName();
        Object value = event.getValue();
        logger.info("|| ------> Binding attribute named [{}] with value of [{}]", nameOfAttribute, value);
    }


    /* (non-Javadoc)
     * @see javax.servlet.http.HttpSessionBindingListener#valueUnbound(javax.servlet.http.HttpSessionBindingEvent)
     */
    @Override
    public void valueUnbound(HttpSessionBindingEvent event)
    {
        // TODO Auto-generated method stub
        // This gets called when your session times out (or when you log out?)
//        HttpSession whichSessionIsTerminating = event.getSession();
//        logger.info("|| ----> whichSessionIsTerminating is {}", whichSessionIsTerminating);
        // session is not-null, but can't get anything from it because it's already been invalidated

        String nameOfAttribute = event.getName();
        Object value = event.getValue();
        logger.info("|| -----> UN-Binding attribute named [{}] with value of [{}]", nameOfAttribute, value);
        logger.info("|| -- poke something to UPDATE the UserLogin table WHERE userId={} AND login='{}'", this.userId, this.login);
    }

    @Override
    public String toString()
    {
        return "Session Timeout Handler for " + this.info;
    }
}
