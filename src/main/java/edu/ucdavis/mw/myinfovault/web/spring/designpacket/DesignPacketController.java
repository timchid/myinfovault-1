/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: DesignPacketController.java
 */


package edu.ucdavis.mw.myinfovault.web.spring.designpacket;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.RequestContextUtils;

import edu.ucdavis.mw.myinfovault.dao.annotation.AnnotationDao;
import edu.ucdavis.mw.myinfovault.domain.annotation.Annotation;
import edu.ucdavis.mw.myinfovault.domain.packet.Packet;
import edu.ucdavis.mw.myinfovault.service.MivServiceLocator;
import edu.ucdavis.mw.myinfovault.service.packet.PacketService;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.mw.myinfovault.web.ControllerBase;
import edu.ucdavis.mw.myinfovault.web.ForbiddenException;
import edu.ucdavis.myinfovault.MIVConfig;
import edu.ucdavis.myinfovault.MIVSession;
import edu.ucdavis.myinfovault.MIVUser;
import edu.ucdavis.myinfovault.MIVUserInfo;
import edu.ucdavis.myinfovault.PropertyManager;
import edu.ucdavis.myinfovault.SectionHeaderUtil;
import edu.ucdavis.myinfovault.data.DisplaySection;
import edu.ucdavis.myinfovault.data.DocumentIdentifier;
import edu.ucdavis.myinfovault.data.QueryTool;
import edu.ucdavis.myinfovault.data.SectionFetcher;
import edu.ucdavis.myinfovault.designer.DesignDocument;
import edu.ucdavis.myinfovault.designer.DisplayOptions;
import edu.ucdavis.myinfovault.format.AnnotationFormatter;
import edu.ucdavis.myinfovault.format.CandidateStatementDesignPacketFormatter;
import edu.ucdavis.myinfovault.format.RecordFormatter;
import edu.ucdavis.myinfovault.htmlcleaner.HtmlCleaner;
import edu.ucdavis.myinfovault.htmlcleaner.InvalidMarkupException;

/**
 * This servlet pulls the information from all the documents except CV and NIH
 * and gives option to include or not to include the records in the packet.
 *
 * @author venkat
 */
@Controller
public class DesignPacketController extends ControllerBase
{
    private final MIVConfig mivConfig = MIVConfig.getConfig();

    private static final String DESIGN_PACKET = "/packet/design";
    private static final String EDIT_HEADERS = DESIGN_PACKET + "/headers";
    private static final PacketService packetService = MivServiceLocator.getPacketService();

    private static final Properties strings = PropertyManager.getPropertySet("packetDesign-form", "strings");
    private static final Properties tooltips = PropertyManager.getPropertySet("packetDesign-form", "tooltips");

    private static final String BREADCRUMB_GROUP = "packetmanagement";

    @ModelAttribute("sessionUser")
    public MIVUser getUser(HttpServletRequest req)
    {
        return MIVSession.getSession(req).getUser();
    }

    @RequestMapping(value=DESIGN_PACKET, method=RequestMethod.GET)
    public ModelAndView doGet(HttpServletRequest req,
                              HttpServletResponse res,
                              @ModelAttribute("sessionUser") MIVUser sessionUser,
                              @ModelAttribute("authHeader") String authHeader,
                              @RequestParam(value="success", defaultValue="false") String success,
                              @RequestParam(value="CompletedMessage", required=false) String completedMessage,
                              @RequestParam(value="packetId", required=false) Long packetId)
    {
        logger.trace("\n ***** DesignPacketController doGet called! *****\t\tGET\n");
        ModelAndView mav = new ModelAndView("design");

        addBreadcrumb("Design My Packet", BREADCRUMB_GROUP, req);

        success = success.trim();

        MIVUserInfo mui = sessionUser.getTargetUserInfo();
        mav.addObject("userId", mui.getPerson().getUserId());
        mav.addObject("strings", strings);
        mav.addObject("tooltips", tooltips);

        if (packetId != null)
        {
            Packet packet = packetService.getPacket(packetId);
            if (packet.getUserId() == mui.getPerson().getUserId())
            {
                mav.addObject("packet", packet);
                mav.addObject("packetid", packetId);
            }
            else
            {
                throw new ForbiddenException();
            }
        }
        else
        {
            Map <String, ?>inputFlashMap = RequestContextUtils.getInputFlashMap(req);

            packetId = (inputFlashMap == null || inputFlashMap.get("packetid") == null) ? 0L : (Long)inputFlashMap.get("packetid");
        }

        collectRecords(mui, req, res, packetId);

        if (completedMessage != null)
        {
            if (success.equalsIgnoreCase("true"))
            {
                String msg;
                try
                {
                    msg = (new HtmlCleaner()).removeTags(completedMessage);
                    if (msg.toLowerCase().contains("deleted"))
                    {
                        msg = "<div id=\"deletebox\">" + msg + "</div>";
                    }
                    else
                    {
                        msg = "<div id=\"successbox\">" + msg + "</div>";
                    }
                }
                catch (InvalidMarkupException e)
                {
                    if (completedMessage.toLowerCase().contains("deleted"))
                    {
                        msg = "<div id=\"deletebox\">Record Deleted Successfully</div>";
                    }
                    else
                    {
                        msg = "<div id=\"successbox\">Record Saved Successfully</div>";
                    }
                }
                mav.addObject("CompletedMessage", msg);
                mav.addObject("success", success);
                mav.addObject("authHeader", authHeader);
            }
        }

        return mav;
    }


    /* Queries used in "collectRecords" (CR_) below. */
    private static final String CR_DOCUMENT_QUERY =
        "SELECT DocumentID, Description FROM CollectionDocument" +
        " JOIN Document ON Document.ID=DocumentID" +
        " WHERE CollectionID=1" + // The Packet is collection 1
        " ORDER BY Sequence ASC";

    private static final String CR_SECTION_QUERY =
        "SELECT s.id,s.Name, s.RecordName" +
        " FROM DocumentSection d, Section s" +
        " WHERE DocumentID = ? AND d.SectionID = s.ID" +
        " ORDER BY Sequence ASC";

    /**
     * This method pulls the information for all the sections listed in section table, creates
     * the custom object with that data, and puts that object in the response object.
     *
     * @param mui -- used to create sectionfetcher and get the userid.
     * @param req -- HttpServletRequest instance.
     * @param res -- HttpServletResponse instance.
     */
    private void collectRecords(MIVUserInfo mui, HttpServletRequest req, HttpServletResponse res, Long packetId)
    {
        Map<String,Object> config = new HashMap<String,Object>();
        config.put("options", mivConfig.getConstants());

        QueryTool qt = mivConfig.getQueryTool();

        List<Map<String, String>> documents = qt.getList(CR_DOCUMENT_QUERY, (String[]) null);
        List<DesignDocument> designDocumentList = new LinkedList<DesignDocument>();

        boolean committeeHeaderPrinted = false;

        SectionFetcher sf = mui.getSectionFetcher();
        Map<String,Map<String,String>> sectionHeaderMap = sf.getHeaderMap();

        // Get all annotations
        AnnotationDao annotationdao = (AnnotationDao) MivServiceLocator.getBean("annotationDao");
        Map<String, Annotation> annotationRecordMap = annotationdao.getAnnotations(mui.getPerson().getUserId(), packetId);

        for (Map<String, String> document : documents)
        {
            String docIdString = document.get("documentid");
            int intDocID = Integer.parseInt(docIdString);
            DocumentIdentifier docID = DocumentIdentifier.convert(intDocID);

            // MIV-3890
            // Skip Contributions, they are incorporated into Publications on the design page.
            if (docID.equals(DocumentIdentifier.CONTRIBUTIONS_TO_JOINTLY_AUTHORED_WORKS) ||
                docID.equals(DocumentIdentifier.CONTRIBUTIONS_TO_JOINTLY_CREATED_WORKS))
            {
                continue;
            }

            // Org Chart is not a Section, so no need to include here.
            if(docID.equals(DocumentIdentifier.ORG_CHART))
            {
                continue;
            }

            String description = document.get("description");

            List<DisplayOptions> displayOptionsList = new LinkedList<DisplayOptions>();
            List<Map<String, String>> sections = qt.getList(CR_SECTION_QUERY, docIdString);

            boolean showSectionHeader = (sections != null && sections.size() > 1);

            for (Map<String, String> records : sections)
            {
                String recordName = records.get("recordname");
                String section = records.get("name");
                String sectionID = records.get("id");

                Map<String, String> heading = sectionHeaderMap.get(section);
                if (heading == null)
                {
                    // Log an error, make a "Header Missing" header
                    logger.error("Missing a header for the section \"{}\" with ID {}", section, sectionID);
                    // Put in a fake heading so user can continue
                    heading = new HashMap<String, String>(1);
                    heading.put("header", "Missing header for " + section);
                }
                String sectionHeading = heading.get("header");

                int i = recordName.lastIndexOf('-');
                String recType = recordName.substring(0, i);

                String subtype = req.getParameter("subtype");
                if (subtype == null) subtype = "";
                if (subtype.length() > 0) {
                    subtype = "-" + subtype.toLowerCase();
                }

                Properties pcfg = PropertyManager.getPropertySet(recType, "config");
                Properties pstr = PropertyManager.getPropertySet(recType+subtype, "strings");
                Properties pclass = PropertyManager.getPropertySet(recType+subtype, "classes");

                config.put("strings", pstr);
                config.put("config", pcfg);
                config.put("classes", pclass);


                RecordFormatter[] f = new RecordFormatter[2];

                switch (docID)
                {
                    case CANDIDATES_STATEMENT:
                        f[0] = new CandidateStatementDesignPacketFormatter(req.getContextPath());
                        break;
                    case PUBLICATIONS:
                        /*f[0]=new PreviewExtender(new String[]{"link"});*/
                        f[1] = new AnnotationFormatter(recType, section, packetId, "publications", annotationRecordMap);
                        break;
                    case CREATIVE_ACTIVITIES:
                        /*f[0]=new PreviewExtender(new String[]{"link"});*/
                        f[1] = new AnnotationFormatter(recType, section, packetId, "creativeactivities", annotationRecordMap);
                        break;
                    case EVALUATIONS:
                        /*f[0]= new PreviewExtender(new String[]{"link"});*/
                        f[1] = new AnnotationFormatter(recType, section, packetId, "evaluation", annotationRecordMap);
                        break;
                    case SERVICE:
                        /*f[0]=new PreviewExtender(new String[]{"link"});*/
                        f[1] = new AnnotationFormatter(recType, section, packetId, "service", annotationRecordMap);
                        break;
                    case EXTENDING_KNOWLEDGE:
                        /*f[0]=new PreviewExtender(new String[]{"link"});*/
                        f[1] = new AnnotationFormatter(recType, section, packetId, "extendingknowledge", annotationRecordMap);
                        break;
                    /*case TEACHING:
                        f[0]=new PreviewExtender(new String[]{"link"});
                        f[1] = new AnnotationFormatter(recType, section, "teaching", annotationRecordMap);
                        break;*/
                    default:
                        f = null;
                        break;
                }

                List<DisplaySection> displaySectionList = sf.getRecordSection(recType, section, null, f);

                if (displaySectionList != null)
                {
                    DisplayOptions displayOptions =
                        new DisplayOptions(recType, section, sectionHeading, displaySectionList,
                                                       docID, mui.getPerson().getUserId(), showSectionHeader);
                    /*
                     * The committeeHeaderPrinted is defaulted to false, there are multiple sections with type committee (campus,systemwide etc)
                     * but we should show committees heading in the jsp only once, so the variable is set to true,
                     * and we set ShowCommitteesHeading to true only once, for all other cases it will be defaulted to false.
                     */
                    if (recType.equalsIgnoreCase("committee") && !committeeHeaderPrinted)
                    {
                        displayOptions.setShowCommitteesHeading(true);
                        committeeHeaderPrinted = true;
                    }

                    boolean printSectionHeader = false;
                    Map<String,String>  sectionMap = sectionHeaderMap.get(section);
                    if (sectionMap != null && sectionMap.size() > 0)
                    {
                        String displayHeading = sectionMap.get("display");
                        printSectionHeader = "true".equals(displayHeading) || "1".equals(displayHeading);

                    }

                    displayOptions.setPrintSectionHeader(printSectionHeader);
                    displayOptions.setDisableSectionHeader();
                    displayOptions.setSectionID(sectionID);

                    displayOptionsList.add(displayOptions);
                }
            }

            DesignDocument designDocument = new DesignDocument(docID, description, displayOptionsList);
            designDocumentList.add(designDocument);
        }

        req.setAttribute("documentsList", designDocumentList);
        req.setAttribute("constants", config);
    }
    // collectRecords()

    /**
     * This updates the header in the UserSectionHeader if it is available, otherwise it inserts a new record into UserSectionHeader table, when the header is included to packet.
     *
     * @param req -- HttpServletRequest
     * @param updateUserID -- the user's id who is updating the records.
     * @param headers -- list of PacketHeaderResource objects to modify
     * @return response entity which sets the response status
     */
    @RequestMapping(value=EDIT_HEADERS, method=RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<?> saveHeaders(HttpServletRequest req,
                                         @RequestBody List<PacketHeaderResource> headers)
    {
        MivPerson shadowPerson = MIVSession.getSession(req).getUser().getTargetUserInfo().getPerson();
        if (validateHeaders(headers, shadowPerson))
        {
            for (PacketHeaderResource header : headers)
            {
                if (header.getSectionId() == 0)
                {
                    SectionHeaderUtil.updateAdditionalSectionHeader(header.getAdditionalInfoHeaderId(),
                                                                    shadowPerson.getUserId(),
                                                                    header.getLabel(),
                                                                    header.getIncluded());
                }
                else
                {
                    SectionHeaderUtil.updateUserSectionHeader(header.getUserId(),
                                                              header.getSectionId(),
                                                              header.getLabel(),
                                                              shadowPerson.getUserId(),
                                                              header.getIncluded());
                }
            }

            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    public boolean validateHeaders(List<PacketHeaderResource> headers, MivPerson shadowPerson)
    {
        for (PacketHeaderResource header : headers)
        {
            if (header.getSectionId() == null ||
                header.getIncluded() == null ||
                header.getLabel() == null)
            {
                return false;
            }

            if (header.getSectionId() == 0)
            {
                if (header.getAdditionalInfoHeaderId() == null) return false;

                if (SectionHeaderUtil.getAdditionalSectionHeader(header.getAdditionalInfoHeaderId(),
                                                                 shadowPerson.getUserId()) == null)
                {
                    return false;
                }
            }
            else if (header.getUserId() == null || header.getUserId() != shadowPerson.getUserId())
            {
                return false;
            }
        }

        return true;
    }
}
