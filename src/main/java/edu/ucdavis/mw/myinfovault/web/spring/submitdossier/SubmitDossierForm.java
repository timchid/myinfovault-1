/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: SubmitDossierForm.java
 */

package edu.ucdavis.mw.myinfovault.web.spring.submitdossier;

import java.io.Serializable;

import edu.ucdavis.mw.myinfovault.domain.action.DossierActionType;
import edu.ucdavis.mw.myinfovault.domain.dossier.Dossier;
import edu.ucdavis.mw.myinfovault.util.DateUtil;
import edu.ucdavis.mw.myinfovault.web.spring.action.AcademicActionForm;

/**
 * Form backing object for the {@link SubmitDossierAction}.
 *
 * @author Rick Hendricks
 * @since MIV 3.0
 */
public class SubmitDossierForm extends AcademicActionForm implements Serializable
{
    private static final long serialVersionUID = 5662675903458463116L;

    private final Dossier dossier;

    /**
     * Create the SubmitDossier form backing bean for a new action.
     */
    protected SubmitDossierForm()
    {
        super(DossierActionType.MERIT);

        this.dossier = null;
    }

    /**
     * Create the SubmitDossier form backing bean.
     *
     * @param dossier dossier to submit
     */
    protected SubmitDossierForm(Dossier dossier)
    {
        super(dossier);

        this.dossier = dossier;
    }

    /**
     * @return dossier to submit or <code>null</code> if new submission
     */
    public Dossier getDossier()
    {
        return dossier;
    }

    /**
     * @return effective date year
     */
    public int getEffectiveDateYear()
    {
        return DateUtil.getYear(getEffectiveDateAsDate());
    }

}
