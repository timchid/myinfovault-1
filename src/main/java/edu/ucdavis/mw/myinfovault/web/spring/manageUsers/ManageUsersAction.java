/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: ManageUsersAction.java
 */

package edu.ucdavis.mw.myinfovault.web.spring.manageUsers;

import org.springframework.webflow.execution.Event;
import org.springframework.webflow.execution.RequestContext;

import edu.ucdavis.mw.myinfovault.service.authorization.AuthorizationService;
import edu.ucdavis.mw.myinfovault.service.person.UserService;
import edu.ucdavis.mw.myinfovault.web.spring.commons.MivFormAction;

/**
 * Form action for the manage users flow
 *
 * @author Craig Gilmore
 * @since 3.0
 */
public class ManageUsersAction extends MivFormAction
{
    /**
     * Create the form action for the manage users flow.
     *
     * @param userService
     * @param authorizationService
     */
    public ManageUsersAction(UserService userService,
                             AuthorizationService authorizationService)
    {
        super(userService, authorizationService);
    }

    /**
     * Add the bread crumb appropriate for the child flow.
     *
     * @param context Webflow request context
     * @return Webflow event status
     */
    public Event addBreadcrumb(RequestContext context)
    {
        //add manage users bread crumb
        addBreadcrumbs(context, "Manage Users");

        return success();
    }
}
