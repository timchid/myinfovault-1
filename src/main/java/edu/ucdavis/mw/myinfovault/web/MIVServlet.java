/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: MIVServlet.java
 */

package edu.ucdavis.mw.myinfovault.web;

import java.io.BufferedReader;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.logging.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.LoggerFactory;

import edu.ucdavis.mw.myinfovault.util.HtmlUtil;
import edu.ucdavis.mw.myinfovault.util.HttpServletUtil;
import edu.ucdavis.myinfovault.MIVConfig;
import edu.ucdavis.myinfovault.MIVSession;
import edu.ucdavis.myinfovault.data.MIVUtil;

@SuppressWarnings("serial") // This warning *shouldn't* occur (but does) because an abstract class is not serializable.
public abstract class MIVServlet extends HttpServlet
{
    protected final String className = this.getClass().getSimpleName();
    /** @deprecated use the SLF4J "logger" instead of this "log" */ @Deprecated
    protected static Logger log = Logger.getLogger("MIV");
    protected final org.slf4j.Logger logger = LoggerFactory.getLogger(this.getClass());

    protected static final ThreadLocal<MIVSession> mivSession = new ThreadLocal<MIVSession>();
    protected MIVConfig mivConfig = null;

    private static ThreadLocal<Boolean> isAjaxRequest =
        new ThreadLocal<Boolean>() {
            @Override protected Boolean initialValue() {
                return false;
            }
        };


    /**
     * Get the MIVSession object from the request.
     * Redirect to the main MIV page, potentially forcing a login, if no session exists.
     *
     * @param req
     * @param res
     * @return The MIV session object or <code>null</code> if there is none.
     * @throws IOException
     */
    protected MIVSession getSession(HttpServletRequest req, HttpServletResponse res)
        throws IOException
    {
        MIVSession ms = MIVSession.getSession(req);

        if (ms == null || ! ms.hasSession())
        {
//            if (ms == null) System.out.println("\n\n"+className+": MIVSession is NULL");
//            else if (! ms.hasSession()) System.out.println("\n\n"+className+": MIVSession.hasSession() is FALSE");
            if (ms == null) {
                logger.info("{} : MIVSession is NULL", className);
            }
            else if (! ms.hasSession()) {
                logger.info("{} : MIVSession.hasSession() is FALSE", className);
            }
            //System.out.println(""+className+": Redirecting to /logout.html\n");
            logger.info("{} : Redirecting to /logout.html", className);
            URL logoutTargetUrl = new URL("http", req.getServerName(), "/logout.html");
            res.sendRedirect(logoutTargetUrl.toString());
            return null;
        }

        // Make sure all servlets treat the input stream (especially form input) at UTF-8 characters.
        // req.setCharacterEncoding("iso-8859-1");
        // res.setCharacterEncoding("utf-8");
        // Made these variables so we can modify them in the debugger.
        // String inputEncoding = "ISO-8859-1";
        // Changing input encoding to UTF-8 fixes MIV-2133 but be on the lookout for it breaking other forms.
        // If this *does* break other things, consider changing PersonalForm.doPost() to use getRequestMap()
        // and requestdata.get() as seen in EditRecord.doPost()
        String inputEncoding = "UTF-8";
        String outputEncoding = "UTF-8";
        req.setCharacterEncoding(inputEncoding);
        res.setCharacterEncoding(outputEncoding);

        return ms;
    }


    /**
     * Convenience method to invoke the RequestDispatcher to send the request on for further processing.
     * This method takes care of getting the {@link javax.servlet.RequestDispatcher RequestDispatcher}
     * and forwarding the request, logging and re-throwing exceptions.
     *
     * @param targetUrl
     * @param req
     * @param res
     * @throws ServletException
     * @throws IOException
     */
    protected void dispatch(String targetUrl, HttpServletRequest req, HttpServletResponse res)
        throws ServletException, IOException
    {
        logger.debug("Dispatching from {} to [{}]", className, targetUrl);
        try {
            RequestDispatcher dispatcher = req.getRequestDispatcher(targetUrl);
            dispatcher.forward(req, res);
        }
        catch (ServletException se) {
            logger.error("SERVLET Error trying to dispatch to ["+targetUrl+"]", se);
            throw(se);
        }
        catch (IOException ioe) {
            logger.error("I/O Error trying to dispatch to ["+targetUrl+"]", ioe);
            throw(ioe);
        }
    }


    /**
     * Convenience method to invoke the RequestDispatcher to send the request on for further processing.
     * This method takes care of getting the {@link javax.servlet.RequestDispatcher RequestDispatcher}
     * and forwarding the request, logging and re-throwing exceptions.
     *
     * @param includeUrl
     * @param req
     * @param res
     * @throws ServletException
     * @throws IOException
     */
    protected void include(String includeUrl, HttpServletRequest req, HttpServletResponse res)
        throws ServletException, IOException
    {
        logger.debug("Including file [{}] from {}", includeUrl, className);
        try {
            RequestDispatcher dispatcher = req.getRequestDispatcher(includeUrl);
            dispatcher.include(req, res);
        }
        catch (ServletException se) {
            logger.error("SERVLET Error trying to include file ["+includeUrl+"]", se);
            throw(se);
        }
        catch (IOException ioe) {
            logger.error("I/O Error trying to include file ["+includeUrl+"]", ioe);
            throw(ioe);
        }
    }

// NO LONGER USED
//
//    /**
//     * New code should not use this method. Instead, throw an {@link MivSevereApplicationError} (or subclass)
//     * with the message you want to display. Optionally, add a message to <tt>errormessages.properties</tt>
//     * and use the message-key along with any message parameters.
//     * @param message
//     * @param req
//     * @param res
//     * @deprecated Throw an MivSevereApplicationError instead of calling showError. Add a message to "errormessages.properties".
//     */
//    @Deprecated
//    protected void showError(String message, HttpServletRequest req, HttpServletResponse res)
//    {
//        StringBuilder urlString = new StringBuilder("/pages/errorpage.jsp");
//
//        message = MIVUtil.escapeMarkup(message);
//        req.setAttribute("error", "message");
//        req.setAttribute("from", this.className);
//        req.setAttribute("message", message);
//
//        logger.warn("Sending user {} to error page. Message is [{}]", mivSession.get().getUser().getUserId(), message);
//
//        try {
//            res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
//            dispatch(urlString.toString(), req, res);
//        }
//        catch (ServletException e) {
//            logger.error("Error while trying to display an error!", e);
//        }
//        catch (IOException e) {
//            logger.error("Error while trying to display an error!", e);
//        }
//    }


    @Override
    public void init(ServletConfig c) throws ServletException
    {
        if (MIVConfig.isInitialized())
        {
            mivConfig = MIVConfig.getConfig(c.getServletContext());
        }
    }


    // If we make this final we can do our standard processing then call doGet, doPost etc. ourself.
    // In that case we wouldn't have to have all the MIVServlet subclasses call getSession().
    /**
     * Always perform our MIV standard processing before doGet(), doPost() etc. and
     * prevent MIVServlet subclasses from overriding the service() method, which HttpServlets
     * shouldn't do.
     */
    @Override
    protected final void service(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException
    {
        // do our things here, then call superclass for standard request handling

        /* In case "init()" couldn't get the config object because it wasn't yet initialized. */
        if (mivConfig == null) {
            try {
                mivConfig = MIVConfig.getConfig();
            }
            catch (Exception e) {
                String target = "/MIVLogin";
                this.dispatch(target, request, response);
            }
        }

        MIVSession ms = this.getSession(request, response);
        if (ms == null) return;
        mivSession.set(ms);

        // Indicate whether or not this is an AJAX request
        isAjaxRequest.set(HttpServletUtil.isAjaxRequest(request));
        if (isAjaxRequest.get()) {
            logger.debug("This is assumed to be an AJAX request and, therefore, client-side JavaScript is enabled.");
        }

        response.setHeader("cache-control", "no-cache, must-revalidate");
        //response.addHeader("Pragma", "no-cache");
        response.setDateHeader("Expires", System.currentTimeMillis()-2);

        super.service(request, response);
    }


    /**
     * Check whether or not the current request is from AJAX.
     * A request is assumed to be from an ajax call if the "[X-]Requested-With" header was found.
     * @return true if the current request is (thought to be) from an AJAX call.
     */
    protected boolean isAjaxRequest()
    {
        return isAjaxRequest.get();
    }


    /* ------------------------- Historical copies of 'getRequestMap()' follow ------------------------- */

    /**
     * FIXME: needs javadoc
     * @param req
     * @return
     */
    protected final Map<String, String[]> getRequestMap(HttpServletRequest req)
    {
        return getRequestMap(req, true);
    }


    /**
     * Version from prior to v4.4 Harcs development - we collect the request parameters ourselves by reading the POST body.
     */
    protected final Map<String, String[]> getRequestMap_version_A(HttpServletRequest req, boolean decode)
        throws IOException
    {
        /*
         * Read the Request body. Since we are using request.getReader, any call to request.getParameter(param)
         * would return null.
         */
        Map<String, ArrayList<String>> reqDataTemp = new HashMap<String, ArrayList<String>>();
        BufferedReader br = req.getReader();
        String reqBody = br.readLine();
        if (reqBody == null)
        {
            //log.severe("No request data was received when trying to add/save a record.");
            logger.warn("No request data was received when trying to add/save a record.");
            //TODO: Should this also show the system error page?
            return null;
        }

        String[] reqParams = reqBody.split("&");
        for (String reqParam : reqParams)
        {
            String[] paramKeyValue = reqParam.split("=");
            String paramKey = paramKeyValue[0];
            String paramValue = "";
            if (paramKeyValue.length == 2)
            {
                paramValue = paramKeyValue[1];
                if (decode)
                {
                    paramValue = MIVUtil.decodeReqParamValue(paramValue);
                }
            }

            ArrayList<String> paramValues = null;
            if (reqDataTemp.get(paramKey) != null && reqDataTemp.get(paramKey).size() > 0)
            {
                paramValues = reqDataTemp.get(paramKey);
                paramValues.add(paramValue);
            }
            else
            {
                paramValues = new ArrayList<String>();
                paramValues.add(paramValue);
            }
            reqDataTemp.put(paramKey, paramValues);
        }

        Map<String, String[]> reqData = new HashMap<String, String[]>();
        Iterator<String> iter = reqDataTemp.keySet().iterator();
        while (iter.hasNext())
        {
            String key = iter.next();
            ArrayList<String> reqDataList = reqDataTemp.get(key);
            String[] reqDataArray = new String[reqDataList.size()];
            int i = 0;
            for (String reqValue: reqDataList)
            {
                reqDataArray[i] = reqValue;
                i++;
            }
            reqData.put(key, reqDataArray);
        }

        return reqData;
    }

    /**
     * Version from first part of Harcs development, but with all the commented-out code
     * that had been left in taken out.  This version had a problem with percent signs
     * in the form data because the standard HttpServletRequest req.getParameterMap()
     * URL-decodes parameters, then we were running our own decodeReqParamValue() also.
     */
    @SuppressWarnings("unchecked")
    protected final Map<String, String[]> getRequestMap_version_B(HttpServletRequest req, boolean decode)
    {
        Map<String, String[]> reqDataTemp = req.getParameterMap();
        System.out.println("reqDataTemp :: "+reqDataTemp);

        if (reqDataTemp == null)
        {
            //log.severe("No request data was received when trying to add/save a record.");
            logger.warn("No request data was received when trying to add/save a record.");
            //TODO: Should this also show the system error page?
            return null;
        }

        Map<String, String[]> reqData = new HashMap<String, String[]>();
        for (String key : reqDataTemp.keySet())
        {
            String[] reqDataArray = reqDataTemp.get(key);

            if (decode)
            {
                for (int index = 0; index < reqDataArray.length; index++)
                {
                    reqDataArray[index] = MIVUtil.decodeReqParamValue(reqDataArray[index]);
                }
            }
            reqData.put(key, reqDataArray);
        }

        return reqData;
    }


    /**
     * Version after correcting the problem with the percent double-decoding.
     * This still has tons of commented-out code and is hard to understand.
     */
    @SuppressWarnings("unchecked")
    protected final Map<String, String[]> getRequestMap_version_C(HttpServletRequest req, boolean decode)
        //throws IOException
    {
    /*
     * Read the Request body. Since we are using request.getReader, any call to request.getParameter(param)
     * would return null.

        Map<String, ArrayList<String>> reqDataTemp = new HashMap<String, ArrayList<String>>();
        BufferedReader br = req.getReader();
        String reqBody = br.readLine();
        if (reqBody == null)
        {
            //log.severe("No request data was received when trying to add/save a record.");
            logger.warn("No request data was received when trying to add/save a record.");
            //TODO: Should this also show the system error page?
            return null;
        }

        String[] reqParams = reqBody.split("&");
        for (String reqParam : reqParams)
        {
            String[] paramKeyValue = reqParam.split("=");
            String paramKey = paramKeyValue[0];
            String paramValue = "";
            if (paramKeyValue.length == 2)
            {
                paramValue = paramKeyValue[1];
                if (decode)
                {
                    paramValue = MIVUtil.decodeReqParamValue(paramValue);
                }
            }

            ArrayList<String> paramValues = null;
            if (reqDataTemp.get(paramKey) != null && reqDataTemp.get(paramKey).size() > 0)
            {
                paramValues = reqDataTemp.get(paramKey);
                paramValues.add(paramValue);
            }
            else
            {
                paramValues = new ArrayList<String>();
                paramValues.add(paramValue);
            }
            reqDataTemp.put(paramKey, paramValues);
        }

        Map<String, String[]> reqData = new HashMap<String, String[]>();
        Iterator<String> iter = reqDataTemp.keySet().iterator();
        while (iter.hasNext())
        {
            String key = iter.next();
            ArrayList<String> reqDataList = reqDataTemp.get(key);
            String[] reqDataArray = new String[reqDataList.size()];
            int i = 0;
            for (String reqValue: reqDataList)
            {
                reqDataArray[i] = reqValue;
                i++;
            }
            reqData.put(key, reqDataArray);
        }*/

        Map<String, String[]> reqData = req.getParameterMap();

        if (reqData == null)
        {
            //log.severe("No request data was received when trying to add/save a record.");
            logger.warn("No request data was received when trying to add/save a record.");
            //TODO: Should this also show the system error page?
            return null;
        }

        return reqData;

        /*Map<String, String[]> reqData = new HashMap<String, String[]>();
        for (String key : reqDataTemp.keySet())
        {
            String[] reqDataArray = reqDataTemp.get(key);

            if (decode)
            {
                for (int index = 0; index < reqDataArray.length; index++)
                {
                    //reqDataArray[index] = MIVUtil.decodeReqParamValue(reqDataArray[index]);
                    reqDataArray[index] = reqDataArray[index];
                }
            }
            reqData.put(key, reqDataArray);
        }
        return reqData;*/
    }



    /**
     * Current Version, after fixing percent decode problem and cleaning out all the old junk.
     * There's basically nothing left.
     * AdditionalEditRecord and SaveThesisRecord call getRequestMap(req, false)
     * All other calls are to getRequestMap(req) which implies getRequestMap(req, true)
     *
     * FIXME: needs javadoc
     *
     * @param req
     * @param decode
     * @return
     */
    @SuppressWarnings("unchecked")
    protected final Map<String, String[]> getRequestMap(HttpServletRequest req, boolean decode)
    {
        Map<String, String[]> reqDataTemp = req.getParameterMap();

        if (reqDataTemp == null)
        {
            logger.warn("No request data was received when trying to add/save a record.");
            //TODO: Should this also show the system error page?
            return null;
        }

        Map<String, String[]> reqData = new HashMap<String, String[]>();
        for (String key : reqDataTemp.keySet())
        {
            String[] reqDataArray = reqDataTemp.get(key);
            if (key.contains("_mce")) // Move actual wysiwyg content to related key.
            {
                // Purify html and resolve broken tags
                for (int index=0; index<reqDataArray.length; index++)
                {
                    if (reqDataArray[index] != null)
                    {
                        logger.debug("========== wysiwyg content: start ==========");
                        logger.debug("input ::\n" + reqDataArray[index]);
                        // Get the actual content length; if content is null or length == 0 set null content
                        reqDataArray[index] = HtmlUtil.filterWysiwygContent(reqDataArray[index]);
                        logger.debug("\nOutput ::\n" + reqDataArray[index] + "\n");
                        logger.debug("========== wysiwyg content: end ==========");
                    }
                }
                reqData.put(key.replace("_mce", ""), reqDataArray);
            }
            else if (reqDataTemp.get(key + "_mce") == null) // null means you don't have any mapped wysiwyg with this key, just store it
            {
                reqData.put(key, reqDataArray);
            }
            else
            {
                logger.info("[key:{}] I have a mapped wysiwyg hidden field with me, so just skip me.", key);
            }
        }
        return reqData;
    }
    // getRequestMap()

}
