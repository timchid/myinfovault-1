/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: EditExecutiveAction.java
 */

package edu.ucdavis.mw.myinfovault.web.spring.manageUsers;

import java.util.List;

import org.springframework.webflow.execution.Event;
import org.springframework.webflow.execution.RequestContext;

import edu.ucdavis.mw.myinfovault.events2.EventDispatcher2;
import edu.ucdavis.mw.myinfovault.events2.ExecutiveUserChangeEvent;
import edu.ucdavis.mw.myinfovault.service.authorization.AuthorizationService;
import edu.ucdavis.mw.myinfovault.service.authorization.AuthorizationService.PermissionDetail;
import edu.ucdavis.mw.myinfovault.service.authorization.AuthorizationService.Qualifier;
import edu.ucdavis.mw.myinfovault.service.authorization.Permission;
import edu.ucdavis.mw.myinfovault.service.person.AssignedRole;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.mw.myinfovault.service.person.MivRole;
import edu.ucdavis.mw.myinfovault.service.person.UserService;
import edu.ucdavis.mw.myinfovault.util.AttributeSet;
import edu.ucdavis.mw.myinfovault.web.spring.commons.MivFormAction;

/**
 * Manages executive roles: {@link MivRole#VICE_PROVOST}, {@link MivRole#PROVOST}, and {@link MivRole#CHANCELLOR}.
 *
 * @author Mary Northup
 * @since MIV 4.8.2
 */
public class EditExecutiveAction extends MivFormAction
{
    /**
     * Create edit user form action bean.
     *
     * @param userService Spring-injected user service
     * @param authService Spring-injected authorization service
     */
    public EditExecutiveAction(UserService userService,
                               AuthorizationService authService)
    {
        super(userService, authService);
    }

    /**
     * Does the user have permission to perform to manage executive roles?
     *
     * @param context webflow request context
     * @return Webflow event status, allowed or denied
     */
    public Event hasPermission(RequestContext context)
    {
        return authorizationService.hasPermission(getShadowPerson(context),
                                                  Permission.MANAGE_EXECUTIVES,
                                                  null)
             ? allowed()
             : denied();
    }

    /**
     * Is the user authorized to install the proposed persons as the new executives?
     *
     * @param context webflow request context
     * @return Webflow event status, allowed or denied
     */
    public Event isAuthorized(RequestContext context)
    {
        EditExecutiveForm form = (EditExecutiveForm) getFormObject(context);

        // check authorization to edit each executive
        return isAuthorized(context, form.getViceProvost())
            && isAuthorized(context, form.getProvost())
            && isAuthorized(context, form.getChancellor())
             ? allowed()
             : denied();

    }

    /**
     * Is the user authorized to install the given person as a new executive?
     *
     * @param context webflow request context
     * @param proposedExecutive proposed executive
     * @return if user is authorized
     */
    private boolean isAuthorized(RequestContext context, MivPerson proposedExecutive)
    {
        // add real, logged-in person user ID to permission detail
        AttributeSet permissionDetails = new AttributeSet();
        permissionDetails.put(PermissionDetail.ACTOR_ID, Integer.toString(getRealPerson(context).getUserId()));

        // add user ID of new executive to qualification
        AttributeSet qualification = new AttributeSet();
        qualification.put(Qualifier.USERID, Integer.toString(proposedExecutive.getUserId()));

        return authorizationService.isAuthorized(getShadowPerson(context),
                                                 Permission.MANAGE_EXECUTIVES,
                                                 permissionDetails,
                                                 qualification);
    }

    /*
     * (non-Javadoc)
     * @see org.springframework.webflow.action.FormAction#bindAndValidate(org.springframework.webflow.execution.RequestContext)
     */
    @Override
    public Event bind(RequestContext context) throws Exception
    {
        Event result = super.bind(context);

        // if successful bind
        if (result.getId().equals(success().getId()))
        {
            EditExecutiveForm form = (EditExecutiveForm) getFormObject(context);

            // use the IDs that are sent from the page to get the new people for the roles
            form.setViceProvost(userService.getPersonByMivId(form.getViceProvostID()));
            form.setProvost(userService.getPersonByMivId(form.getProvostID()));
            form.setChancellor(userService.getPersonByMivId(form.getChancellorID()));
        }

        return result;
    }

    /*
     * (non-Javadoc)
     * @see org.springframework.webflow.action.FormAction#createFormObject(org.springframework.webflow.execution.RequestContext)
     */
    @Override
    public Object createFormObject(RequestContext context)
    {
        return new EditExecutiveForm(userService.getUsers(MivRole.VICE_PROVOST),
                                     userService.getUsers(MivRole.PROVOST),
                                     userService.getUsers(MivRole.CHANCELLOR));
    }

    /**
     * Update the Executive Roles, remove role from any existing and replace/add to those that were selected.
     *
     * @param context WebFlow request context
     * @return WebFlow event status; success, error, or denied
     */
    public Event updateExecutive(RequestContext context)
    {
        EditExecutiveForm editForm = (EditExecutiveForm) getFormObject(context);

        MivPerson shadowPerson = getShadowPerson(context);
        MivPerson realPerson = getRealPerson(context);

        // install new vice provost
        installExecutive(editForm.getViceProvost(),
                         MivRole.VICE_PROVOST,
                         realPerson,
                         shadowPerson);

        // install new provost
        installExecutive(editForm.getProvost(),
                         MivRole.PROVOST,
                         realPerson,
                         shadowPerson);

        // install new chancellor
        installExecutive(editForm.getChancellor(),
                         MivRole.CHANCELLOR,
                         realPerson,
                         shadowPerson);

        return success();
    }

    /**
     * Add the bread crumb appropriate for the child flow.
     *
     * @param context Webflow request context
     * @return Webflow event status
     */
    public Event addBreadcrumb(RequestContext context)
    {
        //add edit executive bread crumb
        addBreadcrumbs(context, "Edit Executives");

        return success();
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.web.spring.commons.MivFormAction#loadProperties(org.springframework.webflow.execution.RequestContext)
     */
    @Override
    public void loadProperties(RequestContext context)
    {
        super.loadProperties(context, DEFAULT_SUBSET, "labels", "tooltips");
    }

    /**
     * Add the given role to the new and remove it from the current executives.
     *
     * @param newExecutive new executive of the given role
     * @param executiveRole executive role to add/remove
     * @param realPerson real, logged-in person
     * @param shadowPerson shadow, switched-to person
     */
    private void installExecutive(MivPerson newExecutive,
                                  MivRole executiveRole,
                                  MivPerson realPerson,
                                  MivPerson shadowPerson)
    {
        /*
         * Current occupants of the executive role.
         */
        List<MivPerson> currentExecutives = userService.getUsers(executiveRole);


        /*
         * New executive, if:
         *  - assigned role added, and
         *  - user successfully saved
         */
        if (newExecutive.addRole(new AssignedRole(executiveRole))
         && userService.savePerson(newExecutive, shadowPerson.getUserId()) != null)
        {
            logger.info("_AUDIT:\n\tAction:\tRole -" + executiveRole + "- added" + "\n\tReal:\t{}\n\tTarget:\t{}", realPerson, newExecutive);
        }
        // otherwise, one of the current executives
        else
        {
            currentExecutives.remove(newExecutive);
        }

        for (MivPerson currentExecutive : currentExecutives)
        {
            /*
             * Current executive, if:
             *  - role removed,
             *  - assigned role removed, and
             *  - user successfully saved
             */
            if (currentExecutive.removeRole(executiveRole)
             && currentExecutive.removeRole(new AssignedRole(executiveRole))
             && userService.savePerson(currentExecutive, shadowPerson.getUserId()) != null)
            {
                logger.info("_AUDIT:\n\tAction:\tRole -" + executiveRole + "- removed" + "\n\tReal:\t{}\n\tTarget:\t{}", realPerson, currentExecutive);

                // fire vice provost role change event
                EventDispatcher2.getDispatcher().post(
                    new ExecutiveUserChangeEvent(realPerson,
                                                 newExecutive,
                                                 currentExecutive,
                                                 executiveRole
                    ).setShadowPerson(shadowPerson));
            }
        }
    }
}
