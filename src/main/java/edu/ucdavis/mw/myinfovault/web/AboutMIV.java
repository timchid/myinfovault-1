/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: AboutMIV.java
 */

package edu.ucdavis.mw.myinfovault.web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import edu.ucdavis.myinfovault.MIVInformation;


/**
 * Show information about the MIV build
 */
public class AboutMIV extends MIVServlet
{
    private static final long serialVersionUID = 200802221500L;

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException
    {
        String listURL = "/jsp/mivInformation.jsp";

        request.setAttribute("buildInformation", MIVInformation.INSTANCE);

        //log.fine("Dispatch target is "+listURL);
        dispatch(listURL, request, response);
        //log.exiting(className, "doGet");
        //response.setContentType("text/html");
    }
}
