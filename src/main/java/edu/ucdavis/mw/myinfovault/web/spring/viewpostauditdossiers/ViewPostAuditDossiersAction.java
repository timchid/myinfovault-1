/**
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: ViewDossierSnapshotAction.java
 */

package edu.ucdavis.mw.myinfovault.web.spring.viewpostauditdossiers;

import java.text.MessageFormat;

import org.apache.log4j.Logger;
import edu.ucdavis.mw.myinfovault.util.AttributeSet;
import org.springframework.webflow.execution.Event;
import org.springframework.webflow.execution.RequestContext;

import edu.ucdavis.mw.myinfovault.domain.action.DossierLocation;
import edu.ucdavis.mw.myinfovault.service.authorization.AuthorizationService;
import edu.ucdavis.mw.myinfovault.service.authorization.AuthorizationService.Qualifier;
import edu.ucdavis.mw.myinfovault.service.authorization.Permission;
import edu.ucdavis.mw.myinfovault.service.dossier.DossierService;
import edu.ucdavis.mw.myinfovault.service.dossier.WorkflowException;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.mw.myinfovault.service.person.UserService;
import edu.ucdavis.mw.myinfovault.web.spring.search.ManageOpenAction;
import edu.ucdavis.mw.myinfovault.web.spring.search.SearchCriteria;
import edu.ucdavis.mw.myinfovault.web.spring.search.SearchStrategy;
import edu.ucdavis.myinfovault.MIVUser;

/**
 * TODO: Add Javadoc
 *
 * @author Rick Hendricks
 *
 */
public class ViewPostAuditDossiersAction extends ManageOpenAction
{
 
    private static final String[] breadcrumbs = {"Dossiers at Post Audit or Appeal: Search", "Search Results"};

    private final Logger logger = Logger.getLogger(this.getClass());
    private static final String AUTH_LOG_PATTERN = "User {0} ({1}) is AUTHORIZED to {2}";
    private static final String NOT_AUTH_LOG_PATTERN = "User {0} ({1}) is ** NOT ** AUTHORIZED to {2}";

    /**
     * TODO: Add Javadoc
     *
     * @param searchStrategy
     * @param authorizationService
     * @param userService
     * @param dossierService
     */
    public ViewPostAuditDossiersAction(SearchStrategy searchStrategy,
                                     AuthorizationService authorizationService,
                                     UserService userService,
                                     DossierService dossierService)
    {
        super(searchStrategy,
              authorizationService,
              userService,
              dossierService);
    }

    /**
     * Checks if user is authorized to view dossiers at post audit.
     *
     * @param context Request context from webflow
     * @return Event, success or error
     */
    public Event isAuthorized(RequestContext context)
    {
        MivPerson currentPerson = getShadowPerson(context);

        // Set the permission
        String permission = Permission.VIEW_POST_AUDIT_DOSSIERS;

        AttributeSet qualification = new AttributeSet();
        qualification.put(Qualifier.USERID,
                          Integer.toString(currentPerson.getUserId()));

        boolean isAuthorized = authorizationService.hasPermission(currentPerson,
                                                                  permission,
                                                                  qualification);

        // log authentication result
        logger.info(MessageFormat.format(isAuthorized ? AUTH_LOG_PATTERN : NOT_AUTH_LOG_PATTERN,
                                         currentPerson.getDisplayName(),
                                         currentPerson.getPersonId(),
                                         permission));

        // return error if not authorized
        if (!isAuthorized) return error();

        return success();
    }

    /**
     * Get dossiers at the post audit location and beyond.
     *
     * @param context Webflow request context
     * @return Webflow event status
     */
    public Event getPostAuditDossiers(RequestContext context)
    {
        //real logged in MIV user
        MIVUser realUser = getUser(context);
        MivPerson shadowPerson = this.getShadowPerson(context);

        try
        {
            //get dossiers for the shadow user
            //set results in request scope
            
            setResults(context,
                       searchStrategy.dossierSearch(getFormObject(context),
                                   realUser,
                                   dossierService.getDossiersByWorkflowLocations(shadowPerson,
                                   DossierLocation.POSTAUDITREVIEW,
                                   DossierLocation.SENATEAPPEAL,
                                   DossierLocation.FEDERATIONAPPEAL,
                                   DossierLocation.FEDERATIONSENATEAPPEAL,
                                   DossierLocation.POSTAPPEALSCHOOL,
                                   DossierLocation.POSTAPPEALVICEPROVOST)));
        }
        
        catch (WorkflowException e)
        {
            logger.warn("Unable to retrieve post audit dossiers " + realUser.getTargetUserInfo().getDisplayName(), e);
            return error();
        }

        return success();
    }

    /**
     * Add bread crumbs for manage open action as parent flow.
     *
     * @param context Webflow request context
     */
    public void addBreadcrumbs(RequestContext context)
    {
        addBreadcrumbs(context, breadcrumbs);
    }

    /**
     * remove bread crumbs for manage open action as parent flow.
     *
     * @param context Webflow request context
     */
    public void removeBreadcrumbs(RequestContext context)
    {
        removeBreadcrumbs(context, breadcrumbs.length);
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public SearchCriteria getFormObject(RequestContext context)
    {
        return (SearchCriteria) super.getFormObject(context);
    }
}
