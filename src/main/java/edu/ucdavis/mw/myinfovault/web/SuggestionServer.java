/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: SuggestionServer.java
 */

package edu.ucdavis.mw.myinfovault.web;

import java.io.IOException;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.MediaType;

import com.google.common.eventbus.Subscribe;

import edu.ucdavis.mw.common.service.suggest.DefaultSuggestionHandlerFactory;
import edu.ucdavis.mw.common.service.suggest.SuggestionHandler;
import edu.ucdavis.mw.common.service.suggest.SuggestionHandlerFactory;
import edu.ucdavis.mw.myinfovault.events.ConfigurationChangeEvent;
import edu.ucdavis.mw.myinfovault.events.EventDispatcher;
import edu.ucdavis.mw.myinfovault.service.suggest.PersonAwareSuggestionHandler;
import edu.ucdavis.mw.myinfovault.util.StringUtil;

/**
 * @author Stephen Paulsen
 * @since MIV v4.4
 */
public class SuggestionServer extends MIVServlet
{
    private static final long serialVersionUID = 201203051305L;
    private SuggestionHandlerFactory<Map<String,String>> factory = null;

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.web.MIVServlet#init(javax.servlet.ServletConfig)
     */
    @Override
    public void init(javax.servlet.ServletConfig c) throws ServletException
    {
        this.init();
    }

    /*
     * (non-Javadoc)
     * @see javax.servlet.GenericServlet#init()
     */
    @Override
    public void init() throws ServletException
    {
        this.factory = new DefaultSuggestionHandlerFactory();

        EventDispatcher.getDispatcher().register(this);
    }

    /*
     * (non-Javadoc)
     * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    @SuppressWarnings("unchecked")
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        String searchPath = req.getPathInfo();
        String searchTerm = req.getParameter("term");
/* Might care about the rest of the query string ... */
        String query = req.getQueryString();
/* or all of the parameters ...
        Map params = req.getParameterMap();
*/
/* but really don't care about these three ...
        String pathTranslated = req.getPathTranslated();
        String reqURI = req.getRequestURI();
        StringBuffer reqURL = req.getRequestURL();
*/
        if (logger.isDebugEnabled())
        {
            logger.debug("Headers received on request for {}?{}", req.getRequestURL(), query);

            for (String headerName : Collections.list((Enumeration<String>)req.getHeaderNames()))
            {
                if (!"cookie".equals(headerName))// skip the big ugly cookie header
                {
                    System.out.println("  header ["+headerName+"] = ["+req.getHeader(headerName)+"]");
                }
            }
        }

        logger.debug("Searching for Term [{}] on Path [{}]", searchTerm, searchPath);

        // Originally the server would suggest nothing if no query was made.
        // This was commented out to provide a result by doing a default SQL query when searchTerm is empty.
        /*if (searchTerm == null || searchTerm.length() == 0) {
            // No search term provided - return nothing
            return;
        }*/

        if (factory == null) {
            factory = new DefaultSuggestionHandlerFactory();
        }

        // Remove leading slash if present - it's not part of the "topic"
        if (searchPath.charAt(0) == '/') {
            searchPath = searchPath.substring(1);
        }

        ServletOutputStream os = resp.getOutputStream();

// Might want to look at the "accept" header before deciding on what content type to return...
// This one came from an XMLHttpRequest for /suggest/work/type?term=C
//   header [accept] = [application/json, text/javascript, */*; q=0.01]
// And this came from manually browsing to /suggest/work/type?term=Ph
//   header [accept] = [text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8]
// Then convert the results to the requested type and set the Content-Type reply header.
        resp.setContentType(isAjaxRequest() ? MediaType.APPLICATION_JSON : MediaType.TEXT_PLAIN);

        SuggestionHandler<Map<String,String>> handler = factory.getHandler(searchPath);

        if (handler == null) {
            // something's wrong - no handler configured + no default handler
            os.println("[ ]");
            return;
        }

// Based on comments above... we might do toJson only if application/json was requested?
// Might we also want a "toXml()" conversion method?
        os.println(StringUtil.toJson(
            searchPath.endsWith("/scoped")//if scoped, handler must be MivPerson aware
          ? ((PersonAwareSuggestionHandler<Map<String,String>>) handler).getSuggestions(mivSession.get().getUser().getTargetUserInfo().getPerson(),
                                                                                        searchPath,
                                                                                        searchTerm)
          : handler.getSuggestions(searchPath, searchTerm)));
    }

    /**
     * Reset factory if suggestion properties change.
     *
     * @param event subscribed configuration change event
     */
    @Subscribe
    public void suggestionsChanged(ConfigurationChangeEvent event)
    {
        this.factory.reset();
    }
}
