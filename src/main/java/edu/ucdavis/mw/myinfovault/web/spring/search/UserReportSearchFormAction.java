/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: UserReportSearchFormAction.java
 */

package edu.ucdavis.mw.myinfovault.web.spring.search;

import edu.ucdavis.mw.myinfovault.service.authorization.AuthorizationService;
import edu.ucdavis.mw.myinfovault.service.person.UserService;
import edu.ucdavis.mw.myinfovault.service.report.UserReportService;
import edu.ucdavis.mw.myinfovault.valuebeans.report.ReportVO;
import edu.ucdavis.mw.myinfovault.web.spring.commons.MivFormAction;
import edu.ucdavis.myinfovault.MIVUser;

/**
 * Spring form action for user report search.
 * 
 * @author Pradeep Haldiya
 * @since MIV 4.0
 */
public class UserReportSearchFormAction extends MivFormAction
{
    private UserReportSearchStrategy searchStrategy;
    protected UserReportService userReportService;

    /**
     * Create the user report search form action bean.
     * 
     * @param userService Spring-injected user service
     * @param authorizationService Spring-injected authorization service
     * @param searchStrategy Spring-injected search strategy
     * @param userReportService Spring-injected user service
     */
    public UserReportSearchFormAction(UserService userService,
                                      AuthorizationService authorizationService,
                                      UserReportSearchStrategy searchStrategy,
                                      UserReportService userReportService)
    {
        super(userService, authorizationService);

        this.searchStrategy = searchStrategy;
        this.userReportService = userReportService;
    }

    /**
     * Gets a list of users by name.
     * 
     * @param user target user doing the search
     * @param criteria - The search criteria to use
     * @return ReportVO objects
     */
    public ReportVO getUsersByName(MIVUser user, SearchCriteria criteria)
    {
        return searchStrategy.getUsersByName(user, criteria);
    }

    /**
     * Gets a list of users by department.
     * 
     * @param user target user doing the search
     * @param criteria The search criteria to use
     * @return ReportVO objects
     */
    public ReportVO getUsersByDepartment(MIVUser user, SearchCriteria criteria)
    {
        return searchStrategy.getUsersByDepartment(user, criteria);
    }

    /**
     * Gets a list of users by school.
     * 
     * @param user target user doing the search
     * @param criteria The search criteria to use
     * @return ReportVO objects
     */
    public ReportVO getUsersBySchool(MIVUser user, SearchCriteria criteria)
    {
        return searchStrategy.getUsersBySchool(user, criteria);
    }
}
