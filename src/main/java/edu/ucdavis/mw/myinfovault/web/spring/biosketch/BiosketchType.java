/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: BiosketchType.java
 */

package edu.ucdavis.mw.myinfovault.web.spring.biosketch;

import edu.ucdavis.myinfovault.data.DocumentIdentifier;
import edu.ucdavis.myinfovault.exception.MivSevereApplicationError;

/**
 * enum to get the miv supported biosketch types
 *
 * @author pradeeph
 * @since v4.6
 */
public enum BiosketchType
{
    CV            ( 2, "cv", "CV","Curriculum Vitae",DocumentIdentifier.CV),
    BIOSKETCH     ( 3, "nih", "Biosketch", "NIH Biosketch",DocumentIdentifier.NIH),
    INVALID  (-1, "invalid", "Invalid","",DocumentIdentifier.INVALID);

    private int typeId;
    private String code;
    private String title;
    private String description;
    private DocumentIdentifier docIdentifier = DocumentIdentifier.INVALID;

    /**
     * Used by BiosketchType internally.
     * @param typeId : BiosketchType id of the MIV Biosketch.
     * @param code
     * @param title
     * @param description
     * @param docIdentifier : DocumentIdentifier of this Biosketch.
     */
    private BiosketchType(int typeId, String code, String title, String description,DocumentIdentifier docIdentifier)
    {
        this.typeId = typeId;
        this.code = code;
        this.title = title;
        if (description == null || description.trim().length() == 0)
        {
            this.description = title;
        }
        else
        {
            this.description = description;
        }
        this.docIdentifier = docIdentifier;
    }

    /**
     * Bean-style access to the TypeId for those places
     * that can't use an enum (like in a jsp)
     *
     * @return the numeric value of this BiosketchType
     */
    public int getTypeId()
    {
        return this.typeId();
    }

    public int typeId()
    {
        return this.typeId;
    }

    /**
     * Bean-style access to the code for those places
     * that can't use an enum (like in a jsp)
     *
     * @return the code value of this BiosketchType
     */
    public String getCode()
    {
        return this.code();
    }

    public String code()
    {
        return this.code;
    }

    /**
     * Bean-style access to the title for those places
     * that can't use an enum (like in a jsp)
     *
     * @return the title of this BiosketchType
     */
    public String getTitle()
    {
        return this.title();
    }

    public String title()
    {
        return title;
    }

    /**
     * Bean-style access to the title for those places
     * that can't use an enum (like in a jsp)
     *
     * @return the description of this BiosketchType
     */
    public String getDescription()
    {
        return this.description();
    }

    public String description()
    {
        return description;
    }

    /**
     * Bean-style access to the docIdentifier for those places
     * that can't use an enum (like in a jsp)
     *
     * @return the enum  Document Identifier of this biosketch Type
     */
    public DocumentIdentifier getDocIdentifier()
    {
        return this.docIdentifier;
    }

    public DocumentIdentifier docIdentifier()
    {
        return this.getDocIdentifier();
    }

    /**
     * Returns the enum corresponding to the typeId
     * @param typeId
     * @return BiosketchType enum
     */
    public static BiosketchType convert(String typeId)
    {
        BiosketchType biotype = null;
        BiosketchType[] enumlist = BiosketchType.values();
        try
        {
            int id = Integer.parseInt(typeId.trim());
            for (BiosketchType type : enumlist)
            {
                if (type.getTypeId() == id) { return type; }
            }

            throw new MivSevereApplicationError("A request was made to display an unknown document type: "+typeId);
        }
        catch (NumberFormatException nfe)
        {
            try
            {
                for (BiosketchType type : enumlist)
                {
                    if (type.getTitle().equalsIgnoreCase(typeId) || type.getCode().equalsIgnoreCase(typeId))
                    {
                        return type;
                    }
                }

                biotype = BiosketchType.valueOf(typeId);
            }
            catch (IllegalArgumentException iae)
            {
                throw new MivSevereApplicationError("A request was made to display an unknown document type: "+typeId, iae);
            }
        }

        return biotype;
    }
}
