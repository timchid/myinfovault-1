/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: ManageOpenAction.java
 */

package edu.ucdavis.mw.myinfovault.web.spring.search;

import java.util.List;

import org.springframework.webflow.core.collection.MutableAttributeMap;
import org.springframework.webflow.execution.Event;
import org.springframework.webflow.execution.RequestContext;

import edu.ucdavis.mw.myinfovault.service.authorization.AuthorizationService;
import edu.ucdavis.mw.myinfovault.service.dossier.DossierService;
import edu.ucdavis.mw.myinfovault.service.person.UserService;
import edu.ucdavis.myinfovault.MIVUser;

/**
 * Contains the methods used to support various actions executed in the web flow.
 *
 * @author Mary Northup
 * @since MIV 3.0
 */
public class ManageOpenAction extends DossierAction
{
    private static final String[] breadcrumbs = {"Manage Open Actions: Search", "Search Results"};

    /**
     * Create the manage open action form action bean.
     *
     * @param searchStrategy Spring-injected search strategy
     * @param authorizationService Spring-injected authorization service
     * @param userService Spring-injected user service
     * @param dossierService Spring-injected dossier service
     */
    public ManageOpenAction(SearchStrategy searchStrategy,
                            AuthorizationService authorizationService,
                            UserService userService,
                            DossierService dossierService)
    {
        super(searchStrategy,
              authorizationService,
              userService,
              dossierService);
    }

    /**
     * getDossiersByDepartment - The Manage Open Actions search by department filtering follows
     * @param criteria - SearchCriteria : the main search form containing the search "criteria" requested
     * @param user - MIVUser requesting the search
     * @return List of MivActionList objects
     */
    public List<MivActionList> getDossiersByDepartment(SearchCriteria criteria, MIVUser user)
    {
        return searchStrategy.getDossiersByDepartment(criteria, user);
    }


    /**
     * getDossiersBySchool - Manage Open Actions search by school filtering follows
     * @param criteria - SearchCriteria : the main search form containing the search "criteria" requested
     * @param user - MIVUser requesting the search
     * @return List of MivActionList objects
     */
    public List<MivActionList> getDossiersBySchool(SearchCriteria criteria, MIVUser user)
    {
        return searchStrategy.getDossiersBySchool(criteria, user);
    }


    /**
     * Reset the values that have been saved in the
     * flowScope until a new dossier is selected.
     *
     * @param context Request context from webflow
     * @return WebFlow success event status
     */
    public Event resetFlowScope(RequestContext context)
    {
        MutableAttributeMap<Object> flowScope = context.getFlowScope();
        flowScope.remove("actionMsg");
        flowScope.remove("actionError");
        flowScope.remove("decision");
        flowScope.remove("routingErrorList");
        flowScope.remove("lastTransaction");
        flowScope.put("displayOnly",true);
        flowScope.put("displayAppointmentDetails", false); // default to no display appointment details
        context.getConversationScope().remove("dossierParam");

        return success();
    }

    /**
     * Add bread crumbs for manage open action as parent flow.
     *
     * @param context Webflow request context
     */
    public void addBreadcrumbs(RequestContext context)
    {
        Boolean direct = context.getRequestParameters().contains("direct");
        if (direct == null || !direct)
        {
            addBreadcrumbs(context, breadcrumbs);
        }
    }

    /**
     * remove bread crumbs for manage open action as parent flow.
     *
     * @param context Webflow request context
     */
    public void removeBreadcrumbs(RequestContext context)
    {
        removeBreadcrumbs(context, breadcrumbs.length);
    }
}
