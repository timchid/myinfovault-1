/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: UploadForm.java
 */

package edu.ucdavis.mw.myinfovault.web.spring.publications;

import java.io.Serializable;

import org.springframework.web.multipart.commons.CommonsMultipartFile;

import edu.ucdavis.mw.myinfovault.domain.publications.PublicationSource;
import edu.ucdavis.mw.myinfovault.service.publications.ImportResult;

/**
 * Form backing for publications import.
 *
 * @author Craig Gilmore
 * @since MIV 3.9.1
 */
public class UploadForm extends ParseForm implements Serializable
{
    private static final long serialVersionUID = 201105161101L;

    private transient CommonsMultipartFile uploadFile = null;
    private PublicationSource source = PublicationSource.ENDNOTE;

    private ImportResult importResult = null;

    /**
     * @return The publications file uploaded
     */
    public CommonsMultipartFile getUploadFile() {
        return uploadFile;
    }

    /**
     * @param uploadFile The publications file uploaded
     */
    public void setUploadFile(CommonsMultipartFile uploadFile) {
        this.uploadFile = uploadFile;
    }

    /**
     * @return The uploaded file's publication source
     */
    public PublicationSource getSource() {
        return source;
    }

    /**
     * @param source The uploaded file's publication source
     */
    public void setSource(PublicationSource source) {
        this.source = source;
    }

    /**
     * @return The import result and statistics for this upload
     */
    public ImportResult getImportResult()
    {
        return importResult;
    }

    /**
     * @param importResult The import result and statistics for this upload
     */
    public void setImportResult(ImportResult importResult)
    {
        this.importResult = importResult;
    }
}
