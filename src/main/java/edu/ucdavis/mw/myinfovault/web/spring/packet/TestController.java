/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: TestController.java
 */


package edu.ucdavis.mw.myinfovault.web.spring.packet;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Minimal spring controller to do some testing.
 * @author Stephen Paulsen
 */
@Controller
public class TestController
{
    @RequestMapping(value="/testpage", method=RequestMethod.GET)
    public String getTestPage(HttpServletRequest request)
    {
        return "boilerplate";
    }
}
