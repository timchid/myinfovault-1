/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: EditRecord.java
 */

package edu.ucdavis.mw.myinfovault.web;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import com.google.gson.Gson;

import edu.ucdavis.mw.myinfovault.valuebeans.RecordVO;
import edu.ucdavis.mw.myinfovault.valuebeans.ResultVO;
import edu.ucdavis.mw.myinfovault.valuebeans.ValidateResultVO;
import edu.ucdavis.myinfovault.ConfigReloadConstants;
import edu.ucdavis.myinfovault.MIVConfig;
import edu.ucdavis.myinfovault.MIVUserInfo;
import edu.ucdavis.myinfovault.PropertyManager;
import edu.ucdavis.myinfovault.data.MIVUtil;
import edu.ucdavis.myinfovault.data.RecordHandler;
import edu.ucdavis.myinfovault.exception.MivSevereApplicationError;
import edu.ucdavis.myinfovault.format.EmptyFormatter;
import edu.ucdavis.myinfovault.format.QuoteCleaner;
import edu.ucdavis.myinfovault.format.TimestampFormatter;


/**
 * Record Editor:
 * Get one record from the data source for editing and send that record to an editing page.
 *
 * @author Stephen Paulsen
 * @since MIV 2.0
 */
public class EditRecord extends MIVServlet
{
    private static final long serialVersionUID = 200703061517L;
    private static EditRecordHelper erHelper = new EditRecordHelper();

    // DateFormat is not thread safe. Always use a ThreadLocal for a DateFormat
    protected static final ThreadLocal<DateFormat> sdf =
        new ThreadLocal<DateFormat>() {
            @Override protected DateFormat initialValue() {
                return new SimpleDateFormat(MIVConfig.getConfig().getProperty("default-config-dateformat"));
            }
    };
    protected static final ThreadLocal<DateFormat> yf =
        new ThreadLocal<DateFormat>() {
            @Override protected DateFormat initialValue() {
                return new SimpleDateFormat("yyyy");
            }
    };

    public enum AjaxResponseParams {
        success, message, errorfields, timestamp, recnum, resultUrl, hiddenValue;
    }


    /*
     * (non-Javadoc)
     * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse res)
        throws ServletException, IOException
    {
        logger.trace("\n ***** EditRecord doGet called! *****\t\tGET\n");

        MIVUserInfo mui = mivSession.get().getUser().getTargetUserInfo();

        RequestDispatcher dispatcher;
        Properties p = null;
        String tableName;
        logger.debug("request parmeter map: {}", req.getParameterMap());

        String recordType = req.getParameter("rectype");
        recordType = MIVUtil.sanitize(recordType);

        logger.debug(" EditRecord-doGet record type is {}", recordType);

        if (recordType == null || recordType.length() == 0) {
//            showError("The record type is missing when trying to edit a record.", req, res);
            throw new MivSevereApplicationError("The record type is missing when trying to edit a record.");
//            return;
        }

        // to setup form specific parameters
        setUpForm(req,res);

        req.setAttribute("recordTypeName", recordType);


        // Additional Information Related and this documentid is used when we add a new Heading.
        // These are passed straight through to the JSPs
        String documentID = MIVUtil.sanitize(req.getParameter("documentid"));
        req.setAttribute("documentid", documentID);
        String sectionName = MIVUtil.sanitize(req.getParameter("sectionname"));
        req.setAttribute("sectionname", sectionName);
        String subType = MIVUtil.sanitize(req.getParameter("subtype"));
        req.setAttribute("subtype", subType);

        if (subType == null) subType = "";
        if (subType.length() > 0)
        {
            subType = "-" + subType.toLowerCase();
        }

        p = PropertyManager.getPropertySet(recordType, "config");
        tableName = p.getProperty("tablename");
//        if (debug) {
//            System.out.println("Config Properties are: [" + p + "]");
//            System.out.println("  tableName from PropertyManager lookup is " + tableName);
//        }
        logger.debug("Config Properties are: [{}]\n  tableName from PropertyManager lookup is {}", p, tableName);
        // TODO: Error if tableName==null or ==""

        String editorForm = p.getProperty("editorpage");
        if (editorForm == null || editorForm.length() < 1)
        {
//            showError("No form was found to edit a record of type " + recordType + ".", req, res);
            throw new MivSevereApplicationError("errors.editrecord.form", recordType);

//            return;
        }

        // Find delete key if any
        String key = findDeleteKey(req);
        // Check for delete
        if (key != null && key != "" && key != "0000" && key != "-9999")
        {
            logger.debug("Call Delete Record : dispatch to remove record");
            String target = "RemoveRecord";
            dispatch(target, req, res);
            return;
        }

        String recordClass = req.getParameter("recordClass");
        if (recordClass != null)
        {
            req.setAttribute("recordClass", recordClass);
        }

        if ((key = findKey(req)) == null)
        {
//            showError("No record key was found when trying to edit a record of type " + recordType + ".", req, res);
            throw new MivSevereApplicationError("errors.editrecord.recordkey", recordType);
//            return;
        }
        int keyval = Integer.parseInt(key);

        logger.info("Record Edit for user {}: table={}, key={}", new Object[] { mui.getPerson().getUserId(), tableName, key });


        RecordHandler rh = mui.getRecordHandler();

        Map<String,String> record = null;
        // recordFromAdditionalServlet is the map of records that are available from AdditionalEditRecord servlet.
        // if recordFromAdditionalServlet has records means it has additonalInformation related stuff and we are doing anything here  just getting from  AdditionalEditRecord
        // and handling it to EditRecord servlet.
        @SuppressWarnings("unchecked")
        Map<String,String> recordFromAdditionalServlet = (Map<String,String>) req.getAttribute("record");
        if (recordFromAdditionalServlet != null && recordFromAdditionalServlet.size() > 0) {
            record = recordFromAdditionalServlet;
        }
        else {
            record = rh.get(tableName, keyval);
        }

        if (keyval > 0)
        {
            if (record == null || record.isEmpty()) {
                logger.error("Record with ID={} was not found in table {}", keyval, tableName);
            }

            boolean allowMarkup = Boolean.parseBoolean(p.getProperty("allowmarkup"));
            if (record != null && allowMarkup) {
                record.put("content", MIVUtil.escapeWysiwygEdit(record.get("content")));
            }
            else {
                escapeMarkup(record);
            }

            new EmptyFormatter()
                .add(new QuoteCleaner())
                .add(new TimestampFormatter("updatetimestamp"))
                .format(record);
        }
        else if (keyval == 0) // Add New Record provides a keyval==0
        {
            String pubtype = req.getParameter("pubtype");
            if (pubtype != null && pubtype.trim().length() > 0)
            {
                record.put("typeid", pubtype);
            }
        }
        req.setAttribute("rec", record);

        Map<String,Object> config = new HashMap<String,Object>();

        p = PropertyManager.getPropertySet(recordType+subType, "strings");
        config.put("strings", p);

        p = PropertyManager.getPropertySet(recordType, "config");
        config.put("config", p);

        p = PropertyManager.getPropertySet(recordType, "labels");
        config.put("labels", p);

        p = PropertyManager.getPropertySet(recordType, "tooltips");
        config.put("tooltips", p);

        config.put("options", MIVConfig.getConfig().getConstants());

        req.setAttribute("constants", config);

        String usepopups = req.getParameter("usepopups");
        if (usepopups != null && usepopups.trim().equalsIgnoreCase("true"))
        {
            usepopups = "true";
            req.setAttribute("usepopups", "true");
        }
        else
        {
            usepopups = "false";
            req.setAttribute("usepopups", "false");
        }


        // Dispatch directly to the edit form if doing popups, which the recordlist page
        // will then load as a dialog; otherwise dispatch to the form template page which
        // jsp:includes the editor form.
        p = (Properties)config.get("config");

        String destination = "/jsp/editform.jsp";
        if ( Boolean.valueOf((String)p.get("usepopups")) || Boolean.valueOf(usepopups) )
        {
            destination = "/jsp/" + editorForm;
        }
        else
        {
            req.setAttribute("editorform", editorForm);
        }
        dispatcher = req.getRequestDispatcher(destination);

        logger.debug("dispatching to target. (Dispatcher: [{}]", dispatcher);
        try {
            // Get the referer and set in the session servlet context.
            // This value will be retrieved by the PDF upload to allow returning to the
            // servlet which referred to this page, which is most likely ItemList servlet.
            String returnTo = req.getHeader("referer");
            req.getSession().setAttribute("returnTo", returnTo);

            res.setHeader("cache-control", "no-cache, must-revalidate");
            res.addHeader("Pragma", "no-cache");
            res.setDateHeader("Expires", System.currentTimeMillis() - 2);
            dispatcher.forward(req, res);
        }
//        catch (ServletException se) { // FIXME: Don't just catch and re-throw. There's no point in catching here.
//            //log.throwing("EditRecord", "doGet", se);
//            throw(se);
//        }
//        catch (IOException ioe) {
//            //log.throwing("EditRecord", "doGet", ioe);
//            throw(ioe);
//        }
        finally {
            logger.trace(" ***** Leaving EditRecord doGet  *****\t\tGET\n");
        }
    }
    //doGet()

    /*
     * (non-Javadoc)
     * @see javax.servlet.http.HttpServlet#doPost(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse res)
        throws ServletException, IOException
    {
        logger.trace("\n ***** EditRecord doPost called! *****\t\tPOST\n");

        logger.debug("request: ", req);
        Map<String, String[]> reqData = getRequestMap(req);

        String recordType = (reqData.get("rectype") != null ? reqData.get("rectype")[0] : null);
        String subType = (reqData.get("subtype") != null ? reqData.get("subtype")[0] : null);
        String reset = (reqData.get("popupformcancel") != null ? reqData.get("popupformcancel")[0] : null);
        String documentID = (reqData.get("documentid") != null ? reqData.get("documentid")[0] : "");
        String sectionName = (reqData.get("sectionname") != null ? reqData.get("sectionname")[0] : null);
        String recordClass = (reqData.get("recordClass") != null ? reqData.get("recordClass")[0] : null);

        if (documentID == null || documentID.trim().length() == 0)
        {
            documentID = ( reqData.get("DocumentID") != null ? reqData.get("DocumentID")[0] : "" );
        }

        documentID = MIVUtil.sanitize(documentID);

        subType = subType != null ? subType : "";
        documentID = documentID != null ? documentID : "";
        sectionName =  sectionName !=null ? sectionName : "";

        if (reset != null && !("".equals(reset)))
        {
            // For Section Header the recordtype is "additionalheader" but in order to redirect the user
            // to additional record page, the recordType should be "additional" and subType as well as
            // sectionname are also required.
            if (recordType.equals("additionalheader"))
            {
                recordType = "additional";
            }

            StringBuilder redirectPath = new StringBuilder(req.getContextPath());
            redirectPath.append("/ItemList?type=").append(recordType);

            if (recordClass != null && !("".equals(recordClass)))
            {
                redirectPath.append("&recordClass=").append(recordClass);
            }

            if (subType != null && !("".equals(subType)))
            {
                redirectPath.append("&subtype=").append(subType);
            }

            if (sectionName != null && !("".equals(sectionName)))
            {
                redirectPath.append("&sectionname=").append(sectionName);
            }

            res.setContentType("text/html");
            res.sendRedirect(redirectPath.toString());

            return;
        }

        MIVUserInfo targetMui = mivSession.get().getUser().getTargetUserInfo();
        MIVUserInfo actingMui = mivSession.get().getUser().getUserInfo();

        RecordVO[] RecordVOList = getRecords(reqData);


        Map<String, ResultVO> resultMap = null;

        try
        {
            resultMap = erHelper.process(actingMui, targetMui, RecordVOList);
        }
        catch (Exception e) // FIXME: Don't catch plain 'Exception' - catch something specific
        {
            if (resultMap == null) {
                resultMap = new HashMap<String, ResultVO>();
            }
            resultMap.put(recordType, new ResultVO(false,"unknown error"));
            e.printStackTrace();
        }

        if (isAjaxRequest())
        {
            Gson gson = new Gson();

            //ServletOutputStream out = res.getOutputStream();
            PrintWriter writer = res.getWriter();
            Map<AjaxResponseParams,Object> outResult = getResultJson(recordType,resultMap);
            logger.debug("outResult :: {}", outResult);

            if ( outResult == null || outResult.size() == 0 )
            {
                outResult.put(AjaxResponseParams.success, false);
                outResult.put(AjaxResponseParams.message, "unknown error");
                outResult.put(AjaxResponseParams.timestamp, System.currentTimeMillis());
            }

            writer.print(gson.toJson(outResult));
        }
        else
        {
            RequestDispatcher dispatcher;
            ResultVO finalResult = getFinalResult(recordType,resultMap);
            try
            {
                if (finalResult != null && finalResult.isSuccess()) // redirect to the list page
                {
                    logger.debug("redirecting to list page");
                    if (recordType.equals("additionalheader"))
                    {
                        recordType = "additional";
                    }

                    StringBuilder redirectPath = new StringBuilder(req.getContextPath());
                    redirectPath.append("/ItemList?type=").append(recordType);

                    if (recordClass != null && !("".equals(recordClass)))
                    {
                        redirectPath.append("&recordClass=").append(recordClass);
                    }

                    if (subType != null && !("".equals(subType)))
                    {
                        redirectPath.append("&subtype=").append(subType);
                    }

                    if (sectionName != null && !("".equals(sectionName)))
                    {
                        redirectPath.append("&sectionname=").append(sectionName);
                    }

                    redirectPath.append("&documentid=").append(documentID);
                    redirectPath.append("&recid=").append(finalResult.getId());
                    redirectPath.append("&success=").append(finalResult.isSuccess());
                    redirectPath.append("&CompletedMessage=").append(StringUtils.join(finalResult.getMessages(), ','));
                    redirectPath.append("&timestamp=").append(finalResult.getTimestamp());

                    logger.debug("redirectPath :: {}", redirectPath.toString());
                    res.setContentType("text/html");
                    res.sendRedirect(redirectPath.toString());

                    return;
                }
                else // There was an error on the page
                {
                    logger.debug("redirecting to edit page");

                    Map<String, Object> record = EditRecordHelper.getFormParameterMap(req);

                    // Put the record ID that came as "recid" back into the record as "id"
                    String recid = record.get("recid").toString();
                    if (recid != null) {
                        record.put("id", recid);
                    }

                    logger.debug("record: ", record);
                    req.setAttribute("rec", record);

                    req.setAttribute("recordTypeName", recordType);

                    // Additional Information Related and this documentid is used when we add a new Heading.
                    // These are passed straight through to the JSPs
                    req.setAttribute("documentid", documentID);
                    req.setAttribute("sectionname", sectionName );
                    req.setAttribute("subtype", subType);
                    req.setAttribute("rectype", recordType);
                    req.setAttribute("recordClass", recordClass);

                    if (subType.length() > 0)
                    {
                        subType = "-" + subType.toLowerCase();
                    }


                    /*String recordID = req.getParameter("recid");
                    if (recordID!=null && recordID.trim().length()>0)
                    {
                        req.setAttribute("E"+recordID, "Edit");
                    }
                    else
                    {
                        req.setAttribute("E0000", "Add a New Record");
                    }*/

                    String usepopups = req.getParameter("usepopups");
                    if (StringUtils.isNotEmpty(usepopups))
                    {
                        usepopups = "false";
                    }
                    req.setAttribute("usepopups", usepopups);
//                    if (usepopups != null && usepopups.trim().equalsIgnoreCase("true"))
//                    {
//                        usepopups = "true";
//                        req.setAttribute("usepopups", "true");
//                    }
//                    else
//                    {
//                        usepopups = "false";
//                        req.setAttribute("usepopups", "false");
//                    }

                    Map<String,Object> config = new HashMap<String,Object>();

                    Properties p = PropertyManager.getPropertySet(recordType+subType, "strings");
                    config.put("strings", p);

                    p = PropertyManager.getPropertySet(recordType, "config");
                    String editorForm = p.getProperty("editorpage");
                    config.put("config", p);

                    p = PropertyManager.getPropertySet(recordType, "labels");
                    config.put("labels", p);

                    p = PropertyManager.getPropertySet(recordType, "tooltips");
                    config.put("tooltips", p);

                    config.put("options", MIVConfig.getConfig().getConstants());

                    req.setAttribute("constants", config);


                    String destination = "/jsp/editform.jsp";
                    if ( Boolean.valueOf((String)p.get("usepopups")) || Boolean.valueOf(usepopups) )
                    {
                        destination = "/jsp/" + editorForm;
                    }
                    else
                    {
                        req.setAttribute("editorform", editorForm);
                    }

                    req.setAttribute("success",finalResult.isSuccess());
                    req.setAttribute("CompletedMessage","<div id=\"errorbox\">"+StringUtils.join(finalResult.getMessages(), ',')+"</div>");
                    req.setAttribute("timestamp",finalResult.getTimestamp());

                    dispatcher = req.getRequestDispatcher(destination);

                    // Get the referer and set in the session servlet context.
                    // This value will be retrieved by the PDF upload to allow returning to the
                    // servlet which referred to this page, which is most likely ItemList servlet.
                    String returnTo = req.getHeader("referer");
                    req.getSession().setAttribute("returnTo", returnTo);

                    res.setHeader("cache-control", "no-cache, must-revalidate");
                    res.addHeader("Pragma", "no-cache");
                    res.setDateHeader("Expires", System.currentTimeMillis() - 2);
                    dispatcher.forward(req, res);

                }

            }
//            catch (ServletException se)
//            {
//                // log.throwing("EditRecord", "doGet", se);
//                se.printStackTrace(); // .printStackTrace() is not an acceptable way to handle an exception.
//                throw (se);
//            }
//            catch (IOException ioe)
//            {
//                // log.throwing("EditRecord", "doGet", ioe);
//                throw (ioe);
//            }
            finally
            {
                logger.trace(" ***** Leaving EditRecord doGet  *****\t\tGET\n");
            }
        }
    }
    //doPost()

    /**
     * set record specific parameters, those are not set by edit record servlet.
     * it also used to set some parameters into request or session.
     * @param req
     * @param res
     */
    public void setUpForm(HttpServletRequest req, HttpServletResponse res)
    {
        // set up form specific data
    }

    /**
     * prepare final result object with collection of errors or success message.
     * @param mainRecordType
     * @param resultMap
     * @return
     */
    private ResultVO getFinalResult(String mainRecordType, Map<String, ResultVO> resultMap)
    {
        Gson gson = new Gson();
        ResultVO finalResult = null;
        String out = "";
        boolean success=true;
        String recordType = "";
        Map<AjaxResponseParams,Object> outResult = new HashMap<>();

        try
        {
            if (resultMap != null)
            {
                ResultVO mainResultVO = resultMap.get(mainRecordType);
                resultMap.remove(mainRecordType);

                if (resultMap.size() > 0)
                {
                    // process child records results only
                    ResultVO resultVO = null;

                    for (Entry<String, ResultVO> entry : resultMap.entrySet())
                    {
                        resultVO = entry.getValue();
                        recordType = entry.getKey();
                        if (recordType.equals("additionalheader"))
                        {
                            recordType = "additional";
                        }

                        if (resultVO != null && !resultVO.isSuccess())
                        {
                            success = false;
                            out += prepareErrorMessage(recordType, resultVO.getErrorfields(), resultVO.getMessages());
                        }
                    }
                }

                // process mainRecord result
                if ( success && mainResultVO.isSuccess() ) // Success
                {
                    Properties cfgProps = PropertyManager.getPropertySet(mainRecordType, "config");
                    String reloadOptions = cfgProps.getProperty("reloadoptions");
                    if (reloadOptions != null && reloadOptions.trim().equalsIgnoreCase("true"))
                    {
                        MIVConfig.getConfig().reload(ConfigReloadConstants.CONSTANTS);
                    }

                    if (logger.isDebugEnabled()) {
                        System.out.println("EditRecord.getFinalResult() --");
                        System.out.println(" success:" + mainResultVO.isSuccess());
                        System.out.println(" recnum:" + mainResultVO.getId());
                        System.out.println(" message:" + mainResultVO.getMessages());
                        System.out.println(" timestamp:" + mainResultVO.getTimestamp());
                    }

                    outResult.put(AjaxResponseParams.success, true);
                    outResult.put(AjaxResponseParams.recnum, mainResultVO.getId());
                    outResult.put(AjaxResponseParams.message, mainResultVO.getMessages());
                    outResult.put(AjaxResponseParams.timestamp, mainResultVO.getTimestamp());

                    out = gson.toJson(outResult);

                    finalResult = new ResultVO(true,mainResultVO.getMessages(), null, mainResultVO.getId());

                }
                else // Fail
                {
                    success = false;
                    // main record failed
                    if (!mainResultVO.isSuccess())
                    {
                        out += prepareErrorMessage(mainRecordType, mainResultVO.getErrorfields(), mainResultVO.getMessages());
                    }

                    logger.debug("success:[{}]   out: [{}]", success, out);

                    /*out = "{ success:false, errorfields:" + (fields != null ? fields : "")
                           + ", message:" + (messages != null ? messages : "unknown error")
                           + ", timestamp:" + mainResultVO.getTimestamp() + " }";*/

                    finalResult = new ResultVO(false, out != null ? "<ul>Error(s) :" + out + "</ul>" : "unknown error");
                }
            }
            else
            {
                finalResult = new ResultVO(false, "unknown error");
            }
        }
        catch (Exception e) // FIXME: Do not catch plain Exception
        {
            // TODO Auto-generated catch block
            e.printStackTrace(); // FIXME: Do not printStackTrace() - log an info, warning, or error instead
            finalResult = new ResultVO(false, "unknown error");
        }
        return finalResult;
    }


    /**
     * prepare list of error messages
     *
     * @param recordType
     * @param fields
     * @param messages
     * @return
     */
    private String prepareErrorMessage(String recordType, List<String> fields, List<String> messages)
    {
        String out = "";
        Properties labelProperties = PropertyManager.getPropertySet(recordType, "labels");
        Properties errorProperties = PropertyManager.getPropertySet(recordType, "errorlabels");

        logger.debug("errorProperties :: {}", errorProperties);

        String fieldLabel = "";
        String field = "";
        if (messages.isEmpty())
        {
            out += "<li> Error on Page </li>";
        }
        else
        {
            for (int index = 0; index < messages.size(); index++)
            {
                fieldLabel = "";
                if (index < fields.size())
                {
                    field = fields.get(index);

                    // check for error labels for error field
                    if (errorProperties != null)
                    {
                        fieldLabel = errorProperties.getProperty(field, field);
                    }

                    // else get labels for error field
                    if (fieldLabel == null || fieldLabel.trim().length() == 0)
                    {
                        fieldLabel = labelProperties.getProperty(field, field);
                    }

                    fieldLabel = fieldLabel.replaceAll(":", "") + " : ";
                }

                out += "<li>" + fieldLabel + messages.get(index) + "</li>";
            }
        }

        return out;
    }

    /**
     * prepare result json map
     * @param mainRecordType
     * @param resultMap
     * @return
     */
    private Map<AjaxResponseParams,Object> getResultJson(String mainRecordType,Map<String, ResultVO> resultMap)
    {
        Map<AjaxResponseParams,Object> outMap = new HashMap<AjaxResponseParams,Object>();
        boolean success = true;

        try
        {
            if (resultMap != null)
            {
                ResultVO mainResultVO = resultMap.get(mainRecordType);
                outMap.put(AjaxResponseParams.timestamp, System.currentTimeMillis());

                resultMap.remove(mainRecordType);

                // process mainRecord result
                if ( success && mainResultVO.isSuccess() ) // Success
                {
                    Properties cfgProps = PropertyManager.getPropertySet(mainRecordType, "config");
                    String reloadOptions = cfgProps.getProperty("reloadoptions");
                    if (reloadOptions != null && reloadOptions.trim().equalsIgnoreCase("true"))
                    {
                        MIVConfig.getConfig().reload(ConfigReloadConstants.CONSTANTS);
                    }

                    if (logger.isDebugEnabled()) {
                        System.out.println("EditRecord.getResultJson() -- success branch");
                        System.out.println(" success:" + mainResultVO.isSuccess());
                        System.out.println(" recnum:" + mainResultVO.getId());
                        System.out.println(" message:" + mainResultVO.getMessages());
                        System.out.println(" timestamp:" + mainResultVO.getTimestamp());
                    }

                    outMap.put(AjaxResponseParams.success, true);
                    outMap.put(AjaxResponseParams.recnum, mainResultVO.getId());
                    outMap.put(AjaxResponseParams.message, mainResultVO.getMessages());
                }
                else // Fail
                {
                    List<String> messages = new ArrayList<>();
                    List<String> fields = new ArrayList<>();

                    if (resultMap.size() > 0)
                    {
                        // process child records results only
                        ResultVO resultVO = null;
                        for (Entry<String, ResultVO> entry : resultMap.entrySet())
                        {
                            resultVO = entry.getValue();

                            if (resultVO != null && !resultVO.isSuccess())
                            {
                                success = false;
                                messages.addAll(resultVO.getMessages());
                                fields.addAll(resultVO.getErrorfields());
                            }
                        }
                    }

                    // main record save success fully
                    if (!mainResultVO.isSuccess())
                    {
                        messages = mainResultVO.getMessages();
                        fields = mainResultVO.getErrorfields();
                    }

                    if (logger.isDebugEnabled()) {
                        System.out.println("EditRecord.getResultJson() -- fail branch");
                        System.out.println(" success: false" );
                        System.out.println(" recnum:" + mainResultVO.getId());
                        System.out.println(" errorfields:" + fields);
                        System.out.println(" message:" + messages);
                        System.out.println(" timestamp:" + mainResultVO.getTimestamp());
                    }

                    outMap.put(AjaxResponseParams.success, false);
                    outMap.put(AjaxResponseParams.errorfields, fields);
                    outMap.put(AjaxResponseParams.message, (messages.isEmpty() ? Arrays.asList("unknown error") : messages));
                }
            }
            else
            {
                outMap.put(AjaxResponseParams.success, false);
                outMap.put(AjaxResponseParams.message, Arrays.asList("unknown error"));
            }
        }
        catch (Exception e) // FIXME: Don't catch plain 'Exception' - catch something specific
        {
            // TODO Auto-generated catch block
            e.printStackTrace(); // FIXME: printStackTrace is not an acceptable response to catching an exception
            outMap.put(AjaxResponseParams.success, false);
            outMap.put(AjaxResponseParams.message, Arrays.asList("unknown error"));
        }

        return outMap;
    }


    /**
     * to get the array of records to be executed
     * @param req HttpServletRequest Object
     * @return Array of RecordVO Object
     */
    RecordVO[] getRecords(Map<String, String[]> reqData) //throws IOException
    {
        RecordVO[] recordVO = { new RecordVO(true, reqData) };

        return recordVO;
    }


    /**
     * Gather the request parameters and values into parallel arrays.
     * The first array is the list of parameter names; the second is the values.
     *
     * @param reqData Map of request's parameter name and value
     * @return An array of exactly two (2) String arrays.
     */
    ValidateResultVO collectValidateParameters(Map <String, String[]> reqData, Properties cfgProps)
    {
        return erHelper.collectValidateParameters(reqData, cfgProps);
    }


    /**
     * Find the record ID key in the request parameters.
     * Searches the parmeters for one in the form "E&lt;nnnn&gt;=edit"
     * where &lt;nnnn&gt; is any 1 or more digit number.
     * @return the key parameter, or <code>null</code> if none was found.
     */
    String findKey(HttpServletRequest req)
    {
        Map<?,?> params = req.getParameterMap();
        boolean matched = false;
        String keyval = null;
        boolean addingAdditionalRecord = false;

        for (Object k : params.keySet())
        {
            String p = (String)k;
            if ( p.matches("AR[0-9]+"))
            {
                addingAdditionalRecord = true;
            }


            // The edit button uses an 'E' prefix on the record number, prototype used 'k' prefix
            if (p.matches("E[0-9]+") || p.matches("k[0-9]+") || p.matches("EH[0-9]+") || p.matches("AR[0-9]+"))
            {
                String[] vals = req.getParameterValues(p);
                for (String v : vals)
                {
                    if (v.equalsIgnoreCase("edit"))
                    {
                        matched = true;
                        break;
                    }
                    if (v.equalsIgnoreCase("Add a New Record"))
                    {
                        matched = true;
                        if (!addingAdditionalRecord)
                        {
                            p = "E0000";
                        }
                        break;
                    }
                    if (v.equalsIgnoreCase("Add a PDF Document"))
                    {
                        matched = true;
                        if (!addingAdditionalRecord)
                        {
                            p = "E0000";
                        }
                        break;
                    }
                    if (v.equalsIgnoreCase("Add a New Section"))
                    {
                        matched = true;
                        p = "E0000";
                        break;
                    }
                }
                if (matched)
                {
                    keyval = p;
                    break;
                }
            }
        }

        if (keyval != null && keyval.length() > 0) {
            keyval = keyval.replaceFirst("[a-zA-Z]*", "");
        }

        return keyval;
    }


    /**
     * Find the record ID key in the request parameters.
     * Searches the parmeters for one in the form "D&lt;nnnn&gt;=Delete"
     * where &lt;nnnn&gt; is any 1 or more digit number.
     * @return the key parameter, or <code>-9999</code> if none was found.
     */
    String findDeleteKey(HttpServletRequest req)
    {
        Map<?,?> params = req.getParameterMap();

        String keyval = "-9999";

        for (Object k : params.keySet())
        {
            String p = (String) k;
            if ( p.matches("D[0-9]+") )
            {
                String[] vals = req.getParameterValues(p);
                for (String v : vals)
                {
                    if ("Delete".equalsIgnoreCase(v))
                    {
                        keyval = p;
                        break;
                    }
                }
            }
        }

        if (keyval != null && keyval.length() > 0 && !keyval.equals("-9999")) {
            keyval = keyval.replaceFirst("[a-zA-Z]*", "");
        }

        return keyval;
    }


    private void escapeMarkup(Map<String, String> rec)
    {
        for (String key : rec.keySet())
        {
            String value = MIVUtil.escapeMarkup(rec.get(key));
            rec.put(key, value);
        }
    }
}
