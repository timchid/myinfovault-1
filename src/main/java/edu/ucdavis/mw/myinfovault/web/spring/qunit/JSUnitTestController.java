
package edu.ucdavis.mw.myinfovault.web.spring.qunit;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * Serves the unit test page.
 * @author japorito
 * @since 5.0
 */
@Controller
public class JSUnitTestController
{
    @RequestMapping("/unittests")
    public  ModelAndView getUnitTestPage() {
        ModelAndView mav = new ModelAndView("unittests");
        return mav;
    }
}
