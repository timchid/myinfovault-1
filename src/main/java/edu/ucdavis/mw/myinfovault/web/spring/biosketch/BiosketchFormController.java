/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: BiosketchFormController.java
 */

// TODO: rename this class and move it out of "biosketch"
package edu.ucdavis.mw.myinfovault.web.spring.biosketch;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * @author Stephen Paulsen
 * @since MIV 2.1
 */
public abstract class BiosketchFormController// extends AbstractController
{
//    protected final String className = this.getClass().getSimpleName();
//    protected ThreadLocal<MIVSession> mivSession = new ThreadLocal<MIVSession>();
    Logger logger = LoggerFactory.getLogger(this.getClass());

//    @Override
//    public ModelAndView handleRequestInternal(HttpServletRequest request, HttpServletResponse response) throws Exception
//    {
//        MIVSession ms = MIVSession.getSession(request);
//        if (ms == null || ! ms.hasSession())
//        {
//            if (ms == null) System.out.println("\n\n" + className + ": MIVSession is NULL");
//            else if (! ms.hasSession()) System.out.println("\n\n" + className + ": MIVSession.hasSession() is FALSE");
//            System.out.println("" + className + ": Redirecting to /logout.html\n");
//            URL logoutTargetUrl = new URL("http", request.getServerName(), "/logout.html");
//            response.sendRedirect(logoutTargetUrl.toString());
//            return null;
//        }

//        this.mivSession = ms;
//        this.mivSession.set(ms);

//        request.setCharacterEncoding("utf-8");
//        response.setCharacterEncoding("utf-8");

//        return super.handleRequestInternal(request, response);
//    }
}
