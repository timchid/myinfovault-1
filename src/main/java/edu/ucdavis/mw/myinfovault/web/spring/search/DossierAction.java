/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: DossierAction.java
 */

package edu.ucdavis.mw.myinfovault.web.spring.search;

import java.util.List;
import java.util.Map;

import edu.ucdavis.mw.myinfovault.domain.dossier.Dossier;
import edu.ucdavis.mw.myinfovault.dossier.attributes.DossierAppointmentAttributeKey;
import edu.ucdavis.mw.myinfovault.dossier.attributes.DossierAttribute;
import edu.ucdavis.mw.myinfovault.dossier.attributes.DossierAttributeStatus;
import edu.ucdavis.mw.myinfovault.service.authorization.AuthorizationService;
import edu.ucdavis.mw.myinfovault.service.dossier.DossierService;
import edu.ucdavis.mw.myinfovault.service.dossier.WorkflowException;
import edu.ucdavis.mw.myinfovault.service.person.UserService;
import edu.ucdavis.myinfovault.MIVConfig;
import edu.ucdavis.myinfovault.MIVUser;
import edu.ucdavis.myinfovault.exception.MivSevereApplicationError;

/**
 * Spring webflow form action for actions on Dossiers.
 *
 * @author Craig Gilmore
 * @since 4.3
 */
public abstract class DossierAction extends SearchFormAction
{
    private static final String ATTRIBUTE_TYPE_BY_NAME = "attributetypesbyname";

    protected DossierService dossierService;

    /**
     * Create a dossier form action bean.
     *
     * @param searchStrategy Spring-injected search strategy
     * @param authorizationService Spring-injected authorization service
     * @param userService Spring-injected user service
     * @param dossierService Spring-injected dossier service
     * @param packetService Spring-injected packet service
     */
    public DossierAction(SearchStrategy searchStrategy,
                         AuthorizationService authorizationService,
                         UserService userService,
                         DossierService dossierService)
    {
        super(searchStrategy, authorizationService, userService);
        this.dossierService = dossierService;
    }

    /**
     * Get the status of the dossier appointment attribute mapped to the attribute key.
     *
     * @param dossier The dossier containing the attribute status
     * @param appointmentKey Appointment associated with the subset of dossier attribtues
     * @param attributeKey Key to which the associated attribute is mapped
     * @return The status of the the dossier attribute
     */
    protected DossierAttributeStatus getDossierAttibuteStatus(Dossier dossier,
                                                              DossierAppointmentAttributeKey appointmentKey,
                                                              String attributeKey)
    {
        return getDossierAttribute(dossier,
                                   appointmentKey,
                                   attributeKey).getAttributeValue();
    }

    /**
     * Set a dossier appointment attribute to a new status and save changes to the
     * dossier. If new status is equal to the current status, no update occurs.
     *
     * @param dossier The dossier containing the attribute status
     * @param appointmentKey Appointment associated with the subset of dossier attributes
     * @param attributeKey Key to which the associated attribute is mapped
     * @param attributeStatus The new status to set for the dossier attribute
     * @return <code>true</code> if update was successful or
     * no change, <code>false</code> if there was an error.
     */
    protected boolean setDossierAttributeStatus(Dossier dossier,
                                                DossierAppointmentAttributeKey appointmentKey,
                                                String attributeKey,
                                                DossierAttributeStatus attributeStatus)
    {
        //get the attribute to update
        DossierAttribute attribute = getDossierAttribute(dossier,
                                                         appointmentKey,
                                                         attributeKey);

        return setDossierAttributeStatus(dossier, attribute, attributeStatus);
    }

    /**
     * Set a dossier appointment attribute to a new status and save changes to the
     * dossier. If new status is equal to the current status, no update occurs.
     *
     * @param dossier The dossier containing the attribute status
     * @param attribute attribute to update
     * @param attributeStatus The new status to set for the dossier attribute
     * @return <code>true</code> if update was successful or
     * no change, <code>false</code> if there was an error.
     */
    protected boolean setDossierAttributeStatus(Dossier dossier,
                                                DossierAttribute attribute,
                                                DossierAttributeStatus attributeStatus)
    {
        //get current dossier attribute status
        DossierAttributeStatus currentAttributeStatus = attribute.getAttributeValue();

        //proceed if the new status is different from the current
        if (attributeStatus != currentAttributeStatus)
        {
            //set the dossier attribute to the new status value
            attribute.setAttributeValue(attributeStatus);

            try
            {
                //save changes to the dossier
                dossierService.saveDossier(dossier);
            }
            catch (WorkflowException e)
            {
                logger.error("Unable to save dossier " + dossier.getDossierId() +
                             " with update attribute " + attribute, e);

                return false;
            }
        }

        return true;
    }

    //Gets the dossier attribute for the appointment mapped to the given attribute key
    protected DossierAttribute getDossierAttribute(Dossier dossier,
                                                   DossierAppointmentAttributeKey appointmentKey,
                                                   String attributeKey)
    {
        //get the map of all attributes associated with this dossier appointment
        Map<String, DossierAttribute> dossierAttributes = dossier.getAttributesByAppointment(appointmentKey).getAttributes();

        //get the attribute to update
        DossierAttribute attribute = dossierAttributes.get(attributeKey);

        //if the attribute is null
        if (attribute == null)
        {
            //create an attribute for this appointment
            attribute = new DossierAttribute(appointmentKey,
                                             MIVConfig.getConfig().getMap(ATTRIBUTE_TYPE_BY_NAME).get(attributeKey),
                                             dossier.getDossierLocation(),
                                             dossier.getAction().getActionType());

            //add new attribute to the attributes map
            dossierAttributes.put(attributeKey,
                                  attribute);
        }

        return attribute;
    }

    /**
     * Search dossiers by user and criteria.
     *
     * @param criteria main search form containing the search "criteria" requested
     * @param user MIVUser requesting the search
     * @return List of MivActionList objects
     */
    public List<MivActionList> dossierSearch(SearchCriteria criteria, MIVUser user)
    {
        try
        {
            return searchStrategy.dossierSearch(
                       criteria,
                       user,
                       dossierService.getDossiersByUserNameSearchCriteria(
                           user.getTargetUserInfo().getPerson(),
                           criteria));
        }
        catch (WorkflowException e)
        {
            logger.error("dossierSearch: WorkflowException executing SearchStrateg" + searchStrategy.getClass().getName(), e);
        }

        return null;
    }

    /**
     * Get the dossier and load all data.
     *
     * @param long dossierId
     * @return loaded dossier
     */
    public Dossier getDossier(long dossierId)
    {

        try
        {
            return dossierService.getDossierAndLoadAllData(dossierId);
        }
        catch (WorkflowException e)
        {
            throw new MivSevereApplicationError("errors.dossier", e, dossierId);
        }
    }
}
