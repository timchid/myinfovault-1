/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: AssignReviewersAction.java
 */

package edu.ucdavis.mw.myinfovault.web.spring.assignreviewers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import org.springframework.webflow.execution.Event;
import org.springframework.webflow.execution.RequestContext;

import edu.ucdavis.mw.myinfovault.domain.dossier.Dossier;
import edu.ucdavis.mw.myinfovault.domain.group.Group;
import edu.ucdavis.mw.myinfovault.domain.group.GroupComparator;
import edu.ucdavis.mw.myinfovault.domain.group.GroupType;
import edu.ucdavis.mw.myinfovault.domain.group.MemberComparator;
import edu.ucdavis.mw.myinfovault.dossier.attributes.DossierAppointmentAttributeKey;
import edu.ucdavis.mw.myinfovault.dossier.attributes.DossierAttributeStatus;
import edu.ucdavis.mw.myinfovault.events2.DossierReviewEvent;
import edu.ucdavis.mw.myinfovault.events2.EventDispatcher2;
import edu.ucdavis.mw.myinfovault.service.authorization.AssignReviewersAuthorizer;
import edu.ucdavis.mw.myinfovault.service.authorization.AuthorizationService;
import edu.ucdavis.mw.myinfovault.service.authorization.AuthorizationService.Qualifier;
import edu.ucdavis.mw.myinfovault.service.authorization.Permission;
import edu.ucdavis.mw.myinfovault.service.dossier.DossierReviewerDto;
import edu.ucdavis.mw.myinfovault.service.dossier.DossierReviewerDto.EntityType;
import edu.ucdavis.mw.myinfovault.service.dossier.DossierService;
import edu.ucdavis.mw.myinfovault.service.dossier.WorkflowException;
import edu.ucdavis.mw.myinfovault.service.group.GroupService;
import edu.ucdavis.mw.myinfovault.service.group.GroupTypeFilter;
import edu.ucdavis.mw.myinfovault.service.person.AssignedRole;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.mw.myinfovault.service.person.MivRole;
import edu.ucdavis.mw.myinfovault.service.person.UserService;
import edu.ucdavis.mw.myinfovault.util.AttributeSet;
import edu.ucdavis.mw.myinfovault.web.spring.assignment.AssignmentAction;
import edu.ucdavis.mw.myinfovault.web.spring.search.MivActionList;
import edu.ucdavis.mw.myinfovault.web.spring.search.SearchStrategy;
import edu.ucdavis.myinfovault.MIVUser;

/**
 * Controller for the assign reviewers spring webflow.
 *
 * @author Rick Hendricks
 * @author Craig Gilmore
 * @since 3.0
 */
public class AssignReviewersAction extends AssignmentAction
{
    //parameter keys
    private final static String SCHOOLID_PARAM = "schoolId";
    private final static String DEPARTMENTID_PARAM = "departmentId";
    private final static String REVIEW_PARAM = "review";

    //assigned roles allowed to review dossiers
    private static MivRole[] roles = AssignReviewersAuthorizer.getPermittedRoles();
    private static final Set<AssignedRole> reviewerAssignedRoles = new HashSet<AssignedRole>(roles.length);
    static
    {
        for (MivRole role : roles)
        {
            reviewerAssignedRoles.add(new AssignedRole(role, true));
        }
    }


    /**
     * Create the assign reviewers webflow form action bean invoked
     * in <code>/WEB-INF/flows/openactions-flow-beans.xml</code>.
     *
     * @param searchStrategy bean injected implementation of SearchStrategy
     * @param authorizationService bean injected authorization service
     * @param userService bean injected user service
     * @param dossierService bean injected dossier service
     * @param groupService bean injected group service
     */
    public AssignReviewersAction(SearchStrategy searchStrategy,
                                 AuthorizationService authorizationService,
                                 UserService userService,
                                 DossierService dossierService,
                                 GroupService groupService)
    {
        super(searchStrategy,
              authorizationService,
              userService,
              dossierService,
              groupService);
    }


    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean canAccess(RequestContext context)
    {
        return authorizationService.hasPermission(getShadowPerson(context),
                                                  Permission.ASSIGN_REVIEWERS,
                                                  null);
    }


    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean canView(RequestContext context)
    {
        return canEdit(context);
    }


    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean canEdit(RequestContext context)
    {
        //get form for this flow
        AssignReviewersForm form = getFormObject(context);

        //permission details for dossier's location and delegation authority
        AttributeSet permissionDetails = new AttributeSet();

        permissionDetails.put(AssignReviewersAuthorizer.DOSSIER_LOCATION,
                              form.getDossier().getLocation());

        //School and department qualifiers for the dossier's appointment
        AttributeSet qualification = new AttributeSet();

        qualification.put(Qualifier.DEPARTMENT,
                          Integer.toString(form.getAppointmentKey().getDepartmentId()));

        qualification.put(Qualifier.SCHOOL,
                          Integer.toString(form.getAppointmentKey().getSchoolId()));

        //is the user allowed to assign reviewers to this dossier?
        return authorizationService.isAuthorized(getShadowPerson(context),
                                                 Permission.ASSIGN_REVIEWERS,
                                                 permissionDetails,
                                                 qualification);
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public Event addBreadcrumb(RequestContext context)
    {
        addBreadcrumbs(context,
                       getFlowId(context).equalsIgnoreCase("assignReviewers-assignment-flow")//in the sub-flow?
                     ? "Assign Dossier Reviewers"
                     : "Assign Dossier Reviewers List");

        return success();
    }


    /**
     * Loads the assign reviewers flow form backing object for the dossier and scope in the context.
     *
     * @param context Webflow request context
     * @return The form backing object for this action
     */
    @Override
    public Object createFormObject(RequestContext context) throws Exception
    {
        //the switched-to, acting person
        MivPerson actingPerson = getShadowPerson(context);

        //the target dossier, owner, and appointment key
        Dossier dossier = getDossier(context);
        DossierAppointmentAttributeKey appointmentKey = Dossier.getAppointmentKey(getParameterValue(context, SCHOOLID_PARAM, 0),
                                                                                  getParameterValue(context, DEPARTMENTID_PARAM, 0));

        //Dossiers may already have people or groups assigned to review
        SortedSet<MivPerson> peopleReviewers = new TreeSet<MivPerson>(new MemberComparator());
        SortedSet<Group> groupReviewers = new TreeSet<Group>(new GroupComparator());

        //separate user entities from group entities
        for (DossierReviewerDto reviewerDto : getReviewersDtos(dossier, appointmentKey))
        {
            switch (reviewerDto.getEntityType())
            {
                case USER:
                    peopleReviewers.add(userService.getPersonByMivId(reviewerDto.getEntityId()));
                    break;
                case GROUP:
                    groupReviewers.add(groupService.getGroup(reviewerDto.getEntityId()));
                    break;
            }
        }

        //remove null from people and groups reviewer sets
        peopleReviewers.remove(null);
        groupReviewers.remove(null);

        /*
         * Set to hold all inactive person reviewers if any. This can only
         * be the owner of the dossier and only if the the owner were
         * somehow assigned to review this dossier.
         */
        Set<MivPerson> inactiveReviewers = new HashSet<MivPerson>(1);

        //remove the owner of the dossier from the reviewers list if they were somehow assigned
        recuseOwner(dossier.getAction().getCandidate(),//owner
                    inactiveReviewers,
                    peopleReviewers,
                    groupReviewers);

        //set up new form with dossier details and current reviewers
        return new AssignReviewersForm(dossier.getAction().getCandidate(),
                                       dossier,
                                       appointmentKey,
                                       getDossierAttibuteStatus(dossier, appointmentKey, REVIEW_PARAM) == DossierAttributeStatus.OPEN,
                                       inactiveReviewers,
                                       peopleReviewers,
                                       groupReviewers,
                                       actingPerson);
    }


    /**
     * Get the dossiers that this user is allowed to view.
     *
     * @param user The logged-in MIV user
     * @return List of MivActionList objects associated with the user
     */
    public List<MivActionList> getDossiers(MIVUser user)
    {
        logger.info("Using "+searchStrategy.getClass().getSimpleName()+" in "+this.className);

        return searchStrategy.getDossiersToAssignReviewers(user);
    }


    //Gets a set of dossier reviewer DTOs for the given dossier and appointment at the current location
    private Set<DossierReviewerDto> getReviewersDtos(Dossier dossier,
                                                     DossierAppointmentAttributeKey appointmentKey)
    {
        try
        {
            return new HashSet<DossierReviewerDto>(dossierService.getDossierReviewers(dossier,
                                                                                      appointmentKey));
        }
        catch (WorkflowException e)
        {
            logger.error("Unable to retrieve currently assigned reviewers for dossier " + dossier.getDossierId(), e);
        }

        return Collections.emptySet();
    }


    /**
     * Sets the dossier review period open for this assignment.
     *
     * @param context Request context from webflow
     * @return Webflow event status
     */
    public Event setOpen(RequestContext context)
    {
        getFormObject(context).setReviewOpen(true);

        return success();
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public Event save(RequestContext context)
    {
        //get form object
        AssignReviewersForm form = getFormObject(context);

        //acting switched-to person for this session
        MivPerson shadowPerson = getShadowPerson(context);

        //recuse the owner of the dossier from the sets of reviewers if he were somehow assigned
        recuseOwner(form.getOwner(),
                    form.getInactivePeople(),
                    form.getAssignedPeople(),
                    form.getAssignedGroups());

        //get a shallow copy of assigned people so as not to alter the assignment form
        Set<MivPerson> assignedPeople = new HashSet<MivPerson>(form.getAssignedPeople());

        //remove all inactive reviewers from the assigned people
        for (MivPerson assignedPerson : assignedPeople)
        {
            for (MivPerson inactivePerson : form.getInactivePeople()) {
                if (assignedPerson.getUserId() == inactivePerson.getUserId()) {
                    assignedPeople.remove(assignedPerson);
                }
            }
        }

        Set<DossierReviewerDto> newReviewers = new HashSet<DossierReviewerDto>(assignedPeople.size()
                                                                             + form.getAssignedGroups().size());

        //create a dossier reviewer DTO for each user assigned
        for (MivPerson personReviewer : assignedPeople)
        {
            newReviewers.add(new DossierReviewerDto(personReviewer.getUserId(),
                                                    EntityType.USER,
                                                    form));
        }

        //create a dossier reviewer DTO for each group assigned
        for (Group groupReviewer : form.getAssignedGroups())
        {
            if (groupReviewer.getCreator().getUserId() == shadowPerson.getUserId() ||
               (!groupReviewer.isConfidential() && !groupReviewer.isReadOnly()))
            {
                try
                {
                    //persist any changes to this group
                    groupService.persist(shadowPerson,
                                         groupReviewer);
                }
                catch(SecurityException e)
                {
                    //no update was performed on the group
                    logger.warn(e.getMessage());
                }
            }

            newReviewers.add(new DossierReviewerDto(groupReviewer.getId(),
                                                    EntityType.GROUP,
                                                    form));
        }

        //get previously assigned reviewers for this dossier
        Set<DossierReviewerDto> formerReviewers = getReviewersDtos(form.getDossier(),
                                                                   form.getAppointmentKey());

        //update reviewers for this dossier
        dossierService.updateDossierReviewers(shadowPerson,
                                              newReviewers,
                                              formerReviewers);

        //remove group copies that ultimately were not assigned
        form.getCopiedGroups().removeAll(form.getAssignedGroups());
        groupService.delete(shadowPerson,
                            form.getCopiedGroups());

        return success();
    }


    /**
     * Recuse the owner of the dossier from his appearance in the reviewers sets.
     *
     * @param owner
     * @param inactiveReviewers
     * @param peopleReviewers
     * @param groupReviewers
     */
    private void recuseOwner(MivPerson owner,
                             Set<MivPerson> inactiveReviewers,
                             Set<MivPerson> peopleReviewers,
                             Set<Group> groupReviewers)
    {
        deactivatePerson(owner,
                         inactiveReviewers,
                         peopleReviewers);

        recuseOwner(owner,
                    groupReviewers);
    }


    /**
     * Recuse the owner of the dossier from his appearance in any assigned reviewer groups.
     *
     * @param owner
     * @param groupReviewers
     */
    private void recuseOwner(MivPerson owner,
                             Set<Group> groupReviewers)
    {
        for (Group groupReviewer : groupReviewers)
        {
            deactivatePerson(owner,
                             groupReviewer.getInactive(),
                             groupReviewer.getMembers());
        }
    }


    /**
     * Deactivate person for given sets of inactive and assigned.
     *
     * @param person
     * @param inactive
     * @param assigned
     */
    private void deactivatePerson(MivPerson person,
                                  Set<MivPerson> inactive,
                                  Set<MivPerson> assigned)
    {
        //add person to the inactive set
        inactive.add(person);

        //inactive set must be a subset of the assigned
        inactive.retainAll(assigned);
    }


    /**
     * {@inheritDoc}
     */
    @Override
    protected List<Object> getAvailable(RequestContext context)
    {
        List<Object> available = new ArrayList<Object>();

        //retrieve all available groups and people
        SortedSet<MivPerson> availablePeople = getAvailablePeople(context);
        SortedSet<Group> availableGroups = getAvailableGroups(context);

        //owner of the dossier for this assignment
        MivPerson owner = getFormObject(context).getOwner();

        //remove owner from people search results
        availablePeople.remove(owner);

        //recuse owner of dossier in search results of available groups
        recuseOwner(owner,
                    availableGroups);

        //combine available people/groups in one collection
        available.addAll(availablePeople);
        available.addAll(availableGroups);

        return available;
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public Event bind(RequestContext context) throws Exception
    {
        //pass off to super for bulk user additions/removals
        Event event = super.bind(context);

        //the switched-to, acting person
        MivPerson actingPerson = getShadowPerson(context);

        //get form object
        AssignReviewersForm form = getFormObject(context);

        //If the remove all flag is not set, copy added groups
        if (!form.isRemoveAll())
        {
            //newly assigned groups are initially of type 'GROUP'
            Set<Group> newGroups = (new GroupTypeFilter(GroupType.GROUP)).apply(form.getAssignedGroups());

            //make 'REVIEW' type copies of all newly assigned groups
            Set<Group> copiedGroups = groupService.copy(actingPerson,
                                                        GroupType.REVIEW,
                                                        reviewerAssignedRoles,
                                                        newGroups);

            //replace newly assigned with copies
            form.getAssignedGroups().removeAll(newGroups);
            form.getAssignedGroups().addAll(copiedGroups);

            //also add copies to the copied list
            form.getCopiedGroups().addAll(copiedGroups);

            //the proposed review period status from the form
            DossierAttributeStatus newStatus = form.isReviewOpen()
                                             ? DossierAttributeStatus.OPEN
                                             : DossierAttributeStatus.CLOSE;

            // Get the current review period status
            DossierAttributeStatus currentStatus = getDossierAttibuteStatus(form.getDossier(),
                    form.getAppointmentKey(),
                    REVIEW_PARAM);


            if (!setDossierAttributeStatus(form.getDossier(),
                                           form.getAppointmentKey(),
                                           REVIEW_PARAM,
                                           newStatus))
            {
                return error();
            }

            // Post the DossierReviewEvent if the status is changed
            if (newStatus != currentStatus)
            {
                DossierReviewEvent drEvent = new DossierReviewEvent(getRealPerson(context),
                                                                    getDossier(context),
                                                                    getDossier(context).getDossierLocation(),
                                                                    newStatus);

                EventDispatcher2.getDispatcher().post(drEvent.setShadowPerson(getShadowPerson(context)));
            }

        }

        //reset form fields
        form.reset();

        return event;
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public Event bindAndValidate(RequestContext context) throws Exception
    {
        Event event = bind(context);

        //validate if bind is successful
        if (event.getId().equals(success().getId()))
        {
            return validate(context);
        }

        return event;
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public AssignReviewersForm getFormObject(RequestContext context)
    {
        return (AssignReviewersForm) super.getFormObject(context);
    }
}
