/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: SwitchUserController.java
 */


package edu.ucdavis.mw.myinfovault.web.spring.switchuser;

import static edu.ucdavis.myinfovault.MIVSession.getSession;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.myinfovault.MIVSession;
import edu.ucdavis.myinfovault.MIVUser;

/**
 * Handles user switching.
 *
 * @author Jacob Saporito
 * @since MIV 4.8.6
 */
@Controller
public class SwitchUserController
{
    /**
     * Switch user to a someone who is in the current user's most recently used list.
     */
    @RequestMapping(value="/switchUser")
    public ModelAndView fromPickList(HttpServletRequest req, @RequestParam Integer pick)
    {
        MIVSession session = getSession(req);
        List<MivPerson> pickList = session.getMru();
        MIVUser currentUser = session.getUser();
        MivPerson target;

        if (pick < pickList.size())
        {
            target = pickList.get(pick);
            session.updateMRU(target);
            currentUser.switchto(target.getUserId());
        }

        return new ModelAndView("redirect:/MIVMain");
    }
}
