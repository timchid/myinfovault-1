/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: Selectable.java
 */

package edu.ucdavis.mw.myinfovault.util;

/**
 * Convenience interface to ensure implementation may be selected.
 * For use with objects that require a simple boolean value be bound
 * to them (e.g. A check box from the user interface).
 *
 * @author Craig Gilmore
 * @since MIV 4.4.2
 */
public interface Selectable
{
    /**
     * @return If the item is selected
     */
    public boolean isSelected();

    /**
     * @param selected If the item is selected
     */
    public void setSelected(boolean selected);
}
