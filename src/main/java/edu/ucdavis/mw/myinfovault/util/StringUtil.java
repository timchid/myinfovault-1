/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: StringUtil.java
 */

package edu.ucdavis.mw.myinfovault.util;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.ParsePosition;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.WordUtils;

import com.google.common.base.CharMatcher;
import com.google.common.base.Splitter;
import com.google.common.collect.Iterators;

import edu.ucdavis.myinfovault.MIVConfig;

/**
 * Standard string and text operations and comparators.
 *
 * @author Stephen Paulsen
 * @author Craig Gilmore
 * @author Pradeep Haldiya
 */
public class StringUtil
{
    /**
     * Field allows us to be explicit when using an empty string value.
     */
    public static final String EMPTY_STRING = "".intern();

    /**
     * Field allows us to be explicit when using a space.
     */
    public static final String SPACE = " ".intern();

    /**
     * This String Comparator is safe to use with nulls and treats null and empty Strings as equal.
     */
    public static Comparator<String> nullComparator = new Comparator<String>()
    {
        // 'a' compared to 'b' with null handling
        @Override
        public int compare(String a, String b)
        {
            // Treat null and empty the same
            if (a == null) a = EMPTY_STRING;

            return a.compareTo(b == null ? EMPTY_STRING : b);
        }
    };


    /**
     * Prepare search string for search. <strong>FIXME: by doing what, for what purpose?</strong>
     * @param searchText
     * @param replacement
     * @return search text with commas and space replaced with "%"
     */
    public static String prepareTextSearchCriteria(String searchText, String replacement)
    {
        if (replacement == null) {
            replacement = "%";
        }

        Matcher commaMatcher = commaPattern.matcher(searchText.trim());
        String step1 = commaMatcher.replaceAll(replacement);

        Matcher spaceMatcher = spacesPattern.matcher(step1);
        String result = spaceMatcher.replaceAll(replacement);

        return result;
    }

    /* Regular Expressions used in prepareTextSearchCriteria() above. */
    private static final String regexCommaWithSpaces = "\\s*,\\s*";
    private static final String regexSpaces = "\\s+";
    /* Pattern objects pre-compiled from the regexes */
    private static final Pattern commaPattern = Pattern.compile(regexCommaWithSpaces);
    private static final Pattern spacesPattern = Pattern.compile(regexSpaces);



    /**
     * FIXME: Need Javadoc to describe this method.
     * FIXME: SDP wrote this javadoc, as a guess. Whoever wrote this method, is this doc correct?
     *
     * Get the number of bytes used by a String.
     * This is used to verify that a String will fit within a byte-allocated space.
     * @param s a String
     * @return the number of bytes used to hold the String
     */
    public static int getSizeInBytes(String s)
    {
        return (s == null ? 0 : s.getBytes().length);
    }


    /**
     * Convert a list of entries into a JSON array of objects.
     * @param entries maps to convert to JSON
     * @return converted JSON
     */
    public static String toJson(Iterable<Map<String,String>> entries)
    {
        StringBuilder result = new StringBuilder();

        if (entries == null || ! entries.iterator().hasNext())
        {
            return "[ ]";
        }

        result.append("[ ");

        boolean firstRecord = true;
        for (Map<String,String> item : entries)
        {
            if (!firstRecord) result.append(",\n");
            firstRecord = false;

            result.append("{ ");
            boolean firstEntry = true;
            for (Entry<String,String> e : item.entrySet())
            {
                if (!firstEntry) result.append(", ");
                firstEntry = false;
                result.append("\"" + jsonEscape(e.getKey()) + "\"").append(" : ").append("\"" + jsonEscape(e.getValue()) + "\"");
            }
            result.append(" }");
        }

        result.append(" ]\n");

        return result.toString();
    }


    /**
     * Escape quote marks withing JSON data.
     * When building JSON responses that have quoted attributes and quoted values,
     * any quote marks within the data will prematurely terminate the string unless
     * they are escaped.
     * @param s
     * @return the escaped String
     */
    private static String jsonEscape(String s)
    {
        return s != null ? s.replaceAll("[\n\r]", " ").replaceAll("'", "&#39;").replaceAll("\"", "&#34") : s;
    }



    private static final String SKIP_CAPITALIZE_KEY = "miv-config-capitalize-skip-list";
    private static final String SKIP_CAPITALIZE_LIST = "in,on,and";

    /**
     * Fully capitalize all words in a string, except those on a configured skip list.
     * @param string
     * @return capitalized string
     */
    public static String capitalizeFully(String string)
    {
        if (StringUtils.isBlank(string))
        {
            return string;
        }

        String skipList = MIVConfig.getConfig().getProperty(SKIP_CAPITALIZE_KEY);

        if (StringUtils.isBlank(skipList))
        {
            skipList = SKIP_CAPITALIZE_LIST;
        }

        // Capitalize all words in the string...
        string = WordUtils.capitalizeFully(string);
        // ...then undo capitalization of words on the skip list.
        for (String skip : skipList.split("\\s*,\\s*"))
        {
            string = string.replaceAll("(?i)(\\s)" + skip + "(\\s)", "$1" + skip + "$2");
        }

        return string;
    }


    /**
     * Returns a string of the first character of the given term.
     *
     * @param term Term from which to get the initial
     * @return The term's initial or an empty string if term is <code>null</code>, whitespace, or empty.
     */
    public static String getInitial(String term)
    {
        return StringUtils.isBlank(term) ? EMPTY_STRING : Character.toString(term.charAt(0));
    }


    /**
     * Return the {@link #toString()} value of an object or an empty String when the object is <code>null</code>.
     *
     * @param obj Object from which to get string value
     * @return given objects {@link #toString()} value or an empty string if <code>null</code>
     */
    public static String insertValue(Object obj)
    {
        return obj != null ? obj.toString() : EMPTY_STRING;
    }


    /**
     * Replace carriage-returns and line-feeds with spaces.
     *
     * @param text text to alter
     * @return altered text
     */
    public static String removeLinefeeds(String text)
    {
        return text != null ? text.replaceAll("[\n\r]", " ") : null;
    }


    /** Splitter used internally and returned by {@link #getListSplitter()} */
    private static final Splitter LIST_SPLITTER = Splitter.on(CharMatcher.anyOf(",;")).trimResults().omitEmptyStrings();

    /**
     * <p>Get a Splitter that will split comma and semi-colon separated lists of strings.
     * Results are trimmed and empty strings are omitted.</p>
     * <p>Note a Splitter returns an {@link java.lang.Iterable Iterable&lt;String>} not a String[], but
     * works just as well in an enhanced-for loop.</p>
     * <pre>for (String token : StringUtil.getListSplitter().split(inputString)) { ...</pre>
     * <p>Be aware that {@code splitter.split(somenullstring)} throws a NullPointerException.</p>
     *
     * @return a Splitter
     */
    public static Splitter getListSplitter()
    {
        return LIST_SPLITTER;
    }


    /**
     * Split the given string on comma or semi-colon into an array of trimmed Strings.
     * Empty strings are omitted.
     * <tt>null</tt> is accepted and results in a zero-length array being returned. <tt>null</tt> is never returned.
     * @param s a CharSequence to split
     * @return an array of non-empty, non-null Strings
     */
    public static String[] splitToArray(CharSequence s)
    {
        if (s == null) return new String[] {};
        return Iterators.toArray(LIST_SPLITTER.split(s).iterator(), String.class);
    }


    /**
     * Converts an object array to a String array.
     *
     * @param items item array to convert
     * @return the converted array
     */
    @SafeVarargs
    public static <F> String[] convert(F...items)
    {
        List<String> converted = new ArrayList<String>(items.length);

        for (F item : items)
        {
            converted.add(item.toString());
        }

        return converted.toArray(new String[converted.size()]);
    }


    /**
     * Parses the given value for the {@link BigDecimal} equivalent.
     * <br>TODO: <strong>How is this different from</strong> <code>return new BigDecimal(value);</code> ?
     * @param value text value to be parsed
     * @return the big decimal equivalent for the given value
     * @throws ParseException if parsing fails or is unable to parse the entire value
     */
    public static BigDecimal parseBigDecimal(String value) throws ParseException
    {
        // decimal format that parses to BigDecimal
        DecimalFormat format = new DecimalFormat();
        format.setParseBigDecimal(true);

        // maintains the index during parsing starting at zero
        ParsePosition position = new ParsePosition(0);

        BigDecimal parsed = (BigDecimal) format.parse(value, position);

        /*
         * If parsing failed or whole input not parsed
         */
        if (parsed == null || position.getIndex() < value.length())
        {
            throw new ParseException("Parsing value '" + value + "' failed", position.getIndex());
        }

        return parsed;
    }


    /**
     * Parse a String into a boolean
     *
     * @param s
     * @return treats "true" and "1" as true, anything else as false
     */
    public static boolean parseBoolean(String s)
    {
        return "true".equalsIgnoreCase(s) || "1".equals(s);
    }
}
