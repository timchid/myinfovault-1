/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: PermissionEvaluator.java
 */

package edu.ucdavis.mw.myinfovault.util.evaluator;

import java.text.ParseException;

import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.mw.myinfovault.service.person.MivRole;
import edu.ucdavis.myinfovault.MIVUser;

/**
 * Parses each literal given as an {@link MivRole}. The literal will evaluate
 * to <code>true</code> if the target person has the role. If the literal is
 * "flagged," the real, logged-in person's roles are tested instead.
 *
 * @author Craig Gilmore
 * @since MIV 4.6.2
 */
public class PermissionEvaluator implements LiteralEvaluator
{
    private final boolean realFlag;

    private final MivPerson realPerson;
    private final MivPerson targetPerson;

    /**
     * Create a permission evaluator for the given user.
     *
     * @param user MIV user whose permission is to be evaluated
     * @param realFlag if <code>true</code> real person is preferred regardless of the literal flag
     */
    public PermissionEvaluator(MIVUser user, boolean realFlag)
    {
        this.realFlag = realFlag;
        this.realPerson = user.getUserInfo().getPerson();
        this.targetPerson = user.getTargetUserInfo().getPerson();
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.myinfovault.parser.LiteralEvaluator#evaluate(java.lang.String)
     */
    @Override
    public boolean evaluate(String literal) throws ParseException
    {
        // evaluate with no flag
        return evaluate(literal, false);
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.util.evaluator.LiteralEvaluator#evaluate(java.lang.String, boolean)
     */
    @Override
    public boolean evaluate(String literal, boolean flagged) throws ParseException
    {
        // resolve role represented by the literal
        MivRole role = MivRole.valueOf(literal);

        /*
         * If flagged, use the real, logged-in person to evaluate
         * the literal. Use the target person otherwise.
         */
        return flagged || realFlag
             ? realPerson.hasRole(role)
             : targetPerson.hasRole(role);
    }
}
