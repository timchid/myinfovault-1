/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: IterableNodeList.java
 */

package edu.ucdavis.mw.myinfovault.util;

import java.util.Iterator;
import java.util.NoSuchElementException;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * Iterable wrapper for W3C node list.
 *
 * @author Craig Gilmore
 * @since 4.4.2
 */
public class IterableNodeList implements NodeList, Iterator<Node>, Iterable<Node>
{
    private NodeList nodeList;
    private int index = 0;

    /**
     * Create new iterable node list without altering original nodelist.
     *
     * @param nodeList Node list to wrap iterable functionality
     */
    public IterableNodeList(NodeList nodeList)
    {
        this.nodeList = nodeList;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Node item(int index)
    {
        return nodeList.item(index);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getLength()
    {
        return nodeList.getLength();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Iterator<Node> iterator()
    {
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean hasNext()
    {
        return index < getLength();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Node next() throws NoSuchElementException
    {
        if (hasNext())
        {
            return item(index++);
        }

        throw new NoSuchElementException();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void remove() throws UnsupportedOperationException, IllegalStateException
    {
        throw new UnsupportedOperationException();
    }
}
