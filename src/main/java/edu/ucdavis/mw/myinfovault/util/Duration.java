/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: Duration.java
 */

package edu.ucdavis.mw.myinfovault.util;

import java.util.concurrent.TimeUnit;

/**
 * Represent a duration in time in a chosen unit of measurement.<br>
 * Allows conversion to other time units and provides a convenience method
 * to Thread.sleep for the duration.
 *
 * @author Stephen Paulsen
 * @author adapted from code by "wangzijian" in the RetryerBuilder
 * @since MIV 4.6
 */
public class Duration
{
    private final long duration;
    private final TimeUnit timeUnit;


    /**
     * Create a new Duration in the specified time unit.
     * @param duration the length of time
     * @param timeUnit unit the length is measured in
     */
    public Duration(long duration, TimeUnit timeUnit)
    {
        this.duration = duration;
        this.timeUnit = timeUnit;
    }


    /**
     * Get the Duration elapsed between two times. Start and end time must be specified in milliseconds
     * but can be in either order. The absolute duration between the two times is used.
     * @param t1 first millisecond time
     * @param t2 second millisecond time
     * @return the elapsed time as a Duration in milliseconds
     */
    public static Duration elapsed(long t1, long t2)
    {
        return new Duration(Math.abs(t1 - t2), TimeUnit.MILLISECONDS);
    }


    /**
     * Get the Duration elapsed between two times. Start and end time must be specified in milliseconds
     * but can be in either order. The absolute duration between the two times is used.
     * @param t1 first millisecond time
     * @param t2 second millisecond time
     * @param timeUnit time unit to use for result
     * @return the elapsed time as a Duration in TimeUnits
     */
    public static Duration elapsedAs(long t1, long t2, TimeUnit timeUnit)
    {
        return new Duration(Duration.elapsed(t1, t2).as(timeUnit), timeUnit);
    }


    /**
     * Convert this duration into another unit of time.<br>
     * Allows constructs such as<br>
     * &nbsp; <code>  new Duration(5, TimeUnit.MINUTES).as(TimeUnit.SECONDS) </code>
     * @param unit the TimeUnit to convert to
     * @return duration expressed as number of given time unit
     */
    public long as(TimeUnit unit)
    {
        return unit.convert(this.duration, this.timeUnit);
    }


    /**
     * Performs a Thread.sleep using this duration.
     * This is a convenience method that converts time arguments into the form required by the Thread.sleep method.
     * @throws InterruptedException
     */
    public void sleep() throws InterruptedException
    {
        this.timeUnit.sleep(this.duration);
    }


    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        return new StringBuilder()
                    .append(duration)
                    .append(" ")
                    .append(timeUnit)
                    .toString();
    }


    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        // Always compute the hash based on Seconds so that 60-MINUTES and 1-HOURS hash to the same value.
        final long seconds = this.as(TimeUnit.SECONDS);
        result = prime * result + (int) (seconds ^ (seconds >>> 32));
        result = prime * result + ((timeUnit == null) ? 0 : TimeUnit.SECONDS.hashCode());
        return result;
    }


    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj)
    {
        if (this == obj) return true;
        if (obj == null) return false;
        if (!(obj instanceof Duration)) return false;
        Duration other = (Duration) obj;

        return this.as(TimeUnit.MILLISECONDS) == other.as(TimeUnit.MILLISECONDS);
    }


    /**
     * Test the .equals() and .hashCode() methods.
     * @param args 
     */
    public static void main(String[] args)
    {
        Duration
            oneTwenty = new Duration(120, TimeUnit.MINUTES),
            oneThirty = new Duration(130, TimeUnit.MINUTES),
            twoHour = new Duration(2, TimeUnit.HOURS);

        System.out.printf("130 minutes == 2 hours : %s\n", oneThirty.equals(twoHour));
        System.out.printf("120 minutes == 2 hours : %s\n", oneTwenty.equals(twoHour));
        System.out.printf("hashCodes  130=%s   120=%s   2=%s\n", oneThirty.hashCode(), oneTwenty.hashCode(), twoHour.hashCode());
        System.out.printf("120 minutes & 2 hours have equal hashCodes? : %s\n", oneTwenty.hashCode()==twoHour.hashCode());
    }
}
