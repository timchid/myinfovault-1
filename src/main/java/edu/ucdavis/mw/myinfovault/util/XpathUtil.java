/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: XpathUtil.java
 */

package edu.ucdavis.mw.myinfovault.util;

import java.util.ArrayList;
import java.util.List;

import javax.xml.namespace.QName;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;


/**
 * Provides XPath evaluation for obtaining lists of nodes or node values.
 *
 * @author Craig Gilmore
 * @since 4.4.2
 */
public class XpathUtil
{
    private static XPathFactory xpathFactory = XPathFactory.newInstance();

    /**
     * Evaluate the XPath search expression on the given node and return an iterable node list.
     *
     * @param expression XPath search expression to compile and evaluate
     * @param parent Node from which to begin evaluation
     * @return The list of nodes yielded from the expression
     * @throws XPathExpressionException If expression cannot be compiled or evaluated
     */
    public static IterableNodeList parseNodeList(final String expression, final Node parent) throws XPathExpressionException
    {
        return new IterableNodeList((NodeList) evaluate(expression, parent, XPathConstants.NODESET));
    }

    /**
     * Evaluate the XPath search expression on the given node and return a list of node values
     *
     * @param expression XPath search expression to compile and evaluate
     * @param parent Node from which to begin evaluation
     * @return The list of node values yielded from the expression
     * @throws XPathExpressionException If expression cannot be compiled or evaluated
     * @throws DOMException If one of the node value Strings is too large
     */
    public static List<String> parseNodeValueList(final String expression, final Node parent) throws XPathExpressionException, DOMException
    {
        List<String> nodeValues = new ArrayList<String>();

        //parse node value for each node from the iterator
        for (Node node : parseNodeList(expression, parent))
        {
            nodeValues.add(parseNodeValue(node));
        }

        return nodeValues;
    }

    /**
     * Evaluate the XPath search expression and return a single node.
     *
     * @param expression XPath search expression to compile and evaluate
     * @param parent Node from which to begin evaluation
     * @return The node yielded from the expression
     * @throws XPathExpressionException If expression cannot be compiled or evaluated
     */
    public static Node parseNode(final String expression, final Node parent) throws XPathExpressionException
    {
        return (Node) evaluate(expression, parent, XPathConstants.NODE);
    }

    /**
     * Evaluate the XPath search expression on the given node and return a singular value.
     *
     * @param expression XPath search expression to compile and evaluate
     * @param parent Node from which to begin evaluation
     * @return The value yielded from the expression or empty string if node was <code>null</code>
     * @throws XPathExpressionException If expression cannot be compiled or evaluated
     * @throws DOMException If node value String is too large
     */
    public static String parseNodeValue(final String expression, final Node parent) throws XPathExpressionException, DOMException
    {
        return parseNodeValue(parseNode(expression, parent));
    }

    /**
     * Gets the value of the given node.
     *
     * @param node Node to parse value
     * @return The value yielded from the node or empty string if node was <code>null</code>
     * @throws DOMException If node value String is too large
     */
    public static String parseNodeValue(final Node node) throws DOMException
    {
        return node != null ? node.getNodeValue().trim() : "";
    }

    /**
     * Graft the nodes from one document for a given path to another document's root.
     *
     * @param stock The primary, root document to receive the grafted document
     * @param scion The document to be grafted
     * @param unionPath Path to the scion's list of nodes to graft to the stock
     * @throws XPathExpressionException If union path cannot be compiled or evaluated
     * @throws DOMException If scion's node format is not supported
     */
    public static void graft(Document stock, Document scion, final String unionPath) throws XPathExpressionException, DOMException
    {
        //iterate over the node list parsed from the document to graft
        for (Node branch : parseNodeList(unionPath, scion))
        {
            //import each node branch into the merged document
            stock.importNode(branch, true);
        }
    }

    /**
     * Evaluate the XPath search expression on the given node.
     *
     * @param expression XPath search expression to compile and evaluate
     * @param node Node from which to begin evaluation
     * @param constant Maps to node type (may be a single node or node set)
     * @return An object representing the evaluated value
     * @throws XPathExpressionException If expression cannot be compiled or evaluated
     */
    private static Object evaluate(final String expression, Node node, final QName constant) throws XPathExpressionException
    {
        /*
         * Removing child node from its parent document increases XPath evaluation
         * performance as evaluation cost is proportional to the size the parent document
         */
        if (node.getParentNode() != null) node = node.getParentNode().removeChild(node);

        return xpathFactory.newXPath().compile(expression).evaluate(node, constant);
    }
}
