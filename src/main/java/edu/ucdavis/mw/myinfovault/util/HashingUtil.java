/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: HashingUtil.java
 */

package edu.ucdavis.mw.myinfovault.util;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.hash.HashCode;
import com.google.common.hash.HashCodes;
import com.google.common.hash.HashFunction;
import com.google.common.hash.Hashing;
import com.google.common.io.Files;

/**
 * Utility class to handle various hashing functions
 * @author rhendric
 *
 */
public class HashingUtil
{

    private static Logger logger = LoggerFactory.getLogger(HashingUtil.class);

    /**
     * Compute the hashcode of a file
     * @param inputFile - file on which to compute the hashcode
     * @param digest - hashing algorithm to use to compute the hashcode
     * @return HashCode - hashcode of the input file
     * @throws IOException
     */
    public static HashCode computeHash(File inputFile, Digest digest) throws IOException
    {
        try
        {
            return Files.hash(inputFile, digest.hashFuncton);
        }
        catch (IOException ioe)
        {
            throw new IOException("Unable to compute "+digest.toString()+" hash on input file: "+ioe.getLocalizedMessage());
        }
    }

    /**
     * Compute the hashcode of a list of files
     * @param inputFiles - files on which to compute the hashcode
     * @param digestType - hashing algorithm to use to compute the hashcode
     * @return HashCode - combined hashcode of the input file list
     * @throws IOException
     */
    public static HashCode computeHash(List<File> inputFiles, String digestType) throws IOException
    {
        return computeHash(inputFiles, Digest.getDigest(digestType));
    }

    /**
     * Compute the hashcode of a list of files
     * @param inputFiles - files on which to compute the hashcode
     * @param digest - hashing algorithm to use to compute the hashcode
     * @return HashCode - combined hashcode of the input file list
     * @throws IOException
     */
    public static HashCode computeHash(List<File> inputFiles, Digest digest) throws IOException
    {

        List<HashCode>hashCodes = new ArrayList<HashCode>();
        for (File file : inputFiles)
        {
                hashCodes.add(computeHash(file, digest));
                continue;
        }

        try
        {
            return Hashing.combineUnordered(hashCodes);
        }
        catch (IllegalArgumentException iae)
        {
                throw new IOException("Unable to compute "+digest.toString()+" hash on input files: "+iae.getLocalizedMessage());
        }
    }

    /**
     * Compute the hashcode of a byte array
     * @param inputBytes - bytes on which to compute the hashcode
     * @param digestType - hashing algorithm to use to compute the hashcode
     * @return HashCode - hashcode of the input bytes
     */
    public static HashCode computeHash(byte [] inputBytes, String digestType)
    {
        return Digest.getDigest(digestType).getHashFunction().hashBytes(inputBytes);
    }

    /**
     * Compute the hashcode of a byte array
     * @param inputBytes - bytes on which to compute the hashcode
     * @param digestType - hashing algorithm to use to compute the hashcode
     * @return byte[] - hashcode of the input bytes as bytes
     */
    public static byte[] computeHashAsBytes(byte [] inputBytes, String digestType)
    {
        return computeHash(inputBytes, digestType).asBytes();
    }

    /**
     * Convert a byte array containing a hash into a HashCode
     * @param inputBytes - bytes to covert
     * @return HashCode - hashcode of the input bytes
     */
    public static HashCode bytesToHashCode(byte [] inputBytes)
    {
        return HashCodes.fromBytes(inputBytes);
    }

    /**
     * Validate the hashcode of a file
     * @param inputFile - file on which to validate the hashcode
     * @param inputHashCode - hashcode to validate
     * @param digest - digest to use for validation
     * @return boolean - true if hashcode is valid, otherwise false
     */
    public static Boolean validateHash (File inputFile, HashCode inputHashCode, Digest digest)
    {
        try
        {
            HashCode hashcode = computeHash(inputFile, digest);
            if (hashcode.equals(inputHashCode))
            {
                logger.debug("Validated "+digest.toString()+" hashcode for "+inputFile+" ---> "+hashcode.toString()+" == "+inputHashCode.toString());
                return true;
            }
            logger.info(digest.toString()+" hashcode for "+inputFile+ " failed validation ---> "+hashcode.toString()+" != "+inputHashCode.toString());

        }
        catch (IOException ioe)
        {
             logger.error("Unable to validate "+digest.toString()+" hashcode for files - "+inputFile, ioe);
        }
        return false;
    }

    /**
     * Validate the hashcode of a list of files
     * @param inputFiles - files on which to validate the hashcode
     * @param inputHashCode - hashcode to validate
     * @param digest - digest to use for validation
     * @return boolean - true if hashcode is valid, otherwise false
     */
    public static Boolean validateHash (List<File> inputFiles, HashCode inputHashCode, Digest digest)
    {
        try
        {
            HashCode hashcode = computeHash(inputFiles, digest);
            if (hashcode.equals(inputHashCode))
            {
                logger.debug("Validated "+digest.toString()+" hashcode for "+inputFiles+" ---> "+hashcode.toString()+" == "+inputHashCode.toString());
                return true;
            }
            logger.info(digest.toString()+" hashcode for "+inputFiles+ " failed validation ---> "+hashcode.toString()+" != "+inputHashCode.toString());

        }
        catch (IOException ioe)
        {
             logger.error("Unable to validate "+digest.toString()+" hashcode for files - "+inputFiles, ioe);
        }
        return false;
    }

    /**
     * This enum represents the available hash types or algorithms available
     */
    public enum Digest {

        SHA256(Hashing.sha256(), "SHA-256"),
        SHA512(Hashing.sha512(), "SHA-512");

        private HashFunction hashFuncton;
        private String digestType;

        Digest (HashFunction hashFunction, String digestType)
        {
            this.hashFuncton = hashFunction;
            this.digestType = digestType;
        }

        public HashFunction getHashFunction()
        {
            return hashFuncton;
        }

        public String getDigestType()
        {
            return digestType;
        }

        public static Digest getDigest(String digestType)
        {
            return digestNameToDigestMap.get(digestType);
        }

        private static final Map<String,Digest> digestNameToDigestMap = new HashMap<String,Digest>();
        static {
            digestNameToDigestMap.put("SHA-256", SHA256);
            digestNameToDigestMap.put("SHA-512", SHA512);
        }
      }

}
