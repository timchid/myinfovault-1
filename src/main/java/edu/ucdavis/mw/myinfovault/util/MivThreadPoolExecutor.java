/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: MivThreadPoolExecutor.java
 */
package edu.ucdavis.mw.myinfovault.util;

import java.util.Properties;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import edu.ucdavis.myinfovault.MIVConfig;

/**
 * ThreadPoolExecutor wrapper class
 * Creates a ThreadPoolExecutor which uses an ArrayBlockingQueue configured for FIFO processing
 * Queue configuration (pool size, capacity, etc.) are retrieved from  mivconfig.properties based
 * on the threadPoolType specified
 *  
 * @author rhendric
 *
 */
public class MivThreadPoolExecutor
{
    private static ThreadPoolExecutor threadPoolExecutor = null;
    private String threadPoolType = null;
    
    /**
     * Construct a ThreadPoolExecutor of the specified threadPoolType 
     * @param threadPoolType
     */
    private MivThreadPoolExecutor(String threadPoolType)
    {
        this.threadPoolType = threadPoolType;
        initThreadPoolExecutor();

// Using MivContextLoader to detect shutdown. Shutdown hook does not work because
// by the time the hook is executed, the ThreadPoolExecutor class has already been
// unloaded        
//        // ShutdownHook for orderly shutdown of the ThreadPoolExecutor
//        Runtime.getRuntime().addShutdownHook(new Thread() {
//            public void run() {
//                shutDown();
//            }
//        });
    }
    
    /**
     * Get an instance of the MivThreadPoolExecutor
     * @param threadPoolType
     * @return
     */
    public static MivThreadPoolExecutor getInstance(String threadPoolType)
    {
        return new MivThreadPoolExecutor(threadPoolType);
    }
    
    
    /**
     * Initialize the ThreadPoolExecutor configuration with properties from the mivconfig.properties file
     * The properties are prepended with the threadPoolType type of ThreadPoolExecutor being created. If no
     * properties are found, the defaults will be used. 
     * 
     */
    private void initThreadPoolExecutor()
    {
        // Create the threadPoolExecutor with the config parameters
        Properties configProperties = MIVConfig.getConfig().getProperties();
        int corePoolSize = Integer.parseInt(configProperties.getProperty(this.threadPoolType+"-threadpool-corePoolSize", "1"));
        int maxPoolSize = Integer.parseInt(configProperties.getProperty(this.threadPoolType+"-threadpool-maxPoolSize", "3"));
        long keepAliveTime = Long.parseLong(configProperties.getProperty(this.threadPoolType+"-threadpool-keepAliveTime", "5"));
        int queueCapacity = Integer.parseInt(configProperties.getProperty(this.threadPoolType+"-threadpool-queueCapacity", "5"));
       
        threadPoolExecutor = new ThreadPoolExecutor(corePoolSize, maxPoolSize, keepAliveTime, TimeUnit.SECONDS, new ArrayBlockingQueue<Runnable>(queueCapacity, true));
        System.out.println(this.threadPoolType.toUpperCase()+" ThreadPoolExecutor Initialization complete.");
        System.out.println(this.getThreadpoolStats());
    }
    
    /**
     * Get the ThreadPoolExecutor.
     * @returns ThreadPoolExecutor 
     */
    public ThreadPoolExecutor getExecutor()
    {
        if (threadPoolExecutor == null)
        {
            this.initThreadPoolExecutor();
        }
        return threadPoolExecutor;
    }
    
    /**
     * Do an orderly shutdown of the ThreadPoolExecutor. 
     */
    public final void shutDown()
    {
        System.out.println(this.threadPoolType.toUpperCase()+" ThreadPoolExecutor shutdown requested...");

        if (threadPoolExecutor != null)
        {
            System.out.println("----> "+threadPoolExecutor.getQueue().size()+" items in the "+this.threadPoolType.toUpperCase()+" queue with "+threadPoolExecutor.getActiveCount()+" items active at shutdown.");
            threadPoolExecutor.shutdown();
            try
            {
                System.out.println("----> Waiting up to 10 seconds to process any remaining entries...");
                System.out.println( 
                        threadPoolExecutor.awaitTermination(10, TimeUnit.SECONDS) 
                        ? this.threadPoolType.toUpperCase()+" ThreadPoolExecutor shutdown complete...All entries processed." 
                                :  this.threadPoolType.toUpperCase()+" ThreadPoolExecutor shutdown complete...*** "+threadPoolExecutor.getQueue().size()+" entries were skipped. ***"
                        );
            }
            catch (InterruptedException e)
            {
                System.out.println("ThreadPoolExecutor shutdown interruped with "+threadPoolExecutor.getQueue().size()+" entries to process...retrying");
                e.printStackTrace();
            }
        }
    }

    /**
     * Reinitialize the ThreadPoolExecutor. 
     */
    public void reinitialize()
    {
        System.out.println(this.threadPoolType.toUpperCase()+" ThreadPoolExecutor reinitialize...");
        this.shutDown();
        this.initThreadPoolExecutor();
    }
    
    /**
     * Gets the current thread pool statistics for this TheadPoolExecutor
     * @return String
     */
    public String getThreadpoolStats()
    {
        return String.format("["+threadPoolType.toUpperCase()+" Queue Monitor] Current: %d, Core: %d, Max: %d,  Active: %d, Backlog: %d, Completed: %d, TotalTask: %d, isShutdown: %s, isTerminated: %s",
                        threadPoolExecutor.getPoolSize(),
                        threadPoolExecutor.getCorePoolSize(),
                        threadPoolExecutor.getMaximumPoolSize(),
                        threadPoolExecutor.getActiveCount(),
                        threadPoolExecutor.getQueue().size(),
                        threadPoolExecutor.getCompletedTaskCount(),
                        threadPoolExecutor.getTaskCount(),
                        threadPoolExecutor.isShutdown(),
                        threadPoolExecutor.isTerminated());
    }

}
