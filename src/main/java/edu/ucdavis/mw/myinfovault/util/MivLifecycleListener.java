/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: MivLifecycleListener
 */

package edu.ucdavis.mw.myinfovault.util;

import org.apache.catalina.LifecycleEvent;
import org.apache.catalina.LifecycleListener;

public class MivLifecycleListener  implements LifecycleListener
{

    @Override
    public void lifecycleEvent(LifecycleEvent lcEvent)
    {
        System.out.println("Life Cycle Event ****** ------->"+lcEvent.getType());
    }       

}
