/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: AcademicActionService.java
 */

package edu.ucdavis.mw.myinfovault.service.academicaction;

import edu.ucdavis.mw.myinfovault.domain.action.AcademicAction;
import edu.ucdavis.mw.myinfovault.domain.dossier.Dossier;
import edu.ucdavis.mw.myinfovault.domain.raf.AcademicActionBo;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;

/**
 * Service for the academic action form.
 *
 * @author Rick Hendricks
 * @since MIV 4.8
 */
public interface AcademicActionService
{
    /**
     * Gets AA business object from DAO.
     * If no AA data exists, a blank default business object is created and returned.
     *
     * @param dossier object
     * @return Academic action form business object
     */
    public AcademicActionBo getBo(Dossier dossier);

    /**
     * Update AA decisions, the dossier, and disclosure certificate with AA changes,
     * save to the database and re-create PDFs.
     *
     * @param aa academic action 
     *      * @param targetPerson Target, switched-to user
     *      * @param realPerson Real, logged-in user performing update
     * @return former academic action
     */
    public AcademicAction saveAcademicAction(AcademicActionBo aa, MivPerson targetPerson, MivPerson realPerson);

    /**
     * Creates the PDF files for the AA.
     *
     * @param aa Academic action
     */
    public void createPdfs(AcademicActionBo aaf);
}
