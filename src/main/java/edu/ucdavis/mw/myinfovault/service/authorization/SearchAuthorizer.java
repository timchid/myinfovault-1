/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: ViewSnapshotAuthorizer.java
 */

package edu.ucdavis.mw.myinfovault.service.authorization;

import static edu.ucdavis.mw.myinfovault.service.person.MivRole.DEAN;
import static edu.ucdavis.mw.myinfovault.service.person.MivRole.DEPT_ASSISTANT;
import static edu.ucdavis.mw.myinfovault.service.person.MivRole.DEPT_CHAIR;
import static edu.ucdavis.mw.myinfovault.service.person.MivRole.DEPT_STAFF;
import static edu.ucdavis.mw.myinfovault.service.person.MivRole.SCHOOL_STAFF;
import static edu.ucdavis.mw.myinfovault.service.person.MivRole.SENATE_STAFF;
import static edu.ucdavis.mw.myinfovault.service.person.MivRole.VICE_PROVOST;
import static edu.ucdavis.mw.myinfovault.service.person.MivRole.VICE_PROVOST_STAFF;

import org.apache.log4j.Logger;
import edu.ucdavis.mw.myinfovault.util.AttributeSet;

import edu.ucdavis.mw.myinfovault.service.person.MivPerson;


/**<p>
 * Authorizes searching of users for the purpose of prducing a list.</p>
 * All admin roles including the DEAN and DEPT_CHAIR roles may search for any user
 * for the purpose of confirming their MIV account status. There is no qualification
 * necessary.
 *
 * @author rhendric
 * @since MIV 4.0.0
 */
public class SearchAuthorizer extends SameScopeAuthorizer implements PermissionAuthorizer
{
    Logger log = Logger.getLogger(this.getClass());

    /**
     * General permission check to determine if the actor has the permission to view the candidate archive.
     *
     * @see SameScopeAuthorizer#hasPermission(MivPerson, String, AttributeSet)
     */
    @Override
    public boolean hasPermission(MivPerson person, String permissionName, AttributeSet permissionDetails)
    {
        return person.hasRole(DEPT_STAFF, DEPT_CHAIR, DEPT_ASSISTANT, SCHOOL_STAFF, DEAN, SENATE_STAFF, VICE_PROVOST, VICE_PROVOST_STAFF);
    }

    /**
     *
     * @see SameScopeAuthorizer#isAuthorized(MivPerson, String, AttributeSet, AttributeSet)
     */
    @Override
    public boolean isAuthorized(MivPerson person, String permissionName, AttributeSet permissionDetails, AttributeSet qualification)
    {

        return  this.hasPermission(person, permissionName, permissionDetails);
    }

}
