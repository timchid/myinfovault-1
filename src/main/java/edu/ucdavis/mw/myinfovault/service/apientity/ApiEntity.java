/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: ApiEntity.java
 */
package edu.ucdavis.mw.myinfovault.service.apientity;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import edu.ucdavis.mw.myinfovault.service.person.MivRole;

/**
 * Interface which API entities must implement.
 *
 * @author japorito
 * @since 5.0
 */
public interface ApiEntity
{
    /**
     * Get the scopes relevant to the resource which is passed in as an argument.
     *
     * @param resource
     * @return The scopes for which the entity is allowed to act on this resource.
     */
    public List<ApiScope> getScopes(ApiResourceType resource);

    /**
     * Get all scopes the entity is allowed to act on.
     *
     * @return A collection of scopes.
     */
    public Collection<ApiScope> getAllScopes();

    /**
     * Sets the scopes on which this entity is allowed to act.
     *
     * @param scopes A map of resource types to the scopes on which this entity is allowed to act on the resource.
     */
    public void setScopes(Map<ApiResourceType, List<ApiScope>> scopes);

    /**
     * Gets the name of the entity.
     *
     * @return The name of this entity.
     */
    public String getEntityName();

    /**
     * Sets the name of the entity.
     *
     * @param entityName The name of the entity.
     */
    public void setEntityName(String entityName);

    /**
     * Gets the database id of the entity or -1, if the entity does not exist in the database.
     *
     * @return The ID of the entity.
     */
    public int getEntityId();

    /**
     * Sets the entity id of the entity.
     *
     * @param entityId
     */
    public void setEntityId(int entityId);

    /**
     * Gets the hash to check authentication against.
     *
     * @return The secret hash.
     */
    public String getKeyHash();

    /**
     * Set the key hash to authenticate again.
     *
     * @param keyHash
     */
    public void setKeyHash(String keyHash);

    /**
     * Gets the salt which, combined with the user's secret and hashed, returns the secret hash.
     *
     * @return The salt used to get the secret hash.
     */
    public String getSalt();

    /**
     * Sets the Salt for the entity.
     *
     * @param Salt
     */
    public void setSalt(String Salt);

    /**
     * Gets the security scheme that this entity uses to authenticate.
     *
     * @return The security scheme that the entity uses.
     */
    public ApiSecurityScheme getSecurityScheme();

    /**
     * Gets the MivRole that the API entity acts as.
     *
     * @return The MivRole of the API entity.
     */
    public MivRole getRole();
}
