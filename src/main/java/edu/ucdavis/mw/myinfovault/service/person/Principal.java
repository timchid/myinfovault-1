/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: Principal.java
 */

package edu.ucdavis.mw.myinfovault.service.person;

import edu.ucdavis.mw.myinfovault.util.StringUtil;

/**
 * Replacement for KimPrincipal if we dump KIM.
 * This is basically just the person's login name.
 * @author Stephen Paulsen
 * @since MIV 4.6
 */
public interface Principal
{
    public String getPrincipalName();
    public String getPrincipalId();

    public static final Principal EMPTY_PRINCIPAL = new Principal() {
        @Override
        public String getPrincipalName()
        {
            return StringUtil.EMPTY_STRING;
        }

        @Override
        public String getPrincipalId()
        {
            return StringUtil.EMPTY_STRING;
        }
    };
}
