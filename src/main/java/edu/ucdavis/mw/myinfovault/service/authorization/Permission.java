/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: Permission.java
 */


package edu.ucdavis.mw.myinfovault.service.authorization;

/**
 * Permission-name string constants. Don't re-type the string; prevent mis-typing the string.
 *
 * @see AuthorizationService.Qualifier Qualifier constants for common permission qualifier values.
 * @see AuthorizationService.PermissionDetail PermissionDetail constants for common permission detail values.
 *
 * @author Stephen Paulsen
 * @since MIV 3.5.2
 */
public interface Permission
{
    // General Actions
    public static final String SWITCH_USER = "Switch User";

    // General Searching
    public static final String SEARCH = "Search";

    // Managing Users
    public static final String ADD_USER = "Add User";
    public static final String EDIT_USER = "Edit User";
    public static final String ACTIVATE_USER = "Activate User";

    public static final String ASSIGN_DEAN = "Assign Dean";
    public static final String ASSIGN_DEPT_CHAIR = "Assign Dept Chair";
    public static final String MANAGE_EXECUTIVES = "Manage Executives";

    // Managing Groups
    public static final String EDIT_GROUP = "Edit Group";
    public static final String VIEW_GROUP = "View Group";
    public static final String VIEW_MEMBERSHIP = "View Membership";

    // Managing Open Actions
    public static final String VIEW_DOCUMENT = "View Document";
    public static final String VIEW_DOSSIER = "View Dossier";
    public static final String EDIT_DOSSIER = "Edit Dossier";
    public static final String ROUTE_DOSSIER = "Route Dossier";
    public static final String EDIT_DISCLOSURE = "Edit Disclosure";
    public static final String UPLOAD_DECISION = "Upload Decision";
    public static final String INITIATE_APPEAL = "Initiate Appeal";
    public static final String VIEW_POST_AUDIT_DOSSIERS = "View Post Audit Dossiers";
    public static final String CANCEL_DOSSIER = "Cancel Dossier";

    // Signing Documents
    public static final String VIEW_DECISION = "View Decision";
    public static final String SIGN_DECISION = "Sign Decision";
    public static final String SIGN_DISCLOSURE = "Sign Disclosure";

    // Reviewing
    public static final String ASSIGN_REVIEWERS = "Assign Reviewers";
    public static final String REVIEW_DOSSIER = "Review Dossier";

    // Snapshots and Archives
    public static final String ARCHIVE_DOSSIER = "Archive Dossier";
    public static final String VIEW_SNAPSHOTS = "View Snapshots";
    public static final String VIEW_CANDIDATE_ARCHIVES = "View Candidate Archives";
    public static final String VIEW_FULL_ARCHIVES = "View Full Archives";
    public static final String VIEW_ADMIN_ARCHIVES = "View Admin Archives";

    // View event logs
    public static final String VIEW_EVENTLOG = "View Event Log";
    public static final String VIEW_SIGNATURELOG = "View Signature Log";

    // VetMed Import
    public static final String IMPORT_VETMED = "Import VetMed";

    // Packet Request
    public static final String PACKET_REQUEST = "Packet Request";

}
