/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: SignatureStatus.java
 */

package edu.ucdavis.mw.myinfovault.service.signature;


/**
 * Represents the status of a signature for a signable document.
 *
 * @author Craig Gilmore
 * @since MIV 4.6.3
 */
public enum SignatureStatus
{
    /**
     * No signature or signature requests exists for the document.
     */
    MISSING,

    /**
     * A signature request exists for the document.
     */
    REQUESTED,

    /**
     * The document is signed and the signature is valid.
     */
    SIGNED,

    /**
     * The document is signed and the signature is NOT valid.
     */
    INVALID;
}
