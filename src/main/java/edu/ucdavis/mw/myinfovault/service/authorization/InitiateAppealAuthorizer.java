/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: UploadDecisionAuthorizer.java
 */

package edu.ucdavis.mw.myinfovault.service.authorization;

import edu.ucdavis.mw.myinfovault.util.AttributeSet;

import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.mw.myinfovault.service.person.MivRole;

/**<p>
 * Checks that the actor is authroized to upload the Appeal Request document based on
 * the qualification scope of the actor's appointments and/or role.</p>
 *
 * @author rhendric
 * @since MIV 4.7.0
 */
public class InitiateAppealAuthorizer extends SameScopeAuthorizer implements PermissionAuthorizer
{

    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.authorization.SameScopeAuthorizer#hasPermission(edu.ucdavis.mw.myinfovault.service.person.MivPerson, java.lang.String, edu.ucdavis.mw.myinfovault.util.AttributeSet)
     * General permission check to determine if the input person has the permission to upload an Appeal Request document.
     */
    @Override
    public boolean hasPermission(MivPerson person, String permissionName, AttributeSet permissionDetails)
    {
        // Only VICE_PROVOST_STAFF, DEAN and SCHOOL_STAFF may upload an Appeal Request Document
        return person.hasRole(MivRole.SYS_ADMIN, MivRole.VICE_PROVOST_STAFF,MivRole.SCHOOL_STAFF, MivRole.DEAN);
    }

    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.authorization.SameScopeAuthorizer#isAuthorized(edu.ucdavis.mw.myinfovault.service.person.MivPerson, java.lang.String, edu.ucdavis.mw.myinfovault.util.AttributeSet, edu.ucdavis.mw.myinfovault.util.AttributeSet)
     * <p>Determine if a person is qualified to upload an Appeal Request document.
     * A vice provost staff member may upload an Appeal Request document for any school and department. A school administrator
     * may upload an Appeal Request document within the same school (any department is acceptable) as any of their appointments.</p>
     */
    @Override
    public boolean isAuthorized(MivPerson person, String permissionName, AttributeSet permissionDetails, AttributeSet qualification)
    {

        // If person has permission to upload the Appeal Request, check if authorized based on department and school of the
        // user
        if (this.hasPermission(person, permissionName, permissionDetails))
        {
                // Vice provost role can upload Appeal Request
                if (person.hasRole(MivRole.SYS_ADMIN, MivRole.VICE_PROVOST_STAFF))
                {
                    return true;
                }

                // School staff/Dean can upload Appeal Request for their qualified school.
                if (person.hasRole(MivRole.SCHOOL_STAFF, MivRole.DEAN) && hasSharedScope(person, qualification))
                {
                    return true;
                }
        }
        return false;
    }

}
