/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: DocumentTypeFilter.java
 */

package edu.ucdavis.mw.myinfovault.service.person;

import edu.ucdavis.mw.myinfovault.service.SearchFilterAdapter;

/**
 * Filters out MIV persons by role and scope.
 *
 * @author Craig Gilmore
 * @since 4.3
 */
public class RoleScopeFilter extends SearchFilterAdapter<MivPerson>
{
    private final MivRole role;
    private final Scope scope;

    /**
     * Filtering by role and school (any department).
     *
     * @param role MIV role that a person must have
     * @param schoolId ID of the school that a person must have
     */
    public RoleScopeFilter(MivRole role, int schoolId)
    {
        this(role, schoolId, Scope.ANY);
    }

    /**
     * Filtering by role and department.
     *
     * @param role MIV role that a person must have
     * @param schoolId ID of the school that a person must have
     * @param departmentId ID of the department that a person must have
     */
    public RoleScopeFilter(MivRole role, int schoolId, int departmentId)
    {
        this(role, new Scope(schoolId, departmentId));
    }

    /**
     * Filtering by role (any scope).
     *
     * @param role MIV role that a person must have
     */
    public RoleScopeFilter(MivRole role)
    {
        this(role, Scope.AnyScope);
    }

    /**
     * Filtering by role and scope.
     *
     * @param role MIV role that a person must have
     * @param scope scope that a person must have
     */
    public RoleScopeFilter(MivRole role, Scope scope)
    {
        this.role = role;
        this.scope = scope;
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.SearchFilter#include(java.lang.Object)
     */
    @Override
    public boolean include(MivPerson item)
    {
        // examine the given person's assigned roles for matching role and scope
        for (AssignedRole assignedRole : item.getAssignedRoles())
        {
            if (assignedRole.getRole() == role
             && assignedRole.getScope().matches(scope))
            {
                return true;// found match
            }
        }

        return false;// match not found
    }
}
