/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: Scope.java
 */


package edu.ucdavis.mw.myinfovault.service.person;

import java.util.Map;

import edu.ucdavis.mw.myinfovault.util.AttributeSet;

import edu.ucdavis.mw.myinfovault.service.search.MivSearchKeys;
import edu.ucdavis.mw.myinfovault.util.SimpleAttributeSet;

/**
 * Represent the Scope of an entity.
 * This can apply to any of several things, for example the Scope of an Appointment,
 * the Scope of a Role (AssignedRole), the Scope of a Reviewer.
 * Scope is intended to indicate School and Department, but could be expanded.
 * <br><strong>This "Scope" class should be in some other package.</strong>
 */
public class Scope extends SimpleAttributeSet
{
    private static final long serialVersionUID = 201107081625L;
    /** Flag to prevent changes after construction. */
    private boolean immutable = false;

    /** Attribute key used for the School */
    private static final String SCHOOL = MivSearchKeys.Person.MIV_SCHOOL_CODE;
    /** Attribute key used for the Department */
    private static final String DEPARTMENT = MivSearchKeys.Person.MIV_DEPARTMENT_CODE;

    /** Use as a wildcard; match any value for the attribute to which it is assigned. */
    public static final int ANY = -1;
    /** Use to indicate nothing assigned. Will not match any valid school or department. (Also used as a mask in EditAction.java) */
    public static final int NONE = 0;

    /** The school that this Scope represents */
    private int school = Scope.ANY;
    /** The department that this Scope represents */
    private int department = Scope.ANY;

    /** Convenience object that will match any Scope */
    public static final Scope AnyScope = new Scope(Scope.ANY, Scope.ANY);
    /** Convenience objects that will not match any other legitimate Scope */
    public static final Scope NoScope = new Scope(Scope.NONE, Scope.NONE), None = NoScope;

    /** String returned from {@link #getAttributes()} - built once as soon as the Scope is constructed */
    private final String attributeString;

    /** String returned from {@link #toString()} - built once as soon as the Scope is constructed */
    private final String scopeString;

    /**
     * Create a Scope for the given School and ANY department.
     * @param schoolId
     */
    public Scope(int schoolId)
    {
        this(schoolId, ANY);
    }


    /**
     * Create a Scope for the given School and Department.
     * The values may be set to {@link Scope#ANY} to act as wildcard matches.
     * @param schoolId
     * @param departmentId
     */
    public Scope(int schoolId, int departmentId)
    {
        // school is ANY if not greater than zero
        this.school = schoolId < 0 ? ANY : schoolId;

        // department is equal to school if school is not greater than NONE.
        this.department = this.school > NONE ? departmentId : this.school;

        // add school and department values to the attribute set
        this.put(SCHOOL, Integer.toString(school));
        this.put(DEPARTMENT, Integer.toString(department));
        this.immutable = true;

        this.attributeString = SCHOOL + "=" + this.school + ", " + DEPARTMENT + "=" + this.department;
        this.scopeString = (this.school == ANY ? "ANY" : this.school) + ":" + (this.department == ANY ? "ANY" : this.department);
    }


    /**
     * Create a school-department scope for the given scope pair (e.g., "14:38" or "14:ANY).
     *
     * @param scopePair school and department IDs delimited by a colon
     */
    public Scope(String scopePair)
    {
        this(scopePair.split(":"));
    }


    /**
     * Create a Scope from an existing Map or AttributeSet (including a {@link SimpleAttributeSet})
     * The existing set <em>MUST</em> contain an attribute for at least
     * {@link edu.ucdavis.mw.myinfovault.service.search.MivSearchKeys.Person#MIV_SCHOOL_CODE}
     * @param scope an existing AttributeSet containing a numeric school code and numeric department code.
     */
    public Scope(Map<String,String> scope)
    {
        this(scope.get(SCHOOL), scope.get(DEPARTMENT));
    }

    /**
     * Create the scope for the given array of school and department ID values.
     *
     * @param scopePair school ID and department ID
     */
    private Scope(String...scopePair)
    {
        /*
         * Parse scope pair array elements if exist, not null, and not empty.
         * Otherwise consider the element "ANY".
         */
        this(scopePair.length > 0 && scopePair[0] != null && !scopePair[0].trim().isEmpty() ? Integer.parseInt(scopePair[0]) : ANY,
             scopePair.length > 1 && scopePair[1] != null && !scopePair[1].trim().isEmpty() ? Integer.parseInt(scopePair[1]) : ANY);
    }


    public int getSchool()
    {
        return this.school;
    }


    public int getDepartment()
    {
        return this.department;
    }


    /**<p>
     * Test if this scope matches the given scope, taking wildcards into account.</p>
     * <p>For example, <code>{ school=3, dept=ANY }</code> matches <code>{ school=3, dept=115 }</code></p>
     * @param testScope Scope to check if the current scope matches
     * @return <code>true</code> if this scope matches the given test scope, <code>false</code> otherwise
     */
    public boolean matches(Scope testScope)
    {
        if (testScope != null)
        {
            if (this.equals(testScope)) return true;

            if (this.school == ANY || testScope.getSchool() == ANY) return true;

            if (this.school == testScope.getSchool())
            {
                if (this.department == ANY || testScope.getDepartment() == ANY) return true;
                if (this.department == testScope.getDepartment()) return true;
            }
        }

        return false;
    }


    /**
     * Test if this scope matches the given scope, supplied as an attribute set.
     *
     * @param testScope Attribute set to check if the current scope matches
     * @return <code>true</code> if this scope matches the given attribute set, <code>false</code> otherwise
     * @see #matches(Scope)
     */
    public boolean matches(AttributeSet testScope)
    {
        return this.matches(new Scope(testScope));
    }


    /**
     * Test if this scope matches the given scope, supplied as a String.
     * The testScope <em>better</em> be in the correct form or results are unpredictable.
     *
     * @param testScope Scope string to check if the current scope matches
     * @return <code>true</code> if this scope matches the given test scope string, <code>false</code> otherwise
     * @see #matches(Scope)
     */
    public boolean matches(String testScope)
    {
        return this.matches(new Scope(new SimpleAttributeSet(testScope)));
    }


    /**
     * Unlike <em>{@link #matches(Scope)}</em>, this allows wildcards only in the "left" Scope,
     * so <code>{ school=3, dept=ANY } .contains( { school=3, dept=115 }</code> is <em>true</em>
     * while <code>{ school=3, dept=115 } .contains( { school=3, dept=ANY }</code> is <em>false</em>.
     *
     * @param testScope Scope to check if the current scope contains
     * @return <code>true</code> if this scope contains the given test scope, <code>false</code> otherwise
     */
    public boolean contains(Scope testScope)
    {
        throw new UnsupportedOperationException("Not Implemented Yet!");
        //return false;
    }

    /**
     * Using the analogy of a bitwise OR, the given scope is a mask for this scope's school and department attributes.
     * <p>
     * E.g. The IET - Application Development masked for a {@link MivRole#DEAN}:
     * <pre>
     *     [  38, 313] <-- this scope (AppDev)
     *  OR [NONE, ANY] <-- mask (Dean)
     *  --------------
     *     [  38, ANY] <-- IET school, any department
     * </pre>
     * </p>
     * @param mask scope mask
     * @return masked scope
     * @see MivRole#getScope()
     */
    public Scope or(Scope mask)
    {
        return new Scope(mask.school == ANY ? ANY : school,
                         mask.department == ANY ? ANY : department);
    }


    /*
     * The java.util.Map interface says these may throw an UnsupportedOperationException
     * We don't want to allow any changes to this Scope after it's been constructed,
     * so we disallow all these methods.
     * put() and putAll() are needed during construction, the others are never needed so
     * always throw the exception.
     */
    /** This {@link java.util.Map#put(Object, Object) optional operation} is not supported and throws an {@link UnsupportedOperationException} */
    @Override public String put(String key, String value) { if (immutable) throw new UnsupportedOperationException(); else return super.put(key, value); }
    /** This {@link java.util.Map#putAll(Map) optional operation} is not supported and throws an {@link UnsupportedOperationException} */
    @Override public void putAll(java.util.Map<? extends String,? extends String> m) { if (immutable) throw new UnsupportedOperationException(); else super.putAll(m); }
    /** This {@link java.util.Map#clear() optional operation} is not supported and throws an {@link UnsupportedOperationException} */
    @Override public void clear() { throw new UnsupportedOperationException(); }
    /** This {@link java.util.Map#remove(Object) optional operation} is not supported and throws an {@link UnsupportedOperationException} */
    @Override public String remove(Object key) { throw new UnsupportedOperationException(); }


    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + department;
        result = prime * result + school;
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj)
    {
        if (this == obj) { return true; }
        if (!super.equals(obj)) { return false; }
//        if (!(obj instanceof Scope)) { return false; }  // Doing an "isAssignableFrom" instead of an "instanceof"
        // What order does the comparison need to be?
//        if (obj.getClass().isAssignableFrom(this.getClass())) { System.out.println("obj ["+obj.getClass().getSimpleName()+"] is assignable from Scope"); }
//        if (this.getClass().isAssignableFrom(obj.getClass())) { System.out.println("Scope is assignable from obj ["+obj.getClass().getSimpleName()+"]"); }
        if (obj instanceof Scope)
        {
            Scope other = (Scope) obj;
            if (this.department != other.department) { return false; }
            if (this.school != other.school) { return false; }
        }
        else
        {
        	if (! obj.getClass().isAssignableFrom(Scope.class)) { return false; }
            // It's not a Scope, but it's some compatible Map, AttributeSet, SimpleAttributeSet etc.
            @SuppressWarnings("unchecked")
            Map<String,String> m = (Map<String,String>) obj;
            String schoolAttr = m.get(SCHOOL);
            String deptAttr   = m.get(DEPARTMENT);
            try {
                int mapSchool = Integer.parseInt(schoolAttr);
                int mapDept   = Integer.parseInt(deptAttr);
                if (mapSchool == this.school && mapDept == this.department) { return true; }
            }
            catch (NumberFormatException e) {
                return false;
            }
            catch (NullPointerException e) {
                return false;
            }
        }
        return true;
    }


    /**
     * Get the attributes to use for this Scope.
     * @return the Key representation of this scope
     */
    public String getAttributes()
    {
        return attributeString;
    }


    /**<p>
     * <code>toString()</code> in this case is used primarily for setting up <code>&lt;select&gt; - &lt;option&gt;</code> lists.
     * The <code>&lt;spring:bind&gt;</code> tag requires that <code>toString()</code> return the String
     * that will be used as the value in the EL expression <code>${status.value}</code>
     * This returns the same key-value mapping as described for {@link java.util.Map} but without the enclosing braces ("{}").
     * </p>
     * {@inheritDoc}
     */
    @Override
    public String toString()
    {
        return this.scopeString;
    }

    /**
     * Make scope information available via EL ${scopeString}
     * @return scope string
     */
    public String getScopeString()
    {
        return this.scopeString;
    }
}
