/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: RecommendationFilter.java
 */

package edu.ucdavis.mw.myinfovault.service.decision;

import edu.ucdavis.mw.myinfovault.domain.decision.Decision;
import edu.ucdavis.mw.myinfovault.service.SearchFilterAdapter;

/**
 * Filters out recommendations.
 *
 * @author Craig Gilmore
 * @since 4.3
 */
public class RecommendationFilter extends SearchFilterAdapter<Decision>
{
    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.SearchFilter#include(java.lang.Object)
     */
    @Override
    public boolean include(Decision item)
    {
        return item.getDocumentType().isDecision();
    }
}
