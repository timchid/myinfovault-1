/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: PublicationServiceImpl.java
 */

package edu.ucdavis.mw.myinfovault.service.publications;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;

import org.apache.log4j.Logger;
import org.springframework.dao.DataAccessException;
import org.springframework.util.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import edu.ucdavis.mw.myinfovault.dao.publications.PublicationDao;
import edu.ucdavis.mw.myinfovault.domain.publications.Publication;
import edu.ucdavis.mw.myinfovault.domain.publications.PublicationImpl;
import edu.ucdavis.mw.myinfovault.domain.publications.PublicationSource;
import edu.ucdavis.mw.myinfovault.service.publications.converter.PublicationConverter;
import edu.ucdavis.mw.myinfovault.service.publications.converter.StreamConverter;
import edu.ucdavis.mw.myinfovault.util.IterableNodeList;
import edu.ucdavis.mw.myinfovault.util.XpathUtil;

/**
 * Service implementation for parsing, importing, and retrieving publications.
 *
 * @author Craig Gilmore
 * @since MIV 3.9.1
 */
public class PublicationServiceImpl implements PublicationService
{
    private static final Logger log = Logger.getLogger(PublicationServiceImpl.class);

    private PublicationDao publicationDao;

    private final DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();

    private final static String RECORDS_EXPRESSION = "//document/records/record";
    private final static String PUBLICATION_STATUS_EXPRESSION = "ref-status/text()";
    private final static String PUBLICATION_TYPE_EXPRESSION = "ref-type/text()";
    private final static String EXTERNAL_SOURCE_ID_EXPRESSION = "external-source/@sourceid";
    private final static String EXTERNAL_ID_EXPRESSION = "external-id/text()";
    private final static String YEAR_EXPRESSION = "date/year/text()";
    private final static String DATE_EXPRESSION = "date/text()";
    private final static String AUTHOR_EXPRESSION = "authors/author/text()";
    private final static String TITLE_EXPRESSION = "title/text()";
    private final static String EDITOR_EXPRESSION = "editor/text()";
    private final static String JOURNAL_EXPRESSION = "periodical/text()";
    private final static String VOLUME_EXPRESSION = "volume/text()";
    private final static String ISSUE_EXPRESSION = "issue/text()";
    private final static String PAGES_EXPRESSION = "pages/text()";
    private final static String PUBLISHER_EXPRESSION = "publisher/text()";
    private final static String CITY_EXPRESSION = "location/text()";
    private final static String ISBN_EXPRESSION = "isbn/text()";
    private final static String LINK_EXPRESSION = "url/text()";


    /**
     * Create publication service bean.
     * 
     * @param publicationDao Spring-injected publication DAO
     */
    public PublicationServiceImpl(PublicationDao publicationDao)
    {
        this.publicationDao = publicationDao;
    }


    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.publications.PublicationService#parsePublications(java.io.File, edu.ucdavis.mw.myinfovault.domain.publications.PublicationSource, int)
     */
    @Override
    public ParseResult parsePublications(final File content,
                                         final PublicationSource source,
                                         final int targetUser) throws IOException, SAXException, XPathExpressionException
    {
        //new file to write to
        File converted = File.createTempFile("convertedPublication", ".xml");

        //create parse result object to carry stats and publications
        ParseResult result = new ParseResult(targetUser, content, converted);

        //proceed if file content exists and has some data
        if (content != null && content.exists() && content.length() > 0)
        {
            //set in and out streams
            Writer out = new FileWriter(converted);
            Reader in = new FileReader(content);

            //get converter
            StreamConverter converter = new PublicationConverter(source);

            //convert stream and close
            converter.convert(in, out);
            out.close();
        }

        //parse and set publications
        result.setPublications(parsePublications(converted, result));

        return result;
    }


    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.publications.PublicationService#importPublications(java.io.File, edu.ucdavis.mw.myinfovault.domain.publications.PublicationSource, int, int)
     */
    @Override
    public ImportResult importPublications(final File content,
                                           final PublicationSource source,
                                           final int realUser,
                                           final int targetUser) throws IOException, SAXException, XPathExpressionException
    {
        return importPublications(Collections.singletonList(
                                      parsePublications(content,
                                                        source,
                                                        targetUser)),
                                  realUser,
                                  targetUser,
                                  true);
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.publications.PublicationService#importPublications(java.util.List, int, int)
     */
    @Override
    public ImportResult importPublications(final List<ParseResult> results,
                                           final int realUser,
                                           final int targetUser)
    {
        return importPublications(results,
                                  realUser,
                                  targetUser,
                                  false);
    }

    /**
     * Import publications from parse results; if not importAll, import result filters out unselected.
     * 
     * @param results
     * @param realUser
     * @param targetUser
     * @param importAll
     * @return import result
     */
    private ImportResult importPublications(final List<ParseResult> results,
                                            final int realUser,
                                            final int targetUser,
                                            final boolean importAll)
    {
        ImportResult result = new ImportResult(realUser,
                                               results,
                                               importAll);

        result.setPersisted(publicationDao.persistPublications(result.getPublications(),
                                                               result.getRealUser()));

        result.report();

        return result;
    }


    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.publications.PublicationService#getPublications(int)
     */
    @Override
    public Set<Publication> getPublications(int userId)
    {
        Set<Publication> publications = new TreeSet<Publication>();

        try
        {
            publications.addAll(publicationDao.getPublications(userId));
        }
        catch (DataAccessException e)
        {
            log.warn("Failed to get Publications for user '" + userId + "'; returning empty set.", e);
        }

        return publications;
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.publications.PublicationService#getPublications(int, edu.ucdavis.mw.myinfovault.domain.publications.PublicationSource)
     */
    @Override
    public Set<Publication> getPublications(int userId, PublicationSource source)
    {
        return (new PublicationSourceFilter(source)).apply(getPublications(userId));
    }

    /**
     * Parse publications from file and store in parse result.
     * 
     * @param content
     * @param result
     * @return parsed publications
     * @throws IOException
     * @throws SAXException
     * @throws XPathExpressionException
     */
    private Set<Publication> parsePublications(final File content, ParseResult result) throws IOException, SAXException, XPathExpressionException
    {
        Document doc = null;

        //proceed if content exists and has some data
        if (content != null && content.exists() && content.length() > 0)
        {
            DocumentBuilder builder;

            try
            {
                builder = builderFactory.newDocumentBuilder();
            }
            catch (ParserConfigurationException e)
            {
                // This can never happen because we use the plain unconfigured parser,
                throw new RuntimeException(e);
            }

            builder.reset();

            doc = builder.parse(content);
        }

        return parsePublications(doc, result);
    }

    /**
     * Parse publications from DOM document and store in parse result.
     * 
     * @param doc
     * @param result
     * @return parsed publications
     * @throws IOException
     * @throws XPathExpressionException
     */
    private Set<Publication> parsePublications(final Document doc, ParseResult result) throws IOException, XPathExpressionException
    {
        //set to return
        Set<Publication> parsedPublications = new TreeSet<Publication>();

        if (doc != null)
        {
            IterableNodeList records = XpathUtil.parseNodeList(RECORDS_EXPRESSION, doc);

            //loop over all <record> nodes from document
            for (Node record : records)
            {
                //record must have child elements
                if (record.hasChildNodes())
                {
                    Publication publication = parsePublication(record, result);

                    if (publication != null)
                    {
                        //increment duplicate if add does not change set
                        if (!parsedPublications.add(publication)) result.duplicate();
                    }
                }
                else
                {
                    result.fail(new IOException("Empty record found"));
                }
            }

            result.setProcessed(records.getLength());
        }

        return removeCurrent(parsedPublications, result);
    }

    /**
     * Remove from the parsed publications the current set of publications for the user.
     * 
     * @param publications
     * @param result
     * @return set difference of the given publications and target user's current publications
     */
    private Set<Publication> removeCurrent(Set<Publication> publications, ParseResult result)
    {
        //size before removing database duplicates
        int parsed = publications.size();

        if (parsed > 0)
        {
            //remove duplicate publications already in database
            publications.removeAll(getPublications(result.getTargetUser()));

            //update duplicate count in result statistics
            result.duplicate(parsed - publications.size());
        }

        return publications;
    }

    /**
     * Parse from a DOM node one publication.
     * 
     * @param node
     * @param result
     * @return parsed publication
     * @throws XPathExpressionException
     */
    private Publication parsePublication(final Node node, ParseResult result) throws XPathExpressionException
    {
        String externalId = XpathUtil.parseNodeValue(EXTERNAL_ID_EXPRESSION, node);

        Publication publication = null;

        try
        {
            publication = new PublicationImpl(result.getTargetUser(),
                                              XpathUtil.parseNodeValue(PUBLICATION_STATUS_EXPRESSION, node),
                                              XpathUtil.parseNodeValue(PUBLICATION_TYPE_EXPRESSION, node),
                                              XpathUtil.parseNodeValue(EXTERNAL_SOURCE_ID_EXPRESSION, node),
                                              externalId,
                                              parseYear(node),
                                              parseAuthors(node),
                                              XpathUtil.parseNodeValue(TITLE_EXPRESSION, node),
                                              XpathUtil.parseNodeValue(EDITOR_EXPRESSION, node),
                                              XpathUtil.parseNodeValue(JOURNAL_EXPRESSION, node),
                                              XpathUtil.parseNodeValue(VOLUME_EXPRESSION, node),
                                              XpathUtil.parseNodeValue(ISSUE_EXPRESSION, node),
                                              XpathUtil.parseNodeValue(PAGES_EXPRESSION, node),
                                              XpathUtil.parseNodeValue(PUBLISHER_EXPRESSION, node),
                                              XpathUtil.parseNodeValue(CITY_EXPRESSION, node),
                                              XpathUtil.parseNodeValue(ISBN_EXPRESSION, node),
                                              XpathUtil.parseNodeValue(LINK_EXPRESSION, node));

            result.parsed();
        }
        catch (IllegalArgumentException e)
        {
            result.fail(externalId, e);
        }

        return publication;
    }

    /**
     * Parse year value from node.
     * 
     * @param parent
     * @return year value
     * @throws XPathExpressionException
     */
    private String parseYear(final Node parent) throws XPathExpressionException
    {
        String year = XpathUtil.parseNodeValue(YEAR_EXPRESSION, parent);

        //grab date string if year tag isn't available
        if (!StringUtils.hasText(year))
        {
            year = XpathUtil.parseNodeValue(DATE_EXPRESSION, parent);
        }

        return year;
    }

    /**
     * Parse author list from node.
     * 
     * @param parent
     * @return delimited list of authors
     * @throws XPathExpressionException
     */
    private String parseAuthors(final Node parent) throws XPathExpressionException
    {
        IterableNodeList authorList = XpathUtil.parseNodeList(AUTHOR_EXPRESSION, parent);

        StringBuilder authors = new StringBuilder();

        for (Node author : authorList)
        {
            authors.append(author.getNodeValue());

            if (authorList.hasNext()) {
                authors.append(", ");
            }
        }

        return authors.toString();
    }
}
