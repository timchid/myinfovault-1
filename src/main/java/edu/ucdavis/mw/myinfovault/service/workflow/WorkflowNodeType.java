package edu.ucdavis.mw.myinfovault.service.workflow;

/**
 * This class represents workflow node type of a workflow node.
 * @author rhendric
 *
 */
public enum WorkflowNodeType
{
    INITIAL ("Initial Node"),
    ENROUTE ("Enroute Node"),
    FINAL ("Final Node"),
    EXCEPTION ("Exception Node");

    private String description = null;

    // Initialize the description.
    WorkflowNodeType(String description)
    {
      //this.setDescription(description);
        this.description = description;
    }

/* No setter - must be immutable after constructor.
    public void setDescription(String description)
    {
        this.description = description;
    }
*/

    public String getDescription()
    {
        return description;
    }
}
