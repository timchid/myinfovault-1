/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: GroupSearchFilter.java
 */

package edu.ucdavis.mw.myinfovault.service.group;

import org.apache.commons.lang.StringUtils;

import edu.ucdavis.mw.myinfovault.domain.group.Group;
import edu.ucdavis.mw.myinfovault.service.SqlAwareFilterAdapter;

/**
 * Filters a collection of groups by search term.
 *
 * @author Craig Gilmore
 * @since MIV 4.3
 */
public class GroupSearchFilter extends SqlAwareFilterAdapter<Group>
{
    private final static String PATTERN = "Groups.Name like ?";

    private final String searchName;

    /**
     * @param searchName String to filter groups by name
     */
    public GroupSearchFilter(String searchName)
    {
        super(PATTERN, "%" + searchName + "%");

        this.searchName = searchName;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean include(Group item)
    {
        //The group must contain the given search term
        return StringUtils.containsIgnoreCase(item.getName(), searchName);
    }
}
