/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: PublicationSourceFilter.java
 */

package edu.ucdavis.mw.myinfovault.service.publications;

import edu.ucdavis.mw.myinfovault.domain.publications.Publication;
import edu.ucdavis.mw.myinfovault.domain.publications.PublicationSource;
import edu.ucdavis.mw.myinfovault.service.SearchFilterAdapter;

/**
 * Filters publications by source.
 *
 * @author Craig Gilmore
 * @since 4.4.2
 */
public class PublicationSourceFilter extends SearchFilterAdapter<Publication>
{
    private final PublicationSource source;

    /**
     * Create a publication source filter.
     *
     * @param source Source by which publications will be filtered
     */
    public PublicationSourceFilter(PublicationSource source)
    {
        this.source = source;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean include(Publication item)
    {
        return item.getSource() == source;
    }
}
