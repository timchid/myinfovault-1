/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: WorkflowServiceImpl.java
 */

package edu.ucdavis.mw.myinfovault.service.workflow;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;

import com.google.common.eventbus.Subscribe;

import edu.ucdavis.mw.myinfovault.dao.workflow.WorkflowDao;
import edu.ucdavis.mw.myinfovault.domain.action.DossierActionType;
import edu.ucdavis.mw.myinfovault.domain.action.DossierDelegationAuthority;
import edu.ucdavis.mw.myinfovault.domain.action.DossierLocation;
import edu.ucdavis.mw.myinfovault.domain.dossier.Dossier;
import edu.ucdavis.mw.myinfovault.events.ConfigurationChangeEvent;
import edu.ucdavis.mw.myinfovault.events.EventDispatcher;
import edu.ucdavis.mw.myinfovault.service.dossier.DossierRoutingDefinitionDto;
import edu.ucdavis.mw.myinfovault.service.dossier.WorkflowException;
import edu.ucdavis.myinfovault.exception.MivSevereApplicationError;

/**
 * Implementation of the WorkflowService interface.
 * The WorkflowService is used to retrieve the valid workflow nodes for a specified dossier.
 * @author rhendric
 * @since MIV 4.6.1
 */
public class WorkflowServiceImpl implements WorkflowService
{

    private WorkflowDao workflowDao = null;
    private static Map<String, WorkflowNode>routingDefinitionMap = new LinkedHashMap<String, WorkflowNode>();
    private static Logger logger = LoggerFactory.getLogger(WorkflowServiceImpl.class);

    /**
     * Instantiate the WorkflowService and load the routing definitions from the database
     * @param workflowDao
     */
    public WorkflowServiceImpl(WorkflowDao workflowDao)
    {
        this.workflowDao = workflowDao;
        this.loadRoutingDefinitions();
        // Register with the event bus to listen for ConfigurationChangeEvents
        EventDispatcher.getDispatcher().register(this);
    }


    /**
     * Load the routing definitions from the database.
     * A map is built of WorkflowNodes with a key of DelegationAuthority:ActionType:Location.
     * The map is built in two passes. The first pass sets up the map using locations the second pass
     * then recursively adds the WorkflowNode objects.
     *
     * A WorkflowNode retrieved from the map will have access to a list of next WorkflowNode's and each
     * of those WorkflowNode's will in turn have access to a list of next WorkflowNode's and on and on...hence the recursion.
     *
     * Currently there is no list of previous WorkflowNode's being built since it is not possible to make use
     * of the list in determining a specific previous node that a dossier may return to.
     *  Previous WorkflowNodes are currently accessed from the Dossier object itself which is updated with only
     *  those nodes in a path which have been visited.
     */
    private void loadRoutingDefinitions()
    {
        routingDefinitionMap.clear();
        // Get the routing definitions
        try
        {
            for (DossierRoutingDefinitionDto dto : workflowDao.getDossierRoutingDefinitions())
            {
                // The key into the map will be DelegationAuthority:ActionType:CurrentLocation
                String key = dto.getDelegationAuthority()+":"+dto.getActionType()+":"+dto.getCurrentWorkflowLocation();

                // If this location is already in the map, just add the next/previous nodes
                WorkflowNode workflowNode = routingDefinitionMap.get(key);
                if (workflowNode == null)
                {
                    workflowNode = new WorkflowNode(dto.getCurrentWorkflowLocation());
                    routingDefinitionMap.put(key, workflowNode);
                }
                workflowNode.addNextLocation(dto.getNextWorkflowLocation());
                workflowNode.addPreviousLocation(dto.getPreviousWorkflowLocation());
                workflowNode.setWorkflowNodeType(dto.getWorkflowNodeType());
                workflowNode.setDisplayOnlyWhenAvailable(dto.isDisplayOnlyWhenAvailable());
                // Parse the LocationPrerequisiteAttributes into absolute and optional
                if ((dto.getLocationPrerequisiteAttributes() != null) &&
                   (workflowNode.optionalLocationPrerequisiteAttributes.isEmpty() || workflowNode.optionalLocationPrerequisiteAttributesList.isEmpty()))
                {
                    // Extract any optional 'or' conditions in parenthesis
                    String preRequisiteStr = dto.getLocationPrerequisiteAttributes();
                    Pattern orConditions = Pattern.compile(".*?(\\(.*?\\)).*?");
                    Matcher m = orConditions.matcher(preRequisiteStr);
                    // Get each of the optional conditions
                    while (m.find())
                    {
                        workflowNode.optionalLocationPrerequisiteAttributes = new HashMap<String, List<String>>();
                        if (!workflowNode.optionalLocationPrerequisiteAttributesList.contains(workflowNode.optionalLocationPrerequisiteAttributes))
                        {
                            workflowNode.optionalLocationPrerequisiteAttributesList.add(workflowNode.optionalLocationPrerequisiteAttributes);
                        }
                        preRequisiteStr = preRequisiteStr.replaceFirst("\\("+m.group(1)+"\\)","");
                        String conditions = m.group(1).replaceFirst("\\(", "").replaceFirst("\\)", "");
                        String [] prerequisites = conditions.split(",");
                        for (String prerequisite : prerequisites)
                        {
                            String [] keyValuePair = prerequisite.split("=");
                            if (keyValuePair.length == 2)
                            {
                                workflowNode.addOptionalLocationPrerequisite(keyValuePair[0],keyValuePair[1]);
                            }
                        }
                    }

                    // Now get the absolute prerequisites to be satisfied
                    String [] prerequisites = preRequisiteStr.split(",");
                    for (String prerequisite : prerequisites)
                    {
                        String [] keyValuePair = prerequisite.split("=");
                        if (keyValuePair.length == 2)
                        {
                            workflowNode.addAbsoluteLocationPrerequisite(keyValuePair[0],keyValuePair[1]);
                        }
                    }
                }
            }
            // Now make a pass thru the map to add the workflow nodes
            for (String key : routingDefinitionMap.keySet())
            {
                String keyParts[] = key.split(":");
                this.loadNextNode(DossierDelegationAuthority.valueOf(keyParts[0]), DossierActionType.valueOf(keyParts[1]), DossierLocation.valueOf(keyParts[2]));
                this.loadPreviousNode(DossierDelegationAuthority.valueOf(keyParts[0]), DossierActionType.valueOf(keyParts[1]), DossierLocation.valueOf(keyParts[2]));
            }

        }
        catch (DataAccessException|WorkflowException e)
        {
            throw new MivSevereApplicationError("errors.workflowservice.loadroutingdefinitions", e);
        }
        this.dumpRoutingDefinitions();
    }


    /**
     * Dump the routing definition map
     */
    private void dumpRoutingDefinitions()
    {
        logger.info("+++ Dumping Routing Definition Map +++");
        for (String key : WorkflowServiceImpl.routingDefinitionMap.keySet())
        {
            logger.info("Key: {}", key);
            logger.info("Node Location: {}", routingDefinitionMap.get(key).getNodeLocation());
            logger.info("Node Name: {}", routingDefinitionMap.get(key).getWorkflowNodeName());
            logger.info("Next Nodes: {}", routingDefinitionMap.get(key).getNextLocations());
            logger.info("Previous Nodes: {}", routingDefinitionMap.get(key).getPreviousLocations());
            logger.info("Node Location Prerequisite Attributes: {}", routingDefinitionMap.get(key).getAbsoluteLocationPrerequisiteAttributes());
            logger.info("++++++++++++++++++++++++++++++++++++++");
        }
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.workflow.WorkflowService#getInitialNode(edu.ucdavis.mw.myinfovault.domain.dossier.Dossier)
     */
    @Override
    public WorkflowNode getInitialNode(Dossier dossier) throws WorkflowException
    {
        // We will not have a location for a newly routed dossier, so we search the routing map using a partial key
        String partialKey = dossier.getAction().getDelegationAuthority() + ":" + dossier.getAction().getActionType() + ":";
        for (String key :routingDefinitionMap.keySet())
        {
            if (key.startsWith(partialKey) && routingDefinitionMap.get(key).getWorkflowNodeType() == WorkflowNodeType.INITIAL)
            {
                return routingDefinitionMap.get(key);
            }
        }

        throw new WorkflowException("Unable to find the " + WorkflowNodeType.INITIAL.getDescription() + " for " + dossier);
    }

    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.workflow.WorkflowService#getFinalNode(edu.ucdavis.mw.myinfovault.domain.dossier.Dossier)
     */
    @Override
    public WorkflowNode getFinalNode(Dossier dossier) throws WorkflowException
    {
        WorkflowNode workflowNode = this.getFinalNode(this.getCurrentWorkflowNode(dossier));

        if (workflowNode == null)
        {
            throw new WorkflowException("Unable to find the " + WorkflowNodeType.FINAL.getDescription() + " for " + dossier);
        }

        return workflowNode;
    }

    /**
     * Recursively walk the workflow nodes to find the final node
     * @param workflowNode
     * @return workflowNode
     */
    private WorkflowNode getFinalNode(WorkflowNode workflowNode)
    {
        if (workflowNode.getWorkflowNodeType() == WorkflowNodeType.FINAL)
        {
            return workflowNode;
        }

        for (WorkflowNode nextNode : workflowNode.getNextWorkflowNodes())
        {
            return getFinalNode(nextNode);
        }
        return null;
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.workflow.WorkflowService#getNextLocations(edu.ucdavis.mw.myinfovault.domain.dossier.Dossier)
     */
    @Override
    public List<DossierLocation> getNextLocations(Dossier dossier)
    {
        return getCurrentWorkflowNode(dossier).getNextLocations();
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.workflow.WorkflowService#getNextWorkflowNodes(edu.ucdavis.mw.myinfovault.domain.dossier.Dossier)
     */
    @Override
    public List<WorkflowNode> getNextWorkflowNodes(Dossier dossier) throws WorkflowException
    {
        return getCurrentWorkflowNode(dossier).getNextWorkflowNodes();
    }

    /**
     * Load a node's next and previous workflow nodes.
     *
     * @param delegationAuthority delegation authority
     * @param actionType action type
     * @param location node location
     * @return WorkflowNode loaded node
     * @throws WorkflowException if no node found for the given criteria
     */
    private WorkflowNode loadNextNode(DossierDelegationAuthority delegationAuthority,
                                      DossierActionType actionType,
                                      DossierLocation location) throws WorkflowException
    {
        WorkflowNode currentWorkflowNode = getNode(delegationAuthority, actionType, location);

        // load next nodes
        for (DossierLocation nextLocation : currentWorkflowNode.getNextLocations())
        {
            // no next node for unknown location
            if (nextLocation == DossierLocation.UNKNOWN) break;

            currentWorkflowNode.addNextWorkflowNode(loadNextNode(delegationAuthority, actionType, nextLocation));
        }

        return currentWorkflowNode;
    }

    /**
     * Load a node's next and previous workflow nodes.
     *
     * @param delegationAuthority delegation authority
     * @param actionType action type
     * @param location node location
     * @return WorkflowNode loaded node
     * @throws WorkflowException if no node found for the given criteria
     */
    private WorkflowNode loadPreviousNode(DossierDelegationAuthority delegationAuthority,
                                          DossierActionType actionType,
                                          DossierLocation location) throws WorkflowException
    {
        WorkflowNode currentWorkflowNode = getNode(delegationAuthority, actionType, location);

        // load previous nodes
        for (DossierLocation previousLocation : currentWorkflowNode.getPreviousLocations())
        {
            // no previous node for unknown location
            if (previousLocation == DossierLocation.UNKNOWN) break;

            currentWorkflowNode.addPreviousWorkflowNode(loadPreviousNode(delegationAuthority, actionType, previousLocation));
        }

        return currentWorkflowNode;
    }

    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.workflow.WorkflowService#getCurrentWorkflowNode(edu.ucdavis.mw.myinfovault.domain.dossier.Dossier)
     */
    @Override
    public WorkflowNode getCurrentWorkflowNode(Dossier dossier)
    {
        try
        {
            return getNode(dossier.getAction().getDelegationAuthority(),
                           dossier.getAction().getActionType(),
                           dossier.getDossierLocation());
        }
        catch (WorkflowException e)
        {
            throw new MivSevereApplicationError("errors.workflowservice.dossier.invalid",
                                                e,
                                                dossier.getDossierId());
        }
    }

    /**
     * Reload the routing definitions when a configuration change event is received.
     *
     * @param e configuration change event
     */
    @Subscribe
    public void reloadRoutingDefinitions(ConfigurationChangeEvent e)
    {
        logger.debug("Reloading routing definitions because a ConfigurationChangeEvent was received");
        this.loadRoutingDefinitions();
    }

    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.workflow.WorkflowService#getWorkflowNodeForLocation(edu.ucdavis.mw.myinfovault.domain.dossier.Dossier, edu.ucdavis.mw.myinfovault.service.dossier.DossierLocation)
     */
    @Override
    public WorkflowNode getWorkflowNodeForLocation(Dossier dossier, DossierLocation location) throws WorkflowException
    {
        /*
         * Return node with unknown location.
         */
        if (location == DossierLocation.UNKNOWN)
        {
            return new WorkflowNode(location);
        }

        return getNode(dossier.getAction().getDelegationAuthority(),
                       dossier.getAction().getActionType(),
                       location);
    }

    /**
     * Get the workflow node for the given delegation authority, action type, and location.
     *
     * @param delegationAuthority delegation authority
     * @param actionType action type
     * @param location action location
     * @return workflow node
     * @throws WorkflowException if no node found
     */
    private WorkflowNode getNode(DossierDelegationAuthority delegationAuthority,
                                 DossierActionType actionType,
                                 DossierLocation location) throws WorkflowException
    {
        WorkflowNode node = routingDefinitionMap.get(delegationAuthority + ":" + actionType + ":" + location);

        if (node != null) return node;

        String msg = "Unable to get workflow node for " + delegationAuthority + " " + actionType + " from " + location + " location.";
        logger.error(msg);
        throw new WorkflowException(msg);
    }
}
