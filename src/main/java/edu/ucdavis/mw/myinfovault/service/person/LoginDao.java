/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: LoginDao.java
 */


package edu.ucdavis.mw.myinfovault.service.person;

import java.util.Collection;

/**
 * Manage login information for people / users.
 *
 * @author Stephen Paulsen
 * @since MIV 3.0
 */
public interface LoginDao
{
    /**
     * Gets login and identifier information for the given login name.
     *
     * @param login
     * @return
     */
    public Login getLogin(String login);

    /**
     * Get all the login names
     *
     * @param userIdentifier
     */
    public Collection<Login> getLoginsForUser(Object userIdentifier);

    /**
     * Persists a login to the backing store.
     *
     * @param login
     * @return
     */
    public boolean save(Login login);
}
