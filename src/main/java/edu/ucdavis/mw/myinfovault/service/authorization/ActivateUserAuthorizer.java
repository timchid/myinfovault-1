/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: ActivateUserAuthorizer.java
 */

package edu.ucdavis.mw.myinfovault.service.authorization;

import static edu.ucdavis.mw.myinfovault.service.person.MivRole.DEAN;
import static edu.ucdavis.mw.myinfovault.service.person.MivRole.DEPT_ASSISTANT;
import static edu.ucdavis.mw.myinfovault.service.person.MivRole.DEPT_CHAIR;
import static edu.ucdavis.mw.myinfovault.service.person.MivRole.DEPT_STAFF;
import static edu.ucdavis.mw.myinfovault.service.person.MivRole.SCHOOL_STAFF;
import static edu.ucdavis.mw.myinfovault.service.person.MivRole.SENATE_STAFF;
import static edu.ucdavis.mw.myinfovault.service.person.MivRole.SYS_ADMIN;
import static edu.ucdavis.mw.myinfovault.service.person.MivRole.VICE_PROVOST_STAFF;

import java.util.EnumSet;

import edu.ucdavis.mw.myinfovault.util.AttributeSet;

import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.mw.myinfovault.service.person.MivRole;

/**
 * Authorize the ability to deactivate and reactivate users, which is one component
 * of the ability to edit users. Overrides the scoping rules of the base SameScopeAuthorizer.
 *
 * @author Stephen Paulsen
 * @since MIV 4.6.1
 */
public class ActivateUserAuthorizer extends EditUserAuthorizer
{
    /** Roles that are allowed to activate/deactivate other users. */
    private static final MivRole[] mayEditRoles = {
        SYS_ADMIN,
        VICE_PROVOST_STAFF,
        SENATE_STAFF,
        SCHOOL_STAFF,
        DEPT_STAFF
    }; // TODO: Externalize this list so changes can be made without editing code.


    /*
     * Override the SameScopeAuthorizer/EditUserAuthorizer scoped and unscoped roles,
     * because scope for editing/activating a user is different than scope for managing
     * a dossier.
     */
    {
        this.scopedRoles = new MivRole[] { DEPT_ASSISTANT, DEPT_STAFF, DEPT_CHAIR, SCHOOL_STAFF, DEAN, SENATE_STAFF };
        this.unscopedRoles = EnumSet.of(SYS_ADMIN, VICE_PROVOST_STAFF);
    }

    @Override
    public boolean hasPermission(MivPerson actor, String permissionName, AttributeSet permissionDetails)
    {
        if (hasPermissionDetails(permissionDetails))
        {
            if (permissionDetails.get("SELF") != null) {
                return actor.hasRole(SYS_ADMIN);
            }
        }

        return actor.hasRole(mayEditRoles);
    }

}
