/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: UserReportService.java
 */

package edu.ucdavis.mw.myinfovault.service.report;

import java.util.List;
import java.util.Map;

import edu.ucdavis.mw.myinfovault.util.AttributeSet;

/**
 * This class defines all of the currently required user report search methods.
 * The UserReportServiceImpl class implements all methods in the interface.
 *
 * @author Pradeep K Haldiya
 * @since MIV 4.0
 */
public interface UserReportService
{

    /**
     * To find user by AttributeSet. It also contains assignments details
     *
     * @param criteria
     * @return List of Map
     */
    public List<Map<String, Object>> findUsers(final AttributeSet criteria);

}
