/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: ConvertPersonXml.java
 */

package edu.ucdavis.mw.myinfovault.service.person;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.apache.log4j.Logger;
import org.jdom2.Document;
import org.jdom2.Element;

import edu.ucdavis.mw.myinfovault.util.XmlManager;

/**
 * Convert MivPerson into xml
 * @author pradeeph
 * @since  MIV 4.0
 */
public class ConvertPersonXml extends XmlManager
{
    private static Logger logger = Logger.getLogger(ConvertPersonXml.class);

    /**
     * To convert mivPerson and generate XML string.
     *
     * @param mivPersons
     * @return
     */
    public static String generateXMLString(MivPerson... mivPersons)
    {
        return documentToString(generateXML(mivPersons));
    }


    /**
     * To convert collection of mivPersons and generate XML string.
     *
     * @param mivPersonList
     * @return
     */
    public static String generateXMLString(Collection<MivPerson> mivPersonList)
    {
        return documentToString(generateXML(mivPersonList));
    }


    /**
     * to convert mivPerson and generate xml Document
     * @param mivPersons
     * @return
     */
    public static Document generateXML(MivPerson... mivPersons)
    {
        return generateXML(Arrays.asList(mivPersons));
    }


    /**
     * to convert list of mivPersons and generate xml Document
     * @param mivPersonList
     * @return
     */
    public static Document generateXML(Collection<MivPerson> mivPersonList)
    {
        Document document = new Document();

        try
        {
            Element root = createElement("miv");//miv is the root of the XML

            if (mivPersonList != null && mivPersonList.size() > 0)
            {
                Element personsElement = createPersonsElement(mivPersonList);
                if (personsElement != null)
                {
                    root.addContent(personsElement);
                }
            }
            document.setContent(root);
        }
        catch (Exception e)
        {
            logger.error("Exception in ConvertPersonXml ["+e.getMessage()+"]");

            // Handle the exception
            Element root = createElement("miv");
            Element exception = createElement("exception");
            exception.setAttribute("Message", e.getMessage());
            document.setContent(root);
        }
        return document;
    }


    /**
     * create Persons element
     * @param personList
     * @return Element
     */
    private static Element createPersonsElement(Collection<MivPerson> mivPersonList)
    {
        Element personsElement = createElement("persons");

        if (mivPersonList != null)// && mivPersonList.size() > 0) // Don't need to test size()>0 : the "for" simply won't loop if size==0
        {
            for (MivPerson mivPerson : mivPersonList)
            {
                Element personElement = createPersonElement(mivPerson);
                if (personElement != null)
                {
                    personsElement.addContent(personElement);
                }
            }
        }

        return personsElement;
    }


    /**
     * create Person element
     * @param person
     * @return Element
     */
    private static Element createPersonElement(MivPerson person)
    {
        Element personElement = createElement("person");

        if (person != null)
        {
            personElement.setAttribute("UserID", String.valueOf(person.getUserId()));
            personElement.setAttribute("SortName", person.getSortName());
            personElement.setAttribute("isActive", String.valueOf(person.isActive()));

            // Add Email's
            Element emailAddresses = createEmailAddressesElement(person.getEmailAddresses(),person.getPreferredEmail());
            if (emailAddresses != null)
            {
                personElement.addContent(emailAddresses);
            }

            // Add Appointments
            Collection<Appointment> appointmentList = person.getAppointments();
            //logger.info("appointmentList :: "+appointmentList);
            if (appointmentList != null && appointmentList.size() > 0)
            {
                //logger.info("appointmentList.size :: "+appointmentList.size());
                Element appointments = createAppointmentsElement(appointmentList);
                if (appointments != null)
                {
                    personElement.addContent(appointments);
                }
            }

            // Add Assignments
            Collection<AssignedRole> assignmentList = person.getAssignedRoles();
            //logger.info("assignmentList :: "+assignmentList);
            if (assignmentList != null && assignmentList.size() > 0)
            {
                //logger.info("assignmentList.size :: "+assignmentList.size());
                Element assignments = createAssignedRolesElement(assignmentList);
                if (assignments != null)
                {
                    personElement.addContent(assignments);
                }
            }

        }

        return personElement;
    }


    /**
     * create AssignedRoles Element
     * @param assignmentList
     * @return
     */
    private static Element createAssignedRolesElement(Collection<AssignedRole> assignmentList)
    {
        Element assignedRoles = createElement("AssignedRoles");

        if (assignmentList != null && assignmentList.size() > 0)
        {
            Element assignedRoleElement = null;
            for (AssignedRole assignment : assignmentList)
            {
                if (assignment != null)
                {
                    assignedRoleElement =  createAssignedRoleElement(assignment);
                    if (assignedRoleElement != null)
                    {
                        assignedRoles.addContent(assignedRoleElement);
                    }
                }
            }
        }

        return assignedRoles;
    }


    /**
     * create AssignedRole Element
     * @param assignment
     * @return
     */
    private static Element createAssignedRoleElement(AssignedRole assignment)
    {
        Element assignedRoleElement = null;
        if (assignment != null)
        {
            assignedRoleElement = createElement("AssignedRole");
            assignedRoleElement.setAttribute("Scope", assignment.getScope().getAttributes());
            assignedRoleElement.setAttribute("Role", assignment.getRole().toString());
            assignedRoleElement.setAttribute("isPrimary", String.valueOf(assignment.isPrimary()));
        }

        return assignedRoleElement;
    }


    /**
     * create Appointments Element
     * @param appointmentList
     * @return
     */
    private static Element createAppointmentsElement(Collection<Appointment> appointmentList)
    {
        Element appointments = createElement("Appointments");

        if (appointmentList != null)// && appointmentList.size() > 0) // Don't need to test size()>0 : the "for" simply won't loop if size==0
        {
            Element appointmentElement = null;
            for (Appointment appointment : appointmentList)
            {
                if (appointment != null)
                {
                    appointmentElement = createAppointmentElement(appointment);
                    if (appointmentElement != null)
                    {
                        appointments.addContent(appointmentElement);
                    }
                }
            }
        }

        return appointments;
    }


    /**
     * create Appointment Element
     * @param appointment
     * @return
     */
    private static Element createAppointmentElement(Appointment appointment)
    {
        Element appointmentElement = null;
        if (appointment != null)
        {
            appointmentElement = createElement("Appointment");
            appointmentElement.setAttribute("Scope", appointment.getScope().getAttributes());
            appointmentElement.setAttribute("Percent", String.valueOf(appointment.getPercent())+"%");
        }

        return appointmentElement;
    }


    /**
     * create Email Addresses Element
     * @param emailAddressList
     * @param preferredEmail
     * @return
     */
    private static Element createEmailAddressesElement(List<String> emailAddressList,String preferredEmail)
    {
        Element emailAddresses = createElement("EmailAddresses");

        // Adding preferredEmail first
        Element emailAddress = createEmailAddressElement(preferredEmail,true);
        emailAddresses.addContent(emailAddress);

        // Adding other EmailAddresses
        if (emailAddressList != null)// && emailAddressList.size() > 0) // Don't need to test size()>0 : the "for" simply won't loop if size==0
        {
            for (String string : emailAddressList)
            {
                if (!string.trim().equalsIgnoreCase(preferredEmail.trim()))
                {
                    emailAddress = createEmailAddressElement(string,false);
                    emailAddresses.addContent(emailAddress);
                }
            }
        }

        return emailAddresses;
    }


    /**
     * create Email Address Element
     * @param emailAddress
     * @param isPreferred
     * @return
     */
    private static Element createEmailAddressElement(String emailAddress, boolean isPreferred)
    {
        Element emailAddressElement = createElement("EmailAddress");
        emailAddressElement.setAttribute("EmailID", emailAddress);
        if (isPreferred)
        {
            emailAddressElement.setAttribute("isPreferred","true");
        }

        return emailAddressElement;
    }

}
