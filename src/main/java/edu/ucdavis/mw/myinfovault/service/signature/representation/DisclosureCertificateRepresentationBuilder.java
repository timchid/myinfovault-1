/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: DisclosureCertificateRepresentationBuilder.java
 */

package edu.ucdavis.mw.myinfovault.service.signature.representation;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.text.DateFormat;
import java.text.MessageFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import com.google.common.hash.HashCode;

import edu.ucdavis.mw.myinfovault.domain.action.Assignment;
import edu.ucdavis.mw.myinfovault.domain.action.DossierActionType;
import edu.ucdavis.mw.myinfovault.domain.action.DossierLocation;
import edu.ucdavis.mw.myinfovault.domain.action.StatusType;
import edu.ucdavis.mw.myinfovault.domain.action.Title;
import edu.ucdavis.mw.myinfovault.domain.disclosurecertificate.DCBo;
import edu.ucdavis.mw.myinfovault.domain.disclosurecertificate.Material;
import edu.ucdavis.mw.myinfovault.domain.raf.Department;
import edu.ucdavis.mw.myinfovault.domain.raf.RafBo;
import edu.ucdavis.mw.myinfovault.domain.raf.Rank;
import edu.ucdavis.mw.myinfovault.domain.signature.MivElectronicSignature;
import edu.ucdavis.mw.myinfovault.service.MivServiceLocator;
import edu.ucdavis.mw.myinfovault.util.HashingUtil;
import edu.ucdavis.myinfovault.MIVConfig;
import edu.ucdavis.myinfovault.exception.MivSevereApplicationError;

/**
 * Builds document representations for candidate disclosure certificate documents.
 *
 * @author Craig Gilmore
 * @since MIV 4.7.1
 */
class DisclosureCertificateRepresentationBuilder extends DocumentRepresentationBuilder
{
    private static final String DIGEST = MIVConfig.getConfig().getProperty("electronicSignature-digest") == null
                                       ? "SHA-256"
                                       : MIVConfig.getConfig().getProperty("electronicSignature-digest");

    private final ThreadLocal<DateFormat> longDateFormat =
            new ThreadLocal<DateFormat>() {
            @Override protected DateFormat initialValue() {
                return SimpleDateFormat.getDateInstance(DateFormat.LONG);
            }
        };

    private final ThreadLocal<DateFormat> emailedDateFormat =
        new ThreadLocal<DateFormat>() {
        @Override protected DateFormat initialValue() {
            return SimpleDateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.SHORT);
        }
    };

    private final ThreadLocal<NumberFormat> salaryFormat =
        new ThreadLocal<NumberFormat>() {
        @Override protected NumberFormat initialValue() {
            NumberFormat format = NumberFormat.getNumberInstance(Locale.US);

            format.setMinimumFractionDigits(2);
            format.setMaximumFractionDigits(2);
            format.setGroupingUsed(false);

            return format;
        }
    };

    /**
     * Version one for candidate disclosure certificate document representations.
     *
     * @param dc candidate disclosure certificate
     * @return document representation
     */
    @SuppressWarnings("deprecation")
    @Version(1)
    protected byte[] getVersionOne(DCBo dc)
    {
        String representation =
                dc.getEnteredDate() +
                Integer.toString(dc.getDocumentId()) +
                Long.toString(dc.getDossier().getDossierId()) +
                Integer.toString(dc.getDossier().getAction().getCandidate().getUserId()) +
                Integer.toString(dc.getSchoolId()) +
                Integer.toString(dc.getDepartmentId()) +
                dc.getCandidateName() +
                dc.getSchoolName() +
                dc.getDepartmentName() +
                dc.getAdditionalInfo() +
                dc.getChangesAdditions() +
                MessageFormat.format("{0,date,yyyy}", dc.getDossier().getAction().getEffectiveDate());

        representation += dc.getDossier().getAction().getActionType() == DossierActionType.PROMOTION ? "1" : "0";
        representation += dc.getDossier().getAction().getActionType() == DossierActionType.MERIT ? "1" : "0";

        representation += dc.isActionAppraisal() ? "1" : "0";
        representation += dc.isActionAcceleration() ? "1" : "0";
        representation += dc.isActionDeferral() ? "1" : "0";
        representation += dc.isActionOther()? "1" : "0";

        for (edu.ucdavis.mw.myinfovault.domain.disclosurecertificate.Rank rank : dc.getRanks())
        {
            representation += rank.getPresentRankAndStep() != null ? rank.getPresentRankAndStep() : "";
            representation += rank.getProposedRankAndStep() != null ? rank.getProposedRankAndStep() : "";
        }

        for (Material material : dc.getMaterials())
        {
            representation += material.isSelected() ? "1" : "0";
            representation += material.getMaterial() != null ? material.getMaterial().toString() : "";
        }

        return representation.getBytes();
    }

    /**
     * Version two for candidate disclosure certificate document representations.
     *
     * @param  dc candidate disclosure certificate
     * @return document representation
     * @author Pradeep Haldiya
     * @since  v4.7.1
     */
    @Version(2)
    protected byte[] getVersionTwo(DCBo dc)
    {
        StringBuilder representation = new StringBuilder();

        representation.append(emailedDateFormat.get().format(getEmailedDate(dc)))
                      .append(Integer.toString(dc.getDocumentId()))
                      .append(Long.toString(dc.getDossier().getDossierId()))
                      .append(Integer.toString(dc.getDossier().getAction().getCandidate().getUserId()))
                      .append(Integer.toString(dc.getSchoolId()))
                      .append(Integer.toString(dc.getDepartmentId()))
                      .append(dc.getCandidateName())
                      .append(dc.getSchoolName())
                      .append(dc.getDepartmentName())
                      .append(dc.getAdditionalInfo())
                      .append(dc.getChangesAdditions())
                      .append(dc.getDossier().getAction().getActionType())
                      .append(longDateFormat.get().format(dc.getDossier().getAction().getEffectiveDate()));

        for (edu.ucdavis.mw.myinfovault.domain.disclosurecertificate.Rank rank : dc.getRanks())
        {
            representation.append(rank != null ? rank.getPresentRankAndStep() : "")
                          .append(rank != null ? rank.getPresentPercentOfTime() : "")
                          .append(rank != null ? rank.getProposedRankAndStep() : "")
                          .append(rank != null ? rank.getProposedPercentOfTime() : "");
        }

        return representation.toString().getBytes(StandardCharsets.UTF_8);
    }

    /**
     * Version three for candidate disclosure certificate document representations.
     *
     * @param  dc candidate disclosure certificate
     * @return document representation
     * @author Rick Hendricks
     * @since  v4.8.4
     */
    @Version(3)
    protected byte[] getVersionThree(DCBo dc)
    {
        StringBuilder representation = new StringBuilder();

        /*
         * Add disclosure certificate fields to representation
         */
        representation.append(emailedDateFormat.get().format(getEmailedDate(dc)))
                      .append(Integer.toString(dc.getDocumentId()))
                      .append(Integer.toString(dc.getSchoolId()))
                      .append(Integer.toString(dc.getDepartmentId()))
                      .append(dc.getAdditionalInfo())
                      .append(dc.getChangesAdditions());

        /*
         * Add action fields to representation
         */
        representation.append(dc.getDossier().getAction().getCandidate().getUserId())
                      .append(longDateFormat.get().format(dc.getDossier().getAction().getEffectiveDate()))
                      .append(dc.getDossier().getAction().getActionType().name())
                      .append(dc.getDossier().getAction().getDelegationAuthority().name())
                      .append(dc.getDossier().getAction().getAccelerationYears());

        /*
         * If the new RAF is present, add to the representation the proposed assignment in the same scope.
         */
        if (dc.getDossier().isAcademicActionFormPresent())
        {
            Assignment assignment = MivServiceLocator.getAcademicActionService()
                                                     .getBo(dc.getDossier())
                                                     .getAssignment(StatusType.PROPOSED, dc.getScope());

            representation.append(assignment.getScope())
                          .append(assignment.isPrimary())
                          .append(salaryFormat.get().format(assignment.getPercentOfTime()));

            for (Title title : assignment.getTitles())
            {
                representation.append(title.getCode())
                              .append(title.getDescription())
                              .append(title.getStep().getValue())
                              .append(salaryFormat.get().format(title.getPercentOfTime()))
                              .append(title.getAppointmentDuration().name())
                              .append(title.getSalaryPeriod().name())
                              .append(salaryFormat.get().format(title.getPeriodSalary()))
                              .append(salaryFormat.get().format(title.getAnnualSalary()))
                              .append(title.getYearsAtRank())
                              .append(title.getYearsAtStep());
            }
        }
        /*
         * Otherwise, use the legacy RAF.
         */
        else
        {
            RafBo raf = MivServiceLocator.getRafService().getBo(dc.getDossier());

            // add department in the matching scope
            Department department = raf.getDepartment(dc.getScope());

            representation.append(department.getSchoolId())
                          .append(department.getDepartmentId())
                          .append(department.getDepartmentType())
                          .append(department.getPercentOfTime());

            // add proposed ranks
            for (Rank rank : raf.getStatus(StatusType.PROPOSED).getRanks())
            {
                representation.append(rank.getTitleCode())
                              .append(rank.getRankAndStep())
                              .append(rank.getPercentOfTime())
                              .append(salaryFormat.get().format(rank.getMonthlySalary()))
                              .append(salaryFormat.get().format(rank.getAnnualSalary()));
            }

            representation.append(raf.getAcceleration().getCoefficient() * (raf.getAccelerationYears() + raf.getDecelerationYears()))
                          .append(raf.getYearsAtRank())
                          .append(raf.getYearsAtStep());
        }

        // Get any files uploaded at the department location
        List<File> fileList = MivServiceLocator.getDossierService().getUploadFiles(dc.getDossier(), dc.getScope(), DossierLocation.DEPARTMENT);

        // Compute a hash on the uploaded files and add to the representation
        if (!fileList.isEmpty())
        {
            try
            {
                HashCode hashCode = HashingUtil.computeHash(fileList, DIGEST);
                representation.append(hashCode.toString());
            }
            catch (IOException e)
            {
                throw new MivSevereApplicationError("errors.representation.dc.uploads", e, dc);
            }
        }

        return representation.toString().getBytes(StandardCharsets.UTF_8);
    }

    /**
     * Version three for candidate disclosure certificate document representations.
     *
     * @param  dc candidate disclosure certificate
     * @return document representation
     * @author Rick Hendricks
     * @since  v4.8.5.2
     */
    @Version(4)
    protected byte[] getVersionFour(DCBo dc)
    {
        StringBuilder representation = new StringBuilder();

        /*
         * Add disclosure certificate fields to representation
         */
        representation.append(emailedDateFormat.get().format(getEmailedDate(dc)))
                      .append(Integer.toString(dc.getDocumentId()))
                      .append(Integer.toString(dc.getSchoolId()))
                      .append(Integer.toString(dc.getDepartmentId()))
                      .append(dc.getAdditionalInfo())
                      .append(dc.getChangesAdditions());

        /*
         * Add action fields to representation
         */
        representation.append(dc.getDossier().getAction().getCandidate().getUserId())
                      .append(longDateFormat.get().format(dc.getDossier().getAction().getEffectiveDate()))
                      .append(dc.getDossier().getAction().getActionType().name())
                      .append(dc.getDossier().getAction().getAccelerationYears());

        /*
         * If the new RAF is present, add to the representation the proposed assignment in the same scope.
         */
        if (dc.getDossier().isAcademicActionFormPresent())
        {
            Assignment assignment = MivServiceLocator.getAcademicActionService()
                                                     .getBo(dc.getDossier())
                                                     .getAssignment(StatusType.PROPOSED, dc.getScope());

            representation.append(assignment.getScope())
                          .append(assignment.isPrimary())
                          .append(salaryFormat.get().format(assignment.getPercentOfTime()));

            for (Title title : assignment.getTitles())
            {
                representation.append(title.getCode())
                              .append(title.getDescription())
                              .append(title.getStep().getValue())
                              .append(salaryFormat.get().format(title.getPercentOfTime()))
                              .append(title.getAppointmentDuration().name())
                              .append(title.getSalaryPeriod().name())
                              .append(salaryFormat.get().format(title.getPeriodSalary()))
                              .append(salaryFormat.get().format(title.getAnnualSalary()))
                              .append(title.getYearsAtRank())
                              .append(title.getYearsAtStep());
            }
        }
        /*
         * Otherwise, use the legacy RAF.
         */
        else
        {
            RafBo raf = MivServiceLocator.getRafService().getBo(dc.getDossier());

            // add department in the matching scope
            Department department = raf.getDepartment(dc.getScope());

            representation.append(department.getSchoolId())
                          .append(department.getDepartmentId())
                          .append(department.getDepartmentType())
                          .append(department.getPercentOfTime());

            // add proposed ranks
            for (Rank rank : raf.getStatus(StatusType.PROPOSED).getRanks())
            {
                representation.append(rank.getTitleCode())
                              .append(rank.getRankAndStep())
                              .append(rank.getPercentOfTime())
                              .append(salaryFormat.get().format(rank.getMonthlySalary()))
                              .append(salaryFormat.get().format(rank.getAnnualSalary()));
            }

            representation.append(raf.getAcceleration().getCoefficient() * (raf.getAccelerationYears() + raf.getDecelerationYears()))
                          .append(raf.getYearsAtRank())
                          .append(raf.getYearsAtStep());
        }

        // Get any files uploaded at the department location
        List<File> fileList = MivServiceLocator.getDossierService().getUploadFiles(dc.getDossier(), dc.getScope(), DossierLocation.DEPARTMENT);

        // Compute a hash on the uploaded files and add to the representation
        if (!fileList.isEmpty())
        {
            try
            {
                HashCode hashCode = HashingUtil.computeHash(fileList, DIGEST);
                representation.append(hashCode.toString());
            }
            catch (IOException e)
            {
                throw new MivSevereApplicationError("errors.representation.dc.uploads", e, dc);
            }
        }

        return representation.toString().getBytes(StandardCharsets.UTF_8);
    }



    /**
     * To get the dc Emailed date to the candidate
     * @return Emailed date if it's been emailed otherwise set current date
     */
    private Date getEmailedDate(DCBo dc)
    {
        // get signature for this document
        MivElectronicSignature signature = MivServiceLocator.getSigningService()
                .getSignatureByDocumentAndUser(dc, dc.getDossier().getAction().getCandidate().getUserId());

        return signature != null ? signature.getCreatedDate() : new Date();
    }
}
