/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: ApiAuthorizer.java
 */
package edu.ucdavis.mw.myinfovault.service.apientity;

import java.util.Collection;
import java.util.Set;

import javax.security.auth.login.CredentialException;

import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.mw.myinfovault.service.person.MivRole;

/**
 * Interface for objects which authorize api entities to act on resources with a specified scope.
 *
 * @author japorito
 * @since 5.0
 */
public interface ApiAuthorizer
{
    /**
     * This function checks whether the entity (and in some cases the person) is allowed to act with the
     * specified scope, and also checks that the Authorization header is valid.
     *
     * @param authHeader The HTTP Authorization header provided in the request.
     * @param scopeRequired An API scope which details the scope that the API entity that this authorizer
     *                      authorizes for must have in order to permit this action.
     * @return Whether the entity that this authorizer authorizes for has the requisite scope and roles and whether the auth header is valid.
     * @throws CredentialException When client-supplied credential do not authenticate.
     */
    public Boolean permits(String authHeader,
                           ApiScope scopeRequired) throws CredentialException;

    /**
     * This function checks whether the entity (and in some cases the person) is allowed to act with the
     * specified scope with the specified roles, and also checks that the Authorization header is valid.
     *
     * @param authHeader The HTTP Authorization header provided in the request.
     * @param scopeRequired An API scope which details the scope that the API entity that this authorizer
     *                      authorizes for must have in order to permit this action.
     * @param roles A collection of roles the API entity can have to satisfy the scope.
     * @return Whether the entity that this authorizer authorizes for has the requisite scope and roles and whether the auth header is valid.
     * @throws CredentialException When client-supplied credential do not authenticate.
     */
    public Boolean permits(String authHeader,
                           ApiScope scopeRequired,
                           Collection<MivRole> roles) throws CredentialException;

    /**
     * This function checks whether the entity (and in some cases the person) is allowed to act with at
     * least one of the specified scopes, and also checks that the Authorization header is valid.
     *
     * @param authHeader The HTTP Authorization header provided in the request.
     * @param scopeRequired An API scope which details the scope that the API entity that this authorizer
     *                      authorizes for must have in order to permit this action.
     * @return Whether the entity that this authorizer authorizes for has the requisite scope and roles and whether the auth header is valid.
     * @throws CredentialException When client-supplied credential do not authenticate.
     */
    public boolean permitsOneOf(String authHeader,
                                Set<ApiScope> scopesRequired) throws CredentialException;

    /**
     * This function checks whether the entity (and in some cases the person) is allowed to act with at least
     * one of the specified scopes with the specified roles, and also checks that the Authorization header is valid.
     *
     * @param authHeader The HTTP Authorization header provided in the request.
     * @param scopesRequired A set of API scopes which details the scope that the API entity that this authorizer
     *                       authorizes for must have in order to permit this action.
     * @param roles A collection of roles the API entity can have to satisfy the scope.
     * @return Whether the entity that this authorizer authorizes for has the requisite scope and roles and whether the auth header is valid.
     * @throws CredentialException When client-supplied credential do not authenticate.
     */
    public boolean permitsOneOf(String authHeader,
                                Set<ApiScope> scopesRequired,
                                Collection<MivRole> roles) throws CredentialException;

    /**
     * Get the real MivPerson represented by this API entity.
     *
     * @return The person this authorizer represents or null, if it does not represent a person.
     */
    public MivPerson getRepresentedPerson();

    /**
     * Gets the entity that this authorizer is authorizing for.
     *
     * @return The entity this authorizer authorizes for.
     */
    public ApiEntity getEntity();

    /**
     * Generates and returns an API key which is never stored anywhere.
     * Also generates and sets the Salt and KeyHash fields of the entity the Authorizer
     * authorizes for.
     *
     * @return A newly generated API key.
     */
    public String generateAPIKey();
}
