/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: InternalApiAuthorizer.java
 */
package edu.ucdavis.mw.myinfovault.service.apientity;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.MessageFormat;
import java.util.Calendar;
import java.util.Collection;

import javax.security.auth.login.CredentialException;
import javax.security.auth.login.CredentialExpiredException;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.StringUtils;

import com.Ostermiller.util.Base64;

import edu.ucdavis.mw.myinfovault.service.MivServiceLocator;
import edu.ucdavis.mw.myinfovault.service.person.AssignedRole;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.mw.myinfovault.service.person.MivRole;
import edu.ucdavis.mw.myinfovault.service.person.UserService;
import edu.ucdavis.myinfovault.exception.MivSevereApplicationError;

/**
 * The authorizer used to authorize API access for internal use (i.e. things submitted from webpages in
 * the MIV front end).
 *
 * @author japorito
 * @since 5.0
 */
public class InternalApiAuthorizer extends ApiAuthorizerBase implements ApiAuthorizer
{
    private static UserService userService = MivServiceLocator.getUserService();

    private ApiEntity entity;
    private MivPerson user; //the person to act as.

    private static final String AUTH_PART_SEPARATOR = "/";
    private static final String USER_PASS_SEPARATOR = ":";

    //this is approximate, because of the way that keys are issued,
    //the time is rounded down to the nearest hour, so currently the key
    //is valid for an amount of time between 8 and 9 hours.
    private static final int HOURS_KEY_IS_VALID = 8;

    /**
     * @throws CredentialException
     *
     */
   public InternalApiAuthorizer(String entityName) throws CredentialException
   {
       super(ApiSecurityScheme.INTERNAL);
       //For internal use, the entityName contains "actualEntityName/userIdToActAs"
       String[] entitySpecifier = entityName.split("/");

       if (entitySpecifier.length != 2) {
           //invalid entity specifier for this auth scheme
           throw new CredentialException(INVALID_CREDENTIAL_FORMAT);
       }

       this.entity = this.loadEntity(entitySpecifier[0]);
       //entity name will not have the userId attached unless it is set below.
       this.entity.setEntityName(entityName);

       this.user = userService.getPersonByMivId(Integer.parseInt(entitySpecifier[1]));
   }

    /*
     * Ensures that requested permission is within this.user's scope and this.entity's resource/permission rules.
     */
    @Override
    public Boolean permits(String authHeader, ApiScope scopeRequired) throws CredentialException
    {
        return this.permits(authHeader, scopeRequired, null);
    }

    /* (non-Javadoc)
     *
     * This authorizer checks roles and scopes against the user, and resource/permission against the API entity.
     *
     * @see edu.ucdavis.mw.myinfovault.service.apientity.ApiAuthorizer#permits(java.lang.String, edu.ucdavis.mw.myinfovault.service.apientity.ApiResourceType, edu.ucdavis.mw.myinfovault.service.apientity.ApiPermission, edu.ucdavis.mw.myinfovault.service.apientity.ApiScope, java.util.Collection)
     */
    @Override
    public Boolean permits(String authHeader, ApiScope scopeRequired, Collection<MivRole> roles) throws CredentialException
    {
        //entity does not exist in the database, and so it cannot be used for authentication.
        if (this.entity.getEntityId() == -1) return false;

        String key = this.getKeyFromHeader(authHeader);

        //supplied key is not valid
        if (!this.isKeyValid(key)) throw new CredentialException("Supplied API key invalid.");

        //user does not have a role permitted to perform the action
        if (roles != null && !user.hasRole(roles)) return false;

        //check whether a user has an assigned role in the required roles (if specified)
        //which has appropriate scope
        ApiScope userScope;
        for (AssignedRole role : user.getAssignedRoles())
        {
            if (roles == null || roles.contains(role.getRole()))
            {
                for (ApiScope scope : entity.getScopes(scopeRequired.getResource()))
                {
                    userScope = new ApiScope(scope.getResource(),
                                             role.getScope().getSchool(),
                                             role.getScope().getDepartment(),
                                             user.getUserId(),
                                             scope.getPermissions());

                    if (userScope.containsScope(scopeRequired))
                    {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.apientity.ApiAuthorizer#getKey()
     */
    private String getKey()
    {
        Calendar calendar = Calendar.getInstance();

        calendar.set(Calendar.MILLISECOND, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MINUTE, 0);

        Long hourIssued = calendar.getTime().getTime();

        return this.getKey(hourIssued);
    }

    private String getKey(Long hourIssued)
    {
        String secret = entity.getSalt();

        try
        {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");

            byte[] hashBytes = digest.digest((secret + "/" +
                                              hourIssued.toString() + "/" +
                                              user.getUserId()).getBytes());

            String key = new String(hashBytes) + AUTH_PART_SEPARATOR + hourIssued.toString();

            return key;
        }
        catch (NoSuchAlgorithmException e)
        {
            throw new MivSevereApplicationError("SHA256 doesn't exist! This shouldn't happen.", e);
        }
    }

    /**
     * Get an auth header which will authenticate with this authorizer.
     *
     * @return A valid HTTP Authorization header for use with this authorizer.
     */
    public String getAuthHeader()
    {
        return entity.getSecurityScheme().toString() + " " +
               Base64.encode(entity.getEntityName() + USER_PASS_SEPARATOR + this.getKey());
    }

    private String getKeyFromHeader(String authHeader) throws CredentialException
    {
        // {scheme, base64-encoded(entityname:key)}
        String[] authParts = authHeader.split(" ");

        if (authParts.length != 2) {
            throw new CredentialException(INVALID_CREDENTIAL_FORMAT);
        }
        if (ApiSecurityScheme.valueOf(authParts[0]) != entity.getSecurityScheme()) {
            throw new CredentialException(INVALID_SECURITY_SCHEME);
        }

        // {entityname, key}
        authParts = Base64.decode(authParts[1]).split(USER_PASS_SEPARATOR, 2); //only split at first ":"; key could feasibly have a ":" too

        if (authParts.length != 2) {
            throw new CredentialException(INVALID_CREDENTIAL_FORMAT);
        }
        if (!entity.getEntityName().equals(authParts[0])) {
            throw new CredentialException(MessageFormat.format(INVALID_ENTITY_NAME, authParts[0], entity.getEntityName()));
        }

        return authParts[1];
    }

    //see http://codahale.com/a-lesson-in-timing-attacks/
    private Boolean isEqual(byte[] a, byte[] b) {
        if (a.length != b.length) {
            return false;
        }

        int result = 0;
        for (int i = 0; i < a.length; i++) {
          result |= a[i] ^ b[i];
        }
        return result == 0;
    }

    private Boolean isKeyValid(String key) throws CredentialException
    {
        try
        {
            Long time = Long.parseLong(StringUtils.substringAfterLast(key, AUTH_PART_SEPARATOR));

            Calendar currentTime = Calendar.getInstance();
            Calendar issueTime = Calendar.getInstance();

            issueTime.setTimeInMillis(time);

            String keyHash = StringUtils.substringBeforeLast(key, AUTH_PART_SEPARATOR);

            //invalid if the key is empty
            if (StringUtils.isEmpty(keyHash)) throw new CredentialException("API key empty.");

            //invalid if key issue time is in the future.
            if (issueTime.compareTo(currentTime) == 1) throw new CredentialException("Invalid issue time.");

            //Subtract the hours that the key is valid
            currentTime.add(Calendar.HOUR, -1 * HOURS_KEY_IS_VALID);
            //invalid if key is expired.
            if (issueTime.compareTo(currentTime) == -1) throw new CredentialExpiredException();

            byte[] realKeyBytes = this.getKey(time).getBytes();
            byte[] headerKeyBytes = key.getBytes();

            //invalid if the key doesn't match what was expected.
            return isEqual(realKeyBytes, headerKeyBytes);
        }
        catch (NumberFormatException e)
        {
            throw new CredentialException(e.getMessage());
        }
    }

    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.apientity.ApiAuthorizer#getRepresentedPerson()
     */
    @Override
    public MivPerson getRepresentedPerson()
    {
        return this.user;
    }

    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.apientity.ApiAuthorizer#getEntity()
     */
    @Override
    public ApiEntity getEntity()
    {
        return this.entity;
    }

    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.apientity.ApiAuthorizer#generateAPIKey()
     */
    @Override
    public String generateAPIKey()
    {
        String notUsed = "Not used in this scheme.";
        entity.setKeyHash(notUsed);

        entity.setSalt(RandomStringUtils.randomAlphanumeric(512));

        return notUsed;
    }
}
