/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: DossierArchiveService.java
 */

package edu.ucdavis.mw.myinfovault.service.archive;

import java.io.File;
import java.util.List;
import java.util.Map;

import edu.ucdavis.mw.myinfovault.domain.dossier.Dossier;


/**
 * DossierArchiveService
 *
 * @author rhendric
 * @since MIV 3.1
 */
public interface DossierArchiveService
{

    /**
     * archiveDossier - Archive the input dossier. This includes both the EDMS and MIV archive process.
     * @param dossier dossier to archive
     * @return String - null if successful, otherwise an error message
     */
    public String archiveDossier(Dossier dossier);

    /**
     * archiveDossier - Archive the input dossier, and optionally include the EDMS archive process.
     * @param dossier dossier to archive
     * @param sendToEDMS <code>true</code> to also include the EDMS archive process,
     *  <code>false</code> to only archive in MIV.
     * @return String - null if successful, otherwise an error message
     */
    public String archiveDossier(Dossier dossier, boolean sendToEDMS);

    /**
     * validateEdmsProcessingState - Validate the EDMS processing status as well as checking for the presence
     * of the EDMS directory.
     *
     * EDMS archiving can not proceed if the nightly transfer is in progress.
     * @return String - message if the EDMS archiving cannot be done, otherwise null
     */
    public String validateEdmsProcessingState();

    /**
     * Get map of the files to archive by edmscode for a dossier.
     *
     * @param dossier dossier
     * @return Map of edms codes associated with a list of files for the code
     */
    public Map<String, List<File>> getDocumentsToArchive(Dossier dossier);

}
