/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: ViewSnapshotAuthorizer.java
 */

package edu.ucdavis.mw.myinfovault.service.authorization;

import static edu.ucdavis.mw.myinfovault.service.person.MivRole.CANDIDATE;
import static edu.ucdavis.mw.myinfovault.service.person.MivRole.DEAN;
import static edu.ucdavis.mw.myinfovault.service.person.MivRole.DEPT_CHAIR;
import static edu.ucdavis.mw.myinfovault.service.person.MivRole.DEPT_STAFF;
import static edu.ucdavis.mw.myinfovault.service.person.MivRole.SCHOOL_STAFF;
import static edu.ucdavis.mw.myinfovault.service.person.MivRole.SYS_ADMIN;
import static edu.ucdavis.mw.myinfovault.service.person.MivRole.VICE_PROVOST;
import static edu.ucdavis.mw.myinfovault.service.person.MivRole.VICE_PROVOST_STAFF;

import edu.ucdavis.mw.myinfovault.util.AttributeSet;

import edu.ucdavis.mw.myinfovault.domain.action.DossierLocation;
import edu.ucdavis.mw.myinfovault.service.authorization.AuthorizationService.Qualifier;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.mw.myinfovault.service.person.MivRole;
import edu.ucdavis.mw.myinfovault.service.person.Scope;


/**<p>
 * Authorizes viewing of dossier snapshots by checking that the target snapshot is of a
 * snapshot type within the qualification scope of actor's appointments.</p>
 * <p>Snapshots are currently of two types, Dossier or regular snapshots created at the various workflow
 * locations, and Archive snapshots created when the dossier is archived. An actor of a given role may be
 * allowed to view Dossier snapshots, while not being allowed to view Archive snapshots.</p>
 * <p>The qualification scope varies based on the actors role, for example a department administrator may
 * view any snapshot created at the department location within be same-school and same-department as any of
 * their appointments, while a school administrator may view snapshots created at the department or school
 * location within the same-school (any department is acceptable) as any of their appointments.</p>
 * <p>Requires input of the <code>SCHOOL</code> and <code>DEPARTMENT</code> qualification parameter as well
 * as the SnapshotType and SnapshotLocation in the permissionsDetails parameter.</p>
 *
 * @author rhendric
 * @since MIV 3.5.2
 */
public class ViewSnapshotAuthorizer extends SameScopeAuthorizer implements PermissionAuthorizer
{
    /**
     * General permission check to determine if the actor has the permission to view snapshots.
     * Snapshots are currently of two types, Dossier snapshots and Archive snapshots. A role may be granted permission
     * for one snapshot type, but not the other.
     *
     * @see SameScopeAuthorizer#hasPermission(MivPerson, String, AttributeSet)
     */
    @Override
    public boolean hasPermission(MivPerson person, String permissionName, AttributeSet permissionDetails)
    {
        return person.hasRole(CANDIDATE, DEPT_STAFF, DEPT_CHAIR, SCHOOL_STAFF, DEAN, VICE_PROVOST, VICE_PROVOST_STAFF, SYS_ADMIN);
    }


    /**
     * <p>A department administrator may view a snapshot created at the department locationwithin be same-school and
     * same-department as any of their appointments.
     * A school administrator may view a created at the school location within the same-school (any department is acceptable)
     * as any of their appointments.</p>
     *
     * @see SameScopeAuthorizer#isAuthorized(MivPerson, String, AttributeSet, AttributeSet)
     */
    @Override
    public boolean isAuthorized(MivPerson person, String permissionName, AttributeSet permissionDetails, AttributeSet qualification)
    {
    	boolean authorized = false;

        // Get the snapshot location. Roles are limited to which location's snapshots they may view
        String snapshotLocation = permissionDetails.get("SNAPSHOTLOCATION");

        // If no location qualifier, not authorized
        if (snapshotLocation == null)
        {
            log.warn("Unable to authorize {} to view snapshots: No SNAPSHOTLOCATION supplied in permissionDetails for ViewSnapshotsAuthorizer.", person);
            return authorized;
        }

        DossierLocation location = DossierLocation.mapWorkflowNodeNameToLocation(snapshotLocation);

        // If role has permission, check if authorized based on department and school
        if (this.hasPermission(person, permissionName, permissionDetails))
        {
            Scope scope = new Scope(Integer.parseInt(qualification.get(Qualifier.SCHOOL)),Integer.parseInt(qualification.get(Qualifier.DEPARTMENT)));

            // Vice provost role can see all
            if (person.hasRole(MivRole.VICE_PROVOST_STAFF,MivRole.VICE_PROVOST))
            {
            	//Vice provost role can see all except their own (pimary role is CANDIDATE)
            	if( !(person.getPrimaryRoleType().equals(MivRole.CANDIDATE) && Integer.parseInt(qualification.get(Qualifier.USERID)) == person.getUserId()))
            	{
            		authorized = true;
            	}
            }
            // School staff can see department, school snapshots except their own, qualified by school...can see all departments
            else if (person.hasRole(MivRole.SCHOOL_STAFF, MivRole.DEAN) &&
                (location == DossierLocation.DEPARTMENT ||
                 location == DossierLocation.SCHOOL ||
                 location == DossierLocation.POSTSENATESCHOOL) &&
                (qualification.containsKey(Qualifier.USERID) && person.getUserId() != Integer.parseInt(qualification.get(Qualifier.USERID))) &&        // cannot see their own snapshot
                hasSharedScope(person, scope))
            {
            	authorized = true;
            }
            // Department can see department snapshots , except their own, qualified by school and department
            else if (person.hasRole(MivRole.DEPT_STAFF, MivRole.DEPT_CHAIR) &&
                    location.equals(DossierLocation.DEPARTMENT) &&
                    (qualification.containsKey(Qualifier.USERID) && person.getUserId() != Integer.parseInt(qualification.get(Qualifier.USERID))) &&    // cannot see their own snapshot
                    hasSharedScope(person, scope))
            {
            	authorized = true;
            }
            // Candidate can see their own snapshot created at the PacketRequest location
            else if (person.hasRole(MivRole.CANDIDATE) &&
                    location.equals(DossierLocation.PACKETREQUEST) &&
                    (qualification.containsKey(Qualifier.USERID) &&    // cannot see their own snapshot
                    hasSharedScope(person, scope)))
            {
            	authorized = true;
            }
        }

        return authorized;
    }

}
