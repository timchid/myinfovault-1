/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: PersonDataSource.java
 */

package edu.ucdavis.mw.myinfovault.service.person;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import edu.ucdavis.myinfovault.exception.ServiceUnavailableException;


/**
 * Defines what MIV needs as a data source for person information, and provides basic
 * versions of necessary classes.
 * (Some, maybe the inner interfaces, should be moved out to their own files.)
 * Adapter and Façade patterns.
 *
 * @author Stephen Paulsen
 * @since MIV 4.6
 */
public interface PersonDataSource
{
    /**
     * MIV needs to get person information for a given person by their unique identifier.
     *
     * @param personId A value that uniquely identifies a person.
     * At UC Davis this is the LDAP ucdPersonUUID which (for now) is the <em>"Mothra" ID</em>.
     *
     * @return A Person data transfer object for the given ID,
     * or <code>null</code> if no person with that ID can be found.
     *
     * @throws ServiceUnavailableException
     */
    public PersonDTO getPersonFromId(String personId) throws ServiceUnavailableException;


    /**
     * MIV needs to get person information from a login name
     *
     * @param principalName login name, aka <em>principalName</em> or <em>uid</em>.
     *
     * @return A Person data transfer object for the given login,
     * or <code>null</code> if no person with that login name can be found.
     *
     * @throws ServiceUnavailableException
     */
    public PersonDTO getPersonFromPrincipal(String principalName) throws ServiceUnavailableException;


    /**
     * MIV Adds people by looking for them from their email address
     *
     * @param email an email address in the standard <em>user@domain.tld</em> form.
     *
     * @return A Collection of people associated with the given email address,
     * or an empty Collection if no people were found. (never returns null)
     *
     * @throws ServiceUnavailableException
     */
    public Collection<PersonDTO> getPersonFromEmail(String email) throws ServiceUnavailableException;


    /**
     * Report if the identified Person exists and is active in the data source.
     *
     * @param personId the unique identifier of the Person to test
     *
     * @return true if active, false if inactive or the person doesn't exist.
     *
     * @throws ServiceUnavailableException
     */
    public boolean isActive(String personId) throws ServiceUnavailableException;


    /**
     * Probably don't need this, and can just stick with the DTO.
     * The idea was to be able to pass something to a service like "hey, fill this in for me".<br>
     * Implemenatations should return <tt>this</tt> to allow setter chaining.
     *
     * @author Stephen Paulsen
     */
    public interface PersonSetter
    {
        public PersonSetter setPersonId(String personId);
        public PersonSetter setEmployeeId(String employeeId);
        /**
         * Set the person to <em>active</em> (true) or <em>inactive</em> (false)
         * @param active indicate whether or not the person is active
         * @return this
         */
        public PersonSetter setActive(boolean active);

        public PersonSetter setGivenName(String name);
        public PersonSetter setMiddleName(String name);
        public PersonSetter setSurname(String name);
        public PersonSetter setSuffix(String suffix);
        public PersonSetter setPreferredName(String name);

        public PersonSetter setPrincipals(Iterable<Principal> principals);
        public PersonSetter setEmails(Iterable<EmailAddress> emails);
    }


    /**
     * Passing around basic Person information requires at least this information.
     * An implementor could extend this interface or stick with the basics.
     *
     * @author Stephen Paulsen
     */
    public interface PersonDTO extends PersonSetter
    {
        public String getPersonId();
        public String getEmployeeId();
        public boolean isActive();
        public String getGivenName();
        public String getMiddleName();
        public String getSurname();
        public String getSuffix();
        public String getPreferredName();
        public List<Principal> getPrincipals();
        public List<EmailAddress> getEmails();

        @Override
        public PersonDTO setPersonId(String personId);
        @Override
        public PersonDTO setEmployeeId(String employeeId);
        @Override
        public PersonDTO setActive(boolean active);
        @Override
        public PersonDTO setGivenName(String name);
        @Override
        public PersonDTO setMiddleName(String name);
        @Override
        public PersonDTO setSurname(String name);
        @Override
        public PersonDTO setSuffix(String suffix);
        @Override
        public PersonDTO setPreferredName(String name);
        @Override
        public PersonDTO setPrincipals(Iterable<Principal> principals);
        @Override
        public PersonDTO setEmails(Iterable<EmailAddress> emails);
    }


    /**
     * Provide the basic implementation for the PersonDTO.
     *
     * @author Stephen Paulsen
     */
    public class BasicPersonDTO implements PersonDTO, Serializable
    {
        private static final long serialVersionUID = 201206111353L;

        private String personId;
        private String employeeId;
        private boolean active;

        private String givenName;
        private String middleName;
        private String surname;
        private String suffix;
        private String preferredName;

        private List<Principal> principals;
        private List<EmailAddress> emails;

        /**
         * @return the personId
         */
        @Override
        public String getPersonId()
        {
            return personId;
        }

        /**
         * @return the employeeId
         */
        @Override
        public String getEmployeeId()
        {
            return employeeId;
        }

        /**
         * @return the active state
         */
        @Override
        public boolean isActive()
        {
            return active;
        }

        /**
         * @return the givenName
         */
        @Override
        public String getGivenName()
        {
            return givenName;
        }

        /**
         * @return the middleName
         */
        @Override
        public String getMiddleName()
        {
            return middleName;
        }

        /**
         * @return the surname
         */
        @Override
        public String getSurname()
        {
            return surname;
        }
        /**
         * @return the suffix
         */
        @Override
        public String getSuffix()
        {
            return suffix;
        }

        /**
         * @return the preferred name
         */
        @Override
        public String getPreferredName()
        {
            return preferredName;
        }

        /**
         * @return the principals
         */
        @Override
        public List<Principal> getPrincipals()
        {
            return principals;
        }

        /**
         * @return the emails
         */
        @Override
        public List<EmailAddress> getEmails()
        {
            return emails;
        }

        /**
         * @param personId the personId to set
         */
        @Override
        public PersonDTO setPersonId(String personId)
        {
            this.personId = personId;
            return this;
        }

        /**
         * @param employeeId the employeeId to set
         */
        @Override
        public PersonDTO setEmployeeId(String employeeId)
        {
            this.employeeId = employeeId;
            return this;
        }

        /**
         * @param active the active to set
         */
        @Override
        public PersonDTO setActive(boolean active)
        {
            this.active = active;
            return this;
        }
        /**
         * @param givenName the givenName to set
         */
        @Override
        public PersonDTO setGivenName(String givenName)
        {
            this.givenName = givenName;
            return this;
        }

        /**
         * @param middleName the middleName to set
         */
        @Override
        public PersonDTO setMiddleName(String middleName)
        {
            this.middleName = middleName;
            return this;
        }

        /**
         * @param surname the surname to set
         */
        @Override
        public PersonDTO setSurname(String surname)
        {
            this.surname = surname;
            return this;
        }

        /**
         * @param suffix the suffix to set
         */
        @Override
        public PersonDTO setSuffix(String suffix)
        {
            this.suffix = suffix;
            return this;
        }

        /**
         * @param name the preferred name to set
         */
        @Override
        public PersonDTO setPreferredName(String name)
        {
            this.preferredName = name;
            return this;
        }

        /**
         * @param principals the principals to set
         */
        @Override
        public PersonDTO setPrincipals(Iterable<Principal> principals)
        {
            this.principals = new ArrayList<Principal>();
            for (Principal p : principals) {
                this.principals.add(p);
            }
            return this;
        }

        /**
         * @param emails the emails to set
         */
        @Override
        public PersonDTO setEmails(Iterable<EmailAddress> emails)
        {
            this.emails = new ArrayList<EmailAddress>();
            for (EmailAddress mail : emails) {
                this.emails.add(mail);
            }
            return this;
        }

        /* (non-Javadoc)
         * @see java.lang.Object#toString()
         */
        @Override
        public String toString()
        {
            StringBuilder builder = new StringBuilder();
            builder.append("BasicPersonDTO [personId=").append(personId).append(", employeeId=").append(employeeId)
                    .append(", active=").append(active).append(", givenName=").append(givenName)
                    .append(", middleName=").append(middleName).append(", surname=").append(surname)
                    .append(", suffix=").append(suffix).append(", principals=").append(principals)
                    .append(", emails=").append(emails).append("]");
            return builder.toString();
        }
    }


    public interface EmailAddress
    {
        public String getAddress();
        public boolean isPrimary();
        public boolean isPreferred();
        public boolean isPrivate();
        public boolean isPublic();
    }


    public class BasicEmailAddress implements EmailAddress, Serializable
    {
        private static final long serialVersionUID = 201206111408L;

        private String address;
        private boolean isPrimary;
        private boolean isPreferred;
        private boolean isPublic;
        private boolean isPrivate;

        /**
         * @return the address
         */
        @Override
        public String getAddress()
        {
            return address;
        }

        /**
         * @return the isPrimary
         */
        @Override
        public boolean isPrimary()
        {
            return isPrimary;
        }

        /**
         * @return the isPreferred
         */
        @Override
        public boolean isPreferred()
        {
            return isPreferred;
        }

        /**
         * @return the isPublic
         */
        @Override
        public boolean isPublic()
        {
            return isPublic;
        }

        /**
         * @return the isPrivate
         */
        @Override
        public boolean isPrivate()
        {
            return isPrivate;
        }

        /**
         * @param address the address to set
         */
        public BasicEmailAddress setAddress(String address)
        {
            this.address = address;
            return this;
        }

        /**
         * @param isPrimary the isPrimary to set
         */
        public BasicEmailAddress setPrimary(boolean isPrimary)
        {
            this.isPrimary = isPrimary;
            return this;
        }

        /**
         * @param isPreferred the isPreferred to set
         */
        public BasicEmailAddress setPreferred(boolean isPreferred)
        {
            this.isPreferred = isPreferred;
            return this;
        }

        /**
         * @param isPublic the isPublic to set
         */
        public BasicEmailAddress setPublic(boolean isPublic)
        {
            this.isPublic = isPublic;
            if (isPublic) this.isPrivate = false;
            return this;
        }

        /**
         * @param isPrivate the isPrivate to set
         */
        public BasicEmailAddress setPrivate(boolean isPrivate)
        {
            this.isPrivate = isPrivate;
            if (isPrivate) this.isPublic = false;
            return this;
        }

        @Override
        public String toString()
        {
            StringBuilder builder = new StringBuilder();
            builder.append(this.address);
            if (this.isPrimary) builder.append("(Primary)");
            if (this.isPreferred) builder.append("(Preferred)");
            if (this.isPrivate) builder.append("(Private)");
            return builder.toString();
        }
    }
}
