/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: ScopeFilter.java
 */

package edu.ucdavis.mw.myinfovault.service;

import java.util.Collection;
import java.util.Collections;

import edu.ucdavis.mw.myinfovault.service.person.Scope;

/**
 * Filters out items not within the defined scope(s).
 *
 * @author Craig Gilmore
 * @param <T> item that has a scope
 * @since 4.3
 */
public abstract class ScopeFilter<T> extends SearchFilterAdapter<T>
{
    private final Collection<Scope> scopes;

    /**
     * Filtering by school (any department).
     *
     * @param schoolId ID of the school that a person must have
     */
    public ScopeFilter(int schoolId)
    {
        this(schoolId, Scope.ANY);
    }

    /**
     * Filtering by school and department.
     *
     * @param schoolId ID of the school that an item must have
     * @param departmentId ID of the department that an item must have
     */
    public ScopeFilter(int schoolId, int departmentId)
    {
        this(new Scope(schoolId, departmentId));
    }

    /**
     * Filtering by any school or department.
     */
    public ScopeFilter()
    {
        this(Scope.AnyScope);
    }

    /**
     * Filtering by scope.
     *
     * @param scope scope filtered items must match
     */
    public ScopeFilter(Scope scope)
    {
        this(Collections.singleton(scope));
    }

    /**
     * Filtering by scope(s).
     *
     * @param scopes scopes filtered items must match at least one
     */
    public ScopeFilter(Collection<Scope> scopes)
    {
        this.scopes = scopes;
    }

    /**
     * Get the scope for the given item.
     *
     * @param item item from filter collection
     * @return item scope
     */
    protected abstract Scope getScope(T item);

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.SearchFilter#include(java.lang.Object)
     */
    @Override
    public boolean include(T item)
    {
        for (Scope scope : scopes)
        {
            if (getScope(item).matches(scope)) return true;
        }

        return false;// item matches none of the scopes
    }
}
