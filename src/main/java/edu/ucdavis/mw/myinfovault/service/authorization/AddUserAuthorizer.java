/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: AddUserAuthorizer.java
 */

package edu.ucdavis.mw.myinfovault.service.authorization;

import static edu.ucdavis.mw.myinfovault.service.person.MivRole.DEAN;
import static edu.ucdavis.mw.myinfovault.service.person.MivRole.DEPT_CHAIR;
import static edu.ucdavis.mw.myinfovault.service.person.MivRole.DEPT_STAFF;
import static edu.ucdavis.mw.myinfovault.service.person.MivRole.SCHOOL_STAFF;
import static edu.ucdavis.mw.myinfovault.service.person.MivRole.SENATE_STAFF;
import static edu.ucdavis.mw.myinfovault.service.person.MivRole.SYS_ADMIN;
import static edu.ucdavis.mw.myinfovault.service.person.MivRole.VICE_PROVOST_STAFF;

import java.util.EnumSet;
import java.util.Set;

import edu.ucdavis.mw.myinfovault.util.AttributeSet;

import edu.ucdavis.mw.myinfovault.service.MivServiceLocator;
import edu.ucdavis.mw.myinfovault.service.authorization.AuthorizationService.Qualifier;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.mw.myinfovault.service.person.MivRole;
import edu.ucdavis.mw.myinfovault.service.person.Scope;

/**
 * Handles permission and authorization to add users. Authorizing
 * to add an existing person is to authorize converting them
 * (e.g. an {@link MivRole#APPOINTEE} to a {@link MivRole#CANDIDATE}).
 * Authorizing to add a new person is authorizing that the scope of the
 * newly created person is editable by the
 *
 * @author Craig Gilmore
 * @since MIV 4.8.5
 */
public class AddUserAuthorizer extends SameScopeAuthorizer
{
    /**
     * When adding a user, primary roles of users that may be converted to another role.
     *
     * TODO: Externalize this list so changes can be made without editing code.
     */
    private static final Set<MivRole> convertibleRoles = EnumSet.of(MivRole.APPOINTEE);

    /**
     * Instantiate the add user authorizer.
     */
    public AddUserAuthorizer()
    {
        // permitting only these roles
        super(DEPT_STAFF, DEPT_CHAIR, SCHOOL_STAFF, DEAN, SENATE_STAFF, VICE_PROVOST_STAFF, SYS_ADMIN);
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.authorization.SameScopeAuthorizer#isAuthorized(edu.ucdavis.mw.myinfovault.service.person.MivPerson, java.lang.String, edu.ucdavis.mw.myinfovault.util.AttributeSet, edu.ucdavis.mw.myinfovault.util.AttributeSet)
     */
    @Override
    public boolean isAuthorized(MivPerson person, String permissionName, AttributeSet permissionDetails, AttributeSet qualification)
    {
        if (hasPermission(person, permissionName, permissionDetails))
        {
            String userId = qualification.get(Qualifier.USERID);

            if (userId != null) {
                // get the target user
                MivPerson target = MivServiceLocator.getUserService().getPersonByMivId(
                                       Integer.parseInt(userId));

                /*
                 * The given person is allowed to add the existing target (convert) if:
                 *   - The target's primary role is convertible
                 *   - The target is in a shared scope
                 */
                return convertibleRoles.contains(target.getPrimaryRoleType())
                    && hasSharedScope(person, target);
            }
            else
            {
                Scope targetScope = new Scope(Integer.parseInt(qualification.get(Qualifier.SCHOOL)),
                                              Integer.parseInt(qualification.get(Qualifier.DEPARTMENT)));

                return hasSharedScope(person, targetScope);
            }
        }

        return false;
    }
}
