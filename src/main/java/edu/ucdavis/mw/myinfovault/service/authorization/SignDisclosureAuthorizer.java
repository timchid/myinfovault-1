/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: SignDisclosureAuthorizer.java
 */


package edu.ucdavis.mw.myinfovault.service.authorization;

import edu.ucdavis.mw.myinfovault.util.AttributeSet;

import edu.ucdavis.mw.myinfovault.domain.action.DossierLocation;
import edu.ucdavis.mw.myinfovault.domain.signature.MivElectronicSignature;
import edu.ucdavis.mw.myinfovault.service.MivServiceLocator;
import edu.ucdavis.mw.myinfovault.service.authorization.AuthorizationService.PermissionDetail;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.mw.myinfovault.service.person.MivRole;

/**
 * Handles authorization for attempts to sign candidate disclosure certificates.
 *
 * @author Craig Gilmore
 * @since MIV 4.7
 */
public class SignDisclosureAuthorizer implements PermissionAuthorizer
{
    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.authorization.PermissionAuthorizer#hasPermission(edu.ucdavis.mw.myinfovault.service.person.MivPerson, java.lang.String, edu.ucdavis.mw.myinfovault.util.AttributeSet)
     */
    @Override
    public boolean hasPermission(MivPerson person,
                                 String permissionName,
                                 AttributeSet permissionDetails)
    {
        // target, switched-to person ID
        int targetUserId = Integer.parseInt(permissionDetails.get(PermissionDetail.TARGET_ID));

        /*
         * User must be acting as themselves
         * or an MIV administrator.
         */
        return person.getUserId() == targetUserId
            || person.hasRole(MivRole.VICE_PROVOST_STAFF);
    }



    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.authorization.PermissionAuthorizer#isAuthorized(edu.ucdavis.mw.myinfovault.service.person.MivPerson, java.lang.String, edu.ucdavis.mw.myinfovault.util.AttributeSet, edu.ucdavis.mw.myinfovault.util.AttributeSet)
     */
    @Override
    public boolean isAuthorized(MivPerson person,
                                String permissionName,
                                AttributeSet permissionDetails,
                                AttributeSet qualification)
    {
        // requested signature
        MivElectronicSignature signature = MivServiceLocator.getSigningService().getSignatureById(
                                               Integer.parseInt(permissionDetails.get(PermissionDetail.TARGET_ID)));

        /*
         * User must be the requested signer
         * or an MIV administrator and the dossier must be at the correct office location.
         */
        return signature != null
            && ( signature.getDocument().getDocumentType().getSigningOffice() == DossierLocation.mapLocationToOffice(signature.getDocument().getDossier().getDossierLocation()))
            && ( person.hasRole(MivRole.VICE_PROVOST_STAFF) || signature.getRequestedSigner().equals( Integer.toString(person.getUserId())));
    }
}
