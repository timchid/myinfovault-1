/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: PrincipalImpl.java
 */


package edu.ucdavis.mw.myinfovault.service.person;

import java.io.Serializable;

/**
 * TODO: Add Javadoc
 * @author Stephen Paulsen
 * @since MIV 4.6
 */
public class PrincipalImpl implements Principal, Serializable
{
    private static final long serialVersionUID = 201206111634L;

    private final String principalName;
    private final String principalId;

    public PrincipalImpl(String principalName, String principalId)
    {
        this.principalName = principalName;
        this.principalId = principalId;
    }

    public PrincipalImpl(String principalName)
    {
        this(principalName, null);
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.person.Principal#getPrincipalName()
     */
    @Override
    public String getPrincipalName()
    {
        return this.principalName;
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.person.Principal#getPrincipalId()
     */
    @Override
    public String getPrincipalId()
    {
        return this.principalId;
    }

    @Override
    public String toString()
    {
        return this.principalName + "/" + this.principalId;
    }
}
