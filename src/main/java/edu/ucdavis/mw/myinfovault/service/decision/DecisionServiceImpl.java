/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: DecisionServiceImpl.java
 */

package edu.ucdavis.mw.myinfovault.service.decision;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map.Entry;
import java.util.SortedSet;
import java.util.TreeSet;

import org.apache.commons.lang.StringUtils;
import edu.ucdavis.mw.myinfovault.util.AttributeSet;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.google.common.eventbus.Subscribe;

import edu.ucdavis.mw.myinfovault.dao.decision.DecisionDao;
import edu.ucdavis.mw.myinfovault.domain.action.DossierLocation;
import edu.ucdavis.mw.myinfovault.domain.decision.Decision;
import edu.ucdavis.mw.myinfovault.domain.decision.DecisionType;
import edu.ucdavis.mw.myinfovault.domain.dossier.Dossier;
import edu.ucdavis.mw.myinfovault.domain.signature.MivElectronicSignature;
import edu.ucdavis.mw.myinfovault.domain.signature.Signable;
import edu.ucdavis.mw.myinfovault.dossier.attributes.DossierAppointmentAttributeKey;
import edu.ucdavis.mw.myinfovault.dossier.attributes.DossierAppointmentAttributes;
import edu.ucdavis.mw.myinfovault.events2.DecisionHoldEvent;
import edu.ucdavis.mw.myinfovault.events2.DecisionReleaseEvent;
import edu.ucdavis.mw.myinfovault.events2.DecisionRequestDeleteEvent;
import edu.ucdavis.mw.myinfovault.events2.DecisionRequestEvent;
import edu.ucdavis.mw.myinfovault.events2.DecisionSignatureDeleteEvent;
import edu.ucdavis.mw.myinfovault.events2.DossierEvent;
import edu.ucdavis.mw.myinfovault.events2.DossierReturnEvent;
import edu.ucdavis.mw.myinfovault.events2.EventDispatcher2;
import edu.ucdavis.mw.myinfovault.service.MivServiceLocator;
import edu.ucdavis.mw.myinfovault.service.dossier.WorkflowException;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.mw.myinfovault.service.person.MivRole;
import edu.ucdavis.mw.myinfovault.service.person.RoleScopeFilter;
import edu.ucdavis.mw.myinfovault.service.person.UserService;
import edu.ucdavis.mw.myinfovault.service.search.MivSearchKeys;
import edu.ucdavis.mw.myinfovault.service.signature.SignableServiceImpl;
import edu.ucdavis.mw.myinfovault.service.signature.SignatureStatus;
import edu.ucdavis.mw.myinfovault.service.signature.SigningService;
import edu.ucdavis.mw.myinfovault.util.MivDocument;
import edu.ucdavis.myinfovault.MIVConfig;
import edu.ucdavis.myinfovault.exception.MivSevereApplicationError;

/**
 * Shared class for services that govern the use of {@link Decision} type objects.
 *
 * @author Craig Gilmore
 * @since MIV 4.7
 */
public class DecisionServiceImpl extends SignableServiceImpl<Decision> implements DecisionService
{
    private static final ThreadLocal<DateFormat> sdf =
        new ThreadLocal<DateFormat>() {
            @Override protected DateFormat initialValue() {
                return new SimpleDateFormat("M/d/yyyy h:mm a");
            }
        };

    /**
     * Decision signature form style filename.
     */
    private static String XSLT_FILENAME = MIVConfig.getConfig().getProperty("document-config-location-xslt-decision");

    private final DecisionDao decisionDao;
    private final UserService userService;

    /**
     * Create the decision service bean.
     *
     * @param decisionDao Spring-injected decision DAO
     * @param userService Spring-injected user service
     * @param signingService Spring-injected signing service
     */
    public DecisionServiceImpl(DecisionDao decisionDao,
                               UserService userService,
                               SigningService signingService)
    {
        super(signingService);

        this.decisionDao = decisionDao;
        this.userService = userService;

        EventDispatcher2.getDispatcher().register(this);
    }

    /**
     * Request signatures for the decision.
     *
     * @param decision signable decision
     * @param realUserId MIV user ID of real, logged-in user making request
     * @return Collection<MivElectronicSignature>
     */
    private Collection<MivElectronicSignature> requestSignatures(Decision decision, int realUserId)
    {
        AttributeSet criteria = new AttributeSet();

        criteria.put(MivSearchKeys.Person.MIV_ROLE,
                     String.valueOf(decision.getDocumentType().getAllowedSigner().getRoleId()));

        criteria.put(MivSearchKeys.Person.MIV_ACTIVE_USER, "");

        // filter by school for dean decision signature requests
        List<MivPerson> signers = decision.getDocumentType().getAllowedSigner() == MivRole.DEAN
                                ? userService.findUsers(criteria, new RoleScopeFilter(MivRole.DEAN, decision.getSchoolId()))
                                : userService.findUsers(criteria);

        final StringBuilder msg = new StringBuilder("User " + realUserId + " requested signatures on:" + decision + "\n\n");
        Collection<MivElectronicSignature>signatures = new ArrayList<MivElectronicSignature>();

        msg.append(signers.isEmpty()
                 ? "No signers were found!"
                 : "Requesting signatures for:");

        // Request signature for each signer that met the criteria
        for (MivPerson signer : signers)
        {
            // Exclude the candidate from the signature requests
            if (signer.getUserId() != decision.getDossier().getAction().getCandidate().getUserId())
            {
                msg.append("\n\t* " + signer);
                signatures.add(signingService.requestSignature(decision, signer.getUserId(), realUserId));
            }
        }

        log.info(msg);

        return signatures;
    }

    /**
     * Remove signatures for the decision and delete.
     *
     * @param decision signable decision
     * @return deleted signatures
     */
    private Collection<MivElectronicSignature> removeSignatures(Decision decision)
    {
        // if nothing to remove
        if (decision == null) return Collections.emptySet();

        log.info("Removed signature for:\n" + decision);

        // remove any signature applied to the document
        Collection<MivElectronicSignature> signatures = signingService.deleteSignatures(decision);

        /*
         * The dean final decision and recommendation are mutually exclusive.
         * Only one or the other may exist, not both.
         * Attempt to delete both if either is encountered, just in case.
         */
        if (MivDocument.DEANS_FINAL_DECISION == decision.getDocumentType()
         || MivDocument.DEANS_RECOMMENDATION == decision.getDocumentType())
        {
            deletePdf(decision,
                      MivDocument.DEANS_FINAL_DECISION,
                      MivDocument.DEANS_RECOMMENDATION);
        }
        // else, delete the one PDF for its document type
        else
        {
            deletePdf(decision, decision.getDocumentType());
        }

        // delete the decision
        decisionDao.deleteDecision(decision);

        return signatures;
    }

    /**
     * Delete the PDF for the signable document (checking all given document types).
     *
     * @param document signable document
     * @param documentTypes document types associated with the PDF
     */
    private void deletePdf(Signable document, MivDocument...documentTypes)
    {
        // remove PDFs for the decision if exist
        for (MivDocument documentType : documentTypes)
        {
            File decisionFile = getSignableFile(document, documentType);

            if (decisionFile.exists()) decisionFile.delete();
        }
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.signature.SignableService#createPdf(edu.ucdavis.mw.myinfovault.domain.signature.Signable)
     */
    @Override
    public boolean createPdf(final Decision decision)
    {
        SignatureStatus status = signingService.getSignatureStatus(decision);

        // don't generate a PDF for a decision without a signature or request
        if (SignatureStatus.MISSING == status
         || SignatureStatus.REQUESTED == status) return false;

        Document doc = getDocument();

        doc.appendChild(getDecisionElement(doc, decision));

        File dossierPDFFile = getSignableFile(decision, decision.getDocumentType());

        // Create the PDF
        return createPdf(dossierPDFFile,
                         XSLT_FILENAME,
                         doc).exists();
    }

    /**
     * Get the file for the given signable document for the document type and format.
     *
     * @param document signable document
     * @param type document type
     * @return file for the given parameters
     */
    private File getSignableFile(Signable document, MivDocument type)
    {
        return document.getDossier().getDossierPdf(type,
                                                   document.getSchoolId(),
                                                   document.getDepartmentId());
    }

    /**
     * Creates and returns the decision node.
     *
     * @param doc
     * @param decision
     * @return decision DOM element
     */
    private Element getDecisionElement(final Document doc, final Decision decision)
    {
        // create decision node
        final Element root = doc.createElement("decision");

        if (decision.isContraryToCommittee())
        {
            root.appendChild(getElement(doc, "iscontrary"));
        }

        if (StringUtils.isNotBlank(decision.getComments()))
        {
            root.appendChild(getElement(doc, "comments", decision.getComments()));
        }

        root.appendChild(getElement(doc, "description", decision.getDocumentType().getDescription()));
        root.appendChild(getElement(doc, "candidate",  decision.getDossier().getAction().getCandidate().getDisplayName()));
        root.appendChild(getElement(doc, "action", decision.getDossier().getAction().getFullDescription()));

        root.appendChild(getChoicesElement(doc, decision));

        SignatureStatus status = signingService.getSignatureStatus(decision);

        // signature signed
        if (SignatureStatus.SIGNED == status || SignatureStatus.INVALID == status)
        {
            root.appendChild(getSignatureElement(doc, signingService.getSignedSignatureByDocument(decision)));
        }

        // Log signature result
        log.info("Decision PDF generated:\n" + decision);

        return root;
    }

    /**
     * Creates and returns the decision choices node.
     *
     * @param doc
     * @param decision dean/VP decision
     * @return choices DOM element
     */
    private Element getChoicesElement(final Document doc, final Decision decision)
    {
        // create choices node
        final Element root = doc.createElement("choices");

        for (DecisionType decisionType : decision.getDecisionTypes())
        {
            root.appendChild(getChoiceElement(doc,
                                              decisionType.getDescription(decision.getDocumentType()),
                                              decision.getDecisionType() == decisionType));
        }

        return root;
    }

    /**
     * Creates and returns the choice node.
     *
     * @param doc
     * @param description decision choice description
     * @param chosen if this decision choice is chosen
     * @return choice DOM element
     */
    private Element getChoiceElement(final Document doc,
                                     final String description,
                                     final boolean chosen)
    {
        // create choices node
        final Element root = doc.createElement("choice");

        root.appendChild(getElement(doc, "description", description));

        if (chosen)
        {
            root.appendChild(getElement(doc, "ischosen"));
        }

        return root;
    }

    /**
     * Creates and returns the signature node.
     *
     * @param doc
     * @param signature
     * @return signature DOM element
     */
    private Element getSignatureElement(final Document doc, final MivElectronicSignature signature)
    {
        // Create signature node
        final Element root = doc.createElement("signature");

        // signature signed
        if (signature.isSigned())
        {
            // Get MIV person for this requested signer
            final MivPerson signer = userService.getPersonByMivId(Integer.parseInt(signature.getRequestedSigner()));

            // signer person must exist
            if (signer == null)
            {
                throw new MivSevereApplicationError("errors.signature.getUser", signature.getRequestedSigner());
            }

            //append children
            root.appendChild(getElement(doc, "name", signer.getDisplayName()));
            root.appendChild(getElement(doc, "date", sdf.get().format(signature.getSigningDate())));

            // add invalid signature message?
            if (!signature.isValid())
            {
                root.appendChild(getElement(doc, "invalidmessage", getInvalidSignatureMessage(signature.getDocument().getDocumentType())));
            }
        }

        return root;
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.decision.DecisionService#getDecisions(edu.ucdavis.mw.myinfovault.domain.dossier.Dossier)
     */
    @Override
    public SortedSet<Decision> getDecisions(Dossier dossier)
    {
        SortedSet<Decision> decisions = new TreeSet<>(new DecisionComparator());

        decisions.addAll(getDecisions(dossier, true));

        return decisions;
    }

    /**
     * Get the recommendations/decisions for the given dossier.
     *
     * @param dossier dossier to which the decision belongs
     * @param includeRecommendations if recommendations should be included
     * @return matching decisions/recommendations
     */
    private List<Decision> getDecisions(Dossier dossier, boolean includeRecommendations)
    {
        List<Decision> decisions = decisionDao.getDecisions(dossier);

        return includeRecommendations ? decisions : new RecommendationFilter().apply(decisions);
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.decision.DecisionService#getDecisions(edu.ucdavis.mw.myinfovault.service.person.MivPerson)
     */
    @Override
    public SortedSet<Decision> getDecisions(MivPerson candidate)
    {
        SortedSet<Decision> decisions = new TreeSet<>(new DecisionComparator());

        try
        {
            for (Dossier dossier : dossierService.getAllDossiersByUser(candidate))
            {
                decisions.addAll(getDecisions(dossier, false));
            }
        }
        catch (WorkflowException e)
        {
            throw new MivSevereApplicationError("errors.dossier.candidate", e, candidate);
        }

        return decisions;
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.decision.DecisionService#getDecision(edu.ucdavis.mw.myinfovault.domain.dossier.Dossier, edu.ucdavis.mw.myinfovault.util.MivDocument, int, int)
     */
    @Override
    public Decision getDecision(Dossier dossier,
                                MivDocument documentType,
                                int schoolId,
                                int departmentId)
    {
        return decisionDao.getDecision(dossier, documentType, schoolId, departmentId);
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.decision.DecisionService#getDecision(int)
     */
    @Override
    public Decision getDecision(int documentId)
    {
        return decisionDao.getDecision(documentId);
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.decision.DecisionService#removeDecisions(edu.ucdavis.mw.myinfovault.domain.dossier.Dossier)
     */
    @Override
    public void removeDecisions(Dossier dossier)
    {
        removeDecisions(dossier, new ArrayList<MivDocument>());
    }

    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.decision.DecisionService#removeDecisions(edu.ucdavis.mw.myinfovault.domain.dossier.Dossier, java.util.List)
     */
    @Override
    public void removeDecisions(Dossier dossier, List<MivDocument>excludeDocs)
    {
        for (Decision decision : getDecisions(dossier))
        {
            if (!excludeDocs.contains(decision.getDocumentType()))
            {
                removeSignatures(decision);
            }
        }
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.signature.SignableServiceImpl#persist(edu.ucdavis.mw.myinfovault.domain.signature.Signable, int)
     */
    @Override
    public boolean persist(Decision decision, int realUserId)
    {
        return decisionDao.persistDecision(decision) && createPdf(decision);
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.decision.DecisionService#requestSignatures(edu.ucdavis.mw.myinfovault.domain.dossier.Dossier, edu.ucdavis.mw.myinfovault.util.MivDocument, int, int, edu.ucdavis.mw.myinfovault.service.person.MivPerson)
     */
    @Override
    public Collection<MivElectronicSignature> requestSignatures(Dossier dossier,
                                                                MivDocument documentType,
                                                                int schoolId,
                                                                int departmentId,
                                                                MivPerson realPerson)
    {
        Decision decision = getDecision(dossier,
                                        documentType,
                                        schoolId,
                                        departmentId);

        /*
         * If a decision doesn't yet exist for the
         * dossier and scope at the current location.
         */
        if (decision == null)
        {
            decision =  new Decision(dossier,
                                     documentType,
                                     schoolId,
                                     departmentId);

            // persist the new decision
            persist(decision, realPerson.getUserId());
        }

        return requestSignatures(decision, realPerson.getUserId());
    }

    /**
     * Remove decision and signatures for this parent event's dossier in the given scope and type.
     *
     * @param parent parent event
     * @param documentType document type
     * @param schoolId school ID
     * @param departmentId department ID
     */
    private void removeSignatures(DossierEvent parent,
                                  MivDocument documentType,
                                  int schoolId,
                                  int departmentId)
    {
        for (MivElectronicSignature removed : removeSignatures(getDecision(parent.getDossier(),
                                                                           documentType,
                                                                           schoolId,
                                                                           departmentId)))
        {
            EventDispatcher2.getDispatcher().post(removed.isSigned()
                                                ? new DecisionSignatureDeleteEvent(parent, removed)
                                                : new DecisionRequestDeleteEvent(parent, removed));
        }
    }

    /**
     * Remove all decision signatures acquired at the origin of the dossier return route.
     *
     * @param event dossier return event
     */
    @Subscribe
    public void removeDecisions(DossierReturnEvent event)
    {
        /*
         * Remove signatures acquired at the return event origin unless
         * returning from the DEPARTMENT. Those signatures must be preserved.
         */
        if (event.getRoute().getOrigin() != DossierLocation.DEPARTMENT)
        {
            // dossier appointments
            for (Entry<DossierAppointmentAttributeKey, DossierAppointmentAttributes> appointment : event.getDossier().getAppointmentAttributes().entrySet())
            {
                // attributes for the appointment, location, and action
                for (String attributeName : dossierService.getAttributeNames(event.getRoute().getOrigin(),
                                                                             event.getDossier().getAction().getDelegationAuthority(),
                                                                             event.getDossier().getAction().getActionType(),
                                                                             appointment.getValue().isPrimary()))
                {
                    // document type associated with the attribute name
                    MivDocument documentType = MivDocument.get(attributeName);

                    // if this document type is signable
                    if (documentType.getAllowedSigner() != MivRole.INVALID_ROLE)
                    {
                        removeSignatures(event,
                                         documentType,
                                         appointment.getKey().getSchoolId(),
                                         appointment.getKey().getDepartmentId());
                    }
                }
            }

        }
    }

    /**
     * Remove decision signatures in response to a decision hold.
     *
     * @param event decision hold event
     */
    @Subscribe
    public void removeDecisions(DecisionHoldEvent event)
    {
        for (MivDocument documentType : MivDocument.values())
        {
            if (event.getAttributeName().equalsIgnoreCase(documentType.getReleaseAttributeName()))
            {
                removeSignatures(event,
                                 documentType,
                                 event.getScope().getSchool(),
                                 event.getScope().getDepartment());
            }
        }
    }

    /**
     * Request decision signatures in response to a decision release.
     *
     * @param event decision release event
     */
    @Subscribe
    public void requestSignatures(DecisionReleaseEvent event)
    {
        MivPerson realPerson = MivServiceLocator.getUserService().getPersonByMivId(Integer.parseInt(event.getRealUserId()));

        MivDocument documentType = MivDocument.getForRelease(event.getAttributeName());

        for (MivElectronicSignature requested : requestSignatures(event.getDossier(),
                                                                  documentType,
                                                                  event.getScope().getSchool(),
                                                                  event.getScope().getDepartment(),
                                                                  realPerson))
        {
            EventDispatcher2.getDispatcher().post(new DecisionRequestEvent(event, requested));
        }
    }
}
