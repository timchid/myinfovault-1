package edu.ucdavis.mw.myinfovault.service.workflow;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import edu.ucdavis.mw.myinfovault.domain.action.DossierLocation;
import edu.ucdavis.myinfovault.MIVConfig;

/**
 * This class represents a WorkflowNode in a defined workflow.
 * @author rhendric
 */
public class WorkflowNode implements Serializable
{

    private static final long serialVersionUID = 20121016095600L;

    // The DossierLocation for this node
    private DossierLocation nodeLocation;

    // The workflowNodeName for this node (remnant of Kuali workflow)
    private String workflowNodeName;

    // The workflowNodeDescription for this node (this is what is displayed on pages for the location)
    private String workflowNodeDesc;

    // The workflowNodeStatus
    private String workflowNodeStatus;

    // The WorkflowNodeType (INITIAL, ENROUTE, FINAL)
    private WorkflowNodeType workflowNodeType;

    // A List of the next legal DossierLocatons available from this node location in workflow.
    private List<DossierLocation> nextLocations;

    // A List of the previous legal DossierLocatons available from this node location in workflow.
    // TODO: Not used currently.
    private List<DossierLocation> previousLocations;

    // A List of the next legal WorkflowNodes available from this node location in workflow.
    private List<WorkflowNode> nextWorkflowNodes;

    // A List of the previous legal WorkflowNodes available from this node location in workflow.
    private List<WorkflowNode> previousWorkflowNodes;

    // A Map by Attribute name with the values which must be satisfied to move from the previous workflow node location
    // to this workflow node location.
    private Map <String,List<String>> absoluteLocationPrerequisiteAttributes;

    // A Map by Attribute name with the values for which any one must be satisfied to move from the previous workflow node location
    // to this workflow node location. There may be multiple sets of such optional attributes which will be placed in a the optionalLocationPrerequisiteAttributesList list.
    protected Map <String,List<String>> optionalLocationPrerequisiteAttributes;
    
    // A List of the above optionalLocationPrerequisiteAttributes maps since there may be multiple sets of option attributes. In such cases, at least one attribute from
    // each of the maps must be satisfied  to move from the previous workflow node location to this workflow node location.
    protected List<Map<String, List<String>>> optionalLocationPrerequisiteAttributesList;

    // displayOnlyWhenAvailable indicates that this node will not be displayed unless it is available for routing selection
    private boolean displayOnlyWhenAvailable;

    // Additional text to display with the route to/send to text for this node
    private List<String> additionalRoutingTextList;

    // Additional text to display with the return to text for this node
    private List<String> additionalReturnTextList;

    /**
     * Instantiate a WorkflowNode for a DossierLocation.
     * 
     * @param location dossier location
     */
    protected WorkflowNode(DossierLocation location)
    {
        this.nodeLocation = location;
        this.previousLocations = new ArrayList<DossierLocation>();
        this.nextLocations = new ArrayList<DossierLocation>();
        this.nextWorkflowNodes = new ArrayList<WorkflowNode>();
        this.previousWorkflowNodes = new ArrayList<WorkflowNode>();
        this.absoluteLocationPrerequisiteAttributes = new HashMap<String, List<String>>();
        this.optionalLocationPrerequisiteAttributes = new HashMap<String, List<String>>();
        this.optionalLocationPrerequisiteAttributesList = new ArrayList<Map<String, List<String>>>();
        this.workflowNodeDesc = location.getDescription();
        this.workflowNodeName = location.mapLocationToWorkflowNodeName();
        this.additionalRoutingTextList = loadAdditionalRoutingText(this.workflowNodeName);
        this.additionalReturnTextList = loadAdditionalReturnText(this.workflowNodeName);
    }

    protected void addNextLocation(DossierLocation location)
    {
        if (!nextLocations.contains(location))
        {
            this.nextLocations.add(location);
        }
    }

    protected void addNextWorkflowNode(WorkflowNode node)
    {
        if (node != null && !nextWorkflowNodes.contains(node))
        {
            this.nextWorkflowNodes.add(node);
        }
    }

    protected void addPreviousWorkflowNode(WorkflowNode node)
    {
        if (node != null && !previousWorkflowNodes.contains(node))
        {
            this.previousWorkflowNodes.add(node);
        }
    }

    protected void addPreviousLocation(DossierLocation location)
    {
        if (!previousLocations.contains(location))
        {
            this.previousLocations.add(location);
        }
    }

    public DossierLocation getNodeLocation()
    {
        return nodeLocation;
    }

    public List<WorkflowNode> getNextWorkflowNodes()
    {
        return nextWorkflowNodes;
    }

    protected List<DossierLocation> getNextLocations()
    {
        return nextLocations;
    }

    public List<DossierLocation> getPreviousLocations()
    {
        return previousLocations;
    }

    protected void setWorkflowNodeType(WorkflowNodeType workflowNodeType)
    {
        this.workflowNodeType = workflowNodeType;
    }

    public WorkflowNodeType getWorkflowNodeType()
    {
        return workflowNodeType;
    }

    protected void setWorkflowNodeName(String workflowNodeName)
    {
        this.workflowNodeName = workflowNodeName;
    }

    public String getWorkflowNodeName()
    {
        return workflowNodeName;
    }

    protected void setWorkflowNodeDesc(String workflowNodeDesc)
    {
        this.workflowNodeDesc = workflowNodeDesc;
    }

    public String getWorkflowNodeDesc()
    {
        return workflowNodeDesc;
    }

    public void setWorkflowNodeStatus(String workflowNodeStatus)
    {
        this.workflowNodeStatus = workflowNodeStatus;
    }

    public String getWorkflowNodeStatus()
    {
        return workflowNodeStatus;
    }


    public Map<String, List<String>> getAbsoluteLocationPrerequisiteAttributes()
    {
        return absoluteLocationPrerequisiteAttributes;
    }

    protected void addAbsoluteLocationPrerequisite(String key, String value)
    {
        String [] conditionalValues = value.split(":");
        List<String>valueList = new ArrayList<String>();
        for (String conditionalValue : conditionalValues)
        {
            valueList.add(conditionalValue);
        }
        this.absoluteLocationPrerequisiteAttributes.put(key, valueList);
    }

    public List<Map<String, List<String>>> getOptionalLocationPrerequisiteAttributesList()
    {
        return optionalLocationPrerequisiteAttributesList;
    }
    
    protected void addOptionalLocationPrerequisite(String key, String value)
    {
        String [] conditionalValues = value.split(":");
        List<String>valueList = new ArrayList<String>();
        for (String conditionalValue : conditionalValues)
        {
            valueList.add(conditionalValue);
        }
        this.optionalLocationPrerequisiteAttributes.put(key, valueList);
    }

    protected void addOptionalLocationPrerequisiteMap(Map <String,List<String>> optionalLocationAttributeMap)
    {
        this.optionalLocationPrerequisiteAttributesList.add(optionalLocationAttributeMap);
    }

    
    public boolean isDisplayOnlyWhenAvailable()
    {
        return displayOnlyWhenAvailable;
    }

    public void setDisplayOnlyWhenAvailable(boolean displayOnlyWhenAvailable)
    {
        this.displayOnlyWhenAvailable = displayOnlyWhenAvailable;
    }

    private List<String> loadAdditionalRoutingText(String workflowNodename)
    {
        // Get any additional text to display with the routing node on the open actions page
        return this.loadTextList(MIVConfig.getConfig().getProperty("routing-additionaltext-"+workflowNodename));
    }

    private List<String> loadAdditionalReturnText(String workflowNodename)
    {
        // Get any additional text to display with the return node on the open actions page
        return this.loadTextList(MIVConfig.getConfig().getProperty("return-additionaltext-"+workflowNodename));
    }

    private List<String> loadTextList(String additionalText)
    {
        ArrayList<String> additionalTextList = new ArrayList<String>();
        if (additionalText != null)
        {
            String [] addtionalTextArr = additionalText.split(",");
            if (addtionalTextArr != null)
            {
                for (String additionalString : addtionalTextArr)
                {
                    String property = MIVConfig.getConfig().getProperty("routing-string-"+additionalString);
                    if (property != null)
                    {
                        additionalTextList.add(property);
                    }
                }
            }
        }
        return additionalTextList;
    }

    public List<String> getAdditionalRoutingTextList()
    {
        return additionalRoutingTextList;
    }

    public List<String> getAdditionalReturnTextList()
    {
        return additionalReturnTextList;
    }

}
