/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: AppointmentScope.java
 */
package edu.ucdavis.mw.myinfovault.service.person;

import java.io.Serializable;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import edu.ucdavis.myinfovault.MIVConfig;

/**
 * Wrapper for {@link Scope}, making available school and department descriptions and abbreviations.
 *
 * @author Craig Gilmore
 * @since MIV 4.8
 */
public class AppointmentScope extends Scope implements Comparable<AppointmentScope>, Serializable
{
    private static final long serialVersionUID = 201301301546L;

    /**
     * Delimiter separating school and department descriptions.
     */
    private static final String DESCRIPTION_DELIMITER = " - ";

    private String description = "";
    private String fullDescription = "";
    private String schoolDescription = "";
    private String schoolAbbreviation = "";
    private String departmentDescription = "";
    private String departmentAbbreviation = "";

    /**
     * Wrapper for {@link Scope#Scope(int, int)}.
     *
     * @param schoolId
     * @param departmentId
     */
    public AppointmentScope(int schoolId, int departmentId)
    {
        super(schoolId, departmentId);

        /*
         * Set school and department descriptions and abbreviations.
         */
        if (this.getSchool() > NONE)
        {
            Map<String, String> school = MIVConfig.getConfig().getMap("schools").get(Integer.toString(this.getSchool()));

            // non-wildcard school must exist
            if (school == null) throw new IllegalArgumentException("No school exists for ID: " + getSchool());

            this.schoolDescription = school.get("description");

            String schoolAbbreviation = school.get("abbreviation");
            this.schoolAbbreviation = StringUtils.isNotBlank(schoolAbbreviation) ? schoolAbbreviation : this.schoolDescription;

            this.description = this.schoolDescription;
            this.fullDescription = this.schoolDescription;

            if (this.getDepartment() > NONE)
            {
                Map<String, String> department = MIVConfig.getConfig().getMap("departments").get(this.toString());

                // non-wildcard department must exist
                if (department == null) throw new IllegalArgumentException("No department exists for scope: " + this.toString());

                this.departmentDescription = department.get("description");

                String departmentAbbreviation = department.get("abbreviation");
                this.departmentAbbreviation = StringUtils.isNotBlank(departmentAbbreviation) ? departmentAbbreviation : this.departmentDescription;

                if (StringUtils.isNotBlank(this.departmentDescription))
                {
                    // standard description is abbreviated school and full department description
                    this.description = this.schoolAbbreviation + DESCRIPTION_DELIMITER  + this.departmentDescription;
                    
                    // append department to the general description if available
                    this.fullDescription += DESCRIPTION_DELIMITER  + this.departmentDescription;
                }
            }
        }
    }

    /**
     * Create an appointment scope with an existing scope.
     *
     * @param scope
     */
    public AppointmentScope(Scope scope)
    {
        this(scope.getSchool(), scope.getDepartment());
    }

    /**
     * @return school abbreviation and department description delimited by {@link #DESCRIPTION_DELIMITER}
     */
    public String getDescription()
    {
        return description;
    }

    /**
     * @return school and department descriptions delimited by {@link #DESCRIPTION_DELIMITER}
     */
    public String getFullDescription()
    {
        return fullDescription;
    }

    /**
     * @return school description
     */
    public String getSchoolDescription()
    {
        return schoolDescription;
    }

    /**
     * @return school abbreviation if available, otherwise the school description
     */
    public String getSchoolAbbreviation()
    {
        return schoolAbbreviation;
    }

    /**
     * @return department description
     */
    public String getDepartmentDescription()
    {
        return departmentDescription;
    }

    /**
     * @return department abbreviation if available, otherwise the department description
     */
    public String getDepartmentAbbreviation()
    {
        return departmentAbbreviation;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Comparable#compareTo(java.lang.Object)
     */
    @Override
    public int compareTo(AppointmentScope o)
    {
        int comparison = schoolDescription.compareTo(o.getSchoolDescription());

        /*
         * Return compare of department description
         * if school descriptions are the same.
         */
        return comparison != 0
             ? comparison
             : departmentDescription.compareTo(o.getDepartmentDescription());
    }
}
