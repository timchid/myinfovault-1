/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: NewDossierServiceImpl.java
 */

package edu.ucdavis.mw.myinfovault.service.dossier;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;
import org.springframework.dao.DataAccessException;

import com.google.common.eventbus.Subscribe;

import edu.ucdavis.mw.myinfovault.dao.dossier.DossierDao;
import edu.ucdavis.mw.myinfovault.domain.action.AcademicAction;
import edu.ucdavis.mw.myinfovault.domain.action.DossierActionType;
import edu.ucdavis.mw.myinfovault.domain.action.DossierDelegationAuthority;
import edu.ucdavis.mw.myinfovault.domain.action.DossierLocation;
import edu.ucdavis.mw.myinfovault.domain.decision.Decision;
import edu.ucdavis.mw.myinfovault.domain.disclosurecertificate.DCBo;
import edu.ucdavis.mw.myinfovault.domain.dossier.Dossier;
import edu.ucdavis.mw.myinfovault.domain.packet.PacketRequest;
import edu.ucdavis.mw.myinfovault.domain.signature.MivElectronicSignature;
import edu.ucdavis.mw.myinfovault.domain.signature.SignableDocument;
import edu.ucdavis.mw.myinfovault.dossier.attributes.DossierAppointmentAttributeKey;
import edu.ucdavis.mw.myinfovault.dossier.attributes.DossierAppointmentAttributes;
import edu.ucdavis.mw.myinfovault.dossier.attributes.DossierAttribute;
import edu.ucdavis.mw.myinfovault.dossier.attributes.DossierAttributeStatus;
import edu.ucdavis.mw.myinfovault.dossier.attributes.DossierAttributeType;
import edu.ucdavis.mw.myinfovault.events.DelegationAuthorityChangeEvent;
import edu.ucdavis.mw.myinfovault.events.EventDispatcher;
import edu.ucdavis.mw.myinfovault.events2.AcademicActionEvent;
import edu.ucdavis.mw.myinfovault.events2.DecisionHoldEvent;
import edu.ucdavis.mw.myinfovault.events2.DecisionRequestDeleteEvent;
import edu.ucdavis.mw.myinfovault.events2.DecisionSignatureDeleteEvent;
import edu.ucdavis.mw.myinfovault.events2.DecisionSignatureEvent;
import edu.ucdavis.mw.myinfovault.events2.DossierCancelEvent;
import edu.ucdavis.mw.myinfovault.events2.DossierEvent;
import edu.ucdavis.mw.myinfovault.events2.DossierReturnEvent;
import edu.ucdavis.mw.myinfovault.events2.DossierRouteEvent;
import edu.ucdavis.mw.myinfovault.events2.DossierUploadDeleteEvent;
import edu.ucdavis.mw.myinfovault.events2.EventDispatcher2;
import edu.ucdavis.mw.myinfovault.events2.ExecutiveUserChangeEvent;
import edu.ucdavis.mw.myinfovault.events2.MivEvent;
import edu.ucdavis.mw.myinfovault.service.IdFilter;
import edu.ucdavis.mw.myinfovault.service.MivServiceLocator;
import edu.ucdavis.mw.myinfovault.service.SearchFilter;
import edu.ucdavis.mw.myinfovault.service.action.LocationFilter;
import edu.ucdavis.mw.myinfovault.service.archive.DossierArchiveService;
import edu.ucdavis.mw.myinfovault.service.authorization.AuthorizationService;
import edu.ucdavis.mw.myinfovault.service.authorization.AuthorizationService.Qualifier;
import edu.ucdavis.mw.myinfovault.service.authorization.Permission;
import edu.ucdavis.mw.myinfovault.service.disclosurecertificate.DCService;
import edu.ucdavis.mw.myinfovault.service.dossier.DossierReviewerDto.EntityType;
import edu.ucdavis.mw.myinfovault.service.group.GroupService;
import edu.ucdavis.mw.myinfovault.service.packet.PacketService;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.mw.myinfovault.service.person.MivRole;
import edu.ucdavis.mw.myinfovault.service.person.Scope;
import edu.ucdavis.mw.myinfovault.service.person.UserService;
import edu.ucdavis.mw.myinfovault.service.raf.RafService;
import edu.ucdavis.mw.myinfovault.service.signature.SigningService;
import edu.ucdavis.mw.myinfovault.service.snapshot.SnapshotService;
import edu.ucdavis.mw.myinfovault.service.workflow.WorkflowNode;
import edu.ucdavis.mw.myinfovault.service.workflow.WorkflowService;
import edu.ucdavis.mw.myinfovault.util.AttributeSet;
import edu.ucdavis.mw.myinfovault.util.MivDocument;
import edu.ucdavis.mw.myinfovault.web.spring.search.SearchCriteria;
import edu.ucdavis.mw.myinfovault.web.spring.viewdossierstatus.DossierAppointment;
import edu.ucdavis.myinfovault.MIVConfig;
import edu.ucdavis.myinfovault.document.DocumentFormat;
import edu.ucdavis.myinfovault.document.PDFConcatenator;
import edu.ucdavis.myinfovault.exception.MivSevereApplicationError;



/**
 * Dossier Service Implementation
 *
 * @author rhendric
 * @since MIV 4.6
 */
public class NewDossierServiceImpl implements DossierService
{
    private static Logger logger = LoggerFactory.getLogger(NewDossierServiceImpl.class);
    private static final Marker auditMarker = MarkerFactory.getMarker("|| -----*-----> ");

    private static final int FPC_RECOMMENDATION_DOCUMENT_ID = 19;
    private static final int JOINT_FPC_RECOMMENDATION_DOCUMENT_ID = 27;
    private static final int DEANS_FINAL_DECISION_COMMENTS_DOCUMENT_ID = 45;
    private static final int JOINT_DEANS_RECOMMENDATION_COMMENTS_DOCUMENT_ID = 46;
    private static final int DEANS_RECOMMENDATION_COMMENTS_DOCUMENT_ID = 47;
    private static final int JOINT_DEANS_DECISION_COMMENTS_DOCUMENT_ID = 48;
    private static final int CAP_RECOMMENDATION_DOCUMENT_ID = 22;
    private static final int CAPAC_RECOMMENDATION_DOCUMENT_ID = 29;
    private static final int ASPC_RECOMMENDATION_DOCUMENT_ID = 30;
    private static final int AFPC_RECOMMENDATION_DOCUMENT_ID = 31;
    private static final int JPC_RECOMMENDATION_DOCUMENT_ID = 32;
    private static final int AD_HOC_COMMITTEE_RECOMMENDATION_DOCUMENT_ID = 49;
    private static final int SHADOW_COMMITTEE_RECOMMENDATION_DOCUMENT_ID = 50;

    private DossierDao dossierDao = null;
    private SnapshotService snapshotService = null;
    private UserService userService = null;
    private GroupService groupService = null;
    private WorkflowService workflowService = null;
    private DossierArchiveService dossierArchiveService = null;
    private SigningService signingService = null;
    private PacketService packetService = null;

    /**
     * Create dossier service implementation bean.
     */
    public NewDossierServiceImpl()
    {
        EventDispatcher2.getDispatcher().register(this);
        // During development I want to see if and when this gets created.
        logger.info("A 'NewDossierServiceImpl' has been created");
    }

    /**
     * Spring injected DossierDao bean
     *
     * @param dossierDao
     */
    public void setDossierDao(DossierDao dossierDao)
    {
        this.dossierDao = dossierDao;
    }

    /**
     * Spring injected SnapshotService bean
     *
     * @param snapshotService
     */
    public void setSnapshotService(SnapshotService snapshotService)
    {
        this.snapshotService = snapshotService;
    }

    /**
     * Spring injected UserService bean
     *
     * @param userService
     */
    public void setUserService(UserService userService)
    {
        this.userService = userService;
    }

    /**
     * Spring injected GroupService bean
     *
     * @param groupService
     */
    public void setGroupService(GroupService groupService)
    {
        this.groupService = groupService;
    }

    /**
     * Spring injected workflow service bean
     *
     * @param workflowService
     */
    public void setWorkflowService(WorkflowService workflowService)
    {
        this.workflowService = workflowService;
    }

    /**
     * Spring injected DossierArchiveService bean
     *
     * @param dossierArchiveService
     */
    public void setDossierArchiveService(DossierArchiveService dossierArchiveService)
    {
        this.dossierArchiveService = dossierArchiveService;
    }

    /**
     * @param signingService Spring injected signing service bean.
     */
    public void setSigningService(SigningService signingService)
    {
        this.signingService = signingService;
    }

    /**
     * @param signingService Spring injected signing service bean.
     */
    public void setPacketService(PacketService packetService)
    {
        this.packetService = packetService;
    }


    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.dossier.DossierService#initiateDossierRouting(edu.ucdavis.mw.myinfovault.domain.action.AcademicAction, edu.ucdavis.mw.myinfovault.service.person.MivPerson)
     */
    @Override
    public Dossier initiateDossierRouting(AcademicAction action, MivPerson routingPerson) throws WorkflowException
    {
        return initiateDossierRouting(action, null, routingPerson);
    }


    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.dossier.DossierService#initiateDossierRouting(edu.ucdavis.mw.myinfovault.domain.action.AcademicAction, java.lang.String, edu.ucdavis.mw.myinfovault.service.person.MivPerson)
     */
    @Override
    public Dossier initiateDossierRouting(AcademicAction action, String annotation, MivPerson routingPerson) throws WorkflowException
    {
        // Only one dossier may be in process for a given action type and year
        for (Dossier dossier : this.getDossiersInProcess(action.getCandidate()))
        {
            AcademicAction dossierAction = dossier.getAction();

            //Get the dossier's effective calendar year
            Calendar dossierYear = Calendar.getInstance();
            dossierYear.setTime(dossierAction.getEffectiveDate());

            //Get the action's effective calendar year
            Calendar actionYear = Calendar.getInstance();
            actionYear.setTime(action.getEffectiveDate());

            //Cannot start an action if there is an action of the same type
            //for the same year already in process.
            if (dossierAction.getActionType() == action.getActionType() &&
                dossierYear.get(Calendar.YEAR) == actionYear.get(Calendar.YEAR))
            {
                String msg = "Dossier with action " + dossierAction.getFullDescription() +
                             " is already in process for " + action.getCandidate() + " at the " +
                             dossier.getLocation() +
                             " location. Only one dossier with a given action type/year combination may be in process for a candidate.";
                logger.error(msg);
                throw new WorkflowException(msg);
            }
        }

        // Create the dossier object
        Dossier dossier = new Dossier(action, getNextDossierId(), annotation);
        // The router is the initiator for a new document
        dossier.setRoutingPerson(routingPerson);
        // Set the submitted date to now
        dossier.setSubmittedDate(Calendar.getInstance().getTime());
        // Set the last routed date to now
        dossier.setLastRoutedDate(Calendar.getInstance().getTime());
        // Get the initial workflow node
        WorkflowNode initialWorkflowNode = this.getInitialWorkflowNode(dossier);
        // Get the next nodes...there should only be one
        if (initialWorkflowNode.getNextWorkflowNodes().isEmpty() || initialWorkflowNode.getNextWorkflowNodes().size() > 1)
        {
            String msg = "Invalid next node(s) found from the "+initialWorkflowNode+" initial workflow node...expected 1. "+
                         "Unable to initiate a dossier for "+action.getCandidate()+".";
            logger.error(msg);
            throw new WorkflowException(msg);
        }
        // Set the location of the dossier to the initial workflow node
        dossier.setLocation(initialWorkflowNode.getWorkflowNodeName());

        // Get the target location of the next node following the initial node
        DossierLocation targetLocation = initialWorkflowNode.getNextWorkflowNodes().get(0).getNodeLocation();

        // Load the dossier attributes
        this.loadDossierAttributes(dossier);

        // Make sure there is a valid dossier primary appointment
        if ((dossier.getPrimaryAppointmentKey().getSchoolId() <= 0) ||
            (dossier.getPrimaryAppointmentKey().getDepartmentId() <= 0))
        {
            String msg = "Invalid primary appointment key generated when attempting to initiate dossier for " + action.getCandidate() + "." +
                         " There must be a valid primary appointment in order to initiate a dossier.";
            logger.error(msg);
            throw new WorkflowException(msg);
        }

        return this.routeDossier(dossier, annotation, targetLocation, null);
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.dossier.DossierService#initiateDossierRouting(long, edu.ucdavis.mw.myinfovault.domain.action.AcademicAction, edu.ucdavis.mw.myinfovault.service.person.MivPerson)
     */
    @Override
    public Dossier initiateDossierRouting(long dossierId,
                                          AcademicAction action,
                                          boolean removeUploads,
                                          MivPerson routingPerson) throws WorkflowException
    {
        return initiateDossierRouting(getDossier(dossierId),
                                      action,
                                      removeUploads,
                                      routingPerson);
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.dossier.DossierService#initiateDossierRouting(edu.ucdavis.mw.myinfovault.domain.dossier.Dossier, edu.ucdavis.mw.myinfovault.domain.action.AcademicAction, edu.ucdavis.mw.myinfovault.service.person.MivPerson)
     */
    @Override
    public Dossier initiateDossierRouting(Dossier dossier,
                                          AcademicAction action,
                                          boolean removeUploads,
                                          MivPerson routingPerson) throws WorkflowException
    {
        if (removeUploads)
        {
            // Remove RAF PDF
            File dossierFilePath = dossier.getDossierPdf(MivDocument.AA);
            if (dossierFilePath.exists()) dossierFilePath.delete();

            // Remove Uploads from all locations
            removeUploadsByLocation(dossier);

        }

        // Update these values since they may have changed
        dossier.setAction(action);

        // Reinitialize all attributes by first deleting any existing
        deleteDossierAttributes(dossier);

        // Initialize the appointments for the dossier
        dossier.initializeAppointmentAttributeMap();

        // Reload the newly initialized attributes/appointments
        loadDossierAttributes(dossier);

        // Set the routing person to be the candidate since in this case the router is the initiator
        dossier.setRoutingPerson(routingPerson);
        // Submitted date is updated to the current date and time.
        dossier.setSubmittedDate(new Date());

        // The dossier is already in process in workflow, approve the dossier to move to the next workflow node but
        // only if it is at the initial node, otherwise leave it where it is.
        if (getCurrentWorkflowNode(dossier).getNodeLocation() == workflowService.getInitialNode(dossier).getNodeLocation())
        {
            approveDossier(dossier, null, getNextWorkflowNode(dossier).getNodeLocation(), null);
        }

        return dossier;
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.dossier.DossierService#initiateDossierRouting(long, edu.ucdavis.mw.myinfovault.domain.action.AcademicAction, edu.ucdavis.mw.myinfovault.service.person.MivPerson)
     */
    @Override
    public Dossier initiateDossierRouting(long dossierId,
                                          AcademicAction action,
                                          List<DossierAppointmentAttributeKey> listRemoveUploads,
                                          MivPerson routingPerson) throws WorkflowException
    {
        return initiateDossierRouting(getDossier(dossierId),
                                      action,
                                      listRemoveUploads,
                                      routingPerson);
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.dossier.DossierService#initiateDossierRouting(edu.ucdavis.mw.myinfovault.domain.dossier.Dossier, edu.ucdavis.mw.myinfovault.domain.action.AcademicAction, edu.ucdavis.mw.myinfovault.service.person.MivPerson)
     */
    @Override
    public Dossier initiateDossierRouting(Dossier dossier,
                                          AcademicAction action,
                                          List<DossierAppointmentAttributeKey> listRemoveUploads,
                                          MivPerson routingPerson) throws WorkflowException
    {
        if (!listRemoveUploads.isEmpty())
        {
            // Remove RAF PDF
            File dossierFilePath = dossier.getDossierPdf(MivDocument.AA);
            if (dossierFilePath.exists()) dossierFilePath.delete();

            // Remove Uploads from all locations
            removeUploads(dossier,listRemoveUploads);

        }

        return initiateDossierRouting(dossier,
                action,
                false,
                routingPerson);
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.dossier.DossierService#routeDossier(edu.ucdavis.mw.myinfovault.domain.dossier.Dossier, java.lang.String, edu.ucdavis.mw.myinfovault.service.dossier.DossierLocation, edu.ucdavis.mw.myinfovault.service.dossier.DossierLocation)
     */
    @Override
    public Dossier routeDossier(Dossier dossier, String annotation, DossierLocation targetLocation, DossierLocation currentLocationForValidation) throws WorkflowException
    {
        synchronized (dossier)
        {
            // Validate the current dossier location prior to routing
            this.validateDossierLocation(dossier, targetLocation, currentLocationForValidation);

            if (dossier.getRoutingPerson() == null)
            {
                String msg = "No routing principal id assigned for " + dossier.getDossierId() + ", unable to route dossier.";
                logger.error(msg);
                throw new WorkflowException(msg);
            }

            return updateRoutingLocationAndStatus(dossier, annotation, targetLocation, dossier.getRoutingPerson());

        } // end synchronized
    }

    /**
     * Update the routing location and status for a dossier.
     *
     * @param dossier
     * @param targetLocation
     * @return Dossier object
     * @throws WorkflowException
     */
    private Dossier updateRoutingLocationAndStatus(Dossier dossier, String annotation, DossierLocation targetLocation, MivPerson routingPerson) throws WorkflowException
    {
        String currentLocation = dossier.getLocation();

        // If the targetLocation is a previous location, the dossier is being returned
        boolean isReturn = this.getPreviousWorkflowLocations(dossier).contains(targetLocation);

        // Update the previous locations with the targetLocation on a return
        if (isReturn)
        {
            dossier.updatePreviousLocations(targetLocation.mapLocationToWorkflowNodeName());
            // Set the completed date to null on a return.
            dossier.setCompletedDate(null);

        }
        else
        {
            // Update the previous locations with the current dossier location before setting the new target location
            dossier.updatePreviousLocations(currentLocation);

            // Dossier at READYFORPOSTREVIEWAUDIT and POSTAUDITREVIEW locations are treated as complete
            switch (targetLocation)
            {
                case POSTAUDITREVIEW:
                case READYFORPOSTREVIEWAUDIT:
                    dossier.setCompletedDate(new Date());
                    break;
                default:
                    break;
            }
        }

        // Update the current location with the target location
        dossier.setLocation(targetLocation.mapLocationToWorkflowNodeName());

        // Set the routing code status based on the target location
        switch (targetLocation)
        {
            // At of the Post Audit and Appeal locations, the dossier is considered processed, but not final until the archive
            case POSTAUDITREVIEW:
            case SENATEAPPEAL:
            case FEDERATIONAPPEAL:
            case FEDERATIONSENATEAPPEAL:
            case POSTAPPEALSCHOOL:
            case POSTAPPEALVICEPROVOST:
                dossier.setWorkflowStatus(MivWorkflowConstants.ROUTE_HEADER_PROCESSED_CD);    //  processed
                break;
            // Archive is the final workflow node
            case ARCHIVE:
                dossier.setWorkflowStatus(MivWorkflowConstants.ROUTE_HEADER_FINAL_CD);         // final
                break;
            // The default routing status is enroute
            default:
                dossier.setWorkflowStatus(MivWorkflowConstants.ROUTE_HEADER_ENROUTE_CD);    //  enroute
        }

        // Set the last routed date to now
        dossier.setLastRoutedDate(Calendar.getInstance().getTime());

        // Save the dossier
        this.saveDossier(dossier);

       return this.getDossierAndLoadAllData(dossier.getDossierId());
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.dossier.DossierService#cancelDossier(java.lang.String, java.lang.Long, java.lang.String)
     */
    @Override
    public Dossier cancelDossier(MivPerson cancellingPerson, Long dossierId, String annotation) throws WorkflowException
    {
        if (cancellingPerson == null)
        {
            String msg = "No cancelling person assigned for " + dossierId + ", unable to cancel dossier.";
            logger.error(msg);
            throw new WorkflowException(msg);
        }

        // Get the dossier
        Dossier dossier = this.getDossierAndLoadAllData(dossierId);
        dossier.setRoutingPerson(cancellingPerson);

        // Check the current location. If the document is not at the DEPARTMENT location, do not cancel
        String currentWorkflowNode = dossier.getLocation();
        if (!DossierLocation.DEPARTMENT.mapLocationToWorkflowNodeName().equalsIgnoreCase(currentWorkflowNode))
        {
            String msg = "Dossier " + dossier.getDossierId()+" must be at the Packet Request location to cancel.";
            logger.error(msg);
            throw new WorkflowException(msg);
        }

        // Clean up the database and file system
        this.cleanupDossierRecordsAndFileSystem(dossier);

        // Set the workflow status to canceled
        dossier.setWorkflowStatus(MivWorkflowConstants.ROUTE_HEADER_CANCEL_CD);
        this.saveDossier(dossier);

        logger.info("Dossier id {} cancelled by {}", dossier.getDossierId(), cancellingPerson);

        return dossier;
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.dossier.DossierService#getDossier(long)
     */
    @Override
    public Dossier getDossier(long dossierId) throws WorkflowException
    {
        return dossierDao.getDossier(dossierId);
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.dossier.DossierService#getDossier(java.lang.String)
     */
    @Override
    public Dossier getDossier(String dossierId) throws WorkflowException
    {
        try
        {
            Long dosseirLongId = new Long(dossierId);
            return getDossier(dosseirLongId);
        }
        catch (NumberFormatException nfe)
        {
            String msg = "Invalid dossierID " + dossierId + ". DossierID must be numeric. " + nfe.getLocalizedMessage();
            logger.error(msg);
            throw new WorkflowException(msg, nfe);
        }
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.dossier.DossierService#getDossierAndLoadAllData(long)
     */
    @Override
    public Dossier getDossierAndLoadAllData(long dossierId) throws WorkflowException
    {
        try
        {
            return dossierDao.getDossierAndLoadAllData(dossierId);
        }
        catch (SQLException sqle)
        {
            String msg = "Unable to locate and load all data for dossier " + dossierId + ". SQLException: " + sqle.getLocalizedMessage();
            logger.error(msg);
            throw new WorkflowException(msg, sqle);
        }
    }


    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.dossier.DossierService#getDossiers(edu.ucdavis.mw.myinfovault.service.person.MivPerson)
     */
    @Override
    public List<Dossier> getDossiers(MivPerson person) throws WorkflowException
    {
        try
        {
            List<Long>dossierIds = dossierDao.getAllActiveDossierIds();
            return this.qualifyDossiers(person, dossierIds);
        }
        catch (SQLException sqle)
        {
            String msg = "Unable to find dossiers for " + person + ". SQLException: " + sqle.getLocalizedMessage();
            logger.error(msg);
            throw new WorkflowException(msg, sqle);
        }

    }


    /*
     * (non-Javadoc)
     *
     * @see
     * edu.ucdavis.mw.myinfovault.service.dossier.DossierService#getDossierByUser
     * (edu.ucdavis.mw.myinfovault.service.person.MivPerson,
     * edu.ucdavis.mw.myinfovault.service.SearchFilter)
     */
    @Override
    public List<Dossier> getDossierByUser(MivPerson person, SearchFilter<Dossier> filter) throws WorkflowException
    {

        List<Dossier> dossiers = new ArrayList<Dossier>();
        for (Dossier item : this.getDossierByUser(person))
        {
            if (filter.include(item))
            {
                dossiers.add(item);
            }
        }

        return dossiers;
    }


    /*
     * (non-Javadoc)
     *
     * @see
     * edu.ucdavis.mw.myinfovault.service.dossier.DossierService#getDossierByUser
     * (edu.ucdavis.mw.myinfovault.service.person.MivPerson)
     */
    @Override
    public List<Dossier> getDossierByUser(MivPerson person) throws WorkflowException
    {
        // Only retrieve dossiers initiated for this user
        return this.getDossier(person, true);
    }

    /*
     * (non-Javadoc)
     *
     * @see edu.ucdavis.mw.myinfovault.service.dossier.DossierService#
     * getDossier(edu.ucdavis.mw.myinfovault.service.person.MivPerson, boolean)
     */
    @Override
    public List<Dossier> getDossier(MivPerson person, boolean overrideIllegalStateException) throws WorkflowException,
            IllegalStateException
    {
        // Only retrieve active dossiers initiated by this user
        try
        {
            List<Dossier> dossiers = dossierDao.getActiveDossiers(person);
            // We expect no more than a single result, throw exception if flag is set

            if (dossiers.size() > 1 && !overrideIllegalStateException)
            {
                String msg = "Found " + dossiers.size() + " active dossiers for " + person + "...expected only 1.";
                logger.error(msg);
                throw new IllegalStateException(msg);
            }
            return dossiers;
        }
        catch (SQLException sqle)
        {
            String msg = "Unable get dossiers for principal " + person + ". SQLException: " + sqle.getLocalizedMessage();
            logger.error(msg);
            throw new WorkflowException(msg, sqle);
        }
    }


    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.dossier.DossierService#getAllDossiersByUser(edu.ucdavis.mw.myinfovault.service.person.MivPerson)
     */
    @Override
    public List<Dossier> getAllDossiersByUser(MivPerson candidate) throws WorkflowException
    {
        try
        {
            return dossierDao.getAllDossiers(candidate);
        }
        catch (DataAccessException e)
        {
            throw new WorkflowException("Unable get dossiers for candidate: " + candidate, e);
        }
    }


    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.dossier.DossierService#getDossierAndLoadBaseData(long)
     */
    @Override
    public Dossier getDossierAndLoadBaseData(long dossierId) throws WorkflowException
    {
        return dossierDao.getDossier(dossierId);
    }


    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.dossier.DossierService#loadDossierAttributes(edu.ucdavis.mw.myinfovault.domain.dossier.Dossier)
     */
    @Override
    public Dossier loadDossierAttributes(Dossier dossier) throws WorkflowException
    {
        try
        {
            return dossierDao.loadDossierAttributes(dossier);
        }
        catch (SQLException sqle)
        {
            String msg = "Unable to load dossier attributes for dossier " + dossier.getDossierId() + ". SQLException: "
                    + sqle.getLocalizedMessage();
            logger.error(msg);
            throw new WorkflowException(msg, sqle);
        }
    }


    /*
     * (non-Javadoc)
     *
     * @see edu.ucdavis.mw.myinfovault.service.dossier.DossierService#
     * loadDossierAttributesForLocation
     * (edu.ucdavis.mw.myinfovault.domain.dossier.Dossier,
     * edu.ucdavis.mw.myinfovault.service.dossier.DossierLocation)
     */
    @Override
    public Dossier loadDossierAttributesForLocation(Dossier dossier, DossierLocation location) throws WorkflowException
    {
        try
        {
            return dossierDao.loadDossierAttributesForLocation(dossier, location);
        }
        catch (SQLException sqle)
        {
            String msg = "Unable to load dossier attributes for dossier " + dossier.getDossierId() + " at location " + location
                    + ". SQLException: " + sqle.getLocalizedMessage();
            logger.error(msg);
            throw new WorkflowException(msg, sqle);
        }
    }


    /*
     * (non-Javadoc)
     *
     * @see edu.ucdavis.mw.myinfovault.service.dossier.DossierService#
     * deleteDossierAttributes
     * (edu.ucdavis.mw.myinfovault.domain.dossier.Dossier)
     */
    @Override
    public Dossier deleteDossierAttributes(Dossier dossier) throws WorkflowException
    {
        try
        {
            return dossierDao.deleteDossierAttributes(dossier);
        }
        catch (SQLException sqle)
        {
            String msg = "Unable to delete dossier attributes for dossier " + dossier.getDossierId() + ". SQLException: "
                    + sqle.getLocalizedMessage();
            logger.error(msg);
            throw new WorkflowException(msg, sqle);
        }
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.dossier.DossierService#saveDossier(edu.ucdavis.mw.myinfovault.domain.dossier.Dossier)
     */
    @Override
    public void saveDossier(Dossier dossier) throws WorkflowException
    {
        try
        {
            // Save the dossier
            dossierDao.updateDossier(dossier);
        }
        catch (SQLException sqle)
        {
            String msg = "Unable to save dossier " + dossier.getDossierId() + ". SQLException: " + sqle.getLocalizedMessage();
            logger.error(msg);
            throw new WorkflowException(msg, sqle);
        }
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.dossier.DossierService#getDossiersWithCriteria(java.util.Map)
     */
    @Override
    public List<Dossier> getDossiersWithCriteria(Map<String, String> criteria) throws WorkflowException
    {
        return this.getDossiersWithCriteria(criteria, null);
    }


    /*
     * (non-Javadoc)
     *
     * @see edu.ucdavis.mw.myinfovault.service.dossier.DossierService#
     * getDossiersWithCriteria(java.util.Map,
     * edu.ucdavis.mw.myinfovault.service.SearchFilter)
     */
    @Override
    public List<Dossier> getDossiersWithCriteria(Map<String, String> criteria, SearchFilter<Dossier> filter) throws WorkflowException
    {
        int schoolId = 0;
        int departmentId = 0;
        DossierLocation matchLocation = DossierLocation.UNKNOWN;
        List<Dossier> results = new ArrayList<Dossier>();
        if (criteria == null || criteria.isEmpty())
        {
            throw new IllegalArgumentException("Search criteria are empty or null.");
        }
        if (criteria.containsKey("schoolId")) {
            schoolId = Integer.parseInt(criteria.get("schoolId"));
        }
        if (criteria.containsKey("departmentId")) {
            departmentId = Integer.parseInt(criteria.get("departmentId"));
        }
        if (criteria.containsKey("LOCATION")) {
            matchLocation = DossierLocation.mapWorkflowNodeNameToLocation(criteria.get("LOCATION"));
        }

        Map<String, String> attributeCriteria = new HashMap<String,String>();
        for (String key : criteria.keySet())
        {
            // Use only attribute criteria (remove school, department, location from the criteria list)
            if (!(key.equalsIgnoreCase("schoolId") || key.equalsIgnoreCase("departmentId") || key.equalsIgnoreCase("location"))) {
                attributeCriteria.put(key, criteria.get(key));
            }
        }

        try
        {
            List<Long> dossiers = dossierDao.getDossiersByAttributeCriteria(schoolId, departmentId, matchLocation, attributeCriteria);
            for (long dossierId : dossiers) {
                results.add(dossierDao.getDossier(dossierId));
            }
        }
        catch (SQLException sqle)
        {
            String msg = "Unable to find dossiers for attributes" + attributeCriteria.toString() + ". SQLException: " + sqle.getLocalizedMessage();
            logger.error(msg);
            throw new WorkflowException(msg);
        }
        return results;
    }

    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.dossier.DossierService#returnDossierToPreviousNode(java.lang.String, long, java.lang.String)
     */
    @Override
    public Dossier returnDossierToPreviousNode(MivPerson returningPerson, MivPerson realPerson, long dossierId, String annotation, DossierLocation targetLoction, DossierLocation currentLocationForValidation) throws WorkflowException
    {
        // Find the dossier and set the routing user
        Dossier dossier = getDossierAndSetRoutingPerson(dossierId, returningPerson);
        return returnDossierToPreviousNode(returningPerson, realPerson, dossier, annotation, targetLoction, currentLocationForValidation);
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.dossier.DossierService#returnDossierToPreviousNode(edu.ucdavis.mw.myinfovault.service.person.MivPerson, edu.ucdavis.mw.myinfovault.domain.dossier.Dossier, java.lang.String, edu.ucdavis.mw.myinfovault.service.dossier.DossierLocation, edu.ucdavis.mw.myinfovault.service.dossier.DossierLocation)
     */
    @Override
    public Dossier returnDossierToPreviousNode(MivPerson routingPerson, MivPerson realPerson, Dossier dossier, String annotation, DossierLocation targetLocation, DossierLocation currentLocationForValidation) throws WorkflowException
    {
        // Get the previous workflow node and current location
        WorkflowNode previousWorkflowNode = workflowService.getWorkflowNodeForLocation(dossier, targetLocation);
        WorkflowNode currentWorkflowNode = workflowService.getCurrentWorkflowNode(dossier);

        synchronized (dossier)
        {
            // Get the review and vote attribute for each appointment and reset.
            Map <DossierAppointmentAttributeKey, DossierAppointmentAttributes> appointmentAttributes =
                dossier.getAppointmentAttributes();

            for (DossierAppointmentAttributeKey appointmentAttributeKey : appointmentAttributes.keySet())
            {
                // Reset review and vote attributes
                resetAttributes(dossier,
                                Arrays.asList("review","vote"),
                                appointmentAttributeKey);

                // Also reset the jointAppointmentComplete flag to TRUE when returning the dossier.
                DossierAppointmentAttributes appointmentAttribute = appointmentAttributes.get(appointmentAttributeKey);
                appointmentAttribute.setComplete(true);

                // Reset applicable attributes
                switch (previousWorkflowNode.getNodeLocation())
                {
                    case PACKETREQUEST:
                        // going back to the packet request node, set packet request attribute to open and remove DC and any signatures.
                        dossier.setAttributeStatus("packet_request", DossierAttributeStatus.REQUESTED, appointmentAttributeKey.getSchoolId(), appointmentAttributeKey.getDepartmentId());
                        removeDCAndSignatures(dossier);
                        break;
                    case DEPARTMENT:
                        // going back to the department, delete any packet_request, dean_release attributes
                        resetAttributes(dossier,
                                        Arrays.asList("packet_request","dean_release", "dean_recommendation_release", "joint_dean_release"),
                                        appointmentAttributeKey);
                        break;
                    case SCHOOL:
                        resetAttributes(dossier,
                                        Arrays.asList("vp_release","vp_recommendation_release", "p_recommendation_release","p_tenure_release", "p_release","chancellor_release"),
                                        appointmentAttributeKey);
                        break;
                    case SENATE_OFFICE:
                    case FEDERATION:
                    case SENATEFEDERATION:
                        // Remove dean_release and vp_release attribute for the location we just left
                        resetAttributes(dossier,
                                        Arrays.asList("dean_release",
                                                "dean_recommendation_release",
                                                "joint_dean_release",
                                                "vp_release",
                                                "vp_recommendation_release",
                                                "p_recommendation_release",
                                                "p_tenure_release",
                                                "p_release",
                                                "chancellor_release"),
                                        appointmentAttributeKey);
                        break;
                    case SENATEAPPEAL:
                    case FEDERATIONAPPEAL:
                        // Remove dean_appeal_release and vp_appeal_release attribute for the location we just left
                        resetAttributes(dossier,
                                        Arrays.asList("dean_appeal_release",
                                                "vp_appeal_release",
                                                "vp_appeal_recommendation_release",
                                                "p_appeal_recommendation_release",
                                                "p_tenure_appeal_release",
                                                "p_appeal_release",
                                                "chancellor_appeal_release"),
                                        appointmentAttributeKey);
                        break;
                    default: break;
                }


            }
            // Set the last routed date to now
            dossier.setLastRoutedDate(Calendar.getInstance().getTime());
            // Set the completed date to null on a return.
      //      dossier.setCompletedDate(null);   // This is done below within updateRoutingLocationAndStatus

            /*
             * MIV-3921 - Remove retroactive date if present when the dossier has been returned.
             */
            if (dossier.getAction().getRetroactiveDate() != null)
            {
                dossier.getAction().setRetroactiveDate(null);

                logger.info("+  Removing retroactive date for dossier " + dossier.getDossierId());
            }

            try
            {
                // Validate the current dossier location prior to routing
                this.validateDossierLocation(dossier, targetLocation, currentLocationForValidation);

                // Get the routingPerson for this dossier
                if (routingPerson == null)
                {
                    logger.warn("RoutingPerson for {} is null when attempting to approve dossier {}",
                                dossier.getRoutingPerson(), dossier.getDossierId());
                }

                dossier = this.updateRoutingLocationAndStatus(dossier, annotation, previousWorkflowNode.getNodeLocation(), routingPerson);

                // Delete any snapshots present for the PREVIOUS workflow location. Since we are moving back to the
                // previous node, any snapshots created for that node are no longer valid.
                snapshotService.deleteSnapshots(dossier, previousWorkflowNode.getNodeLocation());

// Signature removal will be handled by the removeSignatures method which subscribes to the DossierReturnEvent
//                /*
//                 * Remove signatures and associated uploads acquired at the current location
//                 * unless returning from the DEPARTMENT. Those signatures and uploads
//                 * must be preserved.
//                 */
//                if (currentWorkflowNode.getNodeLocation() != DossierLocation.DEPARTMENT)
//                {
//                    signingService.removeSignatures(dossier, currentWorkflowNode.getNodeLocation(), realPerson);
//                }


            }
            catch (SQLException sqle)
            {
                String msg = "Unable to remove " + currentWorkflowNode.getWorkflowNodeName() + " snapshots for dossier " + dossier.getDossierId() + " returned to " + previousWorkflowNode.getWorkflowNodeName();
                logger.error(msg, sqle);
            }

            logger.debug("Dossier id {} returned to {} from {} by {}",
                         new Object[] { dossier.getDossierId(), previousWorkflowNode.getWorkflowNodeName(), currentWorkflowNode.getWorkflowNodeName(), routingPerson });


            logger.info("Return action taken on behalf of {} to return dossier {} to the previous node ({} --> {})",
                        new Object[] { routingPerson, dossier.getDossierId(), currentWorkflowNode.getWorkflowNodeName(), previousWorkflowNode.getWorkflowNodeName() });

        }  // end synchronized

        return dossier;
    }

    /**
     * Reset input list of dossier attributes to NULL
     * @param dossier
     * @param attributeList
     * @param appointmentAttributeKey
     */
    private void resetAttributes(Dossier dossier, List<String>attributeList, DossierAppointmentAttributeKey appointmentAttributeKey)
    {
        for (String attributeName : attributeList)
        {
            if (dossier.getAttributeByAppointment(appointmentAttributeKey, attributeName) != null)
            {
                dossier.getAttributeByAppointment(appointmentAttributeKey, attributeName).setAttributeValue(null);
            }
        }
    }


    /*
     * (non-Javadoc)
     *
     * @see
     * edu.ucdavis.mw.myinfovault.service.dossier.DossierService#routeDossierToNode
     * (java.lang.String, long,
     * edu.ucdavis.mw.myinfovault.service.dossier.DossierLocation,
     * java.lang.String)
     */
    @Override
    public Dossier routeDossierToNode(MivPerson returningPerson, long dossierId, DossierLocation targetNodeLocation, String annotation)
            throws WorkflowException
    {
        // Get the destination node(s)
        String targetNodeName = targetNodeLocation.mapLocationToWorkflowNodeName();
        String[] targetNodeList = new String[1];
        targetNodeList[0] = targetNodeName;

        // Get the dossier and the current location
        Dossier dossier = this.getDossier(dossierId);
        String currentLocation = dossier.getLocation();

        // Set the returningPerson
        dossier.setRoutingPerson(returningPerson);
        this.routeDossier(dossier, annotation, targetNodeLocation, null);

        logger.debug("Dossier id {} received blanket approval to {} from {} by {}",
                     new Object[] { dossierId, targetNodeLocation, currentLocation, returningPerson });

        return dossier;
    }


    /*
     * (non-Javadoc)
     *
     * @see edu.ucdavis.mw.myinfovault.service.dossier.DossierService#
     * getDossiersByWorkflowLocation
     * (edu.ucdavis.mw.myinfovault.service.dossier.DossierLocation)
     */
    @Override
    public List<Dossier> getDossiersByWorkflowLocations(MivPerson person, final DossierLocation ...locations) throws WorkflowException
    {
        List<Long>dossierIds = new ArrayList<Long>();
        try
        {
            for (DossierLocation location : locations)
            {
                dossierIds.addAll(dossierDao.getDossiersByWorkflowLocation(location));
            }
        }
        catch (SQLException sqle)
        {
            String msg = "Unable to locate dossiers at locations " + locations + " for " + person + ". SQLException: "
                    + sqle.getLocalizedMessage();
            logger.error(msg);
            throw new WorkflowException(msg, sqle);
        }
        return this.qualifyDossiers(person, dossierIds);
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.dossier.DossierService#getArchivedDossiers
     */
    @Override
    public List<Dossier> getArchivedDossiers(MivPerson person) throws WorkflowException
    {
        try
        {
            List<Long>dossierIdList = dossierDao.getArchivedDossiers();
            return this.qualifyDossiers(person, dossierIdList);

        }
        catch (SQLException sqle)
        {
            String msg = "Unable to locate archived dossiers for " + person + ". SQLException: "
            + sqle.getLocalizedMessage();
            logger.error(msg);
            throw new WorkflowException(msg, sqle);
        }
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.dossier.DossierService#getArchivedDossiers
     */
    @Override
    public List<Dossier> getArchivedDossiersBySchoolAndDepartment(MivPerson person, int school, int dept) throws WorkflowException
    {
        try
        {
            List<Long>dossierIdList = dossierDao.getArchivedDossiersBySchoolAndDepartment(school, dept);
            return this.qualifyDossiers(person, dossierIdList);

        }
        catch (SQLException sqle)
        {
            String msg = "Unable to locate archived dossiers for " + person + ". SQLException: "
            + sqle.getLocalizedMessage();
            logger.error(msg);
            throw new WorkflowException(msg, sqle);
        }
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.dossier.DossierService#getCompletedDossiers
     */
    @Override
    public List<Dossier> getCompletedDossiers(MivPerson person) throws WorkflowException
    {
        try
        {
            List<Long>dossierIdList = dossierDao.getCompletedDossiers();
            return this.qualifyDossiers(person, dossierIdList);
        }
        catch (SQLException sqle)
        {
            String msg = "Unable to locate completed dossiers for " + person + ". SQLException: "
            + sqle.getLocalizedMessage();
            logger.error(msg);
            throw new WorkflowException(msg, sqle);
        }
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.dossier.DossierService#getDossiersForSnapshots
     */
    @Override
    public List<Dossier> getDossiersForSnapshots(MivPerson person) throws WorkflowException
    {
        try
        {
            List<Long>dossierIdList = dossierDao.getDossiersForSnapshots();
            return this.qualifyDossiers(person, dossierIdList);
        }
        catch (SQLException sqle)
        {
            String msg = "Unable to locate dossiers with snapshots for " + person + ". SQLException: "
            + sqle.getLocalizedMessage();
            logger.error(msg);
            throw new WorkflowException(msg, sqle);
        }
    }


    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.dossier.DossierService#getDossiersBySchoolAndDepartment(edu.ucdavis.mw.myinfovault.service.person.MivPerson, int, int)
     */
    @Override
    public List<Dossier> getDossiersBySchoolAndDepartment(MivPerson person, int schoolId, int departmentId) throws WorkflowException
    {
        return this.getDossiersBySchoolDepartmentAndLocation(person, schoolId, departmentId, null);
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.dossier.DossierService#getDossiersBySchoolDepartmentAndLocation(edu.ucdavis.mw.myinfovault.service.person.MivPerson, int, edu.ucdavis.mw.myinfovault.service.dossier.DossierLocation)
     */
    @Override
    public List<Dossier> getDossiersBySchoolDepartmentAndLocation(MivPerson person, int schoolId, int departmentId,
            DossierLocation matchLocation) throws WorkflowException
    {
        List<Long>dossierIds = new ArrayList<Long>();
        try
        {
            if (matchLocation != null)
            {
                dossierIds = dossierDao.getDossiersBySchoolDepartmentAndLocation(schoolId, departmentId, matchLocation);
            }
            else
            {
                dossierIds = dossierDao.getDossiersBySchoolAndDepartment(schoolId, departmentId);
            }
        }
        catch (SQLException sqle)
        {
            String msg = "Unable to locate dossiers at school " + schoolId + ", department " + departmentId + " and location "
                    + matchLocation + ". SQLException: " + sqle.getLocalizedMessage();
            logger.error(msg);
            throw new WorkflowException(msg, sqle);
        }
        return this.qualifyDossiers(person, dossierIds);
    }


    /**
     * Helper method to retrieve a dossier document and set the given user as the routing person.
     *
     * @param dossierId Dossier ID to find in workflow
     * @param actingPerson user set as routing person
     * @return Dossier object
     * @throws WorkflowException
     */
    private Dossier getDossierAndSetRoutingPerson(Long dossierId, MivPerson actingPerson) throws WorkflowException
    {
        return dossierDao.getDossier(dossierId).setRoutingPerson(actingPerson);
    }


    /*
     * (non-Javadoc)
     *
     * @see edu.ucdavis.mw.myinfovault.service.dossier.DossierService#
     * getDossiersInProcessForPrincipal(edu.ucdavis.mw.myinfovault.service.person.MivPerson)
     */
    @Override
    public List<Dossier> getDossiersInProcess(MivPerson person) throws WorkflowException
    {
        // Get the dossiers in process and override the IllegalStateException
        return this.getDossier(person, true);
    }


    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.dossier.DossierService#getDossierReviewers(edu.ucdavis.mw.myinfovault.domain.dossier.Dossier, edu.ucdavis.mw.myinfovault.dossier.attributes.DossierAppointmentAttributeKey)
     */
    @Override
    public List<DossierReviewerDto> getDossierReviewers(Dossier dossier,
                                                       DossierAppointmentAttributeKey appointmentKey) throws WorkflowException
    {
        //get dossier reviewers by dossier, appointment, and current location
        return dossierDao.getDossierReviewersBySchoolAndDepartment(dossier.getDossierId(),
                                                                   appointmentKey.getSchoolId(),
                                                                   appointmentKey.getDepartmentId(),
                                                                   dossier.getLocation());
    }

    /**
     * Filter the DossierReviewerDto list
     * @param filter
     * @param results
     * @return list of filtered DossierReviewerDtos
     */
    protected List<DossierReviewerDto> filterReviewers(SearchFilter<DossierReviewerDto> filter, List<DossierReviewerDto> results)
    {
        if (filter != null)
        {
            List<DossierReviewerDto> reviewer = results;
            results = new LinkedList<DossierReviewerDto>();
            Collection<DossierReviewerDto> c = filter.apply(reviewer);
            results.addAll(c);
        }
        return results;
    }


    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.dossier.DossierService#updateDossierReviewers(edu.ucdavis.mw.myinfovault.service.person.MivPerson, java.util.Set, java.util.Set)
     */
    @Override
    public void updateDossierReviewers(MivPerson actingPerson,
                                       Set<DossierReviewerDto> currentDossierReviewers,
                                       Set<DossierReviewerDto> previousDossierReviewers)
    {
        //insert reviewers that are in the current set, but not in the previous set
        Set<DossierReviewerDto> insertReviewers = new HashSet<DossierReviewerDto>(currentDossierReviewers);
        insertReviewers.removeAll(previousDossierReviewers);

        //remove all reviewers from the insert set that are already in the database
        if (!insertReviewers.isEmpty())
        {
            DossierReviewerDto insertReviewer = insertReviewers.iterator().next();

            List<DossierReviewerDto> persistedDossierReviewers = dossierDao.getDossierReviewersBySchoolAndDepartment(insertReviewer.getDossierId(),
                                                                                                                     insertReviewer.getSchoolId(),
                                                                                                                     insertReviewer.getDepartmentId(),
                                                                                                                     insertReviewer.getReviewLocation());
            insertReviewers.removeAll(persistedDossierReviewers);
        }

        //delete reviewers that are in the previous set, but not in the current set
        Set<DossierReviewerDto> deleteReviewers = new HashSet<DossierReviewerDto>(previousDossierReviewers);
        deleteReviewers.removeAll(currentDossierReviewers);

        //set to hold dossier reviewer DTOs that this hasn't permission to delete
        Set<DossierReviewerDto> deniedReviewers = new HashSet<DossierReviewerDto>();

        for (DossierReviewerDto deleteReviewer : deleteReviewers)
        {
            //group deletion needed only for GROUP type reviewer entities
            if (deleteReviewer.getEntityType() == EntityType.GROUP)
            {
                try
                {
                    //remove reviewer group as is no longer needed
                    groupService.delete(actingPerson,
                                        deleteReviewer.getEntityId());
                }
                catch (SecurityException e)
                {
                    //logger.warn(e.getMessage());

                    //add this DTO to the denied set
                    deniedReviewers.add(deleteReviewer);
                }
            }
        }

        //remove denied reviewers from delete set
        deleteReviewers.removeAll(deniedReviewers);

        //delete/insert review sets
        dossierDao.deleteDossierReviewers(deleteReviewers);
        dossierDao.insertDossierReviewers(insertReviewers);
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.dossier.DossierService#deleteDossierReviewers(long, int, int)
     */
    @Override
    public void deleteDossierReviewers(long dossierId, int schoolId, int departmentId)
    {
        dossierDao.deleteDossierReviewers(dossierId, schoolId, departmentId);
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.dossier.DossierService#findDossiersToReview(int)
     */
    @Override
    public List<DossierReviewerDto> findDossiersToReview(int userId) throws WorkflowException
    {
        List <DossierReviewerDto> dossiersToReview = new ArrayList<DossierReviewerDto> ();

        // Have to check to make sure that the review period is open and that the dossier is
        // at the reviewLocation
        for (DossierReviewerDto dossierReviewer : dossierDao.getDossiersToReview(userId))
        {
            // Get the dossier
            Dossier dossier = this.getDossier(dossierReviewer.getDossierId());

            // MIV-3447
            // See if the owner of this dossier is an active user, if not skip it
            if (!dossier.getAction().getCandidate().isActive())
            {
                continue;
            }

            // Check the review location against the dossiers current location
            if (dossier.getLocation().equalsIgnoreCase(dossierReviewer.getReviewLocation()))
            {
                // The dossier is at the review location. Load the attributes and check for review period to be open.
                this.loadDossierAttributes(dossier);
                DossierAttribute dossierAttribute =
                    dossier.getAttributeByAppointment(new DossierAppointmentAttributeKey(dossierReviewer.getSchoolId(),dossierReviewer.getDepartmentId()),"review");
                if (dossierAttribute != null &&
                    dossierAttribute.getAttributeValue() == DossierAttributeStatus.OPEN)
                {
                    dossiersToReview.add(dossierReviewer);
                }
            }
        }
        return dossiersToReview;
    }


    /*
     * (non-Javadoc)
     *
     * @see edu.ucdavis.mw.myinfovault.service.dossier.DossierService#
     * validateRoutingState(edu.ucdavis.mw.myinfovault.domain.dossier.Dossier)
     */
    @Override
    public List<String> validateRoutingState(Dossier dossier) throws WorkflowException
    {
        // List of route errors
        List<String> routingExceptions = new ArrayList<String>();

        // Load the dossier attribute data if needed
        if (!dossier.isAttributedataLoaded())
        {
            this.loadDossierAttributes(dossier);
        }

        // If the dossier is at the PACKETREQUEST location, no manual routing is available.
        // The dossier may only move to the next location as the result of the packet request being satisfied
        if (workflowService.getCurrentWorkflowNode(dossier).getNodeLocation() == DossierLocation.PACKETREQUEST)
        {
            routingExceptions.add("The packet request for this action has not been satisfied.");
            return routingExceptions;
        }

        // Get the departments map
        Map<String, Map<String, String>> departments = MIVConfig.getConfig().getMap("departments");

        // Iterate through all attributes for the dossier checking each that
        // is applicable to the current location
        Map<DossierAppointmentAttributeKey, DossierAppointmentAttributes> appointmentAttributeMap = dossier.getAppointmentAttributes();
        for (DossierAppointmentAttributeKey appointmentAttributeKey : appointmentAttributeMap.keySet())
        {
            // Get the school/department description
            String schoolDepartmentDescription = departments.get(appointmentAttributeKey.toString()).get("school") + " - "
                    + departments.get(appointmentAttributeKey.toString()).get("description");

            // See if this is a joint appointment and if it has been marked as complete (except at Vice Provost)
            if (!appointmentAttributeMap.get(appointmentAttributeKey).isComplete() &&        // not complete
                    !appointmentAttributeMap.get(appointmentAttributeKey).isPrimary() &&     // not primary
                    !appointmentAttributeMap.get(appointmentAttributeKey).getAttributes().isEmpty() &&     // there must be attributes
                    !DossierLocation.VICEPROVOST.mapLocationToWorkflowNodeName().equalsIgnoreCase(dossier.getLocation()))   // must not be at ViceProvost location
                routingExceptions.add("Joint appointment for " + schoolDepartmentDescription + " has not been marked complete for this location.");

            for (DossierAttribute dossierAttribute : appointmentAttributeMap.get(appointmentAttributeKey).getAttributes().values())
            {
                // Check the dossierAttribute based on it's type
                switch (dossierAttribute.getAttributeType())
                {
                    case DOCUMENT:
                    case DOCUMENTUPLOAD:
                        // See if this attribute is required, skip if not
                        if (!dossierAttribute.isRequired())
                        {
                            continue;
                        }
                        // Get the documents
                        List<DossierFileDto> documents = dossierAttribute.getDocuments();
                        // No documents present, cannot route until required
                        // documents are present
                        if (documents == null || documents.isEmpty())
                        {
                            routingExceptions.add(dossierAttribute.getAttributeDescription() + " for " + schoolDepartmentDescription + " is not present.");
                        }
                        // Check each document to see if it is signable, if
                        // yes it must be signed
                        for (DossierFileDto dossierFile : documents)
                        {
                            // Don't check signable attribute for raf if at department
                            if (dossierAttribute.getAttributeName().startsWith("raf") &&
                                    DossierLocation.DEPARTMENT.mapLocationToWorkflowNodeName().equalsIgnoreCase(dossier.getLocation()))
                            {
                                continue;
                            }
                            if (dossierFile.isSignable() && !dossierFile.isSigned())
                            {
                                routingExceptions.add(dossierAttribute.getAttributeDescription() + " for " + schoolDepartmentDescription + " is not signed.");
                            }
                        }
                        break;

                    case ACTION:
                        // Note that for an action, just because it is not required does not mean that it's ok to skip.
                        // If an action has any value other than null, it becomes required
                        DossierAttributeStatus attributeStatus = dossierAttribute.getAttributeValue();

                        // Status of the Action attribute must be Complete or Closed if there is a status value present or attribute is required
                        if (attributeStatus == DossierAttributeStatus.COMPLETE || attributeStatus == DossierAttributeStatus.CLOSE)
                            // don't have to test for null because we're doing a "==" comparison
                            //if ((attributeStatus != null) && (attributeStatus == DossierAttributeStatus.COMPLETE || attributeStatus == DossierAttributeStatus.CLOSE))
                        {
                            continue;
                        }
                        else if (!dossierAttribute.isRequired() && attributeStatus == null)
                        {
                            continue;
                        }
                        else if (dossierAttribute.isRequired() && (
                                attributeStatus == DossierAttributeStatus.COMPLETE ||
                                attributeStatus == DossierAttributeStatus.CLOSE   ||
                                attributeStatus == DossierAttributeStatus.RELEASE
                                ))
                        {
                            continue;
                        }
                        routingExceptions.add(dossierAttribute.getAttributeDescription() + " for " + schoolDepartmentDescription + " is not complete.");
                        break;
                    default:
                        break;
                }
            }
        }

        return routingExceptions;
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.dossier.DossierService#getUploadFiles(edu.ucdavis.mw.myinfovault.domain.dossier.Dossier)
     */
    @Override
    public List<File> getUploadFiles(Dossier dossier)
    {
        return this.getUploadFiles(dossier, 0, 0);
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.dossier.DossierService#getUploadFiles(edu.ucdavis.mw.myinfovault.domain.dossier.Dossier, edu.ucdavis.mw.myinfovault.service.person.Scope, edu.ucdavis.mw.myinfovault.domain.action.DossierLocation)
     */
    @Override
    public List<File> getUploadFiles(Dossier dossier, Scope scope, DossierLocation location)
    {
        List<File> uploads = new ArrayList<File>();

        // search all uploads for the given dossier and scope
        for (UploadDocumentDto upload : getUploadFileDtoList(dossier,0, 0, scope.getSchool(), scope.getDepartment()))
        {
            // add upload to return list if upload location matches
            if (upload.getUploadLocation() == location)
            {
                uploads.add(upload.getFile());
            }
        }

        return uploads;
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.dossier.DossierService#getUploadFilesByType(edu.ucdavis.mw.myinfovault.domain.dossier.Dossier, int)
     */
    @Override
    public List<File> getUploadFilesByType(Dossier dossier, int documentType)
    {
        return this.getUploadFiles(dossier, documentType, 0);
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.dossier.DossierService#getUploadFilesByTypeAndUser(edu.ucdavis.mw.myinfovault.domain.dossier.Dossier, int, int)
     */
    @Override
    public List<File> getUploadFilesByTypeAndUser(Dossier dossier, int documentType, int userId)
    {
        return this.getUploadFiles(dossier, documentType, userId);
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.dossier.DossierService#getUploadFilesByTypeAndUser(long, int, int)
     */
    @Override
    public List<File> getUploadFilesByTypeAndUser(long dossierId, int documentType, int userId)
    {
        try {
            return this.getUploadFiles(this.getDossier(dossierId), documentType, userId);
        }
        catch (WorkflowException wfe)
        {
            String msg = "Unable to retrieve upload files for dossier " + dossierId;
            logger.error(msg, wfe);
            return new ArrayList<File>();
        }
    }


    /**
     * Gets a list of file uploaded DTOs for the dossier.
     *
     * @param dossier
     * @return file uploaded DTOs
     */
    private List<UploadDocumentDto> getUploadFileDtos(Dossier dossier)
    {
        return getUploadFileDtoByTypeAndUser(dossier, 0, 0);
    }

    /**
     * Gets a list of file uploaded DTOs of a certain type and by a certain user for the dossier.
     *
     * @param dossier
     * @param documentType the document type ID
     * @param userId the MIV user ID document owner
     * @return List of UploadDocumentDtos
     */
    private List<UploadDocumentDto> getUploadFileDtoByTypeAndUser(Dossier dossier, int documentType, int userId)
    {
        return getUploadFileDtoList(dossier, documentType, userId, 0, 0);
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.dossier.DossierService#getUploadFileDtoByTypeUserSchoolAndDepartment(long, int, int, int, int)
     */
    @Override
    public List<UploadDocumentDto> getUploadFileDtoByTypeUserSchoolAndDepartment(long dossierId, int documentType, int userId, int schoolId, int departmentId)
    {
        try {
            return this.getUploadFileDtoList(this.getDossier(dossierId), documentType, userId, schoolId, departmentId);
        }
        catch (WorkflowException wfe)
        {
            String msg = "Unable to retrieve upload files for dossier " + dossierId;
            logger.error(msg, wfe);
            return new ArrayList<UploadDocumentDto>();
        }
    }


    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.dossier.DossierService#deleteUploadFile(edu.ucdavis.mw.myinfovault.domain.dossier.Dossier, int, int, java.lang.String, int, edu.ucdavis.mw.myinfovault.service.person.MivPerson, edu.ucdavis.mw.myinfovault.service.person.MivPerson)
     */
    @Override
    public void deleteUploadFile(Dossier dossier,
                                 int school,
                                 int department,
                                 String attributeName,
                                 int occurrence,
                                 MivPerson realPerson,
                                 MivPerson shadowPerson)
    {
        // get the dossier attribute for the appointment and name
        DossierAttribute dossierAttribute = dossier.getAttributesByAppointment(
                                                        new DossierAppointmentAttributeKey(school, department))
                                                   .getAttribute(attributeName);

        // remove occurrence of this document from the document list
        DossierFileDto dossierFile = dossierAttribute.getDocuments().remove(occurrence);

        // fill the parts of the uploadDocumentDto needed by the delete method
        UploadDocumentDto uploadDocumentDto = new UploadDocumentDto(dossierFile.getFilePath());
        uploadDocumentDto.setDossierId((int) dossier.getDossierId());
        uploadDocumentDto.setUserId(dossier.getAction().getCandidate().getUserId());
        uploadDocumentDto.setId(dossierFile.getRecordId());
        uploadDocumentDto.setUploadLocation(dossier.getDossierLocation());
        uploadDocumentDto.setUploadName(dossierAttribute.getAttributeDescription());
        uploadDocumentDto.setSchoolId(school);
        uploadDocumentDto.setDepartmentId(department);

        /*
         * If there are no more uploaded documents of this
         * type we need to reset the "value" to null.
         */
        if (dossierAttribute.getDocuments().isEmpty())
        {
            dossierAttribute.setAttributeValue(null);
        }

        try
        {
            saveDossier(dossier);
        }
        catch (WorkflowException e)
        {
            logger.warn("setPdfUpload: failed to save the new state in dossier == {} trying to set attributeName == {}",
                        dossier.getDossierId(), dossierAttribute.getAttributeName());
        }

        if (dossierDao.deleteDocumentUpload(dossier, uploadDocumentDto))
        {
            EventDispatcher2.getDispatcher().post(
                    new DossierUploadDeleteEvent(
                        realPerson,
                        dossier,
                        uploadDocumentDto).setShadowPerson(shadowPerson));
        }
    }

    /*
     * Get upload files for a dossier. filter by document type and/or userid. No
     * filtering done on int zero
     */
    private List<File> getUploadFiles(Dossier dossier, int documentType, int userId)
    {
        Map<String, List<UploadDocumentDto>> uploadMap = dossierDao.getDocumentUploadFiles(dossier);

        // create new file list
        List<File> fileList = new ArrayList<File>();

        File file = null;

        // loop through map keys
        for (String key : uploadMap.keySet())
        {
            // filter by doc type
            if (documentType == 0 || Integer.parseInt(key) == documentType)
            {
                // loop through upload doc DTOs for this key
                for (UploadDocumentDto uddto : uploadMap.get(key))
                {
                    // filter by user id
                    if (userId == 0 || uddto.getUserId() == userId)
                    {
                        // get file from dossier directory and uploaded file
                        // name
                        file = uddto.getFile();

                        if (file.exists())
                        {
                            // add file to list
                            fileList.add(file);
                        }
                        else
                        {
                            file.canRead();
                        }
                    }
                }
            }
        }

        return fileList;
    }


    /*
     * Get upload file map for a dossier. filter by document type and/or userid as well as school and department. No
     * filtering done on int zero
     */
    private List<UploadDocumentDto> getUploadFileDtoList(Dossier dossier, int documentType, int userId, int schoolId, int departmentId)
    {

        Map<String, List<UploadDocumentDto>> uploadMap = null;

        // Check to get uploads by school and department
        if (schoolId == 0 && departmentId == 0)
        {
            uploadMap = dossierDao.getDocumentUploadFiles(dossier);
        }
        else
        {
            uploadMap = dossierDao.getDocumentUploadFilesBySchoolAndDepartment(dossier, schoolId, departmentId);
        }

        // create new file list
        List<UploadDocumentDto> fileList = new ArrayList<UploadDocumentDto>();

        File file = null;

        // loop through map keys
        for (String key : uploadMap.keySet())
        {
            // filter by doc type
            if (documentType == 0 || Integer.parseInt(key) == documentType)
            {
                // loop through upload doc DTOs for this key
                for (UploadDocumentDto uddto : uploadMap.get(key))
                {
                    // filter by user id
                    if (userId == 0 || uddto.getUserId() == userId)
                    {
                        // get file from dossier directory and uploaded file
                        // name
                        file = uddto.getFile();

                        if (file.exists())
                        {
                            // add file to list
                            fileList.add(uddto);
                        }
                    }
                }
            }
        }

        return fileList;
    }


    /**
     * @return next dossier ID
     * @throws WorkflowException
     */
    private long getNextDossierId() throws WorkflowException
    {
        try
        {
            return dossierDao.updateNextDossierId();
        }
        catch (SQLException e)
        {
            String msg = "Unable to get the next Dossier ID when attempting to initiate dossier";
            logger.error(msg);
            throw new WorkflowException(msg);
        }

    }


    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.dossier.DossierService#getNextWorkflowNode(long)
     */
    @Override
    public WorkflowNode getNextWorkflowNode(long dossierId) throws WorkflowException
    {
        return this.getNextWorkflowNode(this.getDossier(dossierId));
    }


    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.dossier.DossierService#getPreviousWorkflowNodeName(edu.ucdavis.mw.myinfovault.domain.dossier.Dossier)
     */
    @Override
    public String getPreviousWorkflowNodeName(Dossier dossier) throws WorkflowException
    {
        DossierLocation location = this.getPreviousWorkflowLocation(dossier);
        if (location == null){
            return DossierLocation.UNKNOWN.mapLocationToWorkflowNodeName();
        }
        return location.mapLocationToWorkflowNodeName();
    }


    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.dossier.DossierService#getPreviousWorkflowNode(edu.ucdavis.mw.myinfovault.domain.dossier.Dossier)
     */
    @Override
    public WorkflowNode getPreviousWorkflowNode(Dossier dossier) throws WorkflowException
    {
        DossierLocation location = this.getPreviousWorkflowLocation(dossier);
        return this.getWorkflowNodeForLocation(dossier, location);
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.dossier.DossierService#getWorkflowNodeForLocation(edu.ucdavis.mw.myinfovault.domain.dossier.Dossier, edu.ucdavis.mw.myinfovault.service.dossier.DossierLocation)
     */
    @Override
    public WorkflowNode getWorkflowNodeForLocation(Dossier dossier, DossierLocation location) throws WorkflowException
    {
        return workflowService.getWorkflowNodeForLocation(dossier, location);
    }


    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.dossier.DossierService#getPreviousWorkflowLocation(edu.ucdavis.mw.myinfovault.domain.dossier.Dossier)
     */
    @Override
    public DossierLocation getPreviousWorkflowLocation(Dossier dossier) throws WorkflowException
    {
        // Get the current workflowNode to make sure there is a previous location allowed
        WorkflowNode currentWorkflowNode = this.workflowService.getCurrentWorkflowNode(dossier);
        List <DossierLocation> previousLocationList = currentWorkflowNode.getPreviousLocations();
        // If the previousLocation list has an UNKNOWN location, a previous node is not available
        if (previousLocationList.isEmpty() || previousLocationList.contains(DossierLocation.UNKNOWN))
        {
            return DossierLocation.UNKNOWN;
        }
        // If this workflow node does allow previousLocations, then get the locations through which this dossier has
        // passed and return the most recent location, which will be at the end of the list
        else
        {
            List<DossierLocation>locations = this.getPreviousWorkflowLocations(dossier);
            if (locations.isEmpty()){
                return DossierLocation.UNKNOWN;
            }
            return locations.get(locations.size()-1);
        }
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.dossier.DossierService#getPreviousWorkflowLocations(edu.ucdavis.mw.myinfovault.domain.dossier.Dossier)
     */
    @Override
    public List <DossierLocation> getPreviousWorkflowLocations(Dossier dossier)
    {
        List<DossierLocation>previousLocations = new ArrayList<DossierLocation>();
        for (String location : dossier.getPreviousLocationsList())
        {
            previousLocations.add(DossierLocation.mapWorkflowNodeNameToLocation(location));
        }
        return previousLocations;
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.dossier.DossierService#getNextWorkflowNode(edu.ucdavis.mw.myinfovault.domain.dossier.Dossier)
     */
    @Override
    public WorkflowNode getNextWorkflowNode(Dossier dossier) throws WorkflowException
    {
        List<WorkflowNode>nextWorkflowNodes = this.getNextWorkflowNodes(dossier);
        if (nextWorkflowNodes.size()>1)
        {
            String msg = "Mutiple next nodes to which to route...expected only 1.";
            logger.error(msg);
            throw new WorkflowException(msg);
        }
        return nextWorkflowNodes.get(0);
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.dossier.DossierService#getNextWorkflowNodes(edu.ucdavis.mw.myinfovault.domain.dossier.Dossier)
     */
    @Override
    public List<WorkflowNode> getNextWorkflowNodes(Dossier dossier) throws WorkflowException
    {
        return workflowService.getNextWorkflowNodes(dossier);
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.dossier.DossierService#getCurrentWorkflowNode(edu.ucdavis.mw.myinfovault.domain.dossier.Dossier)
     */
    @Override
    public WorkflowNode getCurrentWorkflowNode(Dossier dossier) throws WorkflowException
    {
        return workflowService.getCurrentWorkflowNode(dossier);
    }




    /**
     * Set the XML content of the dossier workflow document.
     * The current content contains all school/department pairs for this
     * dossier as well as the ActionType and DelegationAuthority.
     * The structure is as follows:
     * <![CDATA[
     * <xmlData>
     *          <schoolDepartment>
     *                  <schoolCode>ID</schoolCode>
     *                  <departmentCode>ID</departmentCode>
     *          </schoolDepartment>
     *       .
     *       .
     *       .
     *          <schoolDepartment>
     *                  <schoolCode>ID</schoolCode>
     *                  <departmentCode>ID</departmentCode>
     *          </schoolDepartment>
     *          <delegationAuthority>DA</delegationAuthority>
     *          <actionType>AT</actionType>
     * </xmlData>
     * ]]>
     * @param dossier
     * @param workflowDocument
     * @deprecated This was for Kuali Workflow; No need for it any more. It's on the Do Not Call list.
     */
    @Deprecated
    public void setDocumentXmlContent(Dossier dossier, String/*WorkflowDocument*/ workflowDocument) //throws WorkflowException
    {
    }

    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.dossier.DossierService#getDossiersByUserNameSearchCriteria(edu.ucdavis.mw.myinfovault.service.person.MivPerson, edu.ucdavis.mw.myinfovault.web.spring.search.SearchCriteria)
     */
    @Override
    public List<Dossier> getDossiersByUserNameSearchCriteria(MivPerson mivPerson, SearchCriteria searchCriteria) throws WorkflowException
    {
        try
        {
            // get a list of dossier ids for the input search criteria
            List<Long>dossierIdList =  dossierDao.findDossiersByUserNameSearchCriteria(searchCriteria);
            // qualify the list of dossiers for the mivPerson requesting the list and return a list of only those dossier objects
            // for which he/she is qualified to view.
            return this.qualifyDossiers(mivPerson, dossierIdList);
        }
        catch (SQLException e)
        {
            String msg = "Unable to complete getDossiersByUserName: "+e.getMessage();
            logger.error(msg);
            throw new WorkflowException(msg);
        }
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.dossier.DossierService#getDossiersByUserNameSearchCriteriaAndLocation(edu.ucdavis.mw.myinfovault.service.person.MivPerson, edu.ucdavis.mw.myinfovault.web.spring.search.SearchCriteria, edu.ucdavis.mw.myinfovault.service.dossier.DossierLocation)
     */
    @Override
    public List<Dossier> getDossiersByUserNameSearchCriteriaAndLocation(MivPerson mivPerson, SearchCriteria searchCriteria, DossierLocation location) throws WorkflowException
    {
        try
        {
            // get a list of dossier ids for the input search criteria
            List<Long>dossierIdList =  dossierDao.findDossiersByUserNameSearchCriteriaAndLocation(searchCriteria,location);
            // qualify the list of dossiers for the mivPerson requesting the list and return a list of only those dossier objects
            // for which he/she is qualified to view.
            return this.qualifyDossiers(mivPerson, dossierIdList);
        }
        catch (SQLException e)
        {
            String msg = "Unable to complete getDossiersByUserName: "+e.getMessage();
            logger.error(msg);
            throw new WorkflowException(msg);
        }
    }


    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.dossier.DossierService#qualifyDossiersByPerson(edu.ucdavis.mw.myinfovault.service.person.MivPerson, java.util.List)
     */
    @Override
    public List<Dossier> qualifyDossiers(MivPerson mivPerson, List<Long> dossiers)
    {

        AuthorizationService authorizationService = MivServiceLocator.getAuthorizationService();
        ArrayList<Dossier> dossierList = new ArrayList<Dossier>();

        for (Long dossierId : dossiers)
        {
            Dossier dossier = null;
            try
            {
                dossier = this.getDossier(dossierId);
                // Skip cancelled dossier
                if ((dossier.getWorkflowStatus() != null) &&
                     dossier.getWorkflowStatus().equals(MivWorkflowConstants.ROUTE_HEADER_CANCEL_CD))
                {
                    continue;
                }
            }
            catch (WorkflowException wfe)
            {
                logger.error("Could not retrieve dossier {} for {} : {}", new Object[] { dossierId, mivPerson, wfe.getMessage() });
                continue;
            }

            List<Map<String, String>> departments = dossier.getDepartments();
            for (Map<String, String> department : departments)
            {
                // School and department qualifiers
                AttributeSet qualification = new AttributeSet();
                qualification.put(Qualifier.DEPARTMENT, department.get("departmentid"));
                qualification.put(Qualifier.SCHOOL, department.get("schoolid"));
                qualification.put(Qualifier.USERID, dossier.getAction().getCandidate().getUserId()+"");

                if (authorizationService.isAuthorized(mivPerson, Permission.VIEW_DOSSIER, null, qualification)) {

                    if (!dossierList.contains(dossier)) {
                        dossierList.add(dossier);
                    }
                    break;
                }
            }
        }
        return dossierList;
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.dossier.DossierService#removeUploadsByLocation(edu.ucdavis.mw.myinfovault.domain.dossier.Dossier, edu.ucdavis.mw.myinfovault.domain.action.DossierLocation[])
     */
    @Override
    public void removeUploadsByLocation(Dossier dossier, DossierLocation ... dossierLocations)
    {
        removeUploadsByLocation(dossier, Collections.<Integer>emptyList(), dossierLocations);
    }

    /**
     * Remove the PDF uploads for the specified dossier at the upload locations specified.
     *
     * @param dossier dossier from which to remove uploads
     * @param excludeList document IDs to exclude from removal
     * @param dossierLocations dossier locations where the uploads were acquired
     */
    private void removeUploadsByLocation(Dossier dossier, List<Integer> excludeList, DossierLocation ... dossierLocations)
    {
        StringBuilder logMsg = new StringBuilder("+  Removing uploads for dossier " + dossier.getDossierId());

        List<DossierLocation> dossierLocationList = Arrays.asList(dossierLocations);

        // Iterate through all uploads for this dossier
        for (UploadDocumentDto uploadDocumentDto : getUploadFileDtos(dossier))
        {
            /*
             * Delete the upload file if BOTH are true:
             *  - its upload location is on the dossier location list or the dossier location list is empty
             *  - it is not on the exclude list
             */
            if ((dossierLocationList.isEmpty() || dossierLocationList.contains(uploadDocumentDto.getUploadLocation()))
             && !excludeList.contains(uploadDocumentDto.getDocumentId()))
            {
                dossierDao.deleteDocumentUpload(dossier, uploadDocumentDto);

                logMsg.append("\n-  Removed upload ");
            }
            else
            {
                logMsg.append("\n-  Skipped removal of ");
            }

            // append the upload document objects #toString()
            logMsg.append(uploadDocumentDto);
        }

        logger.info(logMsg.toString());
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.dossier.DossierService#approveDossier(edu.ucdavis.mw.myinfovault.domain.dossier.Dossier, java.lang.String, edu.ucdavis.mw.myinfovault.service.dossier.DossierLocation)
     */
    @Override
    public Dossier approveDossier(Dossier dossier, String annotation, DossierLocation nextLocation) throws WorkflowException
    {
        return approveDossier(dossier, annotation, nextLocation, null);
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.dossier.DossierService#approveDossier(edu.ucdavis.mw.myinfovault.service.person.MivPerson, long, java.lang.String, edu.ucdavis.mw.myinfovault.domain.action.DossierLocation, edu.ucdavis.mw.myinfovault.domain.action.DossierLocation)
     */
    @Override
    public Dossier approveDossier(MivPerson approvingPerson, long dossierId, String annotation, DossierLocation nextLocation, DossierLocation currentLocationForValidation ) throws WorkflowException
    {
        // Find the dossier and set the approving user
        Dossier dossier = getDossierAndSetRoutingPerson(dossierId, approvingPerson);
        // Approve
        return approveDossier(dossier, annotation, nextLocation, currentLocationForValidation);
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.dossier.DossierService#approveDossier(edu.ucdavis.mw.myinfovault.domain.dossier.Dossier, java.lang.String, edu.ucdavis.mw.myinfovault.service.dossier.DossierLocation, edu.ucdavis.mw.myinfovault.service.dossier.DossierLocation)
     */
    @Override
    public Dossier  approveDossier(Dossier dossier, String annotation, DossierLocation routeToLocation, DossierLocation currentLocationForValidation)
                    throws WorkflowException
    {
        synchronized (dossier)
        {
            // Validate the current dossier location prior to routing
            this.validateDossierLocation(dossier, routeToLocation, currentLocationForValidation);

            // Build a list of routing errors.
            // Not doing this here currently. The calling methods are currently calling the ValidateRoutingState method.
            List<String> routingErrors = new ArrayList<String>();   // this.validateRoutingState(dossier);

            if (routingErrors.isEmpty()) // Will always be empty until validation is done here.
            {

                if (dossier.getRoutingPerson() == null)
                {
                    String msg = "No approving person assigned for " + dossier.getDossierId()+", unable to approve dossier.";
                    logger.error(msg);
                    throw new WorkflowException(msg);
                }

                // Create a snapshot for this dossier at the current location (before routing). Snapshots are only made at the
                // DEPARTMENT, SCHOOL, POSTSENATESCHOOL, VICEPROVOST, POSTSENATEVICEPROVOST and READYFORPOSTREVIEWAUDIT locations
                MivRole snapshotRole = null;
                switch (DossierLocation.mapWorkflowNodeNameToLocation(dossier.getLocation()))
                {
                    case DEPARTMENT:
                        snapshotRole = MivRole.DEPT_STAFF;
                        break;
                    case SCHOOL:
                    case POSTSENATESCHOOL:
                        snapshotRole = MivRole.SCHOOL_STAFF;
                        break;
                    case VICEPROVOST:
                    case POSTSENATEVICEPROVOST:
                        snapshotRole = MivRole.VICE_PROVOST_STAFF;
                        break;
                    case SENATE_OFFICE:
                    case FEDERATION:
                    case SENATEFEDERATION:
                        snapshotRole = MivRole.SENATE_STAFF;
                        break;
                    case READYFORPOSTREVIEWAUDIT:
                        snapshotRole = MivRole.VICE_PROVOST_STAFF;
                        break;
                    case POSTAUDITREVIEW:
                    case POSTAPPEALSCHOOL:
                    case POSTAPPEALVICEPROVOST:
                        snapshotRole = MivRole.VICE_PROVOST_STAFF;
                        if (routeToLocation == DossierLocation.ARCHIVE)
                        {
                            String msg = this.dossierArchiveService.archiveDossier(dossier);
                            if (msg != null)
                            {
                               logger.error(msg);
                               throw new WorkflowException(msg);
                            }
                            return this.getDossier(dossier.getDossierId());
                        }
                        break;
                    default:
                        break;
                }

                if (snapshotRole != null)
                {
                    // Create a snapshot for each appointment where needed
                    // Create a snapshot for each appointment
                    Map <DossierAppointmentAttributeKey,DossierAppointmentAttributes> appointments =
                        dossier.getAppointmentAttributes();
                    SchoolDepartmentCriteria schoolDepartmentCriteria = null;
                    Map<String, SchoolDepartmentCriteria> schoolDepartmentCriteriaMap = new HashMap<String, SchoolDepartmentCriteria>();

                    switch (snapshotRole)
                    {
                        // VICE_PROVOST_STAFF/SENATE_STAFF sees all documents. Only a single snapshot is needed
                        case VICE_PROVOST_STAFF:
                        case SENATE_STAFF:
                            snapshotService.createDossierSnapshot(dossier, snapshotRole, schoolDepartmentCriteriaMap);
                            break;
                            // DEPT_STAFF needs a snapshot for each appointment.
                        case DEPT_STAFF:
                            for (DossierAppointmentAttributeKey appointmentKey : appointments.keySet())
                            {
                                schoolDepartmentCriteria = new SchoolDepartmentCriteria();
                                schoolDepartmentCriteria.setSchool(appointmentKey.getSchoolId());
                                schoolDepartmentCriteria.setDepartment(appointmentKey.getDepartmentId());
                                schoolDepartmentCriteriaMap.clear();   // clear previous entries. Only want one entry for creatSnapshot
                                schoolDepartmentCriteriaMap.put(schoolDepartmentCriteria.getSchool()+":"+schoolDepartmentCriteria.getDepartment(),schoolDepartmentCriteria);
                                snapshotService.createDossierSnapshot(dossier, snapshotRole, schoolDepartmentCriteriaMap);
                            }
                            break;
                            // SCHOOL_STAFF needs a snapshot for each unique school only.
                        case SCHOOL_STAFF:
                            // Keep track of school Id's processed
                            Map<Integer,Integer>schoolMap = new HashMap<Integer,Integer>();
                            for (DossierAppointmentAttributeKey appointmentKey : appointments.keySet())
                            {
                                // Skip this school if snapshot already created
                                if (schoolMap.containsKey(appointmentKey.getSchoolId()))
                                {
                                    continue;
                                }
                                schoolMap.put(appointmentKey.getSchoolId(),appointmentKey.getSchoolId());
                                schoolDepartmentCriteria = new SchoolDepartmentCriteria();
                                schoolDepartmentCriteria.setSchool(appointmentKey.getSchoolId());
                                schoolDepartmentCriteria.setDepartment(0);
                                schoolDepartmentCriteriaMap.clear();   // clear previous entries. Only want one entry for creatSnapshot
                                schoolDepartmentCriteriaMap.put(schoolDepartmentCriteria.getSchool()+":"+schoolDepartmentCriteria.getDepartment(),schoolDepartmentCriteria);
                                snapshotService.createDossierSnapshot(dossier, snapshotRole, schoolDepartmentCriteriaMap);
                            }
                            break;
                        default:
                            break;
                    }
                }

                dossier = this.updateRoutingLocationAndStatus(dossier, annotation, routeToLocation, dossier.getRoutingPerson());
            }
            else
            {
                StringBuilder msg = new StringBuilder("Dossier id " + dossier.getDossierId() + " for user " + dossier.getAction().getCandidate().getUserId()
                                                      + " is not in a state to be approved and routed to the " + this.getNextWorkflowNode(dossier/*.getDossierId()*/)
                                                      + " node for the following reasons: ");

                for (String errorMsg : routingErrors)
                {
                    msg.append(errorMsg).append(", ");
                }
                // Remove hanging comma
                msg.setLength(msg.length() - 2);

                logger.error(msg.toString());
                throw new WorkflowException(msg.toString());
            }
        }
        return dossier;
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.dossier.DossierService#getInitialWorkflowNode(edu.ucdavis.mw.myinfovault.domain.dossier.Dossier)
     */
    @Override
    public WorkflowNode getInitialWorkflowNode(Dossier dossier) throws WorkflowException
    {
        return workflowService.getInitialNode(dossier);
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.dossier.DossierService#getFinalWorkflowNode(edu.ucdavis.mw.myinfovault.domain.dossier.Dossier)
     */
    @Override
    public WorkflowNode getFinalWorkflowNode(Dossier dossier) throws WorkflowException
    {
        return workflowService.getFinalNode(dossier);
    }

    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.dossier.DossierService#validateWorkflowNodePrerequisites(edu.ucdavis.mw.myinfovault.domain.dossier.Dossier, edu.ucdavis.mw.myinfovault.service.workflow.WorkflowNode)
     */
    @Override
    public boolean validateWorkflowNodePrerequisites(Dossier dossier, WorkflowNode node) throws WorkflowException
    {
        dossier = this.loadDossierAttributesForLocation(dossier, node.getNodeLocation());
        List<DossierAppointment> appointments = new ArrayList<DossierAppointment>();
        for (DossierAppointmentAttributeKey apptKey : dossier.getAppointmentAttributes().keySet())
        {
            DossierAppointmentAttributes dossierAppointmentAttributes = dossier.getAttributesByAppointment(apptKey);
            DossierAppointment dossierAppointment = new DossierAppointment();
            dossierAppointment.setAppointmentDescription(dossierAppointmentAttributes.getDepartmentMap().get("description").equals("") ?
                    dossierAppointmentAttributes.getDepartmentMap().get("school") :
                        dossierAppointmentAttributes.getDepartmentMap().get("school")+ " - " +
                        dossierAppointmentAttributes.getDepartmentMap().get("description"));
            dossierAppointment.setPrimary(dossierAppointmentAttributes.isPrimary());
            dossierAppointment.setSchoolId(Integer.parseInt(dossierAppointmentAttributes.getSchoolId()));
            dossierAppointment.setDeptId(Integer.parseInt(dossierAppointmentAttributes.getDeptId()));
            dossierAppointment.setJointComplete(dossier.isAppointmentComplete(apptKey));
            appointments.add(dossierAppointment);
        }
        return this.validateWorkflowNodePrerequisites(dossier, appointments, node);

    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.dossier.DossierService#validateWorkflowNodePrerequisites(edu.ucdavis.mw.myinfovault.domain.dossier.Dossier, java.util.List, edu.ucdavis.mw.myinfovault.service.workflow.WorkflowNode)
     */
    @Override
    public boolean validateWorkflowNodePrerequisites(Dossier dossier, List<DossierAppointment> dossierAppointments, WorkflowNode node)
    {
        // Check that a packet has been submitted for the dossier
        if (!packetService.isPacketSubmitted(dossier))
        {
            return false;
        }

        // Default to satisfied when there are none
        boolean prerequisiteSatisfied = true;

        // Get the prerequisites required to route to this node
        Map<String, List<String>>nodePrerequisiteMap = node.getAbsoluteLocationPrerequisiteAttributes();

        // validate the absolute prerequisites, none are optional;
        if (!nodePrerequisiteMap.isEmpty())
        {
            return this.validatePrerequisites(dossier, nodePrerequisiteMap, dossierAppointments, node, false);
        }

        // Get the optional prerequisites required to route to this node
        List<Map<String, List<String>>>nodeOptionalPrerequisiteMapList = node.getOptionalLocationPrerequisiteAttributesList();

        // validate the optional prerequisites, all are optional, but at least one must be satisfied from each of the maps;
        for (Map<String, List<String>>nodeOptionalPrerequisiteMap : nodeOptionalPrerequisiteMapList)
        {
            if (!nodeOptionalPrerequisiteMap.isEmpty())
            {
                if (!this.validatePrerequisites(dossier, nodeOptionalPrerequisiteMap, dossierAppointments, node, true))
                {
                    return false; // no need to loop thru further if prerequisite condition failed
                }
            }
        }
        return prerequisiteSatisfied;
    }

    /**
     * Validate the prerequisite attributes for a dossier at the given workflow node
     * @param dossier The Dossier for which to validate the attributes
     * @param nodePrerequisiteMap Map of attributes which must be validated
     * @param dossierAppointments List of DossierAppointments for this dossier as build in the processDossierAttributes method
     * @param node The workflow node to which the dossier may potentially be routed
     * @param optional true if optional prerequisites are being validated, false manditory prerequisites are being validated
     * @return boolean true if prerequisites are satisfied, otherwise false
     * @return
     */
    private boolean validatePrerequisites(Dossier dossier, Map<String, List<String>>nodePrerequisiteMap, List<DossierAppointment> dossierAppointments, WorkflowNode node, boolean optional)
    {
        logger.info(auditMarker, "Valdating "+(optional?"* OPTIONAL *":"* ABSOLUTE *")+" Prequisites for dossier {} to route to the {} location", dossier.getDossierId(), node.getNodeLocation());
        boolean prerequisiteSatisfied = true;
        DossierAppointmentAttributes attributes = null;

        for (DossierAppointment dossierAppointment : dossierAppointments)
        {
            // New validate the attribute prerequisites for the appointment
            DossierAppointmentAttributeKey appointmentKey =
                Dossier.getAppointmentKey(dossierAppointment.getSchoolId(), dossierAppointment.getDeptId());
            attributes = dossier.getAttributesByAppointment(appointmentKey);

            // If the joint_appointment_override attribute is present for this appointment, then we may want to override the normal attribute validation
            // of the joint deans recommendation attribute. Even though the joint appointment may not be complete (isJointComplete), there needed to be a
            // way to allow a dossier to be routed forward when there was a joint appointment with no deans recommendation added. This is the case with a
            // dossier at the school location that could potentially be routed to a senate location. In those cases a signature cannot be present to allow
            // the dossier to be routed forward. When the signature *is* present, then the isJointComplete flag must also be set.
            if (attributes.getAttributes().containsKey("joint_appointment_override") &&
                !dossierAppointment.getIsPrimary() &&
                !dossierAppointment.isJointComplete())
            {
                if (attributes.getAttributes().containsKey("j_deans_recommendation") &&
                    attributes.getAttributes().get("j_deans_recommendation").getAttributeValue() != null &&
                    attributes.getAttributes().get("j_deans_recommendation").getAttributeValue() == DossierAttributeStatus.ADDED)
                {
                    return false;
                }
            }
            // In a no override situation, the joint appointment must be complete to continue with the attribute evaluation
            else if(!dossierAppointment.getIsPrimary() && !dossierAppointment.isJointComplete())
            {
                return false;
            }


            StringBuffer sb = new StringBuffer();
            String attributeKey = null;
            String value = null;
            for (String key :attributes.getAttributes().keySet())
            {
                attributeKey = key;   // access the key outside of the loop for logging
                // If the attribute is not a prerequisite, skip it
                if (!nodePrerequisiteMap.containsKey(attributeKey))
                {
                    continue;
                }
                sb.delete(0, sb.length()>=0?sb.length():0);
                sb.append(attributeKey).append("=").append(attributes.getAttributes().get(attributeKey).getAttributeValue());

                // Replace a null attribute value with "null"
                value = attributes.getAttributes().get(attributeKey).getAttributeValue() ==
                    null ? "null" : attributes.getAttributes().get(attributeKey).getAttributeValue().name();

                // If this attribute is a signature, make sure it is valid
                if (attributes.getAttributes().get(attributeKey).getAttributeType() == DossierAttributeType.DOCUMENT
                 || attributes.getAttributes().get(attributeKey).getAttributeType() == DossierAttributeType.DOCUMENTUPLOAD)
                {
                    for (DossierFileDto documentDto : attributes.getAttributes().get(attributeKey).getDocuments())
                    {
                        if (documentDto.isSignable() && !documentDto.isSigned())
                        {
                            return false;
                        }
                    }
                }

                if (nodePrerequisiteMap.get(attributeKey).contains(value) && optional)
                {
                    prerequisiteSatisfied = true;
                    // Optional indicates any one which is satified is sufficient, so we can exit the attribute loop
                    break;
                }
                else if (!nodePrerequisiteMap.get(attributeKey).contains(value) && !optional)
                {
                    prerequisiteSatisfied = false;
                    // Not optional indicates that it only takes one unsatisfied attribute to fail, so we can exit the attribute loop
                    break;
                }
                else if (nodePrerequisiteMap.get(attributeKey).contains(value))
                {
                    prerequisiteSatisfied = true;
                }
                else
                {
                    prerequisiteSatisfied = false;
                }
            }
            sb.append(": "+(prerequisiteSatisfied ? "*SATISFIED*":"*NOT SATISFIED*")+" - acceptable value(s)=").append(nodePrerequisiteMap.get(attributeKey)).append(" : current value=").append(value);
            logger.info(auditMarker, "---->{}", sb);

            // Don't set the complete attribute for a joint appointment
            if (dossierAppointment.isPrimary())
            {
                dossierAppointment.setComplete(prerequisiteSatisfied);
            }

            // If the attributes are optional and satisfied or not optional and not satisfied, get out
            if ((!optional && !prerequisiteSatisfied) || (optional && prerequisiteSatisfied))
            {
                return prerequisiteSatisfied;
            }
        }

        return prerequisiteSatisfied;
    }

    /**
     * Make sure the dossier is at the expected location prior to routing.
     * @param dossier
     * @param routeToLocation
     * @param inMemoryLocation This value may be present or null. If null, use the location in the Dossier object,
     * otherwise use the location provided by the caller. The location provided by the caller will be the location
     * at the time the dossier was loaded to a JSP page.
     * @throws WorkflowException
     */
    private void validateDossierLocation(Dossier dossier, DossierLocation routeToLocation, DossierLocation inMemoryLocation) throws WorkflowException
    {
        // If this is a brand new dossier, ID=0, there is nothing to validate
        if (dossier.getId() <= 0)
        {
            return;
        }

        StringBuilder sb = new StringBuilder();
        // Make sure the route to location is not UNKNOWN
        if (routeToLocation == DossierLocation.UNKNOWN)
        {
            sb.append("Attempt to route dossier ").append(dossier.getDossierId()).append(" from ").append(dossier.getLocation()).append(" to nonexistant workflow node.");
            logger.error(sb.toString());
            throw new WorkflowException(sb.toString());
        }

        // If no inMememoryLocation supplied, use the location in the dossier object
        if (inMemoryLocation == null)
        {
            inMemoryLocation = DossierLocation.mapWorkflowNodeNameToLocation(dossier.getLocation());
        }

        // Make sure the in memory location matches the database location
        DossierLocation currentLocation = DossierLocation.mapWorkflowNodeNameToLocation(this.getDossier(dossier.getDossierId()).getLocation());
        if (currentLocation != inMemoryLocation)
        {
            sb.append("Dossier ").append(dossier.getDossierId()).append(" location was updated to ").append(currentLocation)
            .append(" prior to this routing attempt. Routing to the ").append(routeToLocation).append(" location has been cancelled.");
            throw new WorkflowException(sb.toString());
        }

        if (currentLocation == routeToLocation)
        {
            sb.append("Dossier ").append(dossier.getDossierId()).append(" is already at ").append(currentLocation)
            .append(" location prior to this routing attempt to the ").append(routeToLocation).append(" location. Routing cancelled.");
            throw new WorkflowException(sb.toString());
        }
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.dossier.DossierService#updateDossier(edu.ucdavis.mw.myinfovault.domain.dossier.Dossier, edu.ucdavis.mw.myinfovault.service.person.MivPerson, edu.ucdavis.mw.myinfovault.service.person.MivPerson)
     */
    @Override
    public void updateDossier(Dossier dossier,
                              MivPerson targetPerson,
                              MivPerson realPerson)
    {
        try
        {
            // Get fresh dossier object from database
            Dossier formerDossier = getDossier(dossier.getDossierId());
            DossierDelegationAuthority formerDelegationAuthority = formerDossier.getAction().getDelegationAuthority();
            DossierActionType formerActionType = formerDossier.getAction().getActionType();

            // If delegation authority has changed, update the RAF
            if (dossier.getAction().getDelegationAuthority() != formerDelegationAuthority)
            {
                for (DossierAppointmentAttributeKey appointmentKey : dossier.getAppointmentAttributes().keySet())
                {
                    // If this is a joint appointment, it is no longer signed so reset the Appointment Complete flag
                    dossier.setIsAppointmentComplete(appointmentKey, false);
                }

                // Signal delegation authority change has been made.
                //TODO: At some point we should refactor this code to be executed as a result of the DelegationAuthorityChangeEvent.
                EventDispatcher.getDispatcher().post(new DelegationAuthorityChangeEvent(formerDossier, dossier, targetPerson, realPerson));

                StringBuilder logMsg = new StringBuilder(
                        "+++ Delegation Authority Change initiated for " + dossier.getAction().getCandidate().getDisplayName() +
                        " (" + dossier.getAction().getCandidate().getPersonId() +
                        ") dossier " + dossier.getDossierId());
                logger.info(logMsg + "\n+  Changing delegation authority for dossier " + dossier.getDossierId() +
                            ": " + formerDelegationAuthority + "-->" + dossier.getAction().getDelegationAuthority());

                /*
                 * Change from Redelegated to Non-Redelegated or
                 * from Redelegated Federation to None-Redelegated,
                 * the following must happen:
                 *
                 *  1. Existing FPC Recommendation Remains
                 *  2. Existing Deans Final Decision comments remain
                 *     and are renamed to Dean's Recommendation
                 *     Comments, all other uploads are removed
                 */
                // previous delegation authority is redelegated
                if (formerDelegationAuthority == DossierDelegationAuthority.REDELEGATED)
                {
                    DossierLocation currentLocation = formerDossier.getDossierLocation();

                    // If the previous delegation authority was REDELEGATED
                    // The dossier must be moved to the school location if it is at
                    // PostSenateSchool location because the location is not valid for a non-redelegated action
                    if (DossierLocation.POSTSENATESCHOOL == currentLocation)
                    {
                        logger.info("+  Moving dossier "+dossier.getDossierId()+" to the "+DossierLocation.SCHOOL+" workflow node because of delegation authority change.");
                        logMsg.delete(0, logMsg.length()-1);

                        // Route the dossier to the school node
                        routeDossierToNode(targetPerson,
                                dossier.getDossierId(),
                                DossierLocation.SCHOOL,
                                "Routing ("+formerDossier.getLocation()+" --> "+DossierLocation.SCHOOL.mapLocationToWorkflowNodeName()+") due to change in delegation authority ("+formerDelegationAuthority+" --> "+dossier.getAction().getDelegationAuthority()+")");
                        // retrieve the updated dossier
                        dossier = getDossier(dossier.getDossierId());
                    }

                    // If going from a redelegated action to a non_redelegated action, some documents must be renamed
                    if (dossier.getAction().getDelegationAuthority() == DossierDelegationAuthority.NON_REDELEGATED)
                    {
                        // Rename any deans final decision comments to deans recommendation comments
                        renameUploads(dossier,
                                      DEANS_FINAL_DECISION_COMMENTS_DOCUMENT_ID,
                                      DEANS_RECOMMENDATION_COMMENTS_DOCUMENT_ID,
                                      targetPerson);
                    }
                }
                // Previous delegation authority is non-redelegated and current is redelegated
                // Remove any retroactive date which may be present
                else if (formerDelegationAuthority == DossierDelegationAuthority.NON_REDELEGATED
                      && dossier.getAction().getDelegationAuthority() == DossierDelegationAuthority.REDELEGATED)
                {
                    dossier.getAction().setRetroactiveDate(null);
                }

                // MIV-3668 Reset deans_release and vp_release attributes for all appointments when delegation authority is changed
                resetDossierAttributes(dossier, false, "vp_release", "dean_release", "dean_recommendation_release", "joint_dean_release");
            }
            /*
             * Otherwise, if JUST the action has been changed on a redelegated action
             */
            else if (dossier.getAction().getActionType() != formerActionType
                  && formerDelegationAuthority == DossierDelegationAuthority.REDELEGATED)
            {
                // MIV-3554 Reset deans_release and vp_release attributes for primary appointment
                resetDossierAttributes(dossier, true, "vp_release", "dean_release", "dean_recommendation_release", "joint_dean_release");
            }

            // If the action has changed
            if (dossier.getAction().getActionType() != formerActionType)
            {
                resetDossierAttributes(dossier, false, "disclosure_certificate", "j_disclosure_certificate");

                // If the dossier is at the school and action type is changed, the
                // dossier must be routed back to the department in order to have the candidate re-sign the
                // disclosure certificate
                if (formerDossier.getDossierLocation() == DossierLocation.SCHOOL)
                {
                    /*
                     * TODO: Much more is done when returning a dossier via the return link in OpenAction#returnDossier.
                     *       Should that code be moved into the implementation of DossierService#returnDossierToPreviousNode?
                     */
                    // Routing the dossier will save it, so we can exit here
                    returnDossierToPreviousNode(targetPerson,
                                                realPerson,
                                                dossier,
                                                "Dossier returned to the department due to change in Action type",
                                                DossierLocation.DEPARTMENT,
                                                DossierLocation.SCHOOL);
                    return;
                }
            }

            // Update the dossier
            saveDossier(dossier);
        }
        catch (final WorkflowException exception)
        {
            /* FIXME: Can't throw the severe error here. There are perfectly innocuous reasons why
                      a WorkflowException may be thrown, such as trying to route a dossier when
                      someone else has already routed it out from under you.
             */
            throw new MivSevereApplicationError("errors.raf.dossier.write2", exception, dossier.getDossierId());
        }
    }

    /**
     * Reset the dossier attributes specified in the input attributeNames list
     * @param dossier Dossier object
     * @param primaryOnly true=reset attributes for primary appointment only, otherwise reset for all
     * @param attributeNames list of attribute names to reset
     * @throws WorkflowException
     */
    private void resetDossierAttributes(Dossier dossier, boolean primaryOnly, String... attributeNames) throws WorkflowException
    {
        // Load the dossier attributes
        loadDossierAttributes(dossier);

        for (DossierAppointmentAttributeKey appointmentAttributeKey : dossier.getAppointmentAttributes().keySet())
        {
            // Check if clearing attributes for primary appointment only and if yes then check to make sure the attributes
            // are for the primary appointment. If not then clear them for all appointments
            if (!primaryOnly || (primaryOnly && dossier.getAppointmentAttributes().get(appointmentAttributeKey).isPrimary()))
            {
                resetAttributes(dossier, Arrays.asList(attributeNames), appointmentAttributeKey);
            }
        }
    }

    /**
     * renameUploads - Renames all uploads from a source document id type to the target
     * document id type.
     * @param dossier - The dossier containing the document to be renamed
     * @param sourceDocumentId - The source document id
     * @param targetDocumentId - The target document id
     * @param targetPerson the current miv user
     * @throws WorkflowException
     */
    private void renameUploads(Dossier dossier, int sourceDocumentId, int targetDocumentId, MivPerson targetPerson)
        throws WorkflowException
    {
        // If there are any Deans Final Decision Comments present, rename to Deans Recommendation Comments
        List<UploadDocumentDto> deansFinalDecisionComments = getUploadFileDtoByTypeAndUser(dossier,
                                                                                           sourceDocumentId,
                                                                                           dossier.getAction().getCandidate().getUserId());

        if (!deansFinalDecisionComments.isEmpty())
        {
            // Get the info for each of the documents for the rename
            Map<String, Map<String,String>> documentMap = MIVConfig.getConfig().getMap("documents");
            Map<String,String> sourceFileNameMap = documentMap.get(Integer.toString(sourceDocumentId));
            Map<String,String> targetFileNameMap = documentMap.get(Integer.toString(targetDocumentId));

            logger.info("+ Renaming "+sourceFileNameMap.get("dossierfilename")+" documents to "+targetFileNameMap.get("dossierfilename"));

            // Rename all final decision documents which may be present.
            for (UploadDocumentDto uploadDocumentDto : deansFinalDecisionComments)
            {
                uploadDocumentDto.setUpdateUserId(targetPerson.getUserId());
                renameDocumentUpload(dossier.getDossierDirectoryByDocumentFormat(DocumentFormat.PDF),
                                     uploadDocumentDto,
                                     sourceFileNameMap,
                                     targetFileNameMap);
            }
        }
    }

    /**
     * Renames an uploaded document on disc and updates the corresponding table entry in the UserUploadDocument table.
     *
     * @param targetDirectory target directory for the rename
     * @param uploadDocumentDto dto containg all info for the upload to be renamed
     * @param sourceFileNameMap A map of values associated with the file type to be renamed
     * @param targetFileNameMap A map of values associated with the file type of the newly renamed file
     * @return <code>true</code> for success <code>false</code> for failure
     * @throws WorkflowException
     */
    private boolean renameDocumentUpload(File targetDirectory,
                                         UploadDocumentDto uploadDocumentDto,
                                         Map <String, String> sourceFileNameMap,
                                         Map <String, String> targetFileNameMap) throws WorkflowException
    {
        // Get the current upload file name
        String sourceFileName = sourceFileNameMap.get("dossierfilename");
        // Get the new file name
        String targetFileName = targetFileNameMap.get("dossierfilename");
        // Get the new file description
        String targetFileDescription = targetFileNameMap.get("description");
        // Get the new file type id
        int targetFileDocumentTypeId = Integer.parseInt(targetFileNameMap.get("id"));

        // Get the current file path
        String sourceUploadFilePath = uploadDocumentDto.getUploadFileName();

        // Build the new file path by getting the currently defined name of this file type and
        // replacing with the defined name for the new file type
        String targetUploadFilePath = sourceUploadFilePath.replaceAll(sourceFileName, targetFileName);

        // Build the new file name by getting the currently defined name of this file type and
        // replacing with the defined name for the new file type
        File sourceDocumentPath = new File(targetDirectory,sourceUploadFilePath);
        File targetDocumentPath = new File(targetDirectory,targetUploadFilePath);


        // Rename the filename on disk, update the PDF bookmark and
        // insert a new record in the UserUploadDocument table
        if (sourceDocumentPath.renameTo(targetDocumentPath))
        {
            logger.info("+ Rename complete:"+sourceDocumentPath+"-->"+targetDocumentPath);
            // Set the new document type id
            uploadDocumentDto.setDocumentId(targetFileDocumentTypeId);
            // Set the new upload file name
            uploadDocumentDto.setUploadFileName(targetUploadFilePath);
            // Set the default description for the new file
            uploadDocumentDto.setUploadName(targetFileDescription);
            // Update the bookmark for the PDF file with the new description
            String result = PDFConcatenator.addBookmark(targetDocumentPath, targetFileDescription, true) ? "complete." : "FAILED.";
            logger.info("+ Update PDF bookmark for "+targetDocumentPath+" "+result);
            // Update the UserUploadDocument record for the newly renamed document
            this.dossierDao.updateUserUploadDocument(uploadDocumentDto);
            return true;
        }
        logger.info("+ Rename FAILED: "+sourceDocumentPath+"-->"+targetDocumentPath);
        return false;
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.dossier.DossierService#getAttributeNames(edu.ucdavis.mw.myinfovault.domain.action.DossierLocation, edu.ucdavis.mw.myinfovault.domain.action.DossierDelegationAuthority, edu.ucdavis.mw.myinfovault.domain.action.DossierActionType, boolean)
     */
    @Override
    public List<String> getAttributeNames(DossierLocation dossierLocation,
                                          DossierDelegationAuthority delegationAuthority,
                                          DossierActionType actionType,
                                          boolean primary)
    {
        return dossierDao.getAttributeNames(dossierLocation,
                                            primary ? DossierAppointmentType.PRIMARY : DossierAppointmentType.JOINT,
                                            delegationAuthority,
                                            actionType);
    }

    /**
     * cleanupDossierRecordsAndFileSystem - When a dossier id cancelled, clear all referencing database records
     * and file system. We will however keep the dossier record itself for histerical
     * purposes.
     *
     * @param evt DossierCancelEvent triggering this execution
     */
    @Subscribe
    public void cleanupDossierRecordsAndFileSystem(DossierCancelEvent evt)
    {
        this.cleanupDossierRecordsAndFileSystem(evt.getDossier());
    }

    /**
     * cleanupDossierRecordsAndFileSystem - When a dossier id cancelled, clear all referencing database records
     * and file system. We will however keep the dossier record itself for histerical
     * purposes.
     *
     * @param Dossier
     */
    private void cleanupDossierRecordsAndFileSystem(Dossier dossier)
    {
        long dossierId = dossier.getDossierId();

        logger.info("+++ Cleaning up dossier records and file system for canceled dossier:"+dossierId);

        RafService rafService = MivServiceLocator.getRafService();
        SigningService signingService = MivServiceLocator.getSigningService();

        // Gather all data for this dossier
        try
        {
            dossier = this.getDossierAndLoadAllData(dossierId);

            // Delete DC and any signatures
            logger.info("+ Deleting Disclosure Certificate and Signatures for canceled dossier:"+dossierId);
            removeDCAndSignatures(dossier);

            // Delete the signature requests
            logger.info("+ Deleting Signature Requests for canceled dossier:"+dossierId);
            signingService.removeSignatureRequestByPrimaryUser(userService.getPersonByMivId(dossier.getAction().getCandidate().getUserId()));

            // delete the RAF
            logger.info("+ Deleting RAF records for canceled dossier:"+dossierId);
            rafService.deleteRaf(dossier);

            // Remove the dossier attributes
            logger.info("+ Deleting dossier attributes for canceled dossier:"+dossierId);
            this.deleteDossierAttributes(dossier);

            // Remove dossier Snapshots
            logger.info("+ Deleting snapshots for canceled dossier:"+dossierId);
            snapshotService.deleteSnapshots(dossier, null);

            // Remove Uploads from all locations
            logger.info("+ Deleting uploads for canceled dossier:"+dossierId);
            removeUploadsByLocation(dossier);

            // Remove any packet requests which may exist for this dossier
            PacketRequest packetRequest = packetService.getPacketRequest(dossier);
            if (packetRequest != null)
            {
                logger.info("+ Deleting packet requests for canceled dossier:"+dossierId);
                 packetService.deletePacketRequest(packetRequest);
            }

            // If the upload database records have been removed for the dossier, go ahead and
            // remove the file system directory.
            if (getUploadFiles(dossier).isEmpty())
            {
                File dossierDirectory = dossier.getDossierDirectory();
                logger.info("+ Removing dossier directory "+dossierDirectory.getAbsolutePath()+" snapshots for canceled dossier:"+dossierId);
                try
                {
                    FileUtils.deleteDirectory(dossierDirectory);
                    logger.info("+++ Record and file system cleanup complete for canceled dossier:"+dossierId);
                }
                catch (IOException ioe)
                {
                    logger.info("*** Failed to remove files from file system for canceled dossier "+dossier.getDossierId()+" FAILED: "+ioe);
                }
            }
        }
        catch (WorkflowException wfe)
        {
           logger.info("*** Failed to complete dossier cleanup for canceled dossier "+dossier.getDossierId()+" FAILED: "+wfe);
        }
        catch (SQLException sqle)
        {
            logger.info("*** Failed to complete dossier cleanup for canceled dossier "+dossier.getDossierId()+" FAILED: "+sqle);
        }
    }

    /**
     * Remove DC and any signatures for this dossier.
     *
     * @param dossier
     */
    private void removeDCAndSignatures(Dossier dossier)
    {
        DCService dcService = MivServiceLocator.getDCService();
        SigningService signingService = MivServiceLocator.getSigningService();

        // Delete DC and signatures for each appointment
        for (DossierAppointmentAttributeKey attributeKey : dossier.getAppointmentAttributes().keySet())
        {
            DCBo dcBo = dcService.getDisclosureCertificate(dossier, attributeKey.getSchoolId(), attributeKey.getDepartmentId());

            // delete the DC
            logger.info("+ Deleting DC records for dossier:"+dossier.getDossierId()+" school:"+attributeKey.getSchoolId()+" department:"+attributeKey.getDepartmentId());
            dcService.deleteDc(dcBo);

            // Remove the reviewers
            logger.info("+ Deleting dossier reviewer records for dossier:"+dossier.getDossierId()+" school:"+attributeKey.getSchoolId()+" department:"+attributeKey.getDepartmentId());
            this.deleteDossierReviewers(dossier.getDossierId(), attributeKey.getSchoolId(), attributeKey.getDepartmentId());

            DossierAppointmentAttributes dossierAttributes = dossier.getAppointmentAttributes().get(attributeKey);
            Map <String, DossierAttribute> dossierAttributeMap = dossierAttributes.getAttributes();

            // Delete signatures for any documents associated with the attributes for this appointment
            for (String key : dossierAttributeMap.keySet())
            {
                for (DossierFileDto dossierFileDto : dossierAttributeMap.get(key).getDocuments())
                {
                    if (dossierFileDto.isSigned())
                    {
                        SignableDocument document =
                            signingService.getDocument(dossier, attributeKey.getSchoolId(), attributeKey.getDepartmentId(), dossierFileDto.getDocumentType());
                        if (document != null)
                        {
                            logger.info("+ Deleting signature for "+document.getDocumentType().getDescription()+" document for dossier: "+dossier.getDossierId());
                            signingService.deleteSignatures(document);
                        }
                    }
                }
            }
        }
    }


    /**
     * Remove uploads acquired at the location from where the dossier was returned.
     *
     * @param event dossier return event
     */
    @Subscribe
    public void removeUploads(DossierReturnEvent event)
    {
        /*
         * Remove uploads acquired at the return event origin unless
         * returning from the DEPARTMENT. Those uploads must be preserved.
         */
        if (event.getRoute().getOrigin() != DossierLocation.DEPARTMENT)
        {
            // create a location filter for the route origin
            SearchFilter<UploadDocumentDto> filter = new LocationFilter<UploadDocumentDto>(event.getRoute().getOrigin()) {
                                                         @Override
                                                         protected DossierLocation getLocation(UploadDocumentDto item) {
                                                             return item.getUploadLocation();
                                                         }
                                                     };

            removeUploads(event, filter.apply(getUploadFileDtos(event.getDossier())));
        }
    }

    /**
     * Remove the wet signature upload for the event decision.
     *
     * @param event decision signature delete event
     */
    @Subscribe
    public void removeDecisionWetSignature(DecisionSignatureDeleteEvent event)
    {
        removeUpload(event);
    }

    /**
     * Remove the wet signature upload for the event decision.
     *
     * @param event decision request delete event
     */
    @Subscribe
    public void removeDecisionWetSignature(DecisionRequestDeleteEvent event)
    {
        removeUpload(event);
    }

    /**
     * Remove the wet signature upload for the event decision.
     *
     * @param event decision signature event
     */
    private void removeUpload(DecisionSignatureEvent event)
    {
        Decision decision = event.getDecision();

        removeUploads(event,
                      getUploadFileDtoByTypeUserSchoolAndDepartment(
                          decision.getDossier().getDossierId(),
                          decision.getDocumentType().getWetSignatureId(),
                          decision.getDossier().getAction().getCandidate().getUserId(),
                          decision.getSchoolId(),
                          decision.getDepartmentId()));
    }

    /**
     * Remove uploads if the delegation authority has changed.
     *
     * @param event academic action event
     */
    @Subscribe
    public void removeUploads(AcademicActionEvent event)
    {
        AcademicAction newAction = event.getCurrentAction();
        AcademicAction formerAction = event.getFormerAction();

        // formerAction is NULL when sending the dossier first time
        // If delegation authority has changed, update the RAF
        if (formerAction != null && newAction.getDelegationAuthority() != formerAction.getDelegationAuthority())
        {
            // Exclude upload documents from removal
            List<Integer> excludeDocs = new ArrayList<Integer>();

            // previous delegation authority is redelegated
            // If going from a redelegated action to a non_redelegated action
            if (formerAction.getDelegationAuthority() == DossierDelegationAuthority.REDELEGATED
             && newAction.getDelegationAuthority() == DossierDelegationAuthority.NON_REDELEGATED)
            {
                // Do not remove the following documents when going from redelegated/redelegated_federation to non_redelegated
                excludeDocs.add(DEANS_RECOMMENDATION_COMMENTS_DOCUMENT_ID);
                excludeDocs.add(JOINT_DEANS_RECOMMENDATION_COMMENTS_DOCUMENT_ID);
                excludeDocs.add(JOINT_DEANS_DECISION_COMMENTS_DOCUMENT_ID);
                excludeDocs.add(CAP_RECOMMENDATION_DOCUMENT_ID);
                excludeDocs.add(CAPAC_RECOMMENDATION_DOCUMENT_ID);
                excludeDocs.add(ASPC_RECOMMENDATION_DOCUMENT_ID);
                excludeDocs.add(AFPC_RECOMMENDATION_DOCUMENT_ID);
                excludeDocs.add(JPC_RECOMMENDATION_DOCUMENT_ID);
                excludeDocs.add(AD_HOC_COMMITTEE_RECOMMENDATION_DOCUMENT_ID);
                excludeDocs.add(SHADOW_COMMITTEE_RECOMMENDATION_DOCUMENT_ID);
            }

            // Remove document uploads, excluding any FPC Recommendations
            excludeDocs.add(FPC_RECOMMENDATION_DOCUMENT_ID);
            excludeDocs.add(JOINT_FPC_RECOMMENDATION_DOCUMENT_ID);

            // include matching location, exclude matching document
            SearchFilter<UploadDocumentDto> filter = (new LocationFilter<UploadDocumentDto>(DossierLocation.SCHOOL,
                                                                                            DossierLocation.POSTSENATESCHOOL,
                                                                                            DossierLocation.VICEPROVOST,
                                                                                            DossierLocation.POSTSENATEVICEPROVOST) {
                                                         @Override
                                                         protected DossierLocation getLocation(UploadDocumentDto item) {
                                                             return item.getUploadLocation();
                                                         }
                                                     }).not(new IdFilter<UploadDocumentDto>(excludeDocs) {
                                                         @Override
                                                         protected int getItemId(UploadDocumentDto item) {
                                                             return item.getDocumentId();
                                                         }
                                                     });

            removeUploads(event, filter.apply(getUploadFileDtos(event.getDossier())));
        }
    }

    /**
     * Remove uploads for the given parent event.
     *
     * @param event parent event requiring upload deletes
     * @param uploads uploads to remove
     */
    private void removeUploads(DossierEvent parent, List<UploadDocumentDto> uploads)
    {
        for (UploadDocumentDto uploadDocumentDto : uploads)
        {
            if (dossierDao.deleteDocumentUpload(parent.getDossier(), uploadDocumentDto))
            {
                EventDispatcher2.getDispatcher().post(
                    new DossierUploadDeleteEvent(
                        parent,
                        parent.getRealPerson(),
                        parent.getDossier(),
                        uploadDocumentDto));
            }
        }
    }

    /**
     * Remove the PDF uploads for the specified dossier with the school/dept keys.
     *
     * @param dossier dossier from which to remove uploads
     * @param dossierAppointmentAttributeKeys the School/department keys for the appointment uploads being removed
     */
    private void removeUploads(Dossier dossier, List<DossierAppointmentAttributeKey> apptkeys)
    {
        StringBuilder logMsg = new StringBuilder("+  Removing uploads for dossier " + dossier.getDossierId());

        // Iterate through all uploads for this dossier
        for (UploadDocumentDto uploadDocumentDto : getUploadFileDtos(dossier))
        {
            DossierAppointmentAttributeKey uploadkey =
                    new DossierAppointmentAttributeKey(uploadDocumentDto.getSchoolId(),uploadDocumentDto.getDepartmentId());
            /*
             * Delete the upload file if it matches the appointmentAttributeKey
             */
            if (apptkeys.contains(uploadkey))
            {
                dossierDao.deleteDocumentUpload(dossier, uploadDocumentDto);

                logMsg.append("\n-  Removed upload ");
            }
            else
            {
                logMsg.append("\n-  Skipped removal of ");
            }

            // append the upload document objects #toString()
            logMsg.append(uploadDocumentDto);
        }

        logger.info(logMsg.toString());
    }


    /**
     * Put signature requests on hold in response to an executive role change.
     *
     * @param event executive user change
     */
    @Subscribe
    public void holdDecisions(DossierRouteEvent event)
    {
        holdRequests(event, signingService.getRequestedSignatures(event.getDossier()));
    }

    /**
     * Put signature requests on hold in response to a dossier route event.
     *
     * @param event dossier route event
     */
    @Subscribe
    public void holdDecisions(ExecutiveUserChangeEvent event)
    {
        // put on hold all released signature requests for the former executive
        holdRequests(event, signingService.getRequestedSignatures(event.getFormerExecutive(),
                                                                  event.getExecutiveRole()));
    }

    /**
     * Put signature requests on hold in response to the parent event.
     *
     * @param parent event prompting the requests hold
     * @param requestedSignatures signature requests to hold
     */
    private void holdRequests(MivEvent parent, List<MivElectronicSignature> requestedSignatures)
    {
        for (MivElectronicSignature request : requestedSignatures)
        {
            Dossier dossier = request.getDocument().getDossier();

            String attributeName = request.getDocument().getDocumentType().getReleaseAttributeName();

            if (attributeName != null)
            {
                try
                {
                    // load dossier attributes at the current location
                    loadDossierAttributesForLocation(dossier, dossier.getDossierLocation());

                    // set the dossier attribute to 'HOLD'
                    if (dossier.setAttributeStatus(attributeName,
                                                   DossierAttributeStatus.HOLD,
                                                   request.getDocument().getSchoolId(),
                                                   request.getDocument().getDepartmentId()))
                    {
                        //save changes to the dossier
                        saveDossier(dossier);

                        // post decision hold event
                        EventDispatcher2.getDispatcher().post(
                                new DecisionHoldEvent(parent,
                                                      dossier,
                                                      new Scope(request.getDocument().getSchoolId(),
                                                                request.getDocument().getDepartmentId()),
                                                      attributeName));
                    }
                }
                catch (WorkflowException e)
                {
                    logger.error("Unable to load and update dossier attributes for " + dossier.getDossierId(), e);
                }
            }
        }
    }
}
