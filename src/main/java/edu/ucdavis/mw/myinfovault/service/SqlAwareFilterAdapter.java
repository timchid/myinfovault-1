/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: SqlAwareFilterAdapter.java
 */

package edu.ucdavis.mw.myinfovault.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;


/**
 * The SQL aware filter adapter maintains an SQL WHERE or AND clause
 * condition string corresponding to its own filter logic.
 *
 * @author Craig Gilmore
 * @since 4.3.1
 * @param <T> Object by which collections will filtered
 */
public abstract class SqlAwareFilterAdapter<T> extends SearchFilterAdapter<T>
{
    private final static String WHERE = " WHERE ";
    private final static String AND = " AND ";

    private List<SearchFilter<T>> nonSqlAwareFilters = new ArrayList<SearchFilter<T>>();

    // SQL pattern for condition string without the leading 'WHERE' or 'AND' operators
    private final StringBuilder conditionPattern = new StringBuilder();

    // Associated arguments for the above pattern
    private List<Object> conditionArguments = new ArrayList<Object>();


    /**
     * Create an SQL aware type filter for the given SQL pattern and associated arguments.
     *
     * @param conditionPattern The SQL containing <code>?<code> in place corresponding to the filters condition
     * @param conditionArguments The arguments associated with the given pattern
     */
    protected SqlAwareFilterAdapter(String conditionPattern,
                                    Object...conditionArguments)
    {
        this.conditionPattern.append(conditionPattern);
        this.conditionArguments.addAll(Arrays.asList(conditionArguments));
    }


    /**
     * Get the SQL where clause condition associated with this search filter
     * to be used in a prepared statement along with its corresponding arguments.
     *
     * @return Get the SQL where clause condition
     */
    public String getWhere()
    {
        return WHERE + conditionPattern;
    }


    /**
     * Get the SQL and clause condition associated with this search filter
     * to be used in a prepared statement along with its corresponding arguments.
     *
     * @return Get the SQL and clause condition
     */
    public String getAnd()
    {
        return AND + conditionPattern;
    }


    /**
     * @return Get the arguments corresponding to the SQL
     */
    public Object[] getArguments()
    {
        return conditionArguments.toArray(new Object[conditionArguments.size()]);
    }


    /**
     *
     * @param filter
     * @return reference to self for chaining
     */
    public <F extends SqlAwareFilterAdapter<T>> SqlAwareFilterAdapter<T> and(F filter)
    {
        //append arguments and pattern for SQL aware type filters
        conditionArguments.addAll(Arrays.asList(filter.getArguments()));
        conditionPattern.append(filter.getAnd());

        super.and(filter);

        return this;
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public <F extends SearchFilter<T>> SearchFilterAdapter<T> and(F filter)
    {
        //maintain a list of the non SQL filters ANDed
        nonSqlAwareFilters.add(filter);

        return super.and(filter);
    }

    /**
     * Apply this and all additional 'AND'ed filters to all the items presented excluding SQL aware
     * additional 'AND'ed filters if <code>excludeSqlAwareFilters</code> is set to <code>true</code>.
     *
     * @param items The collection to be filtered
     * @param excludeSqlAwareFilters If SQL aware filters are to be excluded from filtering.
     * @return A new collection of the same type provided, containing only the filtered elements
     * or an empty collection if null or an empty collection is passed in.
     */
    public <C extends Collection<T>> C apply(C items, boolean excludeSqlAwareFilters)
    {
        return excludeSqlAwareFilters
             ? applyAdditionalFilters(items, nonSqlAwareFilters)
             : apply(items);
    }
}
