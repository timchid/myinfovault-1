/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: MivStandaloneAuthorizationServiceImpl.java
 */


package edu.ucdavis.mw.myinfovault.service.authorization;

import static edu.ucdavis.myinfovault.ConfigReloadConstants.ALL;
import static edu.ucdavis.myinfovault.ConfigReloadConstants.PROPERTIES;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;

import com.google.common.eventbus.Subscribe;

import edu.ucdavis.mw.myinfovault.events.ConfigurationChangeEvent;
import edu.ucdavis.mw.myinfovault.events.EventDispatcher;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.mw.myinfovault.util.AttributeSet;
import edu.ucdavis.myinfovault.MIVConfig;

/**<p>
 * This implements the Authorization service as stand-alone authorization, basing the rules
 * on the existing MIV code, but centralizing the decision-making here instead of allowing
 * it to be scattered across the MIV classes.</p>
 *
 * <p>This is meant as a first step on the way to fully utilizing the KIM roles, responsibilities,
 * and permissions.</p>
 *
 * <p>See the MIV {@link AuthorizationService} documentation.</p>
 *
 * @author Stephen Paulsen
 * @since MIV 3.5
 */
public class MivStandaloneAuthorizationServiceImpl implements AuthorizationService, InitializingBean
{
    private static final Logger log = LoggerFactory.getLogger(AuthorizationService.class);
    private static final String DEFAULT_CONFIG_FILE = "/authorization.properties";

    private String configFileName;
    private Properties config;


    public MivStandaloneAuthorizationServiceImpl()
    {
        this(DEFAULT_CONFIG_FILE);
        log.info(" --------> Ran the no-arg constructor for MivStandaloneAuthorizationServiceImpl");
    }


    public MivStandaloneAuthorizationServiceImpl(final String configFilename)
    {
        log.info(" --------> Running constructor for MivStandaloneAuthorizationServiceImpl with config file \"{}\"", configFilename);
        this.configFileName = configFilename;

        // Check if we are running JUnit test
        if (MIVConfig.getConfig().getApplicationRoot().getPath().contains("test-classes"))
        {
            this.configFileName = "WEB-INF/classes"+configFileName;
        }

        configureService(null);
        EventDispatcher.getDispatcher().register(this);
    }


    /**
     * Load the Authorization Service configuration from the config (properties) file given in the constructor.<br>
     * Re-load the configuration in response to a ConfigurationChangeEvent.
     * @param event the event triggering loading, or <code>null</code> if called internally.
     */
    @Subscribe
    public void configureService(final ConfigurationChangeEvent event)
    {
        if (event == null || event.getWhatChanged() == ALL || event.getWhatChanged() == PROPERTIES)
        {

            log.info("configuring service with file {} : event is [{}]", this.configFileName, event);

            // Using class.getResourceAsStream() caches the resource (properties) file. For this to be reloadable
            // we use getClassLoader().getResource().openStream() instead, which does not cache.
            try (InputStream in = this.getClass().getClassLoader().getResource(this.configFileName).openStream())
            {
                if (in != null)
                {
                    log.info("cache has {} entries - clearing methodCache", methodCache.size());
                    this.methodCache.clear();
                    log.info("cache cleared, now has {} entries", methodCache.size());

                    config = new Properties();
                    config.load(in);
                }
                else {
                    log.warn("InputStream is null when loading config properties");
                }
            }
            catch (IOException e)
            {
                log.error("Authorization Service configuration error; unable to read config file [" + this.configFileName + "]", e);
            }

        }
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.authentication.AuthorizationService#hasPermission(edu.ucdavis.mw.myinfovault.service.person.MivPerson, java.lang.String, edu.ucdavis.mw.myinfovault.util.AttributeSet)
     */
    /**
     * {@inheritDoc}
     */
    @Override
    public boolean hasPermission(final MivPerson person, final String permissionName, final AttributeSet permissionDetails)
    {
        if (person == null) return false;

        PermissionMethod permission = getPermissionMethod(permissionName);

        // Deny permission if no way was found to test the permission.
        // This should never happen here because getPermissionMethod will
        // return the boilerplate "permission denied" object if one isn't
        // found.
        if (permission == null) return false;

        boolean authorized = permission.instance.hasPermission(person, permissionName, permissionDetails);

        log.debug((authorized ? "Granting" : "Denying")
                + " permission for " + permissionName
                + " to " + person.getDisplayName() + " (" + person.getUserId() + ")"
                + "\nPermission Details : " + permissionDetails);

        return authorized;
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.authentication.AuthorizationService#isAuthorized(edu.ucdavis.mw.myinfovault.service.person.MivPerson, java.lang.String, edu.ucdavis.mw.myinfovault.util.AttributeSet, edu.ucdavis.mw.myinfovault.util.AttributeSet)
     */
    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isAuthorized(final MivPerson person, final String permissionName, final AttributeSet permissionDetails, final AttributeSet qualification)
    {
        if (person == null) return false;

        PermissionMethod permission = getPermissionMethod(permissionName);

        // Deny authorization if no way was found to test the permission.
        // This should never happen here because getPermissionMethod will
        // return the boilerplate "permission denied" object if one isn't
        // found.
        if (permission == null) return false;

        boolean authorized = permission.instance.isAuthorized(person, permissionName, permissionDetails, qualification);

        log.debug((authorized ? "Granting" : "Denying")
                + " authority for " + permissionName
                + " to " + person.getDisplayName() + " (" + person.getUserId() + ")"
                + "\nPermission Details : " + permissionDetails
                + "\nQualification      : " + qualification);

        return authorized;
    }


    // Parameter arrays used for reflective Method lookups
    private static final Class<?>[] permParams = { MivPerson.class, String.class, AttributeSet.class };
    private static final Class<?>[] authParams = { MivPerson.class, String.class, AttributeSet.class, AttributeSet.class };

    /**
     * Finds the class configured to test for the named permission, and preloads the interface methods.
     * This <em>may</em> return <code>null</code> if no class is configured for the given permission,
     * or if the configured class can't be found or doesn't contain the necessary methods.
     *
     * @param permission Name of the permission to find
     * @return a PermissionMethod that supports the requested permission, or null if one could not be found or created.
     */
    private PermissionMethod getPermissionMethod(final String permission)
    {
        // Lowercase the permission name and replace any spaces with underscores.
        // This will be used to lookup the configured class that implements the permission.
        // "Edit User" would become "edit_user" so a configuration line in a properties
        // file might look like:
        //    edit_user=edu.ucdavis.mw.myinfovault.service.authentication.EditUserPermission
        String perm = getCanonicalName(permission);
//          perm = permission; // trying out not having to encode the permission name // this made the cache always miss

        PermissionMethod m = methodCache.get(perm);
        if (m == null)
        {
            // Method wasn't in the cache. Find the class and method that implements the requested permission.
            m = loadPermission(perm);
        }

        // If we _still_ have a null there's no implementation for this permission,
        // or the class specified was bad; not found, doesn't have the right methods,
        // couldn't be instantiated, etc.
        // Deny permission and cache that.
        if (m == null)
        {
            m = permissionDenied;
            methodCache.put(perm, m);
        }

        return m;
    }


    /**
     * Load and cache a PermissionAuthorizer for the named permission.
     * @param permissionName Name of the permission to load
     * @return a PermissionAuthorizer for the given permission, or the PermissionDenied authorizer if none could be found.
     */
    @SuppressWarnings("unchecked") // for the 'authorizerClass = ' assignment
    private PermissionMethod loadPermission(String permissionName)
    {
        PermissionMethod m;
        String className = config.getProperty(permissionName);

        // If nothing was found try again with an encoded permission name
        if (className == null)
        {
            // Lowercase the permission name and replace any spaces with underscores.
            // This will be used to lookup the configured class that implements the permission.
            // "Edit User" would become "edit_user" so a configuration line in a properties
            // file might look like:
            //    edit_user=edu.ucdavis.mw.myinfovault.service.authentication.EditUserPermission
            String perm = getCanonicalName(permissionName);
            permissionName = perm;
            className = config.getProperty(perm);
        }

        // If we _still_ have a null then no class was specified in the configuration for the named permission.
        // Deny permission and cache that for next time.
        // TODO: Have a configurable default-allow or default-deny?
        if (className == null)
        {
            log.warn("Configuration Error: No class was specified to implement permission \"" + permissionName + "\" in file " + configFileName);
            methodCache.put(permissionName, permissionDenied);
            return permissionDenied;
        }

        log.info("Loading permission '{}' with class {}", permissionName, className);


        m = new PermissionMethod();
        Exception lastException = null;
        try
        {
            Class<? extends PermissionAuthorizer> authorizerClass = (Class<? extends PermissionAuthorizer>) Class.forName(className);
            Constructor<? extends PermissionAuthorizer> ctor = null;
            PermissionAuthorizer authorizer = null;

            Properties props = new Properties();
            String permPrefix = permissionName + ".";
            for (String prop : config.stringPropertyNames())
            {
                if (prop.startsWith(permPrefix)) {
                    props.put(prop.replace(permPrefix, ""), config.getProperty(prop));
                }
            }

            log.debug("{} props found for permission '{}' : ||{}||", new Object[] { props.size(), permissionName, props });
            if (!props.isEmpty())
            {
                try
                {
                    // There are configuration properties for the permission; try to use the constructor that accepts Properties.
                    // That might fail if there isn't one, and we'll have to back down to the no-arg constructor.
                    ctor = authorizerClass.getConstructor(Properties.class);
                    authorizer = ctor.newInstance(props);
                }
                catch (NoSuchMethodException | SecurityException | //getConstructor exceptions
                      InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException //newInstance exceptions
                      e)
                {
                    // ? nothing? test 'authorizer' for null afterwards?
                }
            }
            else { log.debug("Skipping props arg constructor because props .isEmpty"); }

            // Either we had no properties and didn't attempt that constructor, or
            // getting and using that constructor failed.  In either case 'authorizer'
            // will still be null. Try to get and use the no-arg constructor.
            if (authorizer == null)
            {
                log.debug("Trying to get the no-arg constructor for class {}", authorizerClass.getSimpleName());
                ctor = authorizerClass.getConstructor();
                authorizer = ctor.newInstance();
            }


            m.implementor = authorizerClass;
            m.instance = authorizer;
            Method method = authorizerClass.getMethod("isAuthorized", authParams);
            m.isAuthorized = method;
            method = authorizerClass.getMethod("hasPermission", permParams);
            m.hasPermission = method;
            methodCache.put(permissionName, m);
        }
        catch (NoSuchMethodException | SecurityException | ClassNotFoundException |
               InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e)
        {
            lastException = e;
            e.printStackTrace();
        }
        finally
        {
            // We may have gotten an exception before filling in all of
            // the PermissionMethod attributes. If so, we'll return the
            // special Always Deny authorizer.
            if (m.instance == null) m = permissionDenied;

            // A properly configured setup won't throw any exceptions, so any
            // exceptions we got along the way indicate a configuration error.
            if (lastException != null)
            {
                log.warn("Configuration Error: The class [{}]" +
                         " specified for permission \"{}\"" +
                         " in file {}" +
                         " could not be  instantiated, due to exception: {}" +
                         "\n\tAll permission will be denied for {}",
                         new Object[] { className, permissionName, this.configFileName, lastException.getLocalizedMessage(), permissionName });
            }
        }

        return m;
    }


    /**
     * Convert a permission name to its canonical name.
     * This is done by lowercasing the string, then replacing any spaces with underscores.
     *
     * @param permission an original permission name
     * @return the canonical name of the given permission
     */
    private String getCanonicalName(final String permission)
    {
        return permission.toLowerCase().replaceAll(" ", "_");
    }


    /**
     * A PermissionAuthorizer that always denies permission. This will be used if no
     * implementor was found for a requested permission name.
     */
    private static final PermissionAuthorizer permissionDenier = new PermissionAuthorizer()
    {
        @Override
        public boolean hasPermission(final MivPerson person, final String permissionName, final AttributeSet permissionDetails)
        {
            return false;
        }

        @Override
        public boolean isAuthorized(final MivPerson person, final String permissionName, final AttributeSet permissionDetails, final AttributeSet qualification)
        {
            return false;
        }
    };

    /**
     * An instance of a PermissionMethod that we can return to always
     * deny permission, for cases when an unknown permission is given
     * or a configured permission class is bad.
     * This encloses the 'permissionDenier' above.
     */
    private final PermissionMethod permissionDenied = new PermissionMethod(permissionDenier);


    /*
     * Cache for the looked-up methods.
     */
    private final int CACHE_SIZE = 20;
    private final int SPARES = 2;
    private final float LOAD_FACTOR = 0.9f;
    private final int INITIAL_SIZE = (Math.round(CACHE_SIZE / LOAD_FACTOR) + 1) + SPARES;

    /**
     * An LRU cache used to store the classes and methods that we've found via reflection.
     */
    private final Map<String, PermissionMethod> methodCache = Collections.synchronizedMap(
        new LinkedHashMap<String, PermissionMethod>(INITIAL_SIZE, LOAD_FACTOR, true)
        {
            private static final long serialVersionUID = 201004071109L;

            @Override
            protected boolean removeEldestEntry(final Map.Entry<String, PermissionMethod> eldest) {
                return size() > CACHE_SIZE;
            }
        }
    );


    /**
     * A cacheable object to hold a permission's implementing class, an instance,
     * and the already-resolved methods so we can avoid repeating reflection calls.
     *
     * @author Stephen Paulsen
     * @since MIV 3.5
     */
    private class PermissionMethod
    {
        PermissionMethod() {}

        /**
         * Create a PermissionMethod object based on a prototypal PermissionAuthorizer.
         * @param prototype a PermissionAuthorizer object from which to create a PermissionMethod object.
         */
        PermissionMethod(final PermissionAuthorizer prototype)
        {
            this.implementor = prototype.getClass();
            this.instance = prototype;
            try
            {
                this.isAuthorized = this.implementor.getMethod("isAuthorized", authParams);
                this.hasPermission = this.implementor.getMethod("hasPermission", permParams);
            }
            // These exceptions should never happen since we passed a valid instance of the class to begin with.
            // Maybe we could log these as WTFs if they ever happen.
            catch (SecurityException e) { }
            catch (NoSuchMethodException e) { }
        }

        /** A class that implements the PermissionAuthorizer interface. */
        Class<? extends PermissionAuthorizer> implementor;
        /** An instance of the PermissionAuthorizer implementing class, that can be used to invoke methods on. */
        PermissionAuthorizer instance;
        /** Method object to invoke to test for authorization. */
        @SuppressWarnings("unused")
        Method isAuthorized;
        /** Method object to invoke to test for permission. */
        @SuppressWarnings("unused")
        Method hasPermission;
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public void afterPropertiesSet() throws Exception
    {
        if (config == null)
        {
            log.error("Authorization service properties were not set!");
            config = new Properties();
            config.put("edit_user", "edu.ucdavis.mw.myinfovault.service.authentication.AuthTest");
        }
    }
}
