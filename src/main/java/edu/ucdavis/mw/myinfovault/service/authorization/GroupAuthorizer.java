/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: GroupAuthorizer.java
 */

package edu.ucdavis.mw.myinfovault.service.authorization;

import static edu.ucdavis.mw.myinfovault.service.person.MivRole.DEPT_STAFF;
import static edu.ucdavis.mw.myinfovault.service.person.MivRole.SCHOOL_STAFF;
import static edu.ucdavis.mw.myinfovault.service.person.MivRole.SENATE_STAFF;
import static edu.ucdavis.mw.myinfovault.service.person.MivRole.SYS_ADMIN;
import static edu.ucdavis.mw.myinfovault.service.person.MivRole.VICE_PROVOST_STAFF;

import edu.ucdavis.mw.myinfovault.util.AttributeSet;

import edu.ucdavis.mw.myinfovault.domain.group.Group;
import edu.ucdavis.mw.myinfovault.service.MivServiceLocator;
import edu.ucdavis.mw.myinfovault.service.authorization.AuthorizationService.PermissionDetail;
import edu.ucdavis.mw.myinfovault.service.authorization.AuthorizationService.Qualifier;
import edu.ucdavis.mw.myinfovault.service.person.AssignedRole;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.mw.myinfovault.service.person.Scope;

/**
 * Handles authorization for attempts to view/edit a group or its membership.
 *
 * @author Craig Gilmore
 * @since 4.3
 */
public class GroupAuthorizer extends SameScopeAuthorizer
{
    /**
     * Groups may only be viewed by Department, School, and MIV administrators
     */
    public GroupAuthorizer()
    {
        super(DEPT_STAFF, SCHOOL_STAFF, SENATE_STAFF, VICE_PROVOST_STAFF, SYS_ADMIN);
    }

    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.authorization.PermissionAuthorizer#isAuthorized(edu.ucdavis.mw.myinfovault.service.person.MivPerson, java.lang.String, edu.ucdavis.mw.myinfovault.util.AttributeSet, edu.ucdavis.mw.myinfovault.util.AttributeSet)
     */
    @Override
    public boolean isAuthorized(MivPerson person,
                                String permissionName,
                                AttributeSet permissionDetails,
                                AttributeSet qualification)
    {
        // Deny if person does not have general permission
        if (!hasPermission(person, permissionName, permissionDetails)) return false;

        // Parse target group ID
        int targetGroupId = Integer.parseInt(permissionDetails.get(PermissionDetail.TARGET_ID));

        // If target group ID is zero, it's a new group so it's allowed
        if (targetGroupId == 0) return true;

        // Retrieve target group from service
        Group targetGroup = MivServiceLocator.getGroupService().getGroup(targetGroupId);

        // If target group is null; its an invalid group
        if (targetGroup == null) return true;

        // Always allow the group creator
        if (person.equals(targetGroup.getCreator())) return true;

        // Person group must be open to the public
        if (targetGroup.getHidden() != null && targetGroup.getHidden()) return false;

        // Check view authorization for each role-scope associated with the group
        for (AssignedRole assignedRole : targetGroup.getAssignedRoles())
        {
            // The primary role for the acting person must be associated with one of the group's scopes
            if (person.getPrimaryRoleType() == assignedRole.getRole())
            {
                Scope scope = assignedRole.getScope();

                // set qualification for this scope
                qualification = new AttributeSet();
                qualification.put(Qualifier.SCHOOL, Integer.toString(scope.getSchool()));
                qualification.put(Qualifier.DEPARTMENT, Integer.toString(scope.getDepartment()));

                // check if in the same scope
                if (super.hasSameScope(person, qualification))
                {
                    /*
                     * Now that same scope has been establised, access is authorized if:
                     * - asking to view the group
                     * - or if asking to edit a group that is not read-only
                     * - or if asking to view a group's membership that is not confidential
                     */
                    return Permission.VIEW_GROUP.equals(permissionName)
                        || Permission.EDIT_GROUP.equals(permissionName) && !targetGroup.isReadOnly()
                        || Permission.VIEW_MEMBERSHIP.equals(permissionName) && !targetGroup.isConfidential();
                }
            }
        }

        // If all else has failed, deny
        return false;
    }
}
