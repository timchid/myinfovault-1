/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: IdFilter.java
 */

package edu.ucdavis.mw.myinfovault.service;

import java.util.Arrays;
import java.util.Collection;

import org.apache.commons.lang.ArrayUtils;

/**
 * Filters items by integer IDs.
 *
 * @author Craig Gilmore
 * @since MIV 4.3
 * @param <T> object that has a unique identifier
 */
public abstract class IdFilter<T> extends SearchFilterAdapter<T>
{
    private final int[] IDs;

    /**
     * Creates a filter by the given array of IDs.
     *
     * @param IDs array by which to filter
     */
    public IdFilter(int[] IDs)
    {
        Arrays.sort(IDs);
        this.IDs = IDs;
    }

    /**
     * Creates a filter by the given array of IDs.
     *
     * @param IDs array by which to filter
     */
    public IdFilter(Integer[] IDs)
    {
        this(ArrayUtils.toPrimitive(IDs));
    }

    /**
     * Creates a filter by the given collection of IDs.
     *
     * @param IDs collection by which to filter
     */
    public IdFilter(Collection<Integer> IDs)
    {
        this(IDs.toArray(new Integer[]{}));
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.SearchFilter#include(java.lang.Object)
     */
    @Override
    public boolean include(T item)
    {
        return Arrays.binarySearch(IDs, getItemId(item)) >= 0;
    }

    /**
     * Get the ID from an item by which to search the specified ID array.
     *
     * @param item An item within the collection to filter
     * @return The item ID
     */
    protected abstract int getItemId(T item);
}
