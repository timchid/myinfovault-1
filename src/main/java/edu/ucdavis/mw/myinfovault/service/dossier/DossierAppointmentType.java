/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: DossierAppointmentType.java
 */

package edu.ucdavis.mw.myinfovault.service.dossier;

/**
 * This class represents the available dossier appointment types.
 * 
 * @author Rick Hendricks
 * @since MIV 3.0
 */
public enum DossierAppointmentType 
{
    /**
     * Dossier is a primary appointment. 
     */
    PRIMARY ("Primary Appointment", "Primary"),
    
    /**
     * Dossier is a joint appointment.
     */
    JOINT ("Joint Appointment", "Joint");

    /*
     * FIXME: should be private with public getters.
     */
    public final String description;
    public final String shortDescription;
    
    /**
     * Initialize the descriptions for the appointment type.
     * 
     * @param description
     * @param shortDescription
     */
    private DossierAppointmentType(String description, String shortDescription)
    {
        this.description = description;
        this.shortDescription = shortDescription;
    }
    
}
