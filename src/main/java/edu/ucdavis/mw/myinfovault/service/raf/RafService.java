/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: RafService.java
 */

package edu.ucdavis.mw.myinfovault.service.raf;

import edu.ucdavis.mw.myinfovault.domain.action.AcademicAction;
import edu.ucdavis.mw.myinfovault.domain.dossier.Dossier;
import edu.ucdavis.mw.myinfovault.domain.raf.RafBo;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;

/**
 * Service for the recommended action form.
 *
 * @author Craig Gilmore
 * @since MIV 3.0
 */
public interface RafService
{
    /**
     * Gets RAF business object from DAO.
     * If no RAF data exists, a blank default business object is created and returned.
     *
     * @param dossier object
     * @return Recommended action form business object
     */
    public RafBo getBo(Dossier dossier);

    /**
     * Does the RAF exist for the given dossier?
     *
     * @param dossier the associated dossier
     * @return if the RAF exists for the given dossier
     */
    public boolean rafExists(Dossier dossier);

    /**
     * Update RAF decisions, the dossier, and disclosure certificate with RAF changes,
     * save to the database and re-create PDFs.
     *
     * @param raf recommended action form
     * @param targetPerson Target, switched-to user
     * @param realPerson Real, logged-in user performing update
     * @return former academic action
     */
    public AcademicAction saveRaf(RafBo raf, MivPerson targetPerson, MivPerson realPerson);

    /**
     * Delete the recommended action form PDF, remove entry from database,
     * and update the RAF present flag in the dossier.
     *
     * @param dossier Dossier associated with the RAF
     */
    public void deleteRaf(Dossier dossier);

    /**
     * Creates the PDF files for the RAF.
     *
     * @param raf Recommended action form
     */
    public void createPdfs(RafBo raf);
}
