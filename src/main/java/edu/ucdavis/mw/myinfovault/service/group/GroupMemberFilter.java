/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: GroupMemberFilter.java
 */

package edu.ucdavis.mw.myinfovault.service.group;

import java.util.Collection;

import edu.ucdavis.mw.myinfovault.domain.group.Group;
import edu.ucdavis.mw.myinfovault.service.SearchFilterAdapter;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;

/**
 * Filters out groups with a subset membership of the given collection.
 *
 * @author Craig Gilmore
 * @since 4.3
 */
public class GroupMemberFilter extends SearchFilterAdapter<Group>
{
    private Collection<MivPerson> people;

    /**
     * @param people The collection of MivPerson objects by which to filter
     */
    public GroupMemberFilter(Collection<MivPerson> people)
    {
        this.people = people;
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.SearchFilter#include(java.lang.Object)
     */
    @Override
    public boolean include(Group item)
    {
        //The group must not have a subset membership of the given collection
        return !people.containsAll(item.getMembers());
    }
}
