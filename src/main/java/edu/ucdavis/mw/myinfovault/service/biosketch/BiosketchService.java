/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: BiosketchService.java
 */

package edu.ucdavis.mw.myinfovault.service.biosketch;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.codehaus.plexus.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.ucdavis.mw.myinfovault.dao.biosketch.BiosketchDao;
import edu.ucdavis.mw.myinfovault.dao.biosketch.BiosketchDataDao;
import edu.ucdavis.mw.myinfovault.dao.biosketch.BiosketchRulesetDao;
import edu.ucdavis.mw.myinfovault.dao.biosketch.BiosketchStyleDao;
import edu.ucdavis.mw.myinfovault.dao.biosketch.PersonalDao;
import edu.ucdavis.mw.myinfovault.domain.biosketch.Biosketch;
import edu.ucdavis.mw.myinfovault.domain.biosketch.BiosketchAttributes;
import edu.ucdavis.mw.myinfovault.domain.biosketch.BiosketchData;
import edu.ucdavis.mw.myinfovault.domain.biosketch.BiosketchExclude;
import edu.ucdavis.mw.myinfovault.domain.biosketch.BiosketchSection;
import edu.ucdavis.mw.myinfovault.domain.biosketch.BiosketchSectionLimits;
import edu.ucdavis.mw.myinfovault.domain.biosketch.BiosketchStyle;
import edu.ucdavis.mw.myinfovault.domain.biosketch.Criterion;
import edu.ucdavis.mw.myinfovault.domain.biosketch.Personal;
import edu.ucdavis.mw.myinfovault.domain.biosketch.Ruleset;
import edu.ucdavis.mw.myinfovault.util.StringUtil;
import edu.ucdavis.mw.myinfovault.web.spring.biosketch.BiosketchType;
import edu.ucdavis.myinfovault.MIVUserInfo;
import edu.ucdavis.myinfovault.data.DisplaySection;
import edu.ucdavis.myinfovault.data.SectionFetcher;
import edu.ucdavis.myinfovault.designer.Availability;
import edu.ucdavis.myinfovault.format.PreviewExtender;
import edu.ucdavis.myinfovault.format.RecordFormatter;


/**
 * @author dreddy
 * @since MIV 2.1
 */
public class BiosketchService implements IBiosketchService
{
    private static final Logger logger = LoggerFactory.getLogger(BiosketchService.class);
    private BiosketchDao biosketchDao;
    private BiosketchDataDao biosketchDataDao;
    private BiosketchStyleDao biosketchStyleDao;
    private BiosketchRulesetDao biosketchRulesetDao;
    private PersonalDao personalDao;


    /**
     * @param personalDao
     */
    public void setPersonalDao(PersonalDao personalDao)
    {
        this.personalDao = personalDao;
    }

    /**
     * @param biosketchDao
     */
    public void setBiosketchDao(BiosketchDao biosketchDao)
    {
        this.biosketchDao = biosketchDao;
    }

    /**
     * @param biosketchDataDao
     */
    public void setBiosketchDataDao(BiosketchDataDao biosketchDataDao)
    {
        this.biosketchDataDao = biosketchDataDao;
    }

    /**
     * @param biosketchStyleDao
     */
    public void setBiosketchStyleDao(BiosketchStyleDao biosketchStyleDao)
    {
        this.biosketchStyleDao = biosketchStyleDao;
    }

    /**
     * @param biosketchRulesetDao
     */
    public void setBiosketchRulesetDao(BiosketchRulesetDao biosketchRulesetDao)
    {
        this.biosketchRulesetDao = biosketchRulesetDao;
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.biosketch.IBiosketchService#getBiosketchList(int, int)
     * This method returns the List of biosketch's associated with a particular user
     */
    @Override
    public List<Biosketch> getBiosketchList(int userID, int biosketchType)
    {
        List<Biosketch> biosketchList = null;

        if (userID > 0 && biosketchType > 0)
        {
            biosketchList = new ArrayList<Biosketch>();
            try
            {
                biosketchList = biosketchDao.findBiosketches(userID, biosketchType);
            }
            catch (SQLException e)
            {
                logger.error("***** Exception error in getBiosketchList()...", e);
            }
        }

        return biosketchList;
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.biosketch.IBiosketchService#getBiosketch(int)
     * This method returns a biosketch.
     * Other information associated with it is : style, ruleset, section list and attribute list information
     */
    @Override
    public Biosketch getBiosketch(int biosketchID)
    {
        Biosketch biosketch = null;
        // checks if it is a existing biosketch
        if (biosketchID <= 0)
        {
            return new Biosketch(0, 0);
        }

        try
        {
            // FINDING THE BIOSKETCH
            biosketch = biosketchDao.findBiosketch(biosketchID);

            // FINDING THE BIOSKETCH STYLE
            BiosketchStyle biosketchStyle = biosketchStyleDao.findBiosketchStyleById(biosketch.getStyleID());
            biosketch.setBiosketchStyle(biosketchStyle);

            // FINDING THE BIOSKETCH RULESET LIST ALONG WITH THE CRITERIA LIST
            //List<Ruleset> rulesetList = biosketchRulesetDao.findRulesetListWithCriteria(biosketch.getId(), biosketch.getUserID());

            List<Ruleset> rulesetList = biosketchRulesetDao.findRulesetList(biosketch.getId(), biosketch.getUserID());
            for (Ruleset ruleset1 : rulesetList)
            {
                int rulesetid = ruleset1.getId();
                List<Criterion> criteriaList = biosketchRulesetDao.findCriteriaList(rulesetid);
                ruleset1.setCriteria(criteriaList);
            }

            if (rulesetList.size() != 0)
            {
                // setting the ruleset List
                biosketch.setRuleset(rulesetList);
            }

            // FINDING THE BIOSKETCH SECTION LIST
            List<BiosketchSection> biosketchSectionList = biosketchDataDao.findBiosketchSectionsByBiosketchId(biosketch.getId());
            // setting the biosketch section list on a biosketch
            biosketch.setBiosketchSections(this.checkSectionList(biosketch, biosketchSectionList));
            // FINDING THE BIOSKETCH ATTRIBUTES LIST INFORMATION
            List<BiosketchAttributes> attributesList = biosketchDao.findBiosketchAttributes(biosketch.getId());
            // setting the biosketch attribute list on a biosketch
            biosketch.setBiosketchAttributes(this.checkAttributeList(biosketch, attributesList));
        }
        catch (SQLException e)
        {
            //logger.error("***** Exception error in getBiosketch()..." + e.getMessage()); // FIXME: DON'T LOG e.getMessage() -- LOG TRACEBACKS!!
            logger.error("Error trying to load Biosketch with ID=" + biosketchID, e);
        }

        return biosketch;
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.biosketch.IBiosketchService#createNewBiosketch(int, int)
     * This method returns a new biosketch object with default style, default section list and default attribute list information
     */
    @Override
    public Biosketch createNewBiosketch(BiosketchType biosketchType, int userID)
    {
        // create a new biosketch object
        Biosketch biosketch = new Biosketch(userID, biosketchType, 0);

        // get the default style
        BiosketchStyle biosketchStyle = getStyleDefaults(biosketchType.getTypeId());
        //set the style on biosketch
        biosketch.setBiosketchStyle(biosketchStyle);

        // set the styleid on biosketch
        biosketch.setStyleID(biosketchStyle.getId());

        // get the default section list
        List<BiosketchSection> biosketchSectionList = getSectionDefaults(biosketchType.getTypeId());
        // set the section list on biosketch
        biosketch.setBiosketchSections(biosketchSectionList);

        // get the default attributes list
        List<BiosketchAttributes> attributesList = getDefaultAttributes(biosketchType.getTypeId());
        // set the attributes list on biosketch
        biosketch.setBiosketchAttributes(attributesList);

        return biosketch;
    }


    /**
     * @param biosketchType
     * @return This method returns the default style of a biosketch
     */
    private BiosketchStyle getStyleDefaults(int biosketchType)
    {
        if (biosketchType <= 0)
        {
            return new BiosketchStyle(0, 0);
        }

        BiosketchStyle biosketchStyle = null;
        try
        {
            biosketchStyle = biosketchStyleDao.findDefaultBiosketchStyle(biosketchType);
        }
        catch (SQLException e)
        {
            logger.error("***** Exception error in getStyleDefaults()...", e);
        }

        return biosketchStyle;
    }


    /**
     * @param biosketchType
     * @return This method returns the default section list of a biosketch
     */
    private List<BiosketchSection> getSectionDefaults(int biosketchType)
    {
        List<BiosketchSection> biosketchSectionList = new ArrayList<BiosketchSection>();

        if (biosketchType > 0)
        {
            try
            {
                // finding the bioskecthsectionlimits list
                List<BiosketchSectionLimits> biosketchSectionLimitsList = biosketchDataDao
                        .findBiosketchSectionLimitsByBiosketchType(biosketchType);
                System.out.println("BiosketchSectionLimits list size is " + biosketchSectionLimitsList.size());

                // creating a map which has biosketchsectionlimits id as key and
                // biosketchsectionlimits object as value
                /*Map<Integer, BiosketchSectionLimits> biosketchSectionLimitsMap = new HashMap<Integer, BiosketchSectionLimits>();
                for (BiosketchSectionLimits biosketchSectionLimits : biosketchSectionLimitsList)
                {
                    // adding the key value pair to the map
                    biosketchSectionLimitsMap.put(biosketchSectionLimits.getSectionID(), biosketchSectionLimits);
                }*/


                for (BiosketchSectionLimits biosketchSectionLimits : biosketchSectionLimitsList)
                {
                    Availability availability = biosketchSectionLimits.getAvailability();
                    if (availability == Availability.PROHIBITED) {
                        continue;
                    }

                    BiosketchSection biosketchSection = new BiosketchSection(0);
                    // the id of this is set by the database when inserted into the database
                    // also the biosketchid on this biosketchsection should
                    // be set before inserting into the database.
                    biosketchSection.setSectionID(biosketchSectionLimits.getSectionID());

                    biosketchSection.setSectionName(biosketchSectionLimits.getSectionName());
                    biosketchSection.setAvailability(biosketchSectionLimits.getAvailability());
                    // display header is always set to "true" when the
                    // object is created for the first time
                    biosketchSection.setDisplayHeader(true);
                    biosketchSection.setMayHideHeader(biosketchSectionLimits.isMayHideHeader());
                    biosketchSection.setDisplayInBiosketch(biosketchSectionLimits.getDisplayInBiosketch());
                    biosketchSection.setMainSection(biosketchSectionLimits.isMainSection());
                    biosketchSection.setInSelectList(biosketchSectionLimits.isInSelectList());
/*
  Here's the original block ...
  It uses String comparisons instead of using the enum values,
  such as the "MANDATORY" check below.
                    // display property of a section is dependent on the availability
                    // if the availability is "mandatory", display is "true"
                    if (availability.equals("MANDATORY"))
                    {
                        // setting the display on biosketchsection
                        biosketchSection.setDisplay(true);
                    }
                    else
                    {
                        // if the availability is "optional_on", display is
                        // "true"
                        if (availability.equals("OPTIONAL_ON"))
                        {
                            // setting the display on biosketchsection
                            biosketchSection.setDisplay(true);
                        }
                        else
                        {
                            // if the availability is "optional_off, display
                            // is "false"
                            if (availability.equals("OPTIONAL_OFF"))
                            {
                                // setting the display on biosketchsection
                                biosketchSection.setDisplay(false);
                            }
                        }
                    }
*/
/*
  The first re-write changes the String comparison to enum comparison

                    if (availability == Availability.MANDATORY)
                    {
                        // setting the display on biosketchsection
                        biosketchSection.setDisplay(true);
                    }
                    else
                    {
                        // if the availability is "optional_on", display is
                        // "true"
                        if (availability == Availability.OPTIONAL_ON)
                        {
                            // setting the display on biosketchsection
                            biosketchSection.setDisplay(true);
                        }
                        else
                        {
                            // if the availability is "optional_off, display
                            // is "false"
                            if (availability == Availability.OPTIONAL_OFF)
                            {
                                // setting the display on biosketchsection
                                biosketchSection.setDisplay(false);
                            }
                        }
                    }
*/
/*
  The next re-write ...
  Now that the above block is using the enum values and "==" to compare,
  we can see this can be done simply with a switch statement.

                    switch (availability)
                    {
                        case MANDATORY:
                            biosketchSection.setDisplay(true);
                            break;
                        case OPTIONAL_ON:
                            biosketchSection.setDisplay(true);
                            break;
                        case OPTIONAL_OFF:
                            biosketchSection.setDisplay(false);
                            break;
                    }
*/
/*
  The final re-write recognizes that 'PROHIBITED' has already been dealt with,
  and won't ever occur here.
  Now we have the case to set the display, where OPTIONAL_OFF --> false and
  anything else --> true
*/
                    biosketchSection.setDisplay(availability != Availability.OPTIONAL_OFF);
/* end of rewrites */


                    biosketchSectionList.add(biosketchSection);
                }

/* Original code, which ran through the Section table and matched to the BiosketchSectionLimits table. */
              /*  // finding the section list
                List<Section> sectionList = biosketchDataDao.findSection();
                System.out.println("Section list size is " + sectionList.size());

                for (Section section : sectionList)
                {
                    // retrieving only those biosketchsectionlimits from the map
                    // whose id matches the section id
                    BiosketchSectionLimits biosketchSectionLimits = biosketchSectionLimitsMap.get(section.getId());
                    // retrieving the default biosketchsectionlimits object
                    // where the sectionid is always -1
                    if (biosketchSectionLimits == null)
                    {
                        biosketchSectionLimits = biosketchSectionLimitsMap.get(-1);
                    }
                    // if the availability property of the biosketchsectionlimit
                    // object is "prohibited" then
                    // a biosketchsection object is not populated
                    String availability = biosketchSectionLimits.getAvailability().toString();
                    if (availability.equals("PROHIBITED"))
                    {
                        BiosketchSection biosketchSection = new BiosketchSection(0);
                        // add the empty biosketchsection object to the
                        // biosketch section list
                        biosketchSectionList.add(biosketchSection);
                    }
                    // if the availability property of the biosketchsectionlimit
                    // object is not "prohibited" then
                    // a biosketchsection object is populated with the
                    // biosketchsectionlimit information
                    else
                    {
                        BiosketchSection biosketchSection = new BiosketchSection(0);
                        // the id of this is set by the database when inserted
                        // into the database
                        // also the biosketchid on this biosketchsection should
                        // be set before inserting into the database.
                        biosketchSection.setSectionID(biosketchSectionLimits.getSectionID());

                        biosketchSection.setSectionName(biosketchSectionLimits.getSectionName());
                        biosketchSection.setAvailability(biosketchSectionLimits.getAvailability());
                        // display header is always set to "true" when the
                        // object is created for the first time
                        biosketchSection.setDisplayHeader(true);
                        biosketchSection.setMayHideHeader(biosketchSectionLimits.isMayHideHeader());
                        biosketchSection.setMainSection(biosketchSectionLimits.isMainSection());
                        biosketchSection.setInSelectList(biosketchSectionLimits.isInSelectList());
                        // display property of a section is dependent on the
                        // availability
                        // if the availability is "mandatory", display is "true"
                        if (availability.equals("MANDATORY"))
                        {
                            // setting the display on biosketchsection
                            biosketchSection.setDisplay(true);
                        }
                        else
                        {
                            // if the availability is "optional_on", display is
                            // "true"
                            if (availability.equals("OPTIONAL_ON"))
                            {
                                // setting the display on biosketchsection
                                biosketchSection.setDisplay(true);
                            }
                            else
                            {
                                // if the availability is "optional_off, display
                                // is "false"
                                if (availability.equals("OPTIONAL_OFF"))
                                {
                                    // setting the display on biosketchsection
                                    biosketchSection.setDisplay(false);
                                }
                            }
                        }
                        // add the biosketchsection to the biosketchsectionlist
                        biosketchSectionList.add(biosketchSection);
                    } // else of availability="prohibited" closed
                } // for closed
*/            } // try closed
            catch (SQLException e)
            {
                logger.error("***** Exception error in getSectionDefaults()...", e);
            }
        } // if closed

        return biosketchSectionList;
    }


    /**
     * @param biosketchType
     * @return This method returns the default attributes list of a biosketch
     */
    private List<BiosketchAttributes> getDefaultAttributes(int biosketchType)
    {
        List<BiosketchAttributes> attributesList = null;

        if (biosketchType > 0)
        {
            try
            {
                attributesList = biosketchDao.findDefaultBiosketchAttributes(biosketchType);
            }
            catch (SQLException e)
            {
                logger.error("***** Exception error in getDefaultAttributes()...", e);
            }
        }

        return attributesList;
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.biosketch.IBiosketchService#saveBiosketch(edu.ucdavis.mw.myinfovault.domain.biosketch.Biosketch)
     * This method saves/updates a biosketch.
     */
    @Override
    public Biosketch saveBiosketch(Biosketch biosketch)
    {
        // check if the biosketch object passed is null
        if (biosketch == null) return null;

        try
        {
            // SAVING THE BIOSKETCHSTYLE
            int styleID = saveBiosketchStyle(biosketch.getBiosketchStyle(), biosketch.getBiosketchType().getTypeId());
            // set the auto generated styleid on biosketch
            biosketch.setStyleID(styleID);

            // SAVING THE BIOSKETCH
            int biosketchID = biosketch.getId();
            // this check makes sure that it is a new biosketch and "SAVE" is performed
            if (biosketchID == 0)
            {
                // insert the biosketch and collect the auto generated biosketchid
                biosketchID = biosketchDao.insertBiosketch(biosketch);
                // set the generated biosketchID on biosketch
                biosketch.setId(biosketchID);
            }
            // this check makes sure that it is NOT a new biosketch and "UPDATE" is performed
            else
            {
                // updates the biosketch
                biosketchDao.updateBiosketch(biosketch);
            }

            // SAVING THE BIOSKETCHSECTION LIST
            saveBiosketchSectionList(biosketchID, biosketch.getUserID(), biosketch.getBiosketchSections());

            // SAVING THE BIOSKETCHRULESET WITH BIOSKETCHCRITERIA
            saveBiosketchRulesetListWithCriteria(biosketchID, biosketch.getUserID(), biosketch.getRuleset());

            // SAVING THE BIOSKETCHATTRIBUTES
            saveBiosketchAttributesList(biosketchID, biosketch.getUserID(), biosketch.getBiosketchAttributes());

            //SAVING THE BIOSKSKETCHEXCLUDE List
            updateExcludeList(biosketch);
        }
        catch (SQLException e)
        {
            logger.error("***** Exception error in saveBiosketch()...", e);
        }

        return biosketch;
    }


    /**
     * @param biosketchStyle
     * @return
     * This method saves/updates a biosketchStyle
     */
    private int saveBiosketchStyle(BiosketchStyle biosketchStyle, int biosketchType)
    {
        int styleID = 0;
        try
        {
            // this check makes sure that it is a new biosketch style and "SAVE" is performed
            if (biosketchStyle.getId() == 0 || biosketchStyle.getId() == biosketchType)
            {
                // insert the new style information and get the styleid.
                styleID = biosketchStyleDao.insertBiosketchStyle(biosketchStyle);
            }
            // this check makes sure that it is NOT a new biosketch style and "UPDATE" is performed
            else
            {
                biosketchStyleDao.updateBiosketchStyle(biosketchStyle);
            }
        }
        catch (SQLException e)
        {
            logger.error("***** Exception error in saveBiosketchStyle()...", e);
        }
        return styleID;
    }


    /**
     * @param biosketchID
     * @param userID
     * @param biosketchSectionList
     * This method saves/updates biosketchSection list
     */
    private void saveBiosketchSectionList(int biosketchID, int userID, Iterable<BiosketchSection> biosketchSectionList)
    {
        try
        {
            for (BiosketchSection biosketchSection : biosketchSectionList)
            {
                // this check makes sure that it is a new biosketch section and "SAVE" is performed
                if (biosketchSection.getId() == 0)
                {
                    // setting the biosketchID on biosketch section before making the insertion
                    biosketchSection.setBiosketchID(biosketchID);
                    // insert the biosketchsection
                    biosketchDataDao.insertBiosketchSection(biosketchSection, userID);
                }
                // this check makes sure that it is NOT a new biosketch section and "UPDATE" is performed
                else
                {
                    // updates the biosketchsection table
                    biosketchDataDao.updateBiosketchSection(biosketchSection, userID);
                }
            }
        }
        catch (SQLException e)
        {
            logger.error("***** Exception error in saveBiosketchSectionList()...", e);
        }
    }


    /**
     * @param biosketchID
     * @param userID
     * @param rulesetList
     * This method saves/updates biosketchRuleset list with criteria list
     * note : update here is nothing but deletion followed by a insertion
     */
    private void saveBiosketchRulesetListWithCriteria(int biosketchID, int userID, Iterable<Ruleset> rulesetList)
    {
        try
        {
            if (rulesetList != null)
            {
                for (Ruleset ruleset : rulesetList)
                {
                       // we are deleting existing records and inserting new records every time we save.
                        if (ruleset.getId() != 0)
                        {
                            biosketchRulesetDao.deleteBiosketchCriteria(ruleset.getId());
                            // deletes the ruleset
                            biosketchRulesetDao.deleteBiosketchRuleset(ruleset.getId());
                        }
                        List<Criterion> criteriaList = ruleset.getCriterion();
                        Ruleset newRuleset = new Ruleset(0, userID);
                        // if criterion list is not null then only insert
                        // the ruleset and its criteria list
                        if (criteriaList != null)
                        {
                            // set the auto generated biosketchid and rectype before inserting the ruleset
                            newRuleset.setBiosketchID(biosketchID);
                            newRuleset.setRecType("PublicationSummary");
                            newRuleset.setOperator(ruleset.getOperator());

                            // inserting the ruleset record
                            int rulesetID = biosketchRulesetDao.insertBiosketchRuleset(newRuleset, userID);

                            // getting one criteria at a time
                            for (Criterion criteria : criteriaList)
                            {
                                // setting the autogenerate rulesetid on criteria before inserting the criteria
                                criteria.setRulesetID(rulesetID);
                                // inserting the criteria record
                                biosketchRulesetDao.insertBiosketchCriteria(criteria, userID);
                            }
                        }
                }
            }
        }
        catch (SQLException e)
        {
            logger.error("***** Exception error in saveBiosketchRulesetListWithCriteria()...", e);
        }
    }


    /**
     * @param biosketchID
     * @param userID
     * @param attributesList
     * This method saves/updates biosketchAttributes list
     */
    private void saveBiosketchAttributesList(int biosketchID, int userID, Iterable<BiosketchAttributes> attributesList)
    {
        try
        {
            // if the attributesList is not null, insert it.
            // note : attributes list is always null for "CV"
            if (attributesList != null)
            {
                for (BiosketchAttributes attributes : attributesList)
                {
                    // this check makes sure that it is a new biosketch attribute and "SAVE" is performed
                    if (attributes.getId() == 0)
                    {
                        // setting the biosketchID on biosketchattributes before making the insertion
                        attributes.setBiosketchID(biosketchID);
                        // insert the biosketchattributes
                        biosketchDao.insertBiosketchAttributes(attributes, userID);
                    }
                    // this check makes sure that it is a NOT new biosketch attribute and "UPDATE" is performed
                    else
                    {
                        // updates the biosketchattributes table
                        biosketchDao.updateBiosketchAttributes(attributes, userID);
                    }
                }
            }
        }
        catch (SQLException e)
        {
            logger.error("***** Exception error in saveBiosketchAttributesList()...", e);
        }
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.biosketch.IBiosketchService#deleteBiosketch(edu.ucdavis.mw.myinfovault.domain.biosketch.Biosketch)
     * This method deletes a biosketch. Information associated with it is : style, ruleset, section list and attributes list
     */
    @Override
    public void deleteBiosketch(Biosketch biosketch)
    {
        try
        {
            // check if the biosketch object passed is null
            if (biosketch != null)
            {
                // this check makes sure that there is a biosketch
                if (biosketch.getId() > 0)
                {
                    // DELETING THE BIOSKETCHCRITERIA and BIOSKETCHRULESET
                    Iterable<Ruleset> rulesetList = biosketch.getRuleset();
                    if (rulesetList != null)
                    {
                        for (Ruleset ruleset : rulesetList)
                        {
                            // deletes the criteria
                            biosketchRulesetDao.deleteBiosketchCriteria(ruleset.getId());
                            // deletes the ruleset
                            biosketchRulesetDao.deleteBiosketchRuleset(ruleset.getId());
                        }
                    }

                    // DELETING THE BIOSKETCHSECTION
                    biosketchDataDao.deleteBiosketchSection(biosketch.getId());

                    // DELETING THE BIOSKETCHATTRIBUTES
                    biosketchDao.deleteBiosketchAttributes(biosketch.getId());

                    // DELETING THE BIOSKETCHEXCLUDE
                    biosketchDataDao.deleteBiosketchExclude(biosketch.getId());

                    // DELETING THE BIOSKETCH
                    biosketchDao.deleteBiosketch(biosketch.getId());

                    // DELETING THE BIOSKETCHSTYLE
                    biosketchStyleDao.deleteBiosketchStyle(biosketch.getBiosketchStyle().getId());
                }
            } // biosketch!= null close
        } // try close
        catch (SQLException e)
        {
            logger.error("***** Exception error in deleteBiosketch()...", e);
        }
    }


    /*
     * (non-Javadoc)
     *
     * @see edu.ucdavis.mw.myinfovault.service.biosketch.IBiosketchService#duplicateBiosketch(int)
     *      This method duplicates the information associated with a existing
     *      biosketch
     */
    @Override
    public int duplicateBiosketch(int biosketchId)
    {
        int biosketchID = 0;
        try
        {
            // get the biosketch for the given biosketchid
            Biosketch biosketch = getBiosketch(biosketchId);

            // INSERTING THE BIOSKETCHSTYLE
            // insert the style information and get the styleid.
            int styleID = biosketchStyleDao.insertBiosketchStyle(biosketch.getBiosketchStyle());
            // set the auto generated styleid on biosketch
            biosketch.setStyleID(styleID);

            // new method gives back list of names. select * from Biosketch where name like 'copy%of%'+originalname;
            List<String> nameList = getBiosketchNamesForUser(biosketch.getUserID(), biosketch.getName());
            int i = 1;
            String duplicateBioName = "";
            if (nameList != null && nameList.size() > 0)
            {
                int j = nameList.size();
                boolean singleCopy = true;
                while (j>0)
                {
                    String nextName = "Copy (" + i + ") of " + biosketch.getName();
                    if (nameList.contains("Copy of " + biosketch.getName()) && singleCopy)
                    {
                        i++;
                        duplicateBioName = "Copy (" + i + ") of " + biosketch.getName();
                        singleCopy = false;
                    }
                    if (nameList.contains(nextName))
                    {
                        i++;
                        duplicateBioName = "Copy (" + i + ") of " + biosketch.getName();
                    }
                    if (!nameList.contains("Copy of " + biosketch.getName()))
                    {
                        duplicateBioName = "Copy of " + biosketch.getName();
                    }
                    j--;
                }
            }
            else
            {
                duplicateBioName = "Copy of " + biosketch.getName();
            }


/*            if (duplicateBioName.length() > 255)
            {
                duplicateBioName = duplicateBioName.substring(0, 255);
            }*/

            // setting the duplicate biosketch name.
            biosketch.setName(duplicateBioName);
            // insert the biosketch and get the auto generated biosketchid
            biosketchID = biosketchDao.insertBiosketch(biosketch);

            // INSERTING THE BIOSKETCHSECTION
            // getting the section list
            Iterable<BiosketchSection> biosketchSectionList = biosketch.getBiosketchSections();
            for (BiosketchSection biosketchSection : biosketchSectionList)
            {
                // setting the generated biosketchID on biosketch section before
                // making the insertion
                biosketchSection.setBiosketchID(biosketchID);
                // insert the biosketchsection
                biosketchDataDao.insertBiosketchSection(biosketchSection, biosketch.getUserID());
            }

            // INSERTING THE BIOSKETCHRULESET WITH BIOSKETCHCRITERIA
            // getting the ruleset list
            Iterable<Ruleset> rulesetList = biosketch.getRuleset();
            if (rulesetList != null)
            {
                for (Ruleset ruleset : rulesetList)
                {
                    List<Criterion> criteriaList = ruleset.getCriterion();
                    // if criterion list is not null then only insert
                    // the ruleset and its criteria list
                    if (criteriaList != null)
                    {
                        // set the auto generated biosketchid and rectype before inserting the ruleset
                        ruleset.setBiosketchID(biosketchID);
                        ruleset.setRecType("PublicationSummary");

                        // inserting the ruleset record
                        int rulesetID = biosketchRulesetDao.insertBiosketchRuleset(ruleset, biosketch.getUserID());

                        // getting one criteria at a time
                        for (Criterion criteria : criteriaList)
                        {
                            // setting the autogenerated rulesetid on criteria before inserting the criteria
                            criteria.setRulesetID(rulesetID);
                            // inserting the criteria record
                            biosketchRulesetDao.insertBiosketchCriteria(criteria, biosketch.getUserID());
                        }
                    }
                }
            }

            // INSERTING THE BIOSKETCHATTRIBUTES
            // getting the attributes list
            Iterable<BiosketchAttributes> attributesList = biosketch.getBiosketchAttributes();
            for (BiosketchAttributes attributes : attributesList)
            {
                // setting the generated biosketchID on biosketchattributes
                // before making the insertion
                attributes.setBiosketchID(biosketchID);
                // insert the biosketchattributes
                biosketchDao.insertBiosketchAttributes(attributes, biosketch.getUserID());
            }

            // TODO: need to implement getExcludeList instead of using loadBiosketchData.
            Biosketch biosketchWithBiosketchData = loadBiosketchData(biosketchId);
            biosketch.setBiosketchData(biosketchWithBiosketchData.getBiosketchData());
            // INSERTING THE BIOSKETCHEXCLUDE
            // getting the exclude list
            if (biosketch.getBiosketchData() != null)
            {
                List<BiosketchExclude> excludeList = biosketch.getBiosketchData().getBiosketchExcludeList();
                if (excludeList != null)
                {
                    for (BiosketchExclude biosketchExclude : excludeList)
                    {
                        // setting the generated biosketchID on biosketchExclude
                        // before making the insertion
                        biosketchExclude.setBiosketchID(biosketchID);
                    }
                    // insert the biosketchexclude
                    biosketchDataDao.insertBiosketchExcludeBatch(excludeList, biosketch.getUserID());
                }
            }
        }
        catch (SQLException e)
        {
            logger.error("***** Exception error in duplicateBiosketch()...", e);
        }
        return biosketchID;
    }


    /*
     * (non-Javadoc)
     *
     * @see edu.ucdavis.mw.myinfovault.service.biosketch.IBiosketchService#loadBiosketchDesignData(int)
     *      This method returns the Biosketch after setting BiosketchData on it.
     *      The information associated with BiosketchData is : Biosketch Exclude
     *      list and Display Section list
     */
    @Override
    public Biosketch loadBiosketchDesignData(int biosketchId)
    {
        if (biosketchId <= 0)
        {
            return new Biosketch(0, 0);
        }

        // get the Biosketch with : style information, ruleset information,
        // section list information
        Biosketch biosketch = getBiosketch(biosketchId);
        // gets the biosketch with the design data information
        return loadBiosketchDesignData(biosketch);
    }


    /*
     * (non-Javadoc)
     *
     * @see edu.ucdavis.mw.myinfovault.service.biosketch.IBiosketchService#loadBiosketchDesignData(edu.ucdavis.mw.myinfovault.domain.biosketch.Biosketch)
     *      This method returns the Biosketch after setting BiosketchData on it.
     *      The information associated with BiosketchData is : Biosketch Exclude
     *      list and Display Section list
     */
    @Override
    public Biosketch loadBiosketchDesignData(Biosketch biosketch)
    {
        return loadBiosketchDesignData(biosketch, true);
    }


    /*
     * (non-Javadoc)
     *
     * @see edu.ucdavis.mw.myinfovault.service.biosketch.IBiosketchService#loadBiosketchDesignData(edu.ucdavis.mw.myinfovault.domain.biosketch.Biosketch)
     *      This method returns the Biosketch after setting BiosketchData on it.
     *      The information associated with BiosketchData is : Biosketch Exclude
     *      list and Display Section list
     */
    private Biosketch loadBiosketchDesignData(Biosketch biosketch, boolean applyFormatting)
    {
        if (biosketch == null)
        {
            return new Biosketch(0, 0);
        }

        // Create a new BiosketchData object to set the BiosketchExcludeList and DisplaySectionList
        BiosketchData biosketchData = new BiosketchData(biosketch.getId(), biosketch.getUserID());
        try
        {
            // finding the BiosketchExcludeList
            List<BiosketchExclude> biosketchExcludeList = biosketchDataDao.findBiosketchExcludeListByBiosketchIdAndUserId(
                    biosketch.getId(), biosketch.getUserID());
            // setting the BiosketchExcludeList on biosketchData
            biosketchData.setBiosketchExcludeList(biosketchExcludeList);
          //get the Personal information in the attribute list for CV Biosketch
            if (biosketch.getBiosketchType() == BiosketchType.CV)
            {
                loadPersonalAttributes(biosketch.getUserID(), biosketch.getBiosketchAttributes());
            }

            // See whether or not to include all parent records when there are creative activities
//            String value = MIVConfig.getConfig().getProperty("creativeactivities-biosketch-includeallparents");
//            boolean includeAllParents = value != null ? (value.equalsIgnoreCase("true") ? true : false) : false;

            // getting the reference to section fetcher to retrieve the
            // display section list
            MIVUserInfo mui = new MIVUserInfo(biosketch.getUserID());
            SectionFetcher sf = mui.getSectionFetcher();
//            DataFetcher df = new DataFetcher(mui);
            List<DisplaySection> completeDisplaySectionList = new LinkedList<DisplaySection>();
            RecordFormatter[] postFromatters = new RecordFormatter[1];
            String[] fields = new String[2];
            fields[0] = "sponsor";
            fields[1] = "remark";
            postFromatters[0] = new PreviewExtender(fields);
            // for every Biosketch Section there is a Display Section List if display flag is true
            for (BiosketchSection biosketchSection : biosketch.getBiosketchSections())
            {
                //fetch records only for sections which are chosen to be displayed and when DisplayInBiosketch is 0 or 1000
                //(this flag is 1000 for pesonal sections)
                if (biosketchSection.isDisplay() &&
                   (biosketchSection.getDisplayInBiosketch() == 0 || biosketchSection.getDisplayInBiosketch() == 1000))
                {
                    // finding the displaysectionlist for a particular section
                    String sectionID = Integer.toString(biosketchSection.getSectionID());
                    List<DisplaySection> displaySectionList = null;
                    if (biosketch.getBiosketchType() == BiosketchType.CV)
                    {
                        displaySectionList = sf.getRecordSection(sectionID, applyFormatting, biosketch.getRuleset(), postFromatters);
                    }
                    else
                    {
                        displaySectionList = sf.getRecordSection(sectionID, applyFormatting, biosketch.getRuleset(), new RecordFormatter[0]);
                    }

                    if (displaySectionList != null)
                    {
                        for (DisplaySection ds : displaySectionList)
                        {
//                            ds = df.processAssociatedRecords(ds, includeAllParents);
                            ds.setShowHeader(biosketchSection.isDisplayHeader());
                            // adding each display section to the complete
                            // display section list
                            completeDisplaySectionList.add(ds);
                        }
                    }
                }
            }
            biosketchData.setSectionHeaderMap(sf.getHeaderMap());
            // setting the DisplaySectionList on biosketchData
            biosketchData.setDisplaySectionList(completeDisplaySectionList);
            logger.info("DisplaySectionList size is " + completeDisplaySectionList.size());

            // setting the BiosketchData on Biosketch
            biosketch.setBiosketchData(biosketchData);
        }
        catch (SQLException e)
        {
            logger.error("***** Exception error in loadBiosketchDesignData()...", e);
        }

        return biosketch;
    }


    /*
     * (non-Javadoc)
     *
     * @see edu.ucdavis.mw.myinfovault.service.biosketch.IBiosketchService#loadBiosketchData(int)
     */
    @Override
    public Biosketch loadBiosketchData(int biosketchId)
    {
        if (biosketchId <= 0)
        {
            return new Biosketch(0, 0);
        }

        Biosketch biosketch = loadBiosketchData(getBiosketch(biosketchId));
        return biosketch;
    }


    /*
     * (non-Javadoc)
     *
     * @see edu.ucdavis.mw.myinfovault.service.biosketch.IBiosketchService#loadBiosketchData(edu.ucdavis.mw.myinfovault.domain.biosketch.Biosketch)
     */
    @Override
    public Biosketch loadBiosketchData(Biosketch biosketch)
    {
        if (biosketch == null)
        {
            return new Biosketch(0, 0);
        }

        BiosketchData biosketchData = null;
        List<BiosketchExclude> exclusions = null;

        this.loadBiosketchDesignData(biosketch, false);

        biosketchData = biosketch.getBiosketchData();
        exclusions = biosketchData.getBiosketchExcludeList();

        BiosketchExclude excludeTest = new BiosketchExclude("", 0, 0);
        BiosketchExclude excludeAdditionalHeaderTest = new BiosketchExclude("", 0, 0);

        Iterable<DisplaySection> sections = biosketchData.getDisplaySectionList();
        for (DisplaySection section : sections)
        {
            String sectionID = section.getKey();

            Map<String, Map<String, String>> records = section.getRecords();
            List<String> removeList = new LinkedList<String>();
            for (String recordID : records.keySet())
            {
                excludeTest.setRecType(sectionID);
                excludeTest.setRecordID(Integer.parseInt(recordID));
                if (sectionID.endsWith("additional"))
                {
                    excludeAdditionalHeaderTest.setRecType(sectionID+"-header");
                    excludeAdditionalHeaderTest.setRecordID(section.getRecordNumber());
                }
                if (exclusions.contains(excludeTest)) // FIXME: This line is the trick. This might not do it.
                {
                    removeList.add(recordID);
                    //This was causing exception ConcurrentAccessException, hence building a temp list of
                    //recordIDs to be removed and removing them out of the loop.
                    //records.remove(recordID);
                }
                if (exclusions.contains(excludeAdditionalHeaderTest) && sectionID.endsWith("additional"))
                {
                    section.setHeading("");
                }

            }
            for (String recordID : removeList)
            {
                records.remove(recordID);
            }
        }
        //}
        //catch (SQLException e)
        //{
        //    logger.error("***** Exception error in loadBiosketchData()..." + e.getMessage());
        //}
        //biosketch.setBiosketchData(biosketchData);

        // TODO: process the data through the exclude list before returning

        return biosketch;
    }


    /** This is simply awful. */
    private Iterable<BiosketchAttributes> loadPersonalAttributes(int userId, Iterable<BiosketchAttributes> attributes)
    {
        Personal personal = personalDao.findById(userId);

        if (personal != null)
        {
            //set the attributes with the information in the personal object
            for (BiosketchAttributes attr : attributes)
            {
                if (attr.getName().equalsIgnoreCase("full_name"))
                {
                    attr.setValue(personal.getDisplayName());
                }
                else if (attr.getName().equalsIgnoreCase("permanent_address"))
                {
                    attr.setValue(personal.getPermanentAddress());
                }
                else if (attr.getName().equalsIgnoreCase("permanent_phone"))
                {
                    attr.setValue(personal.getPermanentPhone());
                }
                else if (attr.getName().equalsIgnoreCase("permanent_fax"))
                {
                    attr.setValue(personal.getPermanentFax());
                }
                else if (attr.getName().equalsIgnoreCase("current_address"))
                {
                    attr.setValue(personal.getCurrentAddress());
                }
                else if (attr.getName().equalsIgnoreCase("current_phone"))
                {
                    attr.setValue(personal.getHomePhone());
                }
                else if (attr.getName().equalsIgnoreCase("current_fax"))
                {
                    attr.setValue(personal.getCurrentFax());
                }
                else if (attr.getName().equalsIgnoreCase("office_address"))
                {
                    attr.setValue(personal.getOfficeAddress());
                }
                else if (attr.getName().equalsIgnoreCase("office_phone"))
                {
                    attr.setValue(personal.getOfficePhone());
                }
                else if (attr.getName().equalsIgnoreCase("office_fax"))
                {
                    attr.setValue(personal.getOfficeFax());
                }
                else if (attr.getName().equalsIgnoreCase("cell_phone"))
                {
                    attr.setValue(personal.getCellPhone());
                }
                else if (attr.getName().equalsIgnoreCase("email"))
                {
                    attr.setValue(personal.getEmail());
                }
                else if (attr.getName().equalsIgnoreCase("link"))
                {
                    attr.setValue(personal.getWebsite());
                }
                else if (attr.getName().equalsIgnoreCase("birthDate"))
                {
                    attr.setValue(personal.getBirthDate());
                }
                else if (attr.getName().equalsIgnoreCase("citizen"))
                {
                    attr.setValue(personal.getUsCitizen());
                }
                else if (attr.getName().equalsIgnoreCase("visa_type"))
                {
                    attr.setValue(personal.getVisaType());
                }
                else if (attr.getName().equalsIgnoreCase("date_entered"))
                {
                    attr.setValue(personal.getDateEntry());
                }
            }
        }
        else
        {
            logger.info("**********************Personal record not found***********************");
        }

        return attributes;
    }


    /**
     * This method updates the excludeList in the database by determining the batch of exclude objects to be
     * added/deleted to/from the BiosketchExclude table.
     * @param biosketch
     */
    private void updateExcludeList(Biosketch biosketch)
    {
        //get the excludeList of the biosketch
        if (biosketch.getBiosketchData() != null)
        {
            List<BiosketchExclude> excludeList = biosketch.getBiosketchData().getBiosketchExcludeList();
            //prepare the list of objects to be updated
            List<BiosketchExclude> insertExcludes = new LinkedList<BiosketchExclude>();
            List<BiosketchExclude> deleteExcludes = new LinkedList<BiosketchExclude>();
            for (BiosketchExclude exclude : excludeList)
            {
//                //if the exclude object id the same, continue
//                if (exclude.getIndicator()=="same") continue;
//                //if it is a new object add to the insertlist
//                if (exclude.getIndicator()=="new")
//                {
//                    //add
//                    insertExcludes.add(exclude);
//                }
//                //if it is old object add to the deletelist
//                else
//                {
//                    //remove
//                    deleteExcludes.add(exclude);
//                }
                switch (exclude.getStatus())
                {
                    case EXISTING:
                        // do nothing
                        break;
                    case ADDED:
                        insertExcludes.add(exclude);
                        break;
                    case DELETED:
                        deleteExcludes.add(exclude);
                        break;
                }
            }
            try
            {
                //call the respective daos to do the table updates
                if (insertExcludes.size() > 0) {
                    biosketchDataDao.insertBiosketchExcludeBatch(insertExcludes, biosketch.getUserID());
                }
                if (deleteExcludes.size() > 0) {
                    biosketchDataDao.deleteBiosketchExcludeBatch(deleteExcludes);
                }
            }
            catch (SQLException e)
            {
                logger.error("***** Exception error in updateExcludeList()...", e);
            }
        }
    }


    /**
     *  This method returns all the names(duplicate) of same Biosketch for a single user and useful,
     *  duplicate a biosketch.
     *  @param userID - int the list of names will be retrieved for the given userID.
     *  @param originalName - String the list of biosketch names that matach the given biosketch name.
     *  @return List -- List of names that match(basically copies of) the original biosketch name.
     */
    @Override
    public List<String> getBiosketchNamesForUser(int userID, String originalName)
    {
        List<String> nameList = new ArrayList<String>();
        try
        {
             nameList = biosketchDao.getBiosketchNamesForUser(userID, originalName);
        }
        catch (SQLException e)
        {
            //logger.error("***** Exception error in duplicateBiosketch()..." + e.getMessage());   // log TRACEBACKS!!
            logger.error("Unable to fetch biosketch duplicate names for user [" + userID + "]", e);
        }
        return nameList;
    }


    /**
     * @param rulesetList
     * This method deletes biosketchRuleset list with criteria list.
     *
     */
    public void deleteBiosketchRulesetListWithCriteria(Iterable<Ruleset> rulesetList)
    {
        try
        {
            if (rulesetList != null)
            {
                for (Ruleset ruleset : rulesetList)
                {
                    if (ruleset.getId() != 0)
                    {
                        biosketchRulesetDao.deleteBiosketchCriteria(ruleset.getId());
                        // deletes the ruleset
                        biosketchRulesetDao.deleteBiosketchRuleset(ruleset.getId());
                    }
                }
            }
        }
//        catch (Exception e) // FIXME: Don't EVER catch plain old "Exception"!       EVER!!
        catch (SQLException e)
        {
            // FIXME: handle the exception!
        }
    }


    /**
     * Check a current biosketch's section list against the default sections to make sure all defaults are
     * present and add any missing section to the input section list at the point
     * where it is discovered to be missing. This situation will occurr when there are biosketches which
     * have been created prior to the addition of any new default sections. New biosketches are not affected
     * since they are created using the default sections as a starting point.
     *
     * @param biosketchSectionList
     * @return biosketchSectionList
     * @throws SQLException
     */
    private List<BiosketchSection> checkSectionList(Biosketch biosketch, List<BiosketchSection>biosketchSectionList) throws SQLException
    {
        // Build a map of sections for check against default sections
        HashMap<String, String> sectionMap = new HashMap<String, String>();
        for (BiosketchSection biosketchSection : biosketchSectionList) {
            sectionMap.put(biosketchSection.getSectionName(), biosketchSection.getSectionID()+"");
        }

        // Check the default sections against what is returned and add any sections which are missing.
        // ** This situation will only arise with existing records when new defaults are added. **
        List<BiosketchSectionLimits> defaultSectionsList = biosketchDataDao.findBiosketchSectionLimitsByBiosketchType(biosketch.getBiosketchType().getTypeId());
        BiosketchSectionLimits biosketchSectionLimits = null;
        // Iterate by element to insert missing section at specific location.
        for (int i = 0; i < defaultSectionsList.size() ; i++)
        {
            biosketchSectionLimits = defaultSectionsList.get(i);
            if (sectionMap.containsKey(biosketchSectionLimits.getSectionName())) {
                continue;
            }
            // This section is not present, add it
            BiosketchSection biosketchSection = new BiosketchSection(biosketchSectionLimits);
            biosketchSectionList.add(i,biosketchSection);
        }
        return biosketchSectionList;
    }


    /**
     * Check a current biosketch's attribute list against the default attributes to make sure all defaults are
     * present and add any missing attribute to the input attribute list at the point
     * where it is discovered to be missing. This situation will occur when there are biosketches which
     * have been created prior to the addition of any new default attributes. New biosketches are not affected
     * since they are created using the default attributes as a starting point.
     *
     * @param biosketch
     * @param attributesList
     * @return biosketchSectionList
     * @throws SQLException
     */
    private List<BiosketchAttributes> checkAttributeList(Biosketch biosketch, List<BiosketchAttributes> attributesList) throws SQLException
    {
        // Build a map of attributes for check against default attributes
        HashMap<String, String> attributeMap = new HashMap<String, String>();
        for (BiosketchAttributes biosketchAttributes : attributesList) {
            attributeMap.put(biosketchAttributes.getName(), biosketchAttributes.getValue());
        }

        // Check the default attributes against what is returned and add any attributes which are missing.
        // ** This situation will only arise with existing records when new defaults are added. **

        List<BiosketchAttributes> defaultAttributesList = biosketchDao.findDefaultBiosketchAttributes(biosketch.getBiosketchType().getTypeId());
        BiosketchAttributes defaultBiosketchAttributes = null;

        // Iterate by element to insert missing section at specific location.
        for (int i = 0; i < defaultAttributesList.size() ; i++)
        {
            defaultBiosketchAttributes = defaultAttributesList.get(i);
            if (attributeMap.containsKey(defaultBiosketchAttributes.getName())) {
                continue;
            }
            // This attribute is not present, add it
            BiosketchAttributes biosketchAttributes = new BiosketchAttributes(defaultBiosketchAttributes);
            // If this is the "programdirector_name", initialize the data
            if (defaultBiosketchAttributes.getName().equalsIgnoreCase("programdirector_name")) {
                MIVUserInfo mui = new MIVUserInfo(biosketch.getUserID());
                biosketchAttributes.setValue(mui.getPerson().getSurname()+", "+mui.getPerson().getGivenName()+(StringUtils.isBlank(mui.getPerson().getMiddleName())? StringUtil.EMPTY_STRING : ", "+mui.getPerson().getMiddleName()));
            }
            attributesList.add(i,biosketchAttributes);
        }
        return attributesList;
    }

}
