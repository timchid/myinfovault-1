/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: EditDossierAuthorizer.java
 */

package edu.ucdavis.mw.myinfovault.service.authorization;

import java.util.Set;

import edu.ucdavis.mw.myinfovault.util.AttributeSet;

import edu.ucdavis.mw.myinfovault.domain.action.DossierLocation;
import edu.ucdavis.mw.myinfovault.service.authorization.AuthorizationService.Qualifier;
import edu.ucdavis.mw.myinfovault.service.person.AssignedRole;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.mw.myinfovault.service.person.MivRole;
import edu.ucdavis.mw.myinfovault.service.person.Scope;

/**<p>
 * Authorizes the routing of dossiers by checking that the target dossier is within the qualification
 * scope of actor's appointments.</p>
 * <p>The qualification scope varies based on the actors role, for example a department administrator may
 * edit dossiers within be same-school and same-department as any of their appointments, while a
 * school administrator edit dossiers within the same-school (any department is acceptable) as any of their
 * appointments.</p>
 * <p>Requires input of the <code>SCHOOL</code> and <code>DEPARTMENT</code> qualification parameter</p>
 * <p>Requires input of the <code>DOSSIERLOCATION</code> permissionDetails parameter</p>
 *
 * @author rhendric
 * @since MIV 4.0
 */
public class RouteDossierAuthorizer extends SameScopeAuthorizer implements PermissionAuthorizer
{
    /** Roles that are allowed to edit dossiers (Manage Open Action) */
    private static final MivRole[] mayRouteDossierRoles = {
        MivRole.DEPT_STAFF,
        MivRole.SCHOOL_STAFF,
        MivRole.VICE_PROVOST_STAFF,
        MivRole.SYS_ADMIN,
        MivRole.SENATE_STAFF,
        MivRole.DEAN,
        MivRole.DEPT_CHAIR,
    };
    // These roles are ordered from most to least frequently occurring.


    @Override
    public boolean hasPermission(MivPerson person, String permissionName, AttributeSet permissionDetails)
    {
        if (person.hasRole(mayRouteDossierRoles))
        {
           return validatePermissionDetails(person, permissionDetails);
        }
        return false;
    }
    
    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.authorization.SameScopeAuthorizer#isAuthorized(edu.ucdavis.mw.myinfovault.service.person.MivPerson, java.lang.String, edu.ucdavis.mw.myinfovault.util.AttributeSet, edu.ucdavis.mw.myinfovault.util.AttributeSet)
     * <p>A department administrator may route a dossier within be same-school and same-department as any of their appointments.
     * A school administrator or Dean may route a dossier within the same-school (any department is acceptable)
     * A Dept Chair may route a dossier within be same-school and same-department of the school/department for which they are dept chair.
     * as any of their appointments.</p>
     */
    @Override
    public boolean isAuthorized(MivPerson person, String permissionName, AttributeSet permissionDetails, AttributeSet qualification)
    {
        // If role has permission, check if authorized based on department and school
        if (this.hasPermission(person, permissionName, permissionDetails))
        {
            Scope scope = new Scope(Integer.parseInt(qualification.get(Qualifier.SCHOOL)),
                                    Integer.parseInt(qualification.get(Qualifier.DEPARTMENT)));

//           MivRole mivPrimaryRole = person.getPrimaryRoleType();

            switch (person.getPrimaryRoleType())
            {
                // SYS_ADMIN, Vice provost, and Senate roles can route all
                case SYS_ADMIN:
                case VICE_PROVOST_STAFF:
                    return true;
                case SCHOOL_STAFF:
                    if (hasSharedScope(person, scope))
                    {
                            return true;
                    }
                    break;
                case SENATE_STAFF:
                     return true;
              
                // Check for DEAN or DEPT_CHAIR
                case CANDIDATE:
                    Set <AssignedRole>assignedRoles = person.getAssignedRoles();
                    // Candidate can have both the DEAN *and* DEPT_CHAIR roles and may have more than one of each
                    if (person.hasRole(MivRole.DEAN, MivRole.DEPT_CHAIR))
                    {
                        if (person.hasRole(MivRole.DEAN))
                        {
                            // Check each potential DEAN assignment. If the DEAN scope matches the qualifier scope as
                            // well as the location of SCHOOL or FEDERATIONSCHOOL, then routing is authorized
                            for(AssignedRole assignedRole : assignedRoles)
                            {
                                if (assignedRole.getRole() == MivRole.DEAN &&
                                    assignedRole.getScope().matches(scope))
                                {
                                    return true;
                                }
                            }
                        }
                        if (person.hasRole(MivRole.DEPT_CHAIR))
                        {
                            // Check each potential DEPT_CHAIR assignment. If the DEPT_CHAIR scope matches the qualifier scope as
                            // well as the location of DEPARTMENT, then routing is authorized
                            for (AssignedRole assignedRole : assignedRoles)
                            {
                                if (assignedRole.getRole() == MivRole.DEPT_CHAIR &&
                                    assignedRole.getScope().matches(scope))
                                {
                                    return true;
                                }
                            }
                        }
                    }
                    break;

                // DEPT_STAFF
                case DEPT_STAFF:
                    if (hasSharedScope(person, scope))
                    {
                       return true;
                    }

                default:
                    return false;
            }
        }
        return false;
    }

    private boolean validatePermissionDetails(MivPerson person, AttributeSet permissionDetails)
    {
        
        if (permissionDetails == null)
        {
            return true;
        }
        
        DossierLocation dossierLocation = null;
        
        // Get the dossier location. The location at which a dossier may be routed is based on role.
        String location = permissionDetails.get("DOSSIERLOCATION");
        // Route dossier permission must specify a location
        if (location == null) {
             return false;
         }
      
        dossierLocation = DossierLocation.mapWorkflowNodeNameToLocation(location);
        
        switch (person.getPrimaryRoleType())
        {
            // SYS_ADMIN, Vice provost, and Senate roles can route all
            case SYS_ADMIN:
            case VICE_PROVOST_STAFF:
                return true;
            case SCHOOL_STAFF:
                // If the location is specified, the School_Staff role can only route at department, school locations
                if (dossierLocation == null ||
                    (dossierLocation == DossierLocation.DEPARTMENT ||
                     dossierLocation == DossierLocation.SCHOOL ||
                     dossierLocation == DossierLocation.POSTSENATESCHOOL ||
                     dossierLocation == DossierLocation.POSTAPPEALSCHOOL ||
                     dossierLocation == DossierLocation.POSTAUDITREVIEW ))
                {
                     return true;
                }
                break;
            case SENATE_STAFF:
                // If the location is specified, the Senate_Staff role can at senate locations
                if (dossierLocation != null && 
                   (dossierLocation == DossierLocation.SENATE_OFFICE ||
                    dossierLocation == DossierLocation.FEDERATION ||
                    dossierLocation == DossierLocation.SENATEFEDERATION ||
                    dossierLocation == DossierLocation.SENATEAPPEAL || 
                    dossierLocation == DossierLocation.FEDERATIONAPPEAL ||
                    dossierLocation == DossierLocation.FEDERATIONSENATEAPPEAL))
                    {
                        return true;
                    }
                break;

            // Check for DEAN or DEPT_CHAIR
            case CANDIDATE:
                Set <AssignedRole>assignedRoles = person.getAssignedRoles();
                // Candidate can have both the DEAN *and* DEPT_CHAIR roles and may have more than one of each
                if (person.hasRole(MivRole.DEAN, MivRole.DEPT_CHAIR))
                {
                    if (person.hasRole(MivRole.DEAN))
                    {
                        // Check each potential DEAN assignment. If the DEAN scope matches the qualifier scope as
                        // well as the location of SCHOOL or FEDERATIONSCHOOL, then routing is authorized
                        for(AssignedRole assignedRole : assignedRoles)
                        {
                            if (assignedRole.getRole() == MivRole.DEAN &&
                                (dossierLocation == null ||
                                dossierLocation == DossierLocation.SCHOOL ||
                                dossierLocation == DossierLocation.POSTSENATESCHOOL ||
                                dossierLocation == DossierLocation.POSTAPPEALSCHOOL ||
                                dossierLocation == DossierLocation.POSTAUDITREVIEW ))
                            {
                                return true;
                            }
                        }
                    }
                    if (person.hasRole(MivRole.DEPT_CHAIR))
                    {
                        // Check each potential DEPT_CHAIR assignment. If the DEPT_CHAIR scope matches the qualifier scope as
                        // well as the location of DEPARTMENT, then routing is authorized
                        for (AssignedRole assignedRole : assignedRoles)
                        {
                            if (assignedRole.getRole() == MivRole.DEPT_CHAIR &&
                                // If the location is specified, the Dept_CHAIR role can only route at department location
                                (dossierLocation == null ||
                                 dossierLocation == DossierLocation.DEPARTMENT))
                            {
                                return true;
                            }
                        }
                    }
                }
                break;

            // DEPT_STAFF
            case DEPT_STAFF:
                 // If the location is specified, the Dept_Staff role can only route at department location
                 if (dossierLocation == null || dossierLocation == DossierLocation.DEPARTMENT) {
                     return true;
                 }
            default:
                return false;
        }
        
        return false;
    }
}
