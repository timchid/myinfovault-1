/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: SchoolDepartmentSuggestionHandler.java
 */

package edu.ucdavis.mw.myinfovault.service.suggest;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import org.apache.commons.lang.StringUtils;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.ImmutableSet.Builder;
import com.google.common.eventbus.Subscribe;

import edu.ucdavis.mw.myinfovault.events.ConfigurationChangeEvent;
import edu.ucdavis.mw.myinfovault.events.EventDispatcher;
import edu.ucdavis.mw.myinfovault.service.person.AssignedRole;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.mw.myinfovault.service.person.MivRole;
import edu.ucdavis.mw.myinfovault.service.person.Scope;
import edu.ucdavis.myinfovault.MIVConfig;

/**
 * Serves school - department suggestions
 *
 * <p>
 * The search term may optionally contain one or many school department scopes in the form:
 * <code>term|scope,scope1,...scopen<code> where the scopes take the form
 * <strong>schoolID:departmntID</strong>.
 * </p>
 *
 * <p>
 * Example: <strong>SOM|1:1,1:204,1:290</strong></br>
 *
 * The above search term would return the following suggestions:</br>
 *
 * <pre>
 * {"value"        : "1:1",
 *  "label"        : "SOM - Internal Medicine",
 *  "school"       : "School of Medicine",
 *  "schoolabbrev" : "SOM",
 *  "description"  : "Internal Medicine",
 *  "abbreviation" : "",
 *  "schoolid"     : 1,
 *  "departmentid  : 1}
 *  </pre>
 *
 * <pre>
 * {"value"        : "1:204",
 *  "label"        : "SOM - Physiology and Membrane Biology",
 *  "school"       : "School of Medicine",
 *  "schoolabbrev" : "SOM",
 *  "description"  : "Physiology and Membrane Biology",
 *  "abbreviation  : "",
 *  "schoolid"     : 1,
 *  "departmentid  : 204}
 *  </pre>
 *
 * <pre>
 * {"value"        : "1:290",
 *  "label"        : "SOM - Neurology",
 *  "school"       : "School of Medicine",
 *  "schoolabbrev" : "SOM",
 *  "description"  : "Neurology",
 *  "abbreviation  : "",
 *  "schoolid"     : 1,
 *  "departmentid  : 290}
 * </pre>
 * </p>
 *
 * School/Department abbreviations are returned when available, however either the abbreviation or
 * full school/department may be input. For example, the same results as above would be returned for
 * a search term of <strong>School of Medicine|1:1,1:204,1:290<strong>
 *
 * @author Rick Hendricks
 * @since MIV 4.8
 */
public class SchoolDepartmentSuggestionHandler extends AbstractPersonAwareSuggestionHandler<Map<String,String>>
{
    // department map keys
    private static final String SCHOOL_DESCRIPTION = "school";
    private static final String SCHOOL_ABBREVIATION = "schoolabbrev";
    private static final String DEPARTMENT_DESCRIPTION = "description";

    /*
     * Put the universe of possible department scope-to-map entries
     * into a set sorted by school and department description.
     */
    private static final SortedSet<Map.Entry<String, Map<String,String>>> DEPARTMENTS = Collections.synchronizedSortedSet(
        new TreeSet<Map.Entry<String, Map<String, String>>>(
            new Comparator<Map.Entry<String, Map<String, String>>>() {
                @Override
                public int compare(Entry<String, Map<String, String>> e1, Entry<String, Map<String, String>> e2) {
                    // compare school name
                    int comparison = e1.getValue().get(SCHOOL_DESCRIPTION).compareTo(e2.getValue().get(SCHOOL_DESCRIPTION));

                    /*
                     * Return department name comparison if school names are
                     * equal, otherwise, return the school name comparison.
                     */
                    return comparison != 0
                         ? comparison
                         : e1.getValue().get(DEPARTMENT_DESCRIPTION).compareTo(e2.getValue().get(DEPARTMENT_DESCRIPTION));
                }
            }
        )
    );
    static {
        loadDepartments(null);
    }

    /**
     * Create a suggestion handler for the given handler name, topic, and properties.
     *
     * @param name handler name
     * @param topic handler topic
     * @param p handler properties
     */
    public SchoolDepartmentSuggestionHandler(String name, String topic, Properties p)
    {
        super(name, topic, p);

        // registration for ConfigurationChangeEvent subscription
        EventDispatcher.getDispatcher().register(this);
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.suggest.AbstractSuggestionHandler#getNewSuggestions(java.lang.String, java.lang.String)
     */
    @Override
    protected Iterable<Map<String,String>> getNewSuggestions(String topic, String term)
    {
        return getNewSuggestions(term, Collections.singleton(Scope.AnyScope));
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.suggest.AbstractMivPersonAwareSuggestionHandler#getNewSuggestions(edu.ucdavis.mw.myinfovault.service.person.MivPerson, java.lang.String, java.lang.String)
     */
    @Override
    public Iterable<Map<String,String>> getNewSuggestions(MivPerson person, String topic, String term)
    {
        // Non-scoped suggestions for system administrators and vice provost staff
        if (person.hasRole(MivRole.SYS_ADMIN, MivRole.VICE_PROVOST_STAFF))
        {
            return getNewSuggestions(topic, term);
        }
        // otherwise, get scoped suggestions
        else
        {
            Set<AssignedRole> roles = person.getAssignedRoles();

            // no suggestions for no assigned roles
            if (roles.isEmpty()) return Collections.emptySet();

            Builder<Scope> scopes = ImmutableSet.<Scope>builder();

            for (AssignedRole role : roles)
            {
                scopes.add(role.getScope());
            }

            return getNewSuggestions(term, scopes.build());
        }
    }

    private Iterable<Map<String,String>> getNewSuggestions(String searchTerm, Collection<Scope> scopes)
    {
        /*
         * Filter departments by scope and search term.
         */
        List<Map.Entry<String, Map<String,String>>> filtered = new DepartmentSuggestionFilter(searchTerm, scopes).apply(
                                                                   new ArrayList<Map.Entry<String, Map<String,String>>>(DEPARTMENTS));

        List<Map<String, String>> suggestions = new ArrayList<Map<String, String>>(filtered.size());

        /*
         * Add the entry value for each department passing the search term and scope filter.
         */
        for (Map.Entry<String, Map<String,String>> entry : filtered)
        {
            suggestions.add(entry.getValue());
        }

        return suggestions;
    }

    /**
     * @param event configuration event triggering this execution
     */
    @Subscribe
    public static void loadDepartments(ConfigurationChangeEvent event)
    {
        DEPARTMENTS.clear();
        DEPARTMENTS.addAll(MIVConfig.getConfig().getMap("departments").entrySet());

        /*
         * Create a new value map for each entry.
         *
         * Each new value map is composed of:
         *  - the current value map (from the MIV configuration department properties)
         *  - entry key mapped to "value"
         *  - school and department description mapped to "label"
         */
        for (Map.Entry<String, Map<String, String>> entry : DEPARTMENTS)
        {
            String departmentDescription = entry.getValue().get(DEPARTMENT_DESCRIPTION);
            String schoolAbbreviation = entry.getValue().get(SCHOOL_ABBREVIATION);

            String label = entry.getValue().get(SCHOOL_DESCRIPTION);

            if (StringUtils.isNotBlank(departmentDescription))
            {
                // use school abbreviation instead if available
                if (StringUtils.isNotBlank(schoolAbbreviation))
                {
                    label = schoolAbbreviation;
                }

                // append hyphen and department description
                label += " - " + departmentDescription;
            }

            entry.setValue(ImmutableMap.<String, String>builder()
                                       .putAll(entry.getValue())
                                       .putAll(getSuggestion(label, entry.getKey()))
                                       .build());
        }
    }
}
