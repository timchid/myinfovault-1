/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: IamPersonDataSource.java
 */

package edu.ucdavis.mw.myinfovault.service.person;

import java.util.Collection;


/**
 * Hook up to the UCD Identity and Access Management (IAM) service
 * @author Stephen Paulsen
 * @since MIV 4.6
 */
public class IamPersonDataSource implements PersonDataSource
{

    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.person.PersonDataSource#getPersonFromId(java.lang.String)
     */
    @Override
    public PersonDTO getPersonFromId(String personId)
    {
        // TODO Auto-generated method stub
        return null;
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.person.PersonDataSource#getPersonFromPrincipal(java.lang.String)
     */
    @Override
    public PersonDTO getPersonFromPrincipal(String principalName)
    {
        // TODO Auto-generated method stub
        return null;
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.person.PersonDataSource#getPersonFromEmail(java.lang.String)
     */
    @Override
    public Collection<PersonDTO> getPersonFromEmail(String email)
    {
        // TODO Auto-generated method stub
        return null;
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.person.PersonDataSource#isActive(java.lang.String)
     */
    @Override
    public boolean isActive(String personId)
    {
        PersonDTO p = this.getPersonFromId(personId);
        return p != null && p.isActive();
    }

}
