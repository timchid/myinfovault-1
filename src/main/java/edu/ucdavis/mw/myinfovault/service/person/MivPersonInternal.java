/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: MivPersonInternal.java
 */


package edu.ucdavis.mw.myinfovault.service.person;

/**<p>
 * Extended access to an MivPerson for internal use, so DAOs, DTOs, and Impls can do their jobs.
 * SHOULDN'T BE PUBLIC but EditAction for editing users needs to use it.
 * Don't use this anywhere else without documenting it here.</p>
 * <p>Used by:</p><ul>
 * <li>EditAction</li>
 * <li>EditAcademicAction</li>
 * </ul>
 *
 * @author Stephen Paulsen
 * @since MIV 3.0
 */
public interface MivPersonInternal extends MivPerson
{
    void setUserId(int userId);
    void setPrincipalName(String principalName);
    void setPersonId(String personId);

//    void setOfficialEmail(String email);
//    String getOfficialEmail();
    void addEmailAddress(String email);
    String removeEmailAddress(String email);

    void setGivenName(String name);
    void setMiddleName(String name);
    void setSurname(String name);
    void setSuffix(String suffix);
    void setDisplayName(String name);

    MivPersonInternal clearAppointments();
    MivPersonInternal clearAssignments();
    MivPersonInternal clearRoles();
    void setName(String givenName, String middleName, String surname, String suffix);
}
