/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: SignableService.java
 */

package edu.ucdavis.mw.myinfovault.service;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import edu.ucdavis.mw.myinfovault.domain.dossier.Dossier;
import edu.ucdavis.mw.myinfovault.service.dossier.DossierService;
import edu.ucdavis.mw.myinfovault.service.dossier.WorkflowException;
import edu.ucdavis.mw.myinfovault.util.StringUtil;
import edu.ucdavis.mw.myinfovault.util.XmlManager;
import edu.ucdavis.myinfovault.MIVConfig;
import edu.ucdavis.myinfovault.document.DocumentFormat;
import edu.ucdavis.myinfovault.document.DocumentGenerator;
import edu.ucdavis.myinfovault.document.DocumentGeneratorFactory;
import edu.ucdavis.myinfovault.exception.MivSevereApplicationError;

/**
 * Shared service for generating PDF documents.
 *
 * @author Craig Gilmore
 * @since MIV 4.8
 */
public abstract class DocumentService
{
    /**
     * Path to XSLT directory from MIV configuration.
     */
    protected static final File XSLT_DIR = new File(MIVConfig.getConfig().getProperty("document-config-baselocation-xslt")
                                                  + File.separator
                                                  + MIVConfig.getConfig().getProperty("document-config-location-xslt-packet"));

    /**
     * Date instance of {@link DateFormat#LONG}, e.g. "February 6, 1911".
     */
    protected final ThreadLocal<DateFormat> longDateFormat =
            new ThreadLocal<DateFormat>() {
            @Override protected DateFormat initialValue() {
                return SimpleDateFormat.getDateInstance(DateFormat.LONG);
            }
        };

    /**
     *  Spring injected dossier service.
     */
    protected DossierService dossierService = null;

    /**
     * @param dossierService Spring injected dossier service
     */
    public void setDossierService(DossierService dossierService)
    {
        this.dossierService = dossierService;
    }

    /**
     * Generate the PDF for the given DOM and XSLT style.
     *
     * @param pdfFile path to the generated PDF
     * @param xsltFilename filename of the document style found in {@link XSLT_DIR}
     * @param document DOM element to write to XML file
     * @return File path to the generated PDF
     */
    protected File createPdf(final File pdfFile,
                             final String xsltFilename,
                             final Document document)
    {
        // the XML will have the same name and directory, but different extension
        File xmlFile = new File(pdfFile.getParent(),
                                FilenameUtils.getBaseName(pdfFile.getName())
                              + "."
                              + DocumentFormat.XML.getFileExtension());

        // build XSLT path
        File xsltFile = new File(XSLT_DIR, xsltFilename);

        try
        {
            //create parent directories for PDF and XML if DNE
            pdfFile.getParentFile().mkdirs();

            final DocumentGenerator generator = DocumentGeneratorFactory.getFactory().getGenerator(DocumentFormat.PDF);

            BufferedWriter out = null;
            try
            {
                out = new BufferedWriter(new FileWriter(xmlFile));
                out.write(XmlManager.getXmlStr(document));
            }
            catch (final IOException exception)
            {
                throw new MivSevereApplicationError("errors.decision.xml.write", exception, xmlFile);
            }
            finally
            {
                if (out != null) out.close();
            }

            if (!generator.generate(xsltFile, xmlFile, pdfFile))
            {
                throw new MivSevereApplicationError("errors.decision.pdf.write", pdfFile);
            }
        }
        catch (IOException e)
        {
            throw new MivSevereApplicationError("errors.decision.xml.close", e, xmlFile);
        }
        catch (SecurityException e)
        {
            throw new MivSevereApplicationError("errors.decision.pdf.directory", e, pdfFile.getParentFile());
        }

        return pdfFile;
    }

    /**
     * @return a new document object
     */
    protected Document getDocument()
    {
        try
        {
            // Create new document
            return DocumentBuilderFactory.newInstance()
                                         .newDocumentBuilder()
                                         .getDOMImplementation()
                                         .createDocument(null, null, null);
        }
        catch (final ParserConfigurationException exception)
        {
            throw new MivSevereApplicationError("errors.raf.pdf.xmlNode", exception);
        }
    }

    /**
     * Returns a new node containing a text node.
     *
     * @param doc
     * @param name
     * @param value
     * @return DOM element for the given name and value
     */
    protected Element getElement(final Document doc,
                                 final String name,
                                 final String value)
    {
        return getElement(doc, name, value, null);
    }

    /**
     * Returns a new node containing a text node with label attribute.
     *
     * @param doc
     * @param name
     * @param value
     * @param label
     * @return DOM element for the given name and value
     */
    protected Element getElement(final Document doc,
                                 final String name,
                                 final Object value,
                                 final String label)
    {
        // Create element
        Element elem = doc.createElement(name);

        if (StringUtils.isNotEmpty(label))
        {
            elem.setAttribute("label", label);
        }

        // Append text node
        elem.appendChild(doc.createTextNode(value.toString()));

        return elem;
    }

    /**
     * Returns a new node containing an empty text node.
     *
     * @param doc
     * @param name
     * @return DOM element for the given name
     */
    protected Element getElement(final Document doc,
                                 final String name)
    {
        return getElement(doc, name, StringUtil.EMPTY_STRING);
    }

    /**
     * Get the dossier for the given ID.
     *
     * @param dossierId dossier ID
     * @return dossier for the given ID
     */
    protected Dossier getDossier(long dossierId)
    {
        try
        {
            return dossierService.getDossier(dossierId);
        }
        catch (WorkflowException exception)
        {
            throw new MivSevereApplicationError("errors.raf.dossier.read2", exception, dossierId);
        }
    }
}
