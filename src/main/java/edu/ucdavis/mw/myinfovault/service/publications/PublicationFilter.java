/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: PublicationFilter.java
 */

package edu.ucdavis.mw.myinfovault.service.publications;

import java.util.Collection;

import edu.ucdavis.mw.myinfovault.domain.publications.Publication;
import edu.ucdavis.mw.myinfovault.service.IdFilter;

/**
 * Filters publications by an array of publication external IDs.
 *
 * @author Craig Gilmore
 * @since MIV 3.9.1
 */
public class PublicationFilter extends IdFilter<Publication>
{
    /**
     * Create a publication ID filter.
     *
     * @param publicationIds Collection of publications IDs by which to filter
     */
    public PublicationFilter(Collection<Integer> publicationIds)
    {
        super(publicationIds.toArray(new Integer[publicationIds.size()]));
    }

    /**
     * Create a publication ID filter.
     *
     * @param publicationIds Array of publications IDs by which to filter
     */
    public PublicationFilter(Integer[] publicationIds)
    {
        super(publicationIds);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected int getItemId(Publication item)
    {
        return Integer.parseInt(item.getExternalId());
    }
}
