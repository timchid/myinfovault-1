/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: EditDisclosureAuthorizer.java
 */


package edu.ucdavis.mw.myinfovault.service.authorization;


import edu.ucdavis.mw.myinfovault.util.AttributeSet;

import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.mw.myinfovault.service.person.MivRole;

/**<p>
 * Authorizes the edit action when the actor and the target user disclosure certificate are within the same scope.</p>
 * <p>Currently the actors role must either be vice provost or department administrator. The vice provost role can
 * edit any disclosure certificate while a department administrator must have a scope that must be same-school and
 * same-department.</p>
 * <p>Requires that the qualification parameters <code>SCHOOL</code>, and <code>DEPARTMENT</code>
 * are supplied.</p>
 *
 * @author Rick Hendricks
 * @since MIV 4.0
 */
public class EditDisclosureAuthorizer extends SameScopeAuthorizer implements PermissionAuthorizer
{

    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.authorization.SameScopeAuthorizer#hasPermission(edu.ucdavis.mw.myinfovault.service.person.MivPerson, java.lang.String, edu.ucdavis.mw.myinfovault.util.AttributeSet)
     */
    @Override
    public boolean hasPermission(MivPerson person, String permissionName, AttributeSet permissionDetails)
    {
        // Only Vice Provost office and department admins can edit the disclosure statement.
        return person.hasRole(MivRole.VICE_PROVOST_STAFF, MivRole.SCHOOL_STAFF, MivRole.DEPT_STAFF, MivRole.DEPT_CHAIR);
    }

    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.authentication.PermissionAuthorizer#isAuthorized(edu.ucdavis.mw.myinfovault.service.person.MivPerson, java.lang.String, edu.ucdavis.mw.myinfovault.util.AttributeSet, edu.ucdavis.mw.myinfovault.util.AttributeSet)
     */
    @Override
    public boolean isAuthorized(MivPerson person, String permissionName, AttributeSet permissionDetails, AttributeSet qualification)
    {
        // If role has permission, check if authorized based on department and school qualification
        return this.hasPermission(person, permissionName, permissionDetails) && hasSharedScope(person, qualification);
//        if (this.hasPermission(person, permissionName, permissionDetails) && hasSharedScope(person,qualification))
//        {
//            return true;
//        }
//        return false;
    }

}
