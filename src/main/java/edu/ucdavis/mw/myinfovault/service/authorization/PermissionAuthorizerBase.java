/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: PermissionAuthorizerBase.java
 */


package edu.ucdavis.mw.myinfovault.service.authorization;


import edu.ucdavis.mw.myinfovault.util.AttributeSet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * @author Stephen Paulsen
 * @since MIV 4.0
 */
public abstract class PermissionAuthorizerBase implements PermissionAuthorizer
{
    /**
     * Logger object available to all subclasses.
     */
    Logger log = LoggerFactory.getLogger(this.getClass());


    /**
     * Check to see if any permission details were passed.
     * @param permissionDetails the AttributeSet of details
     * @return {@code true} if there are any detail entries, {@code false} if null or empty.
     */
    public boolean hasPermissionDetails(AttributeSet permissionDetails)
    {
        return (permissionDetails != null && permissionDetails.size() > 0);
    }
}
