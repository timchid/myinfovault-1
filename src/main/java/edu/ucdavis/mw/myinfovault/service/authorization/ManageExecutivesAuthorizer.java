/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: ManageExecutivesAuthorizer.java
 */


package edu.ucdavis.mw.myinfovault.service.authorization;

import edu.ucdavis.mw.myinfovault.util.AttributeSet;

import edu.ucdavis.mw.myinfovault.service.authorization.AuthorizationService.PermissionDetail;
import edu.ucdavis.mw.myinfovault.service.authorization.AuthorizationService.Qualifier;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.mw.myinfovault.service.person.MivRole;
import edu.ucdavis.myinfovault.exception.MivSevereApplicationError;

/**
 * Authorize people to manage the top executive positions,
 * including assigning a person the Dean role for a school.
 *
 * @author Stephen Paulsen
 * @since MIV 3.5
 */
public class ManageExecutivesAuthorizer extends RoleBasedAuthorizer
{
    /**
     * Creates the manage executives authorizer. Only system
     * or MIV administrators may manage executive roles.
     */
    public ManageExecutivesAuthorizer()
    {
        super(MivRole.SYS_ADMIN, MivRole.VICE_PROVOST_STAFF);
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.authorization.RoleBasedAuthorizer#isAuthorized(edu.ucdavis.mw.myinfovault.service.person.MivPerson, java.lang.String, edu.ucdavis.mw.myinfovault.util.AttributeSet, edu.ucdavis.mw.myinfovault.util.AttributeSet)
     */
    @Override
    public boolean isAuthorized(MivPerson shadowPerson,
                                String permissionName,
                                AttributeSet permissionDetails,
                                AttributeSet qualification)
    {
        try
        {
            int realPersonId = Integer.parseInt(permissionDetails.get(PermissionDetail.ACTOR_ID));
            int executivePersonId = Integer.parseInt(qualification.get(Qualifier.USERID));

            /*
             * New executive person may not be the:
             *  - real, logged-in person
             *  - shadow, switched-to person
             */
            return executivePersonId != realPersonId
                && executivePersonId != shadowPerson.getUserId();
        }
        catch (NumberFormatException e)
        {
            throw new MivSevereApplicationError("errors.authorization", e, permissionName, shadowPerson, permissionDetails, qualification);
        }
    }
}
