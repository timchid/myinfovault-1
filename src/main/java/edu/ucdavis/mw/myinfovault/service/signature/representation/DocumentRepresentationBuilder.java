/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: DocumentRepresentationBuilder.java
 */

package edu.ucdavis.mw.myinfovault.service.signature.representation;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import edu.ucdavis.mw.myinfovault.domain.signature.Signable;
import edu.ucdavis.myinfovault.message.ErrorMessages;

/**
 * Builds document representations for {@link Signable} documents. Subclasses must annotate
 * each of their document representation build methods with the corresponding {@link Version}.
 *
 * @author Craig Gilmore
 * @since MIV 4.7.1
 */
abstract class DocumentRepresentationBuilder
{
    private final int latestVersion;
    private final Map<Integer, Method> methods = new HashMap<Integer, Method>();

    /**
     * Instantiate the document representation builder. Map each {@link Version}
     * annotated method of the subclass to its corresponding class.
     */
    protected DocumentRepresentationBuilder()
    {
        for (Method m : this.getClass().getDeclaredMethods())
        {
            Version versionAnn = m.getAnnotation(Version.class);

            if (versionAnn == null) continue;// method has no version annotation

            if (versionAnn.value() <= 0 || methods.containsKey(versionAnn.value()))
            {
                throw new RuntimeException(ErrorMessages.getInstance().getString("errors.representation.annotation", Version.class, m));
            }

            methods.put(versionAnn.value(), m);
        }

        /*
         * No annotated methods results in a NoSuchElementException
         */
        latestVersion = Collections.max(methods.keySet()).intValue();
    }

    /**
     * @return number of the latest version available
     */
    final int getLatestVersion()
    {
        return latestVersion;
    }

    /**
     * Get the given version of the document representation for the given signable document.
     *
     * @param document signable document
     * @param version document representation version
     * @return document representation
     * @throws IllegalArgumentException if no representation is defined for the given version
     * @throws InvocationTargetException if a representation cannot be built for the given document
     */
    final byte[] getDocumentRepresentation(Signable document, int version) throws IllegalArgumentException, InvocationTargetException
    {
        if (version != 0)
        {
            try
            {
                return (byte[]) getMethod(version).invoke(this, document);
            }
            catch (IllegalAccessException e) {}// can never illegally access self
        }

        return new byte[0];
    }

    /**
     * Get the method annotated with the given version.
     *
     * @param version document representation version
     * @return subclass method annotated with the given version
     * @throws IllegalArgumentException if no representation is defined for the given version
     */
    private Method getMethod(int version) throws IllegalArgumentException
    {
        Method m = methods.get(version);

        if (m != null) return m;

        throw new IllegalArgumentException(ErrorMessages.getInstance().getString("errors.representation.version", version));
    }

    /**
     * Document representation version.
     *
     * @author Craig Gilmore
     * @since MIV 4.7.1
     */
    public @Retention(RetentionPolicy.RUNTIME) @interface Version
    {
        int value();
    }
}
