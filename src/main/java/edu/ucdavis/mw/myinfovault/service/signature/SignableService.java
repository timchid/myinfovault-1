/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: SignableService.java
 */

package edu.ucdavis.mw.myinfovault.service.signature;

import edu.ucdavis.mw.myinfovault.domain.signature.Signable;

/**
 * Shared service for signable document services.
 *
 * @author Craig Gilmore
 * @since MIV 4.7
 * @param <T> type of document to persist and request/remove signatures
 */
public interface SignableService<T extends Signable>
{
    /**
     * Persist the signable document to the database and regenerate associated PDFs.
     *
     * @param document signable document
     * @param realUserId ID of the real, logged-in user
     * @return if persistence was successful
     */
    public boolean persist(T document, int realUserId);

    /**
     * Generate the PDF for the given signable document.
     *
     * @param document signable document
     * @return if the PDF generation was successful
     */
    public boolean createPdf(final T document);
}
