/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: DatabasePersonDataSource.java
 */

package edu.ucdavis.mw.myinfovault.service.person;

import java.io.File;
import java.util.Collection;
import java.util.Properties;

/**
 * TODO: Add Javadoc
 * Use a database as the source of Person information.
 *
 * TODO: Write this. Make it configurable.
 * Configuration should include
 * - SQL statements to run for each of the types of getPersonFrom<X>() methods.
 * - Mapping of column names retrieved from those SQL queries to PersonDTO fields.
 * We'll want to use this so we can have Mock people.
 *
 * @author Stephen Paulsen
 * @since MIV 4.6
 */
public class DatabasePersonDataSource implements PersonDataSource
{
    public DatabasePersonDataSource(String configFileName)
    {

    }
    public DatabasePersonDataSource(File configFile)
    {

    }
    public DatabasePersonDataSource(Properties config)
    {

    }

    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.person.PersonDataSource#getPersonFromId(java.lang.String)
     */
    @Override
    public PersonDTO getPersonFromId(String personId)
    {
        // TODO Auto-generated method stub
        return null;
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.person.PersonDataSource#getPersonFromPrincipal(java.lang.String)
     */
    @Override
    public PersonDTO getPersonFromPrincipal(String principalName)
    {
        // TODO Auto-generated method stub
        return null;
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.person.PersonDataSource#getPersonFromEmail(java.lang.String)
     */
    @Override
    public Collection<PersonDTO> getPersonFromEmail(String email)
    {
        // TODO Auto-generated method stub
        return null;
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.person.PersonDataSource#isActive(java.lang.String)
     */
    @Override
    public boolean isActive(String personId)
    {
        PersonDTO p = this.getPersonFromId(personId);
        return p != null && p.isActive();
    }

}
