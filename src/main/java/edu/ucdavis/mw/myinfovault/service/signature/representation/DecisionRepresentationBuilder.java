/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: DecisionRepresentationBuilder.java
 */

package edu.ucdavis.mw.myinfovault.service.signature.representation;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

import org.apache.commons.io.IOUtils;

import edu.ucdavis.mw.myinfovault.domain.decision.Decision;
import edu.ucdavis.mw.myinfovault.util.StringUtil;
import edu.ucdavis.myinfovault.exception.MivSevereApplicationError;

/**
 * Builds document representations for decision documents.
 *
 * @author Craig Gilmore
 * @since MIV 4.7.1
 */
class DecisionRepresentationBuilder extends DocumentRepresentationBuilder
{
    /**
     * Version one for decision document representations.
     *
     * @param decision signable dean/vice-provost decision
     * @return document representation
     */
    @Version(1)
    protected byte[] getVersionOne(Decision decision)
    {
        String description = decision.getDecisionTypeDescription();

        /*
         * The DecisionType enum has been altered. The recommendation counterparts to
         * APPROVED, DENIED, and OTHER have been removed in favor of two descriptions
         * for each decision choice; one for 'decisions' and one for 'recommendations'.
         *
         * For backwards compatibility, I've replaced the DecisionType#toString()
         * with the whitespace-stripped, uppercase description.
         */
        //return (decision.getDecisionType().toString() + Boolean.toString(decision.isContraryToCommittee())).getBytes();
        return (description.replaceAll("\\s", StringUtil.EMPTY_STRING).toUpperCase()
              + Boolean.toString(decision.isContraryToCommittee())).getBytes();
    }

    /**
     * Version two for decision document representations.
     *
     * @param decision signable dean/vice-provost decision
     * @return document representation
     */
    @Version(2)
    protected byte[] getVersionTwo(Decision decision)
    {
        return (decision.getDocumentType().name()
              + decision.getDecisionType().name()
              + Boolean.toString(decision.isContraryToCommittee())
              + decision.getComments()).getBytes(StandardCharsets.UTF_8);
    }

    /**
     * Version three for decision document representations.
     *
     * @param decision signable decision
     * @return document representation
     */
    @Version(3)
    protected byte[] getVersionThree(Decision decision)
    {
        File wetSignature = decision.getWetSignature();

        try (ByteArrayOutputStream stream = new ByteArrayOutputStream())
        {
            // if provided, write wet signature to stream
            if (wetSignature != null && wetSignature.exists())
            {
                stream.write(
                    IOUtils.toByteArray(
                        new FileInputStream(
                            decision.getWetSignature())));
            }

            // write decision attributes to stream
            stream.write(getVersionTwo(decision));

            // return representation as bytes
            return stream.toByteArray();
        }
        catch (IOException e)
        {
            throw new MivSevereApplicationError("errors.representation.decision.wetSignature", e, wetSignature);
        }
    }
}
