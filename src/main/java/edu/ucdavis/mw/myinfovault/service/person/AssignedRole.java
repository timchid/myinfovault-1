/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: AssignedRole.java
 */

package edu.ucdavis.mw.myinfovault.service.person;

import java.io.Serializable;

/**
 * An MIV role assigned role. Holds the MIV role, scope to which the
 * role applies, and if the role is primary (as opposed to joint).
 *
 * @author Stephen Paulsen
 * @since MIV 3.0
 */
public class AssignedRole implements Comparable<AssignedRole>, Serializable
{
    private static final long serialVersionUID = 201107111346L;

    private int recId = 0;

    private final MivRole role;
    private final boolean primaryRole;
    private final AppointmentScope scope;

    private final String toString;

    /**
     * Create a joint assigned role for any scope.
     *
     * @param role MIV role
     */
    public AssignedRole(MivRole role)
    {
        this(role, Scope.AnyScope, false);
    }

    /**
     * Create an assigned role for any scope.
     *
     * @param role MIV role
     * @param primary if the role is primary
     */
    public AssignedRole(MivRole role, boolean primary)
    {
        this(role, Scope.AnyScope, primary);
    }

    /**
     * Create a joint assigned role.
     * <p>
     * Scopes are parameterized based on role:
     * <ul>
     * <li>{@link MivRole#SYS_ADMIN}, {@link MivRole#VICE_PROVOST_STAFF}, and  {@link MivRole#SENATE_STAFF} are always {@link Scope#AnyScope}</li>
     * <li>{@link MivRole#SCHOOL_STAFF} and {@link MivRole#DEAN} are scoped to {@link Scope#ANY} department within their school</li>
     * <li>Other roles retain the scope given</li>
     * </ul>
     * </p>
     * @param role MIV role
     * @param scope scope of the given role
     */
    public AssignedRole(MivRole role, Scope scope)
    {
        this(role, scope, false);
    }


    /**
     * Create an assigned role.
     * <p>
     * Scopes are parameterized based on role:
     * <ul>
     * <li>{@link MivRole#SYS_ADMIN}, {@link MivRole#VICE_PROVOST_STAFF}, and  {@link MivRole#SENATE_STAFF} are always {@link Scope#AnyScope}</li>
     * <li>{@link MivRole#SCHOOL_STAFF} and {@link MivRole#DEAN} are scoped to {@link Scope#ANY} department within their school</li>
     * <li>Other roles retain the scope given</li>
     * </ul>
     * </p>
     *
     * @param role MIV role
     * @param scope scope of the given role
     * @param primary if the role is primary
     */
    public AssignedRole(MivRole role, Scope scope, boolean primary)
    {
        this.role = role;
        this.primaryRole = primary;
        this.scope = new AppointmentScope(scope.or(role.getScope()));// parameterizing scope based on role

        // role, scope, is primary string representation
        this.toString = new StringBuilder().append("[")
                                           .append(this.role.getShortDescription())
                                           .append(", ")
                                           .append("{")
                                           .append(this.getScope())
                                           .append("}") .append(", ")
                                           .append(this.isPrimary() ? "Primary" : "Additional")
                                           .append("]")
                                           .toString();
    }

    /**
     * @return MIV role
     */
    public MivRole getRole()
    {
        return this.role;
    }

    /**
     * @return if the role is primary
     */
    public boolean isPrimary()
    {
        return this.primaryRole;
    }

    /**
     * @return scope of this assignment
     */
    public AppointmentScope getScope()
    {
        return this.scope;
    }

    /**
     * @return scope school description
     * @deprecated use {@link #getScope()#getSchoolName()} instead
     */
    @Deprecated
    public String getSchoolName()
    {
        return this.scope.getSchoolDescription();
    }

    /**
     * @return scope department description
     * @deprecated use {@link #getScope()#getDepartmentName()} instead
     */
    @Deprecated
    public String getDepartmentName()
    {
        return this.scope.getDepartmentDescription();
    }

    /**
     * If splitting into an interface, object, and dao
     * the recId methods would not be in the interface,
     * would be in the object, would only be used by the dao.
     *
     * FIXME: SDP - temporarily made public so the person package can use it.
     * Change it back to protected when person is merged into person package.
     *
     * @return database record ID
     */
    public /*protected*/ int getRecId()
    {
        return this.recId;
    }

    /**
     * @param recId database record ID
     */
    public void setRecId(int recId)
    {
        this.recId = recId;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Comparable#compareTo(java.lang.Object)
     */
    @Override
    public int compareTo(AssignedRole other)
    {
        return this.scope.compareTo(other.getScope());
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + (primaryRole ? 1231 : 1237);
        result = prime * result + ((role == null) ? 0 : role.hashCode());
        result = prime * result + ((scope == null) ? 0 : scope.hashCode());
        return result;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj)
    {
        if (this == obj) return true;
        if (obj == null) return false;
        if (!(obj instanceof AssignedRole)) return false;
        AssignedRole other = (AssignedRole) obj;
        if (primaryRole != other.primaryRole) return false;
        if (role != other.role) return false;
        if (scope == null)
        {
            if (other.scope != null) return false;
        }
        else if (!scope.equals(other.scope)) return false;
        return true;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        return this.toString;
    }
}
