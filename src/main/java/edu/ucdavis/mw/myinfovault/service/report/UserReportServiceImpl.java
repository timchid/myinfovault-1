/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: UserReportServiceImpl.java
 */

package edu.ucdavis.mw.myinfovault.service.report;

import java.util.List;
import java.util.Map;

import edu.ucdavis.mw.myinfovault.util.AttributeSet;

import edu.ucdavis.mw.myinfovault.dao.report.MivUserReportDao;

/**
 * This class implements all methods required for user report. it implements all
 * methods of interface UserReportService.
 *
 * @author Pradeep K Haldiya
 * @since MIV 4.0
 */
public class UserReportServiceImpl implements UserReportService
{

    private MivUserReportDao mivUserReportDao = null;

    /**
     * Spring injected mivUserReportDao
     *
     * @param mivUserReportDao
     */
    public void setMivUserReportDao(MivUserReportDao mivUserReportDao)
    {
        this.mivUserReportDao = mivUserReportDao;
    }

    /**
     * To find user by AttributeSet. It also contains assignments details
     *
     * @param criteria
     * @return List of Map
     */
    @Override
    public List<Map<String, Object>> findUsers(AttributeSet criteria)
    {
        return mivUserReportDao.findUsers(criteria);
    }
}
