/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: ReviewDossierAuthorizer.java
 */

package edu.ucdavis.mw.myinfovault.service.authorization;

import edu.ucdavis.mw.myinfovault.util.AttributeSet;

import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.mw.myinfovault.service.person.MivRole;


/**<p>
 * Authorizes the reviewing of a dossier by checking that the various attributes of the target reviewer are within
 * the qualification scope of the actor's appointments and/or role.</p>
 *
 * @author rhendric
 * @since MIV 3.5.2
 */
public class ReviewDossierAuthorizer extends SameScopeAuthorizer implements PermissionAuthorizer
{
    /**
     * <p>General permission check to determine if the input person has the permission to review a dossier.
     * The potential reviewer cannot be the dossierOwner, or the actor or target user unless they have the VP role.
     * Require permissionDetails which specify the actor, target and dossier owner id, as well as the actor and target user
     * roles.
     * </p>
     *
     * @see SameScopeAuthorizer#hasPermission(MivPerson, String, AttributeSet)
     */
    @Override
    public boolean hasPermission(MivPerson person, String permissionName, AttributeSet permissionDetails)
    {
        if (permissionDetails != null)
        {
            int actorUserId = permissionDetails.get(AuthorizationService.PermissionDetail.ACTOR_ID) != null ? Integer.parseInt(permissionDetails.get(AuthorizationService.PermissionDetail.ACTOR_ID)) : 0;
            int targetUserId = permissionDetails.get(AuthorizationService.PermissionDetail.TARGET_ID) != null ? Integer.parseInt(permissionDetails.get(AuthorizationService.PermissionDetail.TARGET_ID)) : 0;
            int dossierOwnerUserId = permissionDetails.get(AuthorizationService.PermissionDetail.DOSSIER_OWNER_USERID) != null ? Integer.parseInt(permissionDetails.get(AuthorizationService.PermissionDetail.DOSSIER_OWNER_USERID)) : 0;
            boolean assignerHasVPRole = false;

            MivRole targetUserRole = null;
            MivRole actorUserRole = null;

            try {
                targetUserRole = MivRole.valueOf( permissionDetails.get(AuthorizationService.PermissionDetail.TARGET_USER_ROLE) );
                actorUserRole = MivRole.valueOf( permissionDetails.get(AuthorizationService.PermissionDetail.ACTOR_USER_ROLE) );
            }
            catch (IllegalArgumentException e) {
                // A non-existent role was passed - write an error and deny permission
                log.error("A non-existent role was passed in the PermissionDetails: " + permissionDetails, e);
                return false;
            }

            if (actorUserRole == MivRole.VICE_PROVOST_STAFF || targetUserRole == MivRole.VICE_PROVOST_STAFF) {
                 assignerHasVPRole = true;
            }

            // Dossier owner, assigning user (unless VICE_PROVOST_STAFF) or a department helper cannot be reviewers
            return (((actorUserId == person.getUserId() || targetUserId == person.getUserId()) && assignerHasVPRole) ||
                    (person.hasRole(MivRole.CANDIDATE)) &&
                    (person.getUserId() != dossierOwnerUserId) &&
                    (person.getUserId() != actorUserId) &&
                    (person.getUserId() != targetUserId));

        }
        else
        {
            // MIV-3847 prevent Dept. Admins from reviewing dossiers
            return person.hasRole(MivRole.CANDIDATE, MivRole.VICE_PROVOST_STAFF, MivRole.SENATE_STAFF/*, MivRole.SCHOOL_STAFF, MivRole.DEPT_STAFF*/);
        }
    }


    /**
     * <p>Determine if a person is qualified to review a dossier based on the role of person making the reviewing assignment.
     * A school administrator may assign reviewers within the same school (any department is acceptable) as any of their
     * appointments. A department administrator may assign reviewers within the same school-department as any of their
     * appointments.</p>
     *
     * @see SameScopeAuthorizer#isAuthorized(MivPerson, String, AttributeSet, AttributeSet)
     */
    @Override
    public boolean isAuthorized(MivPerson person, String permissionName, AttributeSet permissionDetails, AttributeSet qualification)
    {

        return this.hasPermission(person, permissionName, permissionDetails);

// As of release v4.0, qualified reviewers no longer need to be within the school/department scope
// of the assigning user.

//        // If person has permission to review the dossier, check if authorized based on department and school of the
//        // assigning user
//        if (this.hasPermission(person, permissionName, permissionDetails))
//        {
//            // This is the assigning user principal id, get it and remove because we don't want to consider this principal for the shareScope
//            // check because it will be that of the assigning user
//            final String principalId = qualification.get(Qualifier.PRINCIPAL);
//            qualification.remove(Qualifier.PRINCIPAL);
//
//            final MivPerson assigningUser;
//
//            if (principalId != null &&
//               (assigningUser = MivServiceLocator.getUserService().getPersonByPrincipalId(principalId)) != null)
//            {
//
//                // Sys admin, Vice provost role can assign all
//                if (assigningUser.hasRole(MivRole.SYS_ADMIN, MivRole.VICE_PROVOST_STAFF,MivRole.VICE_PROVOST))
//                {
//                    return true;
//                }
//
//                // School staff can assign reviewers for users - no longer qualified by the department and school.
//                if (assigningUser.hasRole(MivRole.SCHOOL_STAFF) && hasSharedScope(assigningUser, qualification))
//                {
//                    return true;
//                }
//                // Department staff can assign reviewers for users - no longer qualified by the department.
//                else if (assigningUser.hasRole(MivRole.DEPT_STAFF) && hasSharedScope(assigningUser, qualification))
//                {
//                    return true;
//                }
//            }
//        }
//        return false;
    }

}
