/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: DossierReviewerDto.java
 */

package edu.ucdavis.mw.myinfovault.service.dossier;

import java.io.Serializable;
import java.sql.Timestamp;

import edu.ucdavis.mw.myinfovault.service.person.MivRole;
import edu.ucdavis.mw.myinfovault.web.spring.assignreviewers.AssignReviewersForm;

/**
 * This class represents a DossierReviewer record.
 *
 * @author Richard Hendricks
 * @author Craig Gilmore
 */
public class DossierReviewerDto implements Serializable
{
    private static final long serialVersionUID = -1881449209157341773L;

    private int id;
    private int entityId;
    private EntityType entityType;
    private long dossierId;
    private int schoolId;
    private int departmentId;
    private String reviewLocation;
    private int assignedByUserID;
    private MivRole assignedByUserRole;
    private Timestamp insertTimestamp = null;
    private int insertUserID;
    private int updateUserID;
    private Timestamp updateTimestamp = null;

    /**
     * Create a dossier reviewer from values in an AssignReviewersForm.
     *
     * @param entityId Dossier reviewer ID
     * @param entityType Dossier reviewer entity type
     * @param form The form object used for assigning reviewers
     */
    public DossierReviewerDto(int entityId,
                              EntityType entityType,
                              AssignReviewersForm form)
    {
        this(entityId,
             entityType,
             form.getDossier().getDossierId(),
             form.getAppointmentKey().getSchoolId(),
             form.getAppointmentKey().getDepartmentId(),
             form.getDossier().getLocation(),
             form.getActingPerson().getUserId(),
             form.getActingPerson().getPrimaryRoleType(),
             form.getActingPerson().getUserId());
    }

    public DossierReviewerDto(int entityId,
                              EntityType entityType,
                              long dossierId,
                              int schoolId,
                              int departmentId,
                              String reviewLocation,
                              int assignedByUserID,
                              MivRole assignedByUserRole,
                              int insertUserID)
    {
        this(0,
             entityId,
             entityType,
             dossierId,
             schoolId,
             departmentId,
             reviewLocation,
             assignedByUserID,
             assignedByUserRole,
             null,
             insertUserID,
             0,
             null);
    }

    public DossierReviewerDto(int id,
                              int entityId,
                              EntityType entityType,
                              long dossierId,
                              int schoolId,
                              int departmentId,
                              String reviewLocation,
                              int assignedByUserID,
                              MivRole assignedByUserRole,
                              Timestamp insertTimestamp,
                              int insertUserID,
                              int updateUserID,
                              Timestamp updateTimestamp)
    {
        this.id = id;
        this.entityId = entityId;
        this.entityType = entityType;
        this.dossierId = dossierId;
        this.schoolId = schoolId;
        this.departmentId = departmentId;
        this.reviewLocation = reviewLocation;
        this.assignedByUserID = assignedByUserID;
        this.assignedByUserRole = assignedByUserRole;
        this.insertTimestamp = insertTimestamp;
        this.insertUserID = insertUserID;
        this.updateUserID = updateUserID;
        this.updateTimestamp = updateTimestamp;
    }

    /**
     * @return
     */
    public int getID()
    {
        return this.id;
    }

    /**
     * @return
     */
    public int getEntityId()
    {
        return entityId;
    }

    /**
     * @return
     */
    public EntityType getEntityType()
    {
        return entityType;
    }

    /**
     * @return
     */
    public long getDossierId()
    {
        return this.dossierId;
    }

    /**
     * @return
     */
    public String getReviewLocation()
    {
        return reviewLocation;
    }

    /**
     * @return
     */
    public int getSchoolId()
    {
        return this.schoolId;
    }

    /**
     * @return
     */
    public int getDepartmentId()
    {
        return this.departmentId;
    }

    /**
     * @return
     */
    public int getAssignedByUserID()
    {
        return this.assignedByUserID;
    }

    /**
     * @param assignedByUserID
     */
    public void setAssignedByUserID(int assignedByUserID)
    {
        this.assignedByUserID = assignedByUserID;
    }

    /**
     * @return
     */
    public MivRole getAssignedByUserRole()
    {
        return this.assignedByUserRole;
    }

    /**
     * @return
     */
    public Timestamp getInsertTimestamp()
    {
        return insertTimestamp;
    }

    /**
     * @return
     */
    public int getInsertUserID()
    {
        return insertUserID;
    }

    /**
     * @return
     */
    public int getUpdateUserID()
    {
        return updateUserID;
    }

    /**
     * @return
     */
    public Timestamp getUpdateTimestamp()
    {
        return updateTimestamp;
    }

    @Override
    public boolean equals(Object ob)
    {
        if (this == ob) return true;

        if (ob instanceof DossierReviewerDto)
        {
            DossierReviewerDto dossierReviewer = (DossierReviewerDto) ob;

            //matching entity, dossier, department, and location satisfies equality
            return dossierReviewer.getEntityId() == this.entityId
                && dossierReviewer.getEntityType() == this.entityType
                && dossierReviewer.getDossierId() == this.dossierId
                && dossierReviewer.getSchoolId() == this.schoolId
                && dossierReviewer.getDepartmentId() == this.departmentId
                && dossierReviewer.getReviewLocation().equalsIgnoreCase(this.reviewLocation);
        }

        return false;
    }

    @Override
    public int hashCode()
    {
        int hash = 1;

        hash = hash * 31 + entityId;
        hash = hash * 31 + entityType.hashCode();
        hash = hash * 31 + Long.valueOf(dossierId).hashCode();
        hash = hash * 31 + schoolId;
        hash = hash * 31 + departmentId;
        hash = hash * 31 + reviewLocation.hashCode();

        return hash;
    }

    /**
     * The entity type associated with a dossier reviewer.
     *
     * @author Craig Gilmore
     * @since 4.3
     */
    public enum EntityType
    {
        GROUP, USER;
    }
}
