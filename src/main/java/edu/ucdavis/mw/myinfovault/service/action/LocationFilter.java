/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: LocationFilter.java
 */

package edu.ucdavis.mw.myinfovault.service.action;

import java.util.Arrays;
import java.util.EnumSet;

import edu.ucdavis.mw.myinfovault.domain.action.DossierLocation;
import edu.ucdavis.mw.myinfovault.service.SearchFilterAdapter;

/**
 * Filters out items not associated with the defined dossier locations.
 *
 * @author Craig Gilmore
 * @since MIV 4.7.2
 * @param <T> object that has a dossier location
 */
public abstract class LocationFilter<T> extends SearchFilterAdapter<T>
{
    private final EnumSet<DossierLocation> locations;

    /**
     * Creates a filter by the given locations.
     *
     * @param locations location by which to filter
     */
    public LocationFilter(DossierLocation...locations)
    {
        this.locations = EnumSet.copyOf(Arrays.asList(locations));
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.SearchFilter#include(java.lang.Object)
     */
    @Override
    public boolean include(T item)
    {
        return locations.contains(getLocation(item));
    }

    /**
     * Get the location from an item to filter.
     *
     * @param item An item within the collection to filter
     * @return the location for the given item
     */
    protected abstract DossierLocation getLocation(T item);
}
