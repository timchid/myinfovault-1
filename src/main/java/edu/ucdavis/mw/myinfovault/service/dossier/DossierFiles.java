/**
 * Copyright
 * Copyright (c) 2007-2009 The Regents of the University of California
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * RESTRICTED RIGHTS LEGEND
 * Use, duplication, or disclosure by the U.S. Government is subject to restrictions
 * as set forth in Subparagraph (c) (1) (ii) of DFARS 252.227-7013 or in FAR
 * 52.227-19, as applicable.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: DossierFiles.java
 */
package edu.ucdavis.mw.myinfovault.service.dossier;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import edu.ucdavis.myinfovault.document.PathConstructor;

/**
 * This class represents a created Dossiers files and contains among other things
 * contains an ordered list of pdfiles which were used to create the dossier.
 *  
 * @author rhendric
 *
 */
public class DossierFiles
{
    private List<File> pdfFiles = null;
    private boolean created = false;
    private long dossierId;
    private int userId;
    PathConstructor pathConstructor = new PathConstructor();
    List <String> pdfUrlList = null;

    
    /**
     * Set the pdfFiles used to create this dossier
     * @param pdfFiles
     */
    public void setPdfFiles(List<File> pdfFiles)
    {
        this.pdfFiles = pdfFiles;
    }

    /**
     * get the pdfFiles used to create this dossier
     * @return pdfFiles list
     */
    public List<File> getPdfFiles()
    {
        return pdfFiles;
    }

    /**
     * Get the urls of the pdfFiles used to create this dossier
     * @return pdfUrlList
     */
    public List<String> getPdfFileUrls()
    {
        if (this.pdfUrlList == null)
        {
            this.pdfUrlList = new ArrayList<String>();
            for (File pdfFile : pdfFiles)
            {
                pdfUrlList.add(pathConstructor.getUrlFromPath(pdfFile)); 
            }
        }
    
        return  this.pdfUrlList;
    }

    /**
     * Set the flag indicating that the dossier was either created or not 
     * @param created - true if created, otherwise false
     */
    public void setCreated(boolean created)
    {
        this.created = created;
    }
    
    /**
     * Get the flag indicating that the dossier was either created or not 
     * @return created
     */
    public boolean isCreated()
    {
        return created;
    }
    
    /**
     * Set the dossier id
     * @param dossierId
     */
    public void setDossierId(long dossierId)
    {
        this.dossierId = dossierId;
    }
    
    /**
     * Get the dossier id
     * @return dossierId
     */
    public long getDossierId()
    {
        return dossierId;
    }

    /**
     * Set the userId of this dossier
     * @param userId
     */
    public void setUserId(int userId)
    {
        this.userId = userId;
    }
    
    /**
     * Get the userId of this dossier
     * @return userId
     */
    public int getUserId()
    {
        return userId;
    }
    

}
