/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: GroupService.java
 */

package edu.ucdavis.mw.myinfovault.service.group;

import java.util.Collection;
import java.util.Set;

import edu.ucdavis.mw.myinfovault.domain.group.Group;
import edu.ucdavis.mw.myinfovault.domain.group.GroupType;
import edu.ucdavis.mw.myinfovault.service.SearchFilter;
import edu.ucdavis.mw.myinfovault.service.SqlAwareFilterAdapter;
import edu.ucdavis.mw.myinfovault.service.person.AssignedRole;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;

/**
 * Governs the use of MIV groups.
 *
 * @author Craig Gilmore
 * @since MIV 4.3
 */
public interface GroupService
{
    /**
     * Persists an MIV group.
     *
     * @param targetPerson The switched-to, target person
     * @param group The group to persist
     * @return <code>true</code> on success, <code>false</code> on failure no effect
     * @throws SecurityException Acting person is not authorized to persist the group
     */
    public boolean persist(MivPerson targetPerson, Group group) throws SecurityException;

    /**
     * Copies and persists the MIV group corresponding to the given ID with updated attributes.
     *
     * @param targetPerson The switched-to, target person
     * @param type The type assigned to the group (Review or otherwise)
     * @param assignedRoles The role-scopes of which a user must have at least one in order to access the group
     * @param groupId The ID corresponding to the group to copy
     * @return the successfully copied group object or <code>null</code> if no group is found corresponding to the given ID
     * @throws SecurityException Acting person is not authorized to view the group to copy or persist a new group
     */
    public Group copy(MivPerson targetPerson, GroupType type, Set<AssignedRole> assignedRoles, int groupId) throws SecurityException;

    /**
     * Copies and persists the set of MIV groups corresponding to the given array of IDs with updated attributes.
     *
     * @param targetPerson The switched-to, target person
     * @param type The type assigned to the groups (Review or otherwise)
     * @param assignedRoles The role-scopes of which a user must have at least one in order to access the groups
     * @param groupIds The IDs corresponding to the groups to copy
     * @return the successfully copied set of groups
     */
    public Set<Group> copy(MivPerson targetPerson, GroupType type, Set<AssignedRole> assignedRoles, int...groupIds);

    /**
     * Copies and persists the given MIV group with updated attributes.
     *
     * @param targetPerson The switched-to, target person
     * @param type The type assigned to the group (Review or otherwise)
     * @param assignedRoles The role-scopes of which a user must have at least one in order to access the group
     * @param group The group to copy
     * @return the successfully copied group object or <code>null</code>, if given group is <code>null</code>
     * @throws SecurityException Acting person is not authorized to view the group to copy or persist a new group
     */
    public Group copy(MivPerson targetPerson, GroupType type, Set<AssignedRole> assignedRoles, Group group) throws SecurityException;

    /**
     * Copies and persists the given set of MIV groups with updated attributes.
     *
     * @param targetPerson The switched-to, target person
     * @param type The type assigned to the groups (Review or otherwise)
     * @param assignedRoles The role-scopes of which a user must have at least one in order to access the groups
     * @param groups The groups to copy
     * @return the successfully copied set of groups
     */
    public Set<Group> copy(MivPerson targetPerson, GroupType type, Set<AssignedRole> assignedRoles, Set<Group> groups);

    /**
     * Deletes and returns an MIV group.
     *
     * @param targetPerson The switched-to, target person
     * @param groupId The ID of the group to delete
     * @return The successfully deleted group object or <code>null</code> if no group is found corresponding to the given ID
     * @throws SecurityException If target person is not permitted to delete this group
     */
    public Group delete(MivPerson targetPerson, int groupId) throws SecurityException;

    /**
     * Deletes a set of MIV groups and returns the deleted set.
     *
     * @param targetPerson The switched-to, target person
     * @param groupIds The IDs of the groups to delete
     * @return the set of successfully deleted groups
     */
    public Set<Group> delete(MivPerson targetPerson, int...groupIds);

    /**
     * Deletes and returns an MIV group.
     *
     * @param targetPerson The switched-to, target person
     * @param group The group to delete
     * @return <code>true</code> on success, <code>false</code> on no effect
     * @throws SecurityException If user is not permitted to delete this group
     */
    public Group delete(MivPerson targetPerson, Group group) throws SecurityException;

    /**
     * Deletes a collection of MIV groups and returns the deleted set.
     *
     * @param targetPerson The switched-to, target person
     * @param groups The groups to delete
     * @return the set of successfully deleted groups
     */
    public Set<Group> delete(MivPerson targetPerson, Collection<Group> groups);

    /**
     * Get an MIV group for the given ID or <code>null</code> if the group does not exist.
     *
     * @param groupId The ID of the group
     * @return The group associated with the given ID
     */
    public Group getGroup(int groupId);

    /**
     * Get a set of MIV groups for the given array of IDs.
     *
     * @param groupIds The IDs of the groups
     * @return The groups associated with the given IDs
     */
    public Set<Group> getGroups(int...groupIds);

    /**
     * Get a set of MIV groups respecting the given filters.
     *
     * @param filter SQL aware Group filter by which to sift results
     * @param additionalFilters filters to be ANDed to the primary filter
     * @return All groups passing the filters
     */
    public Set<Group> getGroups(SqlAwareFilterAdapter<Group> filter, SearchFilter<Group>...additionalFilters);

    /**
     * Refresh the group cache
     */
    public void refresh();

}
