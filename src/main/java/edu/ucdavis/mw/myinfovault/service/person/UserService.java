/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: UserService.java
 */

package edu.ucdavis.mw.myinfovault.service.person;

import java.util.List;
import java.util.Map;
import java.util.Set;

import edu.ucdavis.mw.myinfovault.service.SearchFilter;
import edu.ucdavis.mw.myinfovault.util.AttributeSet;
import edu.ucdavis.myinfovault.MIVUserInfo;

/**<p>
 * The UserService deals with two things -- MIV Users, regardless of whether or not they
 * are affiliated with the campus; and People, regardless of whether or not they are MIV users.</p>
 * <p><code>getUsersBy...</code> methods generally deal with MIV users,
 * while <code>get[Person|People]By...</code> finds people even if they are not MIV users.
 * </p>
 * @author Mary Northup, Stephen Paulsen
 * @since MIV 3.0
 */
public interface UserService
{
 // Temp hack for appointees
    public MivPerson getAppointee(final int userid);
    public MivPerson getNewAppointee(String surname, String givenName, String middleName);
    public MivPerson getNewAppointee(String surName, String givenName, String middleName, Scope scope);

    /**
     * Gets the MIV Person for a given MIV user ID number.
     *
     * @param userid the MIV internal UserID number
     * @return the corresponding MivPerson object or <code>null</code>
     */
    public MivPerson getPersonByMivId(int userid);


    /**
     * Gets a set of MIV Person objects for an arbitrary array of MIV user ID numbers.
     *
     * @param userids array of MIV internal UserID numbers
     * @return the corresponding set of MivPerson objects
     */
    public Set<MivPerson> getPeopleByMivId(int...userids);


    /**
     * <p>Gets an MIVUserInfo by the Person ID, which is the UCD Mothra ID or IAM ID</p>
     * <p>Sample: <code>us.getMivUserByPrincipalName(person.getEntityId());</code></p>
     *
     * @param personUuid the Person ID / Mothra ID / IAM ID for the person
     * @return an MIVUserInfo object for this MIV user, or <code>null</code> if the provided person UUID is not for an MIV user.
     */
    public MIVUserInfo getMivUserByPersonUuid(String personUuid);

    /**<p>
     * Gets an MIVUserInfo by the principal name (kerberos uid / login name)</p>
     * <p>Sample: <code>us.getMivUserByPrincipalName(person.getPrincipalName());</code></p>
     *
     * @param principalName the login name / LDAP uid for the person
     * @return an MIVUserInfo object for this MIV user, or <code>null</code> if the provided principal name is not for an MIV user.
     */
    public MIVUserInfo getMivUserByPrincipalName(String principalName);


    /**
     * <p>Gets an MIVUserInfo for the User ID number</p>
     * <p>Sample: <code>us.getMivUserById(person.getUserId());</code></p>
     * @param userId an MIV user ID number
     * @return an MIVUserInfo object for the MIV user, or <code>null</code> if the provided user ID is not a valid MIV user.
     */
    public MIVUserInfo getMivUserById(int userId);


    /**<p>
     * Gets a Person object given the person unique identifier.</p>
     *
     * @param personUuid the unique identifier String for the desired person
     * @return an MivPerson
     */
    public MivPerson getPersonByPersonUuid(String personUuid);
    public MivPerson getPersonByPersonId(String personId); // XXX: SDP - Experimental
//    public MivPerson getPersonByPersonIdCached(String personId); // XXX: SDP - Experimental

    /**
     * @param principalName kerberos uid
     * @return
     */
    public MivPerson getPersonByPrincipalName(String principalName);

    /**
     * Save to MIV.
     *
     * @param person
     * @return
     */
    public MivPerson savePerson(MivPerson person, int actorId);

 // Save to MIV
    public MivPerson saveAppointee(MivPerson person, int actorId);


//    /**
//     * Attempt to synchronize roles between MIV and KIM
//     * @param person
//     * @return
//     */
//    public boolean synchronizeRoles(MivPerson person);


    /**
     * Save a new person to MIV. Fails and returns false if the person
     * already exists as an MIV user. Note: all MivPersons should already
     * be present in a configured external person data source.
     *
     * @param person - The user to save. The user ID and record ID of the passed
     * in user should be zero (0), and will be filled in with the newly generated
     * ID values.
     * @return true if the person was saved, false if not.
     */
    public boolean addPerson(MivPerson person);



    /**
     * Returns a list of (active) people in the indicated school.
     * The returned list of users is sorted by last name.
     * Note this list is not limited by the calling user, it returns <em>all</em> users in the school.
     * The ability to call this method may be restricted to the appropriate roles via Spring Security.
     * A Collections.EMPTY_LIST may be returned if there are no people in the requested school.
     *
     * @param mivSchoolId - the MyInfoVault school ID number
     * @return a List of all people in the school
     */
    public List<MivPerson> getUsersBySchool(int mivSchoolId);

    public List<MivPerson> getUsersBySchool(int mivSchoolId, SearchFilter<MivPerson> filter);


    /**
     * Returns a list of (active) people in the indicated department.
     * The returned list of users is sorted by last name.
     * Note this list is not limited by the calling user, it returns <em>all</em> users in the department.
     * The ability to call this method may be restricted to the appropriate roles via Spring Security.
     * A Collections.EMPTY_LIST may be returned if there are no people in the requested school/department.
     *
     * @param mivSchoolId the MyInfoVault school ID number
     * @param mivDeptId the MyInfoVault department ID number within the school
     * @return a List of all people in the combination of school and department
     */
    public List<MivPerson> getUsersByDepartment(int mivSchoolId, int mivDeptId);

    public List<MivPerson> getUsersByDepartment(int mivSchoolId, int mivDeptId, SearchFilter<MivPerson> filter);


    /**
     * Returns a list of people that have the requested role in the given scope.
     * This will primarily be used to list, for example, all the School/College Admins.
     * A Collections.EMPTY_LIST may be returned if there are no people holding the requested role.
     *
     * @param role - The MIV role to find
     * @param scope - An optional MIV scope, such as "in department X", or <code>null</code>.
     *        Currently ignored, <strong>FOR FUTURE USE</strong>
     * @return A List of MivPersons or the <code>EMPTY_LIST</code>
     */
    public List<MivPerson> getUsersByRole(MivRole role, Scope scope);

    /**
     * Returns a list of people that have the requested role in the given scope.
     * This will primarily be used to list, for example, all the School/College Admins.
     * A Collections.EMPTY_LIST may be returned if there are no people holding the requested role.
     *
     * @param role - The MIV role to find
     * @param scope - An optional MIV scope, such as "in department X", or <code>null</code>.
     *        Currently ignored, <strong>FOR FUTURE USE</strong>
     * @param filter - An optional SearchFilter, to further limit the returned list, or <code>null</code>.
     * @return A List of MivPersons or the <code>EMPTY_LIST</code>
     */
    public List<MivPerson> getUsersByRole(MivRole role, Scope scope, SearchFilter<MivPerson> filter);

    /**
     * Get users with the given role.
     *
     * @param role MIV role
     * @return users of the given role
     */
    public List<MivPerson> getUsers(MivRole role);

//    /**<p>
//     * DEPRECATED - This is called only for the Add User "{@link edu.ucdavis.mw.myinfovault.web.spring.manageUsers.EditAction#findPersonByEmail(java.lang.String) findPersonByEmail}"<br>
//     * and will be eliminated when the UserService API is replaced by the in-development UserService2 API.<br>
//     * Do not write any new code to call this method!<br>
//     * Not supporting arbitrary searches for non-MIV-users can simplify using external person data sources.</p>
//     * Don't worrry, {@link #findUsers(AttributeSet)} is still available to find people that have MIV accounts.
//     * @param criteria
//     * @return
//     * @deprecated To be replaced by an email-specific search.
//     */
//    @Deprecated
//    public List<MivPerson> findPeople(AttributeSet criteria);


    /**
     * Find a person <strong>or people</strong> associated with a given email address.
     * It will be common for only one person to be returned, but more <em>may</em> be returned.
     * If no person is found with the given email address an empty list will be returned;
     * <code>null</code> will <em>never</em> be returned.
     * @param emailAddress
     * @return a List of people found
     */
    public List<MivPerson> findPersonByEmail(String emailAddress);

    /**
     * Find MIV users matching the set of provided criteria.
     * This differs from the findPeople() methods by always limiting the search
     * results to include only people that already have MIV accounts.
     * A Collections.EMPTY_LIST may be returned if there are no people matching the criteria.
     * @param criteria - TODO: javadoc
     * @return a List of people matching the criteria or an empty List if there were no matches.
     */
    public List<MivPerson> findUsers(AttributeSet criteria);
    public List<MivPerson> findUsers(AttributeSet criteria, SearchFilter<MivPerson> filter);

    /**
     * Find a person <strong>or people</strong> with a similar name to the input person.
     * If no person is found with a similar name an empty list will be returned;
     * <code>null</code> will <em>never</em> be returned.
     * @param name
     * @return a List of people found
     */
    public List<MivPerson>findAppointeesBySimilarName(final MivPerson person);


    public Map<MivPerson,Map<String,Boolean>> activate(MivPerson... users);
    public Map<MivPerson,Map<String,Boolean>> activate(Iterable<MivPerson> users);

    public Map<MivPerson,Map<String,Boolean>> deactivate(MivPerson... users);
    public Map<MivPerson,Map<String,Boolean>> deactivate(Iterable<MivPerson> users);

// getForDisplay wasn't being used anywhere
//    public List<MivDisplayPerson> getForDisplay(Iterable<MivPerson> people);

    /** This is provided solely for MIVConfig to use so it can reload the "user equivalence" maps. Probably don't need it any more. */
    public void refresh();

    public List<MivDisplayPerson> findActiveCandidates();

    public MivPerson freshen(final MivPerson person);
    public MivPerson freshen(final MivPerson person, boolean force);

}
