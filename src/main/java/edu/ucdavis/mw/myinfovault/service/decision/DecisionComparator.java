/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: DecisionComparator.java
 */

package edu.ucdavis.mw.myinfovault.service.decision;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

import com.google.common.collect.ImmutableList;

import edu.ucdavis.mw.myinfovault.domain.decision.Decision;
import edu.ucdavis.mw.myinfovault.service.person.MivRole;

/**
 * Orders {@link Decision decisions} by {@link MivRole signer authority} and {@link Decision#getDocumentType() document type}.
 * Appeal decisions compare higher, otherwise the decision signed by the greater authority compares higher. Decisions compare
 * higher than recommendation.
 *
 * E.g. A Vice Provost's decision will appear earlier in a sorted {@link Collection} than a Dean's, but not earlier than a Dean's appeal decision.
 */
public class DecisionComparator implements Comparator<Decision>
{
    /**
     * Decision made by a person with a role with the higher index trumps that made by one with a lower.
     */
    private static final List<MivRole> order = ImmutableList.of(MivRole.DEAN, MivRole.VICE_PROVOST, MivRole.PROVOST, MivRole.CHANCELLOR);

    /*
     * (non-Javadoc)
     * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
     */
    @Override
    public int compare(Decision a, Decision b)
    {
        // compare if each document is an appeal
        int comparison = Boolean.compare(b.getDocumentType().name().contains("APPEAL"),
                                         a.getDocumentType().name().contains("APPEAL"));

        // if compared equal, compare if is decision as opposed to recommendation
        if (comparison != 0) return comparison;
        comparison = Boolean.compare(b.getDocumentType().isDecision(),
                                     a.getDocumentType().isDecision());

        // if compared equal, compare decision signer rank
        if (comparison != 0) return comparison;
        comparison = order.indexOf(b.getDocumentType().getAllowedSigner()) - order.indexOf(a.getDocumentType().getAllowedSigner());

        /*
         * If still compared equal, compare decision record ID.
         *
         * Really, further comparison is inconsequential for the purposes of this comparator.
         */
        return comparison != 0
             ? comparison
             : b.getDocumentId() -a.getDocumentId();
    }
}
