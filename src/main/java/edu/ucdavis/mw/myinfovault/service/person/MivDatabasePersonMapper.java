/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: MivDatabasePersonMapper.java
 */

package edu.ucdavis.mw.myinfovault.service.person;

import java.util.List;
import java.util.Map;

import edu.ucdavis.myinfovault.MIVConfig;
import edu.ucdavis.myinfovault.data.QueryTool;

/**
 * Map between Person identifiers based on data already in the MIV tables.
 * @author Stephen Paulsen
 * @since MIV 4.6
 */
public class MivDatabasePersonMapper implements PersonIdMapper
{
    private final QueryTool qt;
    public MivDatabasePersonMapper()
    {
        this.qt = MIVConfig.getConfig().getQueryTool();
        this.qt.setNote("QueryTool used by the MivDatabasePersonMapper");
    }

    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.person.PersonIdMapper#getPersonIdForIamId(java.lang.String)
     */
    @Override
    public String getPersonIdForIamId(String iamId)
    {
        throw new UnsupportedOperationException();
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.person.PersonIdMapper#getPersonIdForPrincipalName(java.lang.String)
     */
    @Override
    public String getPersonIdForPrincipalName(String principalName)
    {
        return this.getPersonIdForUserId(this.getUserIdForPrincipalName(principalName));
/*
        final String query1 = "SELECT PersonUUID FROM UserAccount WHERE Login=?";
        final String query2 = "SELECT UserID FROM UserLogin WHERE Login=?";

        String personId = queryForString(query1, "PersonUUID", principalName);
        if (personId == null)
        {
            String userId = queryForString(query2, "UserID", principalName);
            if (userId != null) {
                return this.getPersonIdForUserId(userId);
            }
        }

        return personId;
*/
//        String personId = null;
//
//        List<Map<String,String>> l = qt.getList(query1, principalName);
//        if (l.isEmpty())
//        {
//            // Try the other double-query: Login from UserLogin followed by UserID from UserAccount;
//            l = qt.getList(query2, principalName);
//            if (!l.isEmpty()) {
//                String userId = l.get(0).get("userid");
//                return this.getPersonIdForUserId(userId);
//            }
//        }
//
//        if (!l.isEmpty()) {
//            personId = l.get(0).get("personuuid");
//        }
//
//        return personId;
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.person.PersonIdMapper#getPersonIdForEmployeeId(java.lang.String)
     */
    @Override
    public String getPersonIdForEmployeeId(String employeeId)
    {
        final String query =
            "SELECT PersonUUID FROM UserAccount" +
            " LEFT JOIN UserIdentifier ON UserIdentifier.UserID=UserAccount.UserID" +
            " WHERE UserIdentifier.IdentifierID=1 AND UserIdentifier.Value=?";
        final String personId = queryForString(query, "PersonUUID", employeeId);
        return personId;
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.person.PersonIdMapper#getPersonIdForStudentId(java.lang.String)
     */
    @Override
    public String getPersonIdForStudentId(String studentId)
    {
        throw new UnsupportedOperationException();
    }


    @Override
    public String getPersonIdForUserId(String userId)
    {
        final String query = "SELECT PersonUUID FROM UserAccount WHERE UserID=?";
        final String personId = queryForString(query, "PersonUUID", userId);
        return personId;

//        String personId = null;
//
//        List<Map<String,String>> l = qt.getList(query, userId);
//        if (!l.isEmpty()) {
//            personId = l.get(0).get("userid");
//        }
//
//        return personId;
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.person.PersonIdMapper#getIamIdForPersonId(java.lang.String)
     */
    @Override
    public String getIamIdForPersonId(String personId)
    {
        throw new UnsupportedOperationException();
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.person.PersonIdMapper#getIamIdForPrincipalName(java.lang.String)
     */
    @Override
    public String getIamIdForPrincipalName(String principalName)
    {
        throw new UnsupportedOperationException();
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.person.PersonIdMapper#getIamIdForEmployeeId(java.lang.String)
     */
    @Override
    public String getIamIdForEmployeeId(String employeeId)
    {
        throw new UnsupportedOperationException();
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.person.PersonIdMapper#getIamIdForStudentId(java.lang.String)
     */
    @Override
    public String getIamIdForStudentId(String studentId)
    {
        throw new UnsupportedOperationException();
    }


    @Override
    public String getIamIdForUserId(String userId)
    {
        throw new UnsupportedOperationException();
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.person.PersonIdMapper#getEmployeeIdForPersonId(java.lang.String)
     */
    @Override
    public String getEmployeeIdForPersonId(String personId)
    {
        final String query =
            "SELECT Value AS EmployeeID FROM UserIdentifier UI" +
            " LEFT JOIN UserAccount UA ON UA.UserID=UI.UserID" +
            " WHERE PersonUUID=? AND IdentifierID=1";
        final String employeeId = queryForString(query, "EmployeeID", personId);
        return employeeId;
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.person.PersonIdMapper#getEmployeeIdForIamId(java.lang.String)
     */
    @Override
    public String getEmployeeIdForIamId(String iamId)
    {
        throw new UnsupportedOperationException();
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.person.PersonIdMapper#getEmployeeIdForPrincipalName(java.lang.String)
     */
    @Override
    public String getEmployeeIdForPrincipalName(String principalName)
    {
        return this.getEmployeeIdForUserId(this.getUserIdForPrincipalName(principalName));
/*
        final String query =
            "SELECT Value AS EmployeeID FROM UserIdentifier UI" +
            " LEFT JOIN UserAccount UA ON UA.UserID=UI.UserID" +
            " WHERE Login=? AND IdentifierID=1";
        final String employeeId = queryForString(query, "EmployeeID", principalName);
        return employeeId;
*/
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.person.PersonIdMapper#getEmployeeIdForStudentId(java.lang.String)
     */
    @Override
    public String getEmployeeIdForStudentId(String studentId)
    {
        throw new UnsupportedOperationException();
    }


    @Override
    public String getEmployeeIdForUserId(String userId)
    {
        final String query = "SELECT Value AS EmployeeID FROM UserIdentifier WHERE UserID=? and IdentifierID=1";
        final String employeeId = queryForString(query, "EmployeeID", userId);
        return employeeId;
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.person.PersonIdMapper#getStudentIdForPersonId(java.lang.String)
     */
    @Override
    public String getStudentIdForPersonId(String personId)
    {
        throw new UnsupportedOperationException();
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.person.PersonIdMapper#getStudentIdForIamId(java.lang.String)
     */
    @Override
    public String getStudentIdForIamId(String iamId)
    {
        throw new UnsupportedOperationException();
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.person.PersonIdMapper#getStudentIdForPrincipalName(java.lang.String)
     */
    @Override
    public String getStudentIdForPrincipalName(String principalName)
    {
        throw new UnsupportedOperationException();
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.service.person.PersonIdMapper#getStudentIdForEmployeeId(java.lang.String)
     */
    @Override
    public String getStudentIdForEmployeeId(String studentId)
    {
        throw new UnsupportedOperationException();
    }


    @Override
    public String getStudentIdForUserId(String userId)
    {
        throw new UnsupportedOperationException();
    }


    @Override
    public String getUserIdForPersonId(String personId)
    {
        final String query = "SELECT UserID FROM UserAccount WHERE PersonUUID=?";
        final String userId = queryForString(query, "UserID", personId);
        return userId;
    }


    @Override
    public String getUserIdForIamId(String iamId)
    {
        throw new UnsupportedOperationException();
    }


    @Override
    public String getUserIdForPrincipalName(String principalName)
    {
        final String query1 = "SELECT UserID FROM UserAccount WHERE Login=?";
        final String query2 = "SELECT UserID FROM UserLogin WHERE Login=?";

        String userId = queryForString(query1, "UserID", principalName);
        if (userId == null) {
            userId = queryForString(query2, "UserID", principalName);
        }

        return userId;
    }


    @Override
    public String getUserIdForEmployeeId(String employeeId)
    {
        final String query = "SELECT UserID FROM UserIdentifier WHERE Value=? AND IdentifierID=1";
        final String userId = queryForString(query, "UserID", employeeId);
        return userId;
    }


    @Override
    public String getUserIdForStudentId(String studentId)
    {
        throw new UnsupportedOperationException();
    }


    private String queryForString(String query, String field, String...args)
    {
        String result = null;
        List<Map<String,String>> list = qt.getList(query, args);
        if (!list.isEmpty()) {
            result = list.get(0).get(field.toLowerCase());
        }
        return result;
    }
}
