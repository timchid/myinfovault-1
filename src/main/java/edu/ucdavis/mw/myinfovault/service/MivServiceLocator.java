/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: MivServiceLocator.java
 */


package edu.ucdavis.mw.myinfovault.service;

import edu.ucdavis.iet.commons.ldap.service.LdapPersonService;
import edu.ucdavis.mw.myinfovault.service.academicaction.AcademicActionService;
import edu.ucdavis.mw.myinfovault.service.archive.DossierArchiveService;
import edu.ucdavis.mw.myinfovault.service.audit.AuditService;
import edu.ucdavis.mw.myinfovault.service.authorization.AuthorizationService;
import edu.ucdavis.mw.myinfovault.service.biosketch.BiosketchService;
import edu.ucdavis.mw.myinfovault.service.biosketch.DocumentCreatorService;
import edu.ucdavis.mw.myinfovault.service.decision.DecisionService;
import edu.ucdavis.mw.myinfovault.service.disclosurecertificate.DCService;
import edu.ucdavis.mw.myinfovault.service.dossier.DossierCreatorService;
import edu.ucdavis.mw.myinfovault.service.dossier.DossierService;
import edu.ucdavis.mw.myinfovault.service.group.GroupService;
import edu.ucdavis.mw.myinfovault.service.packet.PacketService;
import edu.ucdavis.mw.myinfovault.service.person.UserService;
import edu.ucdavis.mw.myinfovault.service.publications.PublicationService;
import edu.ucdavis.mw.myinfovault.service.raf.RafService;
import edu.ucdavis.mw.myinfovault.service.report.UserReportService;
import edu.ucdavis.mw.myinfovault.service.signature.SigningService;
import edu.ucdavis.mw.myinfovault.service.snapshot.SnapshotService;
import edu.ucdavis.mw.myinfovault.service.workflow.WorkflowService;
import edu.ucdavis.mw.myinfovault.system.MivApplicationContext;


/**
 * This is a convenience class to retrieve spring beans from the MIV application context.
 *
 * @author Rick Hendricks
 * @author Craig Gilmore
 */
public final class MivServiceLocator
{
    // IMPORTANT - the Strings defined here must match the bean ID strings in myinfovault-service.xml
    public static final String AUTHORIZATION_SERVICE = "authorizationService";
    public static final String BIOSKETCH_SERVICE = "biosketchService";
    public static final String PUBLICATION_SERVICE = "publicationService";
    public static final String DC_SERVICE = "dcService";
    public static final String DOCUMENT_CREATOR_SERVICE = "documentCreatorService";
    public static final String DOSSIER_CREATOR_SERVICE = "dossierCreatorService";
    public static final String DOSSIER_SERVICE = "dossierService";
    public static final String DOSSIER_ARCHIVE_SERVICE = "dossierArchiveService";
    public static final String PERSONAL_INFORMATION_SERVICE = "personalInformationService";
    public static final String RAF_SERVICE = "rafService";
    public static final String ACADEMIC_ACTION_SERVICE = "academicActionService";
    public static final String DECISION_SERVICE = "decisionService";
    public static final String SIGNING_SERVICE = "signingService";
    public static final String SNAPSHOT_SERVICE = "snapshotService";
    public static final String USER_SERVICE = "userservice";
    public static final String GROUP_SERVICE = "groupService";
    public static final String AUDIT_SERVICE = "auditservice";
    public static final String USER_REPORT_SERVICE = "userReportService";
    public static final String WORKFLOW_SERVICE = "workflowService";
    public static final String LDAP_PERSON_SERVICE = "ldapPersonService";
    public static final String PACKET_SERVICE = "packetService";


    /**
     * Get a bean.
     *
     * @param beanName
     * @return bean in the Spring application context
     */
    @SuppressWarnings("unchecked")
    public static <T> T getBean(String beanName)
    {
        return (T) MivApplicationContext.getApplicationContext().getBean(beanName);
    }

    /**
     * Get the DossierService.
     * @return DossierService
     */
    public static DossierService getDossierService()
    {
        return getBean(DOSSIER_SERVICE);
    }

    /**
     * Get the PublicationService.
     * @return PublicationService
     */
    public static PublicationService getPublicationService()
    {
        return getBean(PUBLICATION_SERVICE);
    }

    /**
     * Get the DocumentCreatorService.
     * @return DocumentCreatorService
     */
    public static DocumentCreatorService getDocumentCreatorService()
    {
        return getBean(DOCUMENT_CREATOR_SERVICE);
    }

    /**
     * Get the DossierCreatorService.
     * @return DossierCreatorService
     */
    public static DossierCreatorService getDossierCreatorService()
    {
        return getBean(DOSSIER_CREATOR_SERVICE);
    }

    /**
     * Get the BiosketchService.
     * @return BiosketchService
     */
    public static BiosketchService getBiosketchService()
    {
        return getBean(BIOSKETCH_SERVICE);
    }

//    /**
//     * Get the PersonalInformationService.
//     * @return PersonalInformationService
//     */
//    public static PersonalInformationService getPersonalInformationService()
//    {
//        return (PersonalInformationService)MivApplicationContext.getApplicationContext().getBean(PERSONAL_INFORMATION_SERVICE);
//    }

    /**
     * Get the UserService.
     * @return UserService
     */
    public static UserService getUserService()
    {
        return getBean(USER_SERVICE);
    }

    /**
     * Get the GroupService.
     * @return GroupService
     */
    public static GroupService getGroupService()
    {
        return getBean(GROUP_SERVICE);
    }


// FIXME: Thinking about local MIV hasPermission and isAuthorized
/*
Use a "TempPermissionService"
Conform to the "IdentityManagementService" API for isAuthorized() and hasPermission()
isAuthorized:
    principalId, namespaceCode, permissionName, AttributeSet permissionDetails, AttributeSet qualification
hasPermission:
    principalId, namespaceCode, permissionName, AttributeSet permissionDetails

This way, calls to MivServiceLocator.getIdentityManagementService() can just be changed to
use KIMServiceLocator.getIdentityManagementService() instead.

The BIG job is determining what all the permissions (permissionNames) should be. What sort
of granularity to use, what kind of scoping.
Also have to consider Responsibility vs. Permission  -  what should we use in the (evolving) KIM model?
*/
    public static AuthorizationService getAuthorizationService()
    {
        return getBean(AUTHORIZATION_SERVICE);
    }




    /**
     * Get the SigningService.
     * @return SigningService
     */
    public static SigningService getSigningService()
    {
        return getBean(SIGNING_SERVICE);
    }

    public static DossierArchiveService getDossierArchiveService()
    {
        return getBean(DOSSIER_ARCHIVE_SERVICE);
    }

    /**
     * Get the SnapshotService.
     * @return SnapshotService
     */
    public static SnapshotService getSnapshotService()
    {
        return getBean(SNAPSHOT_SERVICE);
    }


    /**
     * Get the DCService.
     * @return DCService
     */
    public static DCService getDCService()
    {
        return getBean(DC_SERVICE);
    }

    /**
     * Get the RafService.
     * @return RafService
     */
    public static RafService getRafService()
    {
        return getBean(RAF_SERVICE);
    }

    /**
     * Get the AcademicActionService.
     * @return AcademicActionService
     */
    public static AcademicActionService getAcademicActionService()
    {
        return getBean(ACADEMIC_ACTION_SERVICE);
    }

    /**
     * @return decision service bean
     */
    public static DecisionService getDecisionService()
    {
        return getBean(DECISION_SERVICE);
    }

    /**
     * Get the AuditService.
     * @return AuditService
     */
    public static AuditService getAuditService()
    {
        return getBean(AUDIT_SERVICE);
    }

    /**
     * Get the UserReportService.
     * @return UserReportService
     */
    public static UserReportService getUserReportService()
    {
        return getBean(USER_REPORT_SERVICE);
    }

    /**
     * Get the WorkflowService.
     * @return WorkflowService
     */
    public static WorkflowService getWorkflowService()
    {
        return getBean(WORKFLOW_SERVICE);
    }

    /**
     * @return LDAP person service.
     */
    public static LdapPersonService getLdapPersonService()
    {
        return getBean(LDAP_PERSON_SERVICE);
    }

    /**
     * @return packet service.
     */
    public static PacketService getPacketService()
    {
        return getBean(PACKET_SERVICE);
    }
}
