/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: GroupDaoImpl.java
 */

package edu.ucdavis.mw.myinfovault.dao.group;

import java.io.IOException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import edu.ucdavis.mw.myinfovault.dao.RoleAssignmentDaoSupport;
import edu.ucdavis.mw.myinfovault.domain.group.Group;
import edu.ucdavis.mw.myinfovault.domain.group.GroupImpl;
import edu.ucdavis.mw.myinfovault.domain.group.GroupType;
import edu.ucdavis.mw.myinfovault.service.SqlAwareFilterAdapter;
import edu.ucdavis.mw.myinfovault.service.group.GroupCache;
import edu.ucdavis.mw.myinfovault.service.group.GroupTypeFilter;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.mw.myinfovault.service.person.UserService;
import edu.ucdavis.myinfovault.exception.MivSevereApplicationError;

/**
 * Handles MIV group persistence.
 *
 * @author Craig Gilmore
 * @since MIV 4.3
 */
public class GroupDaoImpl extends RoleAssignmentDaoSupport implements GroupDao
{
    private final String INSERT_GROUP_SQL;
    private final String UPDATE_GROUP_SQL;
    private final String DELETE_GROUP_SQL;
    private final String SELECT_GROUP_SQL;
    private final String SELECT_GROUPS_SQL;
    private final String INSERT_GROUP_MEMBER_SQL;
    private final String UPDATE_GROUP_MEMBER_SQL;
    private final String DELETE_GROUP_MEMBER_SQL;
    private final String SELECT_GROUP_MEMBERS_SQL;
    private final String SELECT_GROUP_MEMBERS_INACTIVE_SQL;

    private GroupCache groupCache = null;

    private UserService userService;

    /**
     * Spring injection setter for the user service.
     *
     * @param userService MIV user service
     */
    public void setUserService(UserService userService)
    {
        this.userService = userService;
    }

    /**
     * Creates the Group DAO object and initializes it with SQL
     * for handling the persistence of {@link Group} objects.
     *
     * @throws IOException If unable to access the default SQL properties file
     */
    public GroupDaoImpl() throws IOException
    {
        //role assignment DAO support for the GroupRoleAssignment table
        super("grouproleassignment.delete.id",
              "grouproleassignment.delete.groupid",
              "grouproleassignment.insert",
              "grouproleassignment.select.id",
              "grouproleassignment.select.groupid");

        //Groups SQL
        INSERT_GROUP_SQL = getSQL("groups.insert");
        UPDATE_GROUP_SQL = getSQL("groups.update");
        DELETE_GROUP_SQL = getSQL("groups.delete");
        SELECT_GROUP_SQL = getSQL("groups.select.id");
        SELECT_GROUPS_SQL = getSQL("groups.select");

        //GroupMembers SQL
        INSERT_GROUP_MEMBER_SQL = getSQL("groupmember.insert");
        UPDATE_GROUP_MEMBER_SQL = getSQL("groupmember.update");
        DELETE_GROUP_MEMBER_SQL = getSQL("groupmember.delete");
        SELECT_GROUP_MEMBERS_SQL = getSQL("groupmember.select");
        SELECT_GROUP_MEMBERS_INACTIVE_SQL = getSQL("groupmember.select.inactive");

        // Get an instance of the group cache
        this.groupCache = GroupCache.getInstance(new DBGroupCallback() {
            @Override
            public Group getGroup(Integer groupId)
            {
                return getJdbcTemplate().queryForObject(SELECT_GROUP_SQL,
                                                            new Object[]{groupId},
                                                            groupMapper);
            }
        });

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean persist(final Group group, final int actingUserId) throws DataAccessException
    {
        KeyHolder keyHolder = new GeneratedKeyHolder();

        //decide if update or insert
        final boolean groupExists = group.getId() > 0;

        int rowsAffected = getJdbcTemplate().update(new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(Connection connection) throws SQLException
            {
                PreparedStatement ps = connection.prepareStatement(groupExists
                                                                 ? UPDATE_GROUP_SQL
                                                                 : INSERT_GROUP_SQL,
                                                                   Statement.RETURN_GENERATED_KEYS);

                ps.setString(1, group.getName());
                ps.setString(2, group.getDescription());
                ps.setInt(3, group.getType().getId());
                ps.setString(4, group.getCreator().getPersonId());
                ps.setBoolean(5, group.isReadOnly());
                ps.setBoolean(6, group.getHidden());
                ps.setBoolean(7, group.isConfidential());
                ps.setBoolean(8, group.isActive());
                ps.setInt(9, actingUserId);

                ps.setInt(10, groupExists ? group.getId() : actingUserId);

                return ps;
            }
        }, keyHolder);

        //continue if the group was persisted
        if (rowsAffected > 0)
        {
            //current time in milliseconds
            group.setUpdated(new Date(System.currentTimeMillis()));

            //if group previously did not exists
            if (!groupExists)
            {
                //Set the group ID with the primary key just generated
                group.setId(keyHolder.getKey().intValue());

                //set created date equal to updated date
                group.setCreated(group.getUpdated());
            }

            persistAssignedRoles(group, actingUserId);
            persistMembers(group, actingUserId);

            // Force a refresh of the cache
            logger.info("Invalidating cache entry for group "+group.getId());
            groupCache.getCache().invalidate(new Integer(group.getId()));

            return true;//group was updated
        }

        return false;//group was not updated
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean delete(Group group) throws DataAccessException
    {
        if (group != null
            && getJdbcTemplate().update(DELETE_GROUP_SQL,
                                        new Object[]{group.getId()}) > 0)
        {
            // Invalidate this entry upon group delete
            logger.info("Invalidating cache entry for group "+group.getId());
            groupCache.getCache().invalidate(new Integer(group.getId()));
            return true;
        }

        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Group get(final int groupId) throws DataAccessException
    {
        // Gets the group from cache if available, otherwise group will be retrieved from DB and placed in cache
        Group result = groupCache.getCache().getIfPresent(groupId);
        result = result == null ? groupCache.getEntry(groupId) : result;
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Group> get(SqlAwareFilterAdapter<Group> filter) throws DataAccessException
    {

        // We get a list of the groups from the DB and then iterate to retrieve each from cache
        // or from the DB if not in cache with GroupDao#get .
        String sql = SELECT_GROUPS_SQL + filter.getWhere();

        List<Map<String, Object>> results = this.getJdbcTemplate().queryForList(sql, filter.getArguments());

        List<Group> groupList = new ArrayList<Group>();

        for (Map<String, Object> groupMap : results)
        {
            groupList.add(this.get((int) groupMap.get("ID")));
        }
        return groupList;
      }

    /**
     * Interface for group database access
     * @author rhendric
     *
     */
    public interface DBGroupCallback {
        public Group getGroup(Integer groupId);
    }

    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.group.GroupDao#refreshCache()
     */
    @Override
    public void refreshCache()
    {
        this.groupCache.getCache().invalidateAll();
        this.get(new GroupTypeFilter(GroupType.GROUP));
        this.groupCache.showCacheStats();
    }


    //Persist a set of members for the given group and acting person
    private void persistMembers(final Group group, final int actingUserId) throws DataAccessException
    {
        //the previous set of group members before this update
        Map<Integer, MivPerson> previousMembers = getMembers(group.getId());

        //insert members are in current, but not previous
        Map<Integer, MivPerson> insertMembers = new HashMap<>(group.getMemberMap());
        for (Integer key : previousMembers.keySet())
        {
            insertMembers.remove(key);
        }

        //update members are in both current and previous
        Map<Integer, MivPerson> updateMembers = new HashMap<>(group.getMemberMap());
        Iterator<Integer> updateMembersIt = updateMembers.keySet().iterator();
        while (updateMembersIt.hasNext())
        {
            if (!previousMembers.containsKey(updateMembersIt.next()))
            {
                updateMembersIt.remove();
            }
        }

        //delete members are in previous, but not current
        Map<Integer, MivPerson> deleteMembers = new HashMap<>(previousMembers);
        for (Integer key : group.getMemberMap().keySet())
        {
            deleteMembers.remove(key);
        }

        //persist sets with appropriate SQL
        persistMembers(actingUserId, group, insertMembers, INSERT_GROUP_MEMBER_SQL);
        persistMembers(actingUserId, group, updateMembers, UPDATE_GROUP_MEMBER_SQL);
        persistMembers(actingUserId, group, deleteMembers, DELETE_GROUP_MEMBER_SQL);
    }

    //insert, update, and delete group members
    private void persistMembers(final int actingUserId,
                                final Group group,
                                Map<Integer, MivPerson> members,
                                final String SQL)
    {
        for (final MivPerson member : members.values())
        {
            getJdbcTemplate().update(new PreparedStatementCreator() {
                @Override
                public PreparedStatement createPreparedStatement(Connection connection) throws SQLException
                {
                    PreparedStatement ps = connection.prepareStatement(SQL);
                    int parameterIndex = 0;

                    //for updates and inserts
                    if (!SQL.equals(DELETE_GROUP_MEMBER_SQL))
                    {
                        ps.setBoolean(++parameterIndex, !group.getInactiveMap().containsKey(member.getUserId()));
                        ps.setInt(++parameterIndex, actingUserId);
                    }

                    ps.setInt(++parameterIndex, group.getId());
                    ps.setString(++parameterIndex, member.getPersonId());

                    return ps;
                }
            });
        }
    }

    /**
     * Get the members of a given group.
     *
     * @param groupId ID of the group
     * @return list MIV persons that are members of the given group
     * @throws DataAccessException if the query fails
     */
    private Map<Integer, MivPerson> getMembers(int groupId) throws DataAccessException
    {
        List<MivPerson> prevMembers = get(SELECT_GROUP_MEMBERS_SQL, memberMapper, groupId);
        Map<Integer, MivPerson> previousMembers = new HashMap<>();

        for (MivPerson person : prevMembers)
        {
            previousMembers.put(person.getUserId(), person);
        }

        return previousMembers;
    }

    /**
     * Get the inactive members of a given group.
     *
     * @param groupId ID of the group
     * @return list MIV persons that are inactive members of the given group
     * @throws DataAccessException if the query fails
     */
    private List<MivPerson> getInactive(final int groupId) throws DataAccessException
    {
        return get(SELECT_GROUP_MEMBERS_INACTIVE_SQL, memberMapper, groupId);
    }

    /**
     * Persist the set of assigned roles associated with the given group.
     *
     * @param group
     * @param actingUserId
     * @throws DataAccessException
     */
    private void persistAssignedRoles(Group group, int actingUserId) throws DataAccessException
    {
        //delete all roles for this group
        deleteAssignedRoles(group.getId());

        //insert the new group roles
        insertAssignedRoles(group.getId(),
                            group.getAssignedRoles(),
                            actingUserId);
    }

    /**
     * Creates and maps 'Groups' table records to {@link Group} objects.
     */
    private RowMapper<Group> groupMapper = new RowMapper<Group>()
    {
        @Override
        public Group mapRow(ResultSet rs, int rowNum) throws SQLException
        {
            final int groupId = rs.getInt("ID");

            try
            {
                return new GroupImpl(
                        groupId,
                        rs.getString("Name"),
                        rs.getString("Description"),
                        GroupType.valueOf(rs.getString("Type")),
                        userService.getPersonByPersonUuid(rs.getString("CreatorPersonUUID")),
                        rs.getDate("InsertTimestamp"),
                        rs.getDate("UpdateTimestamp"),
                        rs.getBoolean("ReadOnly"),
                        rs.getBoolean("Private"),
                        rs.getBoolean("Confidential"),
                        rs.getBoolean("Active"),
                        getAssignedRoles(groupId),
                        getInactive(groupId),
                        getMembers(groupId).values());
            }
            catch (IllegalArgumentException e)
            {
                throw new MivSevereApplicationError("Unable to create group from database record", e);
            }
        }
    };

    /**
     * Creates and maps 'GroupMember' table records to {@link MivPerson} objects.
     */
    private RowMapper<MivPerson> memberMapper = new RowMapper<MivPerson>()
    {
        @Override
        public MivPerson mapRow(ResultSet rs, int rowNum) throws SQLException
        {
            return userService.getPersonByPersonUuid(rs.getString("PersonUUID"));
        }
    };
}
