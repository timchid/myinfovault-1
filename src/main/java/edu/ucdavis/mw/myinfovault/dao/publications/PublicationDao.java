/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: PublicationDao.java
 */

package edu.ucdavis.mw.myinfovault.dao.publications;

import java.util.Collection;
import java.util.List;

import org.springframework.dao.DataAccessException;

import edu.ucdavis.mw.myinfovault.domain.publications.Publication;

/**
 * Handles publication persistence.
 *
 * @author Craig Gilmore
 * @since MIV 3.9.1
 */
public interface PublicationDao
{
    /**
     * Persist the collection of publications to the database for the given target person.
     *
     * @param publications The collection of publications to persist
     * @param realUserId The ID of the real logged-in person
     * @return The number of records inserted
     * @throws DataAccessException
     */
    public int persistPublications(Collection<Publication> publications, int realUserId) throws DataAccessException;

    /**
     * Retrieve a list of publications for the given user.
     *
     * @param userId The MIV user ID associated with the publications
     * @return A list of publications
     * @throws DataAccessException
     */
    public List<Publication> getPublications(int userId) throws DataAccessException;
}
