/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: GroupDao.java
 */

package edu.ucdavis.mw.myinfovault.dao.group;

import java.util.List;

import org.springframework.dao.DataAccessException;

import edu.ucdavis.mw.myinfovault.domain.group.Group;
import edu.ucdavis.mw.myinfovault.service.SqlAwareFilterAdapter;

/**
 * Handles MIV group persistence.
 *
 * @author Craig Gilmore
 * @since MIV 4.3
 */
public interface GroupDao
{
    /**
     * Persists the MIV group to the database. An update occurs if the group already exists. An insert
     * occurs otherwise.
     *
     * @param group The MIV group to update or insert
     * @param actingUserId The user ID of the switched-to, acting person
     * @return <code>true</code> on successful group persistence, <code>false</code> if no update occurred
     * @throws DataAccessException on errors issuing update
     */
    public boolean persist(Group group, int actingUserId) throws DataAccessException;

    /**
     * Removes the MIV group from the database.
     *
     * @param group The group to delete
     * @return <code>true</code> on successful deletion, <code>false</code> if no delete occurred
     * @throws DataAccessException on errors issuing update
     */
    public boolean delete(Group group) throws DataAccessException;

    /**
     * Retrieve a group from the database.
     *
     * @param groupId The ID of the group to retrieve
     * @return The MIV group of MivPerson members
     * @throws DataAccessException on query failure
     */
    public Group get(int groupId) throws DataAccessException;

    /**
     * Retrieve from the database a list of all groups meeting the filter criteria.
     *
     * @param filter An SQL aware filter maintaining a corresponding SQL WHERE or AND clause
     * @return A list of MIV groups of MivPerson members
     * @throws DataAccessException on query failure
     */
    public List<Group> get(SqlAwareFilterAdapter<Group> filter) throws DataAccessException;

    /**
     * Refresh the group cache
     */
    public void refreshCache();

}
