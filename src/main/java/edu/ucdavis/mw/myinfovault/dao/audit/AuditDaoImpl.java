/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: AuditDaoImpl.java
 */

package edu.ucdavis.mw.myinfovault.dao.audit;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import edu.ucdavis.myinfovault.MivEntity;
import edu.ucdavis.myinfovault.audit.AuditAction;
import edu.ucdavis.myinfovault.audit.AuditObject;

/**
 * Implementation for Audit trail
 * @author Pradeep K Haldiya
 * @since MIV 4.0
 */
public class AuditDaoImpl extends JdbcDaoSupport implements AuditDao
{

    private static Logger logger = Logger.getLogger(AuditDaoImpl.class);

    final static String INSERT_AUDIT_TRAIL_SQL = "INSERT INTO AuditTrail" +
            "(Actor,ActionType,EntityType,EntityObjectID,Description,Remarks) VALUES(?,?,?,?,?,?)";

    final static String AUDIT_TRAIL_ENTITY_OBJECT_ID_SQL = "SELECT ID,Actor,ActionType,EntityType,EntityObjectID,Description,Remarks,ActionDate" +
            "  FROM AuditTrail WHERE EntityObjectID = ? ORDER BY ActionDate DESC";


    /**
     * to create audit trail
     * @param auditObject
     * //@//throws MivAuditTrailException
     */
    @Override
    public void createAuditTrail(final AuditObject auditObject)// throws MivAuditTrailException
    {
        try
        {
            KeyHolder keyHolder = new GeneratedKeyHolder();

            this.getJdbcTemplate().update(new PreparedStatementCreator() {
                @Override
                public PreparedStatement createPreparedStatement(Connection connection) throws SQLException
                {
                    PreparedStatement ps = connection.prepareStatement(INSERT_AUDIT_TRAIL_SQL, Statement.RETURN_GENERATED_KEYS);

                    int setCount = 0;
                    // Actor
                    if (auditObject.getActor() != null){
                        ps.setString(++setCount, auditObject.getActor());
                    }
                    else {
                        ps.setNull(++setCount, Types.VARCHAR);
                    }

                    // ActionType
                    if (auditObject.getActionType() != null) {
                        ps.setInt(++setCount, auditObject.getActionType().getActionId());
                    }
                    else {
                        ps.setInt(++setCount, AuditAction.AUDIT.getActionId());
                    }

                    // EntityType
                    if (auditObject.getEntityType() != null) {
                        ps.setInt(++setCount, auditObject.getEntityType().getEntityId());
                    }
                    else {
                        ps.setInt(++setCount, MivEntity.DEFAULT.getEntityId());
                    }

                    // EntityObjectID
                    if (auditObject.getEntityObjectID() != null) {
                        ps.setString(++setCount, auditObject.getEntityObjectID());
                    }
                    else {
                        ps.setNull(++setCount, Types.VARCHAR);
                    }

                    // Description
                    if (auditObject.getDescription() != null) {
                        ps.setString(++setCount, auditObject.getDescription());
                    }
                    else {
                        ps.setNull(++setCount, Types.VARCHAR);
                    }

                    // Remarks
                    if (auditObject.getRemarks() != null) {
                        ps.setString(++setCount, auditObject.getRemarks());
                    }
                    else {
                        ps.setNull(++setCount, Types.VARCHAR);
                    }

                    return ps;
                }
            }, keyHolder);

            //getting the inserted audit ID
//            int auditID = keyHolder.getKey().intValue();
//            if (auditID > 0)
//            {
//                logger.info("_AUDIT : ["+auditObject.getDescription()+"]");
//            }
        }
        catch (DataAccessException e)
        {
            // Use of the Audit Trail should be easy, without needing a try...catch
            // Log an error instead of throwing an exception.
            // If it turns out this is a real problem we could also add sending an email
            //throw new MivAuditTrailException(ErrorMessages.getInstance().getString("errors.audit.dao.write"));
            logger.error("There was a Data Access ERROR when trying to save the audit trail!  Audit information follows this traceback.", e);
        }
        catch (Exception e)
        {
            // Log and Swallow -ALL- exceptions so that use of the Audit Service is non-intrusive
//            throw new MivAuditTrailException(exception.getMessage());
            logger.error("There was an unexpected ERROR [" + e.getLocalizedMessage()
                    + "] when trying to save the audit trail!  Audit information follows this traceback.", e);
        }
        finally
        {
            logger.info("_AUDIT : [" + auditObject.getDescription() + "]");
        }
    }


    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.audit.AuditDao#getAuditTrailByEntityObjectId(int)
     */
    @Override
    public List<AuditObject> getAuditTrailByEntityObjectId(int entityObjectId)
    {
        List<AuditObject> auditList = getAuditTrail(AUDIT_TRAIL_ENTITY_OBJECT_ID_SQL, entityObjectId);

        if (auditList.size() != 1) return null;

        return auditList;
    }


    private List<AuditObject> getAuditTrail(String sql, Object...params)
    {
        List<AuditObject> auditList = null;

        try
        {
            auditList = this.getJdbcTemplate().query(sql, params, auditTrailMapper);
        }
        catch (DataAccessException exception)
        {
            auditList = new ArrayList<AuditObject>();
        }

        return auditList;
    }


    private RowMapper<AuditObject> auditTrailMapper = new RowMapper<AuditObject>()
    {
        @Override
        public AuditObject mapRow(ResultSet rs, int rowNum) throws SQLException
        {
            AuditObject audit = new AuditObject(
                rs.getInt("ID"),
                AuditAction.getAuditActionById(rs.getInt("ActionType")),
                MivEntity.getMivEntityById(rs.getInt("EntityType")),
                rs.getString("EntityObjectID"),
                rs.getString("Description"),
                rs.getString("Remarks"),
                rs.getString("ActionDate"),
                rs.getString("Actor")
            );

            return audit;
        }
    };

}
