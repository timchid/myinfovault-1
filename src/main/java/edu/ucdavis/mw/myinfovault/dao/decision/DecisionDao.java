/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: DecisionDao.java
 */

package edu.ucdavis.mw.myinfovault.dao.decision;

import java.util.List;

import edu.ucdavis.mw.myinfovault.domain.decision.Decision;
import edu.ucdavis.mw.myinfovault.domain.dossier.Dossier;
import edu.ucdavis.mw.myinfovault.util.MivDocument;

/**
 * Handles {@link Decision} persistence.
 *
 * @author Craig Gilmore
 * @since MIV 4.8
 */
public interface DecisionDao
{
    /**
     * Get a VP/dean recommendations/decisions for the given dossier.
     *
     * @param dossier dossier to which the decision belongs
     * @return matching decisions/recommendations
     */
    public List<Decision> getDecisions(Dossier dossier);

    /**
     * Get a VP/dean recommendation/decision for the given dossier, type, and scope.
     *
     * @param dossier dossier to which the decision belongs
     * @param documentType type of decision/recommendation
     * @param schoolId school ID to which the decision applies
     * @param departmentId department ID to which the decision applies
     * @return matching decision/recommendation or <code>null</code> if DNE
     */
    public Decision getDecision(Dossier dossier,
                                MivDocument documentType,
                                int schoolId,
                                int departmentId);

    /**
     * Get a VP/dean recommendation/decision for the given record ID.
     *
     * @param recordId record ID of the decision
     * @return matching decision/recommendation or <code>null</code> if DNE
     */
    public Decision getDecision(int recordId);

    /**
     * Persist to the database a Vice-provost/dean decision/recommendation.
     *
     * @param decision VP/dean decision/recommendation
     * @return if persistence was successful
     */
    public boolean persistDecision(Decision decision);

    /**
     * Remove decision record from database.
     *
     * @param decision decision to remove
     * @return if decision was removed
     */
    public boolean deleteDecision(Decision decision);
}
