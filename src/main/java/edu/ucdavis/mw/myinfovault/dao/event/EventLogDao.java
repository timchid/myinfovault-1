/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: EventLogDao.java
 */

package edu.ucdavis.mw.myinfovault.dao.event;

import java.util.List;

import org.springframework.dao.DataAccessException;

import edu.ucdavis.mw.myinfovault.domain.event.EventLogEntry;

/**
 * Handles persistence and retrieval of {@link EventLogEntry} objects from the database.
 *
 * @author Craig Gilmore
 * @since MIV 4.7.2
 */
public interface EventLogDao
{
    /**
     * Get all event log entries for the given dossier.
     *
     * @param dossierId dossier ID
     * @return event log entries for the given dossier
     * @throws DataAccessException if query fails
     */
    public List<EventLogEntry> getEntries(long dossierId) throws DataAccessException;

    /**
     * Persist event log entry.
     *
     * @param dossierId dossier ID associated with the event log entry
     * @param entry event log entry to persist
     * @throws DataAccessException if persistence fails
     */
    public void persistEntry(long dossierId,
                             EventLogEntry entry) throws DataAccessException;
}
