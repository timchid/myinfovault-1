/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: SignatureDao.java
 */

package edu.ucdavis.mw.myinfovault.dao.signature;

import java.util.List;

import edu.ucdavis.mw.myinfovault.domain.signature.MivElectronicSignature;
import edu.ucdavis.mw.myinfovault.domain.signature.Signable;

/**
 * For requesting, signing, persisting MIV document signatures.
 *
 * @author Craig Gilmore
 * @since MIV 3.0
 */
public interface SignatureDao
{
    /**
     * Get all requested signatures.
     *
     * @return requested signatures
     */
    public List<MivElectronicSignature> getRequestedSignatures();

    /**
     * Returns a list of requested signatures for the given user ID.
     *
     * @param userId ID of the user from whom the signatures have been requested
     * @return list of electronic signatures
     */
    public List<MivElectronicSignature> getRequestedSignatures(int userId);

    /**
     * Returns a list of signatures for the given document.
     *
     * @param document signable document
     * @return list of electronic signatures
     */
    public List<MivElectronicSignature> getSignatures(Signable document);

    /**
     * Returns a list of requested signatures for a given document type and IDs.
     *
     * @param document signable document
     * @return list of electronic signatures
     */
    public List<MivElectronicSignature> getRequestedSignatures(Signable document);

    /**
     * Get the requested signatures for the given dossier.
     *
     * @param dossierId dossier ID
     * @return list of electronic signatures
     */
    public List<MivElectronicSignature> getRequestedSignatures(long dossierId);

    /**
     * Get the list of requested signatures for the given dossier and scope.
     *
     * @param dossierId document dossier ID
     * @param schoolId document school ID
     * @param departmentId document department ID
     * @return list of electronic signatures
     */
    public List<MivElectronicSignature> getRequestedSignatures(long dossierId, int schoolId, int departmentId);

    /**
     * Get the list of signatures for the given dossier, scope, and user.
     *
     * @param dossierId document dossier ID
     * @param schoolId document school ID
     * @param departmentId document department ID
     * @param userId ID of the user from whom the signature has been requested/signed
     * @return list of electronic signatures
     */
    public List<MivElectronicSignature> getSignatures(long dossierId, int schoolId, int departmentId, int userId);

    /**
     * Returns a signature for a given record ID.
     *
     * @param recordId database record ID for the signature
     * @return the electronic signature or <code>null</code> if DNE
     */
    public MivElectronicSignature getSignature(int recordId);

    /**
     * Returns a signature for a given user ID, Document ID, and DocumentType.
     *
     * @param userId ID of the user from whom the signature has been requested/signed
     * @param document signable document
     * @return an electronic signature or <code>null</code> if DNE
     */
    public MivElectronicSignature getSignature(int userId, Signable document);

    /**
     * Returns the signed signature for the given Document.
     *
     * @param document signable document
     * @return an electronic signature or <code>null</code> if DNE
     */
    public MivElectronicSignature getSignedSignature(Signable document);

    /**
     * Persists an electronic signature to the database.
     *
     * @param signature document signature to save
     * @param realUserID ID of the real logged-in user
     * @return if signature was persisted
     */
    public boolean persistSignature(MivElectronicSignature signature, int realUserID);

    /**
     * Deletes an electronic signature.
     *
     * @param recordId
     * @return if the delete operation had any effect
     */
    public boolean deleteSignature(int recordId);
}
