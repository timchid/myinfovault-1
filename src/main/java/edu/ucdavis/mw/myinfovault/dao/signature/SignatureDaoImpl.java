/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: SignatureDaoImpl.java
 */

package edu.ucdavis.mw.myinfovault.dao.signature;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Collections;
import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import edu.ucdavis.mw.myinfovault.dao.DaoSupport;
import edu.ucdavis.mw.myinfovault.domain.signature.MivElectronicSignature;
import edu.ucdavis.mw.myinfovault.domain.signature.Signable;
import edu.ucdavis.mw.myinfovault.service.MivServiceLocator;
import edu.ucdavis.mw.myinfovault.util.MivDocument;
import edu.ucdavis.myinfovault.exception.MivSevereApplicationError;

/**
 * Handles MIV document signature requests, signing, and persistence to the database.
 *
 * @author Craig Gilmore
 * @since MIV 3.0
 */
public class SignatureDaoImpl extends DaoSupport implements SignatureDao
{
    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.signature.SignatureDao#getRequestedSignatures()
     */
    @Override
    public List<MivElectronicSignature> getRequestedSignatures()
    {
        return getElectronicSignatures(null, getSQL("signature.select.requested"));
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.signature.SignatureDao#getRequestedSignatures(int)
     */
    @Override
    public List<MivElectronicSignature> getRequestedSignatures(int userId)
    {
        return getElectronicSignatures(null, getSQL("signature.select.requested.user"), userId);
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.signature.SignatureDao#getSignatures(edu.ucdavis.mw.myinfovault.domain.signature.Signable)
     */
    @Override
    public List<MivElectronicSignature> getSignatures(Signable document)
    {
        return getElectronicSignatures(document, getSQL("signature.select.document"), document.getDocumentType().getId(), document.getDocumentId());
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.signature.SignatureDao#getRequestedSignatures(edu.ucdavis.mw.myinfovault.domain.signature.Signable)
     */
    @Override
    public List<MivElectronicSignature> getRequestedSignatures(Signable document)
    {
        return getElectronicSignatures(document, getSQL("signature.select.requested.document"), document.getDocumentType().getId(), document.getDocumentId());
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.signature.SignatureDao#getRequestedSignatures(long)
     */
    @Override
    public List<MivElectronicSignature> getRequestedSignatures(long dossierId)
    {
        return getElectronicSignatures(null, getSQL("signature.select.requested.dossier"), dossierId);
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.signature.SignatureDao#getRequestedSignatures(long, int, int)
     */
    @Override
    public List<MivElectronicSignature> getRequestedSignatures(long dossierId, int schoolId, int departmentId)
    {
        return getElectronicSignatures(null, getSQL("signature.select.requested.dossierAndScope"), dossierId, schoolId, departmentId);
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.signature.SignatureDao#getSignatures(long, int, int, int)
     */
    @Override
    public List<MivElectronicSignature> getSignatures(long dossierId, int schoolId, int departmentId, int userId)
    {
        return getElectronicSignatures(null, getSQL("signature.select.dossierAndUser"), dossierId, schoolId, departmentId, userId);
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.signature.SignatureDao#getElectronicSignature(int)
     */
    @Override
    public MivElectronicSignature getSignature(int recordId)
    {
        return getFirst(getElectronicSignatures(null, getSQL("signature.select.id"), recordId));
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.signature.SignatureDao#getSignature(int, edu.ucdavis.mw.myinfovault.domain.signature.Signable)
     */
    @Override
    public MivElectronicSignature getSignature(int userId, Signable document)
    {
        return getFirst(getElectronicSignatures(document, getSQL("signature.select.documentAndUser"), userId, document.getDocumentType().getId(), document.getDocumentId()));
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.signature.SignatureDao#getSignedSignature(edu.ucdavis.mw.myinfovault.domain.signature.Signable)
     */
    @Override
    public MivElectronicSignature getSignedSignature(Signable document)
    {
        return getFirst(getElectronicSignatures(document, getSQL("signature.select.signed.documentAndUser"), document.getDocumentType().getId(), document.getDocumentId()));
    }

    /**
     * Get a list of signatures for the given statement and  parameters.
     *
     * @param sql SQL statement to execute
     * @param params parameters that correspond to the given statement
     * @return list of signatures
     */
    private List<MivElectronicSignature> getElectronicSignatures(final Signable document, String sql, Object...params)
    {
        try
        {
            List<MivElectronicSignature> signatures = get(sql, new RowMapper<MivElectronicSignature>()
            {
                @Override
                public MivElectronicSignature mapRow(ResultSet rs, int rowNum) throws SQLException
                {
                    /*
                     * The signable document for which the signature applies.
                     *
                     * Use the given signable document if not null.
                     * Otherwise, get a new fresh instance.
                     */
                    Signable signatureDocument = document != null
                                               ? document
                                               : MivServiceLocator.getSigningService().getDocument(MivDocument.get(rs.getInt("DocumentType")),
                                                                                                   rs.getInt("DocumentID"));

                    try
                    {
                        return new MivElectronicSignature(rs.getInt("UserID"),
                                                          signatureDocument,    // FIXME: Here's where we go awry. A signature has-a document? No, a document has-a signature
                                                          rs.getInt("DocumentVersion"),
                                                          rs.getInt("ID"),
                                                          rs.getBoolean("Signed"),
                                                          rs.getBoolean("Signed") ? rs.getString("UpdateUserID") : null,
                                                          rs.getLong("SignTime"),
                                                          rs.getString("DigestType"),
                                                          rs.getString("CompatibleVersion"),
                                                          rs.getBytes("DocumentHash"),
                                                          rs.getTimestamp("InsertTimestamp"));
                    }
                    catch (IllegalArgumentException e)
                    {
                        /*
                         * FIXME: What should be done here?
                         *
                         * This row of the result set contains a signature that is not valid.
                         * Ignore it or remove it? For now, making a log entry and returning null.
                         */
                        logger.warn("DocumentSignature record #" + rs.getInt("ID") + " is invalid!", e);
                        return null;
                    }
                }
            }, params);

            // remove all null members of the list
            signatures.removeAll(Collections.singleton(null));

            return signatures;
        }
        catch (DataAccessException e)
        {
            logger.error("got a DB error when looking for signatures", e);

            return Collections.emptyList();
        }
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.signature.SignatureDao#persistSignature(edu.ucdavis.mw.myinfovault.domain.signature.MivElectronicSignature, int)
     */
    @Override
    public boolean persistSignature(final MivElectronicSignature signature, final int actingUserId)
    {
        final boolean isNewRecord = signature.getRecordId() == 0;

        try
        {
            KeyHolder keyHolder = new GeneratedKeyHolder();

            int recordsAffected = getJdbcTemplate().update(new PreparedStatementCreator() {
                @Override
                public PreparedStatement createPreparedStatement(Connection connection) throws SQLException
                {
                    PreparedStatement ps = connection.prepareStatement(getSQL(isNewRecord ? "signature.insert" : "signature.update"),
                                                                       Statement.RETURN_GENERATED_KEYS);

                    ps.setBoolean(1, signature.isSigned());
                    ps.setBytes(2, signature.getSignature());
                    ps.setLong(3, signature.getSigningDate() != null ? signature.getSigningDate().getTime() : 0L);
                    ps.setString(4, signature.getDigestType());
                    ps.setInt(5, actingUserId);
                    ps.setInt(6, signature.getDocument().getDocumentType().getId());

                    // for inserts only
                    if (isNewRecord)
                    {
                        ps.setInt(7, Integer.parseInt(signature.getRequestedSigner()));
                        ps.setInt(8, signature.getDocument().getDocumentId());
                        ps.setInt(9, signature.getDocument().getSchoolId());
                        ps.setInt(10, signature.getDocument().getDepartmentId());
                        ps.setLong(11, signature.getDocument().getDossier().getDossierId());
                        ps.setString(12, signature.getCompatibleVersion());
                    }
                    // for updates only
                    else
                    {
                        ps.setInt(7, signature.getDocumentVersion());
                        ps.setInt(8, signature.getRecordId());
                    }

                    return ps;
                }
            }, keyHolder);

            // an insert or update occurred
            if (recordsAffected > 0)
            {
                // update new signature with inserted record ID
                if (isNewRecord)
                {
                    signature.setRecordId(keyHolder.getKey().intValue());
                }

                // successful update/insert
                return true;
            }
        }
        catch(DataAccessException exception)
        {
            throw new MivSevereApplicationError("errors.signature.dao.write", exception);
        }

        // unsuccessful update/insert
        return false;
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.signature.SignatureDao#deleteSignature(int)
     */
    @Override
    public boolean deleteSignature(int signatureId)
    {
        try
        {
            return update(getSQL("signature.delete.id"), signatureId);
        }
        catch(DataAccessException exception)
        {
            throw new MivSevereApplicationError("errors.signature.dao.delete", exception);
        }
    }
}
