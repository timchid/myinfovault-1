/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: CourseEvaluationDaoImpl.java
 */

package edu.ucdavis.mw.myinfovault.dao.vetmed;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Collection;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.PreparedStatementCreator;

import edu.ucdavis.mw.myinfovault.dao.DaoSupport;
import edu.ucdavis.mw.myinfovault.domain.vetmed.CourseEvaluation;

/**
 * Handles persisting VetMed course evaluation records to the database.
 *
 * @author Craig Gilmore
 * @since MIV 4.7.2
 */
public class CourseEvaluationDaoImpl extends DaoSupport implements CourseEvaluationDao
{
    private final String EVALUATION_INSERT;

    /**
     * Create the VetMed course evaluation DAO bean.
     */
    public CourseEvaluationDaoImpl()
    {
        EVALUATION_INSERT = getSQL("courseevaluationsummary.insert");
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.vetmed.CourseEvaluationDao#persistEvaluations(int, java.util.Collection)
     */
    @Override
    public void persistEvaluations(final int realUserId, Collection<CourseEvaluation> evaluations) throws DataAccessException
    {
        for (final CourseEvaluation evaluation : evaluations)
        {
            getJdbcTemplate().update(new PreparedStatementCreator() {
                @Override
                public PreparedStatement createPreparedStatement(Connection connection) throws SQLException
                {
                    PreparedStatement ps = connection.prepareStatement(EVALUATION_INSERT);

                    ps.setInt(1, evaluation.getUser().getUserId());
                    ps.setInt(2, evaluation.getType().getId());
                    ps.setString(3, evaluation.getTerm().getDescription()+" "+Integer.toString(evaluation.getYear()));
                    ps.setString(4, evaluation.getCourse());
                    ps.setString(5, evaluation.getDescription());
                    ps.setString(6, evaluation.getResponseTotal());
                    ps.setString(7, evaluation.getInstructorScore());
                    ps.setString(8, evaluation.getCourseScore());
                    ps.setString(9, evaluation.getLink().toString());
                    ps.setInt(10, realUserId);

                    return ps;
                }
            });
        }
    }
}
