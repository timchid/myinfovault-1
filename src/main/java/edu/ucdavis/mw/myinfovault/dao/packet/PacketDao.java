/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: PacketDao.java
 */

package edu.ucdavis.mw.myinfovault.dao.packet;

import java.util.Date;
import java.util.List;

import edu.ucdavis.mw.myinfovault.domain.packet.Packet;
import edu.ucdavis.mw.myinfovault.domain.packet.PacketContent;
import edu.ucdavis.mw.myinfovault.domain.packet.PacketRequest;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;

/**
 * Data access object interface used to access PacketRequest objects.
 *
 * @author Rick Hendricks
 * @since MIV 5.0
 */
public interface PacketDao
{

    /**
     * Get all packet requests for the input userId
     * @param userId
     * @return List of PacketRequest objects
     */
    public List<PacketRequest> getPacketRequests(final long userId);

    /**
     * Get a specified packet request
     * @param packetRequestId
     * @return PacketRequest
     */
    public PacketRequest getPacketRequest(final long packetRequestId);

    /**
     * Update a PacketRequest
     * @param packetRequest
     * @param updateUserId
     * @return true if update is successful, otherwise false
     */
    public boolean updatePacketRequest(final PacketRequest packetRequest, final int updateUserId);

    /**
     * Delete a packetRequest
     * @param packetRequest
     */
    public boolean deletePacketRequest(final PacketRequest packetRequest);

    /**
     * Delete a packetRequest
     * @param int packetRequestId
     */
    public boolean deletePacketRequest(final int packetRequestId);

    /**
     * Get open packet requests for a user
     * @param userId for which to find open packet requests.
     * @return List of PacketRrequest objects
     */
    public List<PacketRequest> getOpenPacketRequests(final int userId);

    /**
     * Get open packet requests for a user
     * @param mivPerson
     * @return List of PacketRrequest objects
     */
    public List<PacketRequest> getOpenPacketRequests(final MivPerson mivPerson);

    /**
     * Get all packets for the input userId
     * @param userId
     * @return List of Packet objects
     */
    public List<Packet> getPackets(final int userId);

    /**
     * Get a packet
     * @param packetId
     * @return Packet
     */
    public Packet getPacket(final long packetId);

    /**
     * Get a packet
     * @param Packet
     * @return Packet
     */
    public Packet getPacket(final Packet packet);


    /**
     * Insert packet content items
     * @param Packet
     * @return true if update is successful, otherwise false
     */
    public boolean insertPacketContent(final Packet packet);

    /**
     * Delete a packet content item
     * @param PacketContent item
     * @return true if delete is successful, otherwise false
     */
    public boolean deletePacketItem(final PacketContent packetItem);

    /**
     * Delete a packet content items
     * @param List PacketContent items
     */
    public void deletePacketItems(final List<PacketContent> packetItems);

    /**
     * update a packet
     * @param Packet
     * @param updateUserId
     * @return Packet
     */
    public Packet updatePacket(final Packet packet, final int updateUserId);

    /**
     * Delete a packet
     * @param int packetId
     */
    public boolean deletePacket(final long packetId);

    /**
     * Get the last time the specified packet was saved.
     *
     * @param packetId
     * @return The modification time of the specified packet.
     */
    public Date getPacketUpdateTime(final long packetId);

    /**
     * Update packet items
     * @param packetItems
     */
    public boolean updatePacketItems(List<PacketContent> packetItems);

    /**
     * Copy packet annotations from a source packet to a target packet
     * @param Packet - source packet
     * @param Packet - target packet
     * @param updateUserId
     * @return boolean
     */
    public boolean copyPacketAnnotations(final Packet sourcePacket, final Packet targetPacket, final int updateUserId);

}

