/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: DaoSupport.java
 */

package edu.ucdavis.mw.myinfovault.dao;

import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

import com.google.common.eventbus.Subscribe;

import edu.ucdavis.mw.myinfovault.events.ConfigurationChangeEvent;
import edu.ucdavis.mw.myinfovault.events.EventDispatcher;
import edu.ucdavis.myinfovault.ConfigReloadConstants;
import edu.ucdavis.myinfovault.exception.MivSevereApplicationError;

/**
 * JdbcDaoSupport with SQL properties file made available.
 *
 * @author Craig Gilmore
 * @since 4.3
 */
public abstract class DaoSupport extends JdbcDaoSupport
{
    /**
     * Simple Logging Facade for Java (SLF4J) logger.
     */
    protected static Logger log = LoggerFactory.getLogger(DaoSupport.class);

    private static final String DEFAULT_PROPERTIES_FILENAME = "/sql.properties";
    private static final String DEFAULT_SCHEMA = "myinfovault";
    private static final String KEY_DELIMITER = ".";

    private final String propertiesFilename;
    private final String schema;

    private Properties properties = new Properties();

    /**
     * Construct with default SQL properties file and schema.
     */
    protected DaoSupport()
    {
        this(DEFAULT_PROPERTIES_FILENAME, DEFAULT_SCHEMA);
    }


    /**
     * Construct with the default SQL properties file using the given schema as key.
     *
     * @param schema database name used as a prefix for SQL statement property keys
     */
    protected DaoSupport(String schema)
    {
        this(schema, false);
    }

    /**
     * Construct with the default SQL properties file and a schema or sub-schema.<br>
     *
     * Using this with <code>append=false</code> is the same as {@link #DaoSupport(String)},
     * where the schema <em>replaces</em> the default schema name.
     *
     * @param subschema
     * @param append
     */
    protected DaoSupport(String subschema, boolean append)
    {
        this( DEFAULT_PROPERTIES_FILENAME, append ? DEFAULT_SCHEMA + KEY_DELIMITER + subschema : subschema );
    }

    /**
     * Construct with given SQL properties and schema (database) name.
     *
     * @param propertiesFilename
     * @param schema database name used as a prefix for SQL statement property keys
     */
    protected DaoSupport(final String propertiesFilename, final String schema)
    {
        this.propertiesFilename = propertiesFilename;
        this.schema = schema;

        try {
            loadProperties();
        }
        catch (IOException e) {
            throw new MivSevereApplicationError("errors.daosupport.loadProperties", e, propertiesFilename);
        }

        // Register as a listener for configuration changes, but avoid
        // duplicate registration due to being wrapped by dependency injection.
        String className = this.getClass().getSimpleName();
        if (! className.contains("$$EnhancerByCGLIB$$"))
        {
            // Register this as a listener
            log.debug("|| ----> Registering DAO {} as a Change Listener", this.getClass().getSimpleName());
            EventDispatcher.getDispatcher().register(this);
        }
        else
        {
            log.debug("|| ----> Skipping Registration of wrapped DAO {}", this.getClass().getSimpleName());
        }
    }


    /**
     * Load properties read from file in the package of the subclass.
     */
    private void loadProperties() throws IOException
    {
        /*
         * this.getClass() returns the runtime subclass for this object which
         * changes the path based on the subclass package
         */
        InputStream in = DaoSupport.class.getResourceAsStream(this.propertiesFilename);

        if (in == null) {
            throw new IOException("Unable to open SQL properties file [" + this.propertiesFilename + "]");
        }

        properties.load(in);

        in.close();
    }


    /**
     * Get SQL string for property key starting with table name
     *
     * @param propertyKey Corresponds to SQL string in sql.properties
     * @return SQL string for the given property
     */
    protected String getSQL(final String propertyKey)
    {
        return properties.getProperty(this.schema + KEY_DELIMITER + propertyKey);
    }


    /**
     * Get the first object in the given iteration.
     *
     * @param i iteration
     * @return the first object in the iteration or <code>null</code> if none
     */
    protected <E, T extends Iterable<E>> E getFirst(T i)
    {
        Iterator<E> it = i.iterator();

        /*
         * Return one or none
         */
        return it.hasNext() ? it.next() : null;
    }


    /**
     * Perform an update for the given SQL statement and parameter.
     *
     * @param SQL statement to execute
     * @param parameters parameter for the given statement
     * @return if an update occurred
     * @throws DataAccessException if unable to execute query
     */
    protected boolean update(String SQL, Object...parameters) throws DataAccessException
    {
        return getJdbcTemplate().update(SQL, parameters) > 0;
    }


    /**
     * Get the result for the given statement and parameters mapped by the given row mapper.
     *
     * @param SQL statement
     * @param mapper row mapper
     * @param params statement parameters
     * @return result
     * @throws DataAccessException if the query fails
     */
    protected <T> List<T> get(String SQL, RowMapper<T> mapper, Object...params) throws DataAccessException
    {
        return getJdbcTemplate().query(SQL, params, mapper);
    }


    /**
     * @param evt configuration event triggering this execution
     */
    @Subscribe
    public void configurationChanged(ConfigurationChangeEvent evt)
    {
        try
        {
            ConfigReloadConstants whatChanged = evt.getWhatChanged();

            switch (whatChanged)
            {
                case ALL:
                case PROPERTIES:
                    // Log the raw event
                    log.info("|| -----> SQL - Configuration Change Event: [{}] (class {})", evt, this.getClass().getSimpleName());
                    log.info("{} changed, we should re-load.", whatChanged);
                    // Reload properties
                    loadProperties();
                    break;
                default:
                    log.debug("{} changed, we will NOT re-load.", whatChanged);
                    break;
            }
        }
        catch (IOException e)
        {
            // This shouldn't happen as this error would probably be thrown when this was instantiated
            throw new MivSevereApplicationError("Reloading the SQL properties for this DAO has failed", e);
        }
    }
}
