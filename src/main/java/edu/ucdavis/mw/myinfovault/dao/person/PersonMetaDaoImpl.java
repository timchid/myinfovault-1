/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: PersonMetaDaoImpl.java
 */

package edu.ucdavis.mw.myinfovault.dao.person;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;

import edu.ucdavis.mw.myinfovault.service.person.Appointment;
import edu.ucdavis.mw.myinfovault.service.person.AssignedRole;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.mw.myinfovault.service.person.builder.BuiltPersonImpl;
import edu.ucdavis.mw.myinfovault.service.person.builder.MivAccountDto;
import edu.ucdavis.mw.myinfovault.service.person.builder.PersonBuilder;

/**
 * This "DAO" delegates all work to the DAOs of the complex parts that make up a Person
 * with their roles, assignments, and appointments.
 *
 * @author Stephen Paulsen
 * @since MIV 4.6
 */
public class PersonMetaDaoImpl implements PersonDao
{
    private static final Logger logger = LoggerFactory.getLogger(PersonMetaDaoImpl.class);

    private final UserAccountDao accountDao;
    private final RoleAssignmentDao roleDao;
    private final AppointmentDao appointmentDao;

    public PersonMetaDaoImpl(UserAccountDao accountDao,
                             RoleAssignmentDao roleDao,
                             AppointmentDao appointmentDao)
    {
        this.accountDao = accountDao;
        this.roleDao = roleDao;
        this.appointmentDao = appointmentDao;
    }


    public void init()
    {
        logger.debug("|| --- PersonMetaDaoImpl.init() got called because Spring wanted it!   It's MAGIC!");
    }


    @Override
    //@/Transactional
    public boolean insertPerson(MivPerson person, int actorId) // Return the new UserID or -1 instead of boolean ??
    {
        boolean inserted = false;

        MivAccountDto accountDto = this.accountDao.addAccount(actorId, PersonBuilder.buildDto(person));
        int newUserId = accountDto.getUserId();

        inserted = newUserId > 0;

        if (!inserted) {
            logger.warn("Person insert failure detected because userId of {} <= 0 for [{}]", newUserId, person);
            return false; // Just added 2013-11-20 SDP -- Ok for early exit?
        }

        if (inserted) {
            person.setActive(true);
            if (person instanceof BuiltPersonImpl) {    // This better be true or something's terribly wrong.
                ((BuiltPersonImpl) person).setUserId(newUserId);
            }
        }

        try {
            this.roleDao.insertAssignedRoles(newUserId, person.getAssignedRoles(), actorId);
        }
        catch (DataAccessException e) {
            logger.warn("Failed to add roles for new user [{}] Roles were: |{}|", person, person.getAssignedRoles());
            logger.warn("Traceback", e);
            inserted = false;
            return false;
        }

        // Set the UserID on the appointments so persistAppointments can compare them
        // to the existing appointments for the user - for which there are none.
        final Collection<Appointment> appointments = person.getAppointments();
        // MIV-4030 The AppointmentDaoImpl gets an ArrayIndexOutOfBoundsException when the Collection
        // is empty, and there's no point in doing any of this if there are no appointments.
        if (appointments != null && !appointments.isEmpty())
        {
            for (Appointment a : appointments) {
                a.setUserId(newUserId);
            }
            appointmentDao.persistAppointments(appointments);
        }

        return inserted;
    }


    @SuppressWarnings("unused")
    @Override
    //@/Transactional
    public boolean updatePerson(MivPerson person, int actorId)
    {
        // TODO Auto-generated method stub
        MivAccountDto dto = this.accountDao.updateAccount(actorId, PersonBuilder.buildDto(person));

        //this.roleDao.
        return updateRoleAssignments(person, actorId) && updateAppointments(person);
    }


    /**
     * Update the role assignments for the input MivPerson
     * by adding any newly added role assignments and removing any which may have
     * been deleted.
     *
     * @param person
     * @return boolean true = successful, otherwise false
     */
    private boolean updateRoleAssignments(MivPerson person, int actorId)
    {
        boolean updated = true;

        final Map<String,AssignedRole> addedRoles = new HashMap<String,AssignedRole>();

        // Get the currently assigned roles
        Collection<AssignedRole>currentAssignedRoles = person.getAssignedRoles();
        // Build a map of role:scope keys pointing to AssignedRole
        final Map<String, AssignedRole> currentRoles = new HashMap<String, AssignedRole>();

        for (AssignedRole assignedRole : currentAssignedRoles)
        {
            String roleScopeKey = assignedRole.getRole().getRoleId() + ":" + assignedRole.getScope().toString();
            currentRoles.put(roleScopeKey, assignedRole);
        }

        // Load the originally assigned roles from the database
        final Collection<AssignedRole> storedAssignedRoles = loadRoles(person.getUserId());

        logger.trace(" *** currentRoles: {}", currentAssignedRoles);
        logger.trace(" ***  roles in DB: {}", storedAssignedRoles);

        final Map<String, AssignedRole> storedRoles = new HashMap<String, AssignedRole>();
        final Map<String, AssignedRole> deletedRoles = new HashMap<String, AssignedRole>();

        // For each of the stores roles, if it's not in the current roles list then it's been deleted
        for (final AssignedRole storedRole : storedAssignedRoles)
        {
            String roleScopeKey = storedRole.getRole().getRoleId() + ":" + storedRole.getScope();
            if (!currentRoles.containsKey(roleScopeKey) || storedRoles.containsKey(roleScopeKey))
            {
                deletedRoles.put(roleScopeKey, storedRole);
            }
            storedRoles.put(roleScopeKey, storedRole);
        }

        // For each of the current roles, if it's not in the original roles then it's been added
        for (String roleScopeKey : currentRoles.keySet())
        {
            if (!storedRoles.containsKey(roleScopeKey))
            {
                addedRoles.put(roleScopeKey, currentRoles.get(roleScopeKey));
            }
        }

        try {
            // Add the new roles
            if (!addedRoles.isEmpty())
            {
                roleDao.insertAssignedRoles(person.getUserId(), addedRoles.values(), actorId);
                logger.trace(" *** Added roles - addedRoles: {} for {}", addedRoles, person);
            }

            // Remove the deleted roles
            if (!deletedRoles.isEmpty())
            {
                for (String deletedRoleKey : deletedRoles.keySet())
                {
                    roleDao.deleteAssignedRole(deletedRoles.get(deletedRoleKey).getRecId());
                    logger.trace(" *** Removed roles - deletedRoles: {} for {}", deletedRoles, person);
                }

            }
        }
        catch (DataAccessException e) {
            logger.warn("Failed to update user roles for {} Added roles were: |{}| Deleted roles were |{}|", new Object[] { person, addedRoles, deletedRoles });
            updated = false;
        }

        return updated;
    }


    private Collection<AssignedRole> loadRoles(final int userId)
    {
        logger.debug(" ___ LOADING ROLES FOR USER {}", userId);
        Collection<AssignedRole>assignedRoles = null;

        try
        {
            assignedRoles = this.roleDao.getAssignedRoles(userId);
        }
        catch (DataAccessException e)
        {
            logger.warn("Failed to load roles userId [" + userId + "]",e);
        }

        return assignedRoles;
    }

// NOT USED Since being transferred from MivPersonDaoImpl to PersonMetaDaoImpl
//    /*
//     * This query shows 1 row per Role the user has:
//     *
//     * select a.ID, a.UserID, a.PrincipalID, Login, SchoolID, DepartmentID,
//     * Surname, GivenName, Email, Active, RoleID, PrimaryRole, MivCode from
//     * UserAccount a left join RoleAssignment ra on a.UserID=ra.UserID left join
//     * MivRole mr on ra.RoleID=mr.ID where a.UserID=18099;
//     */
//    private Collection<Appointment> loadAppointments(final int userId)
//    {
//        return appointmentDao.getAppointments(userId);
//    }


    /**
     * updateAppointments - Update the appointments for the input MivPerson
     * @param person
     * @return boolean true = successful, otherwise false
     */
    private boolean updateAppointments(MivPerson person)
    {
        boolean updated = true;
        Collection<Appointment> appointments = person.getAppointments();
        if (!appointments.isEmpty())
        {
            int uid = person.getUserId();
            for (final Appointment a : appointments) {
                a.setUserId(uid);
            }
            appointmentDao.persistAppointments(appointments);
        }
        return updated;
    }
}
