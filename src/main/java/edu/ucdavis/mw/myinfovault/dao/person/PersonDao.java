/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: PersonDao.java
 */

package edu.ucdavis.mw.myinfovault.dao.person;

import edu.ucdavis.mw.myinfovault.service.person.MivPerson;

/**
 * TODO: Add Javadoc
 * @author Stephen Paulsen
 * @since MIV 4.6
 */
public interface PersonDao
{
    public boolean insertPerson(MivPerson person, int actorId);
    public boolean updatePerson(MivPerson person, int actorId);
}
