/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: LocationDao.java
 */
package edu.ucdavis.mw.myinfovault.dao.location;

import java.util.List;
import java.util.Map;

/**
 * Interface for Location module
 * @author pradeeph
 * @since MIV v4.4
 */
public interface LocationDao
{
    /**
     * To get the list of all province belongs to given country
     * @param countryCode Its a 3 digit code i.e. AUS for Australia or CAN for Canada etc.
     * @return provinces for the given country
     */
    public List<Map<String,String>> getProvincesByCountryCode(String countryCode);

    /**
     * To get the list of all cities belongs to given country
     * @param countryCode Its a 3 digit code i.e. AUS for Australia or CAN for Canada etc.
     * @return cities for the given country
     */
    public List<Map<String,String>> getCitiesByCountryCode(String countryCode);

    /**
     * To get the list of cities for given country and province
     * @param province
     * @param countryCode Its a 3 digit code i.e. AUS for Australia or CAN for Canada etc.
     * @return cities for the given province
     */
    public List<Map<String,String>> getCitiesByProvinceAndCountry(String province, String countryCode);

    /**
     * To Validate correct combination of country, province and city
     * @param countryCode
     * @param Province
     * @param City
     * @return if city exists
     */
    public boolean validateLocation(String countryCode, String Province, String City);

    /**
     * To suggest provinces for country and given province
     * @param countryCode
     * @param province
     * @return provinces for the given country corresponding to the given province term
     */
    public List<Map<String, String>> suggestProvinces(String countryCode, String province);

    /**
     * To suggest cities for country, province and given city
     * @param countryCode
     * @param province
     * @param city
     * @return provinces for the given province corresponding to the given city term
     */
    public List<Map<String, String>> suggestCities(String countryCode, String province, String city);

    /**
     * To get valid State name by given name and country code
     * @param stateName
     * @param countryCode
     * @return valid state name for the given country and state name term
     */
    public String getValidStateByNameAndCountryCode(String stateName, String countryCode);

    /**
     * To get valid city name by given name and country code
     * @param cityName
     * @param countryCode
     * @return valid city name for the given country and city name term
     */
    public String getValidCityByNameAndCountryCode(String cityName, String countryCode);

}
