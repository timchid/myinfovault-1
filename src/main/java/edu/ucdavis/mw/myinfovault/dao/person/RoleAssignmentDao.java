/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: AppointmentDao.java
 */


package edu.ucdavis.mw.myinfovault.dao.person;

import java.util.Collection;

import org.springframework.dao.DataAccessException;

import edu.ucdavis.mw.myinfovault.service.person.AssignedRole;

/**
 * Persistence handler for MivPerson or Group assigned roles. Assigned roles as applied to a user define
 * their access level. Assigned roles as applied to Groups define the access level that a user must have
 * in order to access the group.
 *
 * @author Rick Hendricks
 * @since MIV 4.0
 */
public interface RoleAssignmentDao
{
    /**
     * getAssignedRole - get the assigned role associated with the input record id.
     * @param recId
     * @return AssignedRole
     * @throws DataAccessException
     */
    public AssignedRole getAssignedRole(int recId) throws DataAccessException;

    /**
     * getAssignedRoles - get the assigned roles associated with the input foreign key id.
     * @param foreignId Foreign key id for the MivPerson or Group ID associated with the assigned roles.
     * @return Collection of AssignedRoles
     * @throws DataAccessException
     */
    public Collection<AssignedRole> getAssignedRoles(int foreignId) throws DataAccessException;

    /**
     * insertAssignedRoles - insert the input assigned roles associated for the input foreign key id.
     * @param foreignId Foreign key id for the MivPerson or Group ID associated with the assigned roles.
     * @param assignedRoles Collection of AssignedRole
     * @param actingUserId The MIV user ID of the acting user performing the insert
     * @throws DataAccessException
     */
    public void insertAssignedRoles(int foreignId, Collection<AssignedRole> assignedRoles, int actingUserId) throws DataAccessException;

    /**
     * deleteAssignedRole - delete the assigned role associated with the input record id.
     * @param recId
     * @throws DataAccessException
     */
    public void deleteAssignedRole(int recId) throws DataAccessException;

    /**
     * deleteAssignedRole - delete the assigned role associated with the input foreign key id.
     * @param foreignId Foreign key id for the MivPerson or Group ID associated with the assigned roles.
     * @throws DataAccessException
     */
    public void deleteAssignedRoles(int foreignId) throws DataAccessException;
}
