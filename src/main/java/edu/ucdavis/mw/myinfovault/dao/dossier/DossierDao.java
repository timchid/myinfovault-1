/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: DossierDao.java
 */

package edu.ucdavis.mw.myinfovault.dao.dossier;

import java.io.File;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.dao.DataAccessException;

import edu.ucdavis.mw.myinfovault.domain.action.DossierActionType;
import edu.ucdavis.mw.myinfovault.domain.action.DossierDelegationAuthority;
import edu.ucdavis.mw.myinfovault.domain.action.DossierLocation;
import edu.ucdavis.mw.myinfovault.domain.dossier.Dossier;
import edu.ucdavis.mw.myinfovault.service.dossier.DossierAppointmentDto;
import edu.ucdavis.mw.myinfovault.service.dossier.DossierAppointmentType;
import edu.ucdavis.mw.myinfovault.service.dossier.DossierDocumentDto;
import edu.ucdavis.mw.myinfovault.service.dossier.DossierReviewerDto;
import edu.ucdavis.mw.myinfovault.service.dossier.SchoolDepartmentCriteria;
import edu.ucdavis.mw.myinfovault.service.dossier.UploadDocumentDto;
import edu.ucdavis.mw.myinfovault.service.dossier.WorkflowException;
import edu.ucdavis.mw.myinfovault.service.person.MivPerson;
import edu.ucdavis.mw.myinfovault.service.person.MivRole;
import edu.ucdavis.mw.myinfovault.service.snapshot.SnapshotDto;
import edu.ucdavis.mw.myinfovault.web.spring.search.SearchCriteria;

/**
 * Data access object interface Methods used to access dossier objects in workflow.
 *
 * @author Rick Hendricks
 * @since MIV 3.0
 */
public interface DossierDao
{
    /**
     * Get and update the next available dossier id
     * @return Long dossierId
     * @throws SQLException
     */
    public Long updateNextDossierId()
        throws SQLException;

    /**
     * Find a specific dossier workflow document for the input dossier id
     * and load data
     * @param dossierId
     * @return Dossier object
     * @throws WorkflowException
     * @throws SQLException
     */
    public Dossier getDossierAndLoadAllData(long dossierId)
        throws WorkflowException, SQLException;

    /**
     * Find a specific dossier workflow document for the input dossier id
     * and load base data
     * @param dossierId
     * @return Dossier object
     * @throws WorkflowException
     */
    public Dossier getDossier(long dossierId)
        throws WorkflowException;

    /**
     * Find all dossier workflow documents at a specific workflow location
     * @param location - DossierLocation in workflow .
     * @return List of Dossier id's
     * @throws WorkflowException
     * @throws SQLException
     */
    public List<Long> getDossiersByWorkflowLocation(DossierLocation location)
        throws WorkflowException, SQLException;

    /**
     * Find all dossiers at the Archive or finalized node
     * @return List of Dossier id's
     * @throws SQLException
     */
    public List<Long> getArchivedDossiers()
        throws SQLException;

    /**
     * Find all dossiers at the Archive or finalized node by school and department
     * @param schoolId
     * @param departmentId
     * @return List of Dossier id's
     * @throws SQLException
     */
    public List<Long> getArchivedDossiersBySchoolAndDepartment(int schoolId, int departmentId)
        throws SQLException;

    /**
     * Get dossiers which have been completed and are awaiting archival.
     *
     * @return List of Dossier id's.
     * @throws SQLException
     */
    public List<Long> getCompletedDossiers()
        throws SQLException;

    /**
     * Get dossiers which have snapshots.
     *
     * @return List of Dossier id's.
     * @throws SQLException
     */
    public List<Long> getDossiersForSnapshots()
        throws SQLException;

    /**
     * Loads Dossier attribute data from miv tables.
     * The attributes are sorted and placed in a DossierAttributeMap
     * which is sorted sequence order as defined in the DossierAttributeLocation table
     * @param dossier
     * @return Dossier object
     * @throws WorkflowException
     * @throws SQLException
     */
    public Dossier loadDossierAttributes(Dossier dossier)
        throws WorkflowException, SQLException;

    /**
     * Loads Dossier attribute data from miv tables for a specified dossier's location.
     * The attributes are sorted and placed in a DossierAttributeMap
     * which is sorted sequence order as defined in the DossierAttributeLocation table
     * @param dossier
     * @param location
     * @return Dossier object
     * @throws WorkflowException
     * @throws SQLException
     */
    public Dossier loadDossierAttributesForLocation(Dossier dossier, DossierLocation location)
        throws WorkflowException, SQLException;

    /**
     * Deletes Dossier attribute data as well as any joint appointment records from miv tables.
     * @param dossier
     * @return Dossier object
     * @throws WorkflowException
     * @throws SQLException
     */
    public Dossier deleteDossierAttributes(Dossier dossier)
        throws WorkflowException, SQLException;

    /**
     * Updates a Dossier and all associated base ant attribute data
     * @param dossier
     * @throws WorkflowException
     * @throws SQLException
     */
    public void updateDossier(Dossier dossier)
        throws WorkflowException, SQLException;

    /**
     * Deletes a Dossier and all associated records
     * @param dossier
     * @throws WorkflowException
     * @throws SQLException
     */
    public void deleteDossier(Dossier dossier)
        throws WorkflowException, SQLException;

    /**
     * Find active dossiers for person
     * @param mivPerson person for which to qualify the dossiers.
     * @return List of Dossier objects
     * @throws WorkflowException
     * @throws SQLException
     */
    public List<Dossier> getActiveDossiers(MivPerson person)
        throws WorkflowException, SQLException;

    /**
     * Find all dossiers for person
     * @param mivPerson person for which to qualify the dossiers.
     * @return List of Dossier objects
     * @throws WorkflowException
     * @throws SQLException
     */
    public List<Dossier> getAllDossiers(MivPerson person) throws DataAccessException;

    /**
     * Find finalized dossiers for person
     * @param mivPerson person for which to qualify the dossiers.
     * @return finalized dossiers for the given person
     * @throws WorkflowException
     * @throws SQLException
     */
    public List<Dossier> getFinalizedDossiers(MivPerson person)
        throws WorkflowException, SQLException;

    /**
     * Find all dossiers for in the school and department specified
     * @param schoolId Set to zero (0) for all
     * @param departmentId Set to zero (0) for all
     * @return A list of Dossier id's
     * @throws WorkflowException
     * @throws SQLException
     */
    public List<Long> getDossiersBySchoolAndDepartment(int schoolId, int departmentId)
        throws WorkflowException, SQLException;

    /**
     * Find all dossiers for in the school, department and location specified
     * @param schoolId Set to zero (0) for all
     * @param departmentId Set to zero (0) for all
     * @param location The workflow location
     * @return A list of Dossier id's
     * @throws WorkflowException
     * @throws SQLException
     */
    public List<Long> getDossiersBySchoolDepartmentAndLocation(int schoolId, int departmentId, DossierLocation location)
        throws WorkflowException, SQLException;

    /**
     * Get all dossier documents the input role is allowed to view.
     * @param mivRole
     * @return SnapshotDto object
     */
    public List<DossierDocumentDto> getDocumentListByRole(MivRole mivRole);

    /**
     * Get all upload documents for this dossier.
     * @param dossier dossier object
     * @return a map of UploadDocumentDto Lists by document id
     */
    public Map<String, List<UploadDocumentDto>> getDocumentUploadFiles(Dossier dossier);

    /**
     * Get all upload documents for this dossier qualified by school and departmnent.
     * @param dossier dossier object
     * @param school
     * @param department
     * @return a map of UploadDocumentDto Lists by document id
     */
    public Map<String, List<UploadDocumentDto>> getDocumentUploadFilesBySchoolAndDepartment(Dossier dossier, int school, int department);

    /**
     * Delete an upload file.
     * @param dossier
     * @param uploadDocumentDto
     * @return if record and file successfully deleted
     */
    public boolean deleteDocumentUpload(Dossier dossier, UploadDocumentDto uploadDocumentDto);

    /**
     * updateSnapshot - Update a DossierSnapshot record in the DossierSnapshot table.
     * @param dossier
     * @param role
     * @param outputFile
     * @param searchCriteria contains the school and department Id's of the dossier appointment - May be null of none.
     * @return SnapshotDto
     * @throws WorkflowException
     */
    public SnapshotDto updateSnapshot(Dossier dossier, MivRole role, File outputFile, SchoolDepartmentCriteria searchCriteria)
        throws WorkflowException;

    /**
     * insertUploadDocument - insert a new UserUploadDocument record in the UserUploadDocument table.
     * @param uploadDocumentDto
     * @throws WorkflowException
     */
    public void insertUserUploadDocument(UploadDocumentDto uploadDocumentDto)
        throws WorkflowException;

    /**
     * updateUploadDocument - Update a UserUploadDocument record in the UserUploadDocument table.
     * @param uploadDocumentDto
     * @throws WorkflowException
     */
    public void updateUserUploadDocument(UploadDocumentDto uploadDocumentDto)
        throws WorkflowException;

    /**
     * updateDossierAndUserUploadDocument - Update a UserUploadDocument record in the UserUploadDocument table
     * and the dossier which references the uploaded document. This method simply calls the standalone updateUserUploadDocument
     * and update methods for the purpose of keeping them in the same transaction.
     * @param dossier
     * @param uploadDocumentDto
     * @throws WorkflowException
     * @throws SQLException
     */
    public void updateDossierAndUserUploadDocument(Dossier dossier, UploadDocumentDto uploadDocumentDto)
        throws WorkflowException, SQLException;

    /**
     * Retrieve a dossier's snapshot based on the role, schoolId and departmentId
     * @param dossier
     * @param role
     * @param schoolId
     * @param departmentId
     * @return SnapshotDto
     * @throws WorkflowException
     */
    public SnapshotDto getSnapshot(Dossier dossier, MivRole role, int schoolId, int departmentId)
        throws WorkflowException;

    /**
     * Retrieve all snapshots for a dossier
     * @param dossier
     * @return List of SnapshotDto's
     * @throws WorkflowException
     */
    public List <SnapshotDto> getAllSnapshots(Dossier dossier)
        throws WorkflowException;

    /**
     * Delete snapshots for a dossier. All dossier snapshots may be deleted, or just the
     * snapshots for a specified location may be deleted.
     * location, or all snaphot may
     * @param dossier dossier for which ssnapshots are to me deleted.
     * @param location dossier workflow location for which to delete snapshots.
     * If the DossierLocation is set to null, all snapshots for the dossier will be deleted.
     * @throws WorkflowException
     * @throws SQLException
     */
    public void deleteSnapshots(Dossier dossier, DossierLocation location)
        throws WorkflowException, SQLException;

    /**
     * Gets the users responsible for reviewing this dossier by school and department.
     * @param dossierId
     * @param schoolId
     * @param departmentId
     * @param reviewLocation
     * @return List of DossierReviewerDto objects.
     */
    public List<DossierReviewerDto> getDossierReviewersBySchoolAndDepartment(long dossierId, int schoolId, int departmentId, String reviewLocation);

    /**
     * Gets dossiers to review for this user.
     * @param userId
     * @return List of DossierReviewerDto objects.
     */
    public List<DossierReviewerDto> getDossiersToReview(int userId);

    /**
     * Delete a set of dossier reviewers.
     *
     * @param dossierReviewers Set of dossier reviewer DTOs to remove
     */
    public void deleteDossierReviewers(Set<DossierReviewerDto> dossierReviewers);

    /**
     * Insert a set of dossier reviewers.
     *
     * @param dossierReviewers Set of dossier reviewer DTOs to insert
     */
    public void insertDossierReviewers(Set<DossierReviewerDto> dossierReviewers);

    /**
     * Delete dossierReviewers for a dossier.
     * @param dossierId - dossierId for which reviewers are to be removed.
     * @param schoolId - schoolId of the dossier appointment for which reviewers are to be removed.
     * @param departmentId - departmentId of the dossier appointment for which reviewers are to be removed.
     */
    public void  deleteDossierReviewers(long dossierId, int schoolId, int departmentId);

    /**
     * Get all appointments for this dossier
     * @param dossierId
     * @return dossier appointments
     * @throws WorkflowException
     * @throws SQLException
     */
    public List<DossierAppointmentDto> getAllAppointments(long dossierId)
        throws WorkflowException, SQLException;

    /**
     * Get all appointments for this dossier
     * @param dossier object
     * @return dossier appointments
     * @throws WorkflowException
     * @throws SQLException
     */
    public List<DossierAppointmentDto> getAllAppointments(Dossier dossier)
        throws WorkflowException, SQLException;

   /**
     * Get joint appointments for this dossier
     * @param dossierId
     * @return dossier appointments
     * @throws SQLException
     */
    public List<DossierAppointmentDto> getJointAppointments(long dossierId)
        throws SQLException;

    /**
     * Get joint appointments for this dossier
     * @param dossier object
     * @return dossier appointments
     * @throws SQLException
     */
    public List<DossierAppointmentDto> getJointAppointments(Dossier dossier)
        throws SQLException;

    /**
     * getAllDossierIds - Retrieves all dossier ids in the system
     * @return a list of dossierIds
     * @throws SQLException
     */
    public List<Long> getAllDossierIds()
        throws SQLException;

    /**
     * getAllActiveDossierIds - Retrieves all active dossier ids in the system
     * @return List - a list of active dossierIds
     * @throws SQLException
     */
    public List<Long> getAllActiveDossierIds()
        throws SQLException;

    /**
     * findDossiersForUserNameSearchCriteria - search for dossiers by name search criteria
     * @param criteria - Search criteria name for which to qualify the dossiers
     * @return List of dossier ids
     * @throws SQLException
     */
    public List<Long> findDossiersByUserNameSearchCriteria(final SearchCriteria criteria)
        throws SQLException;

    /**
     * findDossiersForUserNameSearchCriteriaAndLocation - search for dossiers by name search criteria and specified location
     * @param criteria - Search criteria name for which to qualify the dossiers
     * @param location - DossierLocation for which to qualify the dossiers
     * @return List of dossier ids
     * @throws SQLException
     */
    public List<Long> findDossiersByUserNameSearchCriteriaAndLocation(final SearchCriteria criteria, DossierLocation location)
        throws SQLException;
    /**
     * getDossiersByAttributeCriteria - search for dossiers with specific dossierAttributes as well as
     *       by a specified location, department, and school
     * @param schoolId - schoolId for which to qualify the dossiers
     * @param departmentId - departmentId for which to qualify the dossiers
     * @param matchLocation - DossierLocation for which to qualify the dossiers
     * @param attributeCriteria - Search criteria dossierAttributes by which to qualify dossiers
     * @return List of dossier ids
     * @throws SQLException
     */
    public List<Long> getDossiersByAttributeCriteria(int schoolId, int departmentId, DossierLocation matchLocation,
                                                     Map<String, String> attributeCriteria)
        throws SQLException;

    /**
     * Get the attribute names applicable for the given criteria.
     *
     * @param dossierLocation dossier workflow location
     * @param appointmentType appointment type, primary or joint
     * @param delegationAuthority delegation of authority
     * @param actionType dossier action type
     * @return applicable attribute names
     * @throws DataAccessException if query fails
     */
    public List<String> getAttributeNames(DossierLocation dossierLocation,
                                          DossierAppointmentType appointmentType,
                                          DossierDelegationAuthority delegationAuthority,
                                          DossierActionType actionType) throws DataAccessException;

}

