/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: WorkflowDao.java
 */

package edu.ucdavis.mw.myinfovault.dao.workflow;

import java.util.Set;

import org.springframework.dao.DataAccessException;

import edu.ucdavis.mw.myinfovault.service.dossier.DossierRoutingDefinitionDto;

/**
 * Action routing path workflow DAO.
 *
 * @author Rick Hendricks
 * @since MIV 4.6.1
 */
public interface WorkflowDao
{
    /**
     * Get action routing node definitions.
     *
     * @return action routing node definitions
     * @throws DataAccessException if unable to execute the query
     */
    public Set<DossierRoutingDefinitionDto> getDossierRoutingDefinitions() throws DataAccessException;
}
