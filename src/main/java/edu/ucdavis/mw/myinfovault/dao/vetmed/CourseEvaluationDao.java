/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: CourseEvaluationDao.java
 */

package edu.ucdavis.mw.myinfovault.dao.vetmed;

import java.util.Collection;

import org.springframework.dao.DataAccessException;

import edu.ucdavis.mw.myinfovault.domain.vetmed.CourseEvaluation;

/**
 * Handles persisting VetMed course evaluation records to the database.
 *
 * @author Craig Gilmore
 * @since MIV 4.7.2
 */
public interface CourseEvaluationDao
{
    /**
     * Persist VetMed course evaluations.
     *
     * @param realUserId ID of the real, logged in user performing the persist
     * @param evaluations course evaluations to persist
     * @throws DataAccessException if persistence fails
     */
    public void persistEvaluations(int realUserId, Collection<CourseEvaluation> evaluations) throws DataAccessException;
}
