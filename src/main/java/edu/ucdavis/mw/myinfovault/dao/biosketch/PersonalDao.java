/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: PersonalDao.java
 */

package edu.ucdavis.mw.myinfovault.dao.biosketch;

import edu.ucdavis.mw.myinfovault.domain.biosketch.Personal;

/**
 * TODO: missing javadoc
 * 
 * @author dreddy
 * @since MIV 2.1
 */
public interface PersonalDao
{
    /**
     * 
     * @param userId
     * @return
     */
    public Personal findById(int userId);
    
    /**
     * 
     * @param userId
     * @param typeId
     * @return
     */
    public String findAddressByTypeId(int userId, int typeId);
    
    /**
     * 
     * @param userId
     * @param typeId
     * @return
     */
    public String findPhoneByTypeId(int userId, int typeId);
    
    /**
     * 
     * @param userId
     * @return
     */
    public String findEmailByUserId(int userId);
    
    /**
     * 
     * @param userId
     * @return
     */
    public String findUrlByUserId(int userId);
}
