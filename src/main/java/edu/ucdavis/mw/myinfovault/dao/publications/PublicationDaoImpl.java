/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: PublicationDaoImpl.java
 */

package edu.ucdavis.mw.myinfovault.dao.publications;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;

import edu.ucdavis.mw.myinfovault.dao.DaoSupport;
import edu.ucdavis.mw.myinfovault.domain.publications.Publication;
import edu.ucdavis.mw.myinfovault.domain.publications.PublicationImpl;

/**
 * Handles publication persistence.
 *
 * @author Craig Gilmore
 * @since MIV 3.9.1
 */
public class PublicationDaoImpl extends DaoSupport implements PublicationDao
{
    private final String INSERT_PUBLICATIONS_SQL;
    private final String SELECT_PUBLICATIONS_SQL;
    private final String SELECT_PUBLICATION_SEQUENCE_SQL;

    /**
     * Creates the Publication DAO bean and initializes it with SQL
     * for handling the persistence of {@link Publication} objects.
     *
     * @throws IOException
     */
    public PublicationDaoImpl() throws IOException
    {
        INSERT_PUBLICATIONS_SQL = getSQL("publicationsummary.insert");
        SELECT_PUBLICATIONS_SQL = getSQL("publicationsummary.select");
        SELECT_PUBLICATION_SEQUENCE_SQL = getSQL("publicationsummary.select.sequence");
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.publications.PublicationDao#persistPublications(java.util.Collection, int)
     */
    @Override
    public int persistPublications(Collection<Publication> publications, final int realUserId) throws DataAccessException
    {
        int inserted = 0;

        for (final Iterator<Publication> it = publications.iterator(); it.hasNext();)
        {
            inserted += getJdbcTemplate().update(new PreparedStatementCreator() {
                @Override
                public PreparedStatement createPreparedStatement(Connection connection) throws SQLException
                {
                    Publication publication = it.next();

                    PreparedStatement ps = connection.prepareStatement(INSERT_PUBLICATIONS_SQL);

                    ps.setInt(1, publication.getUserId());
                    ps.setInt(2, publication.getType());
                    ps.setInt(3, publication.getStatus());
                    ps.setInt(4, publication.getSource().getId());
                    ps.setString(5, publication.getExternalId());
                    ps.setString(6, publication.getYear());
                    ps.setString(7, publication.getAuthors());
                    ps.setString(8, publication.getTitle());
                    ps.setString(9, publication.getEditor());
                    ps.setString(10, publication.getJournal());
                    ps.setString(11, publication.getVolume());
                    ps.setString(12, publication.getIssue());
                    ps.setString(13, publication.getPages());
                    ps.setString(14, publication.getPublisher());
                    ps.setString(15, publication.getCity());
                    ps.setString(16, publication.getIsbn());
                    ps.setString(17, publication.getLink());
                    ps.setInt(18, realUserId);
                    ps.setInt(19, realUserId);
                    ps.setInt(20, getSequenceNext(publication.getUserId()));

                    return ps;
                }
            });
        }

        return inserted;
    }

    /**
     * Get the number next in the sequence of publications for the given user.
     *
     * @param userId ID of the MIV user
     * @return next sequence number
     * @throws DataAccessException if unable to execute the query
     */
    private int getSequenceNext(final int userId) throws DataAccessException
    {
        return (getJdbcTemplate().queryForObject(SELECT_PUBLICATION_SEQUENCE_SQL, new Object[]{userId}, Integer.class)) == null ? 1 : +10;
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.publications.PublicationDao#getPublications(int)
     */
    @Override
    public List<Publication> getPublications(int userId) throws DataAccessException
    {
        List<Publication> publications = get(SELECT_PUBLICATIONS_SQL, publicationMapper, userId);
        //remove all null references
        publications.removeAll(Collections.singletonList(null));

        return publications;
    }

    /**
     * Creates and maps the 'PublicationSummary' table records to {@link Publication} objects.
     */
    private RowMapper<Publication> publicationMapper = new RowMapper<Publication>()
    {
        @Override
        public Publication mapRow(ResultSet rs, int rowNum) throws SQLException
        {
            try
            {
                return new PublicationImpl(rs.getInt("ID"),
                                           rs.getInt("Sequence"),
                                           rs.getInt("UserID"),
                                           rs.getString("StatusID"),
                                           rs.getString("TypeID"),
                                           rs.getString("ExternalSourceID"),
                                           rs.getString("ExternalID"),
                                           rs.getString("Year"),
                                           rs.getString("Author"),
                                           rs.getString("Title"),
                                           rs.getString("Editor"),
                                           rs.getString("Journal"),
                                           rs.getString("Volume"),
                                           rs.getString("Issue"),
                                           rs.getString("Pages"),
                                           rs.getString("Publisher"),
                                           rs.getString("City"),
                                           rs.getString("ISBN"),
                                           rs.getString("Link"));
            }
            catch (IllegalArgumentException e)
            {
                log.warn("Could not build publication object for record with ID '" + rs.getInt("ID") + "'", e);
            }

            return null;
        }
    };
}
