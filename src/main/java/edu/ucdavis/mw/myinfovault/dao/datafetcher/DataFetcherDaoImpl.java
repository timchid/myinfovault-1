package edu.ucdavis.mw.myinfovault.dao.datafetcher;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

import edu.ucdavis.mw.myinfovault.domain.packet.Packet;
import edu.ucdavis.mw.myinfovault.domain.packet.PacketContent;
import edu.ucdavis.mw.myinfovault.service.SearchFilterAdapter;
import edu.ucdavis.myinfovault.MIVConfig;
import edu.ucdavis.myinfovault.data.DataMap;
import edu.ucdavis.myinfovault.data.Fetcher;

/**
 * TODO: add javadoc
 *
 * @author Rick Hendricks
 * @since MIV 4.6
 */
public class DataFetcherDaoImpl extends JdbcDaoSupport implements DataFetcherDao
{

    private final Logger log = Logger.getLogger(this.getClass());
    final Map<String, Map<String, String>> documentByAttributeNameMap = MIVConfig.getConfig().getMap("documentsbyattributename");
    final Map<String, String> creativeActivitiesDocAttributes = documentByAttributeNameMap.get("creativeactivities");
    final Map<String, String> worksContributionsDocAttributes = documentByAttributeNameMap.get("workscontributions");
    final Map<String, Map<String, String>> nameBySectionIdMap = MIVConfig.getConfig().getMap("namebysectionid");

     /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.datafetcher.DataFetcherDao#getWholeNameMap(int)
     */
    @Override
    public DataMap getWholeNameMap(int userID)
    {
        final String nameQuery = "SELECT GivenName, Surname FROM UserAccount WHERE UserID = ?";

        DataMap wholeName = null;
        try
        {
            Object[] parms = { userID };
            Map<String,Object> nameMap = this.getJdbcTemplate().queryForMap(nameQuery, parms);
            if (!nameMap.isEmpty())
            {
                wholeName = new DataMap();
                for (String key : nameMap.keySet())
                {
                    wholeName.put(key,nameMap.get(key));
                }

            }
        }
        catch (DataAccessException dae)
        {
            log.error("SQL Error while trying to retrieve user name for user " + userID, dae);
        }

        return wholeName;
    }

    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.datafetcher.DataFetcherDao#getDepartment(int)
     */
    @Override
    public String getDepartment(int userID)
    {
        final String departmentQuery =
            // MIV-3394 - Update select to include the School description as well as the Department description
            "SELECT S.Description as SchoolDescription,D.Description as DepartmentDescription "+
             "FROM (UserAccount A LEFT JOIN SystemDepartment D  ON A.SchoolID = D.SchoolID AND A.DepartmentID = D.DepartmentID) "+
             "LEFT JOIN SystemSchool S on S.SchoolId = D.SchoolId "+
             "WHERE UserID = ?";

        Object department = null;

        try
        {
            Object[] parms = { userID };
            Map<String,Object> resultMap = this.getJdbcTemplate().queryForMap(departmentQuery, parms);
            if (!resultMap.isEmpty())
            {
                // MIV-3394 - If there is no Department Description, use the School Description in it's place
                department = resultMap.get("DepartmentDescription");
                if (department == null || department.toString().length() == 0) {
                    department = resultMap.get("SchoolDescription");
                }
            }
        }
        catch (DataAccessException dae)
        {
            log.error("SQL Error while trying to get department name for user " + userID, dae);
        }

        return department != null ? department.toString() : null;
    }

    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.datafetcher.DataFetcherDao#getHeaderMap(int)
     */
    @Override
    public DataMap getHeaderMap(int userID)
    {
        final String systemHeaderQuery =
            "SELECT Section.Name, Header" +
            " FROM Section, SystemSectionHeader" +
            " WHERE Section.ID = SystemSectionHeader.SectionID";

        final String userHeaderQuery =
            "SELECT Section.Name, Header" +
            " FROM Section, UserSectionHeader" +
            " WHERE UserID = ?" +
            " AND Section.ID = UserSectionHeader.SectionID";

        DataMap headerMap = new DataMap();

        // Get the system standard headers
        try
        {
            int headerCount = 0;
            List<Map<String,Object>> resultList = this.getJdbcTemplate().queryForList(systemHeaderQuery);
            if (!resultList.isEmpty())
            {
                for (Map<String, Object>resultMap : resultList)
                {
                    headerCount++;
                    for (String key : resultMap.keySet())
                    {
                        headerMap.put(key, resultMap.get(key));
                    }
                }
            }
            if (headerCount == 0) // No system headers were found, something's terribly wrong
            {
                log.error("DataFetcherDao: NO SYSTEM HEADERS FOUND!");
            }
        }
        catch (DataAccessException dae)
        {
            log.error("SQL Error while trying to retrieve system headers", dae);
        }

        // Now replace system headers in the map with any user overridden headers
        try
        {

            Object[] parms = { userID };
            List<Map<String,Object>> resultList = this.getJdbcTemplate().queryForList(userHeaderQuery, parms);
            if (!resultList.isEmpty())
            {
                for (Map<String, Object>resultMap : resultList)
                {
                    for (String key : resultMap.keySet())
                    {
                        headerMap.put(key, resultMap.get(key));
                    }
                }
            }
        }
        catch (DataAccessException dae)
        {
            log.error("SQL Error while trying to retrieve user headers for user " + userID, dae);
        }

        return headerMap;
    }

    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.datafetcher.DataFetcherDao#getSectionsByDocumentID(int)
     */
    @Override
    public List<Map<String,Object>> getSectionsByDocumentID(int documentID)
    {

        final String sectionQueryByDocumentID =
            "SELECT Section.ID as SectionID,Name, RecordName, SectionBaseTable, SelectFields, FromTable, WhereClause, OrderBy, OldDset" +
            " FROM DocumentSection, Section" +
            " WHERE DocumentSection.SectionID = Section.ID AND DocumentID = ?" +
            " order by DocumentSection.sequence"
            ;

        Object[] parms = { documentID };
        return getSections(sectionQueryByDocumentID, parms);
    }

    /**
     * Query to retrieve the sections
     * @param query - The query string
     * @param parms - Query parameters
     * @return List of Maps representing the records retrieved
     */
    private List<Map<String,Object>>getSections(String query, Object[] parms)
    {
        List<Map<String,Object>> sectionListMap = null;

        try
        {
            sectionListMap =  this.getJdbcTemplate().queryForList(query, parms);
        }
        catch (DataAccessException dae)
        {
            log.warn("Exception caused by this SQL statement: [" + query+ "]", dae);
        }
        return sectionListMap;

    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.datafetcher.DataFetcherDao#getSectionData(int, java.util.Map, edu.ucdavis.mw.myinfovault.domain.packet.Packet)
     */
    @Override
    public List<Map<String, Object>>getSectionData(int userID, Map<String,Object>sectionRecordMap, Packet packet)
    {
        String query = Fetcher.buildQuery(
                           (String)sectionRecordMap.get("SelectFields"), (String)sectionRecordMap.get("FromTable"),
                           (String)sectionRecordMap.get("WhereClause"), (String)sectionRecordMap.get("OrderBy"));

//        int sectionId = ((String)sectionRecordMap.get("FromTable")).equals("AdditionalHeader") ? 0 : (Integer)sectionRecordMap.get("SectionID");

        // Do not filter additional header records
        PacketContentFilter<Map<String, Object>> filter = ((String)sectionRecordMap.get("FromTable")).equals("AdditionalHeader") ? null : getPacketContentFilter(packet, (Integer)sectionRecordMap.get("SectionID"));

        List<Map<String,Object>>sectionDataMapList = new ArrayList<>();

        try
        {
            List<Object> parms = new ArrayList<>();
            parms.add(userID);

            sectionDataMapList = filter != null ? filter.apply(this.getJdbcTemplate().queryForList(query,parms.toArray())) : this.getJdbcTemplate().queryForList(query,parms.toArray());
        }
        catch (DataAccessException dae)
        {
            log.warn("Exception caused by this SQL statement: [" + query + "]", dae);
        }
        return sectionDataMapList;

    }

    @Override
    public List<Map<String, Object>> getSectionBySectionId(Integer sectionId, Integer userId)
    {
        List<Map<String, Object>> results;

        //sectionId 0 is being used to denote AdditionalInformation, which does not technically have a section id.
        if (sectionId == 0)
        {
            String query = "SELECT ID, HeaderID, Content, Display FROM AdditionalInformation WHERE UserID = ? ORDER BY HeaderID, Sequence";

            results = this.getJdbcTemplate().queryForList(query, userId);
        }
        else
        {
            Map<String, Object> sectionQuery =
                this.getJdbcTemplate().queryForMap("SELECT ID, Name, SelectFields, FromTable, WhereClause, OrderBy FROM Section WHERE ID=?",
                                                   sectionId.toString()); // should be a list of one.

            String query = Fetcher.buildQuery((String) sectionQuery.get("SelectFields"),
                                              (String) sectionQuery.get("FromTable"),
                                              (String) sectionQuery.get("WhereClause"),
                                              (String) sectionQuery.get("OrderBy"));

            results = this.getJdbcTemplate().queryForList(query, userId);
        }

        return results;
    }


    /**
     * Get a PacketContentFilter for this packet and sectionId
     * @param packet
     * @param sectionId
     * @return
     */
    private PacketContentFilter<Map<String, Object>> getPacketContentFilter(Packet packet, Integer sectionId)
    {
        // Create filter to only include matching packetContentKey
        return  new PacketContentFilter<Map<String, Object>>(packet.getPacketId(), sectionId, packet.getPacketItemsMap())
                {
                    /* (non-Javadoc)
                     * @see edu.ucdavis.mw.myinfovault.dao.datafetcher.DataFetcherDaoImpl.PacketContentFilter#getKey(java.lang.Object)
                     */
                    @Override
                    protected String getKey(Map<String, Object> item)
                    {
                        return PacketContent.PacketContentKey.build(packetId, (Integer)item.get("ID"), sectionId);
                    }

                    /* (non-Javadoc)
                     * @see edu.ucdavis.mw.myinfovault.dao.datafetcher.DataFetcherDaoImpl.PacketContentFilter#setDisplay(java.lang.Object, boolean)
                     */
                    @Override
                    protected void setDisplay(Map<String, Object> item, boolean display)
                    {
                        if (item.containsKey("Display"))
                        {
                            item.put("Display", display);
                        }
                    }

                    /* (non-Javadoc)
                     * @see edu.ucdavis.mw.myinfovault.dao.datafetcher.DataFetcherDaoImpl.PacketContentFilter#setDisplayContribution(java.lang.Object, boolean)
                     */
                    @Override
                    protected void setDisplayContribution(Map<String, Object> item, boolean display)
                    {

                        if (item.containsKey("DisplayContribution"))
                        {
                            if (StringUtils.isBlank((String) item.get("Contribution"))  && StringUtils.isBlank((String) item.get("Significance")))
                            {
                                item.put("DisplayContribution", false);
                            }
                            else
                            {
                                item.put("DisplayContribution", display);
                            }
                        }
                    }

                    /* (non-Javadoc)
                     * @see edu.ucdavis.mw.myinfovault.dao.datafetcher.DataFetcherDaoImpl.PacketContentFilter#setDisplayDescription(java.lang.Object, boolean)
                     */
                    @Override
                    protected void setDisplayDescription(Map<String, Object> item, boolean display)
                    {
                        if (item.containsKey("DisplayDescription") && StringUtils.isBlank((String) item.get("Description")))
                        {
                            item.put("DisplayDescription", false);
                        }
                        else
                        {
                            item.put("DisplayDescription", display);
                        }
                    }

                    /* (non-Javadoc)
                     * @see edu.ucdavis.mw.myinfovault.dao.datafetcher.DataFetcherDaoImpl.PacketContentFilter#getDisplay(java.lang.Object)
                     */
                    @Override
                    protected boolean getDisplay(Map<String, Object> item)
                    {
                        return (item.get("Display") != null && (boolean) item.get("Display"))
                                || (item.get("DisplayContribution") != null && (boolean) item.get("DisplayContribution"))
                                || (item.get("DisplayDescription") != null && (boolean) item.get("DisplayDescription"));
                    }
                };

    }


    /**
     * Filter the items based on the packetContentMap
     * @author rhendric
     *
     * @param <T> PacketContentMap
     */
    public abstract class PacketContentFilter<T> extends SearchFilterAdapter<T>
    {

        int sectionId;
        long packetId;
        Map<String, PacketContent>packetContentMap = null;

        /**
         * Creates a filter for the sections in the map.
         *
         * @param sections to filter
         */
        public PacketContentFilter(long packetId, int sectionId, Map<String, PacketContent>packetContentMap)
        {
            this.packetContentMap = packetContentMap;
            this.packetId = packetId;
            this.sectionId = sectionId;
        }

        /*
         * (non-Javadoc)
         * @see edu.ucdavis.mw.myinfovault.service.SearchFilter#include(java.lang.Object)
         */
        @Override
        public boolean include(T item)
        {
            // Include all items for packet id 0 (master)
            if (packetId == 0)
            {
                return true;
            }
            // Filter items with a packet id > 0
            if (packetContentMap.containsKey(getKey(item)))
            {
                // If this item is in the packetContentMap, set the display flags as indicated in the packetContent item
                PacketContent packetContent = packetContentMap.get(getKey(item));

                // displayParts can contain a string of up to three comma delimited text values. The presence of the
                // values indicates that the corresponding record should be displayed in the PDF. The three values
                // are as shown below:
                //
                // "display" : display the parent record / additional information record
                // "displaycontribution" : display the contribution field of the parent record
                // "displaydescription " : diplay the description or goal of a parent Grant record
                String displayParts = packetContent.getDisplayParts();

                // Skip if there are no displayParts defined
                if (displayParts != null)
                {
                    String[]displayValues = displayParts.split(",");

                    Map<String, Boolean>dispPartsMap = new HashMap<>();
                    for (int i=0 ; i < displayValues.length; i++)
                    {
                        dispPartsMap.put(displayValues[i],true);
                    }

                    setDisplay(item, dispPartsMap.containsKey("display"));
                    setDisplayContribution(item, dispPartsMap.containsKey("displaycontribution"));
                    setDisplayDescription(item, dispPartsMap.containsKey("displaydescription"));
                }
                return getDisplay(item);
            }
            return false;
        }

        /**
         * Get the key from an item to filter.
         *
         * @param item An item within the collection to filter
         * @return the key for the given item
         */
        protected abstract String getKey(T item);

        /**
         * Set the Display flag for a filtered item.
         *
         * @param item An item within the collection
         * @param boolean - true to display the item
         */
        protected abstract void setDisplay(T item, boolean display);


        /**
         * Set the Contributions Display flag for a filtered item.
         *
         * @param item An item within the collection
         * @param boolean - true to display the item
         */
        protected abstract void setDisplayContribution(T item, boolean display);

        /**
         * Set the Description Display flag for a filtered item.
         *
         * @param item An item within the collection
         * @param boolean - true to display the item
         */
        protected abstract void setDisplayDescription(T item, boolean display);

        /**
         * See if any parts of the record are set to display.
         *
         * @param item An item within the collection
         * @param boolean - true to display the item
         */
        protected abstract boolean getDisplay(T item);

    }

    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.datafetcher.DataFetcherDao#getAssociatedSectionsByName(java.lang.String)
     */
    @Override
    public List<Map<String,Object>> getAssociatedSectionsByName(String name)
    {
        final String associatedSectionQuery =
            "SELECT ID SectionID,Name, RecordName, SelectFields, FromTable, WhereClause, OrderBy, OldDset" +
            " FROM Section" +
            " WHERE Name = ?"
            ;

        List<Map<String,Object>> sectionListMap = null;
        try
        {
            Object[] parms = { name };
            sectionListMap =  this.getJdbcTemplate().queryForList(associatedSectionQuery, parms);
        }
        catch (DataAccessException dae)
        {
            log.warn("Exception caused by this SQL statement: [" + associatedSectionQuery + "]", dae);
        }
        return sectionListMap;

    }

    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.datafetcher.DataFetcherDao#getAdditionalInformation(int, int)
     */
    @Override
    public List<Map<String,Object>> getAdditionalInformation(int userID, int headerID, int sectionId,  Packet packet)
    {
        final String query = "SELECT ID, Content, Display FROM AdditionalInformation WHERE UserID = ? AND HeaderID = ? ORDER BY Sequence";

        PacketContentFilter<Map<String, Object>> filter = this.getPacketContentFilter(packet, sectionId);

        try
        {
            Object[] params = { userID, headerID };
            return filter.apply(this.getJdbcTemplate().queryForList(query, params));
        }
        catch (DataAccessException dae)
        {
            return null;
        }
    }

    /* (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.datafetcher.DataFetcherDao#getPacketUploadFiles(int, int)
     */
    @Override
    public List<Map<String,Object>> getPacketUploadFiles(int userID, int documentID, Packet packet)
    {

        // Determine the section for this upload document.
        int sectionId = -1;
        List<Map<String,Object>>sections = this.getSectionsByDocumentID(documentID);
        for (Map<String, Object>section : sections)
        {
            if(section.get("FromTable").equals("UserUploadDocument"))
            {
                sectionId = (int) section.get("SectionID");
                break;
            }
        }

        // descending for documentID == 1, otherwise ascending
        // Only get documents where the upload location is Unknown. See MIV-5292
         String query = new StringBuilder("SELECT ID, Display, UploadFile, UploadFirst")
                   .append(" FROM UserUploadDocument")
                   .append(" WHERE UserID = ?")
                   .append(" AND DocumentID = ?")
                   .append(" AND UploadLocation = 'Unknown'")
                   .append(" ORDER BY Sequence ")
                   .append(documentID == 1 ? "DESC" : "ASC").toString();

         PacketContentFilter<Map<String, Object>> filter = this.getPacketContentFilter(packet, sectionId);

         try
         {
             Object[] params = { userID, documentID };
             return filter.apply(this.getJdbcTemplate().queryForList(query, params));
         }
         catch (DataAccessException dae)
         {
             return null;
         }
    }

}
