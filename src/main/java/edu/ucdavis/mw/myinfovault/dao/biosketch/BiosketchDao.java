/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: BiosketchDao.java
 */

package edu.ucdavis.mw.myinfovault.dao.biosketch;

import java.sql.SQLException;
import java.util.List;

import edu.ucdavis.mw.myinfovault.domain.biosketch.Biosketch;
import edu.ucdavis.mw.myinfovault.domain.biosketch.BiosketchAttributes;

/**
 * TODO: missing javadoc
 * 
 * @author dreddy
 * @since MIV 2.1
 */
public interface BiosketchDao
{
    /**
     * Returns the list of biosketches of a particular type associated with the user.
     * 
     * @param userID
     * @param biosketchType
     * @return biosketches for the user and type
     * @throws SQLException
     */
    public List<Biosketch> findBiosketches(int userID, int biosketchType) throws SQLException;

    /**
     * Returns the biosketch depending on the biosketch ID.
     * 
     * @param biosketchID
     * @return biosketch for the given ID
     * @throws SQLException
     */
    public Biosketch findBiosketch(int biosketchID) throws SQLException;

    /**
     * Returns the biosketch attribute list of a biosketch.
     * 
     * @param biosketchID
     * @return biosketch attributes for the given biosketch
     * @throws SQLException
     */
    public List<BiosketchAttributes> findBiosketchAttributes(int biosketchID) throws SQLException;

    /**
     * Returns the default biosketch attribute list.
     * 
     * @param biosketchType
     * @return default biosketch attributes for the given biosketch type
     * @throws SQLException
     */
    public List<BiosketchAttributes> findDefaultBiosketchAttributes(int biosketchType) throws SQLException;

    /**
     * Inserts the biosketch.
     * 
     * @param biosketch
     * @return auto-generated record ID of the inserted biosketch
     * @throws SQLException
     */
    public int insertBiosketch(Biosketch biosketch) throws SQLException;

    /**
     * Updates the biosketch record.
     * 
     * @param biosketch
     * @throws SQLException
     */
    public void updateBiosketch(Biosketch biosketch) throws SQLException;

    /**
     * Deletes the biosketch record.
     * 
     * @param biosketchID
     * @throws SQLException
     */
    public void deleteBiosketch(int biosketchID) throws SQLException;

    /**
     * Inserts the biosketch attributes.
     * 
     * @param attributes
     * @param userID
     * @throws SQLException
     */
    public void insertBiosketchAttributes(BiosketchAttributes attributes, int userID) throws SQLException;

    /**
     * Updates the biosketch attributes record.
     * 
     * @param attributes
     * @param userID
     * @throws SQLException
     */
    public void updateBiosketchAttributes(BiosketchAttributes attributes, int userID) throws SQLException;

    /**
     * Deletes all the biosketch attributes records associated with a biosketch.
     * 
     * @param biosketchID
     * @throws SQLException
     */
    public void deleteBiosketchAttributes(int biosketchID) throws SQLException;
    
    /**
     * Returns all the names(duplicate) of same Biosketch for a single user.
     * 
     * @param userID
     * @param originalName
     * @return the names(duplicate) of same Biosketch for a single user
     * @throws SQLException
     */
    public List<String> getBiosketchNamesForUser(int userID,String originalName) throws SQLException;
}
