/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: WorkflowDaoImpl.java
 */

package edu.ucdavis.mw.myinfovault.dao.workflow;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedHashSet;
import java.util.Set;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.RowMapper;

import edu.ucdavis.mw.myinfovault.dao.DaoSupport;
import edu.ucdavis.mw.myinfovault.domain.action.DossierActionType;
import edu.ucdavis.mw.myinfovault.domain.action.DossierDelegationAuthority;
import edu.ucdavis.mw.myinfovault.domain.action.DossierLocation;
import edu.ucdavis.mw.myinfovault.service.dossier.DossierRoutingDefinitionDto;
import edu.ucdavis.mw.myinfovault.service.workflow.WorkflowNodeType;

/**
 * Action routing path workflow DAO.
 *
 * @author Rick Hendricks
 * @since MIV 4.6.1
 */
public class WorkflowDaoImpl extends DaoSupport implements WorkflowDao
{
    private final String DOSSIER_ROUTING_DEFINITIONS = "dossier.routingdefinitions";

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.myinfovault.dao.workflow.WorkflowDao#getDossierRoutingDefinitions()
     */
    @Override
    public Set<DossierRoutingDefinitionDto> getDossierRoutingDefinitions() throws DataAccessException
    {
        return new LinkedHashSet<DossierRoutingDefinitionDto>(get(getSQL(DOSSIER_ROUTING_DEFINITIONS), routingDefinitionsRowMapper));
    }

    /**
     * Creates and maps 'RoutingPathDefinitions' table records to {@link DossierRoutingDefinitionDto} objects.
     */
    private RowMapper<DossierRoutingDefinitionDto> routingDefinitionsRowMapper = new RowMapper<DossierRoutingDefinitionDto>() {
        @Override
        public DossierRoutingDefinitionDto mapRow(ResultSet rs, int rowNum) throws SQLException
        {
            return new DossierRoutingDefinitionDto(DossierLocation.mapWorkflowNodeNameToLocation(rs.getString("CurrentWorkflowLocation")),
                                                   DossierLocation.mapWorkflowNodeNameToLocation(rs.getString("PreviousWorkflowLocation")),
                                                   DossierLocation.mapWorkflowNodeNameToLocation(rs.getString("NextWorkflowLocation")),
                                                   DossierDelegationAuthority.valueOf(rs.getString("DelegationAuthority")),
                                                   DossierActionType.valueOf(rs.getString("ActionType")),
                                                   WorkflowNodeType.valueOf(rs.getString("WorkflowNodeType")),
                                                   rs.getString("LocationPrerequisiteAttributes"),
                                                   rs.getBoolean("DisplayOnlyWhenAvailable"));
        }
    };
}
