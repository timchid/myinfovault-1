/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: AcademicActionDao.java
 */

package edu.ucdavis.mw.myinfovault.dao.aa;

import java.util.List;

import edu.ucdavis.mw.myinfovault.domain.action.AppointmentDetails;
import edu.ucdavis.mw.myinfovault.domain.action.Assignment;
import edu.ucdavis.mw.myinfovault.domain.action.StatusType;
import edu.ucdavis.mw.myinfovault.domain.raf.AcademicActionBo;

/**
 * Data access object for the academic action.
 *
 * @author Rick Hendricks
 * @since MIV 4.8
 */
public interface AcademicActionDao
{
    /**
     * Get the appointment details for the given academic action.
     *
     * @param academicActionID record ID of the academic action
     * @return appointment details or <code>null</code> if none exist
     */
    public AppointmentDetails getAppointmentDetails(int academicActionID);

    /**
     * Get the {@link StatusType#PRESENT} or {@link StatusType#PROPOSED} assignments for the given academic action.
     *
     * @param academicActionID record ID of the academic action
     * @param status status type of the assignments
     * @return academic action assignments
     */
    public List<Assignment> getActionAssignments(int academicActionID, StatusType status);

    /**
     * Persists AA to database.
     *
     * @param raf recommended action form
     * @param realUserId ID of the real, logged-in user effecting changes to the AA
     * @return <code>true</code> on successful persistence, <code>false</code> if no update occurred
     */
    public boolean persistAa(AcademicActionBo raf, int realUserId);

    /**
     * Removes academic action records from the database.
     *
     * @param academicActionID ID associated with the academic action to delete
     */
    public void delete(int academicActionID);
}
