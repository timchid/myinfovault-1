/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: ExceptionFilter.java
 */

package edu.ucdavis.mw.servlet;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import edu.ucdavis.myinfovault.data.MIVUtil;
import edu.ucdavis.myinfovault.exception.MivIdentityLoadException;
import edu.ucdavis.myinfovault.exception.MivSevereApplicationError;


/**
 * <p>This is a "Last-Chance" exception handler filter for servlets. It chains all
 * requests and catches any exceptions that manage to propagate all the way out
 * of the web application, logs them, and redirects to a configurable error page
 * if one was given in the filter initialization parameters.
 * </p>
 * <p>You may want to put this as the last filter in the chain, to prevent exceptions
 * from reaching any other filters, or as the first filter to give the other filters
 * the chance to handle specific exceptions.
 * </p>
 * <p>Improvement: make this thing more configurable, such as being able to specify
 * an "ExceptionHandler" class that will be instantiated and called, rather than
 * the log-and-redirect currently built in.  Log+Redirect would probably still
 * be the default behavior if no handler class were configured.
 * This would probably look something like this in the web.xml file:
 * </p><pre>
 * &lt;init-param&gt;
 *     &lt;param-name&gt;ExceptionHandler&lt;/param-name&gt;
 *     &lt;param-value&gt;edu.ucdavis.mw.myproject.MyProjectsExceptionHandler&lt;/param-value&gt;
 * &lt;/init-param&gt;
 * </pre>
 * @author Stephen Paulsen
 * @since MIV 2.3
 */
public class ExceptionFilter implements Filter
{
    private String exceptionURL;
    private String errorURL;
    private String logPath;
    private String logFile;

    protected Logger logger = Logger.getLogger(this.getClass().getName());


    /* *
     *
     * /
    public ExceptionFilter()
    {
        // No action required here.
    }
    */


    /**
     * @see javax.servlet.Filter#init(javax.servlet.FilterConfig)
     */
    @Override
    public void init(FilterConfig filterConfig) throws ServletException
    {
        exceptionURL = filterConfig.getInitParameter("ExceptionURL");
        if (exceptionURL != null && exceptionURL.length() < 1) {
            exceptionURL = null; // treat empty string as null
        }

        errorURL = filterConfig.getInitParameter("ErrorURL");
        if (errorURL != null && errorURL.length() < 1) {
            errorURL = null; // treat empty string as null
        }

        logPath = filterConfig.getInitParameter("LogPath");
        logFile = filterConfig.getInitParameter("LogFile");

        // Set up logging
        if (logPath != null && logFile != null)
        {
            try
            {
                Handler h = null;
                h = new FileHandler(new File(logPath, logFile).toString(), /* append= */true);
                h.setFormatter(new SimpleFormatter());
                logger.addHandler(h);
                logger.setUseParentHandlers(false);
            }
            catch (SecurityException e)
            {
                // Ignore this -- use the standard Handler if ours failed.
                e.printStackTrace(System.err);
            }
            catch (IOException e)
            {
                // Ignore this -- use the standard Handler if ours failed.
                e.printStackTrace(System.err);
            }
        }
    }


    /**
     * Filters requests that are about to be sent to the web application.
     * In this case, all requests are simply forwarded to the next filter in
     * the filter chain, but within a <code>try {}</code> block. All exceptions
     * are caught and handled.
     *
     * @see javax.servlet.Filter#doFilter(javax.servlet.ServletRequest, javax.servlet.ServletResponse, javax.servlet.FilterChain)
     */
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
        throws IOException, ServletException
    {
        try {
            chain.doFilter(request, response);
        }
        catch (ServletException se) {
            Throwable t = se.getRootCause();
            if (t == null) t = se;
            handleException((HttpServletRequest)request, (HttpServletResponse)response, t);
        }
        catch (Exception e) {
            handleException((HttpServletRequest)request, (HttpServletResponse)response, e);
        }
    }


    /**
     * Handles ServletExceptions, which are special because the have a <code>getRootCause()</code> method.
     * Looks for, in particular, MivSevereApplicationErrors to redirect to the error message page.
     * All others are passed along to the generic exception handler.
     *
     * @param req standard HTTP request object
     * @param res standard HTTP response object
     * @param e the Throwable exception or error being handled
     * @throws IOException passed on from the specific exception handlers
     */
    protected void handleException(HttpServletRequest req, HttpServletResponse res, Throwable e) throws IOException
    {
        Throwable cause = getRootCause(e);

        if (MivIdentityLoadException.class.isAssignableFrom(cause.getClass()))
        {
            // Is there anything else we want to do when a person is missing from Identity Management?
            showError(req, res, (MivSevereApplicationError) cause);
        }
        else if (MivSevereApplicationError.class.isAssignableFrom(cause.getClass()))
        {
            showError(req, res, (MivSevereApplicationError) cause);
        }
        else
        {
            handleUnknownException(req, res, e);
        }
    }


    /**
     * Handles all exceptions that aren't otherwise singled out for special handling.
     * The main goal is to avoid the servlet container generic error page, and substitute
     * a friendlier "MIV look-and-feel" page.
     *
     * @param req standard HTTP request object
     * @param res standard HTTP response object
     * @param e the Throwable exception or error being handled
     * @throws IOException if redirection to the error page fails
     */
    protected void handleUnknownException(HttpServletRequest req, HttpServletResponse res, Throwable e) throws IOException
    {
        // Log it, redirect, whatever.  This is the "Last Chance" exception handler.
        String userName = null;

        HttpSession s = req.getSession(false);
        if (s != null)
        {
            userName = (String) s.getAttribute("loginName");
            if (userName != null && userName.length() < 1) userName = null;
        }

        logger.log(Level.WARNING, "Last Chance error handler caught the following exception for user \"" + userName + "\"", e);

        if (exceptionURL != null)
        {
            if (!res.isCommitted())
            {
                res.sendRedirect(exceptionURL);
            }
        }
    }


    /**
     * Dispays the custom error page configured by the ErrorURL parameter in web.xml
     *
     * @param req standard HTTP request object
     * @param res standard HTTP response object
     * @param e the Throwable exception or error being handled
     * @throws IOException if redirection to the error page fails
     */
    protected void showError(HttpServletRequest req, HttpServletResponse res, MivSevereApplicationError e) throws IOException
    {
        String userName = null;
        String errorPageURL = errorURL != null ? errorURL : "/jsp/errorpage.jsp";

        HttpSession s = req.getSession(false);
        if (s != null)
        {
            userName = (String) s.getAttribute("loginName");
            if (userName != null && userName.length() < 1) userName = null;
        }

        logger.log(Level.WARNING, "Unexpected error handler is redirecting to the error page due to the following exception for user \"" + userName + "\"", e);

        if (exceptionURL != null)
        {
            if (!res.isCommitted())
            {
                String message = e.getDisplayMessage();
                if (message == null) message = "(No additional message was provided)";

                message = MIVUtil.escapeMarkup(message);

                try {
                    req.setAttribute("message", message);
                    RequestDispatcher dispatcher = req.getRequestDispatcher(errorPageURL);
                    dispatcher.forward(req, res);
                }
                catch (ServletException se) {
                    // Forwarding failed -- redirect instead. The message becomes a URL parameter so has to be url-encoded.
                    try {
                        message = URLEncoder.encode(message, "ISO-8859-1");
                        res.sendRedirect("/miv" + errorPageURL + "?message=" + message); // ick. url param is only way I've found to get the message there.
                    }
                    catch (UnsupportedEncodingException uee) {
                        logger.log(Level.SEVERE, "An \"Impossible\" UnsupportedEncodingException was thrown for ISO-8859-1", uee);
                    }
                }
            }
        }
    }


    /**
     * Crawl through the "caused by" chain of exceptions to find the root cause.
     * Stops looking when either (a) an MivSevereApplicationError is found or
     * (b) the bottom of the chain has been reached.
     *
     * @param t a Throwable (Error or Exception) to crawl for the root cause
     * @return the first MivSevereApplicationError found, or the bottom of the cause chain.
     */
    private Throwable getRootCause(Throwable t)
    {
        if (MivSevereApplicationError.class.isAssignableFrom(t.getClass()))
            return t;

        Throwable cause = t.getCause();
        if (cause != null) {
            return getRootCause(cause);
        }
        return t;
    }


    /* (non-Javadoc)
     * @see javax.servlet.Filter#destroy()
     */
    @Override
    public void destroy()
    {
        // No action required here.
    }
}
