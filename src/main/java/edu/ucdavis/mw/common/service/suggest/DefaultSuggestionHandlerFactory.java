/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: DefaultSuggestionHandlerFactory.java
 */


package edu.ucdavis.mw.common.service.suggest;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.Collections;
import java.util.InvalidPropertiesFormatException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Properties;

/**<p>
 * The Default implementation of a Suggestion Handler Factory.</p>
 * <p>
 * Configurable when instantiated via Properties by passing a
 * {@link java.util.Properties Properties} object,
 * a {@link java.io.File File} for a Properties file,
 * the path/name of a Properties files,
 * or using the default Properties file named "suggestions.properties"
 * which will be found on the resource path.</p>
 * <p>
 * This factory will find a configured {@link SuggestionHandler}
 * based on the <em>topic</em> passed. It will treat the topic
 * as a <em>path</em>, generally supporting the concept of
 * <em>category/topic</em> paths, which can be wildcarded.
 * </p>
 * <p>
 * Configuration is done by specifying a <em>topic</em> handler,
 * which can be listed as <kbd>category/topic</kbd> to take
 * advantage of the wildcarding ability.
 * The handler specified is the <em>name</em> of the handler,
 * which must be followed by providing a Class that implements
 * the handler. Handler specific options may be configured
 * by using the handler name, though this factory will pass
 * all properties on to the handler if it can accept them.
 * </p>
 * <p>
 * These should appear similar to this</p>
 * <pre>
 * suggest.handler.topic.&lt;topic&gt; = &lt;name&gt;
 * suggest.&lt;name&gt;.class = &lt;name of class&gt;
 * suggest.&lt;name&gt;.option.&lt;something&gt; = &lt;anything you want&gt;
 * suggest.&lt;name&gt;.option.&lt;another&gt; = &lt;another thing you want&gt;</pre>
 * <p>
 * For example, one might specify a suggestion handler for the name
 * of a professional journal in this way:</p>
 * <pre>
 * suggest.handler.topic.publication/journal=journalnamehandler
 * suggest.journalnamehandler.class=edu.ucdavis.mw.common.service.suggest.PublicationSuggestionHandler
 * suggest.journalnamehandler.option.table=JOURNAL_NAME_T
 * suggest.journalnamehandler.option.searchcol=JOURNAL_TITLE
 * suggest.journalnamehandler.option.fetchcols=JOURNAL_TITLE AS label, JOURNAL_ID AS ID, JOURNAL_ABBREV AS abbrev</pre>
 *
 * @author Stephen Paulsen
 *
 */
public class DefaultSuggestionHandlerFactory implements SuggestionHandlerFactory<Map<String,String>>
{
    private static final String DEFAULT_CONFIGURATION_PROPERTIES = "/suggestions.properties";
    /** The Prefix used for all suggestion properties. */
    private static final String PREFIX = "suggest.";
    private static final String TOPIC_HANDLER = "handler.topic.";

    private Properties configProps = null;
    private ConfigurationSource configSource = ConfigurationSource.DEFAULT;

    private SuggestionHandler<Map<String,String>> defaultHandler = null;


    /**
     * Create a SuggestionHandlerFactory which is initialized by reading its default Properties file, "suggestions.properties"
     */
    public DefaultSuggestionHandlerFactory() //throws IOException
    {
        loadConfigFromDefault();
    }


    /**
     * Create a SuggestionHandlerFactory which is initialized by reading the named Properties file.
     * The file named must include any relevant path necessary to find the file.
     *
     * @param configurationPropertiesFileName path and file name of a Properties file
     */
    public DefaultSuggestionHandlerFactory(String configurationPropertiesFileName)
    {
        //this(new File(configurationPropertiesFileName));
        loadConfigFromFilename(configurationPropertiesFileName);
    }


    /**
     * Create a SuggestionHandlerFactory which is initialized by reading the given
     * {@link java.io.File File}, which must be a Properties file.
     *
     * @param configurationPropertiesFile a java.io.File that represents a Properties file.
     */
    public DefaultSuggestionHandlerFactory(File configurationPropertiesFile)
    {
        loadConfigFromFile(configurationPropertiesFile);
    }


    /**
     * Create a SuggestionHandlerFactory which is initialized by using the provided Properties.
     *
     * @param configurationProperties
     */
    public DefaultSuggestionHandlerFactory(Properties configurationProperties)
    {
        loadConfigFromProperties(configurationProperties);
    }


    /* (non-Javadoc)
     * @see edu.ucdavis.mw.common.service.suggest.SuggestionHandlerFactory#getHandler(java.lang.String)
     */
    @Override
    public SuggestionHandler<Map<String,String>> getHandler(String topic)
    {
        SuggestionHandler<Map<String,String>> handler = null;
        String foundTopic = topic;
        String handlerName = configProps.getProperty(PREFIX + TOPIC_HANDLER + topic);



        // No handler found, start checking wildcards
        if (handlerName == null)
        {
            // First (?) check for the specific topic under any category - that is, "*/topic"
            foundTopic = topic.replaceAll(".+/([^/]+)", "*/$1");
            handlerName = configProps.getProperty(PREFIX + TOPIC_HANDLER + foundTopic);
        }

        // Still no handler, prune the path
        if (handlerName == null)
        {
            // Then start backing up the levels of the path until
            //   1. A handler is found, or
            //   2. The path is empty - there is no handler configured for the topic
            // Backing up is done by replacing the last path component with a '*'
            //    kingdom/phylum/class/order/family/genus/species --->
            //    kingdom/phylum/class/order/family/genus/*       --->
            //    kingdom/phylum/class/order/family/*             ---> etc.

            String wildTopic = topic;
            while (handlerName == null && wildTopic != null && wildTopic.length() > 0)
            {
                handlerName = configProps.getProperty(PREFIX + TOPIC_HANDLER + wildTopic);
                foundTopic = wildTopic;
                wildTopic = applyWildcard(wildTopic);
            }
        }

        // Found a named handler for the topic, get the handler
        if (handlerName != null)
        {
            // Get a handler instance - will be null if none was configured
            handler = getHandlerForName(handlerName, foundTopic, this.configProps);
        }

        // Finally, if no hander was found but a default handler was configured, use that
        if (handler == null)
        {
            handler = this.defaultHandler;
        }

        return handler;
    }
    // getHandler


    private void loadConfigFromDefault()
    {
        try
        {
            InputStream in = this.getClass().getResourceAsStream(DEFAULT_CONFIGURATION_PROPERTIES);
            loadConfiguration(in);
            in.close();
            this.configSource = ConfigurationSource.DEFAULT;
        }
        catch (IOException e) {
            // FIXME: do something sensible here
            // Report an error, output a stack trace, ¿throw a config exception?
        }
    }

    private void loadConfigFromFilename(String filename)
    {
// Checking where this is actually looking for the file
//      File f = new File(configurationPropertiesFileName);
//      System.out.println(" absoluteFile : " + f.getAbsoluteFile());
//      System.out.println(" absolutePath : " + f.getAbsolutePath());
//      try
//      {
//          System.out.println("canonicalFile : " + f.getCanonicalFile());
//          System.out.println("canonicalPath : " + f.getCanonicalPath());
//      }
//      catch (IOException e)
//      {
//          e.printStackTrace();
//      }

        loadConfigFromFile(new File(filename));

        this.configSource = ConfigurationSource.FILENAME;
        this.configSource.setFilename(filename);
    }


    /**
     * will try to treat the File as an XML Properties file
     * if that fails, will retry as a regular .properties file
     * @param file
     */
    private void loadConfigFromFile(File file)
    {
        File f = file;
        InputStream in;
        boolean tryXML = true;
        for (;;)
        {
            try
            {
                in = new FileInputStream(f);
                loadConfiguration(in, tryXML);
                in.close();
                break;
            }
            catch (FileNotFoundException e)
            {
                // TODO Auto-generated catch block
                // FIXME: Do something sensible here!

                // This is fatal - there is no such file.
                // Error, stacktrace, and get out. Rethrow the exception? Wrapped?
                e.printStackTrace();
                break;
            }
            catch (InvalidPropertiesFormatException e)
            {
                // Not a valid XML properties file.
                // Try again as a regular properties file.
                tryXML = false;
                continue;
            }
            catch (IOException e)
            {
                // TODO Auto-generated catch block
                // FIXME: Do something sensible here!

                // loadConfiguration threw an Exception
                e.printStackTrace();
                break;
            }
        }

        this.configSource = ConfigurationSource.FILE;
        this.configSource.setFile(file);
    }


    /**
     * @param properties
     */
    private void loadConfigFromProperties(Properties properties)
    {
        processConfig(properties);

        this.configSource = ConfigurationSource.PROPERTIES;
        this.configSource.setProperties(properties);
    }


    /**
     *
     * @param in
     * @throws IOException
     */
    private void loadConfiguration(InputStream in) throws IOException
    {
        loadConfiguration(in, false);
    }

    private void loadConfiguration(InputStream in, boolean asXML) throws InvalidPropertiesFormatException, IOException
    {
        Properties p = new Properties();
        if (asXML)
        {
            p.loadFromXML(in);
        }
        else
        {
            p.load(in);
        }
        this.configProps = p;
        processConfig(p);
// This attempt to accept either XML properties or simple key=value properties fails,
// because reading from the InputStream in loadFromXML leaves the stream in a state
// where the subsequent .load() gets corrupted.
// Maybe it could be hoisted up into the caller.
//        Properties p = new Properties();
//        try {
//            if (in.markSupported()) in.mark(256);
//            p.loadFromXML(in);
//        }
//        catch (InvalidPropertiesFormatException e) {
//            in.reset();
//            p.load(in);
//        }
//
//        config = p;
//        processConfig(p);

//        Properties p = new Properties();
//        p.load(in);
//
//        config = p;
//        processConfig(p);
    }
    // loadConfiguration


    private void processConfig(Properties p)
    {
        // debugging - dump all the properties
        System.out.println("Processing configuration properties. Properties found are:");
        for (Object o : p.entrySet())
        {
            System.out.println("  " + o.toString());
        }

        String propname = PREFIX + "handler.default";
        String defaultClassName = p.getProperty(propname);//PREFIX + "handler.default");

        if (defaultClassName != null) {
            System.out.println("\nCreating an instance of " + defaultClassName + " ...");
            this.defaultHandler = createHandlerInstance(defaultClassName, "default", "*", p);
            System.out.println(" ... Done creating instance.");
        }
    }
    // processConfig



    /* Caching for Handler Instances */

    private final int CACHE_SIZE = 20;
    private final int SPARES = 2;
    private final float LOAD_FACTOR = 0.9f;
    private final int INITIAL_SIZE = (Math.round(CACHE_SIZE / LOAD_FACTOR) + 1) + SPARES;

    private Map<String, SuggestionHandler<Map<String,String>>> handlerCache = Collections.synchronizedMap(
        new LinkedHashMap<String, SuggestionHandler<Map<String,String>>>(INITIAL_SIZE, LOAD_FACTOR, true)
        {
            private static final long serialVersionUID = 201202131249L;

            @Override protected boolean removeEldestEntry(Map.Entry<String, SuggestionHandler<java.util.Map<String,String>>> eldest)
            {
                return size() > CACHE_SIZE;
            }
        }
    );
    /* end Caching */


    /**
     *
     * @param name
     * @param config
     * @return
     */
    protected SuggestionHandler<Map<String,String>> getHandlerForName(String name, String topic, Properties config)
    {
        // Get from handlerCache if available, else call createHandlerInstance.

        String key = name + "|" + topic;
        SuggestionHandler<Map<String, String>> handler = handlerCache.get(key);

        if (handler == null)
        {
            String handlerClass = config.getProperty(PREFIX + name + ".class");
            handler = createHandlerInstance(handlerClass, name, topic, config);
            System.out.println("Adding handler for key ["+key+"] to cache");
            handlerCache.put(key, handler);
        }
        else
        {
            System.out.println("Got handler for key ["+key+"] from cache");
        }

        return handler;
    }
    // getHandlerForName


    /**
     * Apply wildcard rules to a search topic.
     * @param topic an initial topic to be wildcarded.
     * @return The original topic with the final portion wildcarded, or an empty topic if no more wildcarding can take place
     */
    private String applyWildcard(CharSequence topic)
    {
        String newTopic = topic.toString();

        // Trim off any existing wildcard at the end
        if (newTopic.endsWith("/*"))
        {
            newTopic = newTopic.substring(0, newTopic.length()-2);
        }

        // Replace the last element of the path with a wildcard
        newTopic = newTopic.replaceAll("(.+)/[^/]+", "$1/*");

        // If the new topic is the same as the original,
        // or the new topic with an added wildcard is the same as the original
        // then we have no more wildcarding to do. Drop the whole topic.
        if (newTopic.equals(topic) || (newTopic + "/*").equals(topic))
        {
            newTopic = "";
        }

        return newTopic;
    }
    // applyWildcard



    /**
     * Try to find, in order of preference
     *<pre>
     *  Handler with a name, for a specific topic, with a configuration
     *    Handler(String name, String topic, Properties p)
     *
     *  Named Handler with a configuration
     *    Handler(String name, Properties p)
     *  //  ----------------------------- OOPS, Method Signature clash - no way to distinguish these two, but see note below.
     *  // We'll skip this one; use Name+Properties then try Name+Topic
     *  Handler for a specific topic, with a configuration
     *    Handler(String topic, Properties p)
     *
     *  Named Handler for a specific topic
     *    Handler(String name, String topic)
     * --------------------------------------- also add "Handler(String name)" here?
     *  A configurable Handler
     *    Handler(Properties p)
     *
     *  A general Handler
     *    Handler()
     *</pre>
     * NOTE!<br>
     * <p>We could use some deep reflection with annotations to distinguish name/properties constructor
     * from topic/properties constructor.  The class actually declaring the constructors still must
     * find a way to declare them without signature clashes.</p>
     *
     * <p>Call <code>Class.getDeclaredConstructors()</code> then inspect the returned <code>Constructor[]</code><br>
     * Use getAnnotation(Class) and/or getDeclaredAnnotations() and/or getParameterAnnotations()</p>
     *
     * @param handlerClassName
     * @param handlerName
     * @param topic
     * @param p
     * @return
     */
    private SuggestionHandler<Map<String, String>> createHandlerInstance(String handlerClassName, String handlerName, String topic, Properties p)
    {
        SuggestionHandler<Map<String,String>> handler = null;
        Class<?> c = null;
        try
        {
            c = Class.forName(handlerClassName);
        }
        catch (ClassNotFoundException e)
        {
            // Configured with a class name that doesn't exist
            // Write an error and return a null handler or
            // throw a "SuggestionFactoryConfigurationException" here?
            System.err.println("Factory Configuration Error: The handler class specified \"" + handlerClassName + "\" was not found");
            e.printStackTrace(System.err);
            return null;
        }

        /*
         * Try all our possible constructors, in order of preference, until
         * a handler has been set or even a no-arg constructor didn't result
         * in getting a handler. null will be returned in that case.
         */
        System.out.println("Trying name, topic, config");
        handler = invokeConstructor(c, handlerName, topic, p);
        if (handler == null)
        {
            System.out.println("Trying name, config");
            handler = invokeConstructor(c, handlerName, p);
        }
        if (handler == null)
        {
            System.out.println("Trying name, topic");
            handler = invokeConstructor(c, handlerName, topic);
        }
        if (handler == null)
        {
            System.out.println("Trying config");
            handler = invokeConstructor(c, p);
        }
        if (handler == null)
        {
            System.out.println("Trying no-arg");
            handler = invokeConstructor(c);
        }

        // Return the handler, which will be null if there were any exceptions along the way
        return handler;
    }
    // createHandlerInstance


    /**
     * Attempt to invoke a Constructor for the Class given with the arguments provided.
     * @param c
     * @param args
     * @return
     */
    @SuppressWarnings("unchecked")
    private SuggestionHandler<Map<String,String>> invokeConstructor(Class<?> c, Object... args)
    {
        SuggestionHandler<Map<String,String>> handler = null;

        Constructor<?> ctor = null;

        if (args != null)
        {
            Class<?>[] cArgs = new Class<?>[args.length];

            int i = 0;
            for (Object o : args)
            {
                cArgs[i++] = o.getClass();
            }

            System.out.println("Looking for Constructors for class " + c.getSimpleName() + " with args ["+Arrays.asList(cArgs)+"]");

            try
            {
                ctor = c.getConstructor(cArgs);
            }
            catch (SecurityException e) { /* Ignore; try next in loop */ }
            catch (NoSuchMethodException e) { /* Ignore; try next in loop */ }

            // We were given some args, but no constructor was found to match those args.
            if (ctor == null)
            {
                System.out.println("  No matching constructor found.");
                return null;
            }
        }

        // A constructor was found, or no constructor args were given to us.
        try
        {
            handler =
                ctor != null ?
                        (SuggestionHandler<Map<String, String>>) ctor.newInstance(args)
                      : (SuggestionHandler<Map<String, String>>) c.newInstance() ;
        }
        catch (InstantiationException e)
        {
            // The configured class couldn't be instantiated - missing a no-arg Constructor?
            System.err.println("A handler of the class \"" + c.getSimpleName() + "\" could not be created");
            e.printStackTrace(System.err);
        }
        catch (IllegalAccessException e)
        {
            // The configured class couldn't be instantiated - security or package-access problems?
            System.err.println("A handler of the class \"" + c.getSimpleName() + "\" is not accessible");
            e.printStackTrace(System.err);
        }
        catch (IllegalArgumentException e)
        {
            // The constructor parameter was illegal somehow - this shouldn't ever happen
            System.err.println("An illegal set of Properties was passed to the constructor of class \"" + c.getSimpleName() +
            "\"\n  This should never happen.");
            e.printStackTrace();
        }
        catch (InvocationTargetException e)
        {
            // Attempting to create an instance - the constructor threw an exception
            System.err.println("The constructor for the handler class \"" + c.getSimpleName() + "\" threw an exception: " + e.getCause());
            e.printStackTrace();
        }

        return handler;
    }


//    /**
//     * Try to find, in order of preference
//     *<pre>
//     *  Handler with a name, for a specific topic, with a configuration
//     *    Handler(String name, String topic, Properties p)
//     *
//     *  Named Handler with a configuration
//     *    Handler(String name, Properties p)
//     *  //  ----------------------------- OOPS, Method Signature clash - no way to distinguish these two, but see note below.
//     *  // We'll skip this one; use Name+Properties then try Name+Topic
//     *  Handler for a specific topic, with a configuration
//     *    Handler(String topic, Properties p)
//     *
//     *  Named Handler for a specific topic
//     *    Handler(String name, String topic)
//     *
//     *  A configurable Handler
//     *    Handler(Properties p)
//     *
//     *  A general Handler
//     *    Handler()
//     *</pre>
//     * NOTE!<br>
//     * <p>We could use some deep reflection with annotations to distinguish name/properties constructor
//     * from topic/properties constructor.  The class actually declaring the constructors still must
//     * find a way to declare them without signature clashes.</p>
//     *
//     * <p>Call <code>Class.getDeclaredConstructors()</code> then inspect the returned <code>Constructor[]</code><br>
//     * Use getAnnotation(Class) and/or getDeclaredAnnotations() and/or getParameterAnnotations()</p>
//     */
//    private Constructor<?> findConstructor(Class<?> c)
//    {
//        System.out.println("Looking for Constructors for class " + c.getSimpleName());
//
//        Constructor<?> ctor = null;
//
//        for (Class<?>[] cArgs : cArgsList)
//        {
//            System.out.println("Checking for args: " + Arrays.asList(cArgs));
//            try {
//                ctor = c.getConstructor(cArgs);
//            }
//            // Ignore these exceptions - if this constructor wasn't found we'll try the next
//            catch (SecurityException e) { }
//            catch (NoSuchMethodException e) { }
//
//            if (ctor != null) {
//                System.out.println("Found a constructor with args: " + Arrays.asList(cArgs));
//                break;
//            }
//        }
//System.out.flush();
//        return ctor;
//    }
//    /** Constructor args used in findConstructor */
//    private static final Class<?>[][] cArgsList = new Class<?>[][] {
//            //  name          topic          config
//            { String.class, String.class, Properties.class },
//            //  name           config
//            { String.class, Properties.class },
//            //  name           topic
//            { String.class, String.class },
//            //  config
//            { Properties.class }
//        };


    /**
     * Clear the cached handlers. Will we need this so MIVConfig can call it when "reload" is pressed?
     */
    @Override
    public void reset()
    {
        handlerCache.clear();
        switch (this.configSource)
        {
            case DEFAULT:
                loadConfigFromDefault();
                break;

            case FILENAME:
                loadConfigFromFilename(this.configSource.getFilename());
                break;

            case FILE:
                loadConfigFromFile(this.configSource.getFile());
                break;

            case PROPERTIES:
                loadConfigFromProperties(this.configSource.getProperties());
                break;
        }
    }


    /** Might we need to keep track of where we got the configuration? In case of possible re-load */
    private enum ConfigurationSource
    {
        DEFAULT,
        FILENAME,
        FILE,
        PROPERTIES;

        private String filename;
        private File file;
        private Properties properties;

        /** @return the filename */
        final String getFilename()
        {
            return filename;
        }

        /** @param filename the filename to set */
        final void setFilename(final String filename)
        {
            this.filename = filename;
        }

        /** @return the file */
        final File getFile()
        {
            return file;
        }

        /** @param file the file to set */
        final void setFile(final File file)
        {
            this.file = file;
        }

        /** @return the properties */
        final Properties getProperties()
        {
            return properties;
        }

        /** @param properties the properties to set */
        final void setProperties(final Properties properties)
        {
            this.properties = properties;
        }
    }
}
