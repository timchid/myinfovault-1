/*
 * Copyright © The Regents of the University of California, Davis campus.
 * All Rights Reserved
 *
 * Unpublished rights reserved under the copyright laws of the United States. The
 * Software contained on this media is proprietary to and embodies the confidential
 * technology of the University of California, Davis.
 * Possession, use, duplication or dissemination of the software and media is
 * authorized only pursuant to a valid written license from University of California, Davis.
 *
 * ---------------------------------------------------------------------------
 * MODULE NAME: AbstractSuggestionHandler.java
 */

package edu.ucdavis.mw.common.service.suggest;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Sets;

/**
 * Suggestion handler with caching and search term tokenization.
 *
 * @author Craig Gilmore
 * @since MIV 4.7.2
 * @param <T> type of returned suggestions
 */
public abstract class AbstractSuggestionHandler<T> implements SuggestionHandler<T>
{
    /*
     * Cache configuration
     */
    private static final int CACHE_SIZE = 20;
    private static final int SPARES = 2;
    private static final float LOAD_FACTOR = 0.9F;
    private static final int INITIAL_SIZE = (Math.round(CACHE_SIZE / LOAD_FACTOR) + 1) + SPARES;

    /**
     * Non-alphabetic regular expression.
     */
    protected static final String NON_ALPHABETIC_RE = "[^\\p{javaLowerCase}\\p{javaUpperCase}]+";

    /**
     * Default minimum search term length.
     */
    private static final int DEFAULT_TERM_LENGTH = 1;

    protected final Logger log = LoggerFactory.getLogger(this.getClass());

    /*
     * Handler options from suggestion.properties
     */
    protected final String handlerName;
    protected final String handlerTopic;
    protected final Properties config;
    protected final MatchType matchType;
    protected final boolean tokenize;
    protected final int minTermLength;

    /**
     * Cache for suggestions yielded by term for this handler instance.
     */
    protected final Map<String, Set<T>> suggestionCache = Collections.synchronizedMap(
        new LinkedHashMap<String, Set<T>>(INITIAL_SIZE, LOAD_FACTOR, true)
        {
            private static final long serialVersionUID = 201311051448L;

            @Override
            protected boolean removeEldestEntry(Map.Entry<String, Set<T>> eldest) {
                return size() > CACHE_SIZE;
            }
        }
    );

    /**
     * Create the suggestion handler for the given name and topic.
     *
     * @param name handler name
     * @param topic handler topic
     * @param p configuration properties
     */
    protected AbstractSuggestionHandler(String name, String topic, Properties p)
    {
        this.handlerName = name;
        this.handlerTopic = topic;
        this.config = p;

        /*
         * Set match type.
         */
        MatchType matchType = MatchType.PREFIX;// default
        try {
            matchType = MatchType.valueOf(getOption("matchtype").toUpperCase());
        }
        catch (NullPointerException e) {
            // Match type was not set. Keep the default.
            log.warn("Match type option not set", e);
        }
        catch (IllegalArgumentException e) {
            // Match type specified was not one of the allowed options. Keep the default.
            log.warn("Invalid match type value", e);
        }
        this.matchType = matchType;

        /*
         * Set search term tokenize option.
         */
        this.tokenize = Boolean.parseBoolean(getOption("tokenize"));

        /*
         * Set minimum term length that triggers a search.
         */
        int minTermLength = DEFAULT_TERM_LENGTH;
        try
        {
            minTermLength = Integer.parseInt(getOption("mintermlength"));
        }
        catch (NumberFormatException e)
        {
            log.warn("Invalid minimum length option. Using default of " + DEFAULT_TERM_LENGTH, e);
        }

        this.minTermLength = minTermLength;
    }

    /**
     * Get the suggestion properties option for this handler.
     *
     * @param key the option key from this handlers "option" properties group
     * @return the option or <code>null</code> if DNE
     */
    protected String getOption(String key)
    {
        return this.config.getProperty("suggest." + this.handlerName + ".option." + key);
    }

    /**
     * Get the suggestion properties option for this handler.
     *
     * @param key the option key from this handlers "option" properties group
     * @param defaultOption defaultOption to return none for given key
     * @return the option or if DNE, the defaultOption
     */
    protected String getOption(String key, String defaultOption)
    {
        String option = getOption(key);

        return option == null ? defaultOption : option;
    }

    /*
     * (non-Javadoc)
     * @see edu.ucdavis.mw.common.service.suggest.SuggestionHandler#getSuggestions(java.lang.String, java.lang.String)
     */
    @Override
    public Set<T> getSuggestions(String topic, String term)
    {
        return getSuggestions(topic, term, tokenize);
    }

    /**
     * Get suggestions for the given topic and search term.
     *
     * @param topic suggestion topic
     * @param term search term
     * @param tokenize if search term is to be tokenized
     * @return suggestions for the given topic and search term
     */
    private Set<T> getSuggestions(String topic, String term, boolean tokenize)
    {
        // ignore search terms that are null or too short
        if (term == null || term.length() < minTermLength) return null;

        String cacheKey = topic + "-" + term;

        // get suggestions from cache
        Set<T> suggestions = suggestionCache.get(cacheKey);

        // if none, get some
        if (suggestions == null)
        {
            // should the term be tokenized?
            if (tokenize)
            {
                /*
                 * Split term in two:
                 *  - Head, an alphabetic token
                 *  - Tail, the rest of the term
                 */
                String[] tokens = term.split(NON_ALPHABETIC_RE, 2);

                // get suggestions from the head token
                suggestions = getSuggestions(topic, tokens[0], false);

                // if the head token yielded valid suggestions
                if (suggestions != null)
                {
                    // recurse if head suggestions aren't empty and there's a tail
                    if (!suggestions.isEmpty() && tokens.length > 1)
                    {
                        // get suggestions from the tail
                        Set<T> tailSuggestions = getSuggestions(topic, tokens[1]);

                        // if the tail yields valid suggestions
                        if (tailSuggestions != null)
                        {
                            // find the intersection of head and tail suggestions
                            suggestions = Sets.intersection(suggestions, tailSuggestions);
                        }
                    }
                }
                // otherwise, get suggestions from the tail if exists
                else if (tokens.length > 1)
                {
                    suggestions = getSuggestions(topic, tokens[1]);
                }
            }
            else
            {
                suggestions = Sets.newHashSet(getNewSuggestions(topic, term));
            }

            log.debug("Caching suggestions for '" + cacheKey + "'");
            log.debug("Suggestion cache: " + suggestionCache.keySet());

            // add suggestions to cache
            suggestionCache.put(cacheKey, suggestions);
        }

        return suggestions;
    }

    /**
     * Get new suggestions for the given topic and search term.
     *
     * @param topic suggestion topic
     * @param term search term
     * @return suggestions for the given topic and search term
     */
    protected abstract Iterable<T> getNewSuggestions(String topic, String term);

    /**
     * Get an immutable map suggestion for the given label and value.
     *
     * <p>e.g. <code>{"label" : "Harry Caray", "value" : "34582"}</code></p>
     *
     * @param label label displayed to user
     * @param value value of the item displayed the user
     * @return label and value keyed suggestion map
     */
    protected static Map<String, String> getSuggestion(String label, String value)
    {
        return ImmutableMap.<String, String>builder()
                           .put("label", label)
                           .put("value", value)
                           .build();
    }
}
