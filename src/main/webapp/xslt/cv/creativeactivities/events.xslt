<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE stylesheet [
 <!ENTITY space " ">
 <!ENTITY comma ",">
 <!ENTITY period ".">
 <!ENTITY semicolon ";">
 <!ENTITY colon ":">
 <!ENTITY quote '"'>
 <!ENTITY qmark "?">
 <!ENTITY expoint "!">
 <!ENTITY hash "#">
 <!ENTITY leftparen "(">
 <!ENTITY rightparen ")">
 ]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  version="1.0" xmlns:fo="http://www.w3.org/1999/XSL/Format">

  <xsl:template match="book-completed|book-scheduled|book-submitted|broadcast-completed|broadcast-scheduled|broadcast-submitted|catalogs-completed|catalogs-scheduled|catalogs-submitted|cd-dvd-video-completed|cd-dvd-video-scheduled|cd-dvd-video-submitted|citation-completed|citation-scheduled|citation-submitted|commission-completed|commission-scheduled|commission-submitted|concert-completed|concert-scheduled|concert-submitted|exhibitions-group-completed|exhibitions-group-scheduled|exhibitions-group-submitted|exhibitions-solo-completed|exhibitions-solo-scheduled|exhibitions-solo-submitted|structure-landscape-completed|structure-landscape-scheduled|structure-landscape-submitted|fashion-show-completed|fashion-show-scheduled|fashion-show-submitted|interview-commentary-completed|interview-commentary-scheduled|interview-commentary-submitted|performance-event-completed|performance-event-scheduled|performance-event-submitted|private-collection-completed|private-collection-scheduled|private-collection-submitted|product-completed|product-scheduled|product-submitted|program-notes-completed|program-notes-scheduled|program-notes-submitted|public-collection-completed|public-collection-scheduled|public-collection-submitted|reading-completed|reading-scheduled|reading-submitted|recordings-completed|recordings-scheduled|recordings-submitted|reproductions-completed|reproductions-scheduled|reproductions-submitted|screening-completed|screening-scheduled|screening-submitted|theatre-production-completed|theatre-production-scheduled|theatre-production-submitted|curated-exhibition-completed|curated-exhibition-scheduled|curated-exhibition-submitted">
    <fo:block space-before="12pt" margin-left="{$content.indent}">
      <fo:block xsl:use-attribute-sets="subheader.format" space-after="12pt" keep-with-next="1">
        <xsl:value-of select="section-header"/>
      </fo:block>
      <xsl:apply-templates select="events-record" />
    </fo:block>
  </xsl:template>

  <!-- Events -->
  <xsl:template match="events-record" name="events-record">
    <fo:block padding-bottom="2pt">
      <fo:table table-layout="fixed" width="100%">
        <fo:table-column column-width="17%" />
        <fo:table-column column-width="83%" />
        <fo:table-body start-indent="0pt">
          <fo:table-row>
            <fo:table-cell>
              <fo:block text-align="left">
                    <xsl:apply-templates select="eventstartdate" />
                    <xsl:if test="descendant::eventenddate">
                    	<fo:block>
                    	<fo:inline>
                    		<xsl:text>to&space;</xsl:text>
                    	</fo:inline>
                    	<xsl:apply-templates select="eventenddate" />
                    	</fo:block>
                    </xsl:if>  
              </fo:block>
            </fo:table-cell>
            <fo:table-cell>
              <fo:block text-align="left">
                <xsl:apply-templates select="eventdescription" />
                <xsl:apply-templates select="venue" />
                <xsl:apply-templates select="venuedescription" />
                <xsl:apply-templates select="city" />
                <xsl:apply-templates select="province" />
              </fo:block>
            </fo:table-cell>
          </fo:table-row>
          <fo:table-row>
            <fo:table-cell>
              <fo:block></fo:block>
            </fo:table-cell>
            <xsl:choose>
              <xsl:when test="link">
                <fo:table-cell text-align="left">
                  <xsl:apply-templates select="link">
                    <xsl:with-param name="urldescription" select="'View Event Information'" />
                    <xsl:with-param name="urlvalue">
                      <xsl:value-of select="link" />
                    </xsl:with-param>
                  </xsl:apply-templates>
                </fo:table-cell>
              </xsl:when>
              <xsl:otherwise>
                <fo:table-cell>
                  <fo:block></fo:block>
                </fo:table-cell>
              </xsl:otherwise>
            </xsl:choose>
          </fo:table-row>
        </fo:table-body>
      </fo:table>
    </fo:block>
  </xsl:template>
</xsl:stylesheet>
