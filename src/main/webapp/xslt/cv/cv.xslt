<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:fo="http://www.w3.org/1999/XSL/Format"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:import href="teaching/teaching-trainees.xslt"/>
    <xsl:import href="teaching/teaching-courses.xslt"/>
    <xsl:import href="teaching/teaching-contact-hours.xslt"/>
    <xsl:import href="teaching/teaching-curriculum.xslt"/>
    <xsl:import href="teaching/teaching-extension.xslt"/>
    <xsl:import href="teaching/teaching-special-advising.xslt"/>
    <xsl:import href="teaching/teaching-student-advising.xslt"/>
    <xsl:import href="teaching/teaching-thesis.xslt"/>
    <xsl:import href="teaching/teachingCommon.xslt"/>
    <xsl:import href="teaching/teaching-additional.xslt"/>
    <xsl:import href="teaching/teaching-supplement-month.xslt"/>
    <xsl:import href="personal/personal.xslt"/>
    <xsl:import href="personal/personal-additional.xslt"/>
    <xsl:import href="publications/publications-additional.xslt"/>
    <xsl:import href="publications/abstracts-published.xslt"/>
    <xsl:import href="publications/publicationsCommon.xslt"/>
    <xsl:import href="publications/books-edited-submitted.xslt"/>
    <xsl:import href="publications/editor-letters-submitted.xslt"/>
    <xsl:import href="publications/limited-submitted.xslt"/>
    <xsl:import href="publications/editor-letters-in-press.xslt"/>
    <xsl:import href="publications/presentations.xslt"/>
    <xsl:import href="publications/books-authored-published.xslt"/>
    <xsl:import href="publications/books-authored-in-press.xslt"/>
    <xsl:import href="publications/media-published.xslt"/>
    <xsl:import href="publications/limited-published.xslt"/>
    <xsl:import href="publications/books-edited-published.xslt"/>
    <xsl:import href="publications/book-chapters-submitted.xslt"/>
    <xsl:import href="publications/reviews-in-press.xslt"/>
    <xsl:import href="publications/journals-published.xslt"/>
    <xsl:import href="publications/journals-submitted.xslt"/>
    <xsl:import href="publications/books-edited-in-press.xslt"/>
    <xsl:import href="publications/abstracts-submitted.xslt"/>
    <xsl:import href="publications/limited-in-press.xslt"/>
    <xsl:import href="publications/reviews-submitted.xslt"/>
    <xsl:import href="publications/books-authored-submitted.xslt"/>
    <xsl:import href="publications/book-chapters-published.xslt"/>
    <xsl:import href="publications/book-chapters-in-press.xslt"/>
    <xsl:import href="publications/editor-letters-published.xslt"/>
    <xsl:import href="publications/reviews-published.xslt"/>
    <xsl:import href="publications/media-submitted.xslt"/>
    <xsl:import href="publications/abstracts-in-press.xslt"/>
    <xsl:import href="publications/media-in-press.xslt"/>
    <xsl:import href="publications/journals-in-press.xslt"/>
    <xsl:import href="publications/publicationsCommon.xslt"/>
    <xsl:import href="publications/patent.xslt"/>
    <xsl:import href="creativeactivities/creativeactivitiesCommon.xslt"/>
    <xsl:import href="creativeactivities/works.xslt"/>
    <xsl:import href="creativeactivities/events.xslt"/>
    <xsl:import href="creativeactivities/publicationevents.xslt"/>
    <xsl:import href="creativeactivities/reviews.xslt"/>
    <xsl:import href="creativeactivities/creative-activities-additional.xslt"/>
    <xsl:import href="evaluation/evaluations.xslt"/>
    <xsl:import href="knowledge/knowledgeCommon.xslt"/>
    <xsl:import href="knowledge/gathering.xslt"/>
    <xsl:import href="knowledge/media.xslt"/>
    <xsl:import href="knowledge/other.xslt"/>
    <xsl:import href="./style-options.xslt"/>
    <xsl:import href="interest/interest.xslt"/>
    <xsl:import href="service/service-other-university.xslt"/>
    <xsl:import href="service/serviceCommon.xslt"/>
    <xsl:import href="service/service-additional.xslt"/>
    <xsl:import href="service/service-administrative.xslt"/>
    <xsl:import href="service/service-campus.xslt"/>
    <xsl:import href="service/service-systemwide.xslt"/>
    <xsl:import href="service/service-other-nonuniversity.xslt"/>
    <xsl:import href="service/service-department.xslt"/>
    <xsl:import href="service/service-board.xslt"/>
    <xsl:import href="service/service-school.xslt"/>
    <xsl:import href="education/education.xslt"/>
    <xsl:import href="education/honors.xslt"/>
    <xsl:import href="education/credentials.xslt"/>
    <xsl:import href="education/education-additional.xslt"/>
    <xsl:import href="employment/employment.xslt"/>
    <xsl:import href="employment/employment-additional.xslt"/>
    <xsl:import href="grants/grants.xslt"/>
    <xsl:import href="./html-tags.xslt"/>
    <xsl:import href="posdescription/posdescription.xslt"/>
    <!-- daocument title font size -->
    <xsl:param name="title.fontsize">20pt</xsl:param>
    <!-- daocument title font size -->
    <xsl:param name="displayname.fontsize">16pt</xsl:param>
    <!-- headers font size; ex for header: Personal -->
    <xsl:param name="header.fontsize">14pt</xsl:param>
    <!-- All text font size -->
    <xsl:param name="text.fontsize">12pt</xsl:param>
    <!-- sub headers format properties -->
    <xsl:attribute-set name="subheader.format">
        <xsl:attribute name="font-size">12pt</xsl:attribute>
        <xsl:attribute name="font-weight">bold</xsl:attribute>
        <xsl:attribute name="text-decoration">underline</xsl:attribute>
    </xsl:attribute-set>
    <!-- sub-section headers format properties:ex-Committees-Department/Section -->
    <xsl:attribute-set name="secsubheader.format">
        <xsl:attribute name="font-size">10pt</xsl:attribute>
        <xsl:attribute name="font-weight">bold</xsl:attribute>
        <xsl:attribute name="text-decoration">underline</xsl:attribute>
    </xsl:attribute-set>
    <!-- TODO: get the page properties from DB -->
    <xsl:template match="/">
        <fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format">
            <fo:layout-master-set>
                <fo:simple-page-master margin-bottom=".5in" margin-left=".75in" margin-right=".75in"
                    margin-top=".5in" master-name="myinfovault" page-height="11in"
                    page-width="8.5in">
                    <fo:region-body margin-bottom="10pt" region-name="xsl-region-body"/>
                    <fo:region-before region-name="xsl-region-before"/>
                    <fo:region-after region-name="xsl-region-after"/>
                </fo:simple-page-master>
            </fo:layout-master-set>
            <fo:page-sequence master-reference="myinfovault">
                <fo:static-content flow-name="xsl-region-after">
                    <fo:table width="100%" table-layout="fixed">
                        <fo:table-column column-number="1" width="100%"/>
                        <fo:table-body>
                            <fo:table-row>
                                <fo:table-cell column-number="1" padding-top="2pt"
                                    xsl:use-attribute-sets="footer.format">
                                    <fo:block font-size="11pt" text-align="center">
                                        <xsl:if test="$pagenumbers = 'true'">
                                            <fo:page-number/>
                                        </xsl:if>
                                    </fo:block>
                                </fo:table-cell>
                            </fo:table-row>
                        </fo:table-body>
                    </fo:table>
                    <!--                    <fo:block font-size="11pt" text-align="center" xsl:use-attribute-sets="footer.format">-->
                    <!--                        <xsl:if test="$pagenumbers = 'true'">-->
                    <!--                            <fo:block space-before="2pt">-->
                    <!--                              <fo:page-number/>-->
                    <!--                            </fo:block>-->
                    <!--                        </xsl:if>-->
                    <!--                    </fo:block>-->
                </fo:static-content>
                <fo:flow flow-name="xsl-region-body">
                    <xsl:apply-templates/>
                </fo:flow>
            </fo:page-sequence>
        </fo:root>
    </xsl:template>
    <xsl:template name="document-title">
        <fo:block space-after="12pt" font-size="{$title.fontsize}"
            xsl:use-attribute-sets="document.title.format">
            <xsl:apply-templates select="attributes/attribute-record/document_title"/>
        </fo:block>
    </xsl:template>
    <xsl:template name="display-name">
        <xsl:if test="$full_name='true'">
            <fo:block space-after="12pt" font-size="{$displayname.fontsize}"
                xsl:use-attribute-sets="display.name.format">
                <xsl:apply-templates select="attributes/attribute-record/full_name"/>
            </fo:block>
        </xsl:if>
    </xsl:template>
    <xsl:template name="section-header">
        <xsl:param name="header"/>
        <fo:block font-size="14pt" keep-with-next="1" space-after="12pt" white-space-collapse="true"
            xsl:use-attribute-sets="header.format">
            <xsl:value-of select="$header"/>
        </fo:block>
    </xsl:template>
    <xsl:template match="section-header">
        <fo:block font-size="14pt" space-after="12pt" keep-with-next="1" white-space-collapse="true"
            xsl:use-attribute-sets="header.format">
            <xsl:apply-templates/>
        </fo:block>
    </xsl:template>
    <xsl:template name="subsection-header">
        <xsl:param name="header"/>
        <fo:block space-after="12pt" white-space-collapse="true"
            xsl:use-attribute-sets="subheader.format">
            <xsl:value-of select="$header"/>
        </fo:block>
    </xsl:template>
    <xsl:template name="secsubsection-header">
        <xsl:param name="header"/>
        <fo:block space-after="12pt" white-space-collapse="true"
            xsl:use-attribute-sets="secsubheader.format">
            <xsl:value-of select="$header"/>
        </fo:block>
    </xsl:template>

    <xsl:template match="biosketch">
        <fo:block font-family="Helvetica">
            <xsl:call-template name="document-title"/>

            <xsl:if test="personal">
                <xsl:call-template name="display-name"/>
                <fo:block id="personal">
                    <xsl:if test="$personal='true'">
                        <xsl:call-template name="section-header">
                            <xsl:with-param name="header">Personal Information</xsl:with-param>
                        </xsl:call-template>
                    </xsl:if>
                    <fo:block padding-bottom="12pt">
                    <!-- Using address information from the xml for the address templates since they need to be formatted differently -->
                        <xsl:choose>
                            <xsl:when test="//addresstypeid/text()='1'">
                                <xsl:apply-templates select="personal-address/address-record[addresstypeid/text()='1']" mode="perm"/>    
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:if test="$permanent_address='true'">
                                    <xsl:call-template name="address">
                                                    <xsl:with-param name="addresstype">Permanent Address</xsl:with-param>
                                                </xsl:call-template>
                                </xsl:if>
                            </xsl:otherwise>
                        </xsl:choose>
                      <xsl:call-template name="permphone">
                          <xsl:with-param name="value">
                              <xsl:value-of select="attributes/attribute-record/permanent_phone"/>
                          </xsl:with-param>
                      </xsl:call-template>

                      <xsl:call-template name="permfax">
                          <xsl:with-param name="value">
                              <xsl:value-of select="attributes/attribute-record/permanent_fax"/>
                          </xsl:with-param>
                      </xsl:call-template>
                      <xsl:choose>
                        <xsl:when test="//addresstypeid/text()='2'">
                            <xsl:apply-templates select="personal-address/address-record[addresstypeid/text()='2']" mode="curr"/>    
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:if test="$current_address='true'">
                                <xsl:call-template name="address">
                                                <xsl:with-param name="addresstype">Current Address</xsl:with-param>
                                            </xsl:call-template>
                            </xsl:if>
                        </xsl:otherwise>
                    </xsl:choose>
                        <xsl:call-template name="currphone">
                            <xsl:with-param name="value">
                                <xsl:value-of select="attributes/attribute-record/current_phone"/>
                            </xsl:with-param>
                        </xsl:call-template>
                        <xsl:call-template name="currfax">
                            <xsl:with-param name="value">
                                <xsl:value-of select="attributes/attribute-record/current_fax"/>
                            </xsl:with-param>
                        </xsl:call-template>
                        <xsl:choose>
                        <xsl:when test="//addresstypeid/text()='3'">
                            <xsl:apply-templates select="personal-address/address-record[addresstypeid/text()='3']" mode="work"/>    
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:if test="$office_address='true'">
                                <xsl:call-template name="address">
                                                <xsl:with-param name="addresstype">Work Address</xsl:with-param>
                                            </xsl:call-template>
                            </xsl:if>
                        </xsl:otherwise>
                    </xsl:choose>
                    <xsl:call-template name="workphone">
                        <xsl:with-param name="value">
                            <xsl:value-of select="attributes/attribute-record/office_phone"/>
                        </xsl:with-param>
                    </xsl:call-template>
                    <xsl:call-template name="workfax">
                        <xsl:with-param name="value">
                            <xsl:value-of select="attributes/attribute-record/office_fax"/>
                        </xsl:with-param>
                    </xsl:call-template>
                    <xsl:call-template name="cellphone">
                        <xsl:with-param name="value">
                            <xsl:value-of select="attributes/attribute-record/cell_phone"/>
                        </xsl:with-param>
                    </xsl:call-template>
                    <xsl:apply-templates select="personal-email"/>
                    <xsl:call-template name="website">
                        <xsl:with-param name="value">
                            <xsl:value-of select="attributes/attribute-record/link"/>
                        </xsl:with-param>
                    </xsl:call-template>
<!--                        <xsl:apply-templates select="personal-link"/>-->
                      <xsl:apply-templates select="personal" mode="birthdate"/>
                      <xsl:apply-templates select="personal" mode="citizen"/>
                      <xsl:apply-templates select="personal" mode="dateentered"/>
                      <xsl:apply-templates select="personal" mode="visatype"/>
                  </fo:block>
                </fo:block>
            </xsl:if>
            
            <xsl:apply-templates select="personal-additional" mode="personal-additional"/>
            <xsl:apply-templates select="interest-area"/>
            <xsl:if test="$agstation-reports='true'">
                <fo:block space-before="24pt">
                    <xsl:call-template name="section-header">
                        <xsl:with-param name="header">Agricultural Experiment
                        Station</xsl:with-param>
                    </xsl:call-template>
                </fo:block>
             </xsl:if>
              <fo:block id="agstation">
                  <xsl:apply-templates select="agstation-reports"/>
              </fo:block>
            

            <xsl:if
                test="$education-header='true' and ($education = 'true' or $credential = 'true' or $education-additional = 'true')">
                <fo:block space-before="24pt">
                    <xsl:call-template name="section-header">
                        <xsl:with-param name="header">Education</xsl:with-param>
                    </xsl:call-template>
                </fo:block>
            </xsl:if>
            <fo:block id="education">
                <xsl:apply-templates select="education"/>
                <xsl:apply-templates select="credential"/>
                <xsl:apply-templates select="education-additional" mode="education-additional"/>
            </fo:block>

            <!-- Make sure at least one sub heading has data selected to display before displaying the header -->
            <xsl:if
                test="$employment-header='true' and ($employment = 'true' or $employment-additional = 'true')">
                <fo:block space-before="24pt">
                    <xsl:call-template name="section-header">
                        <xsl:with-param name="header">Employment</xsl:with-param>
                    </xsl:call-template>
                </fo:block>
            </xsl:if>
            <fo:block id="employment">
                <xsl:apply-templates select="employment"/>
                <xsl:apply-templates select="employment-additional" mode="employment-additional"/>
            </fo:block>

            <!-- Make sure at least one sub heading has data selected to display before displaying the header -->
            <xsl:if
                test="$knowledge-header='true' and ($extending-knowledge-media ='true' or
                             $extending-knowledge-gathering ='true' or
                             $extending-knowledge-other ='true')">
                <fo:block space-before="24pt">
                    <xsl:call-template name="section-header">
                        <xsl:with-param name="header">Extending Knowledge</xsl:with-param>
                    </xsl:call-template>
                </fo:block>
            </xsl:if>
            <fo:block id="knowledge">
                <xsl:apply-templates select="extending-knowledge-media"/>
                <xsl:apply-templates select="extending-knowledge-gathering"/>
                <xsl:apply-templates select="extending-knowledge-other"/>
            </fo:block>

            <!-- Make sure at least one sub heading has data selected to display before displaying the header -->
            <xsl:if
                test="$grants-header = 'true' and ($grants-active = 'true' or 
                             $grants-pending = 'true' or
                             $grants-completed = 'true' or 
                             $grants-notawarded = 'true' or
                             $grants-none = 'true')">
                <fo:block space-before="24pt">
                    <xsl:call-template name="section-header">
                        <xsl:with-param name="header">Grants and Contracts</xsl:with-param>
                    </xsl:call-template>
                </fo:block>
            </xsl:if>
            <fo:block id="grants">
                <xsl:apply-templates select="grants-active"/>
                <xsl:apply-templates select="grants-pending"/>
                <xsl:apply-templates select="grants-completed"/>
                <xsl:apply-templates select="grants-notawarded"/>
                <xsl:apply-templates select="grants-none"/>
            </fo:block>

            <!-- Make sure at least one sub heading has data selected to display before displaying the header -->
            <xsl:if
                test="$honors='true'">
                <fo:block space-before="24pt">
                    <xsl:call-template name="section-header">
                        <xsl:with-param name="header">Honors &amp; Awards</xsl:with-param>
                    </xsl:call-template>
                </fo:block>
            </xsl:if>
            <fo:block id="honors">
                <xsl:apply-templates select="honors"/>
            </fo:block>

            <xsl:if test="$course-evaluations='true'">
                <fo:block space-before="24pt">
                    <xsl:call-template name="section-header">
                        <xsl:with-param name="header">List of Evaluations</xsl:with-param>
                    </xsl:call-template>
                </fo:block>
            </xsl:if>
            <fo:block id="evaluations">
                <xsl:apply-templates select="course-evaluations"/>
            </fo:block>

            <xsl:if test="$position-description='true'">
                <fo:block space-before="24pt">
                    <xsl:call-template name="section-header">
                        <xsl:with-param name="header">Position Description</xsl:with-param>
                    </xsl:call-template>
                </fo:block>
            </xsl:if>
            <fo:block id="posdescription">
                <xsl:apply-templates select="position-description"/>
            </fo:block>

            <!-- Make sure at least one sub heading has data selected to display before displaying the header -->
            <xsl:if
                test="$publications-header='true' and 
                ($abstracts-published = 'true' or
                $abstracts-in-press = 'true' or
                $abstracts-submitted = 'true' or
                $media-published = 'true' or
                $media-in-press = 'true' or
                $media-submitted = 'true' or
                $books-authored-published = 'true' or
                $books-authored-in-press = 'true' or
                $books-authored-submitted = 'true' or
                $book-chapters-published = 'true' or
                $book-chapters-in-press = 'true' or
                $book-chapters-submitted = 'true' or
                $books-edited-published = 'true' or
                $books-edited-in-press = 'true' or
                $books-edited-submitted = 'true' or
                $reviews-published = 'true' or
                $reviews-in-press = 'true' or
                $reviews-submitted = 'true' or
                $journals-published = 'true' or
                $journals-in-press = 'true' or
                $journals-submitted = 'true' or
                $editor-letters-published = 'true' or
                $editor-letters-in-press = 'true' or
                $editor-letters-submitted = 'true' or
                $limited-published = 'true' or
                $limited-in-press = 'true' or
                $limited-submitted = 'true' or
                $presentations = 'true' or
                $patent = 'true' or
                $disclosure = 'true' or
                $publication-additional = 'true')">
                <fo:block space-before="24pt">
                    <xsl:call-template name="section-header">
                        <xsl:with-param name="header">Publications</xsl:with-param>
                    </xsl:call-template>
                </fo:block>
            </xsl:if>
            <fo:block id="publications" font-family="Helvetica" font-size="12pt">
                <xsl:apply-templates select="journals-published"/>
                <xsl:apply-templates select="journals-in-press"/>
                <xsl:apply-templates select="journals-submitted"/>
                <xsl:apply-templates select="book-chapters-published"/>
                <xsl:apply-templates select="book-chapters-in-press"/>
                <xsl:apply-templates select="book-chapters-submitted"/>
                <xsl:apply-templates select="editor-letters-published"/>
                <xsl:apply-templates select="editor-letters-in-press"/>
                <xsl:apply-templates select="editor-letters-submitted"/>
                <xsl:apply-templates select="books-edited-published"/>
                <xsl:apply-templates select="books-edited-in-press"/>
                <xsl:apply-templates select="books-edited-submitted"/>
                <xsl:apply-templates select="reviews-published"/>
                <xsl:apply-templates select="reviews-in-press"/>
                <xsl:apply-templates select="reviews-submitted"/>
                <xsl:apply-templates select="books-authored-published"/>
                <xsl:apply-templates select="books-authored-in-press"/>
                <xsl:apply-templates select="books-authored-submitted"/>
                <xsl:apply-templates select="limited-published"/>
                <xsl:apply-templates select="limited-in-press"/>
                <xsl:apply-templates select="limited-submitted"/>
                <xsl:apply-templates select="media-published"/>
                <xsl:apply-templates select="media-in-press"/>
                <xsl:apply-templates select="media-submitted"/>
                <xsl:apply-templates select="abstracts-published"/>
                <xsl:apply-templates select="abstracts-in-press"/>
                <xsl:apply-templates select="abstracts-submitted"/>
                <xsl:apply-templates select="presentations"/>
                <xsl:apply-templates select="patent-granted"/>
                <xsl:apply-templates select="patent-filed"/>
                <xsl:apply-templates select="disclosure"/>
                <xsl:apply-templates select="publication-additional" mode="publications-additional"/>
            </fo:block>
            
      <!-- Make sure at least one sub heading has data selected to display 
        before displaying the header -->
      <xsl:if test="$creativeactivities-header='true' and 
              ($works-completed='true' or
               $works-scheduled='true' or
               $works-submitted='true')">
        <fo:block space-before="24pt">
          <xsl:call-template name="section-header">
            <xsl:with-param name="header">
              Creative Work
            </xsl:with-param>
          </xsl:call-template>
        </fo:block>
      </xsl:if>
      <fo:block id="creativeactivities-works" font-family="Helvetica"
        font-size="12pt">
          <xsl:apply-templates select="works-completed" />
          <xsl:apply-templates select="works-scheduled" />
          <xsl:apply-templates select="works-submitted" />
          </fo:block>
          <xsl:if test="$creativeactivities-header='true' and
               ($book-completed='true' or
                $book-scheduled='true' or
                $book-submitted='true' or
                $broadcast-completed='true' or
                $broadcast-scheduled='true' or
                $broadcast-submitted='true' or
                $catalogs-completed='true' or
                $catalogs-scheduled='true' or
                $catalogs-submitted='true' or
                $cd-dvd-video-completed='true' or
                $cd-dvd-video-scheduled='true' or
                $cd-dvd-video-submitted='true' or
                $citation-completed='true' or
                $citation-scheduled='true' or
                $citation-submitted='true' or
                $commission-completed='true' or
                $commission-scheduled='true' or
                $commission-submitted='true' or
                $concert-completed='true' or
                $concert-scheduled='true' or
                $concert-submitted='true' or
                $exhibitions-group-completed='true' or
                $exhibitions-group-scheduled='true' or
                $exhibitions-group-submitted='true' or
                $exhibitions-solo-completed='true' or
                $exhibitions-solo-scheduled='true' or
                $exhibitions-solo-submitted='true' or
                $structure-landscape-completed='true' or
                $structure-landscape-scheduled='true' or
                $structure-landscape-submitted='true' or
                $fashion-show-completed='true' or
                $fashion-show-scheduled='true' or
                $fashion-show-submitted='true' or
                $interview-commentary-completed='true' or
                $interview-commentary-scheduled='true' or
                $interview-commentary-submitted='true' or
                $performance-event-completed='true' or
                $performance-event-scheduled='true' or
                $performance-event-submitted='true' or
                $private-collection-completed='true' or
                $private-collection-scheduled='true' or
                $private-collection-submitted='true' or
                $product-completed='true' or
                $product-scheduled='true' or
                $product-submitted='true' or
                $program-notes-completed='true' or
                $program-notes-scheduled='true' or
                $program-notes-submitted='true' or
                $public-collection-completed='true' or
                $public-collection-scheduled='true' or
                $public-collection-submitted='true' or
                $reading-completed='true' or
                $reading-scheduled='true' or
                $reading-submitted='true' or
                $recordings-completed='true' or
                $recordings-scheduled='true' or
                $recordings-submitted='true' or
                $reproductions-completed='true' or
                $reproductions-scheduled='true' or
                $reproductions-submitted='true' or
                $screening-completed='true' or
                $screening-scheduled='true' or
                $screening-submitted='true' or
                $theatre-production-completed='true' or
                $theatre-production-scheduled='true' or
                $theatre-production-submitted='true' or
                $curated-exhibition-completed='true' or
                $curated-exhibition-scheduled='true' or
                $curated-exhibition-submitted='true')">
        <fo:block space-before="24pt">
          <xsl:call-template name="section-header">
            <xsl:with-param name="header">
              Creative Activities: Public Dissemination
            </xsl:with-param>
          </xsl:call-template>
        </fo:block>
      </xsl:if>               
      <fo:block id="creativeactivities-events" font-family="Helvetica"
        font-size="12pt">
          <xsl:apply-templates select="book-completed" />
          <xsl:apply-templates select="book-scheduled" />
          <xsl:apply-templates select="book-submitted" />
          <xsl:apply-templates select="broadcast-completed" />
          <xsl:apply-templates select="broadcast-scheduled" />
          <xsl:apply-templates select="broadcast-submitted" />
          <xsl:apply-templates select="catalogs-completed" />
          <xsl:apply-templates select="catalogs-scheduled" />
          <xsl:apply-templates select="catalogs-submitted" />
          <xsl:apply-templates select="cd-dvd-video-completed" />
          <xsl:apply-templates select="cd-dvd-video-scheduled" />
          <xsl:apply-templates select="cd-dvd-video-submitted" />
          <xsl:apply-templates select="citation-completed" />
          <xsl:apply-templates select="citation-scheduled" />
          <xsl:apply-templates select="citation-submitted" />
          <xsl:apply-templates select="commission-completed" />
          <xsl:apply-templates select="commission-scheduled" />
          <xsl:apply-templates select="commission-submitted" />
          <xsl:apply-templates select="concert-completed" />
          <xsl:apply-templates select="concert-scheduled" />
          <xsl:apply-templates select="concert-submitted" />
          <xsl:apply-templates select="exhibitions-group-completed" />
          <xsl:apply-templates select="exhibitions-group-scheduled" />
          <xsl:apply-templates select="exhibitions-group-submitted" />
          <xsl:apply-templates select="exhibitions-solo-completed" />
          <xsl:apply-templates select="exhibitions-solo-scheduled" />
          <xsl:apply-templates select="exhibitions-solo-submitted" />
          <xsl:apply-templates select="structure-landscape-completed" />
          <xsl:apply-templates select="structure-landscape-scheduled" />
          <xsl:apply-templates select="structure-landscape-submitted" />
          <xsl:apply-templates select="fashion-show-completed" />
          <xsl:apply-templates select="fashion-show-scheduled" />
          <xsl:apply-templates select="fashion-show-submitted" />
          <xsl:apply-templates select="interview-commentary-completed" />
          <xsl:apply-templates select="interview-commentary-scheduled" />
          <xsl:apply-templates select="interview-commentary-submitted" />
          <xsl:apply-templates select="performance-event-completed" />
          <xsl:apply-templates select="performance-event-scheduled" />
          <xsl:apply-templates select="performance-event-submitted" />
          <xsl:apply-templates select="private-collection-completed" />
          <xsl:apply-templates select="private-collection-scheduled" />
          <xsl:apply-templates select="private-collection-submitted" />
          <xsl:apply-templates select="product-completed" />
          <xsl:apply-templates select="product-scheduled" />
          <xsl:apply-templates select="product-submitted" />
          <xsl:apply-templates select="program-notes-completed" />
          <xsl:apply-templates select="program-notes-scheduled" />
          <xsl:apply-templates select="program-notes-submitted" />
          <xsl:apply-templates select="public-collection-completed" />
          <xsl:apply-templates select="public-collection-scheduled" />
          <xsl:apply-templates select="public-collection-submitted" />
          <xsl:apply-templates select="reading-completed" />
          <xsl:apply-templates select="reading-scheduled" />
          <xsl:apply-templates select="reading-submitted" />
          <xsl:apply-templates select="recordings-completed" />
          <xsl:apply-templates select="recordings-scheduled" />
          <xsl:apply-templates select="recordings-submitted" />
          <xsl:apply-templates select="reproductions-completed" />
          <xsl:apply-templates select="reproductions-scheduled" />
          <xsl:apply-templates select="reproductions-submitted" />
          <xsl:apply-templates select="screening-completed" />
          <xsl:apply-templates select="screening-scheduled" />
          <xsl:apply-templates select="screening-submitted" />
          <xsl:apply-templates select="theatre-production-completed" />
          <xsl:apply-templates select="theatre-production-scheduled" />
          <xsl:apply-templates select="theatre-production-submitted" />
          <xsl:apply-templates select="curated-exhibition-completed" />
          <xsl:apply-templates select="curated-exhibition-scheduled" />
          <xsl:apply-templates select="curated-exhibition-submitted" />
          </fo:block>
          <xsl:if test="$creativeactivities-header='true' and
               ($publicationevents-published='true' or
               $publicationevents-in-press='true' or
               $publicationevents-submitted='true')">
        <fo:block space-before="24pt">
          <xsl:call-template name="section-header">
            <xsl:with-param name="header">
              Creative Activities: Publication Events
            </xsl:with-param>
          </xsl:call-template>
        </fo:block>
      </xsl:if>
      <fo:block id="creativeactivities-publicationevents" font-family="Helvetica"
        font-size="12pt">
          <xsl:apply-templates select="publicationevents-published" />
          <xsl:apply-templates select="publicationevents-in-press" />
          <xsl:apply-templates select="publicationevents-submitted" />
      </fo:block>
      <xsl:if test="$creativeactivities-header='true'and $reviews= 'true'">
        <fo:block space-before="24pt">
          <xsl:call-template name="section-header">
            <xsl:with-param name="header">
              Creative Activities: Reviews By Others
            </xsl:with-param>
          </xsl:call-template>
        </fo:block>
      </xsl:if>
      <fo:block id="creativeactivities-reviews" font-family="Helvetica"
        font-size="12pt">
          <xsl:apply-templates select="reviews" />
      </fo:block>
      <xsl:if test="$creativeactivities-header='true' and $creative-activities-additional = 'true'">
        <fo:block space-before="24pt">
          <xsl:call-template name="section-header">
            <xsl:with-param name="header">
              Creative Activities: Additional Information
            </xsl:with-param>
          </xsl:call-template>
        </fo:block>
      </xsl:if>
      <fo:block id="creativeactivities-additional" font-family="Helvetica"
        font-size="12pt">
          <xsl:apply-templates select="creative-activities-additional" />
      </fo:block>
      
            <!-- Make sure at least one sub heading has data selected to display before displaying the header -->
            <xsl:if
                test="$service-header='true' and 
                    ($service-department = 'true' or 
                    $service-school = 'true' or
                    $service-campus = 'true' or
                    $service-systemwide = 'true' or
                    $service-other-university = 'true' or
                    $service-other-nonuniversity = 'true' or
                    $service-board = 'true' or
                    $service-additional = 'true' or
                    $service-administrative = 'true')">
                <fo:block space-before="24pt">
                    <xsl:call-template name="section-header">
                        <xsl:with-param name="header">Service</xsl:with-param>
                    </xsl:call-template>
                </fo:block>
            </xsl:if>
            <fo:block id="service">
                <xsl:apply-templates select="service-administrative"/>
                <xsl:if
                    test="service-department | service-systemwide | service-campus | service-other-nonuniversity | service-other-university   | service-school">
                    <fo:block margin-left="{$content.indent}">
                        <xsl:call-template name="subsection-header">
                            <xsl:with-param name="header">Committees</xsl:with-param>
                        </xsl:call-template>
                    </fo:block>
                    <xsl:apply-templates select="service-department"/>
                    <xsl:apply-templates select="service-school"/>
                    <xsl:apply-templates select="service-campus"/>
                    <xsl:apply-templates select="service-systemwide"/>
                    <xsl:apply-templates select="service-other-university"/>
                    <xsl:apply-templates select="service-other-nonuniversity"/>
                </xsl:if>
                <xsl:apply-templates select="service-board"/>
                <xsl:apply-templates select="service-additional" mode="service-additional"/>
            </fo:block>

            <!-- Make sure at least one sub heading has data selected to display before displaying the header -->
            <xsl:if
                test="$teaching-header='true' and 
                  ($teaching-contact-hours = 'true' or
                  $teaching-courses = 'true' or
                  $teaching-curriculum = 'true' or                
                  $teaching-supplement-term = 'true' or
                  $teaching-supplement-month = 'true' or
                  $teaching-special-advising = 'true' or
                  $teaching-student-advising = 'true' or
                  $teaching-thesis = 'true' or
                  $teaching-trainees = 'true' or
                  $teaching-extension = 'true' or
                  $teaching-additional = 'true' )">
                <fo:block space-before="24pt">
                    <xsl:call-template name="section-header">
                        <xsl:with-param name="header">Teaching</xsl:with-param>
                    </xsl:call-template>
                </fo:block>
            </xsl:if>
            <fo:block id="teaching">
                <xsl:apply-templates select="teaching-contact-hours"/>
                <xsl:apply-templates select="teaching-courses"/>
                <xsl:apply-templates select="teaching-curriculum"/>
                <xsl:apply-templates select="teaching-supplement-term"/>
                <xsl:apply-templates select="teaching-supplement-month"/>
                <xsl:apply-templates select="teaching-special-advising"/>
                <xsl:apply-templates select="teaching-student-advising"/>
                <xsl:apply-templates select="teaching-thesis"/>
                <xsl:apply-templates select="teaching-trainees"/>
                <xsl:apply-templates select="teaching-extension"/>
                <xsl:apply-templates select="teaching-additional" mode="teaching-additional"/>
            </fo:block>
        </fo:block>
    </xsl:template>
</xsl:stylesheet>
