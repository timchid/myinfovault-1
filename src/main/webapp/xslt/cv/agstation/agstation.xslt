<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0"
   xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
   xmlns:fo="http://www.w3.org/1999/XSL/Format">
   
   <xsl:template match="agstation-reports">
     <xsl:apply-templates select="agstation-record"/>
    </xsl:template>
   
   <xsl:template match="agstation-record">
      <fo:block margin-left="{$content.indent}" space-after="12pt" keep-together.within-page="always">
         <fo:table table-layout="fixed" width="100%">
            <fo:table-column column-width="20%" />
            <fo:table-column column-width="80%" />
            <fo:table-body start-indent="0pt">
               <fo:table-row>
                  <fo:table-cell>
                     <fo:block>Title:</fo:block>
                  </fo:table-cell>
                  <fo:table-cell>
                     <fo:block><xsl:apply-templates select="reporttitle"/></fo:block>
                  </fo:table-cell>
               </fo:table-row>
               <fo:table-row>
                  <fo:table-cell>
                     <fo:block>Number:</fo:block>
                  </fo:table-cell>
                  <fo:table-cell>
                     <fo:block><xsl:apply-templates select="reportnumber"/></fo:block>
                  </fo:table-cell>
               </fo:table-row>
               <fo:table-row>
                  <fo:table-cell>
                     <fo:block>Investigators:</fo:block>
                  </fo:table-cell>
                  <fo:table-cell>
                     <fo:block><xsl:apply-templates select="investigatorname"/></fo:block>
                  </fo:table-cell>
               </fo:table-row>
               <fo:table-row>
                  <fo:table-cell>
                     <fo:block>Date(s):</fo:block>
                  </fo:table-cell>
                  <fo:table-cell>
                     <fo:block><xsl:apply-templates select="datespan"/></fo:block>
                  </fo:table-cell>
               </fo:table-row>
            </fo:table-body>
         </fo:table>
      </fo:block>
   </xsl:template>
   
   <xsl:template match="reporttitle">
      <fo:block><xsl:apply-templates /></fo:block>
   </xsl:template>
   
   <xsl:template match="reportnumber">
      <fo:block><xsl:apply-templates /></fo:block>
   </xsl:template>
   
   <xsl:template match="investigatorname">
      <fo:block><xsl:apply-templates /></fo:block>
   </xsl:template>
   
   <xsl:template match="datespan">
      <fo:block><xsl:apply-templates /></fo:block>
   </xsl:template>
   
</xsl:stylesheet>