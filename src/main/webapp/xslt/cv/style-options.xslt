<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:fo="http://www.w3.org/1999/XSL/Format">
    <!--Document title-->
    <xsl:param name="document.title"></xsl:param>
    
    <!--Document title Formatting-->
    <xsl:attribute-set name="document.title.format">
        <xsl:attribute name="text-align"></xsl:attribute>
        <xsl:attribute name="border-top"></xsl:attribute>
        <xsl:attribute name="border-bottom"></xsl:attribute>
    </xsl:attribute-set>
    
    
     <!-- Display Name -->
    <xsl:attribute-set name="display.name.format">
      <xsl:attribute name="text-align"></xsl:attribute>
    </xsl:attribute-set>
    
    <!-- Headers -->
    <xsl:attribute-set name="header.format">
        <xsl:attribute name="font-weight"></xsl:attribute>
<!--        <xsl:attribute name="font-style"></xsl:attribute> -->
        <xsl:attribute name="text-align"></xsl:attribute>  
        <xsl:attribute name="margin-left"></xsl:attribute>
    </xsl:attribute-set>

    
    <!-- Content -->
    <xsl:param name="content.indent"></xsl:param>
    <!-- Footer -->
    <xsl:attribute-set name="footer.format">
        <xsl:attribute name="border-top"></xsl:attribute>
    </xsl:attribute-set>    
     <xsl:param name="pagenumbers"></xsl:param>
    
    <!-- Parameter Definitions -->
    <xsl:param name="full_name"></xsl:param>
    <xsl:param name="permanent_address"></xsl:param>
    <xsl:param name="permanent_phone"></xsl:param>
    <xsl:param name="permanent_fax"></xsl:param>
    <xsl:param name="current_address"></xsl:param>
    <xsl:param name="current_phone"></xsl:param>
    <xsl:param name="current_fax"></xsl:param>
    <xsl:param name="office_address"></xsl:param>
    <xsl:param name="office_phone"></xsl:param>
    <xsl:param name="office_fax"></xsl:param>
    <xsl:param name="cell_phone"></xsl:param>
    <xsl:param name="email"></xsl:param>
    <xsl:param name="link"></xsl:param>
    <xsl:param name="birthDate"></xsl:param>
    <xsl:param name="citizen"></xsl:param>
    <xsl:param name="date_entered"></xsl:param>
    <xsl:param name="visa_type"></xsl:param>
    <xsl:param name="personal"></xsl:param>
    <xsl:param name="personal-address"></xsl:param>
    <xsl:param name="personal-phone"></xsl:param>
    <xsl:param name="personal-email"></xsl:param>
    <xsl:param name="personal-link"></xsl:param>
    <xsl:param name="personal-additional"></xsl:param>
    <xsl:param name="interest-area"></xsl:param>
    <xsl:param name="agstation-reports"></xsl:param>
    <xsl:param name="education"></xsl:param>
    <xsl:param name="honors"></xsl:param>
    <xsl:param name="credential"></xsl:param>
    <xsl:param name="education-header"></xsl:param>
    <xsl:param name="education-additional"></xsl:param>
    <xsl:param name="employment"></xsl:param>
    <xsl:param name="employment-header"></xsl:param>
    <xsl:param name="employment-additional"></xsl:param>
    <xsl:param name="knowledge-header"></xsl:param>
    <xsl:param name="extending-knowledge-media"></xsl:param>
    <xsl:param name="extending-knowledge-gathering"></xsl:param>
    <xsl:param name="extending-knowledge-other"></xsl:param>
    <xsl:param name="grants-header"></xsl:param>
    <xsl:param name="grants-active"></xsl:param>
    <xsl:param name="grants-pending"></xsl:param>
    <xsl:param name="grants-completed"></xsl:param>
    <xsl:param name="grants-notawarded"></xsl:param>
    <xsl:param name="grants-none"></xsl:param>
    <xsl:param name="course-evaluations"></xsl:param>
    <xsl:param name="positions-header"></xsl:param>
    <xsl:param name="position-description"></xsl:param>
    <xsl:param name="publications-header"></xsl:param>
    <xsl:param name="abstracts-published"></xsl:param>
    <xsl:param name="abstracts-in-press"></xsl:param>
    <xsl:param name="abstracts-submitted"></xsl:param>
    <xsl:param name="media-published"></xsl:param>
    <xsl:param name="media-in-press"></xsl:param>
    <xsl:param name="media-submitted"></xsl:param>
    <xsl:param name="books-authored-published"></xsl:param>
    <xsl:param name="books-authored-in-press"></xsl:param>
    <xsl:param name="books-authored-submitted"></xsl:param>
    <xsl:param name="book-chapters-published"></xsl:param>
    <xsl:param name="book-chapters-in-press"></xsl:param>
    <xsl:param name="book-chapters-submitted"></xsl:param>
    <xsl:param name="books-edited-published"></xsl:param>
    <xsl:param name="books-edited-in-press"></xsl:param>
    <xsl:param name="books-edited-submitted"></xsl:param>
    <xsl:param name="reviews-published"></xsl:param>
    <xsl:param name="reviews-in-press"></xsl:param>
    <xsl:param name="reviews-submitted"></xsl:param>
    <xsl:param name="journals-published"></xsl:param>
    <xsl:param name="journals-in-press"></xsl:param>
    <xsl:param name="journals-submitted"></xsl:param>
    <xsl:param name="editor-letters-published"></xsl:param>
    <xsl:param name="editor-letters-in-press"></xsl:param>
    <xsl:param name="editor-letters-submitted"></xsl:param>
    <xsl:param name="limited-published"></xsl:param>
    <xsl:param name="limited-in-press"></xsl:param>
    <xsl:param name="limited-submitted"></xsl:param>
    <!--Declare variables for patent : Start-->
    <xsl:param name="presentations"></xsl:param>
    <xsl:param name="patent"></xsl:param>
    <!--Declare variables for patent : End-->
    <xsl:param name="disclosure"></xsl:param>
    <xsl:param name="publication-additional"></xsl:param>

    <xsl:param name="creativeactivities-header"></xsl:param>
    <xsl:param name="works-completed"></xsl:param>
    <xsl:param name="works-scheduled"></xsl:param>
    <xsl:param name="works-submitted"></xsl:param>
    <xsl:param name="book-completed"></xsl:param>
    <xsl:param name="book-scheduled"></xsl:param>
    <xsl:param name="book-submitted"></xsl:param>
    <xsl:param name="broadcast-completed"></xsl:param>
    <xsl:param name="broadcast-scheduled"></xsl:param>
    <xsl:param name="broadcast-submitted"></xsl:param>
    <xsl:param name="catalogs-completed"></xsl:param>
    <xsl:param name="catalogs-scheduled"></xsl:param>
    <xsl:param name="catalogs-submitted"></xsl:param>
    <xsl:param name="cd-dvd-video-completed"></xsl:param>
    <xsl:param name="cd-dvd-video-scheduled"></xsl:param>
    <xsl:param name="cd-dvd-video-submitted"></xsl:param>
    <xsl:param name="citation-completed"></xsl:param>
    <xsl:param name="citation-scheduled"></xsl:param>
    <xsl:param name="citation-submitted"></xsl:param>
    <xsl:param name="commission-completed"></xsl:param>
    <xsl:param name="commission-scheduled"></xsl:param>
    <xsl:param name="commission-submitted"></xsl:param>
    <xsl:param name="concert-completed"></xsl:param>
    <xsl:param name="concert-scheduled"></xsl:param>
    <xsl:param name="concert-submitted"></xsl:param>
    <xsl:param name="exhibitions-group-completed"></xsl:param>
    <xsl:param name="exhibitions-group-scheduled"></xsl:param>
    <xsl:param name="exhibitions-group-submitted"></xsl:param>
    <xsl:param name="exhibitions-solo-completed"></xsl:param>
    <xsl:param name="exhibitions-solo-scheduled"></xsl:param>
    <xsl:param name="exhibitions-solo-submitted"></xsl:param>
    <xsl:param name="structure-landscape-completed"></xsl:param>
    <xsl:param name="structure-landscape-scheduled"></xsl:param>
    <xsl:param name="structure-landscape-submitted"></xsl:param>
    <xsl:param name="fashion-show-completed"></xsl:param>
    <xsl:param name="fashion-show-scheduled"></xsl:param>
    <xsl:param name="fashion-show-submitted"></xsl:param>
    <xsl:param name="interview-commentary-completed"></xsl:param>
    <xsl:param name="interview-commentary-scheduled"></xsl:param>
    <xsl:param name="interview-commentary-submitted"></xsl:param>
    <xsl:param name="performance-event-completed"></xsl:param>
    <xsl:param name="performance-event-scheduled"></xsl:param>
    <xsl:param name="performance-event-submitted"></xsl:param>
    <xsl:param name="private-collection-completed"></xsl:param>
    <xsl:param name="private-collection-scheduled"></xsl:param>
    <xsl:param name="private-collection-submitted"></xsl:param>
    <xsl:param name="product-completed"></xsl:param>
    <xsl:param name="product-scheduled"></xsl:param>
    <xsl:param name="product-submitted"></xsl:param>
    <xsl:param name="program-notes-completed"></xsl:param>
    <xsl:param name="program-notes-scheduled"></xsl:param>
    <xsl:param name="program-notes-submitted"></xsl:param>
    <xsl:param name="public-collection-completed"></xsl:param>
    <xsl:param name="public-collection-scheduled"></xsl:param>
    <xsl:param name="public-collection-submitted"></xsl:param>
    <xsl:param name="reading-completed"></xsl:param>
    <xsl:param name="reading-scheduled"></xsl:param>
    <xsl:param name="reading-submitted"></xsl:param>
    <xsl:param name="recordings-completed"></xsl:param>
    <xsl:param name="recordings-scheduled"></xsl:param>
    <xsl:param name="recordings-submitted"></xsl:param>
    <xsl:param name="reproductions-completed"></xsl:param>
    <xsl:param name="reproductions-scheduled"></xsl:param>
    <xsl:param name="reproductions-submitted"></xsl:param>
    <xsl:param name="screening-completed"></xsl:param>
    <xsl:param name="screening-scheduled"></xsl:param>
    <xsl:param name="screening-submitted"></xsl:param>
    <xsl:param name="theatre-production-completed"></xsl:param>
    <xsl:param name="theatre-production-scheduled"></xsl:param>
    <xsl:param name="theatre-production-submitted"></xsl:param>
    <xsl:param name="curated-exhibition-completed"></xsl:param>
    <xsl:param name="curated-exhibition-scheduled"></xsl:param>
    <xsl:param name="curated-exhibition-submitted"></xsl:param>
    <xsl:param name="publicationevents-published"></xsl:param>
    <xsl:param name="publicationevents-in-press"></xsl:param>
    <xsl:param name="publicationevents-submitted"></xsl:param>
    <xsl:param name="reviews"></xsl:param>
    <xsl:param name="creative-activities-additional"></xsl:param> 
    
    <xsl:param name="service-header"></xsl:param>
    <xsl:param name="service-administrative"></xsl:param>
    <xsl:param name="service-department"></xsl:param>
    <xsl:param name="service-school"></xsl:param>
    <xsl:param name="service-campus"></xsl:param>
    <xsl:param name="service-systemwide"></xsl:param>
    <xsl:param name="service-other-university"></xsl:param>
    <xsl:param name="service-other-nonuniversity"></xsl:param>
    <xsl:param name="service-board"></xsl:param>
    <xsl:param name="service-additional"></xsl:param>
    <xsl:param name="teaching-header"></xsl:param>
    <xsl:param name="teaching-contact-hours"></xsl:param>
    <xsl:param name="teaching-courses"></xsl:param>
    <xsl:param name="teaching-curriculum"></xsl:param>
    <xsl:param name="teaching-supplement-term"></xsl:param>
    <xsl:param name="teaching-supplement-month"></xsl:param>
    <xsl:param name="teaching-special-advising"></xsl:param>
    <xsl:param name="teaching-student-advising"></xsl:param>
    <xsl:param name="teaching-thesis"></xsl:param>
    <xsl:param name="teaching-trainees"></xsl:param>
    <xsl:param name="teaching-extension"></xsl:param>
    <xsl:param name="teaching-additional"></xsl:param>
    
</xsl:stylesheet>