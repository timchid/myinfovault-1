<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE stylesheet [  
 <!ENTITY cr "&#x0A;">
 <!ENTITY space " ">
 <!ENTITY period ".">
 <!ENTITY comma ",">
 <!ENTITY colon ":">
 ]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
    xmlns:fo="http://www.w3.org/1999/XSL/Format">
    <xsl:template match="teaching-courses">
        <fo:block id="courses" space-before="12pt" margin-left="{$content.indent}">
            <xsl:call-template name="subsection-header">
              <xsl:with-param name="header"><xsl:value-of select="section-header"/></xsl:with-param>
            </xsl:call-template>
            <xsl:if test="course-record">
            <fo:block>
                <fo:table table-layout="fixed" width="100%" space-after="12pt">
                    <fo:table-column column-width="20%"/>
                    <fo:table-column column-width="80%"/>

                    <fo:table-body start-indent="0pt">
                        <xsl:apply-templates select="course-record"/>

                    </fo:table-body>
                </fo:table>
            </fo:block>
            </xsl:if>
        </fo:block>
    </xsl:template>

    <xsl:template match="course-record">
       <fo:table-row keep-together.within-page="1">
           <fo:table-cell>
               <fo:block>
                   <xsl:apply-templates select="year" mode="teaching"/>
               </fo:block>
           </fo:table-cell>
           <fo:table-cell>
               <fo:block>
                   <xsl:apply-templates select="description" mode="teaching-course"/>
                   <xsl:apply-templates select="coursenumber" mode="teaching-course"/>
                   <xsl:apply-templates select="title" mode="teaching-course"/>
                   <xsl:apply-templates select="units" mode="teaching-course"/>
                   <xsl:apply-templates select="undergraduatecount" mode="teaching-course"/>
                   <xsl:apply-templates select="graduatecount" mode="teaching-course"/>
                   <xsl:apply-templates select="effort" mode="teaching-course"/>
               </fo:block>
           </fo:table-cell>
       </fo:table-row>
    </xsl:template>
    
    <xsl:template match="description" mode="teaching-course">
      <fo:inline><xsl:apply-templates /></fo:inline>
   </xsl:template>
   <xsl:template match="coursenumber" mode="teaching-course">
      <fo:inline>&comma;&space;Course Number=<xsl:apply-templates /></fo:inline>
   </xsl:template>
   <xsl:template match="title" mode="teaching-course">
      <fo:inline>&comma;&space;<xsl:apply-templates /></fo:inline>
   </xsl:template>
   <xsl:template match="units" mode="teaching-course">
      <fo:inline>&comma;&space;Units=<xsl:apply-templates /></fo:inline>
   </xsl:template>
   <xsl:template match="undergraduatecount" mode="teaching-course">
      <fo:inline>&comma;&space;Undergraduate Count=<xsl:apply-templates /></fo:inline>
   </xsl:template>
   <xsl:template match="graduatecount" mode="teaching-course">
      <fo:inline>&comma;&space;Graduate Count=<xsl:apply-templates /></fo:inline>
   </xsl:template>
   <xsl:template match="effort" mode="teaching-course">
      <fo:inline>&comma;&space;Percentage Effort=<xsl:apply-templates /></fo:inline>
   </xsl:template>

</xsl:stylesheet>
