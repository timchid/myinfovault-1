<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE stylesheet [  
 <!ENTITY cr "&#x0A;">
 <!ENTITY space " ">
 <!ENTITY period ".">
 <!ENTITY comma ",">
 <!ENTITY qmark "?">
 <!ENTITY expoint "!">
 ]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
    xmlns:fo="http://www.w3.org/1999/XSL/Format">
    
    <xsl:template match="extending-knowledge-gathering">
        <fo:block id="extending-knowledge-gathering" space-before="12pt" margin-left="{$content.indent}"> 
            <xsl:call-template name="subsection-header">
              <xsl:with-param name="header"><xsl:value-of select="section-header"/></xsl:with-param>
            </xsl:call-template>
            <xsl:if test="knowledge-gathering-record">
            <fo:block keep-with-next="auto">
                <fo:table table-layout="fixed" width="100%">
                    <fo:table-column column-width="5%" />
                    <fo:table-column column-width="95%" />
                    <fo:table-body start-indent="0pt">                   
                        <xsl:apply-templates select="knowledge-gathering-record"/>
                    </fo:table-body>
                </fo:table>
            </fo:block>
            </xsl:if>
        </fo:block>
    </xsl:template>
    
    <xsl:template match="knowledge-gathering-record">        
        <fo:table-row>
            <fo:table-cell>
                <fo:block>
                    <xsl:number />.
                </fo:block>
            </fo:table-cell>
            <fo:table-cell>
              <fo:block>
                    <xsl:apply-templates select="title" mode="knowledge"/>
                    <xsl:apply-templates select="name" mode="knowledge"/>
                    <xsl:apply-templates select="audience" mode="knowledge"/>
                    <xsl:apply-templates select="location" mode="knowledge"/>
                    <xsl:apply-templates select="datespan" mode="knowledge"/>
                    <xsl:apply-templates select="headcount" mode="knowledge"/>                                
                <xsl:call-template name="addPunctuation">
                   <xsl:with-param name="value"><xsl:value-of select="."/></xsl:with-param>
                   <xsl:with-param name="punctuationValue">&period;</xsl:with-param>
                   <xsl:with-param name="followWith">&space;</xsl:with-param>
                </xsl:call-template>
              </fo:block>
            </fo:table-cell>
        </fo:table-row>
    </xsl:template>
</xsl:stylesheet>