<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
    xmlns:fo="http://www.w3.org/1999/XSL/Format">

    <xsl:template name="committee-records">
        <fo:block margin-bottom="12pt">
                        <fo:table table-layout="fixed" width="100%">
                                <fo:table-column column-width="20%"/>
                                <fo:table-column column-width="80%"/>
                                <fo:table-body start-indent="0pt">
                                        <xsl:apply-templates select="committee-record"/>
                                </fo:table-body>
                         </fo:table>
             </fo:block>
    </xsl:template>
    
    <xsl:template match="committee-record">
        <fo:table-row>
             <fo:table-cell>
                 <fo:block>
                     <xsl:apply-templates select="year"/>
                 </fo:block>
             </fo:table-cell>
             <fo:table-cell>
                 <fo:block>
                     <xsl:apply-templates select="role"/>
                     <xsl:apply-templates select="description"/>
                 </fo:block>
             </fo:table-cell>
         </fo:table-row>
    </xsl:template>

    <xsl:template match="role">
        <fo:inline><xsl:apply-templates/>&#160;- </fo:inline>
    </xsl:template>

</xsl:stylesheet>
