<?xml version="1.0" encoding="UTF-8"?>

<!DOCTYPE stylesheet [  
 <!ENTITY space " ">
 <!ENTITY comma ",">
 <!ENTITY period ".">
 <!ENTITY singlequote "'''">
 ]>
 
<xsl:stylesheet version="1.0"
   xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
   xmlns:fo="http://www.w3.org/1999/XSL/Format">

   <xsl:import href="html-tags.xslt"/>
   <xsl:import href="mivCommon.xslt"/>
  
  <!-- Define the document name. Note: Numeric entity for single quote is used for appostrophe! -->
  <xsl:variable name="documentName" select="concat('Candidate','&#8217;','s Diversity Statement')"/>
  
  <!-- document setup is done in mivCommon.xslt -->
  <xsl:template match="/">
    <xsl:call-template name="documentSetup">
      <xsl:with-param name="documentName" select="$documentName" />
      <xsl:with-param name="hasSections" select="//diversity-statement" />
    </xsl:call-template>
  </xsl:template>
   
   <xsl:template match="packet">
    <xsl:if test="count(descendant::diversity-statement) &gt; 0">
      <xsl:call-template name='maintitle'>
        <xsl:with-param name="documentName" select="$documentName" />
      </xsl:call-template>
      <xsl:call-template name="nameblock"/>
    <xsl:apply-templates select="diversity-statement"/>
	  </xsl:if>
   </xsl:template>
 
   <xsl:template match="diversity-statement">         
      <fo:block id="diversity-statement">
         <xsl:apply-templates select="diversitystatement-record" />         
      </fo:block>
   </xsl:template>
  
   <!-- CANDIDATE'S DIVERSITY STATEMENT -->
   <xsl:template match="diversitystatement-record">
      <fo:block padding-bottom="12pt">
          <xsl:apply-templates select="teachingcontent" />
          <xsl:apply-templates select="servicecontent" />
          <xsl:apply-templates select="researchcontent" />
      </fo:block>
   </xsl:template>
   
   <xsl:template match="teachingcontent">
   	  <fo:block font-weight="bold">
        <xsl:text>Description/Elaboration of Diversity Activities in Teaching</xsl:text>
      </fo:block>
      <fo:block space-after="16pt" margin-left="16pt">
        <xsl:apply-templates />
      </fo:block>
   </xsl:template>
   
   <xsl:template match="servicecontent">
   	  <fo:block font-weight="bold">
        <xsl:text>Description/Elaboration of Diversity Activities in University and Public Service</xsl:text>
      </fo:block>
      <fo:block space-after="16pt" margin-left="16pt">
        <xsl:apply-templates />
      </fo:block>
   </xsl:template>
   
   <xsl:template match="researchcontent">
   	  <fo:block font-weight="bold">
        <xsl:text>Description/Elaboration of Diversity Activities in Scholarly and Creative Activities</xsl:text>
      </fo:block>
      <fo:block space-after="16pt" margin-left="16pt">
        <xsl:apply-templates />
      </fo:block>
   </xsl:template>
  
</xsl:stylesheet>