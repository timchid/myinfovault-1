<?xml version="1.0" encoding="UTF-8"?>

<!DOCTYPE stylesheet [
 <!ENTITY period ".">
 ]> 

<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format">
	
	<xsl:template name="footnotes">
		<xsl:if test="descendant::footnote">
			<fo:block font-size="8pt" font-style="italic"
				space-after="8pt">
				<fo:table table-layout="fixed" width="100%">
					<fo:table-column column-width="100%" />
					<fo:table-body>
						<fo:table-row>
							<fo:table-cell>
								<fo:block font-weight="bold">Footnotes:</fo:block>
							</fo:table-cell>
						</fo:table-row>						
						<fo:table-row>
							<fo:table-cell>
								<fo:table table-layout="fixed" width="100%">
									<fo:table-column column-width="5%" />
									<fo:table-column column-width="95%" />
									<fo:table-body>				
										<xsl:for-each select="*/descendant::footnote">
											<fo:table-row>
												<fo:table-cell>
													<fo:block>#<xsl:value-of select="../number" />&period;</fo:block>
												</fo:table-cell>	
												<fo:table-cell>
													<fo:block>
														<fo:inline space-start="0.5em">
															<xsl:apply-templates />
														</fo:inline>&period;													
													</fo:block>
												</fo:table-cell>
											</fo:table-row>				
										</xsl:for-each>
									</fo:table-body>
								</fo:table>					
							</fo:table-cell>
						</fo:table-row>
					</fo:table-body>
				</fo:table>
			</fo:block>
		</xsl:if>
	</xsl:template>
	
	<xsl:template match="annotations-legends">
		<fo:block font-size="8pt" font-style="italic">
			<fo:table table-layout="fixed" width="100%">
				<fo:table-column column-width="100%" />
				<fo:table-body>
					<fo:table-row>
						<fo:table-cell>
							<fo:block font-weight="bold">Notations:</fo:block>
							<xsl:apply-templates select="legend" />
						</fo:table-cell>
					</fo:table-row>
				</fo:table-body>
			</fo:table>
		</fo:block>
	</xsl:template>
	
	<xsl:template match="legend">
		<fo:block>
			<xsl:apply-templates />
		</fo:block>
	</xsl:template>
	
	<xsl:template name="label">
		<xsl:param name="rightalign" />
		<xsl:param name="displaybefore" />
		<xsl:variable name="align">
			<xsl:choose>
				<xsl:when test="$rightalign='1' or $rightalign='true'">right</xsl:when>
				<xsl:otherwise>left</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:choose>
			<xsl:when test="$displaybefore='1'">
				<fo:block font-weight="bold" text-align="{$align}" border-bottom="1px solid black">
					<fo:inline>
						<xsl:apply-templates select="labelbefore"/>
					</fo:inline>	
				</fo:block>
			</xsl:when>
			<xsl:otherwise>
				<fo:block font-weight="bold" text-align="{$align}" border-bottom="1px solid black">
					<fo:inline>
						<xsl:apply-templates select="labelafter"/>
					</fo:inline>	
				</fo:block>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	
	<xsl:template match="notation">
		<fo:inline>
			<xsl:apply-templates />
		</fo:inline>
	</xsl:template>	
	
	<xsl:template match="number">
		<fo:inline>
			<xsl:apply-templates />
		</fo:inline>
	</xsl:template>
	
</xsl:stylesheet>	