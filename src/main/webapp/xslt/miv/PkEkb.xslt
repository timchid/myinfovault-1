<?xml version="1.0" encoding="UTF-8"?>

<!DOCTYPE stylesheet [  
 <!ENTITY space " ">
 <!ENTITY comma ",">
 <!ENTITY period ".">
 ]>

<xsl:stylesheet version="1.0"
   xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
   xmlns:fo="http://www.w3.org/1999/XSL/Format">
   
   <xsl:import href="html-tags.xslt"/>
   <xsl:import href="mivCommon.xslt"/>
   <xsl:import href="annotations.xslt"/>
   
    <!-- Define the document name. -->
    <xsl:variable name="documentName" select="'Extending Knowledge'"/>
  
    <!-- Global variable to identify if there is data other than pdf uploads. -->
    <xsl:variable name="hasRealReports">
        <xsl:choose>
            <xsl:when
              test="/*/extending-knowledge-media | /*/extending-knowledge-gathering |  /*/extending-knowledge-other" >true</xsl:when>
            <xsl:otherwise>false</xsl:otherwise>
        </xsl:choose>
    </xsl:variable>    
  
    <!-- document setup is done in mivCommon.xslt -->
    <xsl:template match="/">
    	<xsl:if test="$hasRealReports='true'">
    		<xsl:call-template name="documentSetup">
        		<xsl:with-param name="documentName" select="$documentName" />
        		<xsl:with-param name="hasSections" select="$hasRealReports" />
      		</xsl:call-template>
    	</xsl:if>
    </xsl:template>

   
   <xsl:template match="packet">
   <xsl:if test="$hasRealReports='true'">
     <xsl:call-template name='maintitle'>
        <xsl:with-param name="documentName" select="$documentName" />
      </xsl:call-template>
      <xsl:call-template name="nameblock"/>
      <fo:block id="main" font-size="12pt" font-family="Helvetica">
         <xsl:apply-templates select="extending-knowledge-media"/>
         <xsl:apply-templates select="extending-knowledge-gathering"/>
         <xsl:apply-templates select="extending-knowledge-other"/>
      </fo:block>
   </xsl:if>
   </xsl:template>
   
   <xsl:template match="extending-knowledge-media">
       <xsl:choose>
           <xsl:when test="following-sibling::extending-knowledge-gathering | following-sibling::extending-knowledge-other">
               <fo:block break-after="page">
                   <fo:block id="extending-knowledge-media">
                  <!-- <fo:block font-weight="bold" text-decoration="underline" space-after="12pt">Broadcast, Print, or Electronic Media</fo:block> -->
	               <fo:block white-space-collapse="false" font-weight="bold" text-decoration="underline" space-after="12pt"><xsl:value-of select="section-header"/></fo:block>
                       <xsl:apply-templates select="knowledge-media-record" />
                   </fo:block>
               </fo:block>
           </xsl:when>
           <xsl:otherwise>
               <fo:block id="extending-knowledge-media">
              <!-- <fo:block font-weight="bold" text-decoration="underline" space-after="12pt">Broadcast, Print, or Electronic Media</fo:block> -->
	           <fo:block white-space-collapse="false" font-weight="bold" text-decoration="underline" space-after="12pt"><xsl:value-of select="section-header"/></fo:block>
                   <xsl:apply-templates select="knowledge-media-record" />
               </fo:block>
           </xsl:otherwise>
       </xsl:choose>
   </xsl:template>

   <xsl:template match="extending-knowledge-gathering">
       <xsl:choose>
           <xsl:when test="following-sibling::extending-knowledge-other">
               <fo:block break-after="page">
                   <fo:block id="extending-knowledge-gathering">
		  <!-- <fo:block font-weight="bold" text-decoration="underline" space-after="12pt">Workshops, Conferences, Presentations and Short Courses</fo:block> -->
		       <fo:block white-space-collapse="false" font-weight="bold" text-decoration="underline" space-after="12pt"><xsl:value-of select="section-header"/></fo:block>
                       <xsl:apply-templates select="knowledge-gathering-record" />
                   </fo:block>
               </fo:block>
           </xsl:when>
           <xsl:otherwise>
               <fo:block id="extending-knowledge-gathering">
              <!-- <fo:block font-weight="bold" text-decoration="underline" space-after="12pt">Workshops, Conferences, Presentations and Short Courses</fo:block> -->
		   <fo:block white-space-collapse="false" font-weight="bold" text-decoration="underline" space-after="12pt"><xsl:value-of select="section-header"/></fo:block>
                   <xsl:apply-templates select="knowledge-gathering-record" />
               </fo:block>
           </xsl:otherwise>
       </xsl:choose>
   </xsl:template>

   <xsl:template match="extending-knowledge-other">
      <fo:block id="extending-knowledge-other">
     <!-- <fo:block font-weight="bold" text-decoration="underline" space-after="12pt">Other</fo:block> -->
          <fo:block white-space-collapse="false" font-weight="bold" text-decoration="underline" space-after="12pt"><xsl:value-of select="section-header"/></fo:block>
          <xsl:apply-templates select="knowledge-other-record"/>
      </fo:block>
   </xsl:template>
   
   <xsl:template match="knowledge-media-record">
      <fo:block padding-bottom="12pt" keep-with-next="auto">
      <fo:table table-layout="fixed" width="100%">
	<fo:table-column column-width="100%" />
	<fo:table-body>
		<xsl:if test="./labelbefore">
		<fo:table-row keep-with-next="always">
			<fo:table-cell>
				<fo:block>
					<xsl:call-template name="label">
						<xsl:with-param name="rightalign" select="./labelbefore/@rightjustify" />
						<xsl:with-param name="displaybefore" select="1" />
					</xsl:call-template>
				</fo:block>
			</fo:table-cell>
		</fo:table-row>
		</xsl:if>
		<fo:table-row>
			<fo:table-cell>
		         <fo:table table-layout="fixed" width="100%">
		            <fo:table-column column-width="5%" />
		            <fo:table-column column-width="95%" />
		            <fo:table-body>
		               <fo:table-row>
		                  <fo:table-cell padding="2pt">
		                    <fo:block><xsl:number />&period;</fo:block>	
		                  </fo:table-cell>
		                  <fo:table-cell padding="2pt">
		                     <fo:block>
		                        <xsl:apply-templates select="title"/>
		                        <xsl:apply-templates select="description"/>
		                        <xsl:apply-templates select="datespan"/>
		                        <xsl:apply-templates select="publisher"/>
		                       <!-- Build a string of all fields before applying ending punctuation -->
		                       <!-- since some fields may not be present -->
		                        <xsl:variable name="fieldstr">
		                          <xsl:value-of select="title" />
		                          <xsl:value-of select="description" />
		                          <xsl:value-of select="datespan" />
		                          <xsl:value-of select="publisher" />
		                        </xsl:variable>
		                        <xsl:call-template name="addPunctuation">
		                          <xsl:with-param name="value" select="$fieldstr" />
		                          <xsl:with-param name="punctuationValue" select="'&period;'"/>
		                          <xsl:with-param name="followWith" select="'&space;'"/>
		                        </xsl:call-template>
		                     </fo:block>
		                  </fo:table-cell>
		               </fo:table-row>
		            </fo:table-body>
		         </fo:table>
		     </fo:table-cell>
		</fo:table-row>
		<xsl:if test="./labelafter">
		<fo:table-row keep-with-previous="always">
			<fo:table-cell>
				<fo:block>
					<xsl:call-template name="label">
						<xsl:with-param name="rightalign" select="./labelafter/@rightjustify" />
						<xsl:with-param name="displaybefore" select="0" />
					</xsl:call-template>
				</fo:block>
			</fo:table-cell>
		</fo:table-row>
		</xsl:if>
	</fo:table-body>
</fo:table>
      </fo:block>
   </xsl:template>
   
   <xsl:template match="knowledge-gathering-record">
      <fo:block padding-bottom="12pt" keep-with-next="auto">
      <fo:table table-layout="fixed" width="100%">
	<fo:table-column column-width="100%" />
	<fo:table-body>
		<xsl:if test="./labelbefore">
		<fo:table-row keep-with-next="always">
			<fo:table-cell>
				<fo:block>
					<xsl:call-template name="label">
						<xsl:with-param name="rightalign" select="./labelbefore/@rightjustify" />
						<xsl:with-param name="displaybefore" select="1" />
					</xsl:call-template>
				</fo:block>
			</fo:table-cell>
		</fo:table-row>
		</xsl:if>
		<fo:table-row>
			<fo:table-cell>
		         <fo:table table-layout="fixed" width="100%">
		            <fo:table-column column-width="5%" />
		            <fo:table-column column-width="95%" />
		            <fo:table-body>
		               <fo:table-row>
		                  <fo:table-cell padding="2pt">
		                    <fo:block><xsl:number />&period;</fo:block>	
		                  </fo:table-cell>
		                  <fo:table-cell padding="2pt">
		                     <fo:block>
		                       <xsl:apply-templates select="title"/>
		                       <xsl:apply-templates select="name"/>
		                       <xsl:apply-templates select="audience"/>
		                       <xsl:apply-templates select="location"/>
		                       <xsl:apply-templates select="datespan"/>
		                       <xsl:apply-templates select="headcount"/>
		                       <!-- Build a string of all fields before applying ending punctuation -->
		                       <!-- since some fields may not be present -->
		                       <xsl:variable name="fieldstr">
		                         <xsl:value-of select="title" />
		                         <xsl:value-of select="name" />
		                         <xsl:value-of select="audience" />
		                         <xsl:value-of select="location" />
		                         <xsl:value-of select="datespan" />
		                         <xsl:value-of select="headcount" />
		                       </xsl:variable>
		                       <xsl:call-template name="addPunctuation">
		                         <xsl:with-param name="value" select="$fieldstr" />
		                         <xsl:with-param name="punctuationValue" select="'&period;'"/>
		                         <xsl:with-param name="followWith" select="'&space;'"/>
		                       </xsl:call-template>
		                     </fo:block>
		                  </fo:table-cell>
		               </fo:table-row>
		            </fo:table-body>
		         </fo:table>
		     </fo:table-cell>
		</fo:table-row>
		<xsl:if test="./labelafter">
		<fo:table-row keep-with-previous="always">
			<fo:table-cell>
				<fo:block>
					<xsl:call-template name="label">
						<xsl:with-param name="rightalign" select="./labelafter/@rightjustify" />
						<xsl:with-param name="displaybefore" select="0" />
					</xsl:call-template>
				</fo:block>
			</fo:table-cell>
		</fo:table-row>
		</xsl:if>
	</fo:table-body>
</fo:table>
      </fo:block>
   </xsl:template>
   
   <xsl:template match="knowledge-other-record">
      <fo:block padding-bottom="12pt" keep-with-next="auto">
      <fo:table table-layout="fixed" width="100%">
	<fo:table-column column-width="100%" />
	<fo:table-body>
		<xsl:if test="./labelbefore">
		<fo:table-row keep-with-next="always">
			<fo:table-cell>
				<fo:block>
					<xsl:call-template name="label">
						<xsl:with-param name="rightalign" select="./labelbefore/@rightjustify" />
						<xsl:with-param name="displaybefore" select="1" />
					</xsl:call-template>
				</fo:block>
			</fo:table-cell>
		</fo:table-row>
		</xsl:if>
		<fo:table-row>
			<fo:table-cell>
		         <fo:table table-layout="fixed" width="100%">
		            <fo:table-column column-width="15%" />
		            <fo:table-column column-width="85%" />
		            <fo:table-body>
		               <fo:table-row>
		                  <fo:table-cell padding="2pt">
		                    	<fo:block><xsl:number />&period;</fo:block>						
		                  </fo:table-cell>
		                  <fo:table-cell padding="2pt">
		                     <fo:block>
		                       <xsl:apply-templates select="datespan"/>
		                       <xsl:apply-templates select="comment"/>
		                       <!-- Build a string of all fields before applying ending punctuation -->
		                       <!-- since some fields may not be present -->
		                       <xsl:variable name="fieldstr">
		                         <xsl:value-of select="datespan" />
		                         <xsl:value-of select="comment" />
		                       </xsl:variable>
		                       <xsl:call-template name="addPunctuation">
		                         <xsl:with-param name="value" select="$fieldstr" />
		                         <xsl:with-param name="punctuationValue" select="'&period;'"/>
		                         <xsl:with-param name="followWith" select="'&space;'"/>
		                       </xsl:call-template>
		                     </fo:block>
		                  </fo:table-cell>
		               </fo:table-row>
		            </fo:table-body>
		         </fo:table>
			</fo:table-cell>
		</fo:table-row>
		<xsl:if test="./labelafter">
		<fo:table-row keep-with-previous="always">
			<fo:table-cell>
				<fo:block>
					<xsl:call-template name="label">
						<xsl:with-param name="rightalign" select="./labelafter/@rightjustify" />
						<xsl:with-param name="displaybefore" select="0" />
					</xsl:call-template>
				</fo:block>
			</fo:table-cell>
		</fo:table-row>
		</xsl:if>
	</fo:table-body>
	</fo:table>				    
      </fo:block>
   </xsl:template>
   
   <xsl:template match="title">
      <fo:inline><xsl:apply-templates /></fo:inline>
   </xsl:template>
   
   <xsl:template match="description|publisher|name|audience|location|comment">
      <fo:inline>&comma;&space;<xsl:apply-templates /></fo:inline>
   </xsl:template>
   
   <xsl:template match="datespan">
       <xsl:choose>
           <xsl:when test="parent::knowledge-other-record"><xsl:apply-templates /></xsl:when>
           <xsl:otherwise>&comma;&space;<xsl:apply-templates /></xsl:otherwise>
       </xsl:choose>
   </xsl:template>

   <xsl:template match="headcount">
      <fo:inline>&comma;&space;<xsl:apply-templates /> Attendees</fo:inline>
   </xsl:template>
</xsl:stylesheet>
