<?xml version="1.0" encoding="UTF-8"?>

<!DOCTYPE stylesheet [  
 <!ENTITY percent "&#37;">
  ]>


<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format">

	<xsl:import href="html-tags.xslt" />
	<xsl:import href="mivCommon.xslt" />

	<!-- Define the document name. -->
  <xsl:variable name="documentName" select="'Position Description'"/>
  
  <!-- document setup is done in mivCommon.xslt -->
  <xsl:template match="/">
    <xsl:call-template name="documentSetup">
    <xsl:with-param name="documentName" select="$documentName" />
    <xsl:with-param name="hasSections" select="//position-description" />
  </xsl:call-template>  </xsl:template>
  

  <xsl:template match="packet">
  	<xsl:if test="count(descendant::position-description) &gt; 0">
     <xsl:call-template name='maintitle'>
        <xsl:with-param name="documentName" select="$documentName" />
      </xsl:call-template>
      <xsl:call-template name="nameblock"/>
      <fo:block><xsl:apply-templates select="position-description"/></fo:block>
     </xsl:if> 
   </xsl:template>

	<xsl:template match="position-description">
		<fo:block id="position-description">
			<fo:table table-layout="fixed" width="100%">
				<fo:table-column column-width="15%" />
				<fo:table-column column-width="85%" />
				<fo:table-body>
					<fo:table-row space-after="12pt" font-weight="bold"
						text-decoration="underline">
						<fo:table-cell padding="5px">
							<fo:block>Time</fo:block>
						</fo:table-cell>
						<fo:table-cell padding="5px">
							<fo:block>Description</fo:block>
						</fo:table-cell>
					</fo:table-row>
					<xsl:apply-templates select="position-record" />
				</fo:table-body>
			</fo:table>
		</fo:block>
	</xsl:template>

	<!-- POSITION DESCRIPTION -->
	<xsl:template match="position-record">
		<fo:table-row space-after="12pt" keep-with-previous="1">
			<fo:table-cell padding="5px">
				<fo:block>
					<xsl:apply-templates select="percenttime" />
				</fo:block>
			</fo:table-cell>
			<fo:table-cell padding="5px">
				<fo:block>
					<xsl:apply-templates select="pdcontent" />
				</fo:block>
			</fo:table-cell>
		</fo:table-row>
	</xsl:template>

	<xsl:template match="percenttime">
		<fo:block><xsl:apply-templates />&percent;</fo:block>
	</xsl:template>

	<xsl:template match="pdcontent">
		<fo:block>
			<xsl:apply-templates />
		</fo:block>
	</xsl:template>

   <!-- Don't define a table width for tables which may be nested in this two column format -->
   <xsl:template match="tbody">
   <fo:table table-layout="fixed" width="100%">
      <xsl:attribute name="space-after">10pt</xsl:attribute>
    <xsl:for-each select="tr[1]/th|tr[1]/td">
      <fo:table-column>
       <xsl:if test="@width">
         <xsl:attribute name="column-width">    
              <xsl:value-of select="@width"/>
        </xsl:attribute>
      </xsl:if>
      </fo:table-column>  
      </xsl:for-each>       
    <fo:table-body>
      <xsl:apply-templates />
    </fo:table-body>  
   </fo:table>   
   </xsl:template>

</xsl:stylesheet>