<?xml version="1.0" encoding="UTF-8"?>

<!DOCTYPE stylesheet [
 <!ENTITY space " ">
 <!ENTITY comma ",">
 <!ENTITY colon ":">
 ]>

<xsl:stylesheet version="1.0"
   xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
   xmlns:fo="http://www.w3.org/1999/XSL/Format">

   <xsl:import href="html-tags.xslt"/>
   <xsl:import href="mivCommon.xslt"/>


   <!-- Define the document name. -->
  <xsl:variable name="documentName" select="'Teaching, Advising and Curricular Development'"/>
  
  <xsl:variable name="title1">
  <xsl:choose>
    <xsl:when test="/*/teaching-supplement-term" >Teaching Supplement by Term</xsl:when>
    <xsl:otherwise></xsl:otherwise>
  </xsl:choose>
  </xsl:variable>
  
  <xsl:variable name="title2">
  <xsl:choose>
    <xsl:when test="/*/teaching-supplement-month" >Teaching Supplement by Month</xsl:when>
    <xsl:otherwise></xsl:otherwise>
  </xsl:choose>
  </xsl:variable>
  
  <xsl:variable name="titleAdditional"> 
    <xsl:apply-templates select="//teaching-additional/section-header"/>
  </xsl:variable>
  
  
  
  <!-- Define headers for all the sections -->
  <xsl:variable name="courseHeader"
                select="'Course load carried by candidate during the review period. Please indicate fraction of course (if less than
               100%) for which the candidate is responsible. Attaching IAIS report(s) of courses taught during the review
               period instead of completing this section is acceptable.'"/>

  <xsl:variable name="extensionHeader"
                select="'University Extension teaching load carried by the candidate during the review period.'"/>

  <xsl:variable name="studentAdvisingHeader"
                select="'Student Advising: Indicate average number of advisees during each year of the review period.'"/>

  <xsl:variable name="specialAdvisingHeader"
                select="'Indicate below any special advising responsibilities during the review period (i.e. master advisor, Chair of
                advising committee, etc).'"/>

  <xsl:variable name="curriculumHeader"
                select="'Curricular Development: Please briefly describe the contributions of the candidate to instructional
                improvement and curricular development.'"/>

  <xsl:variable name="hoursHeader"
                select="'Student/Resident Contact Hours per Quarter.'"/>

  <xsl:variable name="traineeHeader"
                select="'List of Trainees.'"/>
 
  <!-- Global variables to identify if a header page/bookmark is needed(i.e) if there is data other than additional information. -->
  <xsl:variable name="hasHeaderpage" select="/*/teaching-courses | /*/teaching-thesis | /*/teaching-extension | /*/teaching-student-advising | /*/teaching-special-advising | /*/teaching-curriculum
      | /*/teaching-contact-hours | /*/teaching-trainees"/>
  
  <xsl:variable name="hasSupplementByMonth" select="/*/teaching-supplement-month"/>
  
  <xsl:variable name="hasSupplementByTerm" select="/*/teaching-supplement-term"/>

  <xsl:variable name="hasAdditional" select="/*/teaching-additional"/>
  
  <!-- document setup is done in mivCommon.xslt -->
  <xsl:template match="/">
        <xsl:choose>
                <xsl:when test="not($hasHeaderpage)">
  		        <xsl:call-template name="documentSetup">
		                <xsl:with-param name="documentName" select="''" />
		                <xsl:with-param name="title1" select="$title1" />
		                <xsl:with-param name="title2" select="$title2" />
		                <xsl:with-param name="titleAdditional" select="$titleAdditional" />
                                <xsl:with-param name="documentFooter" select="$documentName" />
                                <xsl:with-param name="hasSections" select="not($hasHeaderpage)" />
		        </xsl:call-template>
		</xsl:when>        
                <xsl:otherwise>
                        <xsl:call-template name="documentSetup">
		                <xsl:with-param name="documentName" select="$documentName" />
		                <xsl:with-param name="title1" select="$title1" />
		                <xsl:with-param name="title2" select="$title2" />
		                <xsl:with-param name="titleAdditional" select="$titleAdditional" />
                                <xsl:with-param name="documentFooter" select="$documentName" />
                                <xsl:with-param name="hasSections" select="$hasHeaderpage" />
		        </xsl:call-template>
                </xsl:otherwise>
        </xsl:choose>
  </xsl:template>
  
  <xsl:template match="desii-upload">         
      <fo:block id="desii-upload">
        <xsl:apply-templates/>
      </fo:block>
   </xsl:template>

 <xsl:template match="packet">
    <xsl:choose>
        <!-- displaying document name when $hasHeaderpage not exist -->
        <xsl:when test="not($hasHeaderpage)">
            <xsl:call-template name='maintitle'>
                <xsl:with-param name="documentName" select="$documentName" />
            </xsl:call-template>
        </xsl:when>
        <!-- otherwise -->
        <xsl:otherwise>
            <xsl:call-template name='maintitle'>
                <xsl:with-param name="documentName" select="$documentName" />
            </xsl:call-template>
            <fo:block font-size="16pt" font-weight="bold" text-align="center"
                space-after="12pt">
                <fo:table table-layout="fixed" width="100%"
                    space-after="24pt">
                    <fo:table-column column-width="35%" />
                    <fo:table-column column-width="65%" />
                    <fo:table-body>
                        <fo:table-row>
                            <fo:table-cell>
                                <fo:block font-size="12pt" text-align="left">
                                    <fo:inline font-weight="bold">Name:&space;
                                    </fo:inline>
                                    <fo:inline border-bottom="thin solid black">
                                        <xsl:apply-templates
                                            select="fname" />
                                        <xsl:apply-templates
                                            select="lname" />
                                    </fo:inline>
                                </fo:block>
                            </fo:table-cell>
                            <fo:table-cell>
                                <fo:block font-size="12pt" text-align="left">
                                    <fo:inline font-weight="bold">Department:&space;
                                    </fo:inline>
                                    <fo:inline border-bottom="thin solid black">
                                        <xsl:apply-templates
                                            select="department" />
                                    </fo:inline>
                                </fo:block>
                            </fo:table-cell>
                        </fo:table-row>
                    </fo:table-body>
                </fo:table>
            </fo:block>   
        </xsl:otherwise>
     </xsl:choose>
 
      <fo:block><xsl:call-template name="main"/></fo:block>
   </xsl:template>

   <xsl:template name="sectionHeader">
     <xsl:param name="number" />
     <xsl:param name="header" />
     <fo:block>
     <fo:table table-layout="fixed" width="100%" space-after="10pt">
       <fo:table-column column-width="5%" />
       <fo:table-column column-width="95%" />
        <fo:table-body>
          <fo:table-row>
          <fo:table-cell>
             <fo:block><xsl:value-of select="$number" /></fo:block>
          </fo:table-cell>
          <fo:table-cell>
             <fo:block><xsl:value-of select="$header" /></fo:block>
           </fo:table-cell>
           </fo:table-row>
         </fo:table-body>
       </fo:table>
     </fo:block>
   </xsl:template>


   <xsl:template name="main">
     <fo:block id="main" font-size="10pt">
     <xsl:if test="$hasHeaderpage">
      <xsl:call-template name="sectionHeader">
        <xsl:with-param name="number" select="'1.'" />
        <xsl:with-param name="header" select="$courseHeader" />
      </xsl:call-template>
      <xsl:apply-templates select="teaching-courses" />
      <fo:table table-layout="fixed" width="100%" space-after="10pt">
        <fo:table-column column-width="5%" />
        <fo:table-column column-width="95%" />
        <fo:table-body>
           <fo:table-row>
              <fo:table-cell>
                 <fo:block>2.</fo:block>
              </fo:table-cell>
              <fo:table-cell>
                 <fo:block>
                 Name of student(s) of thesis committee(s) on which the candidate
                 <fo:inline text-decoration="underline">has served</fo:inline>
                 or
                 <fo:inline text-decoration="underline">is serving</fo:inline>
                 during the review period. Please include only formal membership on
                 committees guiding thesis research. If a Master's candidate is in a department requiring a field study
                 or research paper in lieu of a thesis, the name of such student(s) supervised should be indicated.
                 </fo:block>
              </fo:table-cell>
           </fo:table-row>
        </fo:table-body>
      </fo:table>
      <xsl:apply-templates select="teaching-thesis" />
      <xsl:call-template name="sectionHeader">
        <xsl:with-param name="number" select="'3.'" />
        <xsl:with-param name="header" select="$extensionHeader" />
      </xsl:call-template>
      <xsl:apply-templates select="teaching-extension" />
      <xsl:call-template name="sectionHeader">
        <xsl:with-param name="number" select="'4.'" />
        <xsl:with-param name="header" select="$studentAdvisingHeader" />
      </xsl:call-template>
      <xsl:apply-templates select="teaching-student-advising" />
      <xsl:call-template name="sectionHeader">
        <xsl:with-param name="number" select="'5.'" />
        <xsl:with-param name="header" select="$specialAdvisingHeader" />
      </xsl:call-template>
      <xsl:apply-templates select="teaching-special-advising" />
      <xsl:call-template name="sectionHeader">
        <xsl:with-param name="number" select="'6.'" />
        <xsl:with-param name="header" select="$curriculumHeader" />
      </xsl:call-template>
      <xsl:apply-templates select="teaching-curriculum" />
      <xsl:call-template name="sectionHeader">
        <xsl:with-param name="number" select="'7.'" />
        <xsl:with-param name="header" select="$hoursHeader" />
      </xsl:call-template>
      <xsl:apply-templates select="teaching-contact-hours" />
      <xsl:call-template name="sectionHeader">
        <xsl:with-param name="number" select="'8.'" />
        <xsl:with-param name="header" select="$traineeHeader" />
      </xsl:call-template>
      <xsl:apply-templates select="teaching-trainees" />
    </xsl:if>	


      <xsl:apply-templates select="teaching-supplement-term"/>
      <xsl:apply-templates select="teaching-supplement-month"/>
      <xsl:apply-templates select="teaching-additional"/>
     </fo:block>
   </xsl:template>

   <!-- COURSES -->

   <xsl:template match="teaching-courses">
    <fo:block id="teaching-courses" padding-bottom="1em">
     <fo:table table-layout="fixed" width="100%" font-size="10pt" space-after="10pt">
        <fo:table-column column-width="10%" />
        <fo:table-column column-width="20%" />
        <fo:table-column column-width="15%" />
        <fo:table-column column-width="30%" />
        <fo:table-column column-width="5%" />
        <fo:table-column column-width="5%" />
        <fo:table-column column-width="5%" />
        <fo:table-column column-width="10%" />
        <fo:table-header>
           <fo:table-row font-weight="bold">
              <fo:table-cell border-bottom="thin solid black" padding="2px" text-align="left">
                 <fo:block>Year</fo:block>
              </fo:table-cell>
              <fo:table-cell border-bottom="thin solid black" padding="2px" text-align="left">
                 <fo:block>Term</fo:block>
              </fo:table-cell>
              <fo:table-cell border-bottom="thin solid black" padding="2px" text-align="left">
                 <fo:block>Course No.</fo:block>
              </fo:table-cell>
              <fo:table-cell border-bottom="thin solid black" padding="2px" text-align="left">
                 <fo:block>Course Title</fo:block>
              </fo:table-cell>
              <fo:table-cell border-bottom="thin solid black" padding="2px" text-align="center">
                 <fo:block>Units</fo:block>
              </fo:table-cell>
              <fo:table-cell border-bottom="thin solid black" padding="2px" text-align="center">
                 <fo:block>UG</fo:block>
              </fo:table-cell>
              <fo:table-cell border-bottom="thin solid black" padding="2px" text-align="center">
                 <fo:block>G</fo:block>
              </fo:table-cell>
              <fo:table-cell border-bottom="thin solid black" padding="2px" text-align="center">
                 <fo:block>%</fo:block>
              </fo:table-cell>
           </fo:table-row>
        </fo:table-header>
        <fo:table-body>
           <xsl:apply-templates select="course-record" />
        </fo:table-body>
     </fo:table>
     </fo:block>
   </xsl:template>

   <xsl:template match="course-record">
       <fo:table-row keep-together.within-page="1">
           <fo:table-cell padding="5px" text-align="left">
               <fo:block>
                   <xsl:apply-templates select="year" />
               </fo:block>
           </fo:table-cell>
           <fo:table-cell padding="5px" text-align="left">
               <fo:block>
                   <xsl:apply-templates select="description" />
               </fo:block>
           </fo:table-cell>
           <fo:table-cell padding="5px" text-align="left">
               <fo:block>
                   <xsl:apply-templates select="coursenumber" />
               </fo:block>
           </fo:table-cell>
           <fo:table-cell padding="5px" text-align="left">
               <fo:block>
                   <xsl:apply-templates select="title" />
               </fo:block>
           </fo:table-cell>
           <fo:table-cell padding="5px" text-align="center">
               <fo:block>
                   <xsl:apply-templates select="units" />
               </fo:block>
           </fo:table-cell>
           <fo:table-cell padding="5px" text-align="center">
               <fo:block>
                   <xsl:apply-templates select="undergraduatecount" />
               </fo:block>
           </fo:table-cell>
           <fo:table-cell padding="5px" text-align="center">
               <fo:block>
                   <xsl:apply-templates select="graduatecount" />
               </fo:block>
           </fo:table-cell>
           <fo:table-cell padding="5px" text-align="center">
               <fo:block>
                   <xsl:apply-templates select="effort" />
               </fo:block>
           </fo:table-cell>
       </fo:table-row>
   </xsl:template>

   <!-- THESIS COMMITTEE -->

   <xsl:template match="teaching-thesis">
      <fo:block text-decoration="underline"
                   space-after="10pt"
                   text-align="center">DO NOT INCLUDE SERVICE ON ORAL EXAMINATION COMMITTEES.</fo:block>
      <fo:block id="teaching-thesis" padding-bottom="1em">
         <fo:table table-layout="fixed"
                   width="100%"
                   font-size="10pt"
                   border="thin solid black"
                   space-after="10pt">
            <fo:table-column column-width="15%" />
            <fo:table-column column-width="7%" />
            <fo:table-column column-width="7%" />
            <fo:table-column column-width="10%" />
            <fo:table-column column-width="7%" />
            <fo:table-column column-width="12%" />
            <fo:table-column column-width="10%" />
            <fo:table-column column-width="10%" />
            <fo:table-column column-width="10%" />
            <fo:table-column column-width="12%" />
            <fo:table-header>
               <fo:table-row font-weight="bold" keep-with-previous="1">
                  <fo:table-cell border="thin solid black" padding="5px">
                     <fo:block text-align="left">Name of Student</fo:block>
                  </fo:table-cell>
                  <fo:table-cell border="thin solid black" padding="5px">
                     <fo:block text-align="left">Chair</fo:block>
                  </fo:table-cell>
                  <fo:table-cell border="thin solid black" padding="5px">
                     <fo:block text-align="left">Co-Chair</fo:block>
                  </fo:table-cell>
                  <fo:table-cell border="thin solid black" padding="5px">
                     <fo:block text-align="left">Member</fo:block>
                  </fo:table-cell>
                  <fo:table-cell border="thin solid black" padding="5px">
                     <fo:block text-align="left">Ph.D.</fo:block>
                  </fo:table-cell>
                  <fo:table-cell border="thin solid black" padding="5px">
                     <fo:block text-align="left">Doctorate</fo:block>
                  </fo:table-cell>
                  <fo:table-cell border="thin solid black" padding="5px">
                     <fo:block text-align="left">Master's</fo:block>
                  </fo:table-cell>
                  <fo:table-cell border="thin solid black" padding="5px">
                     <fo:block text-align="left">Degree In Progress</fo:block>
                  </fo:table-cell>
                  <fo:table-cell border="thin solid black" padding="5px">
                     <fo:block text-align="left">Degree Awarded</fo:block>
                  </fo:table-cell>
                  <fo:table-cell border="thin solid black" padding="5px">
                     <fo:block text-align="left">Current position if known</fo:block>
                  </fo:table-cell>
               </fo:table-row>
            </fo:table-header>
            <fo:table-body>
               <xsl:apply-templates select="thesis-record" />
            </fo:table-body>
         </fo:table>
      </fo:block>
   </xsl:template>

   <xsl:template match="thesis-record">
       <fo:table-row keep-together.within-page="1">
           <fo:table-cell border="thin solid black" padding="5px">
               <fo:block text-align="left">
                   <xsl:apply-templates select="studentname" />
               </fo:block>
           </fo:table-cell>
           <fo:table-cell border="thin solid black" padding="5px">
               <fo:block text-align="center">
                   <xsl:apply-templates select="committeechair" />
               </fo:block>
           </fo:table-cell>
           <fo:table-cell border="thin solid black" padding="5px">
               <fo:block text-align="center">
                   <xsl:apply-templates select="cochair" />
               </fo:block>
           </fo:table-cell>
           <fo:table-cell border="thin solid black" padding="5px">
               <fo:block text-align="center">
                   <xsl:apply-templates select="committeemember" />
               </fo:block>
           </fo:table-cell>
           <fo:table-cell border="thin solid black" padding="5px">
               <fo:block text-align="center">
                   <xsl:apply-templates select="doctoraldegree" />
               </fo:block>
           </fo:table-cell>
           <fo:table-cell border="thin solid black" padding="5px">
               <fo:block text-align="center">
                   <xsl:apply-templates select="doctoratedegree" />
               </fo:block>
           </fo:table-cell>
           <fo:table-cell border="thin solid black" padding="5px">
               <fo:block text-align="center">
                   <xsl:apply-templates select="mastersdegree" />
               </fo:block>
           </fo:table-cell>
           <fo:table-cell border="thin solid black" padding="5px">
               <fo:block text-align="center">
                   <xsl:apply-templates select="degreeprogress" />
               </fo:block>
           </fo:table-cell>
           <fo:table-cell border="thin solid black" padding="5px">
               <fo:block text-align="center">
                   <xsl:apply-templates select="degreeawarded" />
               </fo:block>
           </fo:table-cell>
           <fo:table-cell border="thin solid black" padding="5px">
               <fo:block text-align="left">
                   <xsl:apply-templates select="position" />
               </fo:block>
           </fo:table-cell>
       </fo:table-row>
   </xsl:template>

   <!-- UNIVERSITY EXTENSION -->

   <xsl:template match="teaching-extension">
   <fo:block id="teaching-extension" padding-bottom="1em">
     <fo:table table-layout="fixed" font-size="10pt" width="100%" border="thin solid black" space-after="10pt">
        <fo:table-column column-width="15%" />
        <fo:table-column column-width="25%" />
        <fo:table-column column-width="20%" />
        <fo:table-column column-width="20%" />
        <fo:table-column column-width="20%" />
        <fo:table-header>
           <fo:table-row font-weight="bold" keep-with-next="1">
              <fo:table-cell border="thin solid black" padding="5px" text-align="center">
                 <fo:block>Number of Course</fo:block>
              </fo:table-cell>
              <fo:table-cell border="thin solid black" padding="5px" text-align="center">
                 <fo:block>Title of Course</fo:block>
              </fo:table-cell>
              <fo:table-cell border="thin solid black" padding="5px" text-align="center">
                 <fo:block>Units</fo:block>
              </fo:table-cell>
              <fo:table-cell border="thin solid black" padding="5px" text-align="center">
                 <fo:block>Duration of Course</fo:block>
              </fo:table-cell>
              <fo:table-cell border="thin solid black" padding="5px" text-align="center">
                 <fo:block>Location</fo:block>
              </fo:table-cell>
           </fo:table-row>
        </fo:table-header>
        <fo:table-body>
           <xsl:apply-templates select="extension-record" />
        </fo:table-body>
     </fo:table>
     </fo:block>
   </xsl:template>

   <xsl:template match="extension-record">
       <fo:table-row keep-together.within-page="1">
           <fo:table-cell border="thin solid black" padding="5px" text-align="center">
               <fo:block>
                   <xsl:apply-templates select="coursenumber" />
               </fo:block>
           </fo:table-cell>
           <fo:table-cell border="thin solid black" padding="5px" text-align="left">
               <fo:block>
                   <xsl:apply-templates select="title" />
               </fo:block>
           </fo:table-cell>
           <fo:table-cell border="thin solid black" padding="5px" text-align="center">
               <fo:block>
                   <xsl:apply-templates select="units" />
               </fo:block>
           </fo:table-cell>
           <fo:table-cell border="thin solid black" padding="5px" text-align="left">
               <fo:block>
                   <xsl:apply-templates select="duration" />
               </fo:block>
           </fo:table-cell>
           <fo:table-cell border="thin solid black" padding="5px" text-align="left">
               <fo:block>
                   <xsl:apply-templates select="locationdescription" />
               </fo:block>
           </fo:table-cell>
       </fo:table-row>
   </xsl:template>

   <!-- STUDENT ADVISING -->

   <xsl:template match="teaching-student-advising">
    <xsl:apply-templates select="student-advising-record" />
   </xsl:template>

   <xsl:template match="student-advising-record">
    <fo:block border-bottom="thin solid black" font-size="12pt" font-weight="bold" keep-with-next="1">
       <xsl:apply-templates select="year" />
    </fo:block>
    <fo:table table-layout="fixed" width="100%" space-after="10pt" padding="5px" keep-together.within-page="1">
       <fo:table-column column-width="50%" />
       <fo:table-column column-width="50%" />
       <fo:table-body>
          <fo:table-row>
             <fo:table-cell>
                <fo:block><xsl:apply-templates select="advise1" /></fo:block>
             </fo:table-cell>
             <fo:table-cell>
                <fo:block><xsl:apply-templates select="ugnum" /></fo:block>
             </fo:table-cell>
          </fo:table-row>
          <fo:table-row>
             <fo:table-cell>
                <fo:block><xsl:apply-templates select="advise2" /></fo:block>
             </fo:table-cell>
             <fo:table-cell>
                <fo:block><xsl:apply-templates select="gnum" /></fo:block>
             </fo:table-cell>
          </fo:table-row>
          <xsl:if test="a3num != 0">
          <fo:table-row>
             <fo:table-cell>
                <fo:block><xsl:apply-templates select="advise3" /></fo:block>
             </fo:table-cell>
             <fo:table-cell>
                <fo:block><xsl:apply-templates select="a3num" /></fo:block>
             </fo:table-cell>
          </fo:table-row>
          </xsl:if>
          <xsl:if test="a4num != 0">
          <fo:table-row>
             <fo:table-cell>
                <fo:block><xsl:apply-templates select="advise4" /></fo:block>
             </fo:table-cell>
             <fo:table-cell>
                <fo:block><xsl:apply-templates select="a4num" /></fo:block>
             </fo:table-cell>
          </fo:table-row>
          </xsl:if>
       </fo:table-body>
     </fo:table>
   </xsl:template>

   <!-- SPECIAL ADVISING -->

   <xsl:template match="teaching-special-advising">
   <xsl:apply-templates select="special-advising-record" />
   </xsl:template>

   <xsl:template match="special-advising-record">
      <fo:block font-size="12pt" padding-bottom="12pt">
         <xsl:apply-templates select="year" />: <xsl:apply-templates select="spcontent" />
      </fo:block>
   </xsl:template>

   <!-- CURRICULAR DEVELOPMENT -->

   <xsl:template match="teaching-curriculum">
   <xsl:apply-templates select="curriculum-record" />
   </xsl:template>

   <xsl:template match="curriculum-record">
      <fo:block font-size="12pt" padding-bottom="12pt">
         <xsl:apply-templates select="year" />: <xsl:apply-templates select="curcontent" />
      </fo:block>
   </xsl:template>

   <!-- CONTACT HOURS -->

   <xsl:template match="teaching-contact-hours">
      <fo:block id="teaching-contact-hours" padding-bottom="1em">
         <fo:table table-layout="fixed" font-size="10pt" width="100%" border="thin solid black" space-after="10pt">
            <fo:table-column column-width="25%" />
            <fo:table-column column-width="15%" />
            <fo:table-column column-width="15%" />
            <fo:table-column column-width="15%" />
            <fo:table-column column-width="15%" />
            <fo:table-column column-width="15%" />
            <!--fo:table-column column-width="15%" /-->
            <fo:table-header>

               <fo:table-row font-weight="bold"  keep-with-next="1">
                  <fo:table-cell border="thin solid black" padding="5px" text-align="left">
                     <fo:block>Quarter/Year</fo:block>
                  </fo:table-cell>
                  <fo:table-cell border="thin solid black" padding="5px" text-align="center">
                     <fo:block>Lecture</fo:block>
                  </fo:table-cell>
                  <fo:table-cell border="thin solid black" padding="5px" text-align="center">
                     <fo:block>Discussion</fo:block>
                  </fo:table-cell>
                  <fo:table-cell border="thin solid black" padding="5px" text-align="center">
                     <fo:block>Laboratory</fo:block>
                  </fo:table-cell>
                  <fo:table-cell border="thin solid black" padding="5px" text-align="center">
                     <fo:block>Clinic</fo:block>
                  </fo:table-cell>
                  <fo:table-cell border="thin solid black" padding="5px" text-align="center">
                     <fo:block>Total</fo:block>
                  </fo:table-cell>
               </fo:table-row>
               </fo:table-header>
               <fo:table-body>
               <xsl:apply-templates select="contact-hours-record" />
               <fo:table-row keep-with-next="1">
                  <fo:table-cell border="thin solid black" padding="5px" text-align="left">
                     <fo:block>Total</fo:block>
                  </fo:table-cell>
                  <fo:table-cell border="thin solid black" padding="5px" padding-right="1em" text-align="right">
                     <fo:block><xsl:value-of select="format-number(sum(descendant::lecture),'##0.00')"/></fo:block>
                  </fo:table-cell>
                  <fo:table-cell border="thin solid black" padding="5px" padding-right="1em" text-align="right">
                     <fo:block><xsl:value-of select="format-number(sum(descendant::discussion),'##0.00')"/></fo:block>
                  </fo:table-cell>
                  <fo:table-cell border="thin solid black" padding="5px" padding-right="1em" text-align="right">
                     <fo:block><xsl:value-of select="format-number(sum(descendant::lab),'##0.00')"/></fo:block>
                  </fo:table-cell>
                  <fo:table-cell border="thin solid black" padding="5px" padding-right="1em" text-align="right">
                     <fo:block><xsl:value-of select="format-number(sum(descendant::clinic),'##0.00')"/></fo:block>
                  </fo:table-cell>
                  <fo:table-cell border="thin solid black" padding="5px" padding-right="1em" text-align="right">
                     <fo:block>
                        <xsl:value-of select="format-number(sum(descendant::lecture) + sum(descendant::discussion) + sum(descendant::lab) + sum(descendant::clinic),'##0.00')"/>
                     </fo:block>
                  </fo:table-cell>
               </fo:table-row>
            </fo:table-body>
         </fo:table>
      </fo:block>
   </xsl:template>

   <xsl:template match="contact-hours-record">
       <xsl:variable name="hour-total">
          <xsl:value-of select="format-number(sum(descendant::lecture) + sum(descendant::discussion) + sum(descendant::lab) + sum(descendant::clinic),'##0.00')"/>
       </xsl:variable>
       <fo:table-row>
           <fo:table-cell border="thin solid black" padding="5px" text-align="left">
               <fo:block>
                   <xsl:apply-templates select="term" />
               </fo:block>
           </fo:table-cell>
           <fo:table-cell border="thin solid black" padding="5px" padding-right="1em" text-align="right">
               <fo:block>
                   <xsl:apply-templates select="lecture" />
               </fo:block>
           </fo:table-cell>
           <fo:table-cell border="thin solid black" padding="5px" padding-right="1em" text-align="right">
               <fo:block>
                   <xsl:apply-templates select="discussion" />
               </fo:block>
           </fo:table-cell>
           <fo:table-cell border="thin solid black" padding="5px" padding-right="1em" text-align="right">
               <fo:block>
                   <xsl:apply-templates select="lab" />
               </fo:block>
           </fo:table-cell>
           <fo:table-cell border="thin solid black" padding="5px" padding-right="1em" text-align="right">
               <fo:block>
                   <xsl:apply-templates select="clinic" />
               </fo:block>
           </fo:table-cell>
           <fo:table-cell border="thin solid black" padding="5px" padding-right="1em" text-align="right">
               <fo:block>
                  <xsl:value-of select="$hour-total"/>
               </fo:block>
           </fo:table-cell>
       </fo:table-row>
   </xsl:template>

   <!-- TRAINEES -->

   <xsl:template match="teaching-trainees">
   <fo:block id="teaching-trainees" padding-bottom="1em">
     <fo:table table-layout="fixed" width="100%" font-size="10pt" border="thin solid black" space-after="10pt">
        <fo:table-column column-width="20%" />
        <fo:table-column column-width="20%" />
        <fo:table-column column-width="20%" />
        <fo:table-column column-width="20%" />
        <fo:table-column column-width="20%" />
        <fo:table-header>

           <fo:table-row font-weight="bold" keep-together.within-page="always">
              <fo:table-cell border="thin solid black" padding="5px" text-align="left">
                 <fo:block>Name</fo:block>
              </fo:table-cell>
              <fo:table-cell border="thin solid black" padding="5px" text-align="center">
                 <fo:block>Degree</fo:block>
              </fo:table-cell>
              <fo:table-cell border="thin solid black" padding="5px" text-align="center">
                 <fo:block>Trainee Type</fo:block>
              </fo:table-cell>
              <fo:table-cell border="thin solid black" padding="5px" text-align="center">
                 <fo:block>Years</fo:block>
              </fo:table-cell>
              <fo:table-cell border="thin solid black" padding="5px" text-align="left">
                 <fo:block>Current Position</fo:block>
              </fo:table-cell>
           </fo:table-row>
        </fo:table-header>
        <fo:table-body>
           <xsl:apply-templates select="trainee-record" />
        </fo:table-body>
     </fo:table>
     </fo:block>
   </xsl:template>

   <xsl:template match="trainee-record">
       <fo:table-row keep-together.within-page="1">
           <fo:table-cell border="thin solid black" padding="5px" text-align="left">
               <fo:block>
                   <xsl:apply-templates select="name" />
               </fo:block>
           </fo:table-cell>
           <fo:table-cell border="thin solid black" padding="5px" text-align="center">
               <fo:block>
                   <xsl:apply-templates select="degree" />
               </fo:block>
           </fo:table-cell>
           <fo:table-cell border="thin solid black" padding="5px" text-align="center">
               <fo:block>
                   <xsl:apply-templates select="type" />
               </fo:block>
           </fo:table-cell>
           <fo:table-cell border="thin solid black" padding="5px" text-align="center">
               <fo:block>
                   <xsl:apply-templates select="year" />
               </fo:block>
           </fo:table-cell>
           <fo:table-cell border="thin solid black" padding="5px" text-align="left">
               <fo:block>
                   <xsl:apply-templates select="position" />
               </fo:block>
           </fo:table-cell>
       </fo:table-row>
   </xsl:template>

   <!-- TEACHING SUPPLEMENT -->

   <xsl:template match="teaching-supplement-term">
      <fo:block id="teaching-supplement-term">
            <!-- Adding page break only when $hasHeaderpage exist -->
            <xsl:if test="$hasHeaderpage">
                <xsl:attribute name="break-before">page</xsl:attribute>
            </xsl:if>
            
         <fo:block id="title1" text-align="center" font-size="16pt" font-weight="bold" space-after="12pt">
                <xsl:value-of select="$title1"/>
         </fo:block>
         <fo:block font-size="16pt" font-weight="bold"
            text-align="center" space-after="12pt">
          <xsl:apply-templates select="//fname" />
          <xsl:apply-templates select="//lname" />
         </fo:block>
         <fo:block>
         <fo:table table-layout="fixed" width="100%" font-size="12pt">
            <fo:table-column column-width="10%" />
            <fo:table-column column-width="25%" />
            <fo:table-column column-width="60%" />
            <fo:table-column column-width="5%" />
            <fo:table-header>

               <fo:table-row font-weight="bold" text-decoration="underline">
                  <fo:table-cell padding="4px">
                     <fo:block>Year</fo:block>
                  </fo:table-cell>
                  <fo:table-cell padding="4px">
                     <fo:block>Term</fo:block>
                  </fo:table-cell>
                  <fo:table-cell padding="4px">
                     <fo:block>Title</fo:block>
                  </fo:table-cell>
                  <fo:table-cell padding="4px">
                     <fo:block text-align="center">Hours</fo:block>
                  </fo:table-cell>
               </fo:table-row>
            </fo:table-header>
            <fo:table-body>
               <xsl:apply-templates select="supplement-record" />
            </fo:table-body>
         </fo:table>
         </fo:block>
      </fo:block>
   </xsl:template>

   <xsl:template match="teaching-supplement-month">
      <fo:block id="teaching-supplement-month">
         <!-- Adding page break only when $hasHeaderpage or $$hasSupplementByTerm exist -->
         <xsl:if test="$hasHeaderpage | $hasSupplementByTerm">
                <xsl:attribute name="break-before">page</xsl:attribute>
         </xsl:if>
         <fo:block id="title2" text-align="center" font-size="16pt" font-weight="bold" space-after="12pt">
                <xsl:value-of select="$title2"/>
         </fo:block>
         <fo:block font-size="16pt" font-weight="bold"
            text-align="center" space-after="12pt">
          <xsl:apply-templates select="//fname" />
          <xsl:apply-templates select="//lname" />
         </fo:block>
         <fo:block>
         <fo:table table-layout="fixed" font-size="12pt" width="100%">
            <fo:table-column column-width="10%" />
            <fo:table-column column-width="25%" />
            <fo:table-column column-width="60%" />
            <fo:table-column column-width="5%" />
            <fo:table-header>
               <fo:table-row font-weight="bold" text-decoration="underline">
                  <fo:table-cell padding="4px">
                     <fo:block>Year</fo:block>
                  </fo:table-cell>
                  <fo:table-cell padding="4px">
                     <fo:block>Month</fo:block>
                  </fo:table-cell>
                  <fo:table-cell padding="4px">
                     <fo:block>Title</fo:block>
                  </fo:table-cell>
                  <fo:table-cell padding="4px">
                     <fo:block text-align="center">Hours</fo:block>
                  </fo:table-cell>
               </fo:table-row>
            </fo:table-header>
            <fo:table-body>
               <xsl:apply-templates select="supplement-record" />
            </fo:table-body>
         </fo:table>
         </fo:block>
      </fo:block>
   </xsl:template>

   <xsl:template match="supplement-record">
       <fo:table-row keep-together.within-page="1">
           <fo:table-cell padding="4px">
               <fo:block>
                   <xsl:apply-templates select="year" />
               </fo:block>
           </fo:table-cell>
           <xsl:choose>
           <xsl:when test="monthname">
           <fo:table-cell padding="4px">
               <fo:block>
                   <xsl:apply-templates select="monthname" />
               </fo:block>
           </fo:table-cell>
           </xsl:when>
           <xsl:otherwise>
           <fo:table-cell padding="4px">
               <fo:block>
                   <xsl:apply-templates select="description" />
               </fo:block>
           </fo:table-cell>
           </xsl:otherwise>
           </xsl:choose>
           <fo:table-cell padding="4px">
               <fo:block>
                   <xsl:apply-templates select="teachingtype" />&colon;&space;
                   <xsl:apply-templates select="title" /><xsl:if test="locationdescription">&comma;&space;
                   <xsl:apply-templates select="locationdescription" /></xsl:if>
               </fo:block>
           </fo:table-cell>
           <fo:table-cell padding="4px">
               <fo:block text-align="center">
                   <xsl:apply-templates select="hours" />
               </fo:block>
           </fo:table-cell>
       </fo:table-row>
   </xsl:template>

   <!-- ADDITIONAL INFORMATION -->

   <xsl:template match="teaching-additional">
     <fo:block id="teaching-additional" break-before="page">
      <fo:block id="title-additional" font-size="20pt" font-weight="bold" white-space-collapse="false"
          space-after="16pt" text-align="center"><xsl:apply-templates select="//teaching-additional/section-header"/>
      </fo:block>
      <fo:block font-size="16pt" font-weight="bold"
            text-align="center" space-after="12pt">
          <xsl:apply-templates select="//fname" />
          <xsl:apply-templates select="//lname" />
      </fo:block>
      <xsl:apply-templates select="additional-record" />
     </fo:block>
   </xsl:template>


   <!-- DATA ELEMENTS -->

   <xsl:template match="department|coursenumber|title|description|locationdescription|units|effort|studentname|position|header|content|spcontent|curcontent|duration|term|lecture|discussion|lab|clinic|name|degree|type|teachingtype|monthname|hours">
      <fo:inline><xsl:apply-templates /></fo:inline>
   </xsl:template>

   <xsl:template match="advise1|advise2|advise3|advise4|ugnum|gnum|a3num|a4num">
      <fo:inline font-size="12pt">
         <xsl:apply-templates />
      </fo:inline>
   </xsl:template>

   <xsl:template match="committeechair|cochair|committeemember|doctoraldegree|doctoratedegree|mastersdegree|degreeprogress|degreeawarded">
      <fo:inline><xsl:if test="text() = '1' or text() = 'true'">X</xsl:if></fo:inline>
   </xsl:template>

   <xsl:template match="year">
      <xsl:choose>
      <xsl:when test="parent::student-advising-record">
      <fo:inline>
         <xsl:value-of select="."/> - <xsl:value-of select=".+1"/>
      </fo:inline>
      </xsl:when>
      <xsl:otherwise>
      <fo:inline>
         <xsl:apply-templates />
      </fo:inline>
      </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
</xsl:stylesheet>
<!--
vim:ts=8 sw=2 et:
-->
