<?xml version="1.0" encoding="UTF-8"?>

<!DOCTYPE stylesheet [  
 <!ENTITY cr "&#x0A;">
 <!ENTITY space " ">
 <!ENTITY period ".">
 <!ENTITY comma ",">
 <!ENTITY qmark "?">
 <!ENTITY expoint "!">
 ]>

<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format">

  <xsl:include href="../mivMoreCommon.xslt" />

  <!-- Strip space nodes within all elements -->
  <xsl:strip-space elements="*"/>

  <!-- common setup for all document types -->
	<xsl:template name="documentSetup">
    <xsl:param name="documentName" />
	  <xsl:param name="documentTitle" />
    <xsl:param name="title1" />
    <xsl:param name="title2" />
    <xsl:param name="titleAdditional" />
    <fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format">
     
         <fo:layout-master-set>
            <fo:simple-page-master master-name="myinfovault"
               page-height="11in" page-width="8.5in" margin-top=".3in"
               margin-bottom=".3in" margin-left=".5in"
               margin-right=".5in">
              
               <fo:region-body margin-bottom="2pt" margin-right="2pt" margin-left="2pt" margin-top="2pt" 
               region-name="xsl-region-body">
                
               </fo:region-body>
               <fo:region-before  region-name="xsl-region-before"/>
               <fo:region-after region-name="xsl-region-after"/>
               <fo:region-start  region-name="xsl-region-start" />
               <fo:region-end region-name="xsl-region-end" />
              
            </fo:simple-page-master>
         </fo:layout-master-set>
         <fo:bookmark-tree>
            <fo:bookmark internal-destination="maintitle">
               <fo:bookmark-title><xsl:value-of select="$documentName"/></fo:bookmark-title>
            </fo:bookmark>
            <xsl:if test="$title1">
              <fo:bookmark internal-destination="title1">
                <fo:bookmark-title>
                  <xsl:value-of select="$title1" />
                </fo:bookmark-title>
              </fo:bookmark>
            </xsl:if>
            <xsl:if test="$title2">
              <fo:bookmark internal-destination="title2">
                <fo:bookmark-title>
                  <xsl:value-of select="$title2" />
                </fo:bookmark-title>
              </fo:bookmark>
            </xsl:if>
            <xsl:if test="$titleAdditional">
              <fo:bookmark internal-destination="title-additional">
                <fo:bookmark-title>
                  <xsl:value-of select="$titleAdditional" />
                </fo:bookmark-title>
              </fo:bookmark>
            </xsl:if>
         </fo:bookmark-tree>
         
         <fo:page-sequence master-reference="myinfovault">
          
           <fo:static-content flow-name="xsl-region-after">
               <fo:table table-layout="fixed" width="100%">
                   <fo:table-column column-width="90%" />
                   <fo:table-column column-width="10%" />
                   <fo:table-body>
                       <fo:table-row>
                           <fo:table-cell>
                             <fo:block vertical-align="bottom" font-size="9pt" text-align="left">
                                 <fo:inline>
                                   <!-- <xsl:value-of select="'Revised:  02/14/09'"/> -->
                                 </fo:inline>
                               </fo:block>
                           </fo:table-cell>
                           <fo:table-cell>
                               <fo:block vertical-align="bottom" text-align="right">
                                   <!-- <fo:page-number />  -->
                               </fo:block>
                           </fo:table-cell>
                       </fo:table-row>
                   </fo:table-body>
               </fo:table>
           </fo:static-content>
           
            <fo:flow  flow-name="xsl-region-body">
              <fo:block id="maintitle" border-top-style="solid" border-top-width="0px"
              border-before-style="solid" border-before-width="0px"
              border-after-style="solid" border-after-width="0px"/>
              <xsl:apply-templates />
            </fo:flow>
           
         </fo:page-sequence>
    </fo:root>
 	  
   </xsl:template>
   
   <xsl:template name="maintitle">
      <xsl:param name="documentName"/>
      <fo:block id="maintitle"
                font-size="16pt"
                font-weight="bold"
                space-after="1pt"
                text-align="center">
        <xsl:value-of select="$documentName"/>
      </fo:block>
     <xsl:apply-templates/>
   </xsl:template>
	
	<xsl:template match="highlight">
		<xsl:variable name="fontweight">
		  <!-- Don't set bold if either subscript or supescript are enabled -->
		  <xsl:if test="@bold='1'">
          <xsl:value-of select="'bold'" />
        <!-- Not sure why we were doing this ?? -->
        <!-- 
        <xsl:choose>
          <xsl:when test="@subscript='1'">
            <xsl:value-of select="'normal'" />
          </xsl:when>
          <xsl:when test="@superscript='1'">
            <xsl:value-of select="'normal'" />
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="'bold'" />
          </xsl:otherwise>
        </xsl:choose>
        -->				  
			</xsl:if>
		</xsl:variable>
		<xsl:variable name="fontstyle">
			<xsl:choose>
				<xsl:when test="@italic='1'">
					<xsl:value-of select="'italic'" />
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="'normal'" />
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="textdecoration">
			<xsl:choose>
				<xsl:when test="@underline='1'">
					<xsl:value-of select="'underline'" />
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="'none'" />
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="verticalalign">
			<xsl:choose>
				<xsl:when test="@subscript='1'">
					<xsl:value-of select="'sub'" />
				</xsl:when>
				<xsl:when test="@superscript='1'">
					<xsl:value-of select="'super'" />
				</xsl:when>
			</xsl:choose>
		</xsl:variable>
		<xsl:choose>
			<xsl:when test="@subscript='1' or @superscript='1'">
				<fo:inline font-size="75%"
					vertical-align="{$verticalalign}" font-weight="{$fontweight}"
					font-style="{$fontstyle}" text-decoration="{$textdecoration}">
          <xsl:apply-templates/>
				</fo:inline>
			</xsl:when>
			<xsl:otherwise>
				<fo:inline font-weight="{$fontweight}"
					font-style="{$fontstyle}" text-decoration="{$textdecoration}">
					<xsl:apply-templates/>
				</fo:inline>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template match="symbol">
		<xsl:variable name="fontfamily" select="'Symbol'" />
		<xsl:variable name="fontweight">
			<xsl:choose>
				<xsl:when test="@bold='1'">
					<xsl:value-of select="'bold'" />
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="'normal'" />
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="fontstyle">
			<xsl:choose>
				<xsl:when test="@italic='1'">
					<xsl:value-of select="'italic'" />
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="'normal'" />
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="textdecoration">
			<xsl:choose>
				<xsl:when test="@underline='1'">
					<xsl:value-of select="'underline'" />
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="'none'" />
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="verticalalign">
			<xsl:choose>
				<xsl:when test="@subscript='1'">
					<xsl:value-of select="'bottom'" />
				</xsl:when>
				<xsl:when test="@superscript='1'">
					<xsl:value-of select="'top'" />
				</xsl:when>
			</xsl:choose>
		</xsl:variable>
		<xsl:choose>
			<xsl:when test="@subscript='1' or @superscript='1'">
				<fo:inline font-size="xx-small"
					font-family="{$fontfamily}" vertical-align="{$verticalalign}"
					font-weight="{$fontweight}" font-style="{$fontstyle}"
					text-decoration="{$textdecoration}">
          <xsl:apply-templates/>
				</fo:inline>
			</xsl:when>
			<xsl:otherwise>
				<fo:inline font-weight="{$fontweight}" font-size="9pt"
					font-family="{$fontfamily}" font-style="{$fontstyle}"
					text-decoration="{$textdecoration}">
          <xsl:apply-templates/>
				</fo:inline>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
 
<!-- Additional Information Two Column layout -->

  <xsl:template match="additional-record">
       <fo:block padding-bottom="10pt">
        <!-- xsl:apply-templates select="number" />.
        <xsl:apply-templates select="content" / -->
           <xsl:apply-templates select="addheader" /> 
           <xsl:apply-templates select="addcontent" />   
    </fo:block>
    </xsl:template>
   
   <!-- Code to handle Table data --> 
  <!-- when table-and-caption is supported, that will be the
   wrapper for this template -->
   <xsl:template match="table">
     <!-- Check to make sure there is a tbody element -->
     <xsl:choose>
       <xsl:when test="name(child::node())='tbody'">
           <xsl:apply-templates/>
       </xsl:when>
        <xsl:otherwise>
          <xsl:call-template name="tbody"/>
        </xsl:otherwise>
     </xsl:choose>
   </xsl:template>  
    
    <!--
    find the width= attribute of all the <th> and <td>
    elements in the first <tr> of this table. They are
    in pixels, so divide by 72 to get inches
  -->
   <xsl:template name='tbody' match="tbody">
   <fo:table>
      <xsl:attribute name="space-after">10pt</xsl:attribute>
      <xsl:for-each select="tr[1]/th|tr[1]/td">
        <fo:table-column>
         <xsl:attribute name="width">
           <xsl:choose>
             <xsl:when test="ancestor::table/@width">
               <xsl:value-of select="floor(ancestor::table/@width div 72)"/>in         
             </xsl:when>
             <xsl:otherwise>
               <!-- Provide a default -->
               <xsl:value-of select="510"/>in         
             </xsl:otherwise>
           </xsl:choose>
         </xsl:attribute>      
         <xsl:attribute name="column-width">    
              <xsl:value-of select="@width"/>
        </xsl:attribute> 
      </fo:table-column>  
      </xsl:for-each>       
    <fo:table-body start-indent="0pt">
      <xsl:apply-templates />
    </fo:table-body>  
   </fo:table>   
   </xsl:template>

   <xsl:template match="addheader">
    <fo:block white-space-collapse="false" font-size="12pt" font-weight="bold" text-decoration="underline"
      padding-top="12pt">
      <xsl:apply-templates />
    </fo:block>
  </xsl:template>

  <xsl:template match="addcontent">
    <fo:block font-size="12pt">
      <xsl:apply-templates />
    </fo:block>
  </xsl:template>

</xsl:stylesheet>