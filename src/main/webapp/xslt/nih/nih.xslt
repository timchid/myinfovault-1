<?xml version="1.0" encoding="UTF-8"?>

<!DOCTYPE stylesheet [  
 <!ENTITY cr "&#x0A;">
 <!ENTITY space " ">
 <!ENTITY period ".">
 <!ENTITY comma ",">
 <!ENTITY qmark "?">
 <!ENTITY expoint "!">
 ]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
   xmlns:fo="http://www.w3.org/1999/XSL/Format">

   <xsl:import href="./html-tags.xslt"/>
   <xsl:import href="personal/personal.xslt"/>
   <xsl:import href="education/education.xslt"/>
   <xsl:import href="education/honors.xslt"/>
   <xsl:import href="employment/employment.xslt"/>
   <xsl:import href="grants/grants.xslt"/>
   <xsl:import href="publications/publications.xslt"/>
   <xsl:import href="service/service-committees.xslt"/>
   <xsl:import href="service/service-board.xslt"/>
   <xsl:include href="style-options.xslt"/>

    <xsl:param name="title.fontsize">20pt</xsl:param>
    <xsl:param name="subheader.fontsize">12pt</xsl:param>
    <xsl:param name="text.fontsize">11pt</xsl:param>
    <!-- Headers -->
    <xsl:attribute-set name="header.format">
        <xsl:attribute name="font-weight">bold</xsl:attribute>
        <xsl:attribute name="font-size">12pt</xsl:attribute>
        <xsl:attribute name="space-before">12pt</xsl:attribute>
        <xsl:attribute name="space-after">11pt</xsl:attribute>
    </xsl:attribute-set>
    <xsl:attribute-set name="subheader.format">
        <xsl:attribute name="font-weight">bold</xsl:attribute>
        <xsl:attribute name="text-decoration">underline</xsl:attribute>
        <xsl:attribute name="font-size">11pt</xsl:attribute>
    </xsl:attribute-set>

    <xsl:variable name="document-revision">PHS 398/2590 (Rev. 06/09)</xsl:variable>
    <xsl:variable name="document-name">Biographical Sketch Format Page</xsl:variable>
    <xsl:variable name="heading-label">Program Director/Principal Investigator (Last, First, Middle):&space;</xsl:variable>

    <xsl:template match="/"> 
        <!--  Set up page sequence, header and footer -->
        <fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format">
            <fo:layout-master-set>
                <fo:simple-page-master margin-bottom=".5in"
                    margin-left=".5in" margin-right=".5in"
                    margin-top=".5in" master-name="myinfovault"
                    page-height="11in" page-width="8.5in">
                    <fo:region-body margin-bottom="10pt" region-name="xsl-region-body"/>
                    <fo:region-before region-name="xsl-region-before"/>
                    <fo:region-after  region-name="xsl-region-after" />
                </fo:simple-page-master>
            </fo:layout-master-set>
            <fo:page-sequence master-reference="myinfovault">
            <fo:static-content flow-name="xsl-region-before">
               <fo:table table-layout="fixed" width="100%">
                   <fo:table-column column-width="1%" />
                   <fo:table-column column-width="98%" />
                   <fo:table-column column-width="1%" />
                   <fo:table-body>
                       <fo:table-row>
                           <fo:table-cell>
                               <fo:block vertical-align="top" text-align="left">
                               </fo:block>
                           </fo:table-cell>
                           <fo:table-cell padding-bottom="12pt">
                               <fo:block font-family="Helvetica" vertical-align="top" text-align="center">
                                   <fo:inline font-size="8pt" ><xsl:value-of select="$heading-label"/></fo:inline>
                                   <fo:inline><xsl:value-of select="biosketch/attributes/attribute-record/programdirector_name"/></fo:inline>
                               </fo:block> 
                           </fo:table-cell>
                           <fo:table-cell>
                               <fo:block vertical-align="top" text-align="right">
                               </fo:block>
                           </fo:table-cell>
                       </fo:table-row>
                   </fo:table-body>
               </fo:table>
            </fo:static-content>
            <fo:static-content flow-name="xsl-region-after">
               <fo:table table-layout="fixed" border-top="thin solid black" width="100%">
                   <fo:table-column column-width="45%" />
                   <fo:table-column column-width="10%" />
                   <fo:table-column column-width="45%" />
                   <fo:table-body>
                       <fo:table-row>
                           <fo:table-cell>
                               <fo:block vertical-align="bottom" text-align="left">
                                 <fo:inline font-size="8pt" ><xsl:value-of select="$document-revision"/></fo:inline>
                               </fo:block>
                           </fo:table-cell>
                           <fo:table-cell>
                               <fo:block vertical-align="bottom" text-align="center">
                                   <fo:inline font-size="8pt" >Page <fo:inline text-decoration="underline" ><fo:page-number /></fo:inline></fo:inline>
                               </fo:block>
                           </fo:table-cell>
                           <fo:table-cell>
                               <fo:block vertical-align="bottom" text-align="right">
                                  <fo:inline font-size="8pt" font-weight="bold"><xsl:value-of select="$document-name"/></fo:inline>
                               </fo:block>
                           </fo:table-cell>
                       </fo:table-row>
                   </fo:table-body>
               </fo:table>
            </fo:static-content>
            <fo:flow flow-name="xsl-region-body">
               <xsl:apply-templates />
            </fo:flow>
         </fo:page-sequence>
        </fo:root>
    </xsl:template>
    <xsl:template name="document-header">
        <fo:table border-bottom="thin solid black" border-top="thin solid black">
          <fo:table-column column-width="100%"/>
          <fo:table-body>        
            <fo:table-row>
              <fo:table-cell padding-top="12pt">
                <fo:block text-align="center">
                  <fo:block font-size="12pt" font-weight="bold" space-before="12pt">BIOGRAPHICAL SKETCH</fo:block>
                  <fo:block font-size="8pt">Provide the following information for the Senior/key personnel and other significant
                                            contributors in the order listed on Form Page 2.</fo:block>
                  <fo:block
                    font-size="8pt">Follow this format for each person. <fo:inline font-weight="bold">DO NOT EXCEED FOUR PAGES.</fo:inline>
                  </fo:block>
                </fo:block>
              </fo:table-cell>
            </fo:table-row>
          </fo:table-body>
        </fo:table>
        <fo:table border-bottom="thin solid black" border-top="thin solid black"
            space-before="12pt" table-layout="fixed" width="100%">
            <fo:table-column column-width="50%"/>
            <fo:table-column column-width="50%"/>
            <fo:table-body>
                <fo:table-row>
                    <fo:table-cell border-bottom="thin solid black"
                        border-right="thin solid black" padding="1pt">
                        <fo:block font-size="8pt" space-after="2pt">NAME</fo:block>
                        <fo:block font-size="11pt">
                            <!-- If the value is not present, display space to preserve column size -->
                            <xsl:choose>
                              <xsl:when test="attributes/attribute-record/full_name">
                                <xsl:apply-templates select="attributes/attribute-record/full_name"/>
                              </xsl:when>
                              <xsl:otherwise>
                                <fo:inline>&#160;</fo:inline>
                              </xsl:otherwise>
                            </xsl:choose>
                        </fo:block>
                    </fo:table-cell>
                    <fo:table-cell border-bottom="thin solid black"
                        number-rows-spanned="2" padding-left="4pt" padding-top="1pt">
                        <fo:block font-size="8pt" space-after="2pt" >POSITION TITLE</fo:block>
                        <fo:block font-size="11pt" space-after="2pt">
                            <!-- If the value is not present, display space to preserve column size -->
                            <xsl:choose>
                              <xsl:when test="attributes/attribute-record/position_title1">
                                <xsl:apply-templates select="attributes/attribute-record/position_title1"/>
                              </xsl:when>
                              <xsl:otherwise>
                                <fo:inline>&#160;</fo:inline>
                              </xsl:otherwise>
                            </xsl:choose>
                        </fo:block>
                        <fo:block font-size="11pt" space-after="2pt">
                           <!-- If the value is not present, display space to preserve column size -->
                           <xsl:choose>
                              <xsl:when test="attributes/attribute-record/position_title2">
                                <xsl:apply-templates select="attributes/attribute-record/position_title2"/>
                              </xsl:when>
                              <xsl:otherwise>
                                <fo:inline>&#160;</fo:inline>
                              </xsl:otherwise>
                           </xsl:choose>
                        </fo:block>
                    </fo:table-cell>
                </fo:table-row>
                <fo:table-row>
                    <fo:table-cell border-bottom="thin solid black"
                        border-right="thin solid black" padding="1pt">
                        <fo:block font-size="8pt" space-after="2pt">eRA COMMONS USER NAME (credential, e.g., agency login)</fo:block>
                        <fo:block font-size="11pt" space-after="2pt">
                            <!-- If the value is not present, display space to preserve column size -->
                            <xsl:choose>
                              <xsl:when test="attributes/attribute-record/era_username">
                                <xsl:apply-templates select="attributes/attribute-record/era_username"/>
                              </xsl:when>
                              <xsl:otherwise>
                                <fo:inline>&#160;</fo:inline>
                              </xsl:otherwise>
                            </xsl:choose>
                        </fo:block>
                    </fo:table-cell>
                </fo:table-row>
            </fo:table-body>
        </fo:table>
        <xsl:apply-templates select="education"/>
    </xsl:template>
    <xsl:template name="section-header">
        <xsl:param name="headerNumber"/>
        <xsl:param name="header"/>
        <fo:block xsl:use-attribute-sets="header.format">
            <xsl:value-of
                select="$headerNumber"/>.
            <xsl:value-of select="$header"/>
        </fo:block>
    </xsl:template>
    <xsl:template name="subsection-header">
        <xsl:param name="header"/>
        <fo:block xsl:use-attribute-sets="subheader.format" space-before="12pt">
            <xsl:value-of select="$header"/>
        </fo:block>
    </xsl:template>
    <xsl:template match="section-header" mode="subsectionHeader">
        <fo:block font-size="${subheader.fontsize}" font-weight="bold"
            space-before="12pt" text-decoration="underline">
            <xsl:apply-templates/>
        </fo:block>
    </xsl:template>
    
    <!-- Skips processing any section headers present in the document -->
    <xsl:template match="section-header">
    </xsl:template>
    
    <xsl:template match="biosketch">
      <fo:block font-family="Helvetica">
        <xsl:call-template name="document-header"/>
        <!-- Personal Statement -->
        <xsl:call-template name="section-header">
            <xsl:with-param name="headerNumber">A</xsl:with-param>
            <xsl:with-param name="header">Personal Statement</xsl:with-param>
        </xsl:call-template>    
        <!-- Extra space after Personal Statement header -->
        <fo:block><fo:inline>&#160;</fo:inline></fo:block>    
        <!-- Employment History & Honors are mandatory -->
        <xsl:call-template name="section-header">
            <xsl:with-param name="headerNumber">B</xsl:with-param>
            <xsl:with-param name="header">Positions and Honors</xsl:with-param>
        </xsl:call-template>
        <xsl:call-template name="subsection-header">
            <xsl:with-param name="header">Positions and Employment</xsl:with-param>
        </xsl:call-template>
        <xsl:apply-templates select="employment"/>
       
        <xsl:call-template name="subsection-header">
            <xsl:with-param name="header">Other Experience and Professional Memberships</xsl:with-param>
        </xsl:call-template>
        <xsl:apply-templates select="service-committee"/>

        <xsl:call-template name="subsection-header">
            <xsl:with-param name="header">Editorial Board</xsl:with-param>
        </xsl:call-template>
        <xsl:apply-templates select="service-board"/>

        <xsl:call-template name="subsection-header">
            <xsl:with-param name="header">Honors</xsl:with-param>
        </xsl:call-template>
        <xsl:apply-templates select="honors"/>
        <xsl:call-template name="section-header">
            <xsl:with-param name="headerNumber">C</xsl:with-param>
            <xsl:with-param name="header">Selected peer-reviewed publications (in chronological order).</xsl:with-param>
        </xsl:call-template>
        <xsl:apply-templates select="nih-publications"/>
        <xsl:call-template name="section-header">
            <xsl:with-param name="headerNumber">D</xsl:with-param>
            <xsl:with-param name="header">Research Support</xsl:with-param>
        </xsl:call-template>
        <xsl:if test="grants-ongoing-research/grantnih-record">
            <xsl:call-template name="subsection-header">
                <xsl:with-param name="header">Ongoing Research Support</xsl:with-param>
            </xsl:call-template>
            <xsl:apply-templates select="grants-ongoing-research"/>
        </xsl:if>
        <xsl:if test="grants-completed-research/grantnih-record">
            <xsl:call-template name="subsection-header">
                <xsl:with-param name="header">Completed Research Support</xsl:with-param>
            </xsl:call-template>
            <xsl:apply-templates select="grants-completed-research"/>
        </xsl:if>
        <xsl:if test="grants-other-research/grantnih-record">
            <xsl:call-template name="subsection-header">
                <xsl:with-param name="header">Other Support</xsl:with-param>
            </xsl:call-template>
            <xsl:apply-templates select="grants-other-research"/>
        </xsl:if>
      </fo:block>
    </xsl:template>
</xsl:stylesheet>
