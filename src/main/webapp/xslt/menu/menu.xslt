<!DOCTYPE html>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="html" encoding="UTF-8" indent="yes"/>

    <xsl:variable name="root">
        <xsl:value-of select="/menu-set/@root"/>
    </xsl:variable>
    
    <xsl:template match="/">
        <xsl:text disable-output-escaping="yes"><![CDATA[<%@ taglib prefix="miv" uri="mivTags" %>]]></xsl:text>
        <xsl:text disable-output-escaping="yes"><![CDATA[<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>]]></xsl:text>
        <xsl:text disable-output-escaping="yes"><![CDATA[<!DOCTYPE html>]]></xsl:text>

        <html>
             <xsl:text disable-output-escaping="yes"><![CDATA[<c:choose>]]></xsl:text>
             
             <!-- generate <head> and <body> jsp choices -->
             <xsl:apply-templates select="menu-set"/>
             
             <xsl:text disable-output-escaping="yes"><![CDATA[</c:choose>]]></xsl:text>
        </html>
    </xsl:template>
    
    <!-- one per top level menu -->
    <xsl:template match="menu[not(ancestor::menu)]">
        <!-- start jsp choice -->
        <xsl:text disable-output-escaping="yes"><![CDATA[<c:when test="${menupath == ']]></xsl:text>
        <xsl:value-of select="@root" />
        <xsl:text disable-output-escaping="yes"><![CDATA['}">]]></xsl:text>
        
        <!-- get heading -->
        <xsl:variable name="heading">
            <xsl:choose>
                <!-- use the heading element if available -->
                <xsl:when test="heading != ''">
                    <xsl:value-of select="heading" />
                </xsl:when>
                
                <!-- else, use the name attribute -->
                <xsl:otherwise>
                    <xsl:value-of select="name" />
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        
        <!-- get title -->
        <xsl:variable name="title">
            <xsl:choose>
                <!-- use the title element if available -->
                <xsl:when test="title != ''">
                    <xsl:value-of select="title" />
                </xsl:when>
                
                <!-- else, use the name attribute -->
                <xsl:otherwise>
                    <xsl:value-of select="name" />
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        
        <head>
            <title>
                <xsl:text disable-output-escaping="yes"><![CDATA[MIV &ndash; ]]></xsl:text>
                <xsl:value-of select="$heading" />
            </title>
             
            <xsl:text disable-output-escaping="yes">
                <![CDATA[<%@ include file="/jsp/metatags.html" %>
                <!--[if lt IE 8]>
                <link rel="stylesheet" type="text/css" href="<c:url value='/css/iehacks.css'/>" />
                <![endif]-->
                <script type="text/javascript" src="/miv/js/jquery.js"></script>
                <c:set var="user" scope="request" value="${MIVSESSION.user}"/>]]>
            </xsl:text>
        </head>
        
        <body>
            <xsl:text disable-output-escaping="yes"><![CDATA[<jsp:include page="/jsp/mivheader.jsp" />]]></xsl:text>
             
            <div id="breadcrumbs">
                <a>
                    <xsl:attribute name="href">
                        <xsl:value-of select="$root"/>
                        <xsl:text>/MIVMain</xsl:text>
                    </xsl:attribute>
                    
                    <xsl:attribute name="title"><xsl:text>Home</xsl:text></xsl:attribute>
                    
                    <xsl:text>Home</xsl:text>
                </a>
            
                <xsl:text> > </xsl:text>
            
                <xsl:value-of select="$heading" />
            </div>

            <div id="main">
                <h1>
                    <xsl:attribute name="title">
                            <xsl:value-of select="$title"/>
                    </xsl:attribute>
                    <xsl:value-of select="$heading"/></h1>
             
             		<xsl:if test="description != ''">
             			<p class="description">
		                    <xsl:attribute name="title">
		                        <xsl:value-of select="$title"/>
		                    </xsl:attribute>
		                    <xsl:value-of select="description"/>
		                </p>             		
             		</xsl:if>
                
             
                <div class="menublock">
                    <xsl:text disable-output-escaping="yes"><![CDATA[<ul class="menulist">]]></xsl:text>
                    
                    <xsl:for-each select="menu|permit">
                        <xsl:if test="@break">
                            <xsl:text disable-output-escaping="yes"><![CDATA[</ul></div><div class="menublock"><ul class="menulist">]]></xsl:text>    
                        </xsl:if>
                        <xsl:apply-templates select="."/>                    
                    </xsl:for-each>
                    <xsl:text disable-output-escaping="yes"><![CDATA[</ul>]]></xsl:text>
                 </div>
             </div>
             
             <xsl:text disable-output-escaping="yes"><![CDATA[<jsp:include page="/jsp/miv_small_footer.jsp" />]]></xsl:text>
        </body>
        
        <!-- end jsp choice -->
        <xsl:text disable-output-escaping="yes"><![CDATA[</c:when>]]></xsl:text>
    </xsl:template>
    
    <xsl:template match="menu">
        <!-- get heading to show -->
        <xsl:variable name="heading">
            <xsl:choose>
                <!-- use the heading element if available -->
                <xsl:when test="heading != ''">
                    <xsl:value-of select="heading" />
                </xsl:when>
                
                <!-- else, use the name attribute -->
                <xsl:otherwise>
                    <xsl:value-of select="name" />
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        
        <!-- get title to show -->
        <xsl:variable name="title">
            <xsl:choose>
                <!-- use the title element if available -->
                <xsl:when test="title != ''">
                    <xsl:value-of select="title" />
                </xsl:when>
                
                <!-- else, use the heading variable -->
                <xsl:otherwise>
                    <xsl:value-of select="$heading" />
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        
        <xsl:variable name="display">
            <xsl:choose>
                <!-- wrap url around heading if available -->
                <xsl:when test="url != ''">
                    <a>
                        <xsl:attribute name="href">
                            <xsl:value-of select="$root"/>
                            <xsl:value-of select="url"/>
                        </xsl:attribute>
                        
                        <xsl:attribute name="title">
                            <xsl:value-of select="$title"/>
                        </xsl:attribute>
                        
                        <xsl:value-of select="$heading"/>
                    </a>
                </xsl:when>
                
                <xsl:otherwise>
                    <xsl:value-of select="$heading"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        
        <xsl:choose>
            <!-- is sub-sub menu -->
            <xsl:when test="count(ancestor::menu) > 1">
                <li class="menuitem">
                    <xsl:copy-of select="$display"/>
                    
                    <xsl:if test="description != ''">
                        <p class="description">
                            <xsl:attribute name="title">
                                <xsl:value-of select="$title"/>
                            </xsl:attribute>
                            <xsl:value-of select="description"/>
                         </p>
                    </xsl:if>
                </li>
            </xsl:when>
            
            <!-- is sub menu -->
            <xsl:otherwise>
                <li class="menugroup">
                    <xsl:choose>
                        <xsl:when test="count(descendant::menu) > 0">
                               <h2>
                                   <xsl:attribute name="title">
                                    <xsl:value-of select="$title"/>
                                </xsl:attribute>
                                <xsl:copy-of select="$display"/>
                             </h2>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:copy-of select="$display"/>                            
                        </xsl:otherwise>
                    </xsl:choose>
                    
                    <xsl:if test="description != ''">
                        <p class="description">
                            <xsl:attribute name="title">
                                <xsl:value-of select="$title"/>
                            </xsl:attribute>
                            <xsl:value-of select="description"/>
                         </p>
                    </xsl:if>    
                    
                    <!-- menu has child menus -->
                    <xsl:if test="count(descendant::menu) > 0">
                        <xsl:text disable-output-escaping="yes"><![CDATA[<ul>]]></xsl:text>    
                            <xsl:for-each select="menu|permit">        
                                <xsl:apply-templates select="."/>                            
                            </xsl:for-each>
                        <xsl:text disable-output-escaping="yes"><![CDATA[</ul>]]></xsl:text>    
                    </xsl:if>
                </li>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <!-- permit template (ignoring top-level permit tag) -->
    <xsl:template match="permit[ancestor::menu]">
        <xsl:choose>                
            <xsl:when test="@roles | @action">
                <xsl:text disable-output-escaping="yes"><![CDATA[<miv:permit usertype="]]></xsl:text>
                    <xsl:value-of select="@usertype"/>
                    
                    <xsl:if test="@roles">
                        <xsl:text disable-output-escaping="yes"><![CDATA[" roles="]]></xsl:text>
                        <xsl:value-of select="@roles"/>
                    </xsl:if>
                    
                    <xsl:if test="@action">
                        <xsl:text disable-output-escaping="yes"><![CDATA[" action="]]></xsl:text>
                        <xsl:value-of select="@action"/>
                </xsl:if>
                    
                    <xsl:text disable-output-escaping="yes"><![CDATA[">]]></xsl:text>
                    <xsl:apply-templates select="menu|permit"/>
                    <xsl:text disable-output-escaping="yes"><![CDATA[</miv:permit>]]></xsl:text>
            </xsl:when>
            <xsl:otherwise>
                <xsl:apply-templates select="menu|permit"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <xsl:template name="spacedivs">
        <!-- loop index -->
        <xsl:param name="index"/>
        
        <!-- loop final index -->
        <xsl:param name="final"/>
        
        <!-- continue if not reached final -->
        <xsl:if test="$index &lt; $final">
            <!-- print space div -->
            <div><xsl:text> </xsl:text></div>
            
            <!-- recurse -->
            <xsl:call-template name="spacedivs">
                <xsl:with-param name="index" select="$index + 1"/>
                <xsl:with-param name="final" select="$final"/>
            </xsl:call-template>
        </xsl:if>
    </xsl:template>
</xsl:stylesheet>