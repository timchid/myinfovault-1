<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%--
--%><c:set var="user" scope="request" value="${MIVSESSION.user}"/><%--
--%><c:if test="${constants.config.usepopups}">
<head>
<meta http-equiv="cache-control" content="no-cache, must-revalidate">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="expires" content="0">
</head>
<!-- Using Pop-Up edit dialogs -->
<script type="text/javascript">
if (typeof _container_ != 'undefined') {
    var btn = document.getElementById("popupformcancel");
    _container_.setCloseControl(btn);
}
</script>
</c:if><%-- usepopups --%>
<script type="text/javascript"><!--
    mivFormProfile = {
       required: [ ${constants.config.required} ],
       trim: ["institution", "location", "startdate", "enddate",
              "title", "salary", "remark"]
    };
// -->
</script>

<div id="userheading">
	<span class="standout">${user.targetUserInfo.displayName}</span>
</div>

<%-- <!-- record: ${rec} --> --%>
<%-- constants: ${constants} --%>
<%-- constants.labels: ${constants.labels} --%>

<!-- This is the data entry form for "${recordTypeName}" records -->
<form class="mivdata" id="mytestform"
      method="post" action="<c:url value='${constants.config.saveservlet}'/>">

<input type="hidden" id="rectype" name="rectype" value="${recordTypeName}">
<input type="hidden" id="recid" name="recid" value="${rec.id}">
<%-- input type="hidden" id="typeid" name="typeid" value="1" --%><%-- 1==Course --%>
<input type="hidden" id="display" name="display" value="${not empty rec.display?rec.display:1}">

<div class="formline">
<!-- From Year -->
<span class="textfield" title="${constants.tooltips.startdate}">
  <label for="startdate" class="f_required">${constants.labels.startdate}</label>
  <input type="text"
         id="startdate" name="startdate"
         size="12" maxlength="20"
         value="${rec.startdate}"
         >
</span>

<!-- End Year -->
<span class="textfield" title="${constants.tooltips.enddate}">
  <label for="enddate">${constants.labels.enddate}</label>
  <input type="text"
         id="enddate" name="enddate"
         size="12" maxlength="20"
         value="${rec.enddate}"
         >
</span>
</div><!-- formline -->


<div class="formline">
<!-- Institution -->
<span class="textfield" title="${constants.tooltips.institution}">
  <label for="institution" class="f_required">${constants.labels.institution}</label>
  <input type="text"
         id="institution"
         name="institution"
         size="50" maxlength="100"
         value="${rec.institution}"
         >
</span>
</div><!-- formline -->

<div class="formline">
<!-- Location -->
<span class="textfield" title="${constants.tooltips.location}">
  <label for="location" class="f_required">${constants.labels.location}</label>
  <input type="text"
         id="location"
         name="location"
         size="50" maxlength="50"
         value="${rec.location}"
         >
</span>
</div><!-- formline -->



<div class="formline">
<!-- Job Title -->
<span class="textfield" title="${constants.tooltips.title}">
  <label for="title">${constants.labels.title}</label>
  <input type="text"
         id="title"
         name="title"
         size="50" maxlength="255"
         value="${rec.title}"
         >
</span>
</div><!-- formline -->

<div class="formline">
<!-- Annual Salary -->
<span class="textfield" title="${constants.tooltips.salary}">
  <label for="salary">${constants.labels.salary}</label>
  <input type="text"
         id="salary"
         name="salary"
         class="f_numeric"
         size="10" maxlength="20"
         value="${rec.salary}"
         >
</span>
</div><!-- formline -->

<div class="formline">
<!-- Remark -->
<span class="textfield" title="${constants.tooltips.remark}">
  <label for="remark">${constants.labels.remark}</label><br />
  <textarea
            id="remark" name="remark"
            rows="3" cols="54" wrap="soft"
            >${rec.remark}</textarea>
</span>
</div><!-- formline -->


<div class="buttonset">
 <input type="submit" id="save" name="save" value="Save" title="Save">
 <!-- button id="popupformcancel" name="popupformcancel" value="Cancel">Cancel</button -->
 <input type="submit" id="popupformcancel" name="popupformcancel" value="Cancel" title="Cancel">
</div>

</form>
