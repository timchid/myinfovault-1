<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%><%--
--%><%@ page import="java.util.List" %><%--
--%><%@ page import="edu.ucdavis.myinfovault.*" %><%--
--%><%@ taglib prefix="miv" uri="mivTags" %><%--
--%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%--
--%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %><%--
--%><!DOCTYPE html>
<html>
<head>
    <title>Find MyInfoVault Users</title>
    <script type="text/javascript">
        /* <![CDATA][ */
        var mivConfig = new Object();
        mivConfig.isIE = false;
        /* ]]> */
    </script>
    <%@ include file="/jsp/metatags.html" %>

    <link rel="stylesheet" type="text/css" href="<c:url value='/css/mivEnterData.css'/>">
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/miv-forms.css'/>">
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/data_table_gui.css'/>">

    <!--[if lt IE 8]>
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/iehacks.css'/>">
    <script type="text/javascript">mivConfig.isIE=true;</script>
    <![endif]-->
    <!--[if IE 8]>
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/ie8hacks.css'/>">
    <script type="text/javascript">mivConfig.isIE=true;</script>
    <![endif]-->

    <style type="text/css" media="screen">
        /*
         * Override styles needed due to the mix of different CSS sources!
         */
        .dataTables_info { padding-top: 0; }
        .dataTables_paginate { padding-top: 0; }
        /*.css_right { float: right; }*/
        #theme_links span { float: left; padding: 2px 10px; }
    </style>

    <script type="text/javascript" src="<c:url value='/js/mivcommon.js'/>"></script>
    <script type="text/javascript" src="<c:url value='/js/jquery.dataTables.min.js'/>"></script>
    <script type="text/javascript" src="<c:url value='/js/datatable/data_table_config.js'/>"></script>

    <script type="text/javascript">
    $(document).ready( function() {
        var oTable = fnInitDataTable('userlist', {"bHelpBar" : false,"aaSorting" : [[ 2, 'asc' ],[ 3, 'asc' ]],"aFilterColumns": []});
        setDefaultFocus();
    } );
    </script>

<c:set var="user" scope="request" value="${MIVSESSION.user}"/>

<c:set var="config" value="${MIVSESSION.config}"/>
</head>
<body>
<jsp:include page="/jsp/mivheader.jsp" />

<div id="breadcrumbs">
    <a href="<c:url value='/MIVMain'/>" title="Home">Home</a>
    &gt; MIV User Lookup
</div><!-- breadcrumbs -->

<div id="main">
    <h1>MIV User Lookup</h1>
    <miv:permit roles="SYS_ADMIN|VICE_PROVOST_STAFF">
    <div id="lookupform" class="mivDialog pagedialog">
        <div id="userheading">
            <span class="standout">${user.targetUserInfo.displayName}</span>
        </div>

        <form id="userid" name="userid" action="" method="post">
            <p class="formhelp">Look up user information from an MIV ID number, a first or last name, or email</p>

            <div class="formline">
            <span class="textfield" title="Enter a user ID number, partial user name, or partial email">
            <label for="userinput">User Info:</label>
            <input type="text"
                    id="userinput" name="userinput"
                    size="25" maxlength="40"
                    value="${param.userinput}"
                    autofocus
                    >
            </span>
            </div>

            <div class="buttonset">
                <input type="submit" id="userlookup" name="userlookup" value="Lookup" title="Lookup">
            </div>
        </form>
    </div><!--#lookupform-->
    </miv:permit>

    <%-- Process the form Submit, if any, before displaying the page --%>
    <c:if test="${pageContext.request.method=='POST'}"><div class="results" id="lookupresult">
    <c:if test="${fn:length(param.userlookup)>0}"><%--
    Looking up user &ldquo;${param.userinput}&rdquo;<br> --%>
    <%
    MIVConfig cfg = ((MIVSession)session.getAttribute("MIVSESSION")).getConfig();
    List l = cfg.lookupNames(request.getParameter("userinput"));
    pageContext.setAttribute("userlist", l, PageContext.PAGE_SCOPE);
    //out.write("Map: ["+m+"]<br><br>\n");
    %>
        <h2 id="resultlabel">Search Result(s) for &ldquo;<strong>${param.userinput}</strong>&rdquo;</h2>
        <div class="searchresults">Search Results=${fn:length(userlist)}</div>

        <miv:permit roles="SYS_ADMIN|VICE_PROVOST_STAFF">
        <div class="full_width">
            <table class="display utility" id="userlist">
                <thead>
                    <tr>
                        <th class="datatable-column-filter" title="User ID">User ID</th>
                        <th class="datatable-column-filter" title="MIV User">MIV User</th>
                        <th class="datatable-column-filter" title="School">School</th>
                        <th class="datatable-column-filter" title="Department">Department</th>
                        <th class="datatable-column-filter" title="Login">Login</th>
                        <th class="datatable-column-filter" title="Email">Email</th>
                        <th class="datatable-column-filter" title="Active">Active</th>
                        <th class="datatable-column-filter" title="Role">Role</th>
                    </tr>
                </thead>

                <tbody>
                    <c:forEach var="rec" items="${userlist}" varStatus="row">
                    <tr class="${rec.active == '1' ? 'row_active' : 'row_inactive'} ${(row.count)%2==0?'even':'odd'}">
                            <td>${rec.userid}</td>
                            <td>${rec.givenname} ${rec.surname}</td>
                            <td>${rec.schoolname}</td>
                            <td>${rec.departmentname}</td>
                            <td>${rec.role=="Appointee" ? "" : rec.login}</td>
                            <td>${rec.email}</td>
                            <td>${rec.active == "1" ? "Yes" : "No"}</td>
                            <td>${rec.role}</td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </div>

        <%-- <c:forEach var="rec" items="${userlist}"> <c:set var="matchTotal" value="${matchTotal+1}"/>
        <div class="match active${rec.active}">
            <strong>User&nbsp;ID:</strong>&nbsp;${rec.userid} &nbsp;
            <strong>Name:</strong>&nbsp;${rec.givenname} ${rec.surname} &nbsp;
            <strong>Login:</strong>&nbsp;${rec.login} &nbsp;
            <strong>Email:</strong>&nbsp;${rec.email} &nbsp;
            <strong>Active:</strong> ${rec.active == "1" ? "Yes" : "No"} &nbsp;
            <br>
            &nbsp; <strong>Role:</strong> ${rec.role} &nbsp;
            <strong>School:</strong> ${rec.schoolname} &nbsp;
            <strong>Dept:</strong> ${rec.departmentname} &nbsp;
        </div><!-- match -->
        </c:forEach>

    <p>${matchTotal} match${matchTotal==1?'':'es'} found.</p> --%>
        </miv:permit>
    </c:if><%-- if 'userlookup' button --%>
    </div><!-- #lookupresult.results -->
    </c:if><%-- if POST --%>

</div><!--main-->
<jsp:include page="/jsp/miv_small_footer.jsp" />
</body>
</html>
