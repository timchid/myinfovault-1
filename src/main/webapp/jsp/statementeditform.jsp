<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%--
--%><c:set var="user" scope="request" value="${MIVSESSION.user}"/><%--
--%><c:if test="${constants.config.usepopups}">
<head>
<meta http-equiv="cache-control" content="no-cache, must-revalidate">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="expires" content="0">

</head>
<!-- Using Pop-Up edit dialogs -->
<script type="text/javascript">
if (typeof _container_ != 'undefined') {
    var btn = document.getElementById("popupformcancel");
    _container_.setCloseControl(btn);
}
</script>
</c:if><%-- usepopups --%>

<jsp:scriptlet><![CDATA[
   Integer d = new Integer(java.util.Calendar.getInstance().get(java.util.Calendar.YEAR));
   pageContext.setAttribute("thisYear", d, PageContext.REQUEST_SCOPE);
   ]]></jsp:scriptlet>
   
<script type="text/javascript"><!--
    mivFormProfile = {
       required: ["year"],
       trim: ["year","content"],
       constraints: {
           year: function(contents) {
               var curYear = new Date().getFullYear();
               
               if(isNaN(contents)){
               	return "INVALID_NUMBER";
               }
               
               var numyear = contents * 1;
               
               if( numyear < 1900 || numyear > (curYear +1) ){
               	return "INVALID_YEAR";
               }
               
               return true;
           }
         },
         errormap: {"INVALID_YEAR":"Year must be from 1900 to ${thisYear+1}" }
    };
// -->
</script>

<div id="userheading">
	<span class="standout">${user.targetUserInfo.displayName}</span>
</div>


<!-- This is the data entry form for "${recordTypeName}" records -->
<jsp:include page="/jsp/wysiwyginit.inc"/>
<div id="uploadlink"><%--
--%><c:url var="nextLink" value="/PdfUpload"><%--
--%>    <c:param name="_flowId" value="pdfupload-external-flow"/><%--
--%>    <c:param name="documentAttributeName" value="candidatestatement"/><%--
--%>    <c:param name="isYearField" value="true"/><%--
--%></c:url>
<a href="<c:out value='${nextLink}' escapeXml='true'/>" title="Upload a PDF">Upload a PDF</a> or enter data in the fields below.
</div>
<form class="mivdata" id="mytestform" name="mytestform"
      method="post" action="<c:url value='${constants.config.saveservlet}'/>">

<input type="hidden" id="rectype" name="rectype" value="${recordTypeName}">
<input type="hidden" id="recid" name="recid" value="${rec.id}">
<input type="hidden" id="display" name="display" value="${not empty rec.display?rec.display:1}">


<div class="formline">
<span class="textfield" title="${constants.tooltips.year}">
  <label for="year" class="f_required">${constants.labels.year}</label>
  <input type="text" id="year" name="year" size="5" maxlength="4"
			value="${rec.year}" title="Year must be from 1900 to ${thisYear+1}">
</span>
</div><!-- formline -->


<!-- Content -->
<div class="formline">
  <span class="textfield" title="${constants.tooltips.content}">&#8224;&nbsp;
  <label for="content" class="f_required">${constants.labels.content}</label><br />
  <textarea
            id="content" name="content"
            rows="10" cols="113" wrap="soft" class="mceEditor"
            >${rec.content}</textarea>
</span>
</div><!-- formline -->

<%--
<div id="displaycb" class="cb">
  <div class="formline">
  <span class="checkbox" title="${constants.tooltips.display}">
    <input type="checkbox" id="display" name="display"${rec.display=='1'||rec.display=='true'?" checked":""}>
    <label for="display">${constants.labels.display}</label>
  </span>
  </div><!-- formline -->
</div><!-- displaycb -->
--%>
<div class="buttonset">
 <input type="submit" id="save" name="save" value="Save" title="Save" />
 <input type="submit" id="popupformcancel" name="popupformcancel" value="Cancel" title="Cancel" />
</div>

</form>
