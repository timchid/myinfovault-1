<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%--
--%><c:set var="user" scope="request" value="${MIVSESSION.user}"/><%--
--%><c:if test="${usepopups || constants.config.usepopups}"><%--
--%><!DOCTYPE html>
<head>
    <meta http-equiv="cache-control" content="no-cache, must-revalidate">
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="expires" content="0">
        
        <%@ include file="/jsp/metatags.html" %>

    <script type="text/javascript" src="<c:url value='/js/mivcommon.js'/>"></script>
    
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/mivcommon.css'/>">
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/mivEnterData.css'/>">
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/miv-forms.css'/>">
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/creativeactivities.css'/>">
</head>
<!-- Using Pop-Up edit dialogs -->
<script type="text/javascript">
    if (typeof _container_ != 'undefined') {
        var btn = document.getElementById("popupformcancel");
        _container_.setCloseControl(btn);
    }
</script>
</c:if><%-- usepopups --%>

<c:if test="${ !(usepopups || constants.config.usepopups) }">
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/expandable.css'/>">
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/creativeactivities.css'/>">

    <script type="text/javascript" src="<c:url value='/js/creativeactivities/works.js'/>"></script>
    <script type="text/javascript" src="<c:url value='/js/creativeactivities/creativeactivities.js'/>"></script>
    <script type="text/javascript" src="<c:url value='/js/popupeditform.js'/>"></script>
</c:if>
<c:if test="${ (usepopups || constants.config.usepopups) }"><%--SDP added: always use works.js ... trying to get Work Type working --%>
    <script type="text/javascript" src="<c:url value='/js/creativeactivities/works.js'/>"></script>
</c:if>

<jsp:scriptlet><![CDATA[
   Integer d = new Integer(java.util.Calendar.getInstance().get(java.util.Calendar.YEAR));
   pageContext.setAttribute("thisYear", d, PageContext.REQUEST_SCOPE);
   ]]></jsp:scriptlet>

<c:if test="${ !(usepopups || constants.config.usepopups) }">
<script type="text/javascript">
//<![CDATA[
   var mivFormProfile = {
        required: [ "workyear","worktypeid","statusid","creators","title","description"],
        trim: [ "workyear","worktypeid","statusid","creators","title","description" ],
        constraints: {
            workyear: function(contents) {
                var curYear = new Date().getFullYear();

                if (isNaN(contents)) {
                    return "INVALID_NUMBER";
                }

                var numyear = contents * 1;

                if ( numyear < 1900 || numyear > (curYear +2) ) {
                    return "INVALID_YEAR";
                }

                return true;
            },
            worktypeid: miv.validation.isDropdown,
            statusid: miv.validation.isDropdown,
            url: miv.validation.isLink
        },
        errormap: {"INVALID_YEAR":"Year must be from 1900 to ${thisYear+2}." }
    };
//]]>
</script>
</c:if>

<c:if test="${usepopups || constants.config.usepopups}">
<body class="usepopups workseditform">

    <div id="main">
        <div class="pagedialog" id="maindialog">
            <div class="dojoDialog mivDialog"><!--might want to be more flexible with dialog widths-->
</c:if>

<div id="userheading">
    <span class="standout">${user.targetUserInfo.displayName}</span>
</div>

<!-- dummy hidden values -->
<input type="hidden" id="usepopups" value="${ (usepopups || constants.config.usepopups) }">
<input type="hidden" id="thisYear" value="${thisYear}">


<!-- This is the data entry form for "${recordTypeName}" records -->
<form class="mivdata" id="mytestform" name="mytestform"
      method="post" action="<c:url value='${constants.config.saveservlet}'/>">

    <input type="hidden" id="class" name="recordClass" value="${recordClass}">
    <input type="hidden" id="rectype" name="rectype" value="${recordTypeName}">
    <input type="hidden" id="recid" name="recid" value="${rec.id}">
    <input type="hidden" id="display" name="display" value="${not empty rec.display?rec.display:1}">

    <div class="formline">
        <span class="textfield" title="${constants.tooltips.workyear}">
            <label for="workyear" class="f_required">${constants.labels.workyear}</label>
            <input type="text" id="workyear" name="workyear" size="5" maxlength="4"
                   value="${rec.workyear}" title="Year must be from 1900 to ${thisYear+2}">
        </span>
    </div>
    <c:set var="racworktype" value=""/>
    <!-- non suggestions block  -->
    <div class="formline">
        <span class="dropdown" title="${constants.tooltips.worktypeid}">
            <label for="worktypeid" class="f_required">${constants.labels.worktypeid}</label>
            <select id="worktypeid" name="worktypeid" >
                <option value="0">--- Select ---</option><%--
            --%><c:forEach var="opt" items="${constants.options.worktype}">
                <option value="${opt.id}"${rec.worktypeid==opt.id?' selected="selected"':''} title="${opt.tip}">${opt.value}</option><%--
            --%><c:if test="${rec.worktypeid==opt.id}"><%--
                --%><c:set var="racworktype" value="${opt.value}"/><%--
            --%></c:if><%--
            --%></c:forEach>
            </select>
        </span>
        <a href="<c:url value='/help/worktype.html'/>" target="mivhelp">Help! My work type is not listed</a>
    </div>

    <div class="formline">
        <span class="textfield" title="${constants.tooltips.statusid}">
            <label for="statusid" class="f_required">${constants.labels.statusid}</label>
            <select id="statusid" name="statusid"><%--
            --%><c:forEach var="opt" items="${constants.options.workstatus}">
                <option value="${opt.id}"${ ((rec.statusid==null && opt.id==1)? ' selected="selected"': ((rec.statusid==opt.id)?' selected="selected"':''))} title="${opt.tip}">${opt.value}</option><%--
            --%></c:forEach>
            </select>
        </span>
    </div><!--formline-->

    <div class="formline">
        <span class="textfield" title="${constants.tooltips.creators}">
            <label for="creators" class="f_required">${constants.labels.creators}</label><br>
            <textarea class="f_required"
                      id="creators" name="creators"
                      rows="2" cols="54" wrap="soft"
                      >${rec.creators}</textarea>
        </span>
    </div><!--formline-->

    <div class="formline">
        <span class="textfield" title="${constants.tooltips.title}">
            <label for="title" class="f_required">${constants.labels.title}</label> <span>(Do not add a period to the end of your Title data)</span><br>
            <textarea class="f_required"
                      id="title" name="title"
                      rows="2" cols="54" wrap="soft"
                      >${rec.title}</textarea>
        </span>
    </div><!--formline-->

    <div class="formline">
        <span class="textfield" title="${constants.tooltips.description}">
            <label for="description" class="f_required">${constants.labels.description}</label><br>
            <textarea class="f_required"
                      id="description" name="description"
                      rows="2" cols="54" wrap="soft"
                      >${rec.description}</textarea>
        </span>
    </div><!--formline-->

    <div class="formline">
        <span class="textfield" title="${constants.tooltips.url}">
            <label for="url">${constants.labels.url}</label>
            <input type="url" pattern="(https?|ftp):\/\/.+"
                   placeholder="http://example.com/some+page/some_document.pdf"
                   id="url" name="url"
                   size="52" maxlength="255"
                   value="${rec.url}">
        </span>
    </div><!--formline-->

    <div class="formline">
        <span class="textfield" title="${constants.tooltips.contributors}">
        <label for="contributors">${constants.labels.contributors}</label><br>
        <textarea id="contributors" name="contributors"
                  rows="3" cols="54" wrap="soft"
                  >${rec.contributors}</textarea>
        </span>
    </div><!--formline-->

<c:if test="${ !(usepopups || constants.config.usepopups) }">
    <input type="hidden" id="selectedids" name="selectedids" value="">  
    <fieldset id="associate_fieldset" name="associate_fieldset" class="collapsible ">
        <legend>Associate Events or Publication Events with this Work</legend>
        <div class="hint">expand to see association form</div>

        <div id="association">

            <!-- association error field -->
            <label for="association" class="hidden">Associate Events</label>

            <div class="associationbuttons">
                <div class="buttonset">
                    <input type="button" id="newevent" name="newevent"
                           value="Add a New Event" title="Add a New Event" onclick="" >
                    <input type="button" id="newpublicationevent" name="newpublicationevent"
                           value="Add a New Publication Event" title="Add a New Publication Event" onclick="">
                </div>
            </div>

            <div class="formline">
                <iframe id="associationframe" name="associationframe"
                        src="<c:url value='/Associate?requestType=init&rectype=${recordTypeName}&recid=${rec.id}&year=${rec.workyear}'/>"
                        frameborder="0" scrolling="yes">
                </iframe>
            </div><!--formline-->

        </div><!-- association -->
    </fieldset>
</c:if>

<c:if test="${ !(usepopups || constants.config.usepopups) }"><%-- why is this tested again, instead of just leaving the previous c:if open? --%>
    <div class="buttonset">
        <input type="submit" id="save" name="save" value="Save" title="Save" >
        <input type="submit" id="popupformcancel" name="popupformcancel" value="Cancel" title="Cancel">
    </div>
</c:if>

<c:if test="${ !(usepopups || constants.config.usepopups) }"><%-- and tested yet again! --%>
    <div id="dialogBox">
        <div id="dialog-form" title="Add a New Event"><%-- Add form at runtime --%>
        </div>

        <div id="dialog-pubform" title="Add a New Publication Event"><%-- Add form at runtime --%>
        </div>
    </div><!-- dialogBox -->
</c:if>

</form>

<c:if test="${usepopups || constants.config.usepopups}">
</div>
</div>
</div>
</body>
</c:if>
