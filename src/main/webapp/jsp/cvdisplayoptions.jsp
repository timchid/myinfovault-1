<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%--
--%><%@ taglib prefix="spring" uri="/spring" %><%--
--%>
          <form class="mivdata biosketchdisplayoptions" id="biosketchform" method="post" action="<c:url value='/biosketch/BiosketchDisplayOptions'/>">
            
            <input type="hidden" id="cancelurl" value="<c:url value='/biosketch/BiosketchPreview'/>" >
            
            <div class="formline" title="${constants.tooltips.documentname}">
              <span class="textfield"  title="${constants.tooltips.documentname}">
                <label for="name" class="f_required" errorlabel="${constants.tooltips.documentname}">${constants.labels.documentname}</label>
                <spring:bind path="biosketchCommand.name">
                  <input type="text"
                         id="name" name="name"
                         size="33" maxlength="255"
                         value="${biosketchCommand.name}">
                </spring:bind>
              </span>
            </div><!--formline-->

            <div class="formline">
              <span class="textfield"  title="${constants.tooltips.documentitle}">
                <label for="title" class="f_required" errorlabel="${constants.tooltips.documentitle}">${constants.labels.documenttitle}</label>
                <spring:bind path="biosketchCommand.title">
                  <input type="text"
                         id="title" name="title"
                         size="33" maxlength="50"
                         value="${biosketchCommand.title}">
                </spring:bind>
              </span>
            </div><!--formline-->


            <div><!--  display title -->
              <span class="formsection" title="${constants.tooltips.titledisplay}">
              ${constants.labels.titledisplay}
              </span>
            </div><!--  display title -->

            <c:set var="titleboldselected" value="${biosketchCommand.titleBold=='1'}"/><%--
        --%><c:set var="titleitalicselected" value="${biosketchCommand.titleItalic=='1'}"/><%--
        --%><c:set var="titleunderlineselected" value="${biosketchCommand.titleUnderline=='1'}"/><%--
        --%>
            <div class="formlabel">
                <span class="formlabel" title="${constants.tooltips.format}">${constants.labels.format}</span>
<%--            <label>${constants.labels.format}</label>
              </span>--%>
<%--              <span class="checkbox" title="${constants.tooltips.format}">${constants.labels.format}</span> --%>

              <span class="formatbold" title="${constants.tooltips.formatbold}">
                <spring:bind path="biosketchCommand.titleBold">
                <input type="hidden" name="_titleBold" value="visible">
                <input type="checkbox" id="titleBold" name="titleBold" value="1"${titleboldselected?" checked":""}>
                <label for="titleBold">${constants.labels.bold}</label>
                </spring:bind>&nbsp;
              </span>

              <span class="formatitalic" title="${constants.tooltips.formatitalic}">
                <spring:bind path="biosketchCommand.titleItalic">
                <input type="hidden" name="_titleItalic" value="visible">
                <input type="checkbox" id="titleItalic" name="titleItalic" value="1"${titleitalicselected?" checked":""}>
                <label for="titleItalic">${constants.labels.italic}</label>
                </spring:bind>&nbsp;
              </span>

              <span class="formatunderline" title="${constants.tooltips.formatunderline}">
                <spring:bind path="biosketchCommand.titleUnderline">
                <input type="hidden" name="_titleUnderline" value="visible">
                <input type="checkbox" id="titleUnderline" name="titleUnderline" value="1"${titleunderlineselected?" checked":""}>
                  <label for="titleUnderline" class="formatunderline">${constants.labels.underline}</label>
                </spring:bind>
              </span>
            </div><!-- formlabel -->

            <c:set var="titlerulesselected" value="${biosketchCommand.titleRules=='1'}"/>

            <div class="formlabel"><!-- title rules -->
              <span class="formlabel" title="${constants.tooltips.titlerules}">
                ${constants.labels.titlerules}
              </span>
              <span class="radioset">
                <span class="radiobutton" title="${constants.tooltips.yes}">
                    <spring:bind path="biosketchCommand.titleRules">
                    <label>${constants.labels.yes}
                    <input type="radio"  name="titleRules" value="1"${titlerulesselected?" checked":""}></label>&nbsp;
                    </spring:bind>
                </span>
                <span class="radiobutton" title="${constants.tooltips.no}">
                    <spring:bind path="biosketchCommand.titleRules">
                    <label>${constants.labels.no}
                    <input type="radio"  name="titleRules" value="0"${!titlerulesselected?" checked":""}></label>&nbsp;
                    </spring:bind>
                </span>
              </span><!-- radioset -->
            </div><!-- title rules -->

            <c:set var="titlealignleftselected" value="${biosketchCommand.titleAlign=='LEFT'}"/>
            <c:set var="titlealigncentertselected" value="${biosketchCommand.titleAlign=='CENTER'}"/>
            <c:set var="titlealignrightselected" value="${biosketchCommand.titleAlign=='RIGHT'}"/>
            <div class="formlabel"><!--title alignment -->
              <span class="formlabel" title="${constants.tooltips.alignment}">
                ${constants.labels.alignment}
              </span>
              <span class="radioset">
                <span class="radiobutton" title="${constants.tooltips.left}">
                    <spring:bind path="biosketchCommand.titleAlign">
                    <label>${constants.labels.left}
                    <input type="radio" name="titleAlign"  value="LEFT" ${titlealignleftselected?" checked":""}></label>&nbsp;
                    </spring:bind>
                </span>
                <span class="radiobutton" title="${constants.tooltips.center}">
                    <spring:bind path="biosketchCommand.titleAlign">
                    <label>${constants.labels.center}
                    <input type="radio" name="titleAlign"  value="CENTER" ${titlealigncentertselected?" checked":""}></label>&nbsp;
                    </spring:bind>
                </span>
                <span class="radiobutton" title="${constants.tooltips.right}">
                    <spring:bind path="biosketchCommand.titleAlign">
                    <label>${constants.labels.right}
                    <input type="radio" name="titleAlign" title="${constants.tooltips.right}" value="RIGHT" ${titlealignrightselected?" checked":""}></label>
                    </spring:bind>
                </span>
              </span><!-- radioset -->
            </div><!-- title alignment-->


            <div><!-- display name -->
              <span class="formsection" title="${constants.tooltips.displayname}">
                ${constants.labels.displayname}
              </span>
            </div><!-- display name -->

            <c:set var="displayalignleftselected" value="${biosketchCommand.displayNameAlign=='LEFT'}"/>
            <c:set var="displayaligncentertselected" value="${biosketchCommand.displayNameAlign=='CENTER'}"/>
            <c:set var="displayalignrightselected" value="${biosketchCommand.displayNameAlign=='RIGHT'}"/>
            <div class="formlabel"><!-- display alignment -->
              <span class="formlabel" title="${constants.tooltips.alignment}">
                ${constants.labels.alignment}
              </span>
              <span class="radioset">
                <span class="radiobutton" title="${constants.tooltips.left}">
                    <spring:bind path="biosketchCommand.displayNameAlign">
                    <label>${constants.labels.left}
                    <input type="radio" name="displayNameAlign" value="LEFT" ${displayalignleftselected?" checked":""}></label>&nbsp;
                    </spring:bind>
                </span>
                <span class="radiobutton" title="${constants.tooltips.center}">
                    <spring:bind path="biosketchCommand.displayNameAlign">
                    <label>${constants.labels.center}
                    <input type="radio" name="displayNameAlign" value="CENTER" ${displayaligncentertselected?" checked":""}></label>&nbsp;
                    </spring:bind>
                </span>
                <span class="radiobutton" title="${constants.tooltips.right}">
                    <spring:bind path="biosketchCommand.displayNameAlign">
                    <label>${constants.labels.right}
                    <input type="radio" name="displayNameAlign" value="RIGHT" ${displayalignrightselected?" checked":""}></label>
                    </spring:bind>
                </span>
              </span><!-- radioset -->
            </div><!--display alignment-->


            <div><!-- header display -->
              <span class="formsection" title="${constants.tooltips.headerdisplay}">
                ${constants.labels.headerdisplay}
              </span>
            </div><!-- header display -->

            <c:set var="headerboldselected" value="${biosketchCommand.headerBold=='1'}"/>
            <c:set var="headeritalicselected" value="${biosketchCommand.headerItalic=='1'}"/>
            <c:set var="headerunderlineselected" value="${biosketchCommand.headerUnderline=='1'}"/>
            <div class="formlabel">
              <span class="formlabel" title="${constants.tooltips.format}">
                ${constants.labels.format}
              </span>
              <span class="formatbold" title="${constants.tooltips.formatbold}">
                  <spring:bind path="biosketchCommand.headerBold">
                  <input type="hidden" name="_headerBold" value="visible">
                  <input type="checkbox" id="headerBold" name="headerBold" value="1" ${headerboldselected?"checked":""}>
                  <label for="headerBold">${constants.labels.bold}</label>
                  </spring:bind>&nbsp;
              </span>
              <span class="formatitalic" title="${constants.tooltips.formatitalic}">
                  <spring:bind path="biosketchCommand.headerItalic">
                  <input type="hidden" name="_headerItalic" value="visible">
                  <input type="checkbox" id="headerItalic" name="headerItalic" value="1"  ${headeritalicselected?"checked":""}>
                  <label for="headerItalic">${constants.labels.italic}</label>
                  </spring:bind>&nbsp;
              </span>
              <span class="formatunderline" title="${constants.tooltips.formatunderline}">
                  <spring:bind path="biosketchCommand.headerUnderline">
                  <input type="hidden" name="_headerUnderline" value="visible">
                  <input type="checkbox" id="headerUnderline" name="headerUnderline" value="1" ${headerunderlineselected?"checked":""}>
                  <label for="headerUnderline">${constants.labels.underline}</label>
                  </spring:bind>
              </span>
            </div><!-- formlabel -->

            <c:set var="headeralignleftselected" value="${biosketchCommand.headerAlign=='LEFT'}"/>

            <div class="formlabel"><!-- header alignment -->
              <span class="formlabel" title="${constants.tooltips.alignment}">${constants.labels.alignment}</span>
              <span class="radioset">
                <span class="radiobutton" title="${constants.tooltips.left}">
                    <spring:bind path="biosketchCommand.headerAlign">
                    <label>${constants.labels.left}
                    <input type="radio" name="headerAlign" id="headerLAlign" value="LEFT" ${headeralignleftselected?" checked":""}></label>&nbsp;
                    </spring:bind>
                </span>

                <c:set var="headerindentselected" value="${biosketchCommand.headerIndent}"/>
                <span class="formline" title="${constants.tooltips.indent}">
                  <label for="headerIndent">${constants.labels.indent}</label>
                </span>
                <spring:bind path="biosketchCommand.headerIndent">
                <span class="dropdown" title="${constants.tooltips.indent}">
                  <select id="headerIndent" name="headerIndent" ${!headeralignleftselected?" disabled":""}>
                    <option value="0" ${headerindentselected=='0'?" selected":""}>0</option>
                    <option value="18" ${headerindentselected=='18'?" selected":""}>0.25</option>
                    <option value="36" ${headerindentselected=='36'?" selected":""}>0.5</option>
                    <option value="54" ${headerindentselected=='54'?" selected":""}>0.75</option>
                    <option value="72" ${headerindentselected=='72'?" selected":""}>1</option><%-- " (this stray quote fixex syntax highlighting) --%>
                  </select> inches
                </span>
                </spring:bind>&nbsp;&nbsp;

                <span class="radiobutton" title="${constants.tooltips.center}">
                    <spring:bind path="biosketchCommand.headerAlign">
                    <label>${constants.labels.center}
                    <input type="radio" name="headerAlign"  id="headerCAlign" value="CENTER" ${!headeralignleftselected?" checked":""}></label>&nbsp;
                    </spring:bind>
                </span>
              </span><!-- radioset -->
            </div><!-- header alignment -->

            <div><!-- Content display -->
              <span class="formsection" title="${constants.tooltips.contentdisplay}">
                ${constants.labels.contentdisplay}
              </span>
            </div><!-- Content display -->

            <div class="formlabel"><!-- body indent -->
              <c:set var="bodyindentselected" value="${biosketchCommand.bodyIndent}"/>
              <span class="formlabel" title="${constants.tooltips.indent}">
                <label for="bodyIndent">${constants.labels.indent}</label>
              </span>
              <spring:bind path="biosketchCommand.bodyIndent">
              <span class="dropdown" title="${constants.tooltips.indent}">
                  <select id="bodyIndent" name="bodyIndent">
                    <option value="0" ${bodyindentselected=='0'?" selected":""}>0</option>
                    <option value="18" ${bodyindentselected=='18'?" selected":""}>0.25</option>
                    <option value="36" ${bodyindentselected=='36'?" selected":""}>0.5</option>
                    <option value="54" ${bodyindentselected=='54'?" selected":""}>0.75</option>
                    <option value="72" ${bodyindentselected=='72'?" selected":""}>1</option><%-- " (this stray quote fixex syntax highlighting) --%>
                  </select> inches
              </span>
              </spring:bind>
            </div><!-- body indent -->

            <c:set var="bodyformattingselected" value="${biosketchCommand.bodyFormatting=='1'}"/>
            <div class="formlabel"><!-- body formatting -->
              <span class="formlabel" title="${constants.tooltips.includeformatoptions}">
                ${constants.labels.includeformatoptions}
              </span>
              <span class="radioset">
                <span class="radiobutton" title="${constants.tooltips.yes}">
                    <spring:bind path="biosketchCommand.bodyFormatting">
                    <label>${constants.labels.yes}
                    <input type="radio" name="bodyFormatting" value="1" ${bodyformattingselected?" checked":""}></label>&nbsp;
                    </spring:bind>
                </span>
                <span class="radiobutton" title="${constants.tooltips.no}">
                    <spring:bind path="biosketchCommand.bodyFormatting">
                    <label>${constants.labels.no}
                    <input type="radio" name="bodyFormatting" value="0" ${!bodyformattingselected?" checked":""}></label>&nbsp;
                    </spring:bind>
                </span>
              </span><!-- radioset -->
            </div><!-- body formatting -->

            <div><!-- Footer -->
              <span class="formsection" title="${constants.tooltips.footer}">
                ${constants.labels.footer}
              </span>
            </div><!-- Footer -->

            <c:set var="pagenumbersselected" value="${biosketchCommand.footerPageNumbers=='1'}"/>
            <div class="formlabel"><!-- Page Numbers -->
              <span class="formlabel" title="${constants.tooltips.pagenumbers}">
                ${constants.labels.pagenumbers}
              </span>

              <span class="radioset">
                <span class="radiobutton" title="${constants.tooltips.yes}">
                    <spring:bind path="biosketchCommand.footerPageNumbers">
                    <label>${constants.labels.yes}
                    <input type="radio" name="footerPageNumbers" value="1" ${pagenumbersselected?" checked":""}></label>&nbsp;
                    </spring:bind>
                </span>
                <span class="radiobutton" title="${constants.tooltips.no}">
                    <spring:bind path="biosketchCommand.footerPageNumbers">
                    <label>${constants.labels.no}
                    <input type="radio" name="footerPageNumbers" value="0" ${!pagenumbersselected?" checked":""}></label>&nbsp;
                    </spring:bind>
                </span>
              </span><!-- radioset -->
            </div><!-- Page Numbers -->

            <c:set var="footerrulesselected" value="${biosketchCommand.footerRules=='1'}"/>
            <div  class="formlabel"><!-- Page rules -->
              <span class="formlabel" title="${constants.tooltips.footerrules}">
                ${constants.labels.footerrules}
              </span>

              <span class="radioset">
                <span class="radiobutton" title="${constants.tooltips.yes}">
                    <spring:bind path="biosketchCommand.footerRules">
                    <label>${constants.labels.yes}
                    <input type="radio" name="footerRules" value="1" ${footerrulesselected?" checked":""}></label>&nbsp;
                    </spring:bind>
                </span>
                <span class="radiobutton" title="${constants.tooltips.no}">
                    <spring:bind path="biosketchCommand.footerRules">
                    <label>${constants.labels.no}
                    <input type="radio" name="footerRules" value="0" ${!footerrulesselected?" checked":""}></label>&nbsp;
                    </spring:bind>
                </span>
              </span><!-- radioset -->
            </div><!-- Page rules -->

            <div class="buttonset">
              <c:if test="${showwizard =='true'}">
              <input type="submit" id="save" name="btnSubmit" value="Proceed" title="Proceed">
              </c:if>
              <c:if test="${showwizard !='true'}">
              <input type="submit" id="save" name="btnSubmit" value="Save" title="Save">
              </c:if>
              <input type="reset" name="popupformcancel" id="popupformcancel" value="Cancel" title="Cancel">
            </div>
          </form>
<%--
 vim:ts=8 sw=2 et sr:
--%>
