<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%--
--%><c:set var="user" scope="request" value="${MIVSESSION.user}"/><%--
--%><c:if test="${constants.config.usepopups}">
<head>
<meta http-equiv="cache-control" content="no-cache, must-revalidate">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="expires" content="0">
</head>
<!-- Using Pop-Up edit dialogs -->
<script type="text/javascript">
if (typeof _container_ != 'undefined') {
    var btn = document.getElementById("popupformcancel");
    _container_.setCloseControl(btn);
}
</script>
</c:if><%-- usepopups --%>

<jsp:scriptlet><![CDATA[
   Integer d = new Integer(java.util.Calendar.getInstance().get(java.util.Calendar.YEAR));
   pageContext.setAttribute("thisYear", d, PageContext.REQUEST_SCOPE);
   ]]></jsp:scriptlet>

<script type="text/javascript">
    mivFormProfile = {
      required: ["institution"],
      trim: ["degree", "institution", "location",
             "startdate","startmonthid","enddate","endmonthid","remark"],
      constraints: {
          startdate: function(contents) {
            /* if (trim(contents).length == 0 && dojo.byId('startmonthid').value <= 0)
            {
                return "YEAR_MONTH_REQUIRED";
            } */

            if (trim(contents).length == 0)
            {
                return "YEAR_REQUIRED";
            }

            /* if (dojo.byId('startmonthid').value <= 0)
            {
                return "MONTH_REQUIRED";
            } */

            var curYear = new Date().getFullYear();
            if (isNaN(contents)) {
                return "INVALID_NUMBER";
            }
            var numyear = contents * 1;
            if ( numyear < 1900 || numyear > curYear ) {
                return "INVALID_YEAR";
            }

            return true;
        },
        enddate: function(contents) {

            /* if (trim(contents).length == 0 && dojo.byId('endmonthid').value <= 0)
            {
                return "YEAR_MONTH_REQUIRED";
            } */

            if (trim(contents).length == 0)
            {
                return "YEAR_REQUIRED";
            }

            /* if (dojo.byId('endmonthid').value <= 0)
            {
                return "MONTH_REQUIRED";
            } */

            var curYear = new Date().getFullYear();
            if (isNaN(contents)) {
                return "INVALID_NUMBER";
            }

            var startdate = dojo.byId('startdate').value + "." + leftPad(dojo.byId('startmonthid').value,2);
            var enddate = contents + "." + leftPad(dojo.byId('endmonthid').value,2);

            if ( dojo.byId('startdate') && trim(dojo.byId('startdate').value).length > 0 && !isNaN(dojo.byId('startdate').value)
                 && dojo.byId('startmonthid').value > 0 && parseFloat(startdate) > parseFloat(enddate) ) {
                return "INVALID_END_DATE";
            }

            return true;
        }
      },
      errormap: {
        "INVALID_YEAR":"Year must be from 1900 to ${thisYear}.",
        "INVALID_END_DATE":"To Year and Month must be greater than From Year and Month.",
        "YEAR_MONTH_REQUIRED": "Year and Month are required.",
        "MONTH_REQUIRED": "Month is required.",
        "YEAR_REQUIRED":"Year is required."
      }
    };
// -->
</script>

<div>
    <div id="userheading"><span class="standout">${user.targetUserInfo.displayName}</span></div>
    <div id="links">
        <%-- <div id="specchar"><a href="#">Add Special Characters</a></div>
             <div id="format"><a href="#">Bold / Italic / Underline</a></div> --%>
    </div>
</div>

<p class="formhelp">
Note that Month of degree may be required for NIH biosketch.
</p>

<%-- <!-- record: ${rec} --> --%>
<%-- constants: ${constants} --%>
<%-- constants.labels: ${constants.labels} --%>

<!-- This is the data entry form for "${recordTypeName}" records -->
<form class="mivdata" id="mytestform"
      method="post" action="<c:url value='${constants.config.saveservlet}'/>">

<input type="hidden" id="rectype" name="rectype" value="${recordTypeName}">
<input type="hidden" id="recid" name="recid" value="${rec.id}">
<input type="hidden" id="display" name="display" value="${not empty rec.display?rec.display:1}">



<div class="formline">
<!-- Degree -->
<span class="textfield" title="${constants.tooltips.degree}">
  <label for="degree">${constants.labels.degree}</label>
  <input type="text"
         id="degree" name="degree"
         size="15" maxlength="30"
         value="${rec.degree}"
         >
</span>
</div><!-- formline -->

<div class="formline">
<!-- Institution -->
<span class="textfield" title="${constants.tooltips.institution}">
  <label for="institution" class="f_required">${constants.labels.institution}</label>
  <input type="text"
         id="institution"
         name="institution"
         size="50" maxlength="100"
         value="${rec.institution}"
         >
</span>
</div><!-- formline -->

<div class="formline">
<!-- Location -->
<span class="textfield" title="${constants.tooltips.location}">
  <label for="location">${constants.labels.location}</label>
  <input type="text"
         id="location"
         name="location"
         size="50" maxlength="50"
         value="${rec.location}"
         >
</span>
</div><!-- formline -->

<div class="formline">
<!-- From Year -->
<span class="textfield" title="${constants.tooltips.startdate}">
  <label errorlabel="From Year and Month" for="startdate" class="f_required">${constants.labels.startdate}</label>
  <input type="text" id="startdate" name="startdate" size="5" maxlength="4"
         value="${rec.startdate}" title="Year must be from 1900 to ${thisYear}">
</span>
<!-- From Month -->
<span class="dropdown" title="${constants.tooltips.startmonthid}">
   <label errorlabel="From Month" for="startmonthid">${constants.labels.startmonthid}</label>
   <select id="startmonthid" name="startmonthid">
    <c:forEach var="opt" items="${constants.options.monthname}"><%--
--%> <option value="${opt.id}"${rec.startmonthid==opt.id?" selected=\"selected\"":""}>${opt.name}</option>
    </c:forEach><%--
--%></select>
</span>

</div><!-- formline -->

<div class="formline">
<!-- End Year -->
<span class="textfield" title="${constants.tooltips.enddate}">
  <label errorlabel="To Year and Month" for="enddate" class="f_required">${constants.labels.enddate}</label>
  <input type="text" id="enddate" name="enddate" size="5" maxlength="4"
         value="${rec.enddate}" title="Year must be from 1900 to From Year">

</span>
<!-- End Month -->
<span class="dropdown" title="${constants.tooltips.endmonthid}">
   <label errorlabel="To Month" for="endmonthid">${constants.labels.endmonthid}</label>
   <select id="endmonthid" name="endmonthid">
    <c:forEach var="opt" items="${constants.options.monthname}"><%--
--%> <option value="${opt.id}"${rec.endmonthid==opt.id?" selected=\"selected\"":""}>${opt.name}</option>
    </c:forEach><%--
--%></select>
</span>

</div><!-- formline -->

<div class="formline">
<!-- Field of Study -->
<span class="textfield" title="${constants.tooltips.field}">
  <label for="field">${constants.labels.field}</label>
  <input type="text"
         id="field"
         name="field"
         size="50" maxlength="100"
         value="${rec.field}"
         >
</span>
</div><!-- formline -->

<div class="formline">
<!-- Sponsor/Mentor -->
<span class="textfield" title="${constants.tooltips.sponsor}">
  <label for="sponsor">${constants.labels.sponsor}</label>
  <input type="text"
         id="sponsor"
         name="sponsor"
         size="50" maxlength="50"
         value="${rec.sponsor}"
         >
</span>
</div><!-- formline -->

<div class="formline">
<!-- Remark -->
<span class="textfield" title="${constants.tooltips.remark}">
  <label for="remark">${constants.labels.remark}</label><br />
  <textarea
            id="remark" name="remark"
            rows="3" cols="54" wrap="soft"
            >${rec.remark}</textarea>
</span>
</div><!-- formline -->


<div class="buttonset">
 <input type="submit" id="save" name="save" value="Save" title="Save">
 <!-- button id="popupformcancel" name="popupformcancel" value="Cancel">Cancel</button -->
 <input type="submit" id="popupformcancel" name="popupformcancel" value="Cancel" title="Cancel">
</div>

</form>
