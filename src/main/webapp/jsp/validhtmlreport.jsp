<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="application/report.ms-excel" %>
<% response.setHeader("Content-Disposition","attachment; filename="+"VALID_HTML_LIST_"+request.getAttribute("schName").toString().replaceAll(" ","_"));%>


<html>

<title>Valid HTML Report Generator</title>
<body align="center">

  <table id="table1" border="1" cellspacing="1" bgcolor="#F0FFFF">
  <caption style="padding-bottom:2px;"><b>Valid HTML Report - Do not perform data entry until June 30, 2008.</caption>
    <tr>

      <th style=" width:150px;color:#00000; background-color:#B8CADC; padding-bottom:2px;">Last Name</th>
      <th style=" width:150px;color:#00000; background-color:#B8CADC; padding-bottom:2px;">First Name</th>
      <th style=" width:150px;color:#00000; background-color:#B8CADC; padding-bottom:2px;">School</th>
      <th style=" width:150px;color:#00000; background-color:#B8CADC; padding-bottom:2px;">Department</th>
      <th  style=" width:150px;color:#00000; background-color:#B8CADC; padding-bottom:2px;">Data Type</th>
      <th  style=" width:150px;color:#00000; background-color:#B8CADC; padding-bottom:2px;">Field</th>
      <th  style=" width:150px;color:#00000; background-color:#B8CADC; padding-bottom:2px;">HTML Tags Used</th>      
      <th  style=" width:150px; color:#00000; background-color:#B8CADC; padding-bottom:2px;">Contents of Formatted HTML</th>

    </tr>

    <c:forEach var="result" items="${results}">

      <tr>

        <td><c:out value="${result.lName}"/></td>
        <td><c:out value="${result.FName}"/></td>
        <td><c:out value="${result.school}"/></td>
        <td><c:out value="${result.deptName}"/></td>
        <td><c:out value="${result.table}"/></td>
        <td><c:out value="${result.field}"/></td>
        <td><c:out value="${result.tag}"/></td>
        <td><c:out value="${result.contents}"/></td>

      </tr>

    </c:forEach>

  </table>

</body>

</html>