<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%><%--
--%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %><%--
--%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%--
--%><%@ taglib prefix="spring" uri="/spring"%><%--
--%><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
  <title>MIV &ndash; ${constants.strings.pagehead}</title>
  
  <script type="text/javascript">
  /* <![CDATA[ */
      var mivConfig = new Object();
      mivConfig.isIE = false;
  /* ]]> */
  </script>

  <link rel="shortcut icon" href="<c:url value='/images/favicon.ico'/>" type="image/x-icon" />
  <link rel="stylesheet" type="text/css" href="<c:url value='/js/dojo/dojo/resources/dojo.css'/>">
  <link rel="stylesheet" type="text/css" href="<c:url value='/js/dojo/dijit/themes/tundra/tundra.css'/>">
  
  <%@ include file="/jsp/metatags.html" %>
  <link rel="stylesheet" type="text/css" href="<c:url value='/css/mivcommon.css'/>">
  <link rel="stylesheet" type="text/css" href="<c:url value='/css/mivEnterData.css'/>">
  <link rel="stylesheet" type="text/css" href="<c:url value='/css/miv-forms.css'/>">
  <link rel="stylesheet" type="text/css" href="<c:url value='/css/biosketch.css'/>">
  <!--[if lt IE 8]>
  <link rel="stylesheet" type="text/css" href="<c:url value='/css/iehacks.css'/>">
  <script type="text/javascript">mivConfig.isIE=true;</script>
  <![endif]-->

  <%-- script type="text/javascript" src="<c:url value='/js/dojo/dojo/dojo.js'/>"></script --%>
  <script type="text/javascript" src="<c:url value='/js/dojo/dojo/dojo.js'/>"></script>
  <script type="text/javascript" src="<c:url value='/js/mivcommon.js'/>"></script>
  <script type="text/javascript" src="<c:url value='/js/biosketch.js'/>"></script>

  <c:set var="user" scope="request" value="${MIVSESSION.user}" />
</head>

<body class="tundra">
  <jsp:include page="/jsp/mivheader.jsp" />
  <div id="breadcrumbs">
    <a href="<c:url value='/MIVMain'/>" title="Home">Home</a>
    &gt; ${constants.strings.description}
  </div><!-- breadcrumbs -->

  <div id="main">
    <c:if test="${exception}">
	<div id="errormessage" class="haserrors">
		<div id="errorbox">${constants.strings.message}</div>
	</div>
    </c:if>
    
    <c:if test="${empty exception || !exception}">
    <div id="listarea">
      <div style="background-color: #f0fff0;">
        <h1 id="recordtypedescription" style="float: left;">${constants.strings.pagehead}</h1>
      </div><!-- for H1 button -->
      <div>&nbsp;</div><%-- This div fixes an IE7 rendering bug. --%>
      <div style="clear: both; padding-bottom: 1em;">
        <strong>
          ${constants.strings.description} documents for ${user.targetUserInfo.displayName}
        </strong>
      </div>

     <c:if test="${success}">
	<c:set var="hasMessage" scope="page" value=" class=\"hasmessages\"" />
     </c:if>

     <div id="CompletedMessage" ${hasMessage}>${CompletedMessage}</div>
     
     <script type="text/javascript">
        dojo.removeAttr(dojo.byId("CompletedMessage"), "style");
        dojo.removeClass(dojo.byId("CompletedMessage"), "hasmessages");
     </script>
     
      <div class="addrecord">
        <form  class="recordlist" method="post" action="<c:url value='/biosketch/BiosketchPreview'/>">
          <div class="buttonset">
            <input type="submit" name="Add" value="${constants.strings.addbutton}" title="Add" />
          </div>
        </form>
      </div><!-- addrecord -->

      <form id="biosketchform" class="recordlist" method="post" action="<c:url value='/biosketch/BiosketchPreview'/>">
        <input id="sectionname" type="hidden" value="" name="sectionname"/>
        <div id="section" class="section">
          <div class="sectionhead">
            <h2  class="whitespacepreserve">${constants.strings.sectionheading}</h2>
            &nbsp;<%-- This nbsp is important: it's what gives the div.sectionhead some height --%>
          </div><!-- sectionhead -->
          <c:if test="${fn:length(biosketchList)>0}">
            <ul class="mivrecords" id="records">
            <c:forEach var="biosketch" items="${biosketchList}">
              <c:set var="noprint" value=""/>
              <c:set var="previous" value=""/>
              <c:if test="${biosketchID==biosketch.id}">
                <!-- Duplicated biosketch id: ${biosketchID} -->
                <c:set var="previous" value="previous "/>
                <c:set var="noprint" value=" noprint"/>
              </c:if>
              <li class="${previous}records${noprint}" id="P${biosketch.id}">
              <div class="entry">
                <%-- The styles below should be in a CSS file, not here --%>
                <div class="documentname">${biosketch.name}</div>

                <div class="buttongroup">
                  <div class="buttonset">
                    <input type="submit" name="E${biosketch.id}" value="Edit" title="Edit" />
                    <input type="submit" id="DE${biosketch.id}" class="recorddel" name="DE${biosketch.id}" value="Delete" title="Delete" />
                    <input type="submit" name="C${biosketch.id}" value="Create This Document" title="Create This Document" />
                  </div><!-- buttonset -->
                  <div class="duplicate">
		    <input type="submit" id="DU${biosketch.id}" class="recordduplicate" name="DU${biosketch.id}" value="Duplicate This Document" title="Duplicate This Document" />                                        
                    <%-- <br/> We do not use BR tags to make space --%>
                  </div>
                </div>
              </div><!-- entry -->
              </li>
            </c:forEach>
            </ul>
          </c:if>
        </div><!-- section -->
      </form>
      <c:if test="${fn:length(biosketchList)>0}">
      <div class="addrecord">
        <form method="post" action="<c:url value='/biosketch/BiosketchPreview'/>">
          <div class="buttonset">
            <input type="submit" name="Add" value="${constants.strings.addbutton}" title="Add" />
          </div>
        </form>
      </div>
      </c:if>
    </div><!-- listarea -->    
    </c:if>
  </div> <!-- main -->
  <jsp:include page="/jsp/miv_small_footer.jsp" />
  <c:if test="${not empty biosketchID}">
    <script type="text/javascript">
    /* <![CDATA][ */
    scrollTo("P${biosketchID}");

    var obj = dojo.byId("P${biosketchID}");
    var curtop = 0;
    if (obj && obj.offsetParent) {
        do {
            curtop += obj.offsetTop;
        } while (obj = obj.offsetParent);
    }
    /* ]]> */
    </script>
  </c:if>
  <c:if test="${success}">
     <script type="text/javascript">
    	/* <![CDATA][ */
    	        popupMessage("CompletedMessage");     		     		
 	/* ]]> */
     </script>
  </c:if>
</body>
</html>
<%--
 vim:ts=8 sw=2 et:
--%>
