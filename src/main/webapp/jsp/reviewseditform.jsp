<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%--
--%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %><%--
--%><c:set var="user" scope="request" value="${MIVSESSION.user}"/><%--
--%><c:if test="${constants.config.usepopups}">
<head>
    <meta http-equiv="cache-control" content="no-cache, must-revalidate">
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="expires" content="0">
</head>
<!-- Using Pop-Up edit dialogs -->
<script type="text/javascript">
    if (typeof _container_ != 'undefined') {
        var btn = document.getElementById("popupformcancel");
        _container_.setCloseControl(btn);
    }
</script>
</c:if><%-- usepopups --%>

    <link rel="stylesheet" type="text/css" href="<c:url value='/css/expandable.css'/>">
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/creativeactivities.css'/>">

    <script type="text/javascript" src="<c:url value='/js/mivcommon.js'/>"></script>
    <script type="text/javascript" src="<c:url value='/js/creativeactivities/reviews.js'/>"></script>
    <script type="text/javascript" src="<c:url value='/js/creativeactivities/creativeactivities.js'/>"></script>
    <script type="text/javascript" src="<c:url value='/js/location.js'/>"></script>

<jsp:scriptlet><![CDATA[
   Integer d = new Integer(java.util.Calendar.getInstance().get(java.util.Calendar.YEAR));
   pageContext.setAttribute("thisYear", d, PageContext.REQUEST_SCOPE);
   ]]></jsp:scriptlet>

<input type="hidden" id="thisYear" value="${thisYear}">

<div id="userheading">
    <span class="standout">${user.targetUserInfo.displayName}</span>
</div>

<c:set var="PRINT_REVIEWS" value="1"/>
<c:set var="MEDIA_REVIEWS" value="2"/>

<!-- This is the data entry form for "${recordTypeName}" records -->
<form class="mivdata pre-render" id="mytestform" name="mytestform"
      method="post" action="<c:url value='${constants.config.saveservlet}'/>">

    <input type="hidden" id="class" name="recordClass" value="${recordClass}">
    <input type="hidden" id="rectype" name="rectype" value="${recordTypeName}">
    <input type="hidden" id="recid" name="recid" value="${rec.id}">
    <input type="hidden" id="display" name="display" value="${not empty rec.display?rec.display:1}">

    <p id="formhelp" class="formhelp">
        Only the content of checked block will be saved.
    </p>
    <input type="hidden" id="tempcategoryid" name="tempcategoryid" value="${rec.categoryid}">
    <div id="mivtabs">

        <ul id="tabsbar" class="hidden">
            <li><a href="#print_reviews_div" id="print_reviews" title="enter print reviews">Print Review</a></li>
            <li><a href="#media_reviews_div" id="media_reviews" title="enter media reviews">Media Review</a></li>
            <!-- div class="tabhelp" title="Only the visible content in the active tab will be saved.">help</div -->
        </ul>

        <label for="print_reviews_div"><%-- Does this work? A <label> must be "for" a form control --%>
            <input type="radio" title="enter print reviews" id="regularreviews" name="categoryid"
                    ${empty rec.categoryid || rec.categoryid==PRINT_REVIEWS? 'checked=checked':''  } value="1" maxlength="50">
            <strong>Print Review:</strong>
        </label>

        <div name="print_reviews_div" id="print_reviews_div" class="frameset">
            <div class="formline">
                <span class="textfield" title="${constants.tooltips.printreviewyear}">
                    <label for="printreviewyear" class="f_required">${constants.labels.printreviewyear}</label>
                    <input type="text" id="printreviewyear" name="printreviewyear" size="5" maxlength="4" value="${ rec.categoryid == PRINT_REVIEWS ? ( not empty rec.printreviewyear ? rec.printreviewyear : rec.reviewdate) : ""}">
                </span>
            </div><!--formline-->

            <div class="formline">
                <span class="textfield" title="${constants.tooltips.reviewer}">
                    <label for="printreviewer" class="f_required">${constants.labels.reviewer}</label><br>
                    <textarea class="f_required"
                              id="printreviewer" name="printreviewer"
                              rows="2" cols="51" wrap="soft"
                        >${ rec.categoryid == PRINT_REVIEWS ? ( not empty rec.printreviewer ? rec.printreviewer : rec.reviewer) : ""}</textarea>
                </span>
            </div><!--formline-->

            <div class="formline">
                <span class="textfield" title="${constants.tooltips.printdescription}">
                    <label for="printdescription" >${constants.labels.printdescription}</label><br>
                    <textarea class="f_required"
                              id="printdescription" name="printdescription"
                              rows="2" cols="51" wrap="soft"
                        >${ rec.categoryid == PRINT_REVIEWS ? ( not empty rec.printdescription ? rec.printdescription : rec.description) : ""}</textarea>
                </span>
            </div><!--formline-->

            <div class="formline">
                <span class="textfield" title="${constants.tooltips.title}">
                    <label for="title" >${constants.labels.title}</label>
                    <input type="text" id="title"
                           name="title" value="${rec.title}" size="50"
                           maxlength="255">
                </span>
            </div><!--formline-->

            <div class="formline">
                <span class="textfield" title="${constants.tooltips.publication}">
                <label for="creators">${constants.labels.publication}</label>
                <input type="text" id="publication"
                       name="publication" value="${rec.publication}" size="50"
                       maxlength="255">
                </span>
            </div><!--formline-->

            <div class="formline" id="publisherdiv">
                <span class="textfield" title="${constants.tooltips.publisher}">
                <label for="publisher">${constants.labels.publisher}</label>
                <input type="text"
                       id="publisher" name="publisher"
                       size="48" maxlength="255"
                       value="${rec.publisher}"
                       >
                </span>
            </div><!--formline-->



            <!-- Volume, Issue, Pages -->
            <div class="formline">
                <span class="textfield" title="${constants.tooltips.volume}">
                <label for="volume">${constants.labels.volume}</label>
                <input type="text"
                       id="volume" name="volume"
                       size="10" maxlength="20"
                       value="${rec.volume}"
                       >
                </span>

                <span class="textfield" title="${constants.tooltips.issue}">
                <label for="issue">${constants.labels.issue}</label>
                <input type="text"
                       id="issue" name="issue"
                       size="10" maxlength="20"
                       value="${rec.issue}"
                       >
                </span>

                <span class="textfield" title="${constants.tooltips.pages}">
                <label for="pages">${constants.labels.pages}</label>
                <input type="text"
                       id="pages" name="pages"
                       size="10" maxlength="20"
                       value="${rec.pages}"
                       >
                </span>
            </div><!--formline-->

            <!-- URL / Link -->
            <div class="formline">
                <span class="textfield" title="${constants.tooltips.url}">
                <label for="printurl">${constants.labels.url}</label>
                <input type="url" pattern="(https?|ftp):\/\/.+"
                       placeholder="http://example.com/some+page"
                       id="printurl" name="printurl"
                       size="52" maxlength="255"
                       value="${ rec.categoryid == PRINT_REVIEWS ? ( not empty rec.printurl ? rec.printurl : rec.url) : ""}"
                       >
                </span>
            </div><!-- formline -->

        </div><!--print_reviews_div-->

        <label for="media_reviews_div">
            <input type="radio" title="enter venue of the event" id="regularreviews" name="categoryid"
                    ${rec.categoryid==MEDIA_REVIEWS? 'checked=checked':''  } value="2" maxlength="50">
            <strong>Media Review:</strong>
        </label>
        <div name="media_reviews_div" id="media_reviews_div" class="frameset">
            <div class="formline">
                <span class="textfield" title="${constants.tooltips.reviewdate}">
                    <label for="reviewdate" class="f_required">${constants.labels.reviewdate}</label>
                    <input type="text" id="reviewdate" name="reviewdate" size="11" maxlength="20"  class="datepicker"
                           value="${ rec.categoryid == MEDIA_REVIEWS ? rec.reviewdate : ""}">
                </span>
            </div><!--formline-->

            <div class="formline">
                <span class="textfield" title="${constants.tooltips.reviewer}">
                    <label for="reviewer" class="f_required">${constants.labels.reviewer}</label><br>
                    <textarea class="f_required"
                              id="reviewer" name="reviewer"
                              rows="2" cols="51" wrap="soft"
                        >${ rec.categoryid == MEDIA_REVIEWS ? rec.reviewer : ""}</textarea>
                </span>
            </div><!--formline-->

            <div class="formline">
                <span class="textfield" title="${constants.tooltips.venue}">
                    <label for="venue" class="f_required">${constants.labels.venue}</label>
                    <input type="text" id="venue" name="venue" value="${rec.venue}" size="50" maxlength="255">
                </span>
            </div><!--formline-->

            <div class="formline" id="descriptiondiv">
                <span class="textfield" title="${constants.tooltips.description}">
                    <label for="description" >${constants.labels.description}</label><br>
                    <textarea class="f_required"
                              id="description" name="description"
                              rows="2" cols="51" wrap="soft"
                        >${ rec.categoryid == MEDIA_REVIEWS ? rec.description : ""}</textarea>
                </span>
            </div><!--formline-->

            <!-- URL / Link -->
            <div class="formline">
                <span class="textfield" title="${constants.tooltips.url}">
                <label for="link">${constants.labels.url}</label>
                <input type="url" pattern="(https?|ftp):\/\/.+"
                       placeholder="http://example.com/some+page"
                       id="url" name="url"
                       size="52" maxlength="255"
                       value="${ rec.categoryid == MEDIA_REVIEWS ? rec.url : ""}"
                       >
                </span>
            </div><!-- formline -->

        </div><!--media_reviews_div-->

        <div id="locationblock"> <!-- Location Block -->
            <c:set var="tempcountry" value="${(not empty rec.id ? ( not empty rec.country ? rec.country : '') : constants.config.contrycode)}"/>
            <c:set var="tempprovince" value="${(not empty rec.id ? ( not empty rec.province ? rec.province : '') : constants.config.province)}"/>

            <div class="formline">
                <span class="textfield" title="${constants.tooltips.country}">
                    <label for="country">${constants.labels.country}</label>
                    <select id="country" name="country">
                        <option value="">--- Select Country ---</option><%--
                    --%><c:forEach var="opt" items="${constants.options.countries}">
                        <option value="${opt.id}"${tempcountry==opt.id?' selected="selected"':''} title="${opt.value}">${opt.value}</option><%--
                    --%></c:forEach>
                    </select>
                </span>
            </div><!--formline-->

            <div class="formline">
                <div class="textfield" title="${constants.tooltips.province}" id="provinceblock">
                    <label for="province">${constants.labels.province}</label>
                    <select id="province" name="province">
                        <option value="">--- Select State/Province ---</option><%--
                    --%><c:forEach var="opt" items="${constants.options.provinces}">
                        <option value="${opt.id}"${tempprovince==opt.id?' selected="selected"':''} title="${opt.value}">${opt.value}</option><%--
                    --%></c:forEach>
                    </select>
                </div>

                <div class="textfield" title="${constants.tooltips.city}" id="cityblock">
                    <label for="city">${constants.labels.city}</label>
                    <input type="text" id="city" name="city" value="${rec.city}">
                </div>
            </div><!--formline-->
        </div><!-- Location Block -->

    </div>

    <input type="hidden" id="selectedids" name="selectedids" value="">	
    <fieldset id="associate_fieldset" name="associate_fieldset" class="collapsible ">
        <legend>Associate Events/Works with this review</legend>
        <div class="hint">expand to see association form</div>

        <div id="association">
            <!-- association error field -->
            <label for="association" class="hidden"> Associate Events/Works </label>
                <c:set var="year" value=""/>
                <c:choose>
                    <c:when test="${ rec.categoryid==PRINT_REVIEWS }">
                        <c:set var="year" value="${rec.printreviewyear}"/>
                    </c:when>
                    <c:when test="${ rec.categoryid==MEDIA_REVIEWS }">
                        <c:set var="year" value="${rec.reviewdate}"/>
                    </c:when>
                    <c:otherwise></c:otherwise><%-- if there's no otherwise does this need an otherwise tag? --%>
                </c:choose>
            <div class="formline">
            <%-- <iframe src="<c:url value='pages/creativeactivitiesassociation.jsp'/>" height="350px" width="590px" frameborder="0" scrolling="yes" style="overflow:auto;">
            </iframe> --%>
                <iframe id="associationframe" name="associationframe"  src="<c:url value='/Associate?requestType=init&rectype=${recordTypeName}&recid=${rec.id}&year=${year}'/>" frameborder="0" scrolling="yes">
                </iframe>
            </div><!--formline-->
        </div>
    </fieldset>

    <div class="buttonset">
        <input type="submit" id="save" name="save" value="Save" title="Save">
        <!-- button id="popupformcancel" name="popupformcancel" value="Cancel">Cancel</button -->
        <input type="submit" id="popupformcancel" name="popupformcancel" value="Cancel" title="Cancel">
    </div>

</form>
