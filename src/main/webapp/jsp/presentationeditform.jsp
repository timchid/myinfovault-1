<%@ page import="edu.ucdavis.myinfovault.*" %><%--
--%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%--
--%><c:set var="user" scope="request" value="${MIVSESSION.user}"/><%--
--%><c:if test="${constants.config.usepopups}">
<head>
<meta http-equiv="cache-control" content="no-cache, must-revalidate">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="expires" content="0">
</head>
<!-- Using Pop-Up edit dialogs -->
<script type="text/javascript">
if (typeof _container_ != 'undefined') {
    var btn = document.getElementById("popupformcancel");
    _container_.setCloseControl(btn);
}
</script>
</c:if><%-- usepopups --%>
<script type="text/javascript"><!--
    mivFormProfile = {
        required: [ ${constants.config.required} ],
        trim: ["year", "day", "title", "locationdescription"],
        constraints: {
            day: miv.validation.isInteger
        }
    };
// -->
</script>


<div>
    <div id="userheading"><span class="standout">${user.targetUserInfo.displayName}</span></div>
    <div id="links">
        <%-- <div id="specchar"><a href="#">Add Special Characters</a></div>
             div id="format"><a href="#">Bold / Italic / Underline</a></div --%>
    </div>
</div>

<%-- <!-- record: ${rec} --> --%>
<%-- constants: ${constants} --%>
<%-- constants.labels: ${constants.labels} --%>

<!-- This is the data entry form for "${recordTypeName}" records -->
<form class="mivdata" id="mytestform"
      method="post" action="<c:url value='${constants.config.saveservlet}'/>">

<input type="hidden" id="rectype" name="rectype" value="${recordTypeName}">
<input type="hidden" id="recid" name="recid" value="${rec.id}">
<input type="hidden" id="display" name="display" value="${not empty rec.display?rec.display:1}">

<div class="formline">
<!-- Year -->
<span class="textfield" title="${constants.tooltips.year}">
  <label for="year" class="f_required">${constants.labels.year}</label>
  <input type="text"
         class="f_required f_numeric"
         id="year" name="year"
         size="5" maxlength="4"
         value="${rec.year}"
         >
</span>

<!-- Month -->
<span class="dropdown" title="${constants.tooltips.monthid}">
   <label for="monthid">${constants.labels.monthid}</label>
   <select id="monthid" name="monthid">
     <option value="0" selected="selected"></option>
    <c:forEach var="opt" items="${constants.options.monthname}"><%--
--%> <option value="${opt.id}"${rec.monthid==opt.id?' selected="selected"':''}> ${opt.name}</option>
    </c:forEach><%--
--%></select>
</span>

<!-- Day -->
<span class="textfield" title="${constants.tooltips.day}">
  <label for="day">${constants.labels.day}</label>
  <input type="text" class="f_numeric"
         id="day" name="day"
         size="3" maxlength="2"
         value="${empty rec.day ? '' : rec.day}"
         >
</span>
</div><!-- formline -->


<div class="formline">
<!-- Title/Description -->
<span class="textfield" title="${constants.tooltips.title}">
  &#8224;&nbsp;
  <label for="title" class="f_required">${constants.labels.title}</label><br />
  <textarea id="title" name="title"
            rows="4" cols="50" wrap="soft"
            >${rec.title}</textarea>
</span>
</div><!-- formline -->

<!-- Location/Description -->
<div class="formline">
<span class="textfield" title="${constants.tooltips.locationdescription}">
  <label for="locationdescription">${constants.labels.locationdescription}</label><br />
  <textarea id="locationdescription" name="locationdescription"
            rows="4" cols="50" wrap="soft"
            >${rec.locationdescription}</textarea>
</span>
</div><!-- formline -->

<%--
<div id="displaycb" class="cb">
  <div class="formline">
  <span class="checkbox" title="${constants.tooltips.display}">
    <input type="checkbox" id="display" name="display"${rec.display=='1'||rec.display=='true'?" checked":""}>
    <label for="display">${constants.labels.display}</label>
  </span>
  </div><!-- formline -->
</div><!-- displaycb -->
--%>
<div class="buttonset">
 <input type="submit" id="save" name="save" value="Save" title="Save">
 <!-- button id="popupformcancel" name="popupformcancel" value="Cancel">Cancel</button -->
 <input type="submit" id="popupformcancel" name="popupformcancel" value="Cancel" title="Cancel">
</div>

</form>

