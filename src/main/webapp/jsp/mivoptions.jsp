<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%><%--
--%><%@ page import="edu.ucdavis.myinfovault.*" %><%--
--%><%@ taglib prefix="miv" uri="mivTags" %><%--
--%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%--
--%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %><%--
--%><!DOCTYPE html>
<html>
<head>
    <title>MyInfoVault Options</title>
    <%@ include file="/jsp/metatags.html" %>
    <link rel="shortcut icon" href="<c:url value='/images/favicon.ico'/>" type="image/x-icon" />
    
<style type="text/css">
#log .rb {
  display: block;
}
form fieldset {
  width: 80%;
}
#main #left, #main #right {
  width: 46%;
}
#main #left {
  padding-right: 1.0%;
  border-right: 1px solid #ccc;
}
#main form input {
  padding: 3px;
}
</style> <c:set var="config" value="${MIVSESSION.config}"/>
</head>
<body>
<div id="main">
<%-- Process the form Submit, if any, before displaying the page --%>
<c:if test="${pageContext.request.method=='POST'}"><div>
  <c:forEach var="p" items="${param}">
  {p}: ${p}<br></c:forEach>

  <c:if test="${fn:length(param.reloadbtn)>0}"><%
  ((MIVSession)session.getAttribute("MIVSESSION")).getConfig().reload();
  %><strong>Reloaded!</strong>
  </c:if>

  <c:if test="${fn:length(param.flushpeople)>0}"><%
  ((MIVSession)session.getAttribute("MIVSESSION")).getConfig().reload(ConfigReloadConstants.USER_SERVICE);
  %><strong>Reloaded!</strong>
  </c:if>
  
   <c:if test="${fn:length(param.flushgroups)>0}"><%
  ((MIVSession)session.getAttribute("MIVSESSION")).getConfig().reload(ConfigReloadConstants.GROUP_SERVICE);
  %><strong>Reloaded!</strong>
  </c:if>

  <c:if test="${fn:length(param.debugbtn)>0}"><%
  ((MIVSession)session.getAttribute("MIVSESSION")).getConfig()
      .setDebug( request.getParameter("dodebug") .equalsIgnoreCase("on") );
  %>New Debug setting is <strong>${param.dodebug}</strong>
  </c:if>

  <c:if test="${fn:length(param.timingbtn)>0}"><%
  ((MIVSession)session.getAttribute("MIVSESSION")).getConfig()
      .setDoTimings( request.getParameter("dotiming") .equalsIgnoreCase("on") );
  %>New Timing setting is <strong>${param.dotiming}</strong>
  </c:if>

<%-- 
  <c:if test="${fn:length(param.setlogbtn)>0}"><%
  ((MIVSession)session.getAttribute("MIVSESSION")).getConfig()
      .setLoggingLevel(request.getParameter("loglevel"));
  %>New log level is <strong>${param.loglevel}</strong>
  </c:if>
--%>

  <c:if test="${fn:length(param.setmaintbtn)>0}"><%
  ((MIVSession)session.getAttribute("MIVSESSION")).getConfig()
      .setMaintenanceMode( request.getParameter("domaint") .equalsIgnoreCase("on") );
  %>Maintenance mode is <strong>${param.domaint}</strong>
  </c:if>
</div>
</c:if>


<hr>

<!-- Reload Button -->
<miv:permit roles="SYS_ADMIN">
<div id="reloadform">
    <form id="reload" name="reload" action="" method="post">
        <p>
        Re-load the MyInfoVault options by re-reading the mivconfig properties file,
        and the various Type and Status codes from the database.
        <br><input type="submit" id="reloadbtn" name="reloadbtn" value="Reload Options" title="Reload Options">
        </p>
        <p>
        Invalidate the user-service person caches, so all people will be re-loaded.
        <br><input type="submit" id="flushpeople" name="flushpeople" value="Flush Person Cache" title="Flush the User-Service person cache">
        </p>
        <p>
        Invalidate the group service group cache and reload.
        <br><input type="submit" id="flushgroups" name="flushgroups" value="Flush Group Cache" title="Flush the Group Service group cache">
        </p>
    </form>
</div><!-- reloadform -->
</miv:permit>

<hr>


<div id="left">

<!-- Debug Setting -->
<miv:permit roles="SYS_ADMIN">
<div id="debugform">
  <form id="debug" name="debug" action="" method="post">
  <div class="formitem">
  <c:set var="isdebug" scope="request" value="${config.debug}"/>
   <fieldset id="debugchoice"><legend>Debug</legend>
   <p>
   Current setting is <strong>${isdebug}</strong><br />
   </p>

   <span class="rb" title="Run Debugging code and output">
     <input type="radio" name="dodebug"
            id="debugyes" value="on"${isdebug ? ' checked' : ''}>
     <label for="debugyes">On</label>
   </span><!--.rb-->

   <span class="rb" title="Do not run debug code">
     <input type="radio" name="dodebug"
            id="debugno" value="off"${not isdebug ? ' checked' : ''}>
     <label for="debugno">Off</label>
   </span><!--.rb-->

   </fieldset>
  </div><!--.formitem-->
	
	<div class="buttonset">
  		<input type="submit" id="debugbtn" name="debugbtn" value="Set Debug preference" title="Set Debug preference">
  	</div>

  </form>
  </div><!-- debugform -->
<br><hr><br>
</miv:permit>



</div><!--left-->


<div id="right">

<miv:permit roles="SYS_ADMIN">
<div id="maintform">
  <form id="maint" name="maint" action="" method="post">
    <div class="formitem">
    <c:set var="ismaint" scope="request" value="${config.maintenanceMode}"/>
      <fieldset id="maintchoice"><legend>Maintenance Mode</legend>
      <p>
      Switch into or out of <em>Maintenance Mode</em>. This is also known
      as <em>Lockout</em> mode. Only the development team members can login
      when maintenance mode is turned on.
      </p>

      <p>Current setting is <strong>${ismaint}</strong><br /></p>

      <span class="rb" title="Turn Maintenance mode ON">
        <input type="radio" name="domaint"
               id="maintyes" value="on"${ismaint ? ' checked' : ''}>
        <label for="maintyes">On</label>
      </span><!--.rb-->

      <span class="rb" title="Turn Maintenance mode OFF">
        <input type="radio" name="domaint"
               id="maintno" value="off"${not ismaint ? ' checked' : ''}>
        <label for="maintno">Off</label>
      </span><!--.rb-->

      </fieldset>
    </div><!--.formitem-->
    <miv:permit roles="SYS_ADMIN"><%--
--%>    <div class="buttonset">
			<input type="submit" id="setmaintbtn" name="setmaintbtn" value="Change Mode" title="Change Mode">
		</div><%--
--%></miv:permit>
  </form>
</div><!-- maintform -->
<br><hr><br>
</miv:permit>


<!-- Gather Timings ('Profiling', 'Performance Monitoring') -->
<miv:permit roles="SYS_ADMIN">
<div id="timingform">
  <form id="timing" name="timing" action="" method="post">
  <div class="formitem">
  <c:set var="dotiming" scope="request" value="${config.doTimings}"/>
   <fieldset id="timingchoice"><legend>Gather Timings</legend>

   <p>
   <!-- These lines are broken where they are for a reason. -->
   Note that timings are written to the log
   at the <em>info</em> logging level
   now, during development, but will be written
   at the <em>fine</em> logging level
   later.
   </p>
   <p>
   Current setting is <strong>${dotiming}</strong><br />
   </p>

   <span class="rb" title="Gather timing statistics and write them to the log">
     <input type="radio" name="dotiming"
            id="timingyes" value="on"${dotiming ? ' checked' : ''}>
     <label for="timingyes">On</label>
   </span><!--.rb-->

   <span class="rb" title="Do not gather timing statistics">
     <input type="radio" name="dotiming"
            id="timingno" value="off"${not dotiming ? ' checked' : ''}>
     <label for="timingno">Off</label>
   </span><!--.rb-->

   </fieldset>
  </div><!--.formitem-->

  <miv:permit roles="SYS_ADMIN"><%--
     Note these next two force the log level to FINE when timing is set
     either ON *or* OFF; this is an experiment. A nice behavior might
     be to set logging to FINE when timing is turned ON *and* logging
     is set to a level that is above FINE and wouldn't show timing...
     so if logging is already set to FINER we'd leave it alone.
--%>  <!-- input type="hidden" name="setlogbtn" value="timingBeingSet" --><%--
--%>  <!-- input type="hidden" name="loglevel" value="FINE" --><%--
--%>  <div class="buttonset">
			<input type="submit" id="timingbtn" name="timingbtn" value="Set Timing preference" title="Set Timing Preference">
	  </div>
			<%--
--%>  </miv:permit><%--
--%>
  </form>
  </div><!-- timingform -->
<br><hr><br>
</miv:permit>

</div><!--right-->

<div style="clear:both">
<!-- hr (ugh. no hr, it looks bad.) -->
</div>

</div><!--main-->
</body>
</html>
