<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%--
--%><c:set var="user" scope="request" value="${MIVSESSION.user}"/><%--
--%><c:if test="${constants.config.usepopups}">
<head>
<meta http-equiv="cache-control" content="no-cache, must-revalidate">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="expires" content="0">
</head>
<!-- Using Pop-Up edit dialogs -->
<script type="text/javascript">
if (typeof _container_ != 'undefined') {
    var btn = document.getElementById("popupformcancel");
    _container_.setCloseControl(btn);
}
</script>
</c:if><%-- usepopups --%>
<script type="text/javascript"><!--
    mivFormProfile = {
        required: [ ${constants.config.required} ],
        trim: [ "term" ],
        constraints: {
        	lecture : function(contents) {
                return isFloat(contents,9999.99,0.0);
            },
            discussion : function(contents) {
            	return isFloat(contents,9999.99,0.0);
            },
            lab : function(contents) {
            	return isFloat(contents,9999.99,0.0);
            },
            clinic : function(contents) {
            	return isFloat(contents,9999.99,0.0);
            }
        },
        errormap: {
            "INVALID_MIN_VALUE" : "This value can not be negative.",
            "INVALID_MAX_VALUE" : "The maximum allowed value is '9999.99'."            
        }
    };
// -->
</script>

<div id="userheading">
	<span class="standout">${user.targetUserInfo.displayName}</span>
</div>

<%-- <!-- record: ${rec} --> --%>
<%-- constants: ${constants} --%>
<%-- constants.labels: ${constants.labels} --%>

<!-- This is the data entry form for "${recordTypeName}" records -->
<form class="mivdata" id="mytestform"
      method="post" action="<c:url value='${constants.config.saveservlet}'/>">

<input type="hidden" id="rectype" name="rectype" value="${recordTypeName}">
<input type="hidden" id="recid" name="recid" value="${rec.id}">
<input type="hidden" id="display" name="display" value="${not empty rec.display?rec.display:1}">

<!-- Quarter/Year -->
<div class="formline">
<span class="textfield" title="${constants.tooltips.term}">
  <label for="term" class="f_required">${constants.labels.term}</label>
  <input type="text"
         class="f_required f_numeric"
         id="term" name="term"
         size="12" maxlength="20"
         value="${rec.term}"
         >
</span>
</div><!-- formline -->

<!-- Lecture -->
<div class="formline">
<span class="textfield" title="${constants.tooltips.lecture}">
  <label for="lecture">${constants.labels.lecture}</label>
  <input type="text" class="f_numeric"
         id="lecture" name="lecture"
         size="7" maxlength="7"
         value="${rec.lecture}"
         >
</span>
</div><!-- formline -->

<!-- Discussion -->
<div class="formline">
<span class="textfield" title="${constants.tooltips.discussion}">
  <label for="discussion">${constants.labels.discussion}</label>
  <input type="text" class="f_numeric"
         id="discussion" name="discussion"
         size="7" maxlength="7"
         value="${rec.discussion}"
         >
</span>
</div><!-- formline -->

<!-- Labatory -->
<div class="formline">
<span class="textfield" title="${constants.tooltips.lab}">
  <label for="lab">${constants.labels.lab}</label>
  <input type="text" class="f_numeric"
         id="lab" name="lab"
         size="7" maxlength="7"
         value="${rec.lab}"
         >
</span>
</div><!-- formline -->

<!-- Clinic -->
<div class="formline">
<span class="textfield" title="${constants.tooltips.clinic}">
  <label for="clinic">${constants.labels.clinic}</label>
  <input type="text" class="f_numeric"
         id="clinic" name="clinic"
         size="7" maxlength="7"
         value="${rec.clinic}"
         >
</span>
</div><!-- formline -->

<%--
<div id="displaycb" class="cb">
  <div class="formline">
  <span class="checkbox" title="${constants.tooltips.display}">
    <input type="checkbox" id="display" name="display"${rec.display=='1'||rec.display=='true'?" checked":""}>
    <label for="display">${constants.labels.display}</label>
  </span>
  </div><!-- formline -->
</div><!-- displaycb -->
--%>
<div class="buttonset">
 <input type="submit" id="save" name="save" value="Save" title="Save">
 <!-- button id="popupformcancel" name="popupformcancel" value="Cancel">Cancel</button -->
 <input type="submit" id="popupformcancel" name="popupformcancel" value="Cancel" title="Cancel">
</div>

</form>
