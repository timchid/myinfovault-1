<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%><%--
--%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%--
--%><%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%><%--
--%><c:set var="title" value="MIV Build Information"/><%--
--%><!DOCTYPE HTML>
<html>
<head>
    <title>MIV &ndash; ${title}</title>

    <%@ include file="metatags.html" %>

    <link rel="stylesheet" type="text/css" href="<c:url value='/css/mivEnterData.css'/>">
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/data_table_gui.css'/>">
</head>

<body>
    <jsp:include page="mivheader.jsp" />

    <jsp:include page="/WEB-INF/jsp/breadcrumbs.jsp">
        <jsp:param value="${title}" name="pageTitle"/>
    </jsp:include>

    <div id="main">
        <h1>${title}</h1>
    
        <table>
            <tr>
                <th>Build Author</th>
                <td>${buildInformation.buildAuthor}</td>
            </tr>
            <tr>
                <th>Build Date</th>
                <td>${buildInformation.buildDate}</td>
            </tr>
            <tr>
                <th>Build Revision</th>
                <td><a href="https://bitbucket.org/mivteam/myinfovault/commits/${buildInformation.buildCommit}">${buildInformation.buildRevision}</a></td>
            </tr>
            <tr>
                <th>Build Manager</th>
                <td>${buildInformation.buildManager}</td>
            </tr>
            <tr>
                <th>MIV Version</th>
                <td>${buildInformation.mivVersion}</td>
            </tr>
            <tr>
                <th>JDK Version</th>
                <td>${buildInformation.jdkVersion}</td>
            </tr>
        </table>
    </div>

    <jsp:include page="miv_small_footer.jsp" />
</body>
</html><%--
 vi: se ts=8 et:
--%>
