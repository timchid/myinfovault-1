<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8" buffer="none"%><%--
--%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%--
--%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%><%--
--%><!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Appointment Form</title>
        <%@ include file="/jsp/metatags.html" %>
        <script type="text/javascript">
        /* <![CDATA[ */
            djConfig = {
                isDebug: false,
                parseOnLoad: false
            };
            var mivConfig = new Object();
            mivConfig.isIE = false;
        /* ]]> */
        </script>
        
        <script type="text/javascript" src="<c:url value='/js/mivcommon.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/appointment-form.js'/>"></script>
        
        <link rel="stylesheet" type="text/css" href="<c:url value='/css/mivEnterData.css'/>">
        <link rel="stylesheet" type="text/css" href="<c:url value='/css/miv-forms.css'/>">
        <link rel="stylesheet" type="text/css" href="<c:url value='/css/mivcommon.css'/>">
        <link rel="stylesheet" type="text/css" href="<c:url value='/css/expandable.css'/>">
        <link rel="stylesheet" type="text/css" href="<c:url value='/css/appointment-form.css'/>">
        <link rel="stylesheet" type="text/css" href="<c:url value='/css/special_character2.css'/>">
        <!--[if lt IE 8]>
        <link rel="stylesheet" type="text/css" href="<c:url value='/css/iehacks.css'/>" >
        <script type="text/javascript">mivConfig.isIE=true;</script>
        <![endif]-->
        <!--[if IE 8]>
        <link rel="stylesheet" type="text/css" href="<c:url value='/css/ie8hacks.css'/>">
        <script type="text/javascript">mivConfig.isIE=true;</script>
        <![endif]-->
</head>

<body class=tundra>
  <jsp:include page="/jsp/mivheader.jsp" />

<div id="main">

        <h1>Appointment Form</h1>
        
        <div id="errormessage"></div>

        
        <div id="enterdataguide">
          <div id="requiredfieldlegend">* = Required Field</div>
          <div id="specialformattinghelp"><a target="mivhelp" title="Do you want to format text?" href="/miv/help/special_formatting.html">Do you want to format text?</a></div>
        </div>

<div class="pagedialog" id="maindialog">
      <div class="dojoDialog mivDialog">


<div id="step-1" class="hidden1">
    <fieldset form="appointee-information-form" name="appointee-info" id="appointee-info" class="collapsible collapsed">
        <legend title="Appointee Information">Appointee Info:</legend>


        <div class="formline appointee-info-block">
          <span class="appointee">Appointee's Name:</span>    
            <label class="f_required" for="lname">Last:</label>
            <input type="text" value="" maxlength="40" size="12" name="lname" id="lname">
            
            <label class="f_required" for="fname">First:</label>
            <input type="text" value="" maxlength="40" size="12" name="fname" id="fname">
        
            <label for="mname">Middle:</label>        
            <input type="text" value="" maxlength="40" size="12" name="mname" id="mname">
            <input type="hidden" value="" name="appointee_displayname" id="appointee_displayname">
        </div>
    
        <ul class="questions">
            <li>
                <div class="question-block f_required" id="span-q1">
                    <strong>Is this a current UC Davis employee?</strong> <em>If no, skip to complete Proposed Status, If yes, go to question 2.</em>
                </div>
                <div class="answers-block">
                    <input type="radio" value="Yes" name="q1" id="q1y"/>
                      <label for="q1y">Yes</label>
                    <input type="radio" value="No" name="q1" id="q1n"/>
                      <label for="q1n">No</label>          
                </div>
            </li>
            <li>
                <div class="question-block" id="span-q2" >
                    <strong>Is this employee represented?</strong> <em>If no, skip to complete Present and Proposed Status, If yes, notice to the union may be required under certain collective bargaining aggreements; go to question 3.</em>
                </div>
                <div class="answers-block">
                    <input type="radio" value="Yes" name="q2" id="q2y"/>
                      <label for="q2y">Yes</label>
                    <input type="radio" value="No" name="q2" id="q2n"/>
                      <label for="q2n">No</label>          
                </div>
            </li> 
                    
            <li>
                <div class="question-block" id="span-q3">
                    <strong>Is notice to the union required?</strong> <em>If no, skip to complete Present and Proposed Status, If yes, go to question 4.</em>
                </div>
                <div class="answers-block">
                    <input type="radio" value="Yes" name="q3" id="q3y"/>
                      <label for="q3y">Yes</label>
                      <input type="radio" value="No" name="q3" id="q3n"/>
                      <label for="q3n">No</label>          
                </div>
            </li>
                    
            <li>
                <div class="question-block" id="span-q4">
                    <strong>Is the employee is a represented employee, have you notified labor relations about this proposed change in appointment?</strong> <em>If no, contact Labor Relation and/or your Dean's Office prior to proceeding with this appointment, If yes, proceed to complete Present and Proposed Statuses.</em>
                </div>
                <div class="answers-block">
                    <input type="radio" value="Yes" name="q4" id="q4y"/>
                      <label for="q4y">Yes</label>
                    <input type="radio" value="No" name="q4" id="q4n"/>
                      <label for="q4n">No</label>          
                </div>
            </li>
         </ul>                
    </fieldset>

<div class="buttonset">
   <input type="submit" title="Proceed" value="Proceed" name="btnSubmit" id="save-1">
   <input type="reset" title="Cancel" value="Cancel" id="popupformcancel-1" name="popupformcancel">
</div>
</div><!-- End Step-1 -->    
            
<div id="step-2" class="hidden">
    <div class="appointee-info">
    </div>
    <fieldset form="appointment-details-form" name="appointment-details" id="appointment-details" class="collapsible collapsed">
        <legend title="Appointment Details">Appointment Details:</legend>

        <div id="present">
        <fieldset form="present-status-form" name="present-status" id="present-status" class="collapsible collapsed">
            <legend title="Appointee Information">Present Status</legend>
        
                <div id="home-department-present-1" class="department-block home-department present">
                <div class="header">    
                      <div class="formlines">
                           <div class="inline-formline">
                                   <label for="present_home_dept_1">Home Department: </label>    
                                   <input type="text" value="" class="departments" name="present_home_dept_1" id="present_home_dept_1"/>    
                           </div>
                           <div class="inline-formline">
                                   <label for="present_home_percentage_time_1">% of time: </label>    
                                   <input type="text" value="" name="present_home_percentage_time_1" id="present_home_percentage_time_1" size="4" maxlength="3"/>
                           </div>                            
                      </div>
                </div>  <!-- end .header -->
                
                <div class="title-blocks present">
                    <div class="title-block present" id="title-block-present-1">
                            <div class="formlines">
                                <div class="inline-formline">
                                        <label for="present_title_rank_1">Rank and Title: </label>    
                                        <input type="text" value="" name="present_title_rank" id="present_title_rank_1"/>    
                                </div>
                                <div class="inline-formline">
                                        <label for="present_step_1">Step: </label>    
                                        <input type="text" value="" name="present_step" id="present_step_1" size="3" maxlength="2"/>
                                </div>                            
                            </div>
                            
                            <div class="formlines">
                                <div class="inline-formline">
                                    <label for="present_titlecode_1">Title Code: </label>    
                                    <input type="text" value="" name="present_titlecode" id="present_titlecode_1" size="5" maxlength="4"/>
                                </div>
                                <div class="inline-formline">
                                     <label for="present_percentage_time_1">% of time: </label>    
                                     <input type="text" value="" name="present_percentage_time" id="present_percentage_time_1" size="4" maxlength="3"/>
                                </div>
                            </div>    
                                
                            <div class="formline">
                            Appointment Type:      
                            <input type="radio" value="9mo" name="present_appttype_1" id="present_appttype_9mo_1"/>
                            <label for="present_appttype_9mo_1">9 Months</label>    
                            
                            <input type="radio" value="11mo" name="present_appttype_1" id="present_appttype_11mo_1"/>
                            <label for="present_appttype_11mo_1">11 Months</label>        
                            </div>
                            
                            <div class="formlines">
                            <div class="inline-formline">
                            <select name="present_salarytype" id="present_salarytype_1" class="salarytype">
                                <option value="H">Hourly</option>    
                                <option value="M" selected="selected">Monthly</option>  
                            </select>&nbsp;
                            <label for="present_salary_1">Salary:</label>
                            <input type="text" value="" name="present_salary" id="present_salary_1" maxlength="5" size="6"/>
                            </div>
                            
                            <div class="inline-formline annual_salary">
                                     <label for="present_annual_salary_1">Annual Salary: </label>    
                                     <input type="text" value="" name="present_annual_salary_1" id="present_annual_salary_1" size="8" maxlength="7"/>
                            </div>
                            </div>    
                    </div> <!-- End title-block-->      
                      
                    <div class="buttonset">
                        <input type="submit" title="Add Title" value="Add Title" name="btnAddTitle" id="present_add_title"/>
                    </div>
                </div> <!-- End title-blocks--> 
                </div> <!-- End present_home_department -->
        
        <div class="joint-departments present">    
        </div>                
        <div class="buttonset">
        <input type="submit" title="Add Joint Department" value="Add Joint Department" name="btnPresentAddJointDepartment" id="present_add_joint_department"/>
        </div>
        </fieldset>
        </div>
        
<div id="proposed">
        <fieldset form="proposed-status-form" name="proposed-status" id="proposed-status" class="collapsible collapsed">
        <legend title="Appointee Information">Proposed Status</legend>


        <div id="home-department-proposed-1" class="department-block home-department proposed">
                <div class="header">    
                      <div class="formlines">
                           <div class="inline-formline">
                                   <label for="proposed_home_dept_1">Home Department: </label>    
                                   <input type="text" value="" class="departments" name="proposed_home_dept_1" id="proposed_home_dept_1"/>    
                           </div>
                           <div class="inline-formline">
                                   <label for="proposed_home_percentage_time_1">% of time: </label>    
                                   <input type="text" value="" name="proposed_home_percentage_time_1" id="proposed_home_percentage_time_1" size="4" maxlength="3"/>
                           </div>                            
                      </div>
                </div>  <!-- end .header -->
            
        <div class="title-blocks proposed">
            <div class="title-block proposed" id="title-block-proposed-1">
                    <div class="formlines">
                         <div class="inline-formline">
                                 <label for="proposed_title_rank_1">Rank and Title: </label>
                                 <input type="text" value="" name="proposed_title_rank" id="proposed_title_rank_1" />
                         </div>
                         <div class="inline-formline">
                                 <label for="proposed_step_1">Step: </label>
                                 <input type="text" value="" name="proposed_step" id="proposed_step_1" size="3" maxlength="2"/>
                         </div>
                     </div>
                            
                     <div class="formlines">
                         <div class="inline-formline">
                             <label for="proposed_titlecode_1">Title Code: </label>
                             <input type="text" value="" name="proposed_titlecode" id="proposed_titlecode_1" size="5" maxlength="4"/>
                         </div>
                         <div class="inline-formline">
                              <label for="proposed_percentage_time_1">% of time: </label>
                              <input type="text" value="" name="proposed_percentage_time" id="proposed_percentage_time_1" size="4" maxlength="3"/>
                         </div>
                     </div>
                         
                     <div class="formline">
                     Appointment Type:
                     <input type="radio" value="9mo" name="proposed_appttype_1" id="proposed_appttype_9mo_1"/>
                     <label for="proposed_appttype_9mo_1">9 Months</label>
                     
                     <input type="radio" value="11mo" name="proposed_appttype_1" id="proposed_appttype_11mo_1"/>
                     <label for="proposed_appttype_11mo_1">11 Months</label>
                     </div>
                     
                     <div class="formlines">
                         <div class="inline-formline">
                             <select name="proposed_salarytype" id="proposed_salarytype_1" class="salarytype">
                                 <option value="H">Hourly</option>
                                 <option value="M" selected="selected">Monthly</option>
                             </select>&nbsp;
                             <label for="proposed_salary_1">Salary:</label>
                             <input type="text" value="" name="proposed_salary" id="proposed_salary_1" maxlength="5" size="6"/>
                         </div>
                             
                         <div class="inline-formline annual_salary">
                              <label for="proposed_annual_salary_1">Annual Salary: </label>    
                              <input type="text" value="" name="proposed_annual_salary_1" id="proposed_annual_salary_1" size="8" maxlength="7"/>
                         </div>
                     </div>  
            </div> <!-- End title-block-->       
            
            <div class="buttonset">
                <input type="submit" title="Add Title" value="Add Title" name="btnAddTitle" id="proposed_add_title"/>
            </div>        
        </div> <!-- End title-blocks--> 
</div> <!-- End proposed_home_department-->

<div class="joint-departments proposed">    
</div>
<div class="buttonset">
<input type="submit" title="Add Joint Department" value="Add Joint Department" name="btnProposedAddJointDepartment" id="proposed_add_joint_department"/>
</div>                 
</fieldset>
</div>
</fieldset>

<div class="buttonset">
    <input type="submit" title="Proceed" value="Proceed" name="btnSubmit" id="save-2"/>
       <input type="reset" title="Back" value="Back" id="popupformcancel-2" name="popupformcancel"/>
</div>
       
      </div>
    </div>
</div><!-- End Step-2 -->  
</div><!-- End main -->  


</body>
</html>