<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%--
--%><c:set var="user" scope="request" value="${MIVSESSION.user}"/><%--
--%><c:if test="${constants.config.usepopups}">
<head>
<meta http-equiv="cache-control" content="no-cache, must-revalidate">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="expires" content="0">
</head>
<!-- Using Pop-Up edit dialogs -->
<script type="text/javascript">
if (typeof _container_ != 'undefined') {
    var btn = document.getElementById("popupformcancel");
    _container_.setCloseControl(btn);
}
</script>
</c:if><%-- usepopups --%>
<script type="text/javascript"><!--
    mivFormProfile = {
        required: [ ${constants.config.required} ],
        trim: ["year","name","type","degree","position"]
        /*constraints: {
            year: function(contents) {
                var valid = (1==0);
                var curYear = new Date().getFullYear();
                var numyear = contents * 1;
                valid = (numyear >= 1900 && numyear < (curYear +1));
                return valid;
            }
        }*/
    };
// -->
</script>

<div id="userheading">
	<span class="standout">${user.targetUserInfo.displayName}</span>
</div>

<%-- <!-- record: ${rec} --> --%>
<%-- constants: ${constants} --%>
<%-- constants.labels: ${constants.labels} --%>

<!-- This is the data entry form for "${recordTypeName}" records -->
<form class="mivdata" id="mytestform"
      method="post" action="<c:url value='${constants.config.saveservlet}'/>">

<input type="hidden" id="rectype" name="rectype" value="${recordTypeName}">
<input type="hidden" id="recid" name="recid" value="${rec.id}">
<input type="hidden" id="display" name="display" value="${not empty rec.display?rec.display:1}">

<!-- Year -->
<div class="formline">
<span class="textfield" title="${constants.tooltips.year}">
  <label for="year" class="f_required">${constants.labels.year}</label>  <input type="text"
         id="year" name="year"
         size="6" maxlength="20"
         value="${rec.year}"
         >
</span>
</div><!-- formline -->

<!-- Trainee Name -->
<div class="formline">
<span class="textfield" title="${constants.tooltips.name}">
  <label for="name" class="f_required">${constants.labels.name}</label>
  <input type="text"
         id="name" name="name"
         size="30" maxlength="100"
         value="${rec.name}"
         >
</span>
</div><!-- formline -->

<!-- Trainee Type -->
<div class="formline">
<span class="textfield" title="${constants.tooltips.type}">
  <label for="type" class="f_required">${constants.labels.type}</label>
  <input type="text"
         id="type" name="type"
         size="50" maxlength="50"
         value="${rec.type}"
         >
</span>
</div><!-- formline -->

<!-- Degree -->
<div class="formline">
<span class="textfield" title="${constants.tooltips.degree}">
  <label for="degree">${constants.labels.degree}</label>
  <input type="text"
         id="degree" name="degree"
         size="20" maxlength="20"
         value="${rec.degree}"
         >
</span>
</div><!-- formline -->


<!-- Current Position -->
<div class="formline">
<span class="textfield" title="${constants.tooltips.position}">
  <label for="position">${constants.labels.position}</label>
  <input type="text"
         id="position" name="position"
         size="50" maxlength="200"
         value="${rec.position}"
         >
</span>
</div><!-- formline -->

<%--
<div id="displaycb" class="cb">
  <div class="formline">
  <span class="checkbox" title="${constants.tooltips.display}">
    <input type="checkbox" id="display" name="display"${rec.display=='1'||rec.display=='true'?" checked":""}>
    <label for="display">${constants.labels.display}</label>
  </span>
  </div><!-- formline -->
</div><!-- displaycb -->
--%>
<div class="buttonset">
  <input type="submit" id="save" name="save" value="Save" title="Save">
  <!-- button id="popupformcancel" name="popupformcancel" value="Cancel">Cancel</button -->
  <input type="submit" id="popupformcancel" name="popupformcancel" value="Cancel" title="Cancel">
</div>

</form>
