<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%><%--
--%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%--
--%><!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
    <title>MIV &ndash; Error Alert</title>

    <%@ include file="/jsp/metatags.html" %>

    <link rel="shortcut icon" href="<c:url value='/images/favicon.ico'/>" type="image/x-icon" />
    
    <c:set var="user" scope="request" value="${MIVSESSION.user}"/>

    <style>
        div#icon {
            float: left;
        }
        div#message {
            float: left;
            margin-left: 2em;
        }
        div#decoration {
            clear: both;
        }
    </style>
</head>

<body>

<jsp:include page="/jsp/mivheader.jsp" />

<!-- Breadcrumbs Div -->
  <div id="breadcrumbs">
    <a href="<c:url value='/MIVMain'/>" title="Home">Home</a>
    &gt; Error Alert
  </div><!-- breadcrumbs -->


<%
    java.text.SimpleDateFormat df = new java.text.SimpleDateFormat("EEEEEEEEE MMM dd yyyy kk:mm:ss");
    String date = df.format(new java.util.Date());
    pageContext.setAttribute("errtime", date, PageContext.PAGE_SCOPE);

%>

<!-- MIV Main Div -->
  <div id="main">
    <h1>Error Alert</h1>
    <div id="icon">
        <img src="<c:url value='/images/important.jpg'/>" alt="Important!" border="0" align="left">
    </div>

    <div id="message">
        <p>
            <strong>We're sorry, an error has occurred!</strong>
        </p>
        <p>
            Contact the MIV Project Team at
            <a href="mailto:miv-help@ucdavis.edu" title="miv-help@ucdavis.edu">miv-help@ucdavis.edu</a>
            if you need further assistance.
        </p>
        <p>
            Please copy and include the following date and time when writing to miv-help about this incident:<br>
            &nbsp; ${errtime}<c:if test="${not MIVSESSION.config.productionServer}"> &nbsp; on &nbsp;${MIVSESSION.config.server}</c:if>
        </p>
    </div>

  </div><!-- main -->

<jsp:include page="/jsp/miv_small_footer.jsp" />
<%--
<c:if test="${not empty param.alertmsg}">
<script>
  alert("${param.alertmsg}");
</script>
</c:if>
--%>
</body>
</html><%--
 vi: se ts=8 et:
--%>

