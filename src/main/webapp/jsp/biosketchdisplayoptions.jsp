<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%><%--
--%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%--
--%><%@ taglib prefix="spring" uri="/spring"%><%--
--%><%@ taglib prefix="jwr" uri="http://jawr.net/tags" %><%--
--%><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
    <title>MIV &ndash; ${constants.strings.displayselectoptionstitle}</title>

    <script type="text/javascript">
    /* <![CDATA[ */
        //debugger;
        //alert("editform.jsp setting djConfig");
        djConfig = {
            isDebug: false,
            parseOnLoad: true // parseOnLoad *must* be true for dojo to create the widgets
        };
        var mivConfig = new Object();
        mivConfig.isIE = false;
    /* ]]> */
    </script>

    <link rel="stylesheet" type="text/css" href="<c:url value='/js/dojo/dojo/resources/dojo.css'/>">
    <link rel="stylesheet" type="text/css" href="<c:url value='/js/dojo/dijit/themes/tundra/tundra.css'/>">
    <link rel="stylesheet" type="text/css" href="<c:url value='/js/dojo/dojox/layout/resources/FloatingPane.css'/>">
    <link rel="stylesheet" type="text/css" href="<c:url value='/js/dojo/dojox/layout/resources/ResizeHandle.css'/>">

    <%@ include file="/jsp/metatags.html" %>

    <link rel="stylesheet" type="text/css" href="<c:url value='/css/mivcommon.css'/>">
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/mivEnterData.css'/>">
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/miv-forms.css'/>">
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/biosketch.css'/>">
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/formatOptions.css'/>">
    <!--[if lt IE 8]>
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/iehacks.css'/>">
    <script type="text/javascript">mivConfig.isIE=true;</script>
    <![endif]-->
    <%-- script type="text/javascript" src="<c:url value='/js/dojo/dojo/dojo.js'/>"></script --%>
    <script type="text/javascript" src="<c:url value='/js/dojo/dojo/dojo.js'/>"></script>
    <%-- script type="text/javascript" src="<c:url value='/js/dojo/dijit/dijit-all.js'/>"></script --%>
    <script type="text/javascript" src="<c:url value='/js/dojo/dijit/dijit.js'/>"></script>
    <script type="text/javascript" src="<c:url value='/js/dojo/dojox/layout/FloatingPane.js'/>"></script>
    <script type="text/javascript" src="<c:url value='/js/mivcommon.js'/>"></script>
    <jwr:script src="/bundles/mivvalidation.js" />
    <script type="text/javascript" src="<c:url value='/js/biosketch.js'/>"></script>
    <script type="text/javascript" src="<c:url value='/js/charpalette.js'/>"></script>
    <c:set var="user" scope="request" value="${MIVSESSION.user}" />
</head>

<body class="tundra">
    <jsp:include page="/jsp/mivheader.jsp" />
    <div id="breadcrumbs">
      <a href="<c:url value='/MIVMain'/>" title="Home">Home</a>
      &gt; <a href="<c:url value='/biosketch/BiosketchPreview'/>" title="${constants.strings.description} List">${constants.strings.description} List</a>
      &gt; <a href="<c:url value='/biosketch/BiosketchMenu'/>" title="${constants.strings.description} Menu">${constants.strings.description} Menu</a>
      &gt; ${constants.strings.displayselectoptionstitle}
    </div><!-- breadcrumbs -->

    <div id="main">
      <h1 class="biosketchheader">${constants.strings.displayselectoptionstitle}</h1>
      <c:if test="${showwizard =='true'}">
        <div id="wizardsteps">
            <p>Following are the steps to create your ${constants.strings.description}. You are currently at Step 1.</p>
            <div class="activestep"><span class="step">Step 1:</span> ${constants.strings.displayselectoptionstitle}</div>
            <div class="inactivestep"><span class="step">Step 2:</span> ${constants.strings.selectbiosketchdata}</div>
            <div class="inactivestep"><span class="step">Step 3:</span> ${constants.strings.designmybiosketch}</div>
            <div class="inactivestep"><span class="step">Step 4:</span> ${constants.strings.createmybiosketch}</div>
        </div><!-- wizardsteps -->
        </c:if>

      <spring:bind path="biosketchCommand">
        <c:if test="${not empty status.errorMessages}">
            <c:set var='errorClass' scope='page' value=' class="haserrors"'/>
      	</c:if>
      </spring:bind>

      <div id="errormessage"${errorClass}>
        <spring:bind path="biosketchCommand">
        <c:if test="${not empty status.errorMessages}">
			<div id="errorbox">
		        Error(s):
		        <ul>
		        <c:forEach var="errorMessage" items="${status.errorMessages}">
		        <li>${errorMessage}<br></li>
		        </c:forEach>
		        </ul>
		    </div>
        </c:if>
        </spring:bind>
      </div>

      <div id="enterdataguide">
        <div id="requiredfieldlegend">* = Required Field</div>
      </div>

      <div id="maindialog" class="pagedialog">
        <div class="dojoDialog mivDialog">
          <div id="userheading"><span class="standout">${user.targetUserInfo.displayName}</span></div>
          <jsp:include page="${editorform}" />
        </div><!-- mivDialog -->
      </div><!-- mainDialog -->
    </div><!-- main -->
    <jsp:include page="/jsp/mivfooter.jsp" />
</body>

</html><%--
 vim:ts=8 sw=2 et sr:
--%>
