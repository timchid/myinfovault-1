<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%><%--
--%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%--
--%><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
    <title>Publication Format Options : MyInfoVault</title>

    <link rel="shortcut icon" href="<c:url value='/images/favicon.ico'/>" type="image/x-icon" />
    <link rel="stylesheet" type="text/css" href="<c:url value='/js/dojo/dojo/resources/dojo.css'/>">
    <link rel="stylesheet" type="text/css" href="<c:url value='/js/dojo/dijit/themes/tundra/tundra.css'/>">
    <link rel="stylesheet" type="text/css" href="<c:url value='/js/dojo/dojox/layout/resources/FloatingPane.css'/>">
    <link rel="stylesheet" type="text/css" href="<c:url value='/js/dojo/dojox/layout/resources/ResizeHandle.css'/>">
    
    <%@ include file="/jsp/metatags.html" %>
    
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/mivEnterData.css'/>">
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/miv-forms.css'/>">
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/special_character2.css'/>">

    <script type="text/javascript">
    /* <![CDATA[ */
        djConfig = {
            isDebug: false,
            parseOnLoad: true // parseOnLoad *must* be true to parse the widgets
        };
    /* ]]> */
    </script>

<script type="text/javascript" src="<c:url value='/js/dojo/dojo/dojo.js'/>"></script>
<script type="text/javascript" src="<c:url value='/js/dojo/dijit/dijit.js'/>"></script>
<script type="text/javascript" src="<c:url value='/js/dojo/dojox/layout/FloatingPane.js'/>"></script>
<script type="text/javascript" src="<c:url value='/js/mivcommon.js'/>"></script>
<script type="text/javascript" src="<c:url value='/js/cvsort.js'/>"></script>
<script type="text/javascript" src="<c:url value='/js/charpalette.js'/>"></script>

<c:set var="user" scope="request" value="${MIVSESSION.user}"/>

<%-- We are currently setting all name pattern styles to the same thing
     because of the difficulty in doing dynamic creation of CSS rules to
     match each variation based on which pattern is found.
     Show the all-or-nothing set of boxes, hide the individual lines.
--%>
<style>
div#hl_all, div#hl_options {
    display: block;
    margin-left: 2em;
}
div.styleboxes {
    display: none;
}
</style>
</head>

<body>

<jsp:include page="/jsp/mivheader.jsp" />

<!-- Breadcrumbs Div -->
  <div id="breadcrumbs">
    <a href="<c:url value='/MIVMain'/>" title="Home">Home</a>
    &gt; Format Options
  </div><!-- breadcrumbs -->


<!-- MIV Main Div -->
<div id="main">

<h1>Publication Format Options</h1>

<div id="maindialog" class="pagedialog">
<div class="dojoDialog">
<form class="mivdata" id="formatting" method="post" action="?alertmsg=Formatting Options Saved">
  <h2>How would you like the publications to be sorted in your CV?</h2>

  <div class="formline">
  <span class="dropdown" title="Select the sorting order for publications in your CV">
    <label for="cvsort">Sort the publications in my CV:</label>
    <select id="cvsort" name="cvsort">
      <option value="1"${cvsort eq '1'?' selected="selected"':''}>ascending</option>
      <option value="2"${cvsort eq '2'?' selected="selected"':''}>descending</option>
    </select>
  </span>
  </div><!-- formline -->

  <h2>Would you like to highlight your name in all publications?</h2>
  <div class="formline">
    <input type="checkbox"
           ${highlight ? "checked" : ""}
           id="highlightBox" name="highlightBox">
    <label for="highlightBox">Highlight my name in all publications as:</label>
    <div id="hl_all" name="hl_all">
<input type="checkbox"
               id="bold" name="bold"
               ${bold?"checked ":""}disabled
               ><label for="bold">Bold</label>
&nbsp;  <input type="checkbox"
               id="italic" name="italic"
               ${italic?"checked ":""}disabled
               ><label for="italic">Italic</label>
&nbsp;  <input type="checkbox"
               id="underline" name="underline"
               ${underline?"checked ":""}disabled
               ><label for="underline">Underline</label>
      </div><!-- hl_all -->
  </div><!-- formline -->

<%--  <input type="checkbox" id="high_bold" name="high_bold" disabled>Bold
      <input type="checkbox" id="high_ul" name="high_ul" disabled>Underline --%>

  <div id="hl_options" name="hl_options">
    For the following name patterns:
    <div class="formline">
        <input type="hidden" id="id1" name="id1" value="${id1}">
        <input type="text" class="f_abled"
               id="name1" name="name1"
               size="30" maxlength="60"
               value="${name1}"
               >
      <div class="styleboxes">
&nbsp;  <input type="checkbox"
               id="bold1" name="bold1"
               ${bold1?"checked ":""}disabled
               ><label for="bold1">Bold</label>
        <input type="checkbox"
               id="italic1" name="italic1"
               ${italic1?"checked ":""}disabled
               ><label for="italic1">Italic</label>
        <input type="checkbox"
               id="underline1" name="underline1"
               ${underline1?"checked ":""}disabled
               ><label for="underline1">Underline</label>
      </div><!-- styleboxes -->
    </div><!-- formline -->

    <div class="formline">
        <input type="hidden" id="id2" name="id2" value="${id2}">
        <input type="text" class="f_abled"
               id="name2" name="name2"
               size="30" maxlength="60"
               value="${name2}"
               >
      <div class="styleboxes">
&nbsp;  <input type="checkbox"
               id="bold2" name="bold2"
               ${bold2?"checked ":""}disabled
               ><label for="bold2">Bold</label>
        <input type="checkbox"
               id="italic2" name="italic2"
               ${italic2?"checked ":""}disabled
               ><label for="italic2">Italic</label>
        <input type="checkbox"
               id="underline2" name="underline2"
               ${underline2?"checked ":""}disabled
               ><label for="underline2">Underline</label>
      </div><!-- styleboxes -->
    </div><!-- formline -->

    <div class="formline">
        <input type="hidden" id="id3" name="id3" value="${id3}">
        <input type="text" class="f_abled"
               id="name3" name="name3"
               size="30" maxlength="60"
               value="${name3}"
               >
      <div class="styleboxes">
&nbsp;  <input type="checkbox"
               id="bold3" name="bold3"
               ${bold3?"checked ":""}disabled
               ><label for="bold3">Bold</label>
        <input type="checkbox"
               id="italic3" name="italic3"
               ${italic3?"checked ":""}disabled
               ><label for="italic3">Italic</label>
        <input type="checkbox"
               id="underline3" name="underline3"
               ${underline3?"checked ":""}disabled
               ><label for="underline3">Underline</label>
      </div><!-- styleboxes -->
    </div><!-- formline -->

  </div><!-- #hl_options -->

  <div class="buttonset">
    <input type="submit" id="save" name="save" value="Save" title="Save">
    <input type="reset" id="reset" value="Reset" title="Reset">
  </div><!-- buttonset -->
  <input type="hidden" id="pubsort" name="pubsort" value="${pubsort}">
</form>
</div><!-- dojoDialog -->
</div><!-- pagedialog -->
<%--
cvsort:${cvsort}<br>
highlight:${highlight}<br>
name1:${name1} bold:${bold1} italic:${italic1} underline:${underline1}<br>
name2:${name2} bold:${bold2} italic:${italic2} underline:${underline2}<br>
name3:${name3} bold:${bold3} italic:${italic3} underline:${underline3}<br>
--%>

</div><!-- main -->

<jsp:include page="/jsp/mivfooter.jsp" />
</body>
</html><%--
 vi: se ts=8 et:
--%>
