<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%--
--%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %><%--
--%><%@ taglib prefix="miv" uri="mivTags" %><%--
--%><c:set var="user" scope="request" value="${MIVSESSION.user}"/><%--
--%>  <div class="" id="navskip"><a href="#main">Skip to Content</a></div>
  <!-- MIV header Div -->
  <script type="text/javascript" src="<c:url value='/js/TimeoutMenu.js'/>"></script>
  <script type="text/javascript" src="<c:url value='/js/help.js'/>"></script>

  <div id="mivheader">
    <div id="logos">
        <a target="mivexternal" href="http://www.ucdavis.edu/" title="UC Davis" id="ucdlogosmall">
            <span id="ucdlink">UC Davis</span>
        </a>
        <a href="<c:url value='/MIVMain'/>" title="MyInfoVault" id="mivlogosmall">
            <span id="mivlink">MyInfoVault</span>
        </a>
    </div><!-- #logos -->
    <div id="links">
      <div id="logout"><a class="logout" href="<c:url value='/logout'/>" title="Log Out of MIV">Log Out</a></div>
      <c:if test="${config.maintenanceMode}"><div id="maint">Maintenance Mode is ON</div></c:if>

      <div id="admin"><%--
    --%><miv:permit roles="SYS_ADMIN|VICE_PROVOST_STAFF|SENATE_STAFF|SCHOOL_STAFF|DEPT_STAFF" usertype="PROXY">
        <a href="<c:url value='/user'/>" title="Manage Users">Manage Users</a> |<%--
    --%></miv:permit>
        <a href="<c:url value='/ManageUsers?_flowId=manageUsers-flow&amp;flowType=account'/>" title="My Account">My Account</a> |
        <a href="<c:url value='/help/${helpPage}'/>" title="${fn:length(helpTip)>0 ? helpTip : 'Help'}" target="mivhelp" class="popuplink">Help</a> |
        <a href="<c:url value='/help/contact.html'/>" title="Contact Us" target="mivhelp">Contact Us</a>
<%--
      | <a href="https://www.surveymonkey.com/s.aspx?sm=QTDPFRIWqy4hAf2CYdfV_2fg_3d_3d" title="Feedback" target="mivexternal">Feedback</a>
--%>
      </div><!-- #admin -->
    </div><!-- #links -->
  </div><!-- #mivheader -->

  <!-- Navigation Menus -->
  <div id="navbar">
    <div class="loginuser">
    ${user.userInfo.displayName}<%-- (${MIVSESSION.loginName})--%>
    </div>
    <%-- It is important that the next div for the palette_button be unbroken,
         because the javascript looks for the element.firstChild which will be
         empty text + newline(s) if the line is split. Each tag must *directly*
         follow the preceding tag, with no spaces or linebreaks between.     --%>
    <div id="palette_button"><div id="IE_BotchedAgain_1" style="float:none;"><a title="The Special Character Palette is available on data entry pages">&nbsp;&Omega;&nbsp;</a></div></div>

    <%@ include file="mivmenu.html" %>

  </div><!-- navbar -->

  <div id="proxy">
    <miv:permit action="Switch User" usertype="REAL"><%--roles="SYS_ADMIN|VICE_PROVOST_STAFF|SCHOOL_STAFF|DEPT_STAFF|DEPT_ASSISTANT|OCP_STAFF" usertype="REAL"--%><%--
  --%><c:if test="${user.proxying}"><span class="proxy">(${user.targetUserInfo.displayName})
        <c:url var='nextLink' value='/SwitchBack'>
            <c:param name='_flowId' value='switchback-flow'/>
            <c:param name='id' value='${user.userID}'/>
        </c:url>
        <a href="<c:out value='${nextLink}' escapeXml='true'/>"
           title="Return to My Account">Return to My Account</a></span></c:if><%--
  --%><c:if test="${!user.proxying}"><%--
    --%><c:url var='selectLink' value='/SelectUser'>
            <c:param name='_flowId' value='select-flow'/>
        </c:url><%--
    --%><div class="proxy">
            <a href="<c:out value='${selectLink}' escapeXml='true'/>"
               title="${fn:length(MIVSESSION.mru) gt 0 ? "" : "Allows you to act on behalf of another user"}">Select a User's Account</a>
            <c:if test="${fn:length(MIVSESSION.mru) gt 0}">
            <ul id="proxymru" class="mru">
            <c:forEach var="proxyPerson" items="${MIVSESSION.mru}" varStatus="entryNum">
                <li><a href="switchUser?pick=${entryNum.index}"><c:out value='${proxyPerson.displayName}' escapeXml='true'/></a></li>
            </c:forEach>
            </ul>
            </c:if>
        </div>
      </c:if><%--
--%></miv:permit>
  </div><!-- proxy -->
