<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%--
--%><c:set var="user" scope="request" value="${MIVSESSION.user}"/>
<jsp:include page="/jsp/wysiwyginit.inc" />
<div>
  <div id="userheading"><span class="standout">${user.targetUserInfo.displayName}</span></div>
</div>

<!-- This is the data entry form for "${recordTypeName}" records -->
  <script type="text/javascript"><!--
    mivFormProfile = {
          required: [],
          trim: ["content" ]
        };

    function isEmpty(x)
    {
        while (x.substring(0,1) == ' ') {
            x = x.substring(1);
        }

        if (x.length > 0) {
            return false;
        }
        else {
            return true;
        }
    }
  // -->
  </script>

<form class="mivdata" id="mytestform" name="mytestform"
      method="post" action="<c:url value='${constants.config.saveservlet}'/>">
  <input type="hidden" name="headerid" id="headerid" value="${rec.headerid}"/>
  <input type="hidden" name="recordid" id="recordid" value="${rec.id}"/>
  <input type="hidden" name="subtype" id="subtype" value="${subtype}"/>
  <input type="hidden" name="documentid" id="documentid" value="${documentid}"/>
  <input type="hidden" id="sectionname" name="sectionname" value="${sectionname}">
  <input type="hidden" id="rectype" name="rectype" value="${recordTypeName}">

  <div class="formline">
    <h2 class="whitespacepreserve">${constants.labels.header} ${rec.heading}</h2>
    <input type="hidden" id="heading" name="heading" value="${rec.heading}"> <!-- Just to hold the heading name -->
  </div>

  <div class="formline">
  <span class="textfield" title="${constants.tooltips.content}">&#8224;&nbsp;
    <label for="content" class="f_required">${constants.labels.content}</label><br />
    <textarea id="content"  name="content" cols="95" rows="10" class="mceEditor">${rec.content}</textarea>
  </span>
  </div><!-- formline -->

  <div class="buttonset">
    <input type="submit" name="save" id="save" value="Save" title="Save" />
    <input type="submit" id="popupformcancel" name="popupformcancel" value="Cancel" title="Cancel">    
  </div>
</form>
<%--
 vi: se ts=8 sw=2 et:
--%>
