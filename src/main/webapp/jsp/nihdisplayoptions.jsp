<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%--
--%><%@ taglib prefix="spring" uri="/spring" %><%--
--%>
  <script type="text/javascript">
  /* <![CDATA[ */
      mivFormProfile = {
          required: [ "name","title","programDirectorName","fullName","positionTitle1"],
          trim: ["name","title","programDirectorName","fullName","positionTitle1"]
      };
  /* ]]> */
  </script>
          <form class="mivdata" id="biosketchform" method="post" action="<c:url value='/biosketch/BiosketchDisplayOptions'/>">
            <p>
              <a href=http://grants.nih.gov/grants/funding/424/index.htm target="mivexternal"
                 title="NIH SF424 (R&amp;R) Application and Electronic Submission Information"
               >NIH SF424 (R&amp;R) Application and Electronic Submission Information</a>
            </p>

            <div class="formline">
              <span class="textfield" title="${constants.tooltips.documentname}">
                <label class="f_required" errorlabel="Document Name" for="name">${constants.labels.documentname}</label>
                <spring:bind path="biosketchCommand.name">
                <input type="text"
                       id="name" name="name"
                       size="50" maxlength="255"
                       value="${biosketchCommand.name}">
                </spring:bind>
              </span>
            </div><!--formline-->

            <div class="formline">
              <span class="textfield" title="${constants.tooltips.documenttitle}">
                <label class="f_required" errorlabel="Document Title" for="title">${constants.labels.documenttitle}</label>
                <br>
                <spring:bind path="biosketchCommand.title">
                <input type="text" readonly disabled
                       id="title" name="title"
                       size="50" maxlength="255"
                       value="${biosketchCommand.title}">
                </spring:bind>
              </span>
            </div><!--formline-->

            <div class="formline">
              <span class="textfield" title="${constants.tooltips.programdirectorname}">
                <label class="f_required" errorlabel="Program Director/Principal Investigator" for="programDirectorName">${constants.labels.programdirectorname}</label>
                <br>
                <spring:bind path="biosketchCommand.programDirectorName">
                <input type="text"
                       id="programDirectorName" name="programDirectorName"
                       size="50" maxlength="255"
                       value="${biosketchCommand.programDirectorName}">
                </spring:bind>
              </span>
            </div><!--formline-->

            <div class="formline">
              <span class="textfield" title="${constants.tooltips.fullname}">
                <label class="f_required" errorlabel="Full Name" for="fullName">${constants.labels.fullname}</label>
                <br>
                <spring:bind path="biosketchCommand.fullName">
                <input type="text"
                       id="fullName" name="fullName"
                       size="50" maxlength="255"
                       value="${biosketchCommand.fullName}">
                </spring:bind>
              </span>
            </div><!--formline-->

            <div class="formline">
              <span class="textfield" title="${constants.tooltips.erausername}">
                <label for="eraUserName">${constants.labels.erausername}</label><br>
                (You must be registered with <a href="https://commons.era.nih.gov/commons/" title="eRA Commons" target="mivexternal">eRA Commons</a>)
                <br>
                <spring:bind path="biosketchCommand.eraUserName">
                <input type="text"
                       id="eraUserName" name="eraUserName"
                       size="50" maxlength="255"
                       value="${biosketchCommand.eraUserName}">
                </spring:bind>
              </span>
            </div><!--formline-->


            <div class="formline">
              <span class="textfield" title="${constants.tooltips.positiontitle1}">
                <label class="f_required" for="positionTitle1">${constants.labels.positiontitle1}</label>
                <br>
                <spring:bind path="biosketchCommand.positionTitle1">
                <input type="text"
                       id="positionTitle1" name="positionTitle1"
                       size="50" maxlength="255"
                       value="${biosketchCommand.positionTitle1}">
                </spring:bind>
              </span>
            </div><!--formline-->

            <div class="formline">
              <span class="textfield" title="${constants.tooltips.positiontitle2}">
                <label for="positionTitle2">${constants.labels.positiontitle2}</label>
                <br>
                <spring:bind path="biosketchCommand.positionTitle2">
                <input type="text"
                       id="positionTitle2" name="positionTitle2"
                       size="50" maxlength="255"
                       value="${biosketchCommand.positionTitle2}">
                </spring:bind>
              </span>
            </div><!--formline-->

            <c:set var="bodyformattingselected" value="${biosketchCommand.bodyFormatting=='1'}"/>
            <div class="formline"><!-- body formatting -->
              <fieldset class="mivfieldset">
                      <legend></legend>
                      <span class="formlabel" title="${constants.tooltips.formatoptions}">
                        ${constants.labels.formatoptions}
                      </span>
                      <span class="radioset">
                          <span class="radiobutton" title="${constants.tooltips.yes}"><label for="bodyFormattingYes">${constants.labels.yes}</label>
                            <spring:bind path="biosketchCommand.bodyFormatting">
                            <input type="radio" id="bodyFormattingYes" name="bodyFormatting" value="1" ${bodyformattingselected?" checked":""}>&nbsp;
                            </spring:bind>
                          </span>
                          <span class="radiobutton" title="${constants.tooltips.no}"><label for="bodyFormattingNo">${constants.labels.no}</label>
                            <spring:bind path="biosketchCommand.bodyFormatting">
                            <input type="radio" id="bodyFormattingNo" name="bodyFormatting" value="0" ${!bodyformattingselected?" checked":""}>&nbsp;
                            </spring:bind>
                          </span>
                      </span><!-- radioset -->
              </fieldset>
            </div><!-- body formatting -->

            <div class="buttonset">
              <c:if test="${showwizard =='true'}">
              <input type="submit" id="save" name="btnSubmit" value="Proceed" title="Proceed">
              </c:if>
              <c:if test="${showwizard !='true'}">
              <input type="submit" id="save" name="btnSubmit" value="Save" title="Save">
              </c:if>
              <input type="reset" name="popupformcancel" id="popupformcancel" value="Cancel" title="Cancel">
            </div>
          </form>
<%--
 vim:ts=8 sw=2 et sr:
--%>
