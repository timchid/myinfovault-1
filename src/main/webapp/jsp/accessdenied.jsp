<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%><%--
--%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%--
--%><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
    <title>MIV &ndash; Access Denied</title>

    <%@ include file="/jsp/metatags.html" %>

    <c:set var="user" scope="request" value="${MIVSESSION.user}"/>

    <style>
        div#icon {
            float: left;
        }
        div#message {
            float: left;
            margin-left: 2em;
        }
        div#decoration {
            clear: both;
        }
    </style>
</head>

<body>

<jsp:include page="/jsp/mivheader.jsp" />

<!-- Breadcrumbs Div -->
  <div id="breadcrumbs">
    <a href="<c:url value='/MIVMain'/>" title="Home">Home</a>
  </div><!-- breadcrumbs -->


  <div id="main">
    <h1>Access Denied</h1>
    <div id="icon">
        <img src="<c:url value='/images/important.jpg'/>" alt="Important!" border="0" align="left">
    </div>

    <div id="message">
        <p>
        <strong>We're sorry, you do not have authorization to view this page or perform this action.</strong>
        </p>
        <p>
        Contact the MIV Project Team at
        <a href="mailto:miv-help@ucdavis.edu" title="miv-help@ucdavis.edu">miv-help@ucdavis.edu</a>
        if you need further assistance.
        </p>
    </div>

    <div id="decoration">
        <img src="<c:url value='/images/egghead2.jpg'/>" border="0"
             alt="Robert Arneson's Egghead ceramic sculpture located at UC Davis">
    </div>
  </div><!-- main -->

<jsp:include page="/jsp/miv_small_footer.jsp" />
</body>
</html><%--
 vi: se ts=8 sw=4 sr et:
--%>
