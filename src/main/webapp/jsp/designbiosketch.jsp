<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="/spring" %>

<html>
<head>
    <title>Design My NIH Biosketch</title>
</head>
<body>
    <h1>Design My NIH Biosketch</h1>
    <form method="post" action="<c:url value='/designbiosketch.htm'/>">
        <input type="submit" alignment="center" value="Proceed" title="Proceed">
        <input type="submit" alignment="center" value="Cancel" title="Cancel">
    </form>
</body>
</html>
