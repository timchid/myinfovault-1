<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%--
--%><c:set var="user" scope="request" value="${MIVSESSION.user}"/><%--
--%><c:if test="${constants.config.usepopups}">
<head>
<meta http-equiv="cache-control" content="no-cache, must-revalidate">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="expires" content="0">
</head>
<!-- Using Pop-Up edit dialogs -->
<script type="text/javascript">
if (typeof _container_ != 'undefined') {
    var btn = document.getElementById("popupformcancel");
    _container_.setCloseControl(btn);
}
</script>
</c:if><%-- usepopups --%>
<script type="text/javascript"><!--
    mivFormProfile = {
        required: [],
        trim: ["yearspan", "content", "remark"]
    };
// -->
</script>

<div id="userheading">
	<span class="standout">${user.targetUserInfo.displayName}</span>
</div>

<%-- <!-- record: ${rec} --> --%>
<%-- constants: ${constants} --%>
<%-- constants.labels: ${constants.labels} --%>

<!-- This is the data entry form for "${recordTypeName}" records -->
<form class="mivdata" id="mytestform"
      method="post" action="<c:url value='${constants.config.saveservlet}'/>">

<input type="hidden" id="rectype" name="rectype" value="${recordTypeName}">
<input type="hidden" id="recid" name="recid" value="${rec.id}">
<input type="hidden" id="display" name="display" value="${not empty rec.display?rec.display:1}">

<div class="formline">
<!-- Year -->
<span class="textfield" title="${constants.tooltips.yearspan}">
  <label for="yearspan" class="f_required">${constants.labels.yearspan}</label>
  <input type="text"
         class="f_numeric"
         id="yearspan" name="yearspan"
         size="10" maxlength="20"
         value="${rec.yearspan}"
         >
</span>
</div><!-- formline -->


<!-- Description -->
<div class="formline">
<span class="textfield" title="${constants.tooltips.content}">
  <label for="content" class="f_required">${constants.labels.content}</label><br />
  <textarea
            id="content" name="content"
            rows="4" cols="40" wrap="soft"
            >${rec.content}</textarea>
</span>
</div><!-- formline -->

<!-- Remark -->
<div class="formline">
<span class="textfield" title="${constants.tooltips.remark}">
  <label for="remark">${constants.labels.remark}</label><br />
  <textarea
            id="remark" name="remark"
            rows="4" cols="40" wrap="soft"
            >${rec.remark}</textarea>
</span>
</div><!-- formline -->

<%--
<div id="displaycb" class="cb">
  <div class="formline">
  <span class="checkbox" title="${constants.tooltips.display}">
    <input type="checkbox" id="display" name="display"${rec.display=='1'||rec.display=='true'?" checked":""}>
    <label for="display">${constants.labels.display}</label>
  </span>
  </div><!-- formline -->
</div><!-- displaycb -->
--%>
<div class="buttonset">
  <input type="submit" id="save" name="save" value="Save" title="Save">
  <!-- button id="popupformcancel" name="popupformcancel" value="Cancel">Cancel</button -->
  <input type="submit" id="popupformcancel" name="popupformcancel" value="Cancel" title="Cancel">
</div>

</form>
