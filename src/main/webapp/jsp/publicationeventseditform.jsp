<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%--
--%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %><%--
--%><c:set var="user" scope="request" value="${MIVSESSION.user}"/><%--
--%><c:if test="${usepopups || constants.config.usepopups}"><%--
--%><!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="cache-control" content="no-cache, must-revalidate">
        <meta http-equiv="pragma" content="no-cache">
        <meta http-equiv="expires" content="0">

        <%@ include file="/jsp/metatags.html" %>

        <script type="text/javascript" src="<c:url value='/js/mivcommon.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/location.js'/>"></script>
        
        <link rel="stylesheet" type="text/css" href="<c:url value='/css/mivcommon.css'/>">
        <link rel="stylesheet" type="text/css" href="<c:url value='/css/mivEnterData.css'/>">
        <link rel="stylesheet" type="text/css" href="<c:url value='/css/miv-forms.css'/>">
        <link rel="stylesheet" type="text/css" href="<c:url value='/css/expandable.css'/>">
        <link rel="stylesheet" type="text/css" href="<c:url value='/css/creativeactivities.css'/>">
    </head>
<!-- Using Pop-Up edit dialogs -->
<script type="text/javascript">
    if (typeof _container_ != 'undefined') {
        var btn = document.getElementById("popupformcancel");
        _container_.setCloseControl(btn);
    }
</script>
</c:if><%-- usepopups --%>

<%--
 This 'usepopups' test feels wrong to me, and seems to be taking something that was experimental and running too far with it.
 If you AREN'T using popups then you're being jsp:included into the editform.jsp framework, and it already includes many of
 these .css and .js files (notably, I am seeing mivEnterData being included twice).
 If you ARE using popups (which is never, since they were only experimental) you should count on the popup framework to provide
 most of what you need and you'd only inlucde things that are very specific to your edit form. Again, you wouldn't need to
 include mivEnterData.css or mivcommon.js and so on.
--%>
<c:if test="${ !(usepopups || constants.config.usepopups) }">
        <link rel="stylesheet" type="text/css" href="<c:url value='/css/expandable.css'/>">
        <link rel="stylesheet" type="text/css" href="<c:url value='/css/creativeactivities.css'/>">
        
        <script type="text/javascript" src="<c:url value='/js/popupeditform.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/creativeactivities/events.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/creativeactivities/creativeactivities.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/location.js'/>"></script>
</c:if>

<jsp:scriptlet><![CDATA[
   Integer d = new Integer(java.util.Calendar.getInstance().get(java.util.Calendar.YEAR));
   pageContext.setAttribute("thisYear", d, PageContext.REQUEST_SCOPE);
   ]]></jsp:scriptlet>

<c:if test="${ !(usepopups || constants.config.usepopups) }">
<script type="text/javascript">
//<![CDATA[
    mivFormProfile = {
        required: [${constants.config.required}],
        trim: [${constants.config.required}],
        constraints: {
            year: function(contents) {
                    if (isNaN(contents))
                    {
                        return "INVALID_NUMBER";
                    }
                    else if ( parseInt(contents) < 1900 || parseInt(contents) > (parseInt($("#thisYear").val())+2) )
                    {
                        return "INVALID_YEAR";
                    }
                return true;
            },
            statusid: miv.validation.isDropdown,
            url: miv.validation.isLink
        }
    };
//]]>
</script>
</c:if>

<script type="text/javascript">
    $(document).ready(function() {
        $("#association").addClass('hidden');
    });
</script>

<c:if test="${usepopups || constants.config.usepopups}">
<body class="usepopups ">


    <div id="main">
        <div class="pagedialog" id="maindialog">
            <div class="dojoDialog mivDialog"><!--might want to be more flexible with dialog widths-->
</c:if>

<div id="userheading">
    <span class="standout">${user.targetUserInfo.displayName}</span>
</div>

<!-- dummy hidden values -->
<input type="hidden" id="usepopups" value="${ (usepopups || constants.config.usepopups) }">
<input type="hidden" id="thisYear" value="${thisYear}">

<!-- This is the data entry form for "${recordTypeName}" records -->
<form class="mivdata" id="mytestform" name="mytestform"
      method="post" action="<c:url value='${constants.config.saveservlet}'/>">

    <input type="hidden" id="class" name="recordClass" value="${recordClass}">
    <input type="hidden" id="rectype" name="rectype" value="${recordTypeName}">
    <input type="hidden" id="recid" name="recid" value="${rec.id}">
    <input type="hidden" id="display" name="display" value="${not empty rec.display?rec.display:1}">

    <div class="formline">
        <span class="textfield" title="${constants.tooltips.year}">
            <label for="year" class="f_required">${constants.labels.year}</label>
            <input type="text" id="year" name="year" size="5" maxlength="4" value="${rec.year}" title="Year must be from 1900 to ${thisYear+2}">
        </span>

        <span class="textfield" title="${constants.tooltips.statusid}">
            <label for="statusid" class="f_required">${constants.labels.statusid}</label>
            <select id="statusid" name="statusid"><%--
            --%><c:forEach var="opt" items="${constants.options.publicationstatus}">
                <option value="${opt.id}"${ ((rec.statusid==null && opt.id==1)? ' selected="selected"': (( rec.statusid==opt.id)?' selected="selected"':''))} title="${opt.name}">${opt.name}</option><%--
            --%></c:forEach>
            </select>
        </span>
    </div>

    <div class="formline">
        <span class="textfield" title="${constants.tooltips.title}">
            <label for="title" class="f_required">${constants.labels.title}</label>
            <input type="text" id="title"
                   name="title" value="${rec.title}" size="50"
                   maxlength="255">
        </span>
    </div><!--formline-->

    <div class="formline">
        <span class="textfield" title="${constants.tooltips.publication}">
            <label for="publication">${constants.labels.publication}</label>
            <input type="text" id="publication"
                   name="publication" value="${rec.publication}" size="50"
                   maxlength="255">
        </span>
    </div><!--formline-->

    <!-- Editors, Publisher, City -->
    <div id="publisherinfo">
        <div class="formline">
            <span class="textfield" title="${constants.tooltips.creators}">
                <label for="creators" class="f_required">${constants.labels.creators}</label><br>
                <textarea id="creators" name="creators"
                          rows="2" cols="51" wrap="soft"
                    >${rec.creators}</textarea>
            </span>
        </div><!--formline-->

        <div class="formline">
            <span class="textfield" title="${constants.tooltips.editors}">
                <label for="editors">${constants.labels.editors}</label>
                <input type="text"
                       id="editors" name="editors"
                       size="48" maxlength="255"
                       value="${rec.editors}"
                       >
                <%-- <textarea id="editors" name="editors"
                          rows="2" cols="51" wrap="soft"
                    >${rec.editors}</textarea> --%>
            </span>
        </div><!--formline-->

        <div class="formline">
            <span class="textfield" title="${constants.tooltips.publisher}">
                <label for="publisher">${constants.labels.publisher}</label>
                <input type="text"
                       id="publisher" name="publisher"
                       size="48" maxlength="255"
                       value="${rec.publisher}"
                       >
            </span>
        </div><!--formline-->
    </div><!-- publisherinfo -->

    <div id="locationblock"> <!-- Location Block -->
        <c:set var="tempcountry" value="${(not empty rec.id ? ( not empty rec.country ? rec.country : '') : constants.config.contrycode)}"/><%--
    --%><c:set var="tempprovince" value="${(not empty rec.id ? ( not empty rec.province ? rec.province : '') : constants.config.province)}"/>

        <div class="formline">
            <span class="textfield" title="${constants.tooltips.country}">
                <label for="country">${constants.labels.country}</label>
                <select id="country" name="country">
                    <option value="">--- Select Country ---</option><%--
                --%><c:forEach var="opt" items="${constants.options.countries}">
                    <option value="${opt.id}"${tempcountry==opt.id?' selected="selected"':''} title="${opt.value}">${opt.value}</option><%--
                --%></c:forEach>
                </select>
            </span>
        </div><!--formline-->

        <div class="formline">
            <div class="textfield" title="${constants.tooltips.province}" id="provinceblock">
                <label for="province">${constants.labels.province}</label>
                <select id="province" name="province">
                    <option value="">--- Select State/Province ---</option><%--
                --%><c:forEach var="opt" items="${constants.options.provinces}">
                    <option value="${opt.id}"${tempprovince==opt.id?' selected="selected"':''} title="${opt.value}">${opt.value}</option><%--
                --%></c:forEach>
                </select>
            </div>

            <div class="textfield" title="${constants.tooltips.city}" id="cityblock">
                <label for="city">${constants.labels.city}</label>
                <input type="text" id="city" name="city" value="${rec.city}">
            </div>
        </div><!--formline-->

    </div><!-- Location Block -->

    <!-- Volume, Issue, Pages -->
    <div class="formline">
        <span class="textfield" title="${constants.tooltips.volume}">
            <label for="volume">${constants.labels.volume}</label>
            <input type="text"
                   id="volume" name="volume"
                   size="5" maxlength="20"
                   value="${rec.volume}"
                   >
        </span>

        <span class="textfield" title="${constants.tooltips.issue}">
            <label for="issue">${constants.labels.issue}</label>
            <input type="text"
                   id="issue" name="issue"
                   size="5" maxlength="20"
                   value="${rec.issue}"
                   >
        </span>

        <span class="textfield" title="${constants.tooltips.pages}">
            <label for="pages">${constants.labels.pages}</label>
            <input type="text"
                   id="pages" name="pages"
                   size="10" maxlength="20"
                   value="${rec.pages}"
                   >
        </span>
    </div><!--formline-->

    <!-- URL / Link -->
    <div class="formline" id="urlformline">
        <span class="textfield" title="${constants.tooltips.url}">
        <label for="url">${constants.labels.url}</label>
        <input type="url" pattern="(https?|ftp):\/\/.+"
               id="url" name="url"
               placeholder="http://example.com/some+page"
               size="52" maxlength="255"
               value="${rec.url}"
               >
        </span>
    </div><!-- formline -->

<c:if test="${ !(usepopups || constants.config.usepopups) }">
<input type="hidden" id="selectedids" name="selectedids" value="">
<fieldset id="associate_fieldset" name="associate_fieldset" class="collapsible ">
    <legend>Associate Works with this Event</legend>
    <div class="hint">expand to see association form</div>

    <div id="association">

        <!-- association error field -->
        <label for="association" class="hidden"> Associate Works </label>

        <div class="associationbuttons">
            <div class="buttonset">
                <!-- <a title="Add a new event" href="/miv/EditRecord?rectype=events&pubtype=95&documentid=&sectionname=&subtype=&E0000=Add+a+New+Record" class="linktobutton mivbutton">Add&nbsp;A&nbsp;New&nbsp;Event</a> -->
                <input type="button" id="newevent" name="newevent" value="Add a New Work" title="Add a New Work" onclick="">
            </div>
        </div>

        <div class="formline">
            <%-- <iframe src="<c:url value='pages/creativeactivitiesassociation.jsp'/>" height="350px" width="590px" frameborder="0" scrolling="yes" style="overflow:auto;">
            </iframe> --%>
            <iframe id="associationframe" name="associationframe" src="<c:url value='/Associate?requestType=init&rectype=${recordTypeName}&recid=${rec.id}&year=${rec.year}'/>" frameborder="0" scrolling="yes">
            </iframe>
        </div><!--formline-->

    </div><!-- association -->
</fieldset>
</c:if>

<c:if test="${ !(usepopups || constants.config.usepopups) }">
    <div class="buttonset">
        <input type="submit" id="save" name="save" value="Save" title="Save">
        <!-- button id="popupformcancel" name="popupformcancel" value="Cancel">Cancel</button -->
        <input type="submit" id="popupformcancel" name="popupformcancel" value="Cancel" title="Cancel">
    </div>
</c:if>

<c:if test="${ !(usepopups || constants.config.usepopups) }">
<div id="dialogBox">
    <div id="dialog-form" title="Add a New Work" >
        <%-- Add form at runtime --%>
    </div>
</div>
</c:if>

</form>

<c:if test="${usepopups || constants.config.usepopups}">
</div>
</div>
</div>
</body>
</html>
</c:if>

