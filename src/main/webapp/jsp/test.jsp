<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%><%--
--%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%--
--%><!DOCTYPE html>
<html>
    <head>
        <title>MIV &ndash; Widget Test</title>

        <%@ include file="metatags.html" %>
    </head>

    <body>
        <jsp:include page="mivheader.jsp" />

        <div id="main">
            <h1>Widget Test</h1>

            <form>
                <%@ include file="account-search.jspf" %>

                <%@ include file="dossier-search.jspf" %>
            </form>
        </div>

        <jsp:include page="miv_small_footer.jsp" />
    </body>
</html>