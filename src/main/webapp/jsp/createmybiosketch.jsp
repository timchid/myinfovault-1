<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%><%--
--%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%--
--%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%><%--
--%><%@ taglib prefix="spring" uri="/spring"%><%--
--%><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
    <title>MIV &ndash; ${constants.strings.createmybiosketch}</title>
    <%@ include file="/jsp/metatags.html"%>
    <script type="text/javascript">
    /* <![CDATA[ */
        var mivConfig = new Object();
        mivConfig.isIE = false;
    /* ]]> */
    </script>
    
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/mivEnterData.css'/>">
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/miv-forms.css'/>">
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/mivcommon.css'/>">
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/biosketch.css'/>">
    <!--[if lt IE 8]>
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/iehacks.css'/>">
    <script type="text/javascript">mivConfig.isIE=true;</script>
    <![endif]-->

    <c:set var="user" scope="request" value="${MIVSESSION.user}" />
</head>
<body>
    <jsp:include page="/jsp/mivheader.jsp" />
    <div id="breadcrumbs">
        <a href="<c:url value='/MIVMain'/>" title="Home">Home</a>
        &gt; <a href="<c:url value='/biosketch/BiosketchPreview'/>" title="${constants.strings.description} List">${constants.strings.description} List</a>
        &gt; <a href="<c:url value='/biosketch/BiosketchMenu'/>" title="${constants.strings.description} Menu">${constants.strings.description} Menu</a>
        &gt; ${constants.strings.createmybiosketch}
    </div><!-- breadcrumbs -->

    <div id="main">
        <h1 class="biosketchheader">${constants.strings.createmybiosketch}</h1>
        <c:if test="${showwizard =='true'}">
        <div id="wizardsteps">
            <p>Following are the steps to create your ${constants.strings.description}. You are currently at Step 4.</p>
            <div class="inactivestep"><span class="step">Step 1:</span> ${constants.strings.displayselectoptionstitle}</div>
            <div class="inactivestep"><span class="step">Step 2:</span> ${constants.strings.selectbiosketchdata}</div>
            <div class="inactivestep"><span class="step">Step 3:</span> ${constants.strings.designmybiosketch}</div>
            <div class="activestep"><span class="step">Step 4:</span> ${constants.strings.createmybiosketch}</div>
        </div><!-- wizardsteps -->
        </c:if>
        <c:if test="${!hasDocuments}">
        <p>
        No data has been entered or no data has been set to print for your
        ${constants.strings.description}. Please enter data or set some data to
        print, and then you will be able to create your document. Print options
        are available on the "${constants.strings.designmybiosketch}" page.
        </p>
        </c:if>

        <form action="#" method="post">
            <c:if test="${hasDocuments}">
            <c:set var="docCount" value="${fn:length(documents)}" />
            <%-- MIV-3676: Only show RTF format document for NIH --%>
            <c:choose>
              <c:when test="${fn:contains(constants.strings.description,'NIH')}">
                <div style="clear: both; padding-bottom: 1em; padding-top: 1em;">
                  <strong>${biosketchname}</strong> has been created in the following RTF format.
                </div>
                <!--p>
                   <strong>Please be sure to complete section “A. Personal Statement”</strong> for NIH Biosketches using the NIH
                   instructions (<a title="800K PDF File" target="mivexternal" href="http://grants.nih.gov/grants/funding/phs398/phs398.pdf">http://grants.nih.gov/grants/funding/phs398/phs398.pdf</a>)
                   In addition, <strong>the NIH now requires the dates in the Education/Training section to be in MM/YY format.</strong>
                   Please review the Education/Training data for valid month data and correct where
                   needed either by editing the RTF document or correcting the source data in MIV
                   under <em>Enter Data: Education: Education/Training.</em>
                   http://grants.nih.gov/grants/funding/phs398/phs398.html#updates
                </p-->
                <p>
                    <strong>Please be sure to complete section “A. Personal Statement”</strong> for NIH Biosketches using the
                    NIH <a title="800K PDF File" target="mivexternal" href="http://grants.nih.gov/grants/funding/phs398/phs398.pdf">instructions for PHS 398</a>
                    grant application.
                    In addition, <strong>the NIH now requires the dates in the Education/Training section to be in MM/YY format.</strong>
                    Please review the Education/Training data for valid month data and correct where
                    needed either by editing the RTF document or correcting the source data in MIV
                    under <em>Enter Data: Education: Education/Training.</em>
                    <br>Review the <a href="http://grants.nih.gov/grants/funding/phs398/phs398.html#updates" target="mivexternal">notable changes</a>
                    that have been made to the PHS 398 form and instructions.
                </p>
              </c:when>
              <c:otherwise>
                <div style="clear: both; padding-bottom: 1em; padding-top: 1em;">
                  <strong>${biosketchname}</strong> has been created in the following formats:
                </div>
              </c:otherwise>
            </c:choose>

            <c:set var="damagedFiles" value="0"/>
            <ul class="itemlist">
	            <c:forEach var="documentDetail" items="${documents}">
	            <li>
	                <c:set var="documents" value="${documentDetail.outputDocuments}"/><%--
	            --%><c:set var="documentName" value="${documentDetail.description}"/><%--
	            --%><c:forEach var="document" items="${documents}"><%--
	
	            <!-- MIV-3676: Only show RTF format document for NIH -->
	           --%><c:if test="${fn:contains(constants.strings.description,'NIH') && document.documentFormat != 'PDF' || !fn:contains(constants.strings.description,'NIH')}"><%--
	            --%><c:choose><%--
	                --%><c:when test="${document.createSuccess}">
	                    <a title="${documentName} ${document.documentFormat}" href="${document.outputUrl}">${document.documentFormat}</a>&nbsp; ${documentName}&nbsp;  <br>
	                    </c:when><%--
	                --%><c:otherwise><%--
	                    --%><c:set var = "damagedFiles" value="${damagedFiles+1}"/>
	                        <strong class="warning">ERROR-${document.documentFormat}</strong>&nbsp; ${documentName}&nbsp;  <br>
	                    </c:otherwise><%--
	            --%></c:choose><%--
	           --%></c:if><%--
	            --%></c:forEach>
	            </li>
	            </c:forEach>
	        </ul>    
            <%--
        --%><c:choose><%--
            --%><c:when test="${damagedFiles > 0}"><%--
                --%><c:set var="fileWord" value="file"/><%--
                --%><c:set var="verb" value="was"/><%--
                --%><c:if test="${damagedFiles > 1}"><%--
                    --%><c:set var="fileWord" value="files"/><%--
                    --%><c:set var="verb" value="were"/><%--
                --%></c:if>
                    <p class="warning">
                    ERROR: ${damagedFiles} damaged ${fileWord} could not be created.
                    </p>
                </c:when><%--
        --%></c:choose><%--
        --%></c:if>
        </form>
        <p>
            <span class="small">
            <!-- MIV-3676: Only show RTF format document for NIH -->
            <c:if test="${!fn:contains(constants.strings.description,'NIH')}">
              PDF = Uneditable "Portable Document Format."  Viewable with a PDF reader.<br>
            </c:if>
            RTF = Editable "Rich Text Format."  Viewable with a text editor like MS Word, etc.
            </span>
        </p>
        <p>
        If you have problems creating or viewing your ${constants.strings.description},
        please contact the MIV Project Team at
        <a title="miv-help@ucdavis.edu" href="mailto:miv-help@ucdavis.edu">miv-help@ucdavis.edu</a>.
        </p>
    </div> <!-- main -->
    <jsp:include page="/jsp/miv_small_footer.jsp" />
</body>
</html><%--
 vim:ts=8 sw=4 et sr:
--%>
