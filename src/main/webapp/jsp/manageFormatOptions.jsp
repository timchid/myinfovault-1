<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%><%--
--%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %><%--
--%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%--
--%><%@ taglib prefix="jwr" uri="http://jawr.net/tags" %><%--
--%><!DOCTYPE html>
<html>
<head>
    <title>MIV &ndash; Manage Format Options</title>

    <link rel="stylesheet" type="text/css" href="<c:url value='/js/dojo/dojo/resources/dojo.css'/>">
    <link rel="stylesheet" type="text/css" href="<c:url value='/js/dojo/dijit/themes/tundra/tundra.css'/>">
    <link rel="stylesheet" type="text/css" href="<c:url value='/js/dojo/dojox/layout/resources/FloatingPane.css'/>">
    <link rel="stylesheet" type="text/css" href="<c:url value='/js/dojo/dojox/layout/resources/ResizeHandle.css'/>">

    <%@ include file="/jsp/metatags.html" %>

    <link rel="stylesheet" type="text/css" href="<c:url value='/css/mivEnterData.css'/>">
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/miv-forms.css'/>">
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/mivcommon.css'/>">
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/formatOptions.css'/>">

    <script type="text/javascript">
    /* <![CDATA[ */
        djConfig = {
            isDebug: false,
            parseOnLoad: true // parseOnLoad *must* be true to parse the widgets
        };
        var mivConfig = new Object();
        mivConfig.isIE = false;
    /* ]]> */
    </script>

<!--[if lt IE 8]>
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/iehacks.css'/>">
    <script type="text/javascript">mivConfig.isIE=true;</script>
 <![endif]-->

    <script type="text/javascript" src="<c:url value='/js/dojo/dojo/dojo.js'/>"></script>
    <script type="text/javascript" src="<c:url value='/js/dojo/dijit/dijit.js'/>"></script>
    <script type="text/javascript" src="<c:url value='/js/dojo/dojox/layout/FloatingPane.js'/>"></script>
    <script type="text/javascript" src="<c:url value='/js/charpalette.js'/>"></script>
    <script type="text/javascript" src="<c:url value='/js/mivcommon.js'/>"></script>
    <jwr:script src="/bundles/mivvalidation.js" />
    <script type="text/javascript" src="<c:url value='/js/fieldFormat.js'/>"></script>
    <c:set var="user" scope="request" value="${MIVSESSION.user}"/>

    <script type="text/javascript">
    //<![CDATA[
        var mivFormProfile = {
            required: [],
            trim: [],
            constraints: {
            },
            errormap: { "INVALID_PATTERN_FORMAT":"All three fields are required to set a pattern format." }
        };
    //]]>
     </script>
</head>

<body>

<!-- Adding report help file link -->
<c:set var="helpPage" scope="request" value="format_options.html"/>
<jsp:include page="/jsp/mivheader.jsp" />

<!-- Breadcrumbs Div -->
    <div id="breadcrumbs">
        <a href="<c:url value='/MIVMain'/>" title="Home">Home</a>
        &gt; Manage Format Options
        <c:if test="${not empty documentdescription}" >
        &gt; ${documentdescription}
        </c:if>
    </div><!-- breadcrumbs -->

<!-- MIV Main Div -->
<div id="main"><%--
--%><c:set var="doEntireField" value="false"/><%--
--%><c:forEach var="fields" items="${fieldFormatData.fieldFormatData}"><%--
    --%><c:if test="${empty fields.value['pattern']}"><%--
        --%><c:set var="doEntireField" value="true"/><%--
    --%></c:if><%--
--%></c:forEach><%--
--%>
    <c:if test="${doEntireField == 'true'}">
    <h1>Manage Format Options for ${documentdescription}</h1>
    </c:if><%--
    --%><c:if test="${doEntireField != 'true'}">
    <h1>Error: Manage Format Options for ${documentdescription} Not Found</h1>
    </c:if>

    <!-- <div id="timestamp">&nbsp;</div> -->

    <c:if test="${not empty success && success == false && not empty CompletedMessage}">
        <c:set var='hasErrors' scope='page' value=' class="hasmessages"'/>
        <c:set var='Errors' scope='page' value='${CompletedMessage}'/>
        <c:set var='CompletedMessage' scope='page' value=''/>
    </c:if>

    <!-- Palette look for field by the name of "errormessage" to insert maxlength exceeded message -->
    <div id="errormessage"${hasErrors}>
        ${Errors}
    </div>


<%-- FIXME: Apply the style to CompletedMessage by using a stylesheet, not an inline style="..." declaration.
            Use the test for 'not empty CompletedMessage' to pick a class:
                <div id="CompletedMessage" class="${empty CompletedMessage ? 'empty' : 'full'}">
            Then the CSS can refer to:
                #CompletedMessage {
                    display: block;
                    position: relative;
                    margin: 2px 0;
                    padding: 7px 5px;
                    font-size: 1em;
                }
                #CompletedMessage.empty {
                    display: none;
                }
            Where the javascript is now removing the style attribute, have it remove/add the "empty" class instead.
            Use this same tactic in design.jsp, editform.jsp, PersonalForm.jsp, and recordlist.jsp
--%>

    <c:if test="${(empty success || success == true) && not empty CompletedMessage}">
        <c:set var='hasMessages' scope='page' value=' class="hasmessages"'/>
    </c:if>
    <div id="CompletedMessage"${hasMessages}>
        ${CompletedMessage}
    </div>

    <script type="text/javascript">
        var $CompletedMessage = $('#CompletedMessage');
        $CompletedMessage.removeClass('hasmessages');
        $CompletedMessage.removeProp('style');
    </script>

    <br><%-- FIXME: Do not use <br> tags to add space -- use CSS margins and/or padding --%><%--

--%><c:if test="${doEntireField == 'true'}">

<div id="formatOptions" class="mivDialog">


<form name="formatData" class="mivdata" id="formatData"
      method="post" accept-charset="utf-8"
      action="?dataType=${param.dataType}">

    <input type="hidden" value="${fieldFormatData.selectionFields}" name="formatFieldSelection">
    <input type="hidden" value="${maxpatterns}" name="maxpatterns" id="maxpatterns">
    <input type="hidden" value="${constants.tooltips.patternbold}" name="boldToolTip">
    <input type="hidden" value="${constants.labels.bold}" name="boldLabel">
    <input type="hidden" value="${constants.tooltips.patternitalic}" name="italicToolTip">
    <input type="hidden" value="${constants.labels.italic}" name="italicLabel">
    <input type="hidden" value="${constants.tooltips.patternunderline}" name="underlineToolTip">
    <input type="hidden" value="${constants.labels.underline}" name="underlineLabel">
    <input type="hidden" value="${constants.tooltips.pattern}" name="patternToolTip">
    <input type="hidden" value="${constants.tooltips.fieldselect}" name="fieldSelectToolTip">
    <input type="hidden" value="${constants.tooltips.fielddelete}" name="fieldDeleteToolTip">
    <input type="hidden" value="${constants.labels.fielddelete}" name="fieldDeleteLabel">
    <input type="hidden" value="${deleteKey}" name="deleteKey">

    <c:set var="isInitForm" value="${empty isInitForm || isInitForm}" />
    <div id="userheading"><span class="standout">${user.targetUserInfo.displayName}</span></div>

    <fieldset>
        <legend>Format entire field contents:</legend>

        <!-- <p><a href="javascript:;" onclick="addField('dfCount', 'df');">Add a new Field Format Option</a></p> -->
        <div id="df">
            <c:set var="fieldNum" value="0"/>

            <table class="noborder">
                <thead>
                    <tr>
                        <th>Data Category</th>
                        <th>Field Name</th>
                        <th>Format Options</th>
                    </tr>
                </thead>
                <tbody><%--
            --%><c:forEach var="fields" items="${fieldFormatData.fieldFormatData}"><%--
            --%><c:if test="${empty fields.value['pattern']}"><%--
                --%><c:set var="fieldNum" value="${fieldNum+1}"/>
                    <c:set var="currentfield" value="df${fieldNum}:recID" />
            <%--          <div id="df${fieldNum}" name="df${fieldNum}">  --%>
                        <tr>
                        <!-- Split the field description into category and field name (category:fieldname)-->
                        <c:set var="list" value="${fn:split(fields.value['fieldDescription'], ':')}" />
                        <c:forEach var="value" items="${list}">
                            <td>${value}</td>
                        </c:forEach>
                            <td>
                            <input type="hidden" value="${not empty rec[currentfield]? rec[currentfield] : fields.key}" name="df${fieldNum}:recID" id="df${fieldNum}:recID">
                            <c:set var="currentfield" value="df${fieldNum}:recID" /><%--This was just done above --%>
                            <input type="hidden" value="${fields.value['fieldName']}" id="df${fieldNum}:" name="df${fieldNum}:">

                            <span class="formatlabel">
                                <span class="formatbold" title="${constants.tooltips.fieldbold}">
                                    <c:set var="currentfield" value="df${fieldNum}:bold" />
                                    <input type="checkbox" name="df${fieldNum}:bold"
                                           id="df${fieldNum}:bold"${not empty rec[currentfield] && rec[currentfield] eq 'on' ? ' checked' : ( isInitForm && fields.value['bold'] eq '1' ? ' checked' : '')}>
                                    <label for="df${fieldNum}:bold">${constants.labels.bold}</label>
                                </span>

                                <span class="formatitalic" title="${constants.tooltips.fielditalic}">
                                    <c:set var="currentfield" value="df${fieldNum}:italic" />
                                    <input type="checkbox" name="df${fieldNum}:italic"
                                           id="df${fieldNum}:italic"${not empty rec[currentfield] && rec[currentfield] eq 'on' ? ' checked' : ( isInitForm && fields.value['italic'] eq '1' ? ' checked' : '')}>
                                    <label for="df${fieldNum}:italic">${constants.labels.italic}</label>
                                </span>

                                <span class="formatunderline" title="${constants.tooltips.fieldunderline}">
                                    <c:set var="currentfield" value="df${fieldNum}:underline" />
                                    <input type="checkbox" name="df${fieldNum}:underline"
                                           id="df${fieldNum}:underline"${not empty rec[currentfield] && rec[currentfield] eq 'on' ?'" checked' : ( isInitForm && fields.value['underline'] eq '1' ? ' checked' : '')}>
                                    <label for="df${fieldNum}:underline">${constants.labels.underline}</label>
                                </span>
                            </span><!-- formatlabel -->

                            </td>
                        </tr>
                        </c:if>
                    </c:forEach>
                </tbody>
            </table>
            <input type="hidden" value="${fieldNum}" id="dfCount">
        </div><!-- #df -->
    </fieldset>

    <%--
    --%><c:set var="srno" value="0"/><%--
    --%>

    <fieldset class="format-patterns">
        <legend>Format patterns in fields (a maximum of ${maxpatterns} patterns allowed):</legend>
        <p class="formhelp">
            Select a field for formatting, enter a pattern to look for in that field,
            and choose one or more styles to apply when that pattern is found.
            All three parts, the <em>Field Name</em>, a <em>Pattern</em>, and at least one <em>Format Option</em>, are required.
        </p>

        <c:if test="${empty isHideAddButton || !isHideAddButton}">
        <div class="buttonset formline">
            <%-- <a class="linktobutton mivbutton" href="javascript:void(0);" title="${constants.tooltips.fieldadd}" onclick="addField('pfCount', 'pf', 'pattern','patternCount');">${constants.labels.fieldadd}</a> --%>
            <input type="submit" id="AddPatterns" name="AddPatterns" class="mivbutton"
                   value="${constants.labels.fieldadd}" title="${constants.tooltips.fieldadd}" onclick="javascript:addField('pfCount', 'pf', 'pattern','patternCount');">
        </div>
        </c:if>

        <div class="formatpatterns header">
            <h2 class="srno">&nbsp;</h2>
            <h2 class="patternfield">Data Category: Field Name</h2>
            <h2 class="patternvalue">Pattern</h2>
            <h2 class="patternstyle">Format Options</h2>
        </div>

        <div id="pf"><%--
        --%><c:set var="fieldNum" value="0"/><%--
        --%><c:set var="patternCount" value="0"/><%--
        --%><c:if test="${isInitForm}">
            <c:forEach var="fields" items="${fieldFormatData.fieldFormatData}"><%--
        --%><c:if test="${not empty fields.value['pattern']}"><%--
            --%><c:set var="fieldNum" value="${fieldNum+1}"/><%--
            --%><c:set var="patternCount" value="${patternCount+1}"/>
                <input type="hidden" value="${fields.key}" name="pf${fieldNum}:recID" id="pf${fieldNum}:recID">

                <div id="pf${fieldNum}" class="formatpatterns">
                <c:set var="srno" value="${srno+1}"/>
                <span class="formatline"><%-- FIXME: This is a SPAN, which is an INLINE element. The DIVs below are BLOCK elements, and are not allowed in a SPAN --%>
        <%-- Note! that in formatfield and formatpattern some tags are broken across lines,
                but there is NO space in between tags: <span...><label...>  This is necessary
                to eliminate extra whitespace that leaves too wide a gap when the page is rendered.
                Do NOT break this up into a more readable format such as formatlabel below. Sorry. --%>

                    <div class="srno" id="pf${fieldNum}:srno" name="pf${fieldNum}:srno">
                        <input type="hidden" readonly="readonly" value="${srno}" id="pf${fieldNum}:rowid" name="pf${fieldNum}:rowid">${srno}.
                    </div>

                    <div class="patternfield" title="${constants.tooltips.fieldselect}"><label class="hidden"
                        for="pf${fieldNum}:">Data Category: Field Name</label><select id="pf${fieldNum}:" name="pf${fieldNum}:">
                        <c:set var="selections" value="${fn:split(fieldFormatData.selectionFields,',')}"/><%--
                    --%><c:forEach var="selection" items="${selections}">
                        <option value="${selection}"${ fields.value['fieldDescription'] eq selection ? " selected=\"selected\"" : ""}>${selection}</option>
                        </c:forEach>
                    </select>
                    </div>

                    <div class="patternvalue" title="${constants.tooltips.pattern}"><label class="hidden"
                        for="pf${fieldNum}:pattern">Pattern</label><input type="text" maxlength="255" id="pf${fieldNum}:pattern"
                        name="pf${fieldNum}:pattern" value="${fn:replace(fields.value['pattern'], '"', "&quot;")}">
                    </div>

                    <div class="patternstyle">
                        <div id="pf${fieldNum}:style" name="pf${fieldNum}:style" class="styleblock">
                            <span class="formatbold" title="${constants.tooltips.patternbold}">
                                <input type="checkbox" name="pf${fieldNum}:bold"
                                       id="pf${fieldNum}:bold"${fields.value['bold'] eq '1' ? ' checked' : ''}>
                                <label for="pf${fieldNum}:bold">${constants.labels.bold}</label>
                            </span>

                            <span class="formatitalic" title="${constants.tooltips.patternitalic}">
                                <input type="checkbox" name="pf${fieldNum}:italic"
                                       id="pf${fieldNum}:italic"${fields.value['italic'] eq '1' ? ' checked' : ''}>
                                <label for="pf${fieldNum}:italic">${constants.labels.italic}</label>
                            </span>

                            <span class="formatunderline" title="${constants.tooltips.patternunderline}">
                                <input type="checkbox" name="pf${fieldNum}:underline"
                                       id="pf${fieldNum}:underline"${fields.value['underline'] eq '1' ? ' checked' : ''}>
                                <label for="pf${fieldNum}:underline">${constants.labels.underline}</label>
                            </span>
                        </div>
                        <div class="buttonset">
                            <%-- <a class="linktobutton mivbutton" href="javascript:void(0);" title="${constants.tooltips.fielddelete}" onclick="removeFLD('pf${fieldNum}','pf')">${constants.labels.fielddelete}</a> --%>
                            <input type="submit" class="mivbutton" id="D${fieldNum}" name="D${fieldNum}"
                                   value="${constants.labels.fielddelete}" title="${constants.tooltips.fielddelete}" onclick="removeFLD('pf${fieldNum}','pf')">
                        </div>
                    </div><!-- patternstyle -->
                </span><!-- formatline -->
                </div><!-- formatpatterns -->
            </c:if>
            </c:forEach>
        </c:if>


        <c:if test="${!isInitForm}">
            <c:forEach  begin="1" end="${rec['pfCount']}" varStatus="loopIndex"><%--
            --%><c:set var="fieldNum" value="${fieldNum+1}"/><%--
            --%><c:set var="currentfield" value="~${fieldNum}~" />
            <c:if test="${!fn:contains(deleteKey, currentfield)}">

                <c:set var="patternCount" value="${patternCount+1}"/>
                <c:set var="currentfield" value="pf${fieldNum}:recID" />

                <c:if test="${not empty rec[currentfield]}">
                    <input type="hidden" value="${not empty rec[currentfield] ? rec[currentfield] : ''}" name="pf${fieldNum}:recID" id="pf${fieldNum}:recID">
                </c:if>

                <div id="pf${fieldNum}" class="formatpatterns">
                <c:set var="srno" value="${srno+1}"/>
                <span class="formatline">
            <%-- Note! that in formatfield and formatpattern some tags are broken across lines,
                 but there is NO space in between tags: <span...><label...>  This is necessary
                 to eliminate extra whitespace that leaves too wide a gap when the page is rendered.
                 Do NOT break this up into a more readable format such as formatlabel below. Sorry. --%>

                    <div class="srno" id="pf${fieldNum}:srno" name="pf${fieldNum}:srno">
                        <input type="hidden" readonly="readonly" value="${srno}" id="pf${fieldNum}:rowid" name="pf${fieldNum}:rowid">${srno}.
                    </div>

                    <div class="patternfield" title="${constants.tooltips.fieldselect}">
                        <c:set var="currentfield" value="pf${fieldNum}:" />
                        <label class="hidden" for="pf${fieldNum}:">Data Category: Field Name</label>
                        <select id="pf${fieldNum}:" name="pf${fieldNum}:">
                        <c:set var="selections" value="${fn:split(fieldFormatData.selectionFields, ',')}"/><%--
                    --%><c:forEach var="selection" items="${selections}">
                        <option value="${selection}"${not empty rec[currentfield] && rec[currentfield] eq selection ? ' selected="selected"' : ''}>${selection}</option>
                        </c:forEach>
                    </select>
                    </div>

                    <div class="patternvalue" title="${constants.tooltips.pattern}">
                        <c:set var="currentfield" value="pf${fieldNum}:pattern" />
                        <label class="hidden" for="pf${fieldNum}:pattern">Pattern</label>
                        <input type="text" id="pf${fieldNum}:pattern" name="pf${fieldNum}:pattern" maxlength="255"
                               value="${not empty rec[currentfield] ? (fn:replace(rec[currentfield], '"', '&quot;')) : ''}">
                        <span class="hidden">${currentfield}</span>
                    </div>

                    <div class="patternstyle" >
                        <div id="pf${fieldNum}:style" name="pf${fieldNum}:style" class="styleblock">
                            <span class="formatbold" title="${constants.tooltips.patternbold}">
                                <c:set var="currentfield" value="pf${fieldNum}:bold" />
                                <input type="checkbox" name="pf${fieldNum}:bold"
                                       id="pf${fieldNum}:bold"${not empty rec[currentfield] && rec[currentfield] eq 'on' ? ' checked' : ''}>
                                <label for="pf${fieldNum}:bold">${constants.labels.bold}</label>
                            </span>

                            <span class="formatitalic" title="${constants.tooltips.patternitalic}">
                                <c:set var="currentfield" value="pf${fieldNum}:italic" />
                                <input type="checkbox" name="pf${fieldNum}:italic"
                                       id="pf${fieldNum}:italic"${not empty rec[currentfield] && rec[currentfield] eq 'on' ? ' checked' : ''}>
                                <label for="pf${fieldNum}:italic">${constants.labels.italic}</label>
                            </span>

                            <span class="formatunderline" title="${constants.tooltips.patternunderline}">
                                <c:set var="currentfield" value="pf${fieldNum}:underline" />
                                <input type="checkbox" name="pf${fieldNum}:underline"
                                       id="pf${fieldNum}:underline"${not empty rec[currentfield] && rec[currentfield] eq 'on' ? ' checked' : ''}>
                                <label for="pf${fieldNum}:underline">${constants.labels.underline}</label>
                            </span>
                        </div>

                        <div class="buttonset">
                            <%-- <a class="linktobutton mivbutton" href="javascript:void(0);" title="${constants.tooltips.fielddelete}" onclick="removeFLD('pf${fieldNum}','pf')">${constants.labels.fielddelete}</a> --%>
                            <input type="submit" class="mivbutton" id="D${fieldNum}" name="D${fieldNum}"
                                   value="${constants.labels.fielddelete}" title="${constants.tooltips.fielddelete}" onclick="removeFLD('pf${fieldNum}','pf')">
                        </div>
                    </div><!-- patternstyle -->
                </span><!-- formatline -->
                </div><!-- formatpatterns -->
                </c:if>
            </c:forEach>

        </c:if>


        <!-- If no data was displayed, display at least an initial blank entry -->
        <c:if test="${ (not empty isAddPattern && isAddPattern ) || patternCount == 0}">
        <%--
            --%><c:set var="fieldNum" value="${fieldNum+1}"/><%--
            --%><c:set var="patternCount" value="${patternCount+1}"/>

        <div id="pf${fieldNum}" class="formatpatterns">
            <c:set var="srno" value="${srno+1}"/>
            <span class="formatline">
        <%-- Note! that in formatfield and formatpattern some tags are broken across lines,
                but there is NO space in between tags: <span...><label...>  This is necessary
                to eliminate extra whitespace that leaves too wide a gap when the page is rendered.
                Do NOT break this up into a more readable format such as formatlabel below. Sorry. --%>

            <div class="srno" id="pf${fieldNum}:srno" name="pf${fieldNum}:srno">
                <input type="hidden" readonly="readonly" value="${srno}" id="pf${fieldNum}:rowid" name="pf${fieldNum}:rowid">${srno}.
            </div>

            <div class="patternfield" title="${constants.tooltips.fieldselect}"><label class="hidden"
                for="pf${fieldNum}:">Data Category: Field Name</label><select id="pf${fieldNum}:" name="pf${fieldNum}:">
                    <c:set var="selections" value="${fn:split(fieldFormatData.selectionFields, ',')}"/><%--
                --%><c:forEach var="selection" items="${selections}">
                        <option value="${selection}"${fields.value['fieldDescription'] eq selection ? ' selected="selected"' : ''}>${selection}</option>
                    </c:forEach>
                </select>
            </div>

            <div class="patternvalue" title="${constants.tooltips.pattern}">
                <label class="hidden" for="pf${fieldNum}:pattern">Pattern</label>
                <input type="text" maxlength="255" id="pf${fieldNum}:pattern"
                       name="pf${fieldNum}:pattern" value="">
            </div>

            <div class="patternstyle">
                <div id="pf${fieldNum}:style" name="pf${fieldNum}:style" class="styleblock">
                    <span class="formatbold" title="${constants.tooltips.patternbold}">
                        <input type="checkbox" id="pf${fieldNum}:bold" name="pf${fieldNum}:bold">
                        <label for="pf${fieldNum}:bold">${constants.labels.bold}</label>
                    </span>

                    <span class="formatitalic" title="${constants.tooltips.patternitalic}">
                        <input type="checkbox" id="pf${fieldNum}:italic" name="pf${fieldNum}:italic">
                        <label for="pf${fieldNum}:italic">${constants.labels.italic}</label>
                    </span>

                    <span class="formatunderline" title="${constants.tooltips.patternunderline}">
                        <input type="checkbox" id="pf${fieldNum}:underline" name="pf${fieldNum}:underline">
                        <label for="pf${fieldNum}:underline">${constants.labels.underline}</label>
                    </span>
                </div>
                <div class="buttonset">
                    <%-- <a class="linktobutton mivbutton" href="javascript:void(0);" title="${constants.tooltips.fielddelete}" onclick="removeFLD('pf${fieldNum}', 'pf')">${constants.labels.fielddelete}</a></span> --%>
                    <%-- FIXME: Don't use inline "onclick" handlers. Use an external .js file and attach handlers as needed. --%>
                    <input type="submit" class="mivbutton" id="D${fieldNum}" name="D${fieldNum}"
                           value="${constants.labels.fielddelete}" title="${constants.tooltips.fielddelete}" onclick="removeFLD('pf${fieldNum}','pf')">
                </div><!-- buttonset -->
            </div><!-- patternstyle -->
        </div><!-- formatpatterns -->
        </c:if>

        <input type="hidden" value="${fieldNum}" id="pfCount" name="pfCount">
        <input type="hidden" value="${patternCount}" id="patternCount" name="patternCount">

        </div><!-- id="pf" -->

        <c:if test="${empty isHideAddButton || !isHideAddButton}">
        <div class="buttonset formline">
            <%-- <a class="linktobutton mivbutton" href="javascript:void(0);" title="${constants.tooltips.fieldadd}" onclick="addField('pfCount', 'pf', 'pattern','patternCount');">${constants.labels.fieldadd}</a> --%>
            <input type="submit" id="AddPatternsDown" name="AddPatternsDown" class="mivbutton"
                   value="${constants.labels.fieldadd}" title="${constants.tooltips.fieldadd}" onclick="javascript:addField('pfCount', 'pf', 'pattern','patternCount');">
        </div>
        </c:if>

    </fieldset>

    <div class="buttonset">
        <input type="submit" id="save" name="save" value="Save" title="Save">
        <input type="submit" id="reset" name ="reset" value="Reset" title="Reset">
    </div><!-- buttonset -->

</form>
</div> <!-- formatOptions -->

</c:if>
</div><!-- main -->

<jsp:include page="/jsp/mivfooter.jsp" />

<script type="text/javascript">
/* <![CDATA[ */
$(document).ready(function() {
    $(".mivbutton").each(function(index) {
        if ($(this).attr('type') != 'button')
        {
            // type property can’t be changed using jquery
            //document.getElementById($(this).attr('id')).setAttribute('type', 'button'); // not working in IE
            changeInputType(document.getElementById($(this).attr('id')), "button");
        }
    });
});
/* ]]> */
</script>


</body>
</html><%--
 vi: se ts=8 sw=4 sr et:
--%>
