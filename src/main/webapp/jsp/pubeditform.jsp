<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%--
--%><c:set var="user" scope="request" value="${MIVSESSION.user}"/><%--
--%><c:if test="${constants.config.usepopups}">
<head>
<meta http-equiv="cache-control" content="no-cache, must-revalidate">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="expires" content="0">
</head>

<!-- Using Pop-Up edit dialogs -->
<script type="text/javascript">
if (typeof _container_ != 'undefined') {
    var btn = document.getElementById("popupformcancel");
    _container_.setCloseControl(btn);
}
</script>
</c:if><%-- usepopups --%>
<script type="text/javascript"><!--
    mivFormProfile = {
        required: [ ${constants.config.required} ],
        trim: [ "title", "citation", "journal", "author", "editor",
                "publisher", "city", "volume", "issue", "pages",
                "link", "contribution", "significance" ],
        constraints: {
            year: function(contents) {
                var curYear = new Date().getFullYear();

                if (isNaN(contents)) {
                    return "INVALID_NUMBER";
                }

                var numyear = contents * 1;

                if ( numyear < 1900 || numyear > (curYear +1) ) {
                    return "INVALID_YEAR";
                }

                return true;
            },
            citation: function(contents) {
                var valid = contents.length < 1;
                if (!valid) {
                    alert("The record cannot be saved until all data\nis transferred out of the scratchpad area");
                }
                return valid;
            },
            link: miv.validation.isLink
        },
        errormap: {"INVALID_YEAR":"Year must be from 1900 to ${thisYear+1}" }
    };

   //set custom error message for form input
    $(document).ready(function() {
        var link = document.getElementById("link");

        link.onchange = function(e) {
            e.target.setCustomValidity("");
        };

        link.oninvalid = function(e) {
            if (!e.target.validity.valid) {
                e.target.setCustomValidity("Must be a valid web address");
            }
        };
    });
// -->
</script>
<div id="userheading">
    <span class="standout">${user.targetUserInfo.displayName}</span>
</div>

<%-- <!-- record: ${rec} --> --%>


<!-- This is the data entry form for "${recordTypeName}" records -->
<form class="mivdata" id="mytestform"
      method="post" action="<c:url value='${constants.config.saveservlet}'/>">

<input type="hidden" id="class" name="recordClass" value="${recordClass}">
<input type="hidden" id="rectype" name="rectype" value="${recordTypeName}">
<input type="hidden" id="recid" name="recid" value="${rec.id}">
<input type="hidden" id="display" name="display" value="${not empty rec.display?rec.display:1}">

<div class="formline">
<span class="textfield" title="${constants.tooltips.year}">
  <label for="year" class="f_required">${constants.labels.year}</label>
  <input type="text" id="year" name="year" size="5" maxlength="4"  
         value="${rec.year}" title="Year must be from 1900 to ${thisYear+1}">
</span>

<span class="dropdown" title="${constants.tooltips.statusid}">
<label for="statusid" class="f_required">${constants.labels.statusid}</label>
<select id="statusid" name="statusid">
  <c:forEach var="opt" items="${constants.options.publicationstatus}"><%--
--%>  <option value="${opt.id}"${rec.statusid==opt.id?' selected="selected"':''}>${opt.name}</option>
  </c:forEach><%--
--%></select>
</span>
</div><!--formline-->
<div class="formline">
<span class="dropdown" title="${constants.tooltips.typeid}">
  <label for="typeid" class="f_required">${constants.labels.typeid}</label>
  <select id="typeid" name="typeid">
  <c:forEach var="opt" items="${constants.options.publicationtype}"><%--
--%>  <option value="${opt.id}"${rec.typeid==opt.id ?' selected="selected"':''}>${opt.name}</option>
  </c:forEach><%--
--%></select>
</span>

</div><!--formline-->

<%--Add annotation field for school of engineering only--%>
<c:if test="${MIVSESSION.user.targetUserInfo.schoolID == 34}">
<div class="formline">
<span class="textfield" title="${constants.tooltips.engineeringannotation}">
  <label for="engineeringannotation">${constants.labels.engineeringannotation}</label>
  <input type="text"
         id="engineeringannotation" name="engineeringannotation"
         size="4" maxlength="4"
         value="${rec.engineeringannotation}"
         ><br/>
  <span>${constants.labels.engineeringannotationadditional}</span><br/>
</span>
</div><!--formline-->
</c:if>

<%--
<div class="formline">
<span class="rb" title="Select if you are the author of this work">
 <input type="radio" name="authorship" id="isauthor" value="isauthor">
 <label for="isauthor">Author</label>
</span>
&nbsp;
<span class="rb" title="Select if you are a co-author of this work">
 <input type="radio" name="authorship" id="iscoauthor" value="iscoauthor">
 <label for="iscoauthor">Co-author</label>
</span>
</div><!--formline-->
 --%>

<div class="formline">
<span class="textfield" title="${constants.tooltips.citation}">
  <label for="citation">${constants.labels.citation}</label><br />
  <textarea
            id="citation" name="citation"
            rows="3" cols="54" wrap="soft"
            >${rec.citation}</textarea>
</span>
</div><!--formline-->

<div class="formline">
<span class="textfield" title="${constants.tooltips.title}">
  <label for="title" class="f_required">${constants.labels.title}</label> <span>(Do not add a period to the end of your Title data)</span>
  <br/><textarea id="title" name="title" rows="2" cols="54" wrap="soft">${rec.title}</textarea>
</span>
</div><!--formline-->

<div class="formline">
<span class="textfield" title="${constants.tooltips.journal}">
  <label for="journal">${constants.labels.journal}</label><br />
  <input type="text" 
         id="journal" name="journal"
         size="57" maxlength="255"
         value="${rec.journal}"
         >
</span>
</div><!--formline-->

<!-- Authors field not required in Books Edited -->
<c:if test="${recordTypeName != 'book-editor'}">
    <c:set var='requiredClass' scope='page' value=' class="f_required"'/>
</c:if>

<div class="formline">
<span class="textfield" title="${constants.tooltips.author}">
  <label for="author" ${requiredClass}>${constants.labels.author}</label><br />
  <textarea id="author" name="author" rows="2" cols="54" wrap="soft">${rec.author}</textarea>
</span>
</div><!--formline-->

<!-- Editors, Publisher, City -->
<div id="publisherinfo">
<div class="formline">
<span class="textfield" title="${constants.tooltips.editor}">
  <label for="editor">${constants.labels.editor}</label>
  <input type="text"
         id="editor" name="editor"
         size="50" maxlength="255"
         value="${rec.editor}"
         >
</span>
</div><!--formline-->

<div class="formline">
<span class="textfield" title="${constants.tooltips.publisher}">
  <label for="publisher">${constants.labels.publisher}</label>
  <input type="text"
         id="publisher" name="publisher"
         size="48" maxlength="255"
         value="${rec.publisher}"
         >
</span>
</div><!--formline-->

<div class="formline">
<span class="textfield" title="${constants.tooltips.city}">
  <label for="city">${constants.labels.city}</label>
  <input type="text"
         id="city" name="city"
         size="53" maxlength="50"
         value="${rec.city}"
         >
</span>
</div><!--formline-->
</div><!-- publisherinfo -->

<!-- Volume, Issue, Pages -->
<div class="formline">
<span class="textfield" title="${constants.tooltips.volume}">
  <label for="volume">${constants.labels.volume}</label>
  <input type="text"
         id="volume" name="volume"
         size="5" maxlength="20"
         value="${rec.volume}"
         >
</span>

<span class="textfield" title="${constants.tooltips.issue}">
  <label for="issue">${constants.labels.issue}</label>
  <input type="text"
         id="issue" name="issue"
         size="5" maxlength="20"
         value="${rec.issue}"
         >
</span>

<span class="textfield" title="${constants.tooltips.pages}">
  <label for="pages">${constants.labels.pages}</label>
  <input type="text"
         id="pages" name="pages"
         size="10" maxlength="20"
         value="${rec.pages}"
         >
</span>
</div><!--formline-->

<div class="formline">
<span class="textfield" title="${constants.tooltips.link}">
  <label for="link">${constants.labels.link}</label>
  <input type="url" pattern="(https?|ftp):\/\/.+"
         placeholder="http://example.com/some+page/some_document.pdf"
         id="link" name="link"
         size="52" maxlength="255"
         value="${rec.link}"
         >
</span>
</div><!--formline-->

<div class="formline">
<span class="textfield" title="${constants.tooltips.contribution}">&#8224;&nbsp;
  <label for="contribution">${constants.labels.contribution}</label><br />
  <textarea
            id="contribution" name="contribution"
            rows="3" cols="54" wrap="soft"
            >${rec.contribution}</textarea>
</span>
</div><!--formline-->

<div class="formline">
<span class="textfield" title="${constants.tooltips.significance}">&#8224;&nbsp;
  <label for="significance">${constants.labels.significance}</label><br />
  <textarea
            id="significance" name="significance"
            rows="3" cols="54" wrap="soft"
            >${rec.significance}</textarea>
</span>
</div><!--formline-->

<%--
<div id="displaycb" class="cb">
  <div class="formline">
  <span class="checkbox" title="${constants.tooltips.display}">
    <input type="checkbox" id="display" name="display"${rec.display=='1'||rec.display=='true'?" checked":""}>
    <label for="display">${constants.labels.display}</label>
  </span>
  </div><!-- formline -->
</div><!-- displaycb -->
--%>
<div class="buttonset">
  <input type="submit" id="save" name="save" value="Save" title="Save">
  <!-- button id="popupformcancel" name="popupformcancel" value="Cancel">Cancel</button -->
  <input type="submit" id="popupformcancel" name="popupformcancel" value="Cancel" title="Cancel">
</div>

</form>

