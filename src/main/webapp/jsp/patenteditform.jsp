<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%--
--%><!DOCTYPE html><%--
--%><c:set var="user" scope="request" value="${MIVSESSION.user}"/><%--
--%><c:if test="${constants.config.usepopups}">
<head>
<meta http-equiv="cache-control" content="no-cache, must-revalidate">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="expires" content="0">
</head>
<!-- Using Pop-Up edit dialogs -->
<script type="text/javascript">
if (typeof _container_ != 'undefined') {
    var btn = document.getElementById("popupformcancel");
    _container_.setCloseControl(btn);
}
</script>
</c:if><%-- usepopups --%>

<link rel="stylesheet" type="text/css" href="<c:url value='/css/patent.css'/>">
<script type="text/javascript" src="<c:url value='/js/jquery-ui.plugins.js'/>"></script>
<script type="text/javascript" src="<c:url value='/js/patentedit.js'/>" defer="defer"></script>

<script type="text/javascript">
//<![CDATA[
    mivFormProfile = {
        required: [ ${constants.config.required} ],
        trim: [ "patenttitle", "patentauthor", "disclosuretitle", "disclosureauthor","link"],
        constraints: {
            link: miv.validation.isLink
        }
    };
//]]>
</script>

<div id="userheading">
    <span class="standout">${user.targetUserInfo.displayName}</span>
</div>
${constants.config.required}

<!-- This is the data entry form for "${recordTypeName}" records -->
<form class="mivdata" id="mytestform" name="mytestform"
      method="post" action="<c:url value='${constants.config.saveservlet}'/>">

    <input type="hidden" id="rectype" name="rectype" value="${recordTypeName}">
    <input type="hidden" id="recid" name="recid" value="${rec.id}">
    <input type="hidden" id="display" name="display" value="${not empty rec.display?rec.display:1}">

    <div class="formline">
        <span class="dropdown" title="type Disclosure or Patent">
            <%--span class="required">*</span--%>
            <label for="typeid">${constants.labels.typeid}</label>
            <select id="typeid" name="typeid"><%--
            --%><c:forEach var="opt" items="${constants.options.patenttypes}">
                 <option value="${opt.id}"${ ((rec.typeid==null && opt.id==1)? ' selected="selected"': ((rec.typeid==opt.id)?' selected="selected"':''))} title="${opt.tip}">${opt.value}</option><%--
            --%></c:forEach><%--
        --%></select>
        </span>
    </div><!-- formline -->

<!-- DISCLOSURE SECTION -->
    <fieldset id="disclosure" name="disclosure" form="mytestform">
        <legend>Disclosure</legend>
        <div class="requiredfieldlegend">* = Required Field when patent type is <strong>Disclosure</strong></div>

        <!-- disclosure title -->
        <div class="formline">
            <span class="textfield" title="${constants.tooltips.disclosuretitle}">
                <%--span class="required">*</span--%>
                <label for="disclosuretitle" class="f_required">${constants.labels.disclosuretitle}</label><br>
                <textarea class="f_required"
                          id="disclosuretitle" name="disclosuretitle"
                          rows="2" cols="54" wrap="soft"
                          >${rec.disclosuretitle}</textarea>
            </span>
        </div><!-- formline -->

        <!-- disclosure authors -->
        <div class="formline">
            <span class="textfield" title="${constants.tooltips.disclosureauthor}">
                <%--span class="required">*</span--%>
                <label for="disclosureauthor" class="f_required">${constants.labels.disclosureauthor}</label><br>
                <textarea class="f_required"
                          id="disclosureauthor" name="disclosureauthor"
                          rows="2" cols="54" wrap="soft"
                          >${rec.disclosureauthor}</textarea>
            </span>
        </div><!-- formline -->

        <!-- disclosure number and date -->
        <div class="formline">
            <span class="textfield" title="${constants.tooltips.disclosureid}">
                <%--span class="required">*</span--%>
                <label for="disclosureid" class="f_required">${constants.labels.disclosureid}</label>
                <input type="text"
                       id="disclosureid" name="disclosureid"
                       size="16" maxlength="20"
                       value="${rec.disclosureid}"
                       >
            </span>

            <span class="textfield" title="${constants.tooltips.disclosuredate}">
                <%--span class="required">*</span--%>
                <label for="disclosureid" class="f_required">${constants.labels.disclosuredate}</label>
                <input type="text" class="datepicker"
                       id="disclosuredate" name="disclosuredate"
                       size="11"
                       value="${rec.disclosuredate}"
                       >
            </span>
        </div><!-- formline -->
    </fieldset><!-- disclosure -->

<!-- PATENT SECTION -->
    <fieldset id="patent" name="patent" form="mytestform">
        <legend>Patent</legend>
        <div class="requiredfieldlegend">* = Required Field when patent type is <strong>Patent</strong></div>

        <div class="formline">
            <span class="dropdown" title="${constants.tooltips.statusid}">  <%--NOTE: js handler for this select will need to update "required" fields --%>
                <%--span class="required">*</span--%>
                <label for="patentstatusid">${constants.labels.statusid}</label>
                <select id="patentstatusid" name="patentstatusid"><%--
                --%><c:forEach var="opt" items="${constants.options.patentstatus}">
                    <option value="${opt.id}"${rec.patentstatusid==opt.id?' selected="selected"':''} title="${opt.description}">${opt.status}</option><%--
                --%></c:forEach>
                </select>
            </span>
            <span>Select whether this is a pending patent application or a granted patent.</span>
        </div><!-- formline -->

        <!-- patent title -->
        <div class="formline">
            <span class="textfield" title="${constants.tooltips.patenttitle}">
                <%--span class="required">*</span--%>
                <label for="patenttitle" class="f_required">${constants.labels.patenttitle}</label><br>
                <textarea class="f_required"
                          id="patenttitle" name="patenttitle"
                          rows="2" cols="54" wrap="soft"
                          >${rec.patenttitle}</textarea>
            </span>
        </div><!-- formline -->

        <!-- authors -->
        <div class="formline">
            <span class="textfield" title="${constants.tooltips.patentauthor}">
                <%--span class="required">*</span--%>
                <label for="patentauthor" class="f_required">${constants.labels.patentauthor}</label><br>
                <textarea class="f_required"
                          id="patentauthor" name="patentauthor"
                          rows="2" cols="54" wrap="soft"
                          >${rec.patentauthor}</textarea>
            </span>
        </div><!-- formline -->

        <!-- Jurisdiction / Country / Patent Office (e.g. U.S. = USPTO) -->
        <div class="formline">
            <span class="dropdown" title="${constants.tooltips.jurisdiction}">
                <label for="jurisdiction">${constants.labels.jurisdiction}</label>
                <select id="jurisdiction" name="jurisdiction"><%--
                --%><c:forEach var="opt" items="${constants.options.jurisdiction}">
                    <option value="${opt.id}"${ ((rec.jurisdiction==null && opt.id==1)? ' selected="selected"': ((rec.jurisdiction==opt.id)?' selected="selected"':''))} title="${opt.tooltip}">${opt.value}</option><%--
                --%></c:forEach>
                </select>
            </span>
            <a href="<c:url value='/help/patentjurisdiction.html'/>" target="mivhelp">Help! My Jurisdiction is not listed</a>
        </div><!-- formline -->

        <!-- application number and date-->
        <div class="formline">
            <span class="textfield" title="${constants.tooltips.applicationid}">
                <%--span class="required">*</span--%>
                <label for="applicationid" class="f_required">${constants.labels.applicationid}</label>
                <input type="text"
                       id="applicationid" name="applicationid"
                       size="16" maxlength="20"
                       value="${rec.applicationid}"
                       >
            </span>

            <span class="textfield" title="${constants.tooltips.applicationdate}">
                <%--span class="required">*</span--%>
                <label for="applicationdate" class="f_required">${constants.labels.applicationdate}</label>
                <input type="text" class="datepicker"
                       id="applicationdate" name="applicationdate"
                       size="11"
                       value="${rec.applicationdate}"
                       >
            </span>
        </div><!-- formline -->

        <!-- patent number and date -->
        <div class="formline">
            <span class="textfield" title="${constants.tooltips.patentid}">
                <%--span class="required">*</span--%>
                <label for="patentid" class="f_required">${constants.labels.patentid}</label>
                <input type="text"
                       id="patentid" name="patentid"
                       size="16" maxlength="20"
                       value="${rec.patentid}"
                       >
            </span>

            <span class="textfield" title="${constants.tooltips.patentdate}">
                <%--span class="required">*</span--%>
                <label for="patentdate" class="f_required">${constants.labels.patentdate}</label>
                <input type="text" class="datepicker"
                       id="patentdate" name="patentdate"
                       size="11"
                       value="${rec.patentdate}"
                       >
            </span>
        </div><!-- formline -->

        <!-- URL / Link -->
        <div class="formline">
            <span class="textfield" title="${constants.tooltips.link}">
            <label for="link">${constants.labels.link}</label>
            <input type="url" pattern="(https?|ftp):\/\/.+"
                   placeholder="http://patft1.uspto.gov/netacgi/nph-Parser?patentnumber=1234567"
                   id="link" name="link"
                   size="52" maxlength="255"
                   value="${rec.link}"
                   >
            </span>
        </div><!-- formline -->
    </fieldset><!-- patent -->

    <fieldset id="aux" name="aux" form="mytestform">
        <legend title="Contributions, Significance, and Licensing information">Contributions, etc.</legend>

        <div class="formline">
            <span class="textfield" title="${constants.tooltips.contribution}">&#8224;&nbsp;
                <label for="contribution">${constants.labels.contribution}</label><br />
                <textarea
                    id="contribution" name="contribution"
                    rows="3" cols="54" wrap="soft"
                >${rec.contribution}</textarea>
            </span>
        </div><!--formline-->

        <div class="formline">
            <span class="textfield" title="${constants.tooltips.significance}">&#8224;&nbsp;
                <label for="significance">${constants.labels.significance}</label><br />
                <textarea
                    id="significance" name="significance"
                    rows="3" cols="54" wrap="soft"
                >${rec.significance}</textarea>
            </span>
        </div><!--formline-->

        <div class="formline">
            <span class="textfield" title="${constants.tooltips.licensing}">&#8224;&nbsp;
                <label for="licensing">${constants.labels.licensing}</label><br />
                <textarea
                    id="licensing" name="licensing"
                    rows="3" cols="54" wrap="soft"
                >${rec.licensing}</textarea>
            </span>
        </div><!--formline-->

    </fieldset>


    <div class="buttonset">
        <input type="submit" id="save" name="save" value="Save" title="Save">
        <!-- button id="popupformcancel" name="popupformcancel" value="Cancel">Cancel</button -->
        <input type="submit" id="popupformcancel" name="popupformcancel" value="Cancel" title="Cancel">
    </div>

</form>
