<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%><%--
--%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%--
--%><%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%><%--
--%><c:set var="user" scope="request" value="${MIVSESSION.user}"/><%--
--%><!DOCTYPE html>
<html>
    <head>
        <title>MIV &ndash; ${strings.pageTitle}</title>

        <%@ include file="/jsp/metatags.html" %>
    </head>

    <body>
        <jsp:include page="/jsp/mivheader.jsp" />

        <jsp:include page="breadcrumbs.jsp">
            <jsp:param value="${strings.pageTitle}" name="pageTitle"/>
        </jsp:include>

        <div id="main">
            <h1>${strings.pageTitle}</h1>

            <p>${strings.successMessage}&nbsp;&nbsp;<fmt:formatDate pattern="MM/dd/yy, h:mm a" value="${form.emailSent}"/></p>

            <p><strong>${strings.toLabel}:</strong> ${form.emailTo}</p>

            <c:if test="${!empty form.emailCc}">
                <p><strong>${strings.ccLabel}:</strong> ${form.emailCc}</p>
            </c:if>
        </div>

        <jsp:include page="/jsp/miv_small_footer.jsp" />
    </body>
</html>