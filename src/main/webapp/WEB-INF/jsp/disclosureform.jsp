<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%><%--
--%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%--
--%><%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%><%--
--%><%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%><%--
--%><%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%><%--
--%><%@ taglib prefix="jwr" uri="http://jawr.net/tags" %><%--
--%><c:set var="pageTitle" value="${form.dc.documentId > 0 ? strings.pageTitleEdit : strings.pageTitleAdd}"/><%--
--%><!DOCTYPE html>
<html>
    <head>
        <title>MIV &ndash; ${pageTitle}</title>

        <%@ include file="/jsp/metatags.html"%>

        <link rel="stylesheet" type="text/css" href="<c:url value='/css/mivcommon.css'/>">
        <link rel="stylesheet" type="text/css" href="<c:url value='/css/mivEnterData.css'/>">
        <link rel="stylesheet" type="text/css" href="<c:url value='/css/miv-forms.css'/>">

        <script type="text/javascript" src="<c:url value='/js/mivcommon.js'/>"></script>
        <jwr:script src="/bundles/mivvalidation.js" />
        <script type="text/javascript" src="<c:url value='/js/disclosureform.js'/>"></script>
    </head>

    <body>
        <jsp:include page="/jsp/mivheader.jsp" />

        <jsp:include page="breadcrumbs.jsp">
            <jsp:param value="${pageTitle}" name="pageTitle"/>
        </jsp:include>

        <div id="main">
            <h1>${pageTitle}</h1>

            <c:set var='additionalInformation' scope='page' value=' autofocus'/>

            <c:if test="${form.revision}">
                <c:set var='additionalInformation' scope='page' value=''/>
                <c:set var='revisedAdditionalInformation' scope='page' value=' autofocus'/>
            </c:if>

            <jsp:include page="binderrors.jsp" />

            <form:form id="dc" cssClass="mivdata" method="post" commandName="form">
                <div id="enterdataguide">
                    <div id="requiredfieldlegend">* = Required Field</div>
                </div>

                <div id="maindialog" class="pagedialog">
                    <div class="mivDialog wideDialog">
                        <h1>${form.dc.candidateName}</h1>

                        <div id="summary" class="summary">
                            <div class="formline">
                                <jsp:useBean id="now" class="java.util.Date" scope="page" />
                                <strong>Date:</strong>&nbsp;<fmt:formatDate type="date" dateStyle="long" value="${now}"/>
                            </div>
                            <div class="formline">
                                <strong>School/College - Department:</strong>&nbsp;${form.dc.scope.fullDescription}
                            </div>
                            <div class="formline">
                                <strong>Action:</strong>&nbsp;${form.dc.dossier.action.description}
                            </div>
                        </div><!-- #summary -->

                        <c:choose>
                            <c:when test="${!empty form.dc.ranks}">
                                <div class="formline">
                                    <table class="display rank_step"><%-- what is class "display"? does it compare to "not-display"? --%>
                                    <thead>
                                        <tr>
                                            <th align="left" width="3%">&nbsp;</th>
                                            <th align="left" width="35%" class="headercolumn"><strong>Present Rank, Title &amp; Step </strong></th>
                                            <th align="left" width="10%" class="headercolumn"><strong>% of Time</strong></th>

                                            <th align="left" width="2%">&nbsp;</th>
                                            <th align="left" width="35%" class="headercolumn"><strong>Proposed Rank, Title &amp; Step </strong></th>
                                            <th align="left" width="10%" class="headercolumn"><strong>% of Time</strong></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <c:forEach var="rankAndStep" items="${form.dc.ranks}" varStatus="rankRow">
                                        <tr>
                                            <td class="sequence">${rankRow.count}.</td>
                                            <td>${rankAndStep.presentRankAndStep}</td>
                                            <td>${rankAndStep.presentPercentOfTime}${empty rankAndStep.presentPercentOfTime ? '' : '%'}</td>
                                            <td>&nbsp;</td>
                                            <td>${rankAndStep.proposedRankAndStep}</td>
                                            <td>${rankAndStep.proposedPercentOfTime}${empty rankAndStep.proposedPercentOfTime ? '' : '%'}</td>
                                        </tr>
                                        </c:forEach>
                                    </tbody>
                                    </table>
                                </div>

                                <div class="certificate">
                                    <p>
                                    This is to certify that I have reviewed all of the materials
                                    being submitted for my personnel action effective
                                    <strong><fmt:formatDate type="date" dateStyle="long" value="${form.dc.dossier.action.effectiveDate}"/></strong>.
                                    </p>
                                </div>

                                <spring:bind path="form.dc.additionalInfo"><%--
                            --%><label for="${status.expression}"><strong>Additional information (optional)&nbsp;:</strong></label>
                                <div class="formline">
                                    <textarea id="${status.expression}" class="additionalinfo" name="${status.expression}"
                                              rows="2" cols="100" wrap="soft" ${additionalInformation}>${status.value}</textarea>
                                </div><%--
                            --%></spring:bind>

                                <c:if test="${form.revision}">
                                    <div class="formline">
                                        <span class="ui-state-default ui-corner-all ui-icon ui-icon-check vertical-align-top inline-block">✓</span>

                                        <span>
                                            <span class="vertical-align-top"><strong>Revised:</strong></span>

                                            <span class="attention vertical-align-top inline-block">
                                                <span>This Disclosure Certificate supersedes the signed certificate dated below.</span>
                                                <br>
                                                <span class="italic">If this document is revised, the candidate will need to sign again.</span>
                                            </span>
                                        </span>
                                        <%--/spring:bind--%>
                                    </div><!-- formline -->

                                    <div class="formline">
                                        <spring:bind path="form.dc.changesAdditions">
                                        <label class="f_required" for="${status.expression}"><strong>The following changes/additions have been made:</strong></label>
                                        <textarea id="${status.expression}" class="additionalinfo" name="${status.expression}"
                                                  rows="2" cols="100" wrap="soft" ${revisedAdditionalInformation}>${status.value}</textarea>
                                        </spring:bind>
                                    </div>
                                </c:if><%-- form.revision --%>

                                <div class="formline">
                                    <strong>Date signed:</strong>

                                    <span class="${not empty form.dateSigned ? 'helpExample' : 'notsigned'}">
                                    <c:choose>
                                        <c:when test="${not empty form.dateSigned}">
                                            <fmt:formatDate type="both" dateStyle="long" timeStyle="short" value="${form.dateSigned}"/>
                                        </c:when>
                                        <c:otherwise>Not Signed</c:otherwise>
                                    </c:choose>
                                    </span>

                                    <div class="italic">Must be dated same date as the Department Letter or after.</div>
                                </div><!-- formline -->

                                <div class="formline certificate">
                                    <p>
                                    I certify that I have reviewed my dossier and
                                    I have reviewed the department's recommendation
                                    (and redacted evaluations, if applicable).
                                    </p>
                                </div>

                                <div class="buttonset">
                                    <input type="submit" name="_eventId_save" id="save" value="Save" title="Save">
                                    <input type="submit" name="_eventId_mail" id="mail" value="Email Candidate for Signature" title="Email Candidate for Signature">
                                    <input type="submit" name="_eventId_breadcrumb1" id="cancel" value="Cancel" title="Cancel">
                                </div>

                                <p><em>${strings.submissionNote}</em></p>
                            </c:when><%-- rank and steps not empty --%>

                            <c:otherwise>
                                <div class="attention">
                                    <p>An Action Form has not been added for this candidate yet.</p>
                                </div>

                                <div class="buttonset">
                                    <input type="submit" name="_eventId_breadcrumb1" id="cancel" value="Cancel" title="Cancel" autofocus>
                                </div>
                            </c:otherwise>
                        </c:choose>
                    </div><!-- mivDialog -->
                </div><!-- mainDialog -->
            </form:form>
        </div><!-- main -->

        <jsp:include page="/jsp/miv_small_footer.jsp" />
    </body>
</html><%--
 vi: se ts=8 sw=4 et:
--%>
