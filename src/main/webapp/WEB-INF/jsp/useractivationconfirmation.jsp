<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%><%--
--%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%--
--%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %><%--
--%><!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
    <head>
        <title>MIV &ndash; ${strings.pageTitle}</title>

        <script type="text/javascript">
        /* <![CDATA][ */
            djConfig = {
                isDebug: false,
                parseOnLoad: true // parseOnLoad *must* be true to parse the widgets
            };
            var mivConfig = new Object();
            mivConfig.isIE = false;
        /* ]]> */
        </script>

        <%@ include file="/jsp/metatags.html" %>

        <link rel="stylesheet" type="text/css" href="<c:url value='/css/mivcommon.css'/>">
        <!--[if lt IE 8]>
        <link rel="stylesheet" type="text/css" href="<c:url value='/css/iehacks.css'/>" />
        <script type="text/javascript">mivConfig.isIE=true;</script>
        <![endif]-->
        <c:set var="user" scope="request" value="${MIVSESSION.user}"/>
    </head>
    <body>
        <jsp:include page="/jsp/mivheader.jsp" />
        
        <jsp:include page="breadcrumbs.jsp">
            <jsp:param value="${strings.pageTitle}" name="pageTitle"/>
        </jsp:include>
        
        <!-- MIV Main Div -->
        <div id="main">
            <h1>${strings.pageTitle}</h1>

            <c:choose><%--
            --%><c:when test="${fn:length(searchCriteria.changeList) > 0}">
                    <p>The following user's account status has been updated.</p>
                    <ul class="itemlist">
                    	<c:forEach var="user" items="${searchCriteria.changeList}" varStatus="personRow">
	                    	<li>
	                    		<strong>${user.surname}, ${user.givenName}:</strong>
	                    		<c:choose>
                                <c:when test="${user.active}">Reactivated User</c:when>
                                <c:otherwise>Deactivated User</c:otherwise>
                                </c:choose>
	                    	</li>
                    	</c:forEach>
                    </ul>
                </c:when><%--
            --%><c:otherwise>
                    <p>No users have been updated.</p></c:otherwise><%--
        --%></c:choose>

            <c:if test="${fn:length(searchCriteria.failureList) > 0}">
                <p>The following user's account status has not been updated.</p>
                <ul class="itemlist">
                   	<c:forEach var="user" items="${searchCriteria.failureList}" varStatus="personRow">
                    	<li>
                    		<strong>${user.surname}, ${user.givenName}:</strong>
                        	Cannot reactivate: <span class="warning">User does not appear in the Campus Identity Management Service</span>
                    	</li>
                   	</c:forEach>
                </ul>
            </c:if>
        </div><!-- main -->
        <jsp:include page="/jsp/miv_small_footer.jsp" />
    </body>
</html>
