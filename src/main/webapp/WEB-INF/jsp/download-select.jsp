<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%><%--
--%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%--
--%><%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%><%--
--%><%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %><%--
--%><c:set var="user" scope="request" value="${MIVSESSION.user}"/><%--
--%><!DOCTYPE HTML>
<html>
    <head>
        <title>MIV &ndash; ${strings.title}</title>
        <script type="text/javascript">
	/* <![CDATA[ */
	    var mivConfig = new Object();
	    mivConfig.isIE = false;
	/* ]]> */
    </script>
        
        <%@ include file="/jsp/metatags.html" %>
    
        <link rel="stylesheet" type="text/css" href="<c:url value='/css/mivEnterData.css'/>">
        <link rel="stylesheet" type="text/css" href="<c:url value='/css/miv-forms.css'/>">
	<link rel="stylesheet" type="text/css" href="<c:url value='/css/data_table_gui.css'/>">
        <link rel="stylesheet" type="text/css" href="<c:url value='/css/expandable.css'/>">
        
        <!--[if lt IE 8]>
	        <link rel="stylesheet" type="text/css" href="<c:url value='/css/iehacks.css'/>">
	        <script type="text/javascript">mivConfig.isIE=true;</script>
        <![endif]-->
	<!--[if IE 8]>
	    <link rel="stylesheet" type="text/css" href="<c:url value='/css/ie8hacks.css'/>">
	    <script type="text/javascript">mivConfig.isIE=true;</script>
	<![endif]-->
        
        <style type="text/css" media="screen">
            /*
             * Override styles needed due to the mix of different CSS sources!
             */
            .dataTables_info { padding-top: 0; }
            .dataTables_paginate { padding-top: 0; }
            /*.css_right { float: right; }*/
            #theme_links span { float: left; padding: 2px 10px; }
            html.js table.display { border: 1px solid #CBC7BD; }
            html.js table.display thead th:focus,
            html.js table.display thead th:hover{ color: #654B24;}            
        </style>

        

        <script type="text/javascript" src="<c:url value='/js/jquery-ui.plugins.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/pubmed.js'/>"></script>
    </head>

    <body>
        <jsp:include page="/jsp/mivheader.jsp"/>
        
        <div id="breadcrumbs">
            <a href="<c:url value='/MIVMain'/>" title="Home">Home</a>
            
            &gt; <a href="<c:url value='/Imports'>
                          <c:param name='_flowId' value='${flowRequestContext.activeFlow.id}'/>
                          <c:param name='_eventId' value='root'/>
                          <c:param name="_flowExecutionKey" value="${flowExecutionKey}"/>
                          </c:url>" title="${strings.roottitle}">${strings.roottitle}</a>
                          
            &gt; <a href="<c:url value='/Imports'>
                          <c:param name='_flowId' value='${flowRequestContext.activeFlow.id}'/>
                          <c:param name='_eventId' value='form'/>
                          <c:param name="_flowExecutionKey" value="${flowExecutionKey}"/>
                          </c:url>" title="${strings.formtitle}">${strings.formtitle}</a>
                          
            &gt; ${strings.title}
        </div>
    
        <div id="main" class="designer">
            <h1>${strings.title}</h1>
          
            <c:if test="${not empty form.page.records}">
                <p>
                    ${strings.eliminateDuplicates}
                    
                    <div>
                        <strong>${strings.eliminateDuplicateWarning}</strong>
                    </div>
                </p>
            </c:if>
            
            <form:form  id="downloadForm" cssClass="mivdata" method="post" commandName="form">
                <c:choose>
                    <c:when test="${empty form.page.records}">
                        <div id="errormessage" class="haserrors">
                        	<div id="errorbox">${strings.noRecords}</div>
                        </div>
                    </c:when>
                    
                    <c:otherwise>
                        <div class="buttonset">
                            <button id="selectall" title="${strings.selectAllTitle}">${strings.selectAllLabel}</button>
                            <button id="clearall" title="${strings.deselectAllTitle}">${strings.deselectAllLabel}</button>
                        </div>
                        <div class="full_width">
	                        <table id="publist" class="display dataTable" summary="${strings.resultSummary}" >
	                            <caption>${strings.resultCaption}</caption>
	                            
	                            <thead>
	                                <tr>
	                                    <th colspan="2">${strings.resultHead}</th>
	                                    
	                                </tr>
	                            </thead>
	                            
	                            <tfoot>
	                                <tr>
	                                    <th colspan="2">
	                                        <spring:message text="${strings.resultFoot}"
	                                                        arguments="${form.page.recordIndex+1},${form.page.recordIndex+form.page.pageRecordCount},${form.page.recordCount}"/>
	                                    </th>
	                                </tr>
	                            </tfoot>
	                            
	                            <tbody>
	                                <c:forEach var="publication"
	                                           items="${form.page.records}"
	                                           begin="${form.page.recordIndex}"
	                                           end="${form.page.recordIndex+form.page.pageRecordCount-1}"
	                                           varStatus="row">
	                                    <tr class="${row.count % 2 == 0 ? 'even' : 'odd'}">
	                                        <td>
	                                            <form:checkbox path="page.records[${row.index}].selected"
	                                                           value="true"
	                                                           title="${strings.recordCheckboxTitle}"/>
	                                        </td>
	                                        
	                                        <td>
	                                            <form:label path="page.records[${row.index}].selected">
	                                               <cite>${publication.citation}&nbsp;${publication.year}</cite>
	                                           </form:label>
	                                        </td>
	                                    </tr>
	                                </c:forEach>
	                            </tbody>
	                        </table>
                        </div>
                        <div class="buttonset">
                            <c:if test="${form.page.pageCount > 1}">
                                <button type="submit"
                                        name="_eventId"
                                        value="previous"
                                        ${form.page.firstPage ? 'disabled="disabled"' : ''}
                                        title="${strings.previousTitle}">${strings.previousLabel}</button>
                                         
                                <spring:message text="${strings.pageIndex}"
                                                arguments="${form.page.pageIndex+1},${form.page.pageCount}"/>
                                 
                                <button type="submit"
                                        name="_eventId"
                                        value="next"
                                        ${form.page.lastPage ? 'disabled="disabled"' : ''}
                                        title="${strings.nextTitle}">${strings.nextLabel}</button>
                           </c:if>
                           
                           <button type="submit"
                                   name="_eventId"
                                   value="import"
                                   title="${strings.saveTitle}">${strings.saveLabel}</button>
                                   
                           <button type="submit"
                                   name="_eventId"
                                   value="form"
                                   title="${strings.cancelTitle}">${strings.cancelLabel}</button>
                        </div>
                    </c:otherwise>
                </c:choose>
            </form:form>
        </div><!-- main -->
        
        <jsp:include page="/jsp/mivfooter.jsp" />
    </body>
</html>
<%--
 vim:ts=8 sw=2 et:
--%>