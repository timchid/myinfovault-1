<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%><%--
--%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%--
--%><%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %><%--
--%><%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %><%--
--%><%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%><%--
--%><%@ taglib prefix="jwr" uri="http://jawr.net/tags" %><%--
--%><c:set var="user" scope="request" value="${MIVSESSION.user}"/><%--
--%><!DOCTYPE HTML>
<html>
<head>
    <title>MIV &ndash; ${strings.pageTitle}</title>

    <%@ include file="/jsp/metatags.html" %>

    <link rel="stylesheet" type="text/css" href="<c:url value='/css/mivcommon.css'/>">
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/mivEnterData.css'/>"/>
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/miv-forms.css'/>">
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/createaction.css'/>"/>
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/jquery.dataTables.min.css'/>">
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/widget-datatables-serverside.css'/>">

    <%@ include file="apiclientutil.jspf" %>

    <script type="text/javascript" src="<c:url value='/js/mivcommon.js'/>"></script>
    <script type="text/javascript" src="<c:url value='/js/jquery.dataTables.min.js'/>"></script>
    <script type="text/javascript" src="<c:url value='/js/widget-datatables-serverside.js'/>"></script>
    <script type="text/javascript" src="<c:url value='/js/date.js'/>"></script>
    <script type="text/javascript" src="<c:url value='/js/createaction.js'/>"></script>
    <script type="text/javascript" src="<c:url value='/js/usersearch.js'/>"></script>

    <script type="text/javascript">
    var actionTypes = {
    <c:forEach items="${form.dossierActionTypes}" var="actionType">
		"${actionType}" : [
		    <c:forEach items="${actionType.validDelegationAuthorities}" var="delAuth"><%--
--%>		    "${delAuth}",
		    </c:forEach><%--
--%>        ],
    </c:forEach>
    };
    </script>
</head>

<body>
    <jsp:include page="/jsp/mivheader.jsp" />
    <jsp:scriptlet><![CDATA[
         Integer d = new Integer(java.util.Calendar.getInstance().get(java.util.Calendar.YEAR));
         pageContext.setAttribute("currentYear", d, PageContext.REQUEST_SCOPE);
    ]]></jsp:scriptlet>
    <input type="hidden" id="currentYear" value="${currentYear}">

    <jsp:include page="breadcrumbs.jsp">
        <jsp:param value="${strings.pageTitle}" name="pageTitle"/>
    </jsp:include>

    <!-- Dossier Data -->
    <div id="main">
        <h1>${strings.pageTitle}</h1>

        <form:form commandName="form" method="post" cssClass="mivdata">
            <fieldset class="account-search">
                <legend>Find a Candidate</legend>

                <label class="label" for="usersearch">Candidate Name</label>
                <input id="usersearch" type="search" name="account" data-constraints="dataTableInitialized">
                <input id="searchbutton" type="button" value="Search" title="Search">
            </fieldset>

            <fieldset>
                <legend>${strings.actionLegend}</legend>

                <p>${strings.instruction}</p>

                <input type="hidden" id="submitterId" name="submitterId" value="${submitterId}">

                <div class="inline-formline">
                    <form:label path="actionType" cssClass="label f_required">${strings.actionLabel}</form:label>
                    <form:select path="actionType"
                                 items="${form.dossierActionTypes}"
                                 itemLabel="description"
                                 title="${tooltips.actionTitle}"
                                 required="required" />
                </div>

                <div class="formline">
                    <form:label path="delegationAuthority" cssClass="label f_required">${strings.delegationLabel}</form:label>
                    <form:select path="delegationAuthority"
                                 items="${delegations}"
                                 itemLabel="description"
                                 title="${tooltips.delegationTitle}"
                                 required="required" />
                </div>

                <div class="formline" title="${strings.effectiveTitle}">
                    <spring:bind path="effectiveDate">
                        <label for="${status.expression}" class="label f_required">${strings.effectiveLabel}</label>

                        <c:if test="${empty status.errorCodes}">
                            <fmt:formatDate type="date" dateStyle="long" value="${form.effectiveDateAsDate}" var="effectiveDate" />
                        </c:if>

                        <input type="text" class="datepicker"
                               id="${status.expression}"
                               name="${status.expression}"
                               value="${effectiveDate}"
                               size="20"
                               required/>
                    </spring:bind>
                </div>

                <div class="formline">
                    <div class="buttonset">
                        <input type="button" name="_eventId_select" class="startAction"
                               value="${strings.sendLabel}" title="${tooltips.sendTitle}">
                        <input type="button" class="buttonlink" value="${strings.cancelLabel}"
                               title="${strings.cancelLabel}" data-href="<c:url value="/MIVMain" />">
                    </div>
                </div>
            </fieldset>
        </form:form>

        <p>${strings.helpLink}</p>
    </div><!-- main -->

    <jsp:include page="/jsp/miv_small_footer.jsp" />
</body>
</html>
