<%--
    DESCRIPTION:
        Displays breadcrumbs for a webflow page OR for a "breadcrumbs" map of URLs keyed to titles in the servlet context.

    INPUT:
        * "param.pageTitle" : Page title for end of bread crumb list

--%><%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%><%--
--%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%--
--%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<ul id="breadcrumbs">
    <li><a href="<c:url value='/MIVMain'/>" title="Home">Home</a></li>
    <c:if test="${not empty breadcrumbs || not empty param.pageTitle}">
        <c:choose>
            <c:when test="${not empty flowRequestContext.activeFlow.id}">
                <c:forEach var="breadcrumb" items="${breadcrumbs}" varStatus="crumbRow">
                    <c:url var="link" value='${context.request.contextPath}'>
                        <c:param name='_flowId' value='${flowRequestContext.activeFlow.id}'/>
                        <c:param name="_flowExecutionKey" value="${flowExecutionKey}"/>
                        <c:param name='_eventId' value='breadcrumb${fn:length(breadcrumbs) - crumbRow.index}'/>
                    </c:url>
                    <li><a href="<c:out escapeXml="true" value="${link}"/>" title="${breadcrumb}">${breadcrumb}</a></li>
                 </c:forEach>
            </c:when>
            <c:otherwise>
                <c:forEach var="breadcrumb" items="${breadcrumbs}">
                    <c:url var="link" value="${breadcrumb.value}"/>
                    <li><a href="<c:out escapeXml="true" value="${link}"/>" title="${breadcrumb.key}">${breadcrumb.key}</a></li>
                </c:forEach>
            </c:otherwise>
        </c:choose>
       <li>${param.pageTitle}</li>
    </c:if>
</ul>
