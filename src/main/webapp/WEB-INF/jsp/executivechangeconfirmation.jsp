<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%><%--
--%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%--
--%><!DOCTYPE html>
<html>
    <head>
        <title>MIV &ndash; ${strings.pageTitle}</title>

        <%@ include file="/jsp/metatags.html" %>

        <link rel="stylesheet" type="text/css" href="<c:url value='/css/executive.css'/>"/>
    </head>

    <body>
        <jsp:include page="/jsp/mivheader.jsp" />

        <jsp:include page="breadcrumbs.jsp">
            <jsp:param value="${strings.pageTitle}" name="pageTitle"/>
        </jsp:include>

        <div id="main">
            <h1>${strings.pageTitle}</h1>

            <table>
                <caption>${strings.description}</caption>
                
                <tr>
                    <th scope="row">${labels.viceProvost}</th>
                    <td>${form.viceProvost.displayName}</td>
                </tr>

                <tr>
                    <th scope="row">${labels.provost}</th>
                    <td>${form.provost.displayName}</td>
                </tr>

                <tr>
                    <th scope="row">${labels.chancellor}</th>
                    <td>${form.chancellor.displayName}</td>
                </tr>
            </table>
        </div>

        <jsp:include page="/jsp/miv_small_footer.jsp" />
    </body>
</html>
