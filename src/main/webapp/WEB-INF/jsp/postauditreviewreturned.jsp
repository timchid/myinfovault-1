<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%><%--
--%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%--
--%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %><%--
--%><!DOCTYPE HTML>
<html>

<c:choose>
        <c:when test="${not empty confirmationError}">
                <c:set var="pageTitle" value="Dossier Return Failed"/>
        </c:when>
        <c:otherwise>
                <c:set var="pageTitle" value="Dossier Returned"/>
        </c:otherwise>
</c:choose>

<head>
  <title>MIV &ndash; ${pageTitle}</title>
  <script type="text/javascript">
  /* <![CDATA[ */
      var mivConfig = new Object();
      mivConfig.isIE = false;
  /* ]]> */
  </script>

  <%@ include file="/jsp/metatags.html" %>

  <!--[if lt IE 8]>
  <link rel="stylesheet" type="text/css" href="<c:url value='/css/iehacks.css'/>">
  <script type="text/javascript">mivConfig.isIE=true;</script>
  <![endif]-->
<c:set var="user" scope="request" value="${MIVSESSION.user}"/>
</head>
<body>

<jsp:include page="/jsp/mivheader.jsp" />

<div id="breadcrumbs">
    <a href="<c:url value='/MIVMain'/>" title="Home">Home</a>
    &gt;<a href="<c:url value='${context.request.contextPath}'>
	    <c:param name='_flowId' value='${flowRequestContext.activeFlow.id}'/>
    </c:url>" title="Send Dossiers to Post Audit">Send Dossiers to Post Audit</a>
    &gt; ${pageTitle}
</div><!-- breadcrumbs -->

<div id="main"><!-- MIV Main Div -->

<h1>${pageTitle}</h1>

<%-- Display a confirmation error if present --%>
<c:choose>
<c:when test="${not empty confirmationError}">
        <p>An error has occurred and the dossier could not be returned.</p>
        <p>Please contact the MIV Project Team at
        <a href="mailto:miv-help@ucdavis.edu"
           title="miv-help@ucdavis.edu">miv-help@ucdavis.edu</a> for assistance.
        </p>
</c:when>
<c:when test="${not empty returnInProgress}">
        <p>
        Multiple return requests have been made while there was a pending request to return the following dossier to the ${previousLocation}
        </p>
        <div class="formline"><strong>Candidate:</strong>&nbsp;${candidate}</div>
        <div class="formline"><strong>School/College - Department:</strong>&nbsp;${department}</div>
        <div class="formline"><strong>Action:</strong>&nbsp;${action}</div>
</c:when>
<c:otherwise>
        <p>
        The following dossier has been returned to the ${previousLocation}:
        </p>
        <div class="formline"><strong>Candidate:</strong>&nbsp;${candidate}</div>
        <div class="formline"><strong>School/College - Department:</strong>&nbsp;${department}</div>
        <div class="formline"><strong>Action:</strong>&nbsp;${action}</div>
</c:otherwise>
</c:choose>

</div>
<!-- main -->

<jsp:include page="/jsp/miv_small_footer.jsp" />

</body>
</html>
