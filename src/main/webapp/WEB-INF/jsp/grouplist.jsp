<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%><%--
--%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%--
--%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %><%--
--%><%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %><%--
--%><%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %><%--
--%><c:set var="user" scope="request" value="${MIVSESSION.user}"/><%--
--%><!DOCTYPE html>
<html>
    <head>
        <title>MIV &ndash; ${strings.title}</title>

        <%@ include file="/jsp/metatags.html" %>

        <link rel="stylesheet" type="text/css" href="<c:url value='/css/mivEnterData.css'/>">
        <link rel="stylesheet" type="text/css" href="<c:url value='/css/miv-forms.css'/>">
        <link rel="stylesheet" type="text/css" href="<c:url value='/css/data_table_gui.css'/>">

	<!--[if lt IE 8]>
		<link rel="stylesheet" type="text/css" href="<c:url value='/css/iehacks.css'/>">
		<script type="text/javascript">mivConfig.isIE=true;</script>
        <![endif]-->
    	<!--[if IE 8]>
		<link rel="stylesheet" type="text/css" href="<c:url value='/css/ie8hacks.css'/>">
		<script type="text/javascript">mivConfig.isIE=true;</script>
    	<![endif]-->

        <script type="text/javascript" src="<c:url value='/js/jquery.dataTables.min.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/datatable/data_table_config.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/grouplist.js'/>"></script>
    </head>

    <body>
        <c:set var="helpPage" scope="request" value="manage_groups.html"/>
        <c:set var="helpTip" scope="request" value="Help with creating and managing groups"/>
        <jsp:include page="/jsp/mivheader.jsp" />

        <jsp:include page="breadcrumbs.jsp">
            <jsp:param value="${strings.title}" name="pageTitle"/>
        </jsp:include>

        <div id="main">
            <h1>${strings.title}</h1>

            <p>
                <form:form method="post">
                    <input type="submit" name="_eventId_edit" value="${strings.createLabel}" title="${strings.createTitle}"/>
                </form:form>
            </p>

            <c:if test="${not empty results}">
                <table class="display" id="grouplist" summary="${strings.listSummary}" width="100%">
                    <thead>
                        <tr>
                            <th width="25%" class="datatable-column-filter" scope="col" title="${strings.nameTitle}">${strings.nameHeader}</th>
                            <th width="30%" class="datatable-column-filter" scope="col" title="${strings.descriptionTitle}">${strings.descriptionHeader}</th>
                            <th width="15%" class="datatable-column-filter" scope="col" title="${strings.creatorTitle}">${strings.creatorHeader}</th>
                            <th width="15%" class="datatable-column-filter" scope="col" title="${strings.createdTitle}">${strings.createdHeader}</th>
                            <th width="15%" class="datatable-column-filter" scope="col" title="${strings.actionsTitle}">${strings.actionsHeader}</th>
                        </tr>
                    </thead>

                    <tfoot>
                        <tr class="searchresults">
                            <th colspan="5">${strings.searchResults}${fn:length(results)}</th>
                        </tr>
                    </tfoot>

                    <tbody>
                        <c:forEach items="${results}" var="record" varStatus="row">
                            <tr class="${(row.count)%2==0?'even':'odd'}">
                                <td>${record.name}</td>
                                <td class="groupdesc"><span class="groupdesc">${record.description}</span></td>
                                <td class="groupcreator">${record.creator.displayName}</td>
                                <td class="groupcreated"><fmt:formatDate pattern="M/d/yyyy" value="${record.created}"/></td>
                                <td class="groupaction">
                                    <form:form method="post">
                                        <input type="hidden" name="groupId" value="${record.id}"/>

                                        <input type="submit" name="_eventId_view" value="${strings.viewLabel}" title="${strings.viewTitle}"/>

                                        <c:if test="${not record.readOnly or record.creator.userId == user.targetUserInfo.person.userId}">
                                            <input type="submit" name="_eventId_edit" value="${strings.editLabel}" title="${strings.editTitle}"/>
                                            <input type="submit" name="_eventId_delete" value="${strings.deleteLabel}" title="${strings.deleteTitle}"/>
                                        </c:if>
                                    </form:form>
                                </td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </c:if>
        </div>

        <jsp:include page="/jsp/miv_small_footer.jsp" />
    </body>
</html>
