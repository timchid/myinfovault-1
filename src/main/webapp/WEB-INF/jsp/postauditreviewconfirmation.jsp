<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%><%--
--%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%--
--%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %><%--
--%><%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%><%--
--%><!DOCTYPE html>
<html>
    <head>
        <c:set var="pagetitle" value="Send Dossier to Post Audit: Verification"/>
        <title>MIV &ndash; ${pagetitle}</title>
        <script type="text/javascript">
           var mivConfig = new Object();
           mivConfig.isIE = false;
        </script>
        <%@ include file="/jsp/metatags.html" %>

        <link rel="stylesheet" type="text/css" href="<c:url value='/css/mivcommon.css'/>">
        <link rel="stylesheet" type="text/css" href="<c:url value='/css/mivEnterData.css'/>">
        <link rel="stylesheet" type="text/css" href="<c:url value='/css/miv-forms.css'/>">
        <link rel="stylesheet" type="text/css" href="<c:url value='/css/postauditreview.css'/>">

        <!--[if lt IE 8]>
            <link rel="stylesheet" type="text/css" href="<c:url value='/css/iehacks.css'/>" />
            <script type="text/javascript">mivConfig.isIE=true;</script>
        <![endif]-->

        <c:set var="user" scope="request" value="${MIVSESSION.user}"/>
    </head>

    <body>
        <jsp:include page="/jsp/mivheader.jsp" />

    <!-- Breadcrumbs Div -->
    <div id="breadcrumbs">
      <a href="<c:url value='/MIVMain'/>" title="Home">Home</a>
          &gt; <a href="<c:url value='${context.request.contextPath}'>
            <c:param name='_flowId' value='${flowRequestContext.activeFlow.id}'/>
            <c:param name="_flowExecutionKey" value="${flowExecutionKey}"/>
            <c:param name='_eventId' value='root'/>
            </c:url>" title="Send Dossiers to Post Audit">Send Dossiers to Post Audit</a>
         &gt; ${pagetitle}
    </div><!-- breadcrumbs -->

        <!-- MIV Main Div -->
        <div id="main">
            <h1>${pagetitle}</h1>
            <form:form name="verifyDossiers" cssClass="mivdata" method="post" commandName="PostAuditDossiersForm" >

            <c:if test="${empty dossiersToPostAuditReviewMap}">
                <div>No dossiers have been selected to route to Post Audit location.</div>
            </c:if>

            <c:if test="${!empty dossiersToPostAuditReviewMap}">
            	<c:set var="dossierCount" scope="page" value="${fn:length(dossiersToPostAuditReviewMap)}"/>
            	
                <p>Select the "Send Dossier to Post Audit" button to route the ${dossierCount>1?'dossiers':'dossier'} to Post Audit location,
                     or the "Cancel" button to stop the routing of these ${dossierCount>1?'dossiers':'dossier'} at this time.</p>
                <p class="attention"><strong>Dossiers can no longer be returned for edits once they have been sent to Post Audit.</strong></p>
                
                <div class="buttonset">
                    <input type="submit" title="Send Dossier to Post Audit" name="_eventId_routeDossier" value="Send Dossier to Post Audit">
                    <input type="submit" title="Cancel" name="_eventId_cancel" value="Cancel">
                    <input type="hidden" name="_flowExecutionKey" value="${flowExecutionKey}">
                </div>

                <div id="listarea" class="verifydossiers">
                	<div class="dossiercount">Total&nbsp;Dossier${dossierCount>1?'s':''}&nbsp;=&nbsp;${dossierCount}</div>
                    <ul class="itemlist"><%--
                    --%><c:forEach var="dossier" items="${dossiersToPostAuditReviewMap}" varStatus="rowIndex">
                        <li class="records ${(rowIndex.count)%2==0?'even':'odd'}">
                        	<strong>${dossier.key}:</strong> ${dossier.value}
                        </li><%--
                    --%></c:forEach>
                    </ul>
                </div>

                <div class="buttonset">
                    <input type="submit" title="Send Dossier to Post Audit" name="_eventId_routeDossier" value="Send Dossier to Post Audit">
                    <input type="submit" title="Cancel" name="_eventId_cancel" value="Cancel">
                    <input type="hidden" name="_flowExecutionKey" value="${flowExecutionKey}">
                </div>

            </c:if>
          </form:form>
        </div><!-- main -->

        <jsp:include page="/jsp/miv_small_footer.jsp" />
    </body>
</html>
