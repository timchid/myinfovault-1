<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%><%--
--%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%--
--%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %><%--
--%><%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%><%--
<%----%><!DOCTYPE html>
<html>
    <head>
        <c:set var="pagetitle" value="Send Dossier to Post Audit: Confirmation"/>
        <title>MIV &ndash; ${pagetitle}</title>
        <script type="text/javascript">
           var mivConfig = new Object();
           mivConfig.isIE = false;
        </script>
        <%@ include file="/jsp/metatags.html" %>

        <link rel="stylesheet" type="text/css" href="<c:url value='/css/mivcommon.css'/>">
        <link rel="stylesheet" type="text/css" href="<c:url value='/css/mivEnterData.css'/>">
        <link rel="stylesheet" type="text/css" href="<c:url value='/css/miv-forms.css'/>">
		<link rel="stylesheet" type="text/css" href="<c:url value='/css/postauditreview.css'/>">
		
        <!--[if lt IE 8]>
            <link rel="stylesheet" type="text/css" href="<c:url value='/css/iehacks.css'/>" />
            <script type="text/javascript">mivConfig.isIE=true;</script>
        <![endif]-->

        <c:set var="user" scope="request" value="${MIVSESSION.user}"/>
    </head>

    <body>
        <jsp:include page="/jsp/mivheader.jsp" />

    <!-- Breadcrumbs Div -->
    <div id="breadcrumbs">
      <a href="<c:url value='/MIVMain'/>" title="Home">Home</a>
          &gt; <a href="<c:url value='${context.request.contextPath}'>
            <c:param name='_flowId' value='${flowRequestContext.activeFlow.id}'/>
            <c:param name="_flowExecutionKey" value="${flowExecutionKey}"/>
            <c:param name='_eventId' value='root'/>
            </c:url>" title="Send Dossiers to Post Audit">Send Dossiers to Post Audit</a>
           <%-- No need Verification breadcrumb here because action has been complete all ready --%> 
          <%-- &gt; <a href="<c:url value='${context.request.contextPath}'>
            <c:param name='_flowId' value='${flowRequestContext.activeFlow.id}'/>
            <c:param name="_flowExecutionKey" value="${flowExecutionKey}"/>
            <c:param name='_eventId' value='confirmation'/>
            </c:url>" title="Post Audit Dossiers: Verification">Post Audit Dossiers: Verification</a> --%>
         &gt; ${pagetitle}
    </div><!-- breadcrumbs -->

        <!-- MIV Main Div -->
        <div id="main">
            <h1> ${pagetitle}</h1>
            <form:form name="confirmDossiers" cssClass="mivdata" method="post" commandName="PostAuditDossiersForm" >
				<c:if test="${empty dossiersToPostAuditReviewMap}">
					<div>No dossiers have been selected to route to Post Audit location.</div>
				</c:if>
	
				<c:if test="${!empty dossiersToPostAuditReviewMap}">
					<c:set var="dossierCount" scope="page" value="${fn:length(dossiersToPostAuditReviewMap)}" />
	
					<div>The following ${dossierCount>1?'dossiers have':'dossier has'} been routed to Post Audit location.</div>
					<br>
					<div id="listarea" class="verifydossiers">
						<div class="dossiercount">Total&nbsp;Dossier${dossierCount>1?'s':''}&nbsp;=&nbsp;${dossierCount}</div>
						<ul class="itemlist">
							<c:forEach var="dossier" items="${dossiersToPostAuditReviewMap}"
								varStatus="rowIndex">
								<li class="records ${(rowIndex.count)%2==0?'even':'odd'}">
									<strong>${dossier.key}:</strong>${dossier.value}
								</li>
							</c:forEach>
						</ul>
					</div>
				</c:if>	
		</form:form>
        </div><!-- main -->

        <jsp:include page="/jsp/miv_small_footer.jsp" />
    </body>
</html>
