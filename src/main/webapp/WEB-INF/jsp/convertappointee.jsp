<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%><%--
--%><%@ taglib prefix="miv" uri="mivTags" %><%--
--%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%--
--%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %><%--
--%><!DOCTYPE html>
<html>
    <head>
        <%@ include file="/jsp/metatags.html" %>

        <link rel="stylesheet" type="text/css" href="<c:url value='/css/mivcommon.css'/>"/>
        <link rel="stylesheet" type="text/css" href="<c:url value='/css/mivEnterData.css'/>"/>
        <link rel="stylesheet" type="text/css" href="<c:url value='/css/miv-forms.css'/>"/>

        <title>MIV &ndash; Convert Appointee to Candidate</title>
        <style type="text/css">
            .mivDialog label:after {
                content: ":";
            }
            #messages.confirm {
                border: 1px solid #666;
                box-shadow: 3px 3px 5px #666;
                padding-left: 1em;
                max-width: 90%;
            }
        </style>
    </head>
    <body>
        <jsp:include page="/jsp/mivheader.jsp" />
        <div id="main">
        <miv:permit roles="SYS_ADMIN|VICE_PROVOST_STAFF">
            <h1>Convert Appointee to Candidate</h1>
            <!-- <p>Status: [${status}]</p> -->
            <c:if test="${fn:length(pagemessage) > 0}">
            <div id="messages" class="${status}"><p>${pagemessage}<p>
            </div>
            </c:if>
            <div id="instructions">
                <p>
                To convert an Appointee to a Candidate you must have the MIV user ID number of the Appointee
                to be converted, and the campus Employee ID number assigned to that person when they were hired.
                </p>
                <p>
                Enter the MIV User ID of the Appointee who is to be changed to a Candidate.
                The user ID can be found by entering the appointee's last name in the
                <a href="<c:url value='/jsp/finduser.jsp'/>"
                   title="Find an MIV User">MIV User Lookup Utility</a>.
                </p>
                <p>
                Enter the person's Employee Number and press <em>Convert</em>.
                </p>
                <p class="caution">
                Excercise caution when using this feature! This is a <em>"utility"</em> and is not
                meant to be a polished product suitable for any end-user to employ.
                </p>
            </div>

            <div class="mivDialog">
                <form action="<c:url value='/ConvertAppointee'/>" method="post">

                    <div class="formline">
                        <span class="textfield" title="Enter the MIV User ID of the person to convert">
                            <label for="userId" class="f_required">MIV User ID</label>
                            <input type="text"
                                   name="userId"
                                   id="userId"
                                   autofocus
                                   size="6" maxlength="6"
                                   placeholder="User ID"
                                   value="${userId}"
                                   required
                                   >
                        </span>
                    </div>

                    <div class="formline">
                        <span class="textfield" title="Enter the Employee Number of this person">
                            <label for="empnum" class="f_required">Employee Number</label>
                            <input type="text"
                                   name="empnum"
                                   id="empnum"
                                   size="15" maxlength="9"
                                   placeholder="Employee Number"
                                   value="${empnum}"
                                   required
                                   >
                        </span>
                    </div>

                    <div class="buttonset">
                        <input type="submit" name="convert" value="Convert"
                               title="Convert Appointee to Candidate">
                        <input type="reset" id="popupformcancel" name="popupformcancel"
                               value="Cancel" title="Cancel Conversion">
                    </div>

                </form>
            </div><!-- .mivDialog -->
        </miv:permit>

        <miv:permit roles="(!SYS_ADMIN)&(!VICE_PROVOST_STAFF)">
        <p><strong>You are not authorized to view this page.</strong></p>
        </miv:permit>
        </div><!-- #main -->
    </body>
</html>
