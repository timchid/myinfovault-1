<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%><%--
--%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%--
--%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %><%--
--%><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>

<c:set var="pageTitle" value="PDF Upload."/>
<c:set var="confirmationStatus" value=": Confirmation"/>

<c:if test="${not empty confirmationError}">
    <c:set var="confirmationStatus" value=": Error"/>
</c:if>

<head>
  <title>MIV &ndash; ${pageTitle}${confirmationStatus}</title>
  <script type="text/javascript">
    var mivConfig = new Object();
    mivConfig.isIE = false;
  </script>

 <%@ include file="/jsp/metatags.html" %>

  <!--[if lt IE 8]>
  <link rel="stylesheet" type="text/css" href="<c:url value='/css/iehacks.css'/>">
  <script type="text/javascript">mivConfig.isIE=true;</script>
  <![endif]-->
<c:set var="user" scope="request" value="${MIVSESSION.user}"/>
</head>
<body>

<jsp:include page="/jsp/mivheader.jsp" />

<div id="breadcrumbs">
    <a href="<c:url value='/MIVMain'/>" title="Home">Home</a>
    <c:if test="${empty confirmationError}">
    &gt; <a title="${pageTitle}" href="PdfUpload?_flowId=pdfupload-flow">${pageTitle}</a>
    </c:if>
    &gt; ${pageTitle}${confirmationStatus}
</div><!-- breadcrumbs -->

<div id="main"><!-- MIV Main Div -->

<h1>${pageTitle}${confirmationStatus}</h1>

<!-- Display a confirmation error if present -->
 <c:if test="${not empty confirmationError}">
        <p>${confirmationError}</p>
</c:if>

<!-- Display upload information -->
</div>
<!-- main -->

<jsp:include page="/jsp/miv_small_footer.jsp" />

</body>
</html>
