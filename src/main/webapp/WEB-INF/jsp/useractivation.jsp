<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%><%--
--%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%--
--%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %><%--
--%><%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %><%--
--%><!DOCTYPE html>
<html>
<head>
    <title>MIV &ndash; <%-- View MIV Users: Search Results --%>
    ${strings.pagetitle}<c:if test="${fn:length(strings.subtitle)>0}">:
    ${strings.subtitle}</c:if>
    </title>
    <script type="text/javascript">
    /* <![CDATA][ */
      var mivConfig = new Object();
      mivConfig.isIE = false;
    /* ]]> */
    </script>

    <%@ include file="/jsp/metatags.html" %>

    <link rel="stylesheet" type="text/css" href="<c:url value='/css/mivEnterData.css'/>">
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/miv-forms.css'/>">
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/data_table_gui.css'/>">
    <!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<c:url value='/css/iehacks.css'/>">
	<script type="text/javascript">mivConfig.isIE=true;</script>
    <![endif]-->
    <!--[if IE 8]>
	<link rel="stylesheet" type="text/css" href="<c:url value='/css/ie8hacks.css'/>">
	<script type="text/javascript">mivConfig.isIE=true;</script>
    <![endif]-->

    <style type="text/css" media="screen">
        /*
         * Override styles needed due to the mix of different CSS sources!
         */
        .dataTables_info { padding-top: 0; }
        .dataTables_paginate { padding-top: 0; }
        .css_right { float: right; }
        #theme_links span { float: left; padding: 2px 10px; }
    </style>

    <script type="text/javascript" src="<c:url value='/js/jquery.dataTables.min.js'/>"></script>
    <script type="text/javascript" src="<c:url value='/js/datatable/data_table_config.js'/>"></script>

    <script type="text/javascript">
        $(document).ready( function() {
            // for more details look into /js/datatable/data_table_config.js
            var customizeSettingMap = new Object();
            customizeSettingMap["aoColumns"] = [{ "sSortDataType": "dom-checkbox" },null,null,null,null];
            customizeSettingMap["aaSorting"] = [[ 1, 'asc' ]];
            customizeSettingMap["aFilterColumns"] = [1,2,3,4];
            oTable = fnInitDataTable('userlist', customizeSettingMap);

            // Add all checked checkboxes from the hidden pages to the page for submit
            $('#reactivateuser').submit ( function() {
                $(oTable.fnGetHiddenTrNodes()).find('input:checked').hide().appendTo(this);
            });

            /* Add a checkbox click handler */
            $('#userlist tr td').on('click', 'input:checkbox', function () {
                tr = this.parentNode.parentNode;
                $(tr).toggleClass('row_active');
            } );
        });
    </script>

    <c:set var="user" scope="request" value="${MIVSESSION.user}"/>
</head>

<body>
    <jsp:include page="/jsp/mivheader.jsp" />

    <jsp:include page="breadcrumbs.jsp">
        <jsp:param value="${strings.pagetitle}" name="pageTitle"/>
    </jsp:include>

    <div id="main">
      <h1>${strings.description} for ${searchCriteriaText}</h1><%--
  --%><c:if test="${empty results}">
        <p>${strings.noResultsMessage}</p><%--
  --%></c:if>
      <c:if test="${!empty results}">
        <p>${strings.reactivateinstruction} ${strings.deactivatedusers}</p>
        <p>${strings.message}</p>
        <p>${strings.activateusersselect}</p>
        <%--   leo added for user reactivation   --%>
        <form:form commandName="form" method="post" id="reactivateuser" cssClass="DELETEmivdataME"><%-- shouldn't need the "mivdata" class, and it causes problems with the checkboxes --%>
        <%-- button submit --%>
        <div class="searchresults">Search Results = ${fn:length(results)}</div>
        <div class="buttonset">
            <input type="submit" name="_eventId_save" id="saveA" value="Save" title="Save">
            <input type="submit" name="_eventId_breadcrumb1" id="cancelA" value="Cancel" title="Cancel">
        </div>

        <div class="full_width">
            <table cellpadding="0" cellspacing="0" border="0" class="display" id="userlist" width="100%">
                <thead>
                    <tr><%-- TODO: These hrefs need to actually point to something that will do sorting if no Javascript is available --%>
                        <th class="datatable-column-filter" title="Active status">Active User</th>
                        <th class="datatable-column-filter" title="MIV User">MIV User</th>
                        <th class="datatable-column-filter" title="School/College">School/College</th>
                        <th class="datatable-column-filter" title="Department">Department</th>
                        <th class="datatable-column-filter" title="Role">Role</th>
                    </tr>
                </thead>
                <tbody><%--
                --%><c:forEach var="usr" items="${results}" varStatus="row">
                    <tr class="${(row.count)%2==0?'even':'odd'}<c:if test="${usr.active}"> row_active</c:if>" >
                        <td><%-- TODO: attach event handlers via a separate .js file --%>
                            <input type="checkbox" name="${usr.userId}" id="A${usr.userId}"
                                   value="${usr.userId}" title="Check to activate/deactivate this user" ${usr.active ? ' checked' : ''}>
                        </td>
                        <td>${usr.surname}, ${usr.givenName} ${usr.middleName}</td>
                        <td>${usr.primaryAppointment.schoolName}</td>
                        <td>${!empty usr.primaryAppointment.departmentName ? usr.primaryAppointment.departmentName : '&nbsp;'}</td>
                        <td>${usr.primaryRoleType.shortDescription}</td>
                    </tr><%--
                --%></c:forEach>
                </tbody>
            </table>
        </div>

        <div class="buttonset">
            <input type="submit" name="_eventId_save" id="sav02" value="Save" title="Save">
            <input type="submit" name="_eventId_breadcrumb1" id="cancel02" value="Cancel" title="Cancel">
        </div>
        </form:form><%--
        <!--
        jQuery one-liner for checking all checkboxes
          $("input[type='checkbox']).each(function(index) { allboxes[index].checked="checked"; })
        -->
      --%>
      </c:if>
    </div><!--main-->
    <jsp:include page="/jsp/miv_small_footer.jsp"/>
    </body>
</html>
