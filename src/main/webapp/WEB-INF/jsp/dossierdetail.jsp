<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%--
--%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %><%--
--%><%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %><%--
--%><%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%><%--
--%><%@ taglib prefix="miv" uri="mivTags" %><%--
--%>
<link rel="stylesheet" type="text/css" href="<c:url value='/css/jquery.qtip.min.css'/>">
<link rel="stylesheet" type="text/css" href="<c:url value='/css/mivEnterData.css'/>">
<link rel="stylesheet" type="text/css" href="<c:url value='/css/miv-forms.css'/>">
<link rel="stylesheet" type="text/css" href="<c:url value='/css/resequence.css'/>">
<link rel="stylesheet" type="text/css" href="<c:url value='/css/mivcommon.css'/>">


<!--[if IE 8]>
<link rel="stylesheet" type="text/css" href="<c:url value='/css/ie8hacks.css'/>">
<script type="text/javascript">mivConfig.isIE=true;</script>
<![endif]-->

<script type="text/javascript" src="<c:url value='/js/jquery-ui.plugins.js'/>"></script>
<script type="text/javascript" src="<c:url value='/js/jquery-contained-sticky-scroll.js'/>"></script>
<script type="text/javascript" src="<c:url value='/js/jquery.qtip.min.js'/>"></script>
<script type="text/javascript" src="<c:url value='/js/qtip_config.js'/>"></script>
<script type="text/javascript" src="<c:url value='/js/detect.min.js'/>"></script>
<script type="text/javascript" src="<c:url value='/js/mivcommon.js'/>"></script>
<script type="text/javascript" src="<c:url value='/js/dossierdetail.js'/>"></script>
<%--
--%><c:if test="${empty viewStatusError}"><%--
        MIV-3475 - If no results and location is ready to archive, show below message.

    --%><c:if test="${fn:length(results) == 0 && fn:toLowerCase(dossierLocation) == 'readytoarchive'}">
          <p>The following action is pending archiving, but has not been archived yet.</p><%--
    --%></c:if>

<input type="hidden" name="userId" id="userId" value="${dossier.userId}">
<input type="hidden" id="_flowExecutionKey" value="${flowExecutionKey}">

<div id="openaction-summary"><%-- Adding div to handle sticky header --%>
    <div id="sticky">

        <%-- dossier attribute is in request scope, so no need to create page scoped variable --%>
        <jsp:include page="/jsp/dossierheader.jsp"/>

        <ul class="dossierlinks">
<c:set var="anyUpdate" value="${1==0}"/>
<c:forEach var="appt" items="${results}" varStatus="apploop">
<%-- anyUpdate is ${anyUpdate} &nbsp; &nbsp; &nbsp; --%><%--debug--%>
<c:set var="anyUpdate" value="${anyUpdate || appt.isUpdate}"/>
<%-- anyUpdate is now ${anyUpdate}<br> --%><%--debug--%>
</c:forEach>
<%--<li>displayAppointmentDetails is [${displayAppointmentDetails}]</li>--%><%--debug--%>
            <miv:permit action="View Dossier">
<miv:permit roles="!CANDIDATE|VICE_PROVOST">
<!-- Don't show link when dossier has been routed in either direction -->
<c:if test="${anyUpdate && !(fn:contains(actionMsg, 'sent') || fn:contains(actionMsg, 'return'))}">
                <li><a href="${dossierpdf}" title="View the Dossier as One PDF File">View the Dossier as One PDF File</a></li>
</c:if><%-- THIS IF is THE CHANGE --%>
</miv:permit>
            </miv:permit>

            <miv:permit action="View Event Log">
                <li><a href="<c:url value='${context.request.contextPath}'>
                             <c:param name='_flowId' value='${flowRequestContext.activeFlow.id}'/>
                             <c:param name="_flowExecutionKey" value="${flowExecutionKey}"/>
                             <c:param name='_eventId' value='event_log'/>
                             <c:param name='category' value='DOSSIER'/>
                             <c:param name='dossierId' value='${dossier.dossierId}'/>
                             </c:url>" title="View the Event Log" >View the Event Log</a></li>
            </miv:permit>

            <miv:permit action="View Signature Log">
                <li><a href="<c:url value='${context.request.contextPath}'>
                             <c:param name='_flowId' value='${flowRequestContext.activeFlow.id}'/>
                             <c:param name="_flowExecutionKey" value="${flowExecutionKey}"/>
                             <c:param name='_eventId' value='event_log'/>
                             <c:param name='category' value='SIGNATURE'/>
                             <c:param name='dossierId' value='${dossier.dossierId}'/>
                             </c:url>" title="View the Signature Log" >View the Signature Log</a></li>
            </miv:permit>
        </ul><!-- .dossierlinks -->

        <div class="dossier-buttons">
             <c:if test="${displayAppointmentDetails}">
                 <div class="buttonset hidden" id="ResequenceButtons">
                     <input type="button" id="SaveResequence" class="SaveResequence hidden" value="Save Resequencing" title="Save the new order of the letters">
                     <a href="<c:url value='OpenActions?_flowExecutionKey=${flowExecutionKey}'/>"
                        id="CancelResequence" class="CancelResequence linktobutton hidden"
                        title="Cancel resequencing and restore original order">Cancel Resequencing</a>
                     <input type="button" id="ActiveResequence" class="ActiveResequence" value="Resequence Letters" title="Begin resequencing letters">
                 </div>
             </c:if>
        </div><!-- .dossier-buttons -->
    </div>
</div><!-- openaction-summary -->

<c:if test="${displayAppointmentDetails}">
    <div id="appointments" data-location="${dossier.dossierLocation}">
        <%-- START OF THE APPOINTMENT LOOP : each "appt" gets a section heading followed by a list of actions.
--%><c:forEach var="appt" items="${results}" varStatus="apptLoopCount"><%--
   --%><form:form commandName="searchCriteria" method="get" cssClass="mivdata" action="/miv/OpenActions?_flowId=${flowRequestContext.activeFlow.id}&_flowExecutionKey=${flowExecutionKey}">
         <div id="formcontents"><!-- form contents div (the ID is not important) -->
        <input type="hidden" name="_flowExecutionKey" value="${flowExecutionKey}">
        <input type="hidden" name="_flowId" value="${flowRequestContext.activeFlow.id}">
        <input type="hidden" name="dossierLocation" value="${dossier.location}">
        <input type="hidden" name="dossierId" value="${dossier.dossierId}">
        <input type="hidden" name="schoolId" value="${appt.schoolId}">
        <input type="hidden" name="departmentId" value="${appt.deptId}">
        <input type="hidden" name="flowType" value="${decision}">
        <input type="hidden" name="isPrimary" value="${appt.isPrimary}">
        <div class="appointment">
            <strong>${appt.isPrimary?'Primary':'Joint'} Appointment:&nbsp;&nbsp;&nbsp;${appt.appointmentDescription}</strong>
        </div>

        <%-- Get the school:department ID's from the first appointment. The schoolDeptQualifier will be used in the permit tag on the managedossier page--%>
        <c:if test="${apptLoopCount.count == 1}">
           <c:set var="schoolDeptQualifier" value="${appt.schoolId}:${appt.deptId}" scope="request"/>
        </c:if>

    <div class="${appt.isPrimary?'primary':'joint'} ${appt.isComplete?'complete':'inprogress'} sectionhead"
         id="${appt.isPrimary?'Primary':'Joint'}-${apptLoopCount.count}">
      <h2 class="appointmenthead">${appt.isPrimary?'Primary':'Joint'}&nbsp;Appointment&nbsp;processing:&nbsp;
                 &nbsp;${appt.isComplete?'Completed': (currentNodeName == 'PacketRequest' ? 'Unsatisfied Packet Request' : 'In Progress') }</h2><%--
     --%><c:if test="${appt.showJointSendBackLink}">
         <c:url var='sendbackLink' value='OpenActions'><%--
                --%><c:param name='_flowExecutionKey' value='${flowExecutionKey}'/><%--
                --%><c:param name='_eventId' value='sendback'/><%--
                --%><c:param name='dossierId' value='${dossier.dossierId}'/><%--
                --%><c:param name='schoolId' value='${appt.schoolId}'/><%--
                --%><c:param name='departmentId' value='${appt.deptId}'/><%--
              --%></c:url>
       <a href="<c:out value='${sendbackLink}' escapeXml='true'/>" class="appointmentlink"
          title="Send Back to Secondary ${dossier.dossierLocation.description}">Send Back to Secondary ${dossier.dossierLocation.description}</a><%--
     --%></c:if><%--
     --%><c:choose><%--
     --%><c:when test="${!empty appt.jointWaitingMsg}"><%--
          --%><span class="waiting">${appt.jointWaitingMsg}</span><%--
     --%></c:when><%--
     --%><c:otherwise><%--
     --%><c:if test="${!appt.isPrimary && appt.isUpdate}">
          <c:url var='nextLink' value='OpenActions'>
            <c:param name='_flowExecutionKey' value='${flowExecutionKey}'/>
            <c:param name='_eventId' value='jointcomplete-${apptLoopCount.count}'/>
            <c:param name='dossierId' value='${dossier.dossierId}'/>
            <c:param name='schoolId' value='${appt.schoolId}'/>
            <c:param name='departmentId' value='${appt.deptId}'/>
            </c:url>
            <!-- Only the below roles may return control of a dossier back to the primary department  -->
            <c:set var="canSendBack" value="false"/>
            <miv:permit roles="DEPT_STAFF|SCHOOL_STAFF|VICE_PROVOST_STAFF|VICE_PROVOST|SYS_ADMIN">
                <c:set var="canSendBack" value="true"/>
            </miv:permit>
          <c:choose>
          <c:when  test="${canSendBack}">
          <a href="<c:out value='${nextLink}' escapeXml='true'/>" class="jointroute"
             title="Dossier Complete/Send Back to the Primary ${dossier.dossierLocation.description}">Dossier Complete/Send Back to the Primary ${dossier.dossierLocation.description}</a>
          </c:when>
          <c:otherwise>
<!-- Don't show anything for right now. -->
<!--          <a class="jointroute"-->
<!--             title="Dossier Complete/Send Back to the Primary ${location}">Dossier Complete/Send Back to the Primary ${location}</a>-->
          </c:otherwise>
          </c:choose>
       </c:if>
     </c:otherwise>
     </c:choose>
<%-- <span>
     &nbsp; isUpdate=${appt.isUpdate}
     &nbsp; isPrimary=${appt.isPrimary}
     &nbsp; isComplete=${appt.isComplete}
     &nbsp; isJointComplete=${appt.isJointComplete}
     </span> --%>
    </div><!-- sectionhead -->

<%--   Display text if there are no actions to be displayed  --%>
<c:if test="${empty appt.actionList && !canSeePrimary}">
    <p>${strings.noaction}</p>
</c:if>
<%--   Replaced this span+p block with just the p above.
<span>
<c:if test="${empty appt.actionList && !canSeePrimary}">
           <p>${strings.noaction}</p><!--    It is illegal to put a paragraph '<p>' inside a '<span>' -->
</c:if>
</span>
<!--  Here, this comes out as either "<span></span>" or "<span><p>..text..</p></span>" but it's illegal in HTML
      for an inline element (like span) to contain a block-level element (like p) regardless of how you style them.
      That is, even if your CSS says "span { display:block; }" and "p { display:inline; }" it doesn't change the
      structuring rules of HTML.
      When you have an illegal HTML construct like this, one or both of two things will happen.
      1. The browser will be thrown into "Quirks Mode" and all bets are off. Good luck figuring out
         what's wrong with your CSS and why the page doesn't look the way you think it should, and
         no matter what you do the CSS adjustments you make aren't having the effect they ought to.
      2. The browser will restructure the DOM into something valid. In this example, what usually
         happens is a close tag is inferred.  The browser sees the opening span, then the opening 'p',
         but that's illegal so it closes the span THEN opens the 'p'.  It will later find a closing
         span tag with no open span, so it will infer an opening span tag...   so this:
             <span>
                <p>...stuff...</p>
             </span>
         is transformed into this:
             <span></span>
                <p>...stuff...</p>
             <span></span>
    -->
--%>
<%--   Now the list of actions for this appointment  --%>
 
  <ul class="dossieractions" id="${appt.isPrimary?'Primary':'Joint'}${apptLoopCount.count}-actions"><%--
<%-- Start of the ACTIONs Loop : a list item is created for each of the actions in the action list.--%><%--
--%><c:forEach var="action" items="${appt.actionList}">
        <c:set var="attributeaction" value="${action.actionType}" />
    <li class="actions ${action.actionType} ${action.attributeName}" data-status="${action.attributeStatus}" id="P${action.attributeId}"><%--
--%><c:set var="colorclass" value="${(action.isMissing && action.isRequired) ? 'waiting' : 'ready'}"/>
    <div class="${action.displayProperty}">
     <div class="attributetype"><%--
 --%><c:choose><%--
 --%><c:when test="${action.attributeId=='18' && appt.isUpdate}"><%-- this is the "review" attribute so display a link
     --%><c:url var='assignReviwersLink' value='AssignReviewers'>
             <c:param name='_flowExecutionKey' value='${flowExecutionKey}'/>
             <c:param name='_eventId' value='assignReviewers'/>
             <c:param name='dossierId' value='${dossier.dossierId}'/>
             <c:param name='schoolId' value='${appt.schoolId}'/>
             <c:param name='departmentId' value='${appt.deptId}'/>
         </c:url>
        <div class="description">${action.description}&nbsp;(<a href="<c:out value='${assignReviwersLink}' escapeXml='true'/>"
             title="Assign Reviewers">Assign Reviewers</a>)
        </div>
    </c:when><%--
 --%><c:when test="${action.actionType=='HEADER'}"><%-- header
      --%><div class="header">${action.description}</div>
    </c:when><%--
 --%><c:otherwise>
        <div class="description">${action.description}</div>
     </c:otherwise><%--
 --%></c:choose><%--

  --%><c:choose><%--
  --%><c:when test="${appt.isUpdate}">
      <div class="buttons"><%--
      --%><c:choose><%-- how many buttons will we see
      --%><c:when test="${action.updateButtonCount == 0}"><%-- no buttons, do nothing
          </div><%--
      --%></c:when><%--
      --%><c:when test="${action.updateButtonCount == 1}">
          <div class="addeditbutton"><c:set var="whichButton" value="${action.attributeName}_button2"/><!--${whichButton}-->
              <input type="submit" name="_eventId_${action.attributeName}" id="E${action.attributeId}"
                     value="Add/Edit" title="Add/Edit"${action.disableAddButton}>
          </div><%--
      --%></c:when><%--
      --%><c:otherwise ><%--test="${action.updateButtons == 2}"--%>
          <div class="addbutton ${dossier.dossierLocation.description == 'Department' && attributeaction=='DOCUMENTUPLOAD' ?' confirm' : ''}"><!-- add 'confirm' class only at the department --><%--
          --%><c:set var="whichButton" value="${action.attributeName}_button1"/><!--${whichButton}--><%--
          --%><c:set var="button1label" value="${empty labels[whichButton] ? labels.button1 : labels[whichButton]}"/><%--
          --%><c:set var="button1tooltip" value="${empty tooltips[whichButton] ? tooltips.button1 : tooltips[whichButton]}"/>
              <input type="submit" name="_eventId_${action.attributeName}" id="A${action.attributeId}-${apptLoopCount.count}"
                     value="${button1label}" title="${button1tooltip}"${action.disableAddButton}>
          </div><%--
      --%></c:otherwise><%--
      --%></c:choose><%--  end choose how many buttons to display

      --%><c:if test="${fn:length(action.documents)>0}">
          <div class="deletebuttons${action.updateButtonCount == 1?' addedit':''}"><%--
      --%>
      <ul id="openaction:${action.attributeId}" class="openactionattrs ${fn:length(action.documents)>1 ? 'miv-sortable' : ''}">
      <c:forEach var="document" items="${action.documents}" varStatus="loopCount">
      <c:set var="openactionattrdata" value="id='P${document.recordId}'"/><%--
        --%><c:choose><%--
              --%><c:when test="${action.updateButtonCount == 1}">
            <li ${document.recordId !=0 ? openactionattrdata: ''} rel="openaction:${action.attributeId}">
                <span class="pdflink">
                    <a href="<c:out value='${document.fileUrl}' escapeXml='true'/>"
                       title="${document.fileDescription}">View: ${document.fileLabel}</a>
                </span><!-- signable ${document.signable} signed ${document.signed} missing ${action.isMissing} value ${action.value} --><%--
            --%><c:if test="${document.signable}"><%--
                --%><span class="${colorclass} lastmsg">${action.value}</span><%--
            --%></c:if>

                <%-- help tool-tip if document is signed, but invalid --%>
                <c:if test="${document.signatureStatus == 'INVALID'}">
                    <span class="help"><spring:message text="${strings.invalidSignature}"
                                                arguments="${document.documentType.description},${document.documentType.allowedSigner.description}"/></span>
                    <span class="helpicon"><img src="<c:url value='/images/help.png'/>" alt="Help for invalid decisions"></span>
                </c:if>
            </li>
                    <%--
             --%></c:when><%--
             --%><c:otherwise>
                <li ${document.recordId !=0 ? openactionattrdata: ''} rel="openaction:${action.attributeId}">
                     <div class="deleteline">
                       <div class="deletebutton ${dossier.dossierLocation.description == 'Department' && attributeaction=='DOCUMENTUPLOAD'?' confirm' : ''}"> <!-- add 'confirm' class only at the department -->
                        <c:set var="whichButton" value="${action.attributeName}_button2"/><!--${whichButton}-->
                        <c:set var="button2label" value="${empty labels[whichButton] ? labels.button2 : labels[whichButton]}"/><%--
                    --%><c:set var="button2tooltip" value="${empty tooltips[whichButton] ? tooltips.button2 : tooltips[whichButton]}"/><%--
                     --%><c:if test="${loopCount.count == 1}"><%--
                       --%><input type="submit" name="_eventId_${action.attributeName}" id="D${action.attributeId}-${loopCount.count}"
                               value="${button2label}" title="${button2tooltip}"${action.disableCloseButton}><%--
                     --%></c:if><%--
                     --%><c:if test="${loopCount.count != 1}">
                          <input type="submit" name="_eventId_${action.attributeName}-${loopCount.count}" id="D${action.attributeId}-${loopCount.count}"
                                 value="${button2label}" title="${button2tooltip}"${action.disableCloseButton}><%--
                     --%></c:if>
                       </div><!-- end deletebutton -->
                       <span class="pdflink">
                         <a href="<c:out value='${document.fileUrl}' escapeXml='true'/>"
                            title="${document.fileDescription}">View: ${document.fileLabel}</a>
                       </span><%--
                   --%><c:if test="${document.redactable}">
                       <span class="redactionstatus">(${document.redacted?'Redacted':'Non-Redacted'})</span><%--
                   --%></c:if>
                     </div>
                  </li><!--  end deleteline --><%--
             --%></c:otherwise><%--
         --%></c:choose><%--
      --%></c:forEach>
      </ul>
          </div><!-- end div class="deletebuttons" -->
          </c:if><%-- end if test="${fn:length(action.documents)>0}"
      --%><c:if test="${fn:length(action.documents) == 0}"><%-- No document yet --%>
          <div class="deletebuttons${action.updateButtonCount == 1?' addedit':''}"><%--
          --%><c:if test="${action.updateButtonCount == 2}">
              <div class="deleteline deletebutton"><%--
          --%><c:set var="whichButton" value="${action.attributeName}_button2"/><!--${whichButton}--><%--
          --%><c:set var="button2label" value="${empty labels[whichButton] ? labels.button2 : labels[whichButton]}"/><%--
          --%><c:set var="button2tooltip" value="${empty tooltips[whichButton] ? tooltips.button2 : tooltips[whichButton]}"/>
                  <input type="submit" name="_eventId_${action.attributeName}" id="D${action.attributeId}-${apptLoopCount.count}"
                         value="${button2label}" title="${button2tooltip}"${action.disableCloseButton}>
              </div><%--
          --%></c:if><%--
              added below for submitted packet requests, show "view dossier as one pdf" link  --%>
              <c:choose>
              <c:when test="${action.attributeName == 'packet_request' && action.value == 'Closed'}">
              	<span class="pdflink">
                     <a href="${dossierpdf}" title="View the Dossier as One PDF File">View the Dossier as One PDF File</a>
                </span>
              </c:when>
              <c:otherwise>
              <span class="${colorclass} pdflink">${action.value}</span>
              </c:otherwise>
              </c:choose>
          </div><!-- end deletebuttons -->
          </c:if>
      </div><!-- end div class="buttons" -->
      </c:when><%--
  --%><c:otherwise><%--
  --%><span class="${colorclass}">${action.value}</span><%--
  --%><c:if test="${fn:length(action.documents)>0 && canSeePrimary && !displayOnly && appt.showJointSendBackLink}"><%--
      --%><c:forEach var="document" items="${action.documents}" varStatus="loopCount"><%--
          --%><span class="pdflinkviewonly">
                 <a href="<c:out value='${document.fileUrl}' escapeXml='true'/>"
                    title="${document.fileDescription}">View: ${document.fileLabel}</a>
              </span>
    <!-- signable ${document.signable} signed ${document.signed} missing ${action.isMissing} value ${action.value} --><%--
      --%></c:forEach><%--
  --%></c:if><%--
  --%></c:otherwise><%--
  --%></c:choose><%--
 --%></div>
 </div>
    </li>

</c:forEach><%-- End of the Action Loop --%>
    </ul>
    </div><!-- form contents div -->
    </form:form>
    </c:forEach><%-- END OF THE Appointment LOOP --%>
    </div><%-- end of id="appointments" div --%>
    </c:if><%-- !empty primaryAppointment --%>
</c:if><%--test="${empty viewStatusError}" --%>
