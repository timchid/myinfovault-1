<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%><%--
--%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%--
--%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %><%--
--%><%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%><%--
<%----%><!DOCTYPE html>
<html>
    <head>
        <c:set var="pagetitle" value="Cancel Actions: Confirmation"/>
        <title>MIV &ndash; ${pagetitle}</title>
        <script type="text/javascript">
           var mivConfig = new Object();
           mivConfig.isIE = false;
        </script>
        <%@ include file="/jsp/metatags.html" %>
        
        <link rel="stylesheet" type="text/css" href="<c:url value='/css/mivcommon.css'/>">
        <link rel="stylesheet" type="text/css" href="<c:url value='/css/mivEnterData.css'/>">
        <link rel="stylesheet" type="text/css" href="<c:url value='/css/miv-forms.css'/>">
		<link rel="stylesheet" type="text/css" href="<c:url value='/css/postauditreview.css'/>">
		
        <!--[if lt IE 8]>
            <link rel="stylesheet" type="text/css" href="<c:url value='/css/iehacks.css'/>" />
            <script type="text/javascript">mivConfig.isIE=true;</script>
        <![endif]-->

        <c:set var="user" scope="request" value="${MIVSESSION.user}"/>
    </head>

    <body>
        <jsp:include page="/jsp/mivheader.jsp" />

    <!-- Breadcrumbs Div -->
 <div id="breadcrumbs">
      <a href="<c:url value='/MIVMain'/>" title="Home">Home</a>
          &gt; <a href="<c:url value='${context.request.contextPath}'>
            <c:param name='_flowId' value='${flowRequestContext.activeFlow.id}'/>
            <c:param name="_flowExecutionKey" value="${flowExecutionKey}"/>
            <c:param name='_eventId' value='root'/>
            </c:url>" title="Cancel Action: Search">Cancel Action: Search</a>
          &gt; <a href="<c:url value='${context.request.contextPath}'>
            <c:param name='_flowId' value='${flowRequestContext.activeFlow.id}'/>
            <c:param name="_flowExecutionKey" value="${flowExecutionKey}"/>
            <c:param name='_eventId' value='back'/>
            </c:url>" title="Cancel Action: Search">Cancel Action: Search Results</a>
         &gt; ${pagetitle}
    </div><!-- breadcrumbs -->

        <!-- MIV Main Div -->
        <div id="main">
            <h1> ${pagetitle}</h1>
            <form:form name="confirmDossierCancelation" cssClass="mivdata" method="post" commandName="CancelDossierForm" >
				<c:if test="${empty canceledDossiersMap}">
					<div>No actions have been selected to cancel.</div>
				</c:if>
	
				<c:if test="${!empty canceledDossiersMap}">
					<c:set var="dossierCount" scope="page" value="${fn:length(canceledDossiersMap)}" />
	
					<div>The following ${dossierCount>1?'actions have':'action has'} been canceled.</div>
					<br>
					<div id="listarea" class="verifydossiers">
						<div class="dossiercount">Total&nbsp;Action${dossierCount>1?'s':''}&nbsp;=&nbsp;${dossierCount}</div>
						<ul class="itemlist">
							<c:forEach var="dossier" items="${canceledDossiersMap}"
								varStatus="rowIndex">
								<li class="records ${(rowIndex.count)%2==0?'even':'odd'}">
									<strong>${dossier.key}:</strong>${dossier.value}
								</li>
							</c:forEach>
						</ul>
					</div>
				</c:if>	
		</form:form>
        </div><!-- main -->

        <jsp:include page="/jsp/miv_small_footer.jsp" />
    </body>
</html>
