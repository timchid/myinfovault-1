<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%><%--
--%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%--
--%><%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%><%--
--%><%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %><%--
--%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %><%--
--%><%@ taglib prefix="jwr" uri="http://jawr.net/tags" %><%--
--%><!DOCTYPE html>
<html>
    <head>
        <title>MIV &ndash; ${strings.pageTitle}</title>

        <%@ include file="/jsp/metatags.html" %>

        <link rel="stylesheet" type="text/css" href="<c:url value='/css/mivcommon.css'/>">
        <link rel="stylesheet" type="text/css" href="<c:url value='/css/mivEnterData.css'/>">
        <link rel="stylesheet" type="text/css" href="<c:url value='/css/miv-forms.css'/>">
        <link rel="stylesheet" type="text/css" href="<c:url value='/css/jquery.dataTables.min.css'/>">
        <link rel="stylesheet" type="text/css" href="<c:url value='/css/widget-datatables-serverside.css'/>">
        <link rel="stylesheet" type="text/css" href="<c:url value='/css/useradd.css'/>">

        <script type="text/javascript" src="<c:url value='/js/mivcommon.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/jquery.dataTables.min.js'/>"></script>

        <jwr:script src="/bundles/mivwidgets.js" />
        <jwr:script src="/bundles/mivvalidation.js" />
        <jwr:script src="/bundles/adduser.js" />


        <script type="text/javascript">
            var editorAssignedRoles = [
                <c:forEach var="role" items="${MIVSESSION.user.targetUserInfo.person.assignedRoles}">
                {
                    "scope" : "${role.scope}",
                    "role" : "${role.role}"
                },<%--
            --%></c:forEach>
            ];
        </script>
    </head>

    <body>
        <c:set var="helpPage" scope="request" value="add_edit_user_accounts.html"/>
        <jsp:include page="/jsp/mivheader.jsp" />

        <jsp:include page="/WEB-INF/jsp/breadcrumbs.jsp">
            <jsp:param value="${strings.pageTitle}" name="pageTitle"/>
        </jsp:include>

        <!-- MIV Main Div -->
        <div id="main">
            <h1>${strings.pageTitle}</h1>

            <div class="attention">
                <p><strong>Important:</strong> For new appointments use the
                <a href="<c:url value='/OpenActions'>
                         <c:param name='_flowId' value='initiateAcademicAction-flow'/>
                         </c:url>"
                   title="Start a New Appointment">Start&nbsp;an&nbsp;Appointment</a> form
                &mdash; do <strong><em>not</em></strong> create an MIV account
                if you want to be able to create an appointment packet.
                </p>
            </div>
            <div id="errormessages"></div>

            <jsp:include page="/WEB-INF/jsp/binderrors.jsp" />

            <div class="columns"><%--
                DO NOT REMOVE -- whitespace on either side of ".columns > div" alters the column width percentages
            --%><div>
                    <ul id="summary"></ul>
                </div><%--
                DO NOT REMOVE (see comment above)
            --%><div>
                    <form:form id="manageUsersForm" cssClass="mivdata box" method="post" commandName="form">
                        <input type="hidden" id="userId" name="userId">
                        <fieldset id="accountTypeSelect">
                            <legend>Account Type</legend>

                            <p class="formhelp">${strings.selectAccountType}</p>

                            <div class="formline accountTypeSelector">
                                <c:forEach items="${categories}" var="category">
                                    <form:radiobutton path="accountType"
                                                      value="${category}"
                                                      label="${category.description}" />
                                </c:forEach>
                            </div>
                        </fieldset>

                        <%@ include file="searchldap.jspf" %>

                        <fieldset id="fuzzyMatches">
                            <legend>MIV Appointee Matches</legend>

                            <p class="formhelp">${strings.selectFuzzyMatch}</p>
                        </fieldset>

                        <%@ include file="usereditform.jspf" %>

                        <fieldset id="confirmation">
                            <legend>Confirmation</legend>

                            <p class="formhelp">${strings.confirmation}<span class="appointeeConversionWarning"></span></p>

                            <p>Name: <span id="confirmName"></span></p>
                            <p>MIV Role: <span id="confirmRole"></span></p>
                            <p>Email: <span id="confirmEmail"></span></p>

                            <span class="appointment"></span>s:
                            <ul id="confirmAppointments"></ul>
                        </fieldset>

                        <button type="submit" name="cancel" formnovalidate>Cancel</button>
                    </form:form>
                </div><%--
            DO NOT REMOVE (see comment above)
        --%></div>
        </div><!-- main -->

        <div id="fuzzyDialog">
            <%-- The href for the link below is set in useradd.js, because it changes
                 depending on the person selected in the LDAP selection bit --%>
            <p class="formhelp">If you know this person’s ICA/new appointment action was processed in MIV, and you don’t see them listed as an appointee below, please contact <a>miv-help@ucdavis.edu</a>.</p>
            <fieldset><input type="checkbox" id="dialogCheck" />
            <label for="dialogCheck">
                This candidate’s initial action (ICA or New Appointment) was not processed in MIV.
            </label></fieldset>
        </div>

        <jsp:include page="/jsp/miv_small_footer.jsp" />
    </body>
</html>
