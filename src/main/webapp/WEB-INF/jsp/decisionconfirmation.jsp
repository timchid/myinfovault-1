<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%><%--
--%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%--
--%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %><%--
--%><%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%><%--
--%><%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%><%--
--%><c:set var="decision" value="${form.decision}"/><%--
--%><spring:message var="pageTitle" text="${strings.pageTitle}" arguments="${decision.documentType.description}"/><%--
--%><!DOCTYPE HTML>
<html>
<head>
    <title>MIV &ndash; ${pageTitle}</title>

    <%@ include file="/jsp/metatags.html" %>
  
</head>
<body>
    <jsp:include page="/jsp/mivheader.jsp" />

    <jsp:include page="breadcrumbs.jsp">
        <jsp:param value="${pageTitle}" name="pageTitle"/>
    </jsp:include>

    <div id="main">
        <h1>${pageTitle}</h1>

        <p><spring:message text="${strings.confirmation}" arguments="${decision.documentType.description}"/></p>

        <div><strong>${decision.dossier.candidate.displayName}</strong>
             <a href="<c:out escapeXml='true' value='${form.dossierUrl}'/>"
                title="${decision.dossier.action.description}">${decision.dossier.action.description}</a></div>
             
        <div>${decision.dossier.delegationAuthorityDescription} Action:
             <strong>${fn:toUpperCase(decision.decisionTypeDescription)}</strong>,
             <fmt:formatDate type="both"
                             dateStyle="short"
                             timeStyle="short" 
                             value="${form.signature.signingDate}"/></div>
    </div>

    <jsp:include page="/jsp/miv_small_footer.jsp" />
</body>
</html><%--
vi: se ts=8 sw=2 et:
--%>
