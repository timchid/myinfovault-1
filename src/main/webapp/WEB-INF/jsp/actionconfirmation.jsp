<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%><%--
--%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%--
--%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %><%--
--%><!DOCTYPE html>
<html>

<c:set var="pageTitle" value="${strings.pageTitle}"/>
<c:set var="confirmationStatus" value="${strings.confirmationStatus}"/>

<head>
  <title>MIV &ndash; ${pageTitle}${confirmationStatus}</title>

 <%@ include file="/jsp/metatags.html" %>

  <script type="text/javascript" src="<c:url value='/js/actionconfirmation.js'/>"></script>

  <link rel="stylesheet" type="text/css" href="<c:url value='/css/actionconfirmation.css'/>">

  <link rel="shortcut icon" href="<c:url value='/images/favicon.ico'/>" type="image/x-icon" />
</head>
<body>

<jsp:include page="/jsp/mivheader.jsp" />

<div id="breadcrumbs">
    <a href="<c:url value='/MIVMain'/>" title="Home">Home</a>
    <c:if test="${empty confirmationError}">
    &gt; <a title="${pageTitle}" href="<c:url value='/createAction' />">${pageTitle}</a>
    </c:if>
    &gt; ${pageTitle}${confirmationStatus}
</div><!-- breadcrumbs -->

<div id="main"><!-- MIV Main Div -->

<h1>${pageTitle}${confirmationStatus}</h1>

<%-- Display dossier information if present --%>
<c:if test="${not empty dossierId}">
        <p>
        ${strings.actionInfo}
        <ul>
        <c:forEach var="department" items="${departments}">
          <li>${department}</li>
        </c:forEach>
        </ul>

        <div class="formline"><strong>${strings.candidateLabel}</strong>&nbsp;${candidate}</div>
        <div class="formline"><strong>${strings.actionLabel}</strong>&nbsp;
            ${dossierDescription}&nbsp;(${submitDate})
        </div>

        <div class="buttonset">
            <button type="button"
                    title="${tooltips.goHome}"
                    data-href="<c:url value='/MIVMain'/>">
                ${strings.home}
            </button>
            <button type="button"
                    title="${tooltips.goToAction}"
                    data-href="<c:url value='/OpenActions' />?_flowId=openactions-flow&direct&dossierId=${dossierId}">
                ${strings.goToAction}
            </button>
        </div>
</c:if>
</div>
<!-- main -->

<jsp:include page="/jsp/miv_small_footer.jsp" />

</body>
</html>
