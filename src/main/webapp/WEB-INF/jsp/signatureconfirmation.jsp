<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%><%--
--%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%--
--%><%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%><%--
--%><c:set var="user" scope="request" value="${MIVSESSION.user}"/><%--
--%><!DOCTYPE HTML>
<html>
<head>
    <title>MIV &ndash; ${strings.pageTitle}</title>
    <%@ include file="/jsp/metatags.html" %>
</head>

<body>
    <jsp:include page="/jsp/mivheader.jsp" />

    <div id="breadcrumbs">
        <a href="<c:url value='/MIVMain'/>" title="Home">Home</a>
        
        &gt; <a href="<c:url value='${context.request.contextPath}'>
                      <c:param name='_flowId' value='${flowRequestContext.activeFlow.id}'/>
                      <c:param name='_eventId' value='root'/>
                      <c:param name="_flowExecutionKey" value="${flowExecutionKey}"/>
                      </c:url>" title="${strings.rootTitle}">${strings.rootTitle}</a>
                      
        &gt; ${strings.pageTitle}
    </div>

    <div id="main">
        <h1>${strings.pageTitle}</h1>

        <p>${strings.signConfirm}:</p>

        <strong>${form.signature.document.dossier.candidate.displayName}:</strong>
        <fmt:formatDate type="both"
                        dateStyle="short"
                        timeStyle="short"
                        value="${form.signature.signingDate}"/>
    </div>

    <jsp:include page="/jsp/miv_small_footer.jsp" />
</body>
</html><%--
vi: se ts=8 sw=2 et:
--%>
