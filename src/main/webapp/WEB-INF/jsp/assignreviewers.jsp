<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%><%--
--%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%--
--%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %><%--
--%><%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%><%--
--%><%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%><%--
--%><c:set var="user" scope="request" value="${MIVSESSION.user}"/><%--
--%><c:set var="appointment" value="${form.dossier.appointmentAttributes[form.appointmentKey]}"/><%--
--%><c:set var="isReviewOpen" value="${appointment.attributes.review.attributeValue == 'OPEN'}"/><%--
--%><c:set var="title" value="${isEdit
                              ? strings.editPageTitle
                              : (isConfirmation
                               ? strings.confirmationPageTitle
                               : strings.viewPageTitle)}"/><%--
--%><!DOCTYPE html>
<html>
<head>
    <title>MIV &ndash; ${title}</title>

    <%@ include file="/jsp/metatags.html" %>

    <link rel="stylesheet" type="text/css" href="<c:url value='/css/mivEnterData.css'/>">
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/miv-forms.css'/>">
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/data_table_gui.css'/>">
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/dossierAction.css'/>">
    <link rel="stylesheet" type="text/css" href="<c:url value='/css/assignment.css'/>">

    <!--[if IE 8]>
	<link rel="stylesheet" type="text/css" href="<c:url value='/css/ie8hacks.css'/>">
	<script type="text/javascript">mivConfig.isIE=true;</script>
    <![endif]-->

    <script type="text/javascript" src="<c:url value='/js/jquery.dataTables.min.js'/>"></script>
</head>

<body>
    <c:set var="helpPage" scope="request" value="assign_dossier_reviewers.html"/>
    <c:set var="helpTip" scope="request" value="Help with assigning people and groups to a dossier review"/>
    <jsp:include page="/jsp/mivheader.jsp" />

    <jsp:include page="breadcrumbs.jsp">
        <jsp:param value="${title}" name="pageTitle"/>
    </jsp:include>

    <div id="main">
        <h1>${title}</h1>

        <spring:hasBindErrors name="form">
            <c:set var='errorClass' scope='page' value=' class="haserrors"'/>
        </spring:hasBindErrors>

        <div id="errormessage"${errorClass}>
            <spring:hasBindErrors name="form">
                <c:if test="${errors.errorCount > 0}">
			<div id="errorbox">
		                <strong>${errors.errorCount == 1? "Error:":"Errors:"}</strong>
	                    <ul>
	                        <c:forEach var="errMsgObj" items="${errors.allErrors}">
	                            <li>
	                                <spring:message htmlEscape="false" text="${errMsgObj.defaultMessage}"/>
	                            </li>
	                        </c:forEach>
	                    </ul>
	                </div>
                </c:if>
            </spring:hasBindErrors>
        </div>

        <c:if test="${not isEdit and isConfirmation}">
            <p class="assignmentConfirmation">${strings.confirmationMessage}</p>
        </c:if>

        <div class="assignmentSummary">
            <div>
                <span>${strings.dossierName}</span>
                ${form.owner.displayName}
            </div>
            <div>
                <span>${strings.dossierAppointment}</span>
                ${appointment.departmentDescription ? appointment.departmentDescription : ""}
                ${appointment.departmentDescription ? ", " : ""}${appointment.schoolDescription}
            </div>
            <div>
                <span>${strings.dossierLocation}</span>
                ${form.dossierLocation}
            </div>
            <div>
                <span>${strings.dossierReview}</span>
                ${isReviewOpen ? strings.reviewOpen : strings.reviewClosed}
            </div>
        </div>

        <c:if test="${isEdit}">
            <p>${strings.selectionInstruction}</p>

            <p><strong>${strings.message}</strong></p>

            <p><em>${strings.note}</em></p>
        </c:if>

        <form:form id="assignForm" method="post" commandName="form">
            <jsp:include page="assignment.jsp">
                <jsp:param name="isReview" value="true"/>
                <jsp:param name="canSwitch" value="${canSwitch}"/>
                <jsp:param name="editMembership" value="${isEdit}"/>
                <jsp:param name="editMembers" value="${isEdit}"/>
                <jsp:param name="editGroups" value="${isEdit}"/>
            </jsp:include>

            <%-- use buttonset if available table is shown so that buttons appear under the assigned table --%>
            <fieldset class="${isEdit ? 'buttonset' : 'mivfieldset'}">
                <c:choose>
                    <c:when test="${isEdit}">
                        <button name="_eventId"
                                value="save"
                                title="${strings.saveTitle}">
                            ${strings.saveLabel}
                        </button>

                        <button name="_eventId"
                                value="open"
                                title="${isReviewOpen
                                       ? strings.saveOpenAlreadyTitle
                                       : strings.saveOpenTitle}"
                                ${isReviewOpen ? 'disabled="true"' : ''}>
                            ${strings.saveOpenLabel}
                        </button>
                    </c:when>

                    <c:when test="${canSwitch}">
                        <button name="_eventId"
                                value="edit"
                                title="${strings.editTitle}">
                            ${strings.editLabel}
                        </button>

                        <button name="reviewOpen"
                                value="${not isReviewOpen}"
                                title="${isReviewOpen
                                       ? strings.reviewCloseTitle
                                       : strings.reviewOpenTitle}">
                            ${isReviewOpen
                            ? strings.reviewCloseLabel
                            : strings.reviewOpenLabel}
                        </button>
                    </c:when>
                </c:choose>

                <button name="_eventId"
                        value="breadcrumb1"
                        title="${strings.backTitle}">
                    ${isEdit
                    ? strings.cancelLabel
                    : strings.doneLabel}
                </button>
            </fieldset>
        </form:form>
    </div><!--main-->

    <jsp:include page="/jsp/miv_small_footer.jsp" />
</body>
</html>
