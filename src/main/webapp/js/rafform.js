'use strict';
/* global dojo, trim, isNumber, validateForm, stripCurrency,
   formatAsMoneyFromString, getClickedButton */
/**
 * validate raf form
 */

var mivFormProfile = {
    required: [ 'yearsAtRank', 'yearsAtStep' ],
    trim: ['yearsAtRank', 'yearsAtStep'],
    constraints: {
        appointmentType9mo: function(contents) {
            if (!dojo.byId('appointmentType9mo').checked &&
                !dojo.byId('appointmentType11mo').checked) {
                return 'REQUIRED';
            }

            return true;
        },
        accelerationYears: function(contents) {
            if (dojo.byId('accelerationAccel').checked) {
                var accelerationYear = dojo.byId('accelerationYears').value;

                if (trim(accelerationYear) === '') {
                    return 'REQUIRED_ACCEL';
                }
                else if (isNaN(accelerationYear)) {
                    return 'INVALID_YEARS';
                }
                else if (accelerationYear < 0 || accelerationYear > 99) {
                    return 'INVALID_YEARS_RANGE';
                }
            }

            return true;
        },
        decelerationYears: function(contents) {
            if (dojo.byId('accelerationDecel').checked) {
                var decelerationYear = dojo.byId('decelerationYears').value;

                if (trim(decelerationYear) === '') {
                    return 'REQUIRED_DECEL';
                }
                else if (isNaN(decelerationYear)) {
                    return 'INVALID_YEARS';
                }
                else if (decelerationYear < 0 || decelerationYear > 99) {
                    return 'INVALID_YEARS_RANGE';
                }
            }

            return true;
        },
        yearsAtRank: function(contents) {

            if (isNaN(contents)) {
                return 'INVALID_YEARS';
            }
            else if (contents < 0 || contents > 99) {
                return 'INVALID_RANK_YEAR_RANGE';
            }
            return true;
        },
        yearsAtStep: function(contents) {

            if (isNaN(contents)) {
                return 'INVALID_STEP';
            }
            else if (contents < 0 || contents > 99) {
                return 'INVALID_STEP_RANGE';
            }

            return true;
        },
        Present1RankAndStep: function(contents) {
            return validateStatus(contents, 1, 'Present');
        },
        Present2RankAndStep: function(contents) {
            return validateStatus(contents, 2, 'Present');
        },
        Present3RankAndStep: function(contents) {
            return validateStatus(contents, 3, 'Present');
        },
        Present4RankAndStep: function(contents) {
            return validateStatus(contents, 4, 'Present');
        },
        Present5RankAndStep: function(contents) {
            return validateStatus(contents, 5, 'Present');
        },
        Proposed1RankAndStep: function(contents) {
            return validateStatus(contents, 1, 'Proposed');
        },
        Proposed2RankAndStep: function(contents) {
            return validateStatus(contents, 2, 'Proposed');
        },
        Proposed3RankAndStep: function(contents) {
            return validateStatus(contents, 3, 'Proposed');
        },
        Proposed4RankAndStep: function(contents) {
            return validateStatus(contents, 4, 'Proposed');
        },
        Proposed5RankAndStep: function(contents) {
            return validateStatus(contents, 5, 'Proposed');
        }
    },
    errormap: { 'REQUIRED_ACCEL': 'Acceleration Years are required.',
                'REQUIRED_DECEL': 'Deceleration Years are required.',
                'INVALID_RANK_YEAR_RANGE':'Years at Rank must be from 0 to 99.',
                'INVALID_YEARS_RANGE':'Years must be from 0 to 99.',
                'INVALID_YEARS':'Years must be numeric.',
                'INVALID_PERCENT_TIME':'% of Time must be numeric or three characters: WOS.',
                'INVALID_PERCENT_TIME_RANGE':'% of Time must be from 0 to 100, or three characters: WOS.',
                'REQUIRED_PERCENT_TIME':'% of Time is required.',
                'STATUS_REQUIRED':'At least one status entry is required.',
                'STATUS_ALL_REQUIRED':'All fields are required for status entry.',
                'INVALID_TITLE_CODE':'Title Code must be numeric.',
                'INVALID_MONTHLY_SALARY':'Monthly salary must be numeric from 0 to 9,999,999.99',
                'INVALID_ANNUAL_SALARY':'Annual salary must be numeric from 0 to 9,999,999.99',
                'INVALID_STEP':'Step must be numeric.',
                'INVALID_STEP_RANGE':'Step must be from 0 to 99.'}
};

$(document).ready(function() {
    $('.datepicker').datepicker({
        dateFormat : 'MM d, yy',
        changeMonth: true,
        changeYear: true,
        showButtonPanel: true,
        showOtherMonths: true,
        selectOtherMonths: true,
        constrainInput : false,
        yearRange: '1900:c+2',
        currentText: 'Find Today',
        closeText: 'Close',
        buttonImage: '/miv/images/buttons/datepicker.png',
        buttonImageOnly: true,
        showOn: 'both'
    });

    // end date range: last year to fifty years from now
    $('.datepicker[name$=endDate]').datepicker('option', 'yearRange', 'c-1:c+50');

    $('img[class="ui-datepicker-trigger"]').each(function() {
        $(this).attr('title', 'Click to open a datepicker. format is (MM d, yy)');
    });

    var $form = $('#rafForm');
    $form.submit(function(event) {
        var $clickedButton = getClickedButton($(this));

        if ($clickedButton.prop('id') !== 'cancel')
        {
            var $errormessage = $('#messagebox');

            if (typeof mivFormProfile !== 'undefined') {
                var formValidation = mivFormProfile;

                //adding department percent of time in required and trim set
                var percents = dojo.query('div#departmentBlock input[id^=departmentPercentOfTime]');

                for (var i=0; i < percents.length; i++) {
                    if (dojo.indexOf(formValidation.required,percents[i].id) === -1) {
                        formValidation.required.push(percents[i].id);
                        formValidation.trim.push(percents[i].id);
                    }

                    if (dojo.indexOf(formValidation.constraints,percents[i].id) === -1) {
                        formValidation.constraints[percents[i].id] = validatePercentOfTime;
                    }
                }

                //adding split appointments constraints
                var splitDepartmentPercentOfTime = dojo.query('div#splitDepartmentBlock input[id^=splitDepartmentPercentOfTime]');

                var splitDeptPercent = function(contents) {
                    return validateSplitDepartmentPercentOfTime(contents, i);
                };

                for (i=0; i < splitDepartmentPercentOfTime.length; i++) {
                    if (dojo.indexOf(formValidation.constraints,splitDepartmentPercentOfTime[i].id) === -1) {
                        formValidation.constraints[splitDepartmentPercentOfTime[i].id] = splitDeptPercent;
                    }
                }

                var splitDepartmentName = dojo.query('div#splitDepartmentBlock input[id^=splitDepartmentName]');

                for (i=0; i < splitDepartmentName.length; i++) {
                    if (dojo.indexOf(formValidation.constraints, splitDepartmentName[i].id) === -1) {
                        formValidation.constraints[splitDepartmentName[i].id] = splitDeptPercent;
                    }
                }

                if (!validateForm($form, formValidation, $errormessage)) {
                    //enable($clickedButton);
                    event.preventDefault();
                }

            }
        }
    });

    //disable/enable acceleration/deceleration years if corresponding radio is not selected
    $('input[name=raf\\.acceleration]').change(function(event) {
        var accelerationType = $(event.currentTarget).val();

        $('input#decelerationYears').prop('disabled',
                                          accelerationType === 'NORMAL' ||
                                          accelerationType === 'ACCELERATION');

        $('input#accelerationYears').prop('disabled',
                                          accelerationType === 'NORMAL' ||
                                          accelerationType === 'DECELERATION');
    }).filter(':checked').trigger('change');//trigger the change for the initially selected

    $('input[id$="TitleCode"]').focusout(function (event) {// to get 4 digit Title code
/*
        var returnValue = trim(event.currentTarget.value);

        if (trim(returnValue) != '' && // returnValue was already trim()med, no need to do it again
            !isNaN(returnValue)) {
            var numberOfDigits = 4;
            var returnValue = trim(event.currentTarget.value); // we alredy got this, why declaring and getting (and trimming!) it again???

            for (var i=0; i < numberOfDigits - trim(event.currentTarget.value).length; i++) { // getting and trimming Again and Again?!?
                returnValue = "0"+returnValue;
            }


            if (returnValue != '' && isNumber(returnValue))
            {
                returnValue = ('0000' + returnValue).slice(-4);
            }
            event.currentTarget.value = returnValue;
        }
*/
        var returnValue = event.currentTarget.value.trim();
        if (returnValue !== '' && isNumber(returnValue))
        {
            returnValue = ('0000' + returnValue).slice(-4);
        }
        event.currentTarget.value = returnValue;
    });

    var fnFormatAsMoneyHandler = function (event) {
        fnFormatAsMoney(0, event.currentTarget);
    };

    var fnFormatAsMoney = function (index, element) {
        var upperlimit = 999999999999999.00;

        var useDecimal = element.value.indexOf('.') !== -1;

        var nStr = stripCurrency(element.value);
        element.value = formatAsMoneyFromString(nStr, upperlimit, useDecimal);
    };

    var fnStripCurrencyFromMoneyHandler = function (event) {
        event.currentTarget.value = stripCurrency(event.currentTarget.value);
    };

    $('input[id$="Salary"]').focusin(fnStripCurrencyFromMoneyHandler).focusout(fnFormatAsMoneyHandler).each(fnFormatAsMoney);
});


function validatePercentOfTime(value) {
    if (value.toUpperCase() === 'WOS') {
        return true;
    }

    if (trim(value)==='') {
        return 'REQUIRED_PERCENT_TIME';
    }
    else if (isNaN(value)) {
        return 'INVALID_PERCENT_TIME';
    }
    else if (value < 0 ||
             value > 100) {
        return 'INVALID_PERCENT_TIME_RANGE';
    }

    return true;
}


function validateSplitDepartmentPercentOfTime(value, i) {
    return dojo.byId('splitDepartmentName' + i) === undefined ||
           dojo.byId('splitDepartmentName' + i) === null ||
           trim(dojo.byId('splitDepartmentName' + i).value) === '' ||
           value === 'WOS' ||
           validatePercentOfTime(value);
}

function validateStatus(value,i,statustype) {
    var RankAndStep = dojo.byId(statustype + i + 'RankAndStep');
    var PercentOfTime = dojo.byId(statustype + i + 'PercentOfTime');
    var TitleCode = dojo.byId(statustype + i + 'TitleCode');
    var MonthlySalary = dojo.byId(statustype + i + 'MonthlySalary');
    var AnnualSalary = dojo.byId(statustype + i + 'AnnualSalary');

    if (RankAndStep.value !== '' ||
        PercentOfTime.value !== '' ||
        TitleCode.value !== '' ||
        (MonthlySalary.value !== '' && MonthlySalary.value !== 0) ||
        (AnnualSalary.value !== '' && AnnualSalary.value !== 0) ) {
        // Check for all fields
        if (RankAndStep.value === '' ||
            PercentOfTime.value === '' ||
            TitleCode.value === '' ||
            MonthlySalary.value === '' ||
            AnnualSalary.value === '' ) {
            return 'STATUS_ALL_REQUIRED';
        }

        // check for % of Time.
        if (PercentOfTime.value !== 'WOS') {
            if (isNaN(PercentOfTime.value)) {
                return 'INVALID_PERCENT_TIME';
            }
            else if (PercentOfTime.value < 0 || PercentOfTime.value > 100) {
                return 'INVALID_PERCENT_TIME_RANGE';
            }
        }

        // Title Code must be number and 4 digits max.
        if (isNaN(TitleCode.value)) {
            return 'INVALID_TITLE_CODE';
        }

        if (!isNumber(stripCurrency(MonthlySalary.value)) ||
            parseFloat(stripCurrency(MonthlySalary.value)) > 9999999.99 ) {
            return 'INVALID_MONTHLY_SALARY';
        }

        if (!isNumber(stripCurrency(AnnualSalary.value)) ||
            parseFloat(stripCurrency(AnnualSalary.value)) > 9999999.99 ) {
            return 'INVALID_ANNUAL_SALARY';
        }
    }

    if (i === 1) {
        var statusRowCount=0;

        for (var index=1; index<=5; index++) {
            RankAndStep = dojo.byId(statustype + i + 'RankAndStep');
            PercentOfTime = dojo.byId(statustype + i + 'PercentOfTime');
            TitleCode = dojo.byId(statustype + i + 'TitleCode');
            MonthlySalary = dojo.byId(statustype + i + 'MonthlySalary');
            AnnualSalary = dojo.byId(statustype + i + 'AnnualSalary');

            if (RankAndStep.value !== '' ||
                PercentOfTime.value !== '' ||
                TitleCode.value !== '' ||
                MonthlySalary.value !== '' ||
                AnnualSalary.value !== '' ) {
                statusRowCount++;
            }
        }

        if (statusRowCount === 0) {
            return 'STATUS_REQUIRED';
        }
    }

    return true;
}

/*<span class="vim">
vim:ts=8 sw=4 et sm:
</span>*/
