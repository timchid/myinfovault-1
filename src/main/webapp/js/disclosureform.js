'use strict';
/* global getElementById, getClickedButton, validateForm */
/**
 * Validate Disclosure Certificate Form.
 *
 * @author Pradeep Haldiya
 * @since MIV 4.7.1
 */

 var mivFormProfile;

$(document).ready(function() {

    // 'changesAdditions' field required for signature re-request
    if (getElementById('dc.changesAdditions').size() > 0)
    {
        mivFormProfile = {
            required: [ 'dc.changesAdditions' ]
        };
    }

    var $form = $('#dc');
    $form.submit(function(event) {
        var $clickedButton = getClickedButton($(this));

        if( $clickedButton.prop('id') !== 'cancel')
        {
            var $errormessage = $('#errormessage');
            if (typeof mivFormProfile !== 'undefined') {
                //disable($clickedButton);
                if (!validateForm($form, mivFormProfile, $errormessage)) {
                    //enable($clickedButton);
                    event.preventDefault();
                }
                /*else
                {
                    //addClickedButtonToForm($form, $clickedButton);
                }*/
            }
        }
    });
});

/*
 vim:ts=8 sw=4 noet sr:
*/
