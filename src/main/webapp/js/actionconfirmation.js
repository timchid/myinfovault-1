'use strict';

$(document).ready(function() {
    $('button').click(function(e) {
        var href = $(e.target).data('href');

        if (href !== undefined) {
            window.location.href = href;
        }
    });
});
