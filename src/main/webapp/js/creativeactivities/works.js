
$(document).ready(function() {

    $('#association').addClass('hidden');

    if ($('#usepopups').val() == 'false' /*&& typeof jQuery.ui !== 'undefined'*/ ) {

        /*var selectedids = '<input type="hidden" id="selectedids" name="selectedids" value="">';

        $('#display').after(selectedids);*/

        // a workaround for a flaw in the demo system (http://dev.jqueryui.com/ticket/4375), ignore!
        $('#dialog:ui-dialog').dialog('destroy');

        tips = $('.validateTips');

        $('#newevent').button().click(function() {
            var innerHTMLData = '<div id="tipsbox">' +
                                    '<p class="validateTips">* = Required Field</p>' +
                                '</div>' +
                                /*'<fieldset style="width: 94%; height: 94%;">'+*/
                                '<fieldset>' +
                                    '<iframe id="addelement" name="addelement" scrolling="auto" height="433px" width="680px" src="/miv/EditRecord?rectype=events&pubtype=95&documentid=&sectionname=&subtype=&E0000=Add+a+New+Record&usepopups=true" frameborder="0" scrolling="yes">' +
                                    '</iframe>' +
                                '</fieldset>';

            $('#dialog-form').html(innerHTMLData);

            $('#dialog-form').dialog('open');
        });
        
        $('#dialog-form').dialog({
            autoOpen: false,
            height: 565,
            width: 710,
            modal: true,
            buttons: {
                'Save': function() {
                    removeErrorDiv();
                    var dialogContents = $('#addelement').contents();
                    var eventstartdate = dialogContents.find('#eventstartdate');
                    var eventenddate = dialogContents.find('#eventenddate');
                    var eventtypeid = dialogContents.find('#eventtypeid');
                    var statusid = dialogContents.find('#statusid');
                    var venue = dialogContents.find('#venue');
                    var venuedescription = dialogContents.find('#venuedescription');
                    var eventdescription = dialogContents.find('#eventdescription');
                    var country = dialogContents.find('#country');
                    var province = dialogContents.find('#province');
                    //var city = $('#city').contents().find('#city');
                    var city = dialogContents.find('#city');
                    var url = dialogContents.find('#url');

                    var allFields = $([]).add(eventstartdate).add(eventenddate).add(eventtypeid).add(statusid).add(venue)
                                .add(venuedescription).add(eventdescription).add(country).add(province).add(city).add(url);

                    var bValid = true;
                    allFields.removeClass('errorfield');

                    if (!checkRequired(eventstartdate)) bValid = false;

                    if (eventtypeid.val() == 0) {
                        eventtypeid.addClass( 'errorfield' );
                        updateTips( eventtypeid, 'This field is required.' );
                        bValid = false;
                    }

//                    if (!checkRequired(eventdescription)) bValid = false; // MIV-4395 'eventdescription' no longer required

//                    if (!checkRequired(venue)) bValid = false; // MIV-4396 'venue' no longer required

                    if (url.val().replace(/(^\s*|\s*$)/i, '').length > 0
                        && !checkRegexp(url, /^(https?|ftp):\/\//, 'Invalid url string.')) {
                        bValid = false;
                    }

                    if (!bValid) {
                        focusFirstError(allFields);
                    }
                    else {
                        var $from = $('#addelement').contents().find('form');
                        var action = $from.attr('action');
                        var formdata = $from.serialize();

                        /* Send the data using post and put the results in a div */
                        $.post(action, formdata, function(data) {
                            var resultObj = eval('(' + data + ')');
                            var recid = resultObj.recnum;
                            var result = resultObj.success;
                            if (resultObj != undefined) {
                                if (result != undefined && result)  {
                                    $('#associationframe').attr('src', $('#associationframe').attr('src'));
                                    $('#dialog-form').dialog('close');
                                    showMessage(null,"<div id=\"successbox\">"+resultObj.message+"</div>",$("#associationframe").position().top);
                                }
                                else {
                                    showResponseError(resultObj.errorfields,resultObj.message,allFields);
                                }
                            }
                        });
                    }
                },
                Cancel: function() {
                    $(this).dialog('close');
                }
            },
            close: function() {
                $('#dialog-form').html('');
            }
        });

        $('#newpublicationevent').button().click(function() {
            var innerHTMLData = '<div id="tipsbox">' +
                                    '<p class="validateTips">* = Required Field</p>' +
                                '</div>' +
                                /*'<fieldset style="width: 94%; height: 94%;">'+*/
                                '<fieldset>' +
                                    '<iframe id="addelement" name="addelement" scrolling="auto" height="420px" width="680px" src="/miv/EditRecord?rectype=publicationevents&E0000=Add+a+New+Record&usepopups=true" frameborder="0" scrolling="yes">' +
                                    '</iframe>' +
                                '</fieldset>';

            $('#dialog-pubform').html(innerHTMLData);

            $('#dialog-pubform').dialog('open');
        });

        $('#dialog-pubform').dialog({
            autoOpen: false,
            height: 550,
            width: 710,
            modal: true,
            buttons: {
                'Save': function() {
                    removeErrorDiv();
                    var year = $('#addelement').contents().find('#year');
                    var statusid = $('#addelement').contents().find('#statusid');
                    var title = $('#addelement').contents().find('#title');
                    var publication = $('#addelement').contents().find('#publication');
                    var creators = $('#addelement').contents().find("#creators");
                    var editors = $('#addelement').contents().find('#editors');
                    var publisher = $('#addelement').contents().find('#publisher');
                    var volume = $('#addelement').contents().find('#volume');
                    var issue = $('#addelement').contents().find('#issue');
                    var pages = $('#addelement').contents().find('#pages');
                    var country = $('#addelement').contents().find('#country');
                    var province = $('#addelement').contents().find('#province');
                    var city = $('#addelement').contents().find('#city');
                    var url = $('#addelement').contents().find('#url');

                    var allFields = $([]).add(year).add(statusid).add(title).add(publication).add(editors).add(publisher)
                                         .add(volume).add(issue).add(pages).add(country).add(province).add(city).add(url);

                    var bValid = true;

                    allFields.removeClass('errorfield');

                    if (!checkRequired(year)) bValid = false;

                    if (bValid && !checkRegexp(year, /\d{4}/i, 'Year must be 4 digit number.')) bValid = false;

                    if (bValid && !isNaN(year.val())) {
                        var numyear = year.val() * 1;
                        var curYear = new Date().getFullYear();

                        if (numyear < 1900 || numyear > (curYear +2)) {
                            year.addClass('errorfield');
                            updateTips(year, 'Year must be from 1900 to '+(parseInt($('#thisYear').val())+2)+'.');
                            bValid = false;
                        }
                    }

                    if (!checkRequired(title)) bValid = false;

                    //if (!checkRequired(publication)) bValid = false;

                    if (!checkRequired(creators)) bValid = false;

                    if (url.val().replace(/(^\s*|\s*$)/i, '').length > 0
                        && !checkRegexp(url, /^(https?|ftp):\/\//, 'Invalid url string.')) {
                        bValid = false;
                    }

                    if (!bValid) {
                        focusFirstError(allFields);
                    }
                    else {
                        var $from = $('#addelement').contents().find('form');
                        var action = $from.attr('action');
                        var formdata = $from.serialize();

                        /* Send the data using post and put the results in a div */
                        $.post(action, formdata, function(data) {
                            var resultObj = eval('(' + data + ')');
                            var recid = resultObj.recnum;
                            var result = resultObj.success;

                            if (resultObj != undefined) {
                                if (result != undefined && result) {
                                    $('#associationframe').attr('src', $('#associationframe').attr('src'));
                                    
                                    $('#dialog-pubform').dialog('close');
                                    showMessage(null,"<div id=\"successbox\">"+resultObj.message+"</div>",$("#associationframe").position().top);
                                }
                                else {
                                    showResponseError(resultObj.errorfields,resultObj.message,allFields);
                                }
                            }
                        });
                    }
                },
                Cancel: function() {
                    $(this).dialog('close');
                }
            },
            close: function() {
                $('#dialog-pubform').html('');
            }
        });
/*
        // adding fields for suggestion
        var txtworktypeid = '<input type="text" id="txtworktypeid" name="txtworktypeid" value="" maxlength="50">';
        $('#worktypeid').after(txtworktypeid);

        //var txtworktypeerrorlabel = '<label for="txtworktypeid" class="hidden">Type:</label>';
        //$('#txtworktypeid').after(txtworktypeerrorlabel);
        // Move the label that referenced 'worktypeid' to now point to 'txtworktypeid'
        $('label[for="worktypeid"]').attr('for', 'txtworktypeid');


        if ($('#worktypeid option:selected').val() != 0) {
            $('#txtworktypeid').val($('#worktypeid option:selected').text());
        }

        $('#txtworktypeid').autocomplete({
            minLength : 0,
            source : '/miv/suggest/work/type',
            focus : function(event, ui) {
                $('#txtworktypeid').val(ui.item.value);

                return false;
            },
            select : function(event, ui) {
                $('#txtworktypeid').val(ui.item.value);
                $('#worktypeid').val(ui.item.id);

                return false;
            },
            change : function(event, ui) {
                if (ui.item == null) {
                    $('#txtworktypeid').val('');
                    $('#worktypeid').val('0');
                    $('#txtworktypeid').focus();
                }
            }
        });
*/
    }
    else {
        $('.associationbuttons').remove();
    }

    $('#workyear').change(function() {
        if (typeof validateAssociation == 'function') {
            validateAssociation($(this).val());
        }
    });

    /*$('#associationframe').load(function() {
        removeInvalidAssociationOptions();
    });*/
});
