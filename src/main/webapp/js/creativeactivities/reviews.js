//maximum review year is two years from now
var nMaxYear = (new Date()).getFullYear() + 2;

var mivFormProfile = {
    required: [],
    trim: [],
    constraints: {
        printreviewyear: function(contents) {
            var categoryid = $('#categoryid').val();

            if ( categoryid == 1 ) {
                if (contents == '') {
                    return 'REQUIRED';
                }
                else if (isNaN(contents)) {
                    return 'INVALID_NUMBER';
                }
                else if (parseInt(contents) < 1900
                        || parseInt(contents) > (parseInt($('#thisYear').val())+2)) {
                    return 'INVALID_YEAR';
                }
            }
            return true;
        },
        url: miv.validation.isLink,
        printurl: miv.validation.isLink
    },
    errormap: {'INVALID_YEAR':'Year must be from 1900 to ' + nMaxYear + '.' }
};

var formProfile = {
    'print_reviews' : {
        'required' : [ 'printreviewyear', 'printreviewer' ],
        'trim'     : [ 'printreviewyear', 'printreviewer', 'printinterviewer', 'printdescription', 'title', 'publication', 'publisher', 'country', 'province', 'city', 'volume', 'issue', 'pages', 'printurl']},
    'media_reviews' : {
        'required' : [ 'reviewdate', 'reviewer', 'venue'],
        'trim'     : [ 'reviewdate', 'reviewer', 'interviewer', 'venue', 'description', 'country', 'province', 'city', 'url']}
};

$(document).ready(function() {

    /*var selectedids = '<input type="hidden" id="selectedids" name="selectedids" value="">';
    $('#display').after(selectedids);*/

    $('#formhelp').html('Only the visible content in the active tab will be saved.');

    if (typeof jQuery.ui !== 'undefined') {
        $( '#mivtabs' ).tabs();

        $('#print_reviews').click(function() {

            if ( !$('#print_reviews_div').hasClass('ui-tabs-hide')) { // show #print_reviews_div
                $('#categoryid').val('1');
            }

            $('#publisherdiv').after($('#locationblock'));

            mivFormProfile['required'] = formProfile['print_reviews']['required'];
            mivFormProfile['trim'] = formProfile['print_reviews']['trim'];

        });

        $('#media_reviews').click(function() {
            if ( !$('#media_reviews_div').hasClass('ui-tabs-hide')) { // show #media_reviews_div
                $('#categoryid').val('2');
            }

            $('#descriptiondiv').after($('#locationblock'));

            mivFormProfile['required'] = formProfile['media_reviews']['required'];
            mivFormProfile['trim'] = formProfile['media_reviews']['trim'];

        });

        var txtcategoryid = '<input type="hidden" id="categoryid" name="categoryid" value="" maxlength="50">';
        $('#mivtabs').before(txtcategoryid);
        $('#tabsbar').removeClass('hidden');

        $('#media_reviews_div').removeClass('frameset');
        $('#print_reviews_div').removeClass('frameset');

        $('label[for="media_reviews_div"]').remove();
        $('label[for="print_reviews_div"]').remove();

        var reccategoryid = $('#tempcategoryid').val();
        if (reccategoryid == 2) { // media_reviews
            $( '#mivtabs' ).tabs({ selected: 1 });
            $('#categoryid').val('2');
        }
        else {
            $('#categoryid').val('1');
            $('#print_reviews').click();
        }

        // Now reveal the re-written page
        $('.pre-render').removeClass('pre-render');

        $('.datepicker').datepicker({
            dateFormat : 'mm/dd/yy',
            changeMonth: true,
            changeYear: true,
            showButtonPanel: true,
            showOtherMonths: true,
            selectOtherMonths: true,
            constrainInput : false,
            yearRange: '1900:'+(parseInt($('#thisYear').val())+2),
            currentText: 'Find Today',
            closeText: 'Close',
            buttonImage: '/miv/images/buttons/datepicker.png',
            buttonImageOnly: true,
            showOn: 'both'
        });
        $('img[class="ui-datepicker-trigger"]').each(function() {
            $(this).attr('title','Click to open a datepicker. format is (mm/dd/yy)');
        });
    }
    else {
        // Now reveal the re-written page
        $('.pre-render').removeClass('pre-render');
    }

    $('#reviewdate').change(function() {
        validateAssociation($(this).val());
    });

    $('#printreviewyear').change(function() {
        if (typeof validateAssociation == 'function') {
            validateAssociation($(this).val());
        }
    });

    /*$('#associationframe').load(function() {
        removeInvalidAssociationOptions();
    });*/
});
