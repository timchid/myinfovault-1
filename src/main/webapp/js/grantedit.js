'use strict';

$(document).ready(function() {
    $('.datepicker').datepicker({
            dateFormat : 'mm/dd/yy',
            changeMonth: true,
            changeYear: true,
            showButtonPanel: true,
            showOtherMonths: true,
            selectOtherMonths: true,
            constrainInput : false,
            yearRange: '1900:c+20',
            currentText: 'Find Today',
            closeText: 'Close',
            buttonImage: '/miv/images/buttons/datepicker.png',
            buttonImageOnly: true,
            showOn: 'both'
        });

    $('img[class="ui-datepicker-trigger"]').each(function() {
        $(this).attr('title','Click to open a datepicker. format is (mm/dd/yy)');
    });

    // Set visibility of required asterisk based on grant status selected
    // 2012-10-04 SDP: should instead add or remove the "f_required" class on the *label*
    //     and get rid of the <span class="required">*</span> altogether.
    //     See patenteditform.jsp and patentedit.js
    $('#statusid').change(function() {
        var value = $(this).val();

        $('#startdate').parent().children('.required').css('visibility',
                                                           value === 1 || value === 3 ?
                                                           'visible' :
                                                           'hidden');

        $('#submitdate').parent().children('.required').css('visibility',
                                                            value === 2 || value === 4 ?
                                                            'visible' :
                                                            'hidden');
    }).trigger('change'); // Initial trigger for status
});

/*<span class="vim">
 vim:ts=8 sw=4 et sm:
</span>*/
