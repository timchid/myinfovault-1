'use strict';

var miv = miv || {};

(function () {
    function MivDOM()
    {
        /**
         * Generate an html element.
         * @function tag
         * @memberof miv.dom
         * @param {string} tag - The kind of tag to generate.
         * @param {Object} attributes - A js object where the property names are the
         *                              attribute names and the property values are the
         *                              attribute values of the generated tag.
         */
        this.tag = function(tag, attributes)
        {
            var element = document.createElement(tag);

            if (attributes !== undefined) {
                $.each(attributes, function(attribute, value) {
                    element.setAttribute(attribute, value);
                });
            }

            return $(element);
        };

        /**
         * Gets the text of an element, but NOT the element's children.
         * @function getTextNode
         * @memberof miv.dom
         * @param {element} element - The element from which to extract the text node.
         */
        this.getTextNode = function(element) {
            return $(element).clone()
                             .children()
                             .remove()
                             .end()
                             .text();
        };
    }

    /**
     * Namespace for dom manipulation.
     * @namespace miv.dom
     * @memberof miv
     * @author Jacob Saporito
     * @since 5.0
     * @version 1.0
     */
    miv.dom = new MivDOM();
})();
