'use strict';
/* global showMessage, validateForm */
/**
 * validate personal data entry form
 */

var mivFormProfile = {
            required: [ 'displayName' ],
            trim: [ 'displayName' ]
        };

$(document).ready(function() {

    if($('#successbox, #infobox', $('#CompletedMessage')).text().length > 0)
    {
        showMessage('CompletedMessage',null);
    }

    var $form = $('#mytestform');
    $form.submit(function(event) {
        if (typeof mivFormProfile !== 'undefined') {
            var $errormessage = $('#errormessage');

            if (!validateForm($form, mivFormProfile, $errormessage)) {
                event.preventDefault();
            }
        }
    });

});
