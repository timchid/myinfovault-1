'use strict';
/* global mivFormProfile */
/**
 *
 */

// Set custom error message for form input
$(document).ready(function()
{
    var link = document.getElementById('link');

    link.onchange = function(e) {
        if (e.target.setCustomValidity) {
            e.target.setCustomValidity('');
        }
    };

    link.oninvalid = function(e) {
        if (e.target.validity && !e.target.validity.valid) {
            e.target.setCustomValidity('Must be a valid web address');
        }
    };

    $('.datepicker').datepicker({
        dateFormat : 'mm/dd/yy',
        changeMonth: true,
        changeYear: true,
        showButtonPanel: true,
        showOtherMonths: true,
        selectOtherMonths: true,
        constrainInput : false,
        yearRange: '1900:c+10',
        currentText: 'Find Today',
        closeText: 'Close',
        buttonImage: '/miv/images/buttons/datepicker.png',
        buttonImageOnly: true,
        showOn: 'both'
    });

    $('img[class="ui-datepicker-trigger"]').each(function() {
        $(this).attr('title', 'Click to open a datepicker. format is (mm/dd/yy)');
    });

});

// Handle expand/collapse of the sections, and changing requirements when Type and Status change.
$(document).ready(function() {
    var jTypeField = $('#typeid');
    var jStatusField = $('#patentstatusid');

    var currentReqSet = '';
    var requiredSets = {
        'disclosure' : { // two of the same makes updateRequirements() easier even though "status" doesn't apply to disclosures
            'filed'   : [ 'disclosuretitle', 'disclosureauthor', 'disclosureid', 'disclosuredate' ],
            'granted' : [ 'disclosuretitle', 'disclosureauthor', 'disclosureid', 'disclosuredate' ]
        },
        'patent' : {
            'filed'   : [ 'patenttitle', 'patentauthor', 'applicationid', 'applicationdate' ],
            'granted' : [ 'patenttitle', 'patentauthor', 'patentid', 'patentdate' ]
        }
    };

    var formProfile = {
        'patent-filed' : {
            'required' : [ 'patenttitle', 'patentauthor', 'applicationid', 'applicationdate' ],
            'trim'     : [ 'patenttitle', 'patentauthor', 'applicationid', 'applicationdate', 'patentid', 'patentdate', 'link']},
        'patent-granted' : {
            'required' : [ 'patenttitle', 'patentauthor', 'patentid', 'patentdate' ],
            'trim'     : [ 'patenttitle', 'patentauthor', 'applicationid', 'applicationdate', 'patentid', 'patentdate', 'link']},
        'disclosure-filed' : {
            'required' : [ 'disclosuretitle', 'disclosureauthor', 'disclosureid', 'disclosuredate' ],
            'trim'     : [ 'disclosuretitle', 'disclosureauthor', 'disclosureid', 'disclosuredate']},
        'disclosure-granted' : {
            'required' : [ 'disclosuretitle', 'disclosureauthor', 'disclosureid', 'disclosuredate' ],
            'trim'     : [ 'disclosuretitle', 'disclosureauthor', 'disclosureid', 'disclosuredate']}
    };


    $('fieldset').collapsible();

    jTypeField.change(function(evt) {
        var choice = 1 * $(evt.target).val(); // makes the choice numeric instead of a string
        var newtext = $($('option', jTypeField)[choice - 1]).text().toLowerCase();
        // expand/collapse the relevant sections
        expandChosen(newtext);
        // also Update "required" fields
        updateRequirements();
    });

    jStatusField.change(function(evt) {
        // Update "required" fields
        updateRequirements();
    });

    function expandChosen(choice)
    {
        switch (choice) {
        case 'patent':
            $('fieldset#patent').collapsible('open');
            $('fieldset#disclosure').collapsible('close');
            break;
        case 'disclosure':
            $('fieldset#patent').collapsible('close');
            $('fieldset#disclosure').collapsible('open');
            break;
        }
    }

    function updateRequirements()
    {
        // get value of typeid and statusid to decide which fields are required.
        //  typeid==2 (disclosure) is one set (regardless of value of statusid)
        //  typeid==1 (patent) && statusid==1 (filed) is another set
        //  typeid==1 (patent) && statusid==2 (granted) is the last set
        var typeVal = 1 * jTypeField.val();
        var statusVal = 1 * jStatusField.val();
        var typeWord = $($('option', jTypeField)[typeVal - 1]).text().toLowerCase();
        var statusWord = $($('option', jStatusField)[statusVal - 1]).text().toLowerCase();

        var reqSet = typeWord + '-' + statusWord;
        if (reqSet === currentReqSet) { return; }
        currentReqSet = reqSet;

        var reqlist = $('[required]'); // show what's currently required (for debug only)
        $('.f_required').removeClass('f_required');
        reqlist.removeAttr('required');

        var fieldList = requiredSets[typeWord][statusWord];

        /* Adding required and trim objects dynamically*/
        mivFormProfile.required = formProfile[reqSet].required;
        mivFormProfile.trim = formProfile[reqSet].trim;

        for (var n in fieldList)
        {
            if (fieldList.hasOwnProperty(n)) {
                var field = fieldList[n];

                var fieldSelector = '#' + field;
                $(fieldSelector).attr('required', 'required');

                var labelSelector = 'label[for="'+field+'"]';
                $(labelSelector).addClass('f_required');
            }
        }
    }

    var option = $('option', jTypeField)[ (1 * jTypeField.val()) - 1 ];
    expandChosen($(option).text().toLowerCase());
    updateRequirements();
    $('fieldset#aux').collapsible('close');
    // Convert the Jurisdiction help link to a Pop-up
});
// End of "patentform.js"

/*
 vim:ts=8 sw=4 et sm:
*/
