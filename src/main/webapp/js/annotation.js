'use strict';
/* global showMessage, showLoadingDiv, checkBoxState, getListOfIds,
   getListOfValues, highlight, initTriStateCheckBox, getPositionTop,
   setDefaultFocus, hideLoadingMessage */
$(document).ready(function() {
	// don't show loading image for pop-up form.
	if ($('#dialog-form').size() === 0) {
		showLoadingDiv();
	}

	setTimeout(highlightInit, 200);

	$('.recorddisplay', $('.previewitemlist')).click(function() {
		setRecordVisibilityInDossier($(this));
	});

	// only provide pop-up notation form when annotation form has notation block.
	if ($('#isNotation').size() !== 0 && $('#isNotation').val()) {
		initNotation();
	}
});


/**
 * Set things up just one time when the DOM is ready.
 */
function initNotation()
{
	// FIXME:  * urp *   These should just be in the jsp if possible; otherwise done by cloning a template in the jsp.
	$('div.section-buttonset').append('<input type="button" id="addnotations" value="Add Notations" title="Add notations on all selected records" disabled>');
	$('div.section-buttonset').append('<input type="button" id="clearselections" value="Clear Selected Records" title="Clear selected records" class="hidden">');

	var $addnotations = $('#addnotations');
	var $clearselections = $('#clearselections');

	// Attach the click-handler to the "Add Notations" button. This will
	// clear the action result when clicked, and create the dialog/form
	// if it doesn't currently exist in the page.
	$addnotations.click(function() {
		$('#actionResult').val('');
		// It's ALWAYS size===0 because it's REMOVED in the close:function (about line 221)
		if ( $('#dialog-form').size() === 0 ) {
			initNotationForm();
		}
	});

	$clearselections.click(function() {
		$('.publine.ui-select', $('.previewitemlist')).removeClass('ui-select');
		$addnotations.prop('disabled', true);
		$clearselections.addClass('hidden');
		$('#main').off('selectstart').removeClass('unselectable');
	});


	// Select records with click, shift-click, or ctrl/cmd-click.
	var $publines = $('#main div.previewitemlist div.publine');
	var lastSelectedIndex = 0;
	$publines.click(function(e) {

	if (!$(e.target).is('a.linktobutton') && !$(e.target).is('.recorddisplay')) {
		e.preventDefault();

		var $this = $(this);
		// Detecting ctrl (windows) / meta (mac) key.
		if (e.ctrlKey || e.metaKey) {
			if ($this.hasClass('ui-select')) {
				$this.removeClass('ui-select');
				if ( $('div.recordsection .ui-select').size() < 1 ) {
					$('#main').off('selectstart').removeClass('unselectable');
				}
			}
			else {
				// Every sane browser uses CSS 'user-select' to prevent selection. IE 8 & 9 need to prevent the selectstart event.
				$('#main').on('selectstart', function(e) { e.preventDefault(); }).addClass('unselectable');
				$this.addClass('ui-select');
				lastSelectedIndex = $publines.index($this);
			}
		}
		// Detecting shift key
		else if (e.shiftKey) {
				$('#main').on('selectstart', function(e) { e.preventDefault(); }).addClass('unselectable');
			// Get the shift+click element
			var selectedElementIndex = $publines.index($this);
			var indexOfRows;

			// Mark selected between them
			if (lastSelectedIndex < selectedElementIndex) {
				// deselect all selected records above the last selected record
				for (indexOfRows = 0; indexOfRows < lastSelectedIndex; indexOfRows++) {
					$publines.eq(indexOfRows).removeClass('ui-select');
				}

				for (indexOfRows = lastSelectedIndex; indexOfRows <= selectedElementIndex; indexOfRows++) {
					$publines.eq(indexOfRows).addClass('ui-select');
				}
			}
			else {
				for (indexOfRows = selectedElementIndex; indexOfRows <= lastSelectedIndex; indexOfRows++) {
					$publines.eq(indexOfRows).addClass('ui-select');
				}
			}
		}
		else {
			lastSelectedIndex = $publines.index($this);
			$publines.removeClass('ui-select');
			$this.addClass('ui-select');
			$('#main').on('selectstart', function(e) { return false; }).addClass('unselectable');
		}

		if ($('.publine.ui-select', $('.previewitemlist')).size() > 0) {
			$addnotations.prop('disabled', false);
			$clearselections.removeClass('hidden');
		}
		else {
			$addnotations.prop('disabled', true);
			$clearselections.addClass('hidden');
		}
	}
	});

}


/**
 * Open the dialog for the currently selected set of records.
 *
 * Create and configure the notation dialog as it should appear on this page
 * if it doesn't exist yet, which takes into account whether this is for Master
 * or Packet annotations.
 */
function initNotationForm()
{
	// Add the <div> that will hold the dialog to the page.
	$('#main').append($('<div id="dialog-form" title="Add/Edit ' + $('#annotationname').val() + ' Notations"></div>'));


	// Load the dialog/form itself from its source file.
	// Call the "complete" function/parameter when loading is done.
	var $dialogForm = $( '#dialog-form' ).load('/miv/jsp/notationform.jspf', function() {
		setupNotationForm();

		$('#tri_check_asis', $('#dialog-form')).prop('checked', false).prop('indeterminate', true);
		$('#tri_check_add', $('#dialog-form')).prop('checked', true).prop('indeterminate', false);
		$('#tri_check_remove', $('#dialog-form')).prop('checked', false).prop('indeterminate', false);
	});

	// This doesn't do anything.
	// $('#dialog') selects nothing, $('dialog:ui-dialog') selects nothing.
	$( '#dialog:ui-dialog' ).dialog( 'destroy' );

	$dialogForm.dialog({
		autoOpen: false,
		height: 270,
		width: 490,
		modal: true,
		buttons: {
			'Save': function() {
				var $from = $dialogForm.contents().find('form');
				var action = $from.attr( 'action' );

				var notations = {};
				notations.included = checkBoxState($('#included', $from));
				notations.significant = checkBoxState($('#significant', $from));
				notations.mentoring = checkBoxState($('#mentoring', $from));
				notations.refereed = checkBoxState($('#refereed', $from));

				var formdata = $from.serialize();
				var callSave = false;

				// Compare the load state of each checkbox to the current state.
				// Add the checkbox & state to the formdata if the state has been changed.
				if ($('#included', $from).data('load.state') !== notations.included) {
					formdata += '&included=' + notations.included;
					callSave = true;
				}

				if ($('#significant', $from).data('load.state') !== notations.significant) {
					formdata += '&significant=' + notations.significant;
					callSave = true;
				}

				if ($('#mentoring', $from).data('load.state') !== notations.mentoring) {
					formdata += '&mentoring=' + notations.mentoring;
					callSave = true;
				}

				if ($('#refereed', $from).data('load.state') !== notations.refereed) {
					formdata += '&refereed=' + notations.refereed;
					callSave = true;
				}

				// Save the notations if any of the checkbox states changed.
				if (callSave) {
					/* Send the data using post and put the results in a div */
					$.post( action, formdata,
						function( data ) {
							var resultObj = JSON.parse( data );

							try {
								if (resultObj.success) {
									var selectedRecords = getListOfIds($('.publine.ui-select', $('.previewitemlist')));
									window.location.href = window.location.href + '&actionResult=notations_saved&selectedRecords=' + selectedRecords.join(',');
								}
							} catch (e) {
								console.warn('exception \'e\' is: ' + e);
							} finally {
								$( this ).dialog( 'close' );
							}
					});
				}
				else {
					window.location.href = window.location.href + '&actionResult=nothing_to_save';
					$( this ).dialog( 'close' );
				}
			},
			Cancel: function() {
				$( this ).dialog( 'close' );
			}
		},
		close: function() {
			$dialogForm.remove();
		}
	});

	$dialogForm.dialog( 'open' );
}


/**
 *
 */
function setupNotationForm()
{
	var $from = $('#dialog-form').contents().find('form');

	var packetId = 1 * $('#annotationPacketId').val();
	$from.append( $('<input type="hidden" name="packetid" value="' + packetId + '">') );
	$from.append( $('<input type="hidden" name="selections">') );

	var allowedNotations = /_/;
	if (packetId === 0) {
		allowedNotations = /[+@]/; // Master annotations allow only Major Mentoring (+) and Refereed (@) ...
	}
	else {
		allowedNotations = /[*x]/; // ... while Packet annotations allows Included (*) and Most Significant (x)
	}

	var $selected = $('.publine.ui-select');
	var selectedCount = $selected.length;

	var selectedRecs = [];         // Array of objects, 1 for each selected record, holding the selected 'rectype' and 'recid'
	var notationCounts = {};       // Count of each type of notation found in the selected records.

	$selected.each(function(/*index, value*/) {

		var notesPresent = $(this).data('notations');
		if (notesPresent !== undefined) {
			$.each(notesPresent.split(','),
				function(i, notation) {
					console.log('i=' + i + '  and notation=' + notation + '  and this=' + this);
					if (notationCounts[notation] === undefined) {
						notationCounts[notation] = 1;
					}
					else {
						notationCounts[notation]++;
					}
			});
		}

		selectedRecs.push(
			{
			section : $(this).data('section'), // I hope to get rid of section; record type should be enough information.
			recid   : $(this).data('recid'),
			rectype : $(this).data('rectype')
			}
		);

	});

	console.log('stringified notation counts: ' + JSON.stringify(notationCounts));
	$('input[name=selections]').val( JSON.stringify(selectedRecs) );

	// Now set the initial state of the checkboxes...
	//     If a notation-count is zero that notation's checkbox will be UNCHECKED
	//     If a notation-count matches the total record count that notation's checkbox will be CHECKED
	//     If 0 < notation-count < record-count the notation's checkbox will be UNCHECKED + INDETERMINATE


	/* This is determining what notations are being used, and counting them, by inspecting the text()
	 * of the span.notations within each selected publine.  Doing that counts the Master annotations
	 * that are in the notations string even though we're working on Packet annotations! (see below)
	 * Instead, we need to look at the data-notations on the publine itself... (see above)
	 *
	 * This is all being done so the initial state of the checkboxes can be set: checked, unchecked, or "indeterminate"
	 * This will be Replaced.
	 */
/* Here's the old code, with tests like needAnnotations2 thrown in ... */
	var notations = '';
	var notationCountMap = { 'included':0, 'significant':0, 'mentoring': 0, 'refereed':0 };
	$('.publine.ui-select span.notations', $('.previewitemlist')).each(function() {
		$.each(String($(this).text()).split(''), function(index, value) {
			if (notations.indexOf(value) === -1) {
				notations += value;
			}

			switch (value) {
				case '*':
					notationCountMap.included++;// = parseInt(notationCountMap.included) + 1;
					break;
				case 'x':
					notationCountMap.significant++;// = parseInt(notationCountMap.significant) + 1;
					break;
				case '+':
					notationCountMap.mentoring++;// = parseInt(notationCountMap.mentoring) + 1;
					break;
				case '@':
					notationCountMap.refereed++;// = parseInt(notationCountMap.refereed) + 1;
					break;
			}
		});
	});

	var annotationtype = $('#annotationtype').val();
	var annotationname = $('#annotationname').val();
	var ids = getListOfValues(
		$('.publine.ui-select input.recorddisplay', $('.previewitemlist')).filter(function(index) {
			return $('span.notations', $(this).closest('div.publine')).size() !== 0;
		})
	);
	var selecteds = $('.publine.ui-select', $('.previewitemlist'));
	var recIdMap = $(selecteds).map(function() {
		return $(this).data('recid');
	});
	var recIdList = recIdMap.get();
/*
	var test_ids = getListOfValues(
		$('.previewitemlist .publine.ui-select').filter(function(index) {
			//console.log( 'this is ' + this + '  and  $(this) is ' + $(this) );
			var keep = $('span.notations', this).text().match(allowedNotations) !== null;
			var old_keep = $('span.notations', this).size() !== 0;
			return keep;
		})
	);
*/
	var hasAnnotations = getListOfValues(
		$('.previewitemlist .publine.ui-select input.recorddisplay').filter(function(index) {
			//console.log( 'this is ' + this + '  and  $(this) is ' + $(this) );
			var notes = $('span.notations', $(this).closest('div.publine')).text();
			console.log('hasAnnotations: notes=['+notes+']');
			var keep = notes.match(allowedNotations) !== null;
			var old_keep = $('span.notations', $(this).closest('div.publine')).size() !== 0;
			return keep;
		})
	);

	var needAnnotations = getListOfValues(
		$('.publine.ui-select input.recorddisplay', $('.previewitemlist')).filter(function(index) {
			//console.log( 'this is ' + this + '  and  $(this) is ' + $(this) );
			return $('span.notations', $(this).closest('div.publine')).size() === 0;
		})
	);

	var needAnnotations2 = getListOfValues(
			$('.previewitemlist .publine.ui-select input.recorddisplay').filter(function(index) {
			var notes = $('span.notations', $(this).closest('div.publine')).text();
			console.log('needAnnotations2: notes=['+notes+']');
			var keep = notes.match(allowedNotations) === null;
			return keep;
		})
	);

//ids = hasAnnotations;
//needAnnotations = needAnnotations2;

	$('.annotation-label', $from).html(annotationname);
	$('#annotationtype', $from).val(annotationtype);
	$('#sectionIds', $from).val(ids.join(','));				// THERE'S YOUR PROBLEM
	$('#addSectionIds', $from).val(needAnnotations.join(','));

	setNotationState($('#included',    $from), notations, '*', notationCountMap.included    === selectedCount);
	setNotationState($('#significant', $from), notations, 'x', notationCountMap.significant === selectedCount);
	setNotationState($('#mentoring',   $from), notations, '+', notationCountMap.mentoring   === selectedCount);
	setNotationState($('#refereed',    $from), notations, '@', notationCountMap.refereed    === selectedCount);
	reallySetNotationState(selectedCount, notationCounts);
	initTriStateCheckBox();
}


function setNotationState($notationObj, notations, notationChar, isChecked)
{
	$notationObj.removeClass('ui-rotate-indeterminate-checkbox');
	if (notations.indexOf(notationChar) !== -1) {
		$notationObj.prop('checked', isChecked).prop('indeterminate', !isChecked);
		if (!isChecked) {
			$notationObj.addClass('ui-rotate-indeterminate-checkbox');
		}
	}

	$notationObj.data( 'load.state', checkBoxState($notationObj) );
}
/* This 'reallySetNotationState' function is going to replace the one above. */
function reallySetNotationState(/*int*/selectedCount, /*object*/notationCounts)
{
	console.log("really Set Notation State");
	console.log("  count: " + selectedCount);
	console.log("present: " + JSON.stringify(notationCounts));
}

function setRecordVisibilityInDossier(record)
{
    // TODO: THIS Needs to change to get the PacketId and send it along with the record-section and the record-id
	var publineDiv = $(record).closest('div.publine');
	var packetId = 1 * $('#annotationPacketId').val();
	var recType = publineDiv.data('rectype');
	var recId = publineDiv.data('recid');
	var lists = $(record).val().split('~');
	var msg = '';
	if ($(record).is(':checked')) {
		msg = '<div id="successbox">Record has been added to dossier, to apply changes create my dossier again.</div>';
	}
	else {
		msg = '<div id="deletebox">Record has been removed from dossier, to apply changes create my dossier again.</div>';
	}

	/* Send the data using post and put the results in a div */
	$.post('/miv/Annotations?requestType=save-record-visibility' +
	        '&packetid=' + packetId +
	        '&section=' + lists[0] +
		'&rectype=' + recType +
		'&recid=' + recId +
		'&display-record=' + $(record).is(':checked'),
		function(data) {
		var resultObj = JSON.parse( data );

		try {
			if (resultObj.success) {
				highlight( publineDiv.attr('id') );
				showMessage('CompletedMessage', msg, getPositionTop($(publineDiv).attr('id')));

				if ($(record).is(':checked')) {
					$(publineDiv).removeClass('inactive');
				}
				else {
					$(publineDiv).addClass('inactive');
				}

				if ($('#display').val() === 'true' || $('#display').val() === 1) {
					var recordSectionDiv = $(publineDiv).closest('div.recordsection');
					$(publineDiv).remove();

					// remove section block when it has no record in it.
					if ($('div.publine', $(recordSectionDiv)).size() === 0) {
						var recordBlockDiv = $(recordSectionDiv).closest('div.recordblock');
						$(recordSectionDiv).remove();

						// remove recordsection block when it has no record in it.
						if ($('div.recordsection', $(recordBlockDiv)).size() === 0) {
							var previewItemListDiv = $(recordBlockDiv).closest('div.previewitemlist');
							$(recordBlockDiv).remove();

							// if no section and records available for annotation set error message.
							if ($('div.recordblock', $(previewItemListDiv)).size() === 0) {
								$('#selectheader').css('display', 'none');
								$('#errormessage').css('display', 'block').append('<div id="errorbox"></div>');
								$('#errorbox').html('Annotations can be added to extending knowledge records.<br>' +
										    'You have not selected any extending knowledge records to display at ' +
										    '<a href="/miv/packet" title="Packet Management" style="color: inherit;">' +
										    '<em>Packet Management</em></a> page or click on the <em>Show All Records</em> button.</div>');
								$(previewItemListDiv).append($('#errormessage'));
							}
						}
					}
				}
			}
		} catch (e) {
			console.warn('exception \'e\' is: ' + e);
		}
	});
}


/**
 * Highlight saved record with confirmation messages.
 */
function highlightInit()
{
	var action = $('#annotationAction').val();
	var actionResult = $('#actionResult').val();
	$('#actionResult').val('');
	var isMessage = false;

	if (actionResult === 'notations_saved') {
		isMessage = true;
		var selectedRecords = $('#selectedRecords').val();
		$.each(selectedRecords.split(','), function(index, value) {
			// highlight the saved record
			highlight(value);
			});
		showMessage(null, '<div id="successbox">Notations has been updated successfully.</div>');
	}
	else if (actionResult === 'nothing_to_save') {
		isMessage = true;
		showMessage(null, '<div id="successbox">There is nothing to update</div>');
	}
	else {
		var message = '<div id="successbox">' + $('#annotationname').val() +
			      ' annotation has been saved successfully.</div>';
		var recordID = $('#annotationRecId').val();
		var recType = $('#annotationRecType').val();
		if (action !== null &&
			action !== undefined &&
			action === 'save') {
			isMessage = true;
			// highlight the saved record
			highlight('L' + recordID + recType);

			// show confirmation messages
			showMessage('CompletedMessage', message, getPositionTop('L' + recordID + recType));
		}
		else {
			setDefaultFocus();
		}
	}

	// modifying history using pushState
	if (isMessage && typeof history.pushState === 'function') {
		var url = (window.location.href)
			.replace(/&recid=[^&]+/, '')
			.replace(/&section=[^&]+/, '')
			.replace(/&type=[^&]+/, '')
			.replace(/&actionType=[^&]+/, '')
			.replace(/&actionResult=[^&]+/, '')
			.replace(/&selectedRecords=[^&]+/, '');
		var stateObj = {};
		history.pushState(stateObj, 'dummy', url);
	}

	hideLoadingMessage();
}
/* vim:ts=8 sw=8 noet sr:
   EOF -- Don't put anything after this line. */
