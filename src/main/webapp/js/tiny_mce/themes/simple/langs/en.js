tinyMCE.addI18n('en.simple', {
	"cleanup_desc" : "Cleanup Messy Code",
	"redo_desc" : "Redo (Ctrl+Y)",
	"undo_desc" : "Undo (Ctrl+Z)",
	"numlist_desc" : "Insert/Remove Ordered list",
	"bullist_desc" : "Insert/Remove Unordered list",
	"striketrough_desc" : "Strikethrough",
	"underline_desc" : "Underline (Ctrl+U)",
	"italic_desc" : "Italic (Ctrl+I)",
	"bold_desc" : "Bold (Ctrl+B)"
});