'use strict';
/* global showLoadingDiv, hideLoadingMessage, aTipSize, qTipObject, qTipSelectObject */
//perform on load
$(document).ready(function() {
	showLoadingDiv();

	// hide form help because we are using jquery qtip
	//$('p.formhelp').attr('style','display:none');

	fnInitTooltipHelp();

	$('fieldset.mivcollapsible').collapsible({
		sCollapsedClass       : 'collapsed',
		bOpen : function(element) {
			return $('textarea.mceEditor',element).val().length > 0;
		}
	});

	if ($('fieldset.collapsed').length === 3) {
		$('fieldset.mivcollapsible').first().collapsible({bOpen : true});
	}

	setTimeout(function() {
		hideLoadingMessage();
	}, 200);
});

//settings object for each qtip
function fnInitTooltipHelp()
{
	$('div.forminfo').each(
		function() {
			// add specific content for the qtip
			// qTipSelectObject is available at qtip_config.js
			qTipSelectObject.style.classes = qTipObject.style.classes + aTipSize.large;
			qTipSelectObject.content = {
				title: { text: $(this).attr('alt') },
				text: $('div.formhelp').html()
			};

			$(this).qtip(qTipSelectObject);
		});
}
