'use strict';
/* global Modernizr */

/**
 * MIV Namespace used for the miv javascript libraries.
 * @namespace miv
 * @author Jacob Saporito
 * @since 5.0
 * @version 0.7
 */
var miv = miv || {};

(function() {
    function MivValidate()
    {
        var DEFAULT_ERROR = '{field} is invalid.';
        var DEFAULT_FIELD = 'Field';
        var validationSelector = '.validated, input, select, textarea';

        var self = this;

        var options = {
            favorNative : true,
            html5messages : false
        };

        function isEmpty(element) {
            if (element.is('[type=radio]')) {
                // is there a radio button in the group checked.
                return !($('[name="' + element.attr('name') + '"]').is(':checked'));
            }
            return element.val() === '' && !element.is(':checked');
        }

        function getValue(element) {
            if (element.is('[type=radio]')) {
                return $('[name="' + element.attr('name') + '"]:checked').val();
            }
            return element.val();
        }

        var validators = {
            between : function(min, max) {
                //implies numeric(), established in both min and max
                if (isEmpty(this) ||
                    (validators.min.call(this, min) === true &&
                    validators.max.call(this, max) === true)) {
                    return true;
                }
                return '{field} must be between ' + min + ' and ' + max + '.';
            },

            email : function() {
                // implementation based loosely off of RFC 5322. See http://www.regular-expressions.info/email.html for info.
                // Actually not perfect, but close.
                var emailRegex = /^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$/;

                return (isEmpty(this) || emailRegex.test(getValue(this))) ? true : '{field} must contain a valid email address.';
            },

            integer : function() {
                var val = getValue(this) * 1;

                if (isEmpty(this) || (!isNaN(val) && val === Math.round(val))) { return true; }
                return '{field} must contain an integer.';
            },

            max : function(value) {
                //implies numeric()
                if (isEmpty(this) ||
                    (validators.numeric.call(this) === true &&
                    value * 1 >= getValue(this) * 1)) {
                    return true;
                }
                return '{field} must be at most ' + value + '.';
            },

            maxlength : function(length) {
                return (isEmpty(this) || getValue(this).length <= length * 1) ? true : '{field} must be at most ' + length + ' characters long.';
            },

            min : function(value) {
                //implies numeric()
                if (isEmpty(this) ||
                    (validators.numeric.call(this) === true &&
                    value * 1 <= getValue(this) * 1)) {
                    return true;
                }
                return '{field} must be at least ' + value + '.';
            },

            minlength : function(length) {
                return (isEmpty(this) || getValue(this).length >= length * 1) ? true : '{field} must be at least ' + length + ' characters long.';
            },

            numeric : function() {
                var val = getValue(this) * 1;
                if (isEmpty(this) || (!isNaN(val) && isFinite(val))) {
                    return true;
                }
                return '{field} must contain a numeric value.';
            },

            pattern : function(patternRegex) {
                patternRegex = new RegExp(patternRegex);

                //I assume we don't want to show the user regex
                //so it doesn't make much sense to have any specific error...
                return (isEmpty(this) || patternRegex.test(getValue(this))) ? true : DEFAULT_ERROR;
            },

            required : function() {
                return !isEmpty(this) ? true : '{field} is required.';
            },

            step : function(min, step) {
                return (isEmpty(this) ||
                    (validators.numeric.call(this) &&
                    (getValue(this) - parseFloat(min)) % parseFloat(step) === 0)) ?
                true :
                '{field} must be a multiple of ' + step + ' above ' + min + '.';
            }
        };

        function getErrorMessage(element, defaultMessage) {
            var message = element.data('errormessage') || defaultMessage || DEFAULT_ERROR;

            var re = /\{([A-Z0-9]+)\}/gi;
            var match;
            while ((match = re.exec(message)) !== null) {
                //for string 'this is a {test}', match will contain ['{test}', 'test']
                var value = element.data(match[1]);
                if (match[1] === 'field' && value === undefined) {
                    //special case: we'll intelligent guess what to call {field}
                    //and if that fails, just replace it with 'Field'
                    //In order, the guesses are:
                    // 1. element.data('field')
                    // 2. enclosing <label> tag
                    // 3. label tag where for=element.id
                    // 4. enclosing fieldset's <legend> tag
                    // 5. DEFAULT_FIELD variable ( = 'Field')
                    value = miv.dom.getTextNode(element.closest('label'));

                    if (value === '') {
                        var id = element.attr('id');
                        if (id !== undefined) {
                            value = miv.dom.getTextNode($('label[for="' + id + '"]'));
                        }
                    }

                    if (value === '') {
                        value = miv.dom.getTextNode(element.closest('fieldset').find('legend'));
                    }

                    value = value === '' ? DEFAULT_FIELD : value;
                }

                if (value !== undefined) {
                    var varRegex = new RegExp(match[0], 'g');

                    message = message.replace(varRegex, value);
                }
            }

            return message;
        }

        /**
         * Get list of constraints which apply to the element.
         * @function getConstraints
         * @memberof miv.validate
         * @param {jQuery} element - jQuery-wrapped element for which we are getting an array of all constraints that will be applied.
         * @returns {string[]} List of constraints that apply to the element.
         */
        this.getConstraints = function(element) {
            var constraints = element.data('constraints');
            constraints = constraints ? constraints.split(/\s*;\s*/) : [];

            function constrainIf(element, constraint, selector, nativeSupport) {
                var isSelector = element.is(selector);

                if (isSelector && !(options.favorNative && nativeSupport)) {
                    constraints.push(constraint);
                }

                return isSelector;
            }

            constrainIf(element, 'email', '[type=email]', Modernizr.inputtypes.email);
            constrainIf(element, 'numeric', '[type=number]', Modernizr.inputtypes.number);
            constrainIf(element, 'required', '[required]', Modernizr.input.required);
            constrainIf(element, 'min(' + element.attr('min') + ')', '[min]:not([max])', Modernizr.input.min);
            constrainIf(element, 'max(' + element.attr('max') + ')', '[max]:not([min])', Modernizr.input.max);
            constrainIf(element,
                        'between(' +
                            element.attr('min') + ',' +
                            element.attr('max') + ')',
                        '[min][max]',
                        Modernizr.input.min && Modernizr.input.max);
            constrainIf(element,
                        'step(' + element.attr('min') + ',' + element.attr('step') + ')',
                        '[min][step]',
                        Modernizr.input.min && Modernizr.input.step);

            //No modernizr detection
            constrainIf(element, 'maxlength', '[maxlength]', false);
            constrainIf(element, 'minlength', '[minlength]', false);

            return constraints;
        };

        /**
         * Register a validation function for use by {@link miv.validate#validate}.
         * @function registerValidator
         * @memberof miv.validate
         * @param {string} name - The name by which you wish to reference this validation function.
         * @param {Function} func - A function which returns true if it passes this step of validation, or an error message otherwise.
         * @throws {Exception} If you try to register a validator which is not a function.
         */
        this.registerValidator = function(name, func) {
            if (func instanceof Function) {
                validators[name] = func;
            }
            else {
                throw 'Invalid Validator - not a function';
            }
        };

        /**
         * Runs the validator on the current page.
         * @function validate
         * @memberof miv.validate
         * @param {jQuery} [elements] jQuery set of elements to validate. Validates $('.validated, input, select, textarea') if argument not provided.
         * @returns {boolean} False if validate showed errors, True if it did not.
         * Note that it can catch errors and still return true, if it is set to defer to html5 validation messages.
         */
        this.validate = function(elements) {
            var errors = [];
            elements = elements ? elements : $(validationSelector);

            var radiobuttons = elements.filter('input[type=radio]');
            elements = elements.filter(':not(input[type=radio])');

            //Add only one radio button from each radio button group to the list of elements to validate.
            //constraints which are applied to the first radio button will be applied to the group.
            while (radiobuttons.length > 0) {
                var radiogroup = radiobuttons.first().attr('name');
                var groupelements = radiobuttons.filter('[name="' + radiogroup + '"]');

                elements = elements.add(groupelements.first());
                radiobuttons = radiobuttons.filter(':not([name="' + radiogroup + '"])');
            }

            //remove old error classes
            $('.errorfield').removeClass('errorfield');

            elements.each(function(_index, element) {
                element = $(element);

                var constraints = self.getConstraints(element);

                for (var i = 0; i < constraints.length; i++) {
                    //trims leading and ending spaces, and takes off the closing paren, if it exists
                    //it then splits it at ( and , which leads to the first element being
                    //the constraint name and all following elements being arguments
                    //e.g. '   test ( one,two,   three  )  ' goes to ['test', 'one', 'two', 'three']
                    var constraint = constraints[i].replace(/^\s*|\s*\)?\s*$/g, '')
                                                   .split(/\s*[\(,]\s*/);

                    var valid = validators[constraint[0]].apply(element, constraint.slice(1));
                    var errorMessage = '';
                    if (valid !== true) {
                        if (element.is('input[type=radio]')) {
                            $('input[type=radio][name="' + element.attr('name') + '"]').addClass('errorfield');
                        }
                        else {
                            element.addClass('errorfield');
                        }

                        errorMessage = getErrorMessage(element, valid);
                    }

                    if (options.html5messages && element[0].setCustomValidity instanceof Function) {
                        element[0].setCustomValidity(errorMessage);
                        if (errorMessage !== '') {
                            break;
                        }
                    }
                    else if (errorMessage !== '') {
                        errors.push(errorMessage);
                        break;
                    }
                }
            });

            //focus to first error.
            var firstError = $('.errorfield').first();
            if (firstError.length === 1) {
                firstError[0].focus();
            }

            //well, this is a mouthful
            miv.errors.error(errors);

            return errors.length === 0;
        };

        /**
         * Set a configuration option.
         * @function config
         * @memberof miv.validate
         * @param {String} option - The name of the option we are setting.
         * @param value - The value to which we wish to set the option.
         */
        this.config = function(option, value) {
            options[option] = value;
        };
    }

    /**
     * Miv validation tools
     * @namespace miv.validate
     * @memberof miv
     * @requires {@link miv.errors}
     * @author Jacob Saporito
     * @since 5.0
     * @version 0.6
     */
    miv.validate = new MivValidate();
})();
