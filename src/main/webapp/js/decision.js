'use strict';
/* global fnInitDataTable */
/**
 * Java Script for Dean's/VP's Final Decision/Recommendation
 */

$(document).ready( function() {
    // for more details look into /js/datatable/data_table_config.js
    var customizeSettingMap = {
            aFilterColumns : [],
            aaSorting : [[ 4, 'desc' ]],
    };

    /*var oTable = */fnInitDataTable('userlist', customizeSettingMap);
    /*var oappointmentlist = */fnInitDataTable('appointmentlist', customizeSettingMap);
} );
