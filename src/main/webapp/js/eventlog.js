'use strict';
/* global detect, fnInitDataTable */
/*
 * Event Log script for /WEB-INF/jsp/report.jsp
 */

var _navigator = {
    ua : detect.parse(navigator.userAgent)
};

$(document).ready(function() {

    $('.summary').containedStickyScroll({
        duration : _navigator.ua.browser.family === 'IE' ? 1 : 0,
        hideClose : false
    });

    fnInitDataTable('report', {
            aFilterColumns : [0,1,2,3,4,5],
            aaSorting      : [[ 6, 'desc' ]],
            aoColumnDefs   : [{"aTargets" : [0,1,2,3,4,5], "bSortable" : false},
                            //{"aTargets" : [6], "sSortDataType": "custom-sort", "sType": "numeric", "bSortable" : true},
                              {"aTargets" : [6], "bSortable" : true, "sType": "numeric"},
                              {"aTargets" : [7], "bVisible" : false}],
            fnRowCallback  : function (nRow, aData) {
                                 if (aData[7] === 'true') {
                                     $(nRow).addClass('eventChild');
                                 }
                             }
    });
});
