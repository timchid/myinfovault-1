'use strict';
/**
 * For use with rafselect.jsp
 */
$(document).ready(function() {
    var oSelect = {
        sLabel   : 'Action Type',
        sName    : 'actionType',
        aOptions : [
            {
                sLabel  : 'Appointment',
                sValue  : 'APPOINTMENT',
                oSelect : {
                    sLabel   : 'Option',
                    sTitle   : 'Appointment Option',
                    sName    : 'actionType',
                    aOptions : [
                        {
                            sLabel  : 'Endowed Chair',
                            sTitle  : 'Endowed Chair Appointment',
                            sValue  : 'APPOINTMENT_ENDOWED_CHAIR',
                            oSelect : {
                                sLabel   : 'Delegation Authority',
                                sTitle   : 'Endowed Chair Appointment Delegation Authority',
                                sName    : 'delegationAuthority',
                                aOptions : [
                                    {
                                        sLabel   : 'Non-Redelegated',
                                        sValue   : 'NON_REDELEGATED',
                                        bDefault : true
                                    }
                                ]
                            }
                        },
                        {
                            sLabel  : 'Endowed Professorship',
                            sTitle  : 'Endowed Professorship Appointment',
                            sValue  : 'APPOINTMENT_ENDOWED_PROFESSORSHIP',
                            oSelect : {
                                sLabel   : 'Delegation Authority',
                                sTitle   : 'Endowed Professorship Appointment Delegation Authority',
                                sName    : 'delegationAuthority',
                                aOptions : [
                                    {
                                        sLabel   : 'Non-Redelegated',
                                        sValue   : 'NON_REDELEGATED',
                                        bDefault : true
                                    }
                                ]
                            }
                        },
                        {
                            sLabel  : 'Endowed Specialist in CE',
                            sTitle  : 'Endowed Specialist in CE Appointment',
                            sValue  : 'APPOINTMENT_ENDOWED_SPECIALIST',
                            oSelect : {
                                sLabel   : 'Delegation Authority',
                                sTitle   : 'Endowed Specialist in CE Appointment Delegation Authority',
                                sName    : 'delegationAuthority',
                                aOptions : [
                                    {
                                        sLabel   : 'Non-Redelegated',
                                        sValue   : 'NON_REDELEGATED',
                                        bDefault : true
                                    }
                                ]
                            }
                        },
                        {
                            sLabel  : 'Initial Continuing (Unit 18)',
                            sTitle  : 'Initial Continuing Appointment',
                            sValue  : 'APPOINTMENT_INITIAL_CONTINUING',
                            oSelect : {
                                sLabel   : 'Delegation Authority',
                                sTitle   : 'Initial Continuing Appointment Delegation Authority',
                                sName    : 'delegationAuthority',
                                aOptions : [
                                    {
                                        sLabel   : 'Non-Redelegated',
                                        sValue   : 'NON_REDELEGATED',
                                        bDefault : true
                                    }
                                ]
                            }
                        },
                        {
                            sLabel  : 'via change in Department',
                            sTitle  : 'Appointment via change in Department',
                            sValue  : 'APPOINTMENT_CHANGE_DEPARTMENT',
                            oSelect : {
                                sLabel   : 'Delegation Authority',
                                sTitle   : 'Appointment via change in Department Delegation Authority',
                                sName    : 'delegationAuthority',
                                aOptions : [
                                    {
                                        sLabel   : 'Non-Redelegated',
                                        sValue   : 'NON_REDELEGATED',
                                        bDefault : true
                                    }
                                ]
                            }
                        },
                        {
                            sLabel  : 'via change in Title',
                            sTitle  : 'Appointment via change in Title',
                            sValue  : 'APPOINTMENT_CHANGE_TITLE',
                            oSelect : {
                                sLabel   : 'Delegation Authority',
                                sTitle   : 'Appointment via change in Title Delegation Authority',
                                sName    : 'delegationAuthority',
                                aOptions : [
                                    {
                                        sLabel   : 'Redelegated',
                                        sValue   : 'REDELEGATED',
                                        bDefault : true
                                    },
                                    {
                                        sLabel : 'Non-Redelegated',
                                        sValue : 'NON_REDELEGATED'
                                    }
                                ]
                            }
                        }
                    ]
                }
            },
            {
                sLabel  : 'Appraisal',
                sValue  : 'APPRAISAL',
                oSelect : {
                    sLabel   : 'Option',
                    sTitle   : 'Appraisal Option',
                    sName    : 'actionType',
                    aOptions : [
                        {
                            sLabel   : 'None',
                            sTitle   : 'Appraisal',
                            sValue   : 'APPRAISAL',
                            bDefault : true,
                            oSelect  : {
                                sLabel   : 'Delegation Authority',
                                sTitle   : 'Appraisal Delegation Authority',
                                sName    : 'delegationAuthority',
                                aOptions : [
                                    {
                                        sLabel : 'Redelegated',
                                        sValue : 'REDELEGATED'
                                    },
                                    {
                                        sLabel   : 'Non-Redelegated',
                                        sValue   : 'NON_REDELEGATED',
                                        bDefault : true
                                    }
                                ]
                            }
                        }
                    ]
                }
            },
            {
                sLabel  : 'Deferral',
                sValue  : 'DEFERRAL',
                oSelect : {
                    sLabel   : 'Option',
                    sTitle   : 'Deferral Option',
                    sName    : 'actionType',
                    aOptions : [
                        {
                            sLabel   : '1st Year',
                            sTitle   : '1st Year Deferral',
                            sValue   : 'DEFERRAL_FIRST_YEAR',
                            bDefault : true,
                            oSelect  : {
                                sLabel   : 'Delegation Authority',
                                sTitle   : '1st Year Deferral Delegation Authority',
                                sName    : 'delegationAuthority',
                                aOptions : [
                                    {
                                        sLabel   : 'Redelegated',
                                        sValue   : 'REDELEGATED',
                                        bDefault : true
                                    },
                                    {
                                        sLabel   : 'Non-Redelegated',
                                        sValue   : 'NON_REDELEGATED'
                                    }
                                ]
                            }
                        },
                        {
                            sLabel  : '2nd Year',
                            sTitle  : '2nd Year Deferral',
                            sValue  : 'DEFERRAL_SECOND_YEAR',
                            oSelect : {
                                sLabel   : 'Delegation Authority',
                                sTitle   : '2nd Year Deferral Delegation Authorit',
                                sName    : 'delegationAuthority',
                                aOptions : [
                                    {
                                        sLabel   : 'Redelegated',
                                        sValue   : 'REDELEGATED',
                                        bDefault : true
                                    },
                                    {
                                        sLabel   : 'Non-Redelegated',
                                        sValue   : 'NON_REDELEGATED'
                                    }
                                ]
                            }
                        },
                        {
                            sLabel  : '3rd Year',
                            sTitle  : '3rd Year Deferral',
                            sValue  : 'DEFERRAL_THIRD_YEAR',
                            oSelect : {
                                sLabel   : 'Delegation Authority',
                                sTitle   : '3rd Year Deferral Delegation Authority',
                                sName    : 'delegationAuthority',
                                aOptions : [
                                    {
                                        sLabel   : 'Non-Redelegated',
                                        sValue   : 'NON_REDELEGATED',
                                        bDefault : true
                                    }
                                ]
                            }
                        },
                        {
                            sLabel  : '4th Year',
                            sTitle  : '4th Year Deferral',
                            sValue  : 'DEFERRAL_FOURTH_YEAR',
                            oSelect : {
                                sLabel   : 'Delegation Authority',
                                sTitle   : '4th Year Deferral Delegation Authority',
                                sName    : 'delegationAuthority',
                                aOptions : [
                                    {
                                        sLabel   : 'Non-Redelegated',
                                        sValue   : 'NON_REDELEGATED',
                                        bDefault : true
                                    }
                                ]
                            }
                        }
                    ]
                }
            },
            {
                sLabel  : 'Emeritus Status',
                sValue  : 'EMERITUS_STATUS',
                oSelect : {
                    sLabel   : 'Option',
                    sTitle   : 'Emeritus Status Option',
                    sName    : 'actionType',
                    aOptions : [
                        {
                            sLabel   : 'None',
                            sTitle   : 'Emeritus Status',
                            sValue   : 'EMERITUS_STATUS',
                            bDefault : true,
                            oSelect  : {
                                sLabel   : 'Delegation Authority',
                                sTitle   : 'Emeritus Status Delegation Authority',
                                sName    : 'delegationAuthority',
                                aOptions : [
                                    {
                                        sLabel   : 'Non-Redelegated',
                                        sValue   : 'NON_REDELEGATED',
                                        bDefault : true
                                    }
                                ]
                            }
                        }
                    ]
                }
            },
            {
                sLabel  : 'Five Year Review',
                sValue  : 'FIVE_YEAR_REVIEW',
                oSelect : {
                    sLabel   : 'Option',
                    sTitle   : 'Five Year Review Option',
                    sName    : 'actionType',
                    aOptions : [
                        {
                            sLabel   : 'None',
                            sTitle   : 'Five Year Review',
                            sValue   : 'FIVE_YEAR_REVIEW',
                            bDefault : true,
                            oSelect  : {
                                sLabel   : 'Delegation Authority',
                                sTitle   : 'Five Year Review Delegation Authority',
                                sName    : 'delegationAuthority',
                                aOptions : [
                                    {
                                        sLabel   : 'Non-Redelegated',
                                        sValue   : 'NON_REDELEGATED',
                                        bDefault : true
                                    }
                                ]
                            }
                        },
                        {
                            sLabel  : 'Department Chair Review',
                            sTitle  : 'Department Chair Review - Five Year Review',
                            sValue  : 'FIVE_YEAR_REVIEW_DEPARTMENT_CHAIR',
                            oSelect : {
                                sLabel   : 'Delegation Authority',
                                sTitle   : 'Department Chair Review - Five Year Review Delegation Authority',
                                sName    : 'delegationAuthority',
                                aOptions : [
                                    {
                                        sLabel   : 'Non-Redelegated',
                                        sValue   : 'NON_REDELEGATED',
                                        bDefault : true
                                    }
                                ]
                            }
                        }
                    ]
                }
            },
            {
                sLabel  : 'Merit',
                sValue  : 'MERIT',
                oSelect : {
                    sLabel   : 'Option',
                    sTitle   : 'Merit Option',
                    sName    : 'actionType',
                    aOptions : [
                        {
                            sLabel   : 'None',
                            sTitle   : 'Merit',
                            sValue   : 'MERIT',
                            bDefault : true,
                            oSelect  : {
                                sLabel   : 'Delegation Authority',
                                sTitle   : 'Merit Delegation Authority',
                                sName    : 'delegationAuthority',
                                aOptions : [
                                    {
                                        sLabel   : 'Redelegated',
                                        sValue   : 'REDELEGATED',
                                        bDefault : true
                                    },
                                    {
                                        sLabel : 'Non-Redelegated',
                                        sValue : 'NON_REDELEGATED'
                                    }
                                ]
                            }
                        }
                    ]
                }
            },
            {
                sLabel  : 'Promotion',
                sValue  : 'PROMOTION',
                oSelect : {
                    sLabel   : 'Option',
                    sTitle   : 'Promotion Option',
                    sName    : 'actionType',
                    aOptions : [
                        {
                            sLabel   : 'None',
                            sTitle   : 'Promotion',
                            sValue   : 'PROMOTION',
                            bDefault : true,
                            oSelect  : {
                                sLabel   : 'Delegation Authority',
                                sTitle   : 'Promotion Delegation Authority',
                                sName    : 'delegationAuthority',
                                aOptions : [
                                    {
                                        sLabel : 'Redelegated',
                                        sValue : 'REDELEGATED'
                                    },
                                    {
                                        sLabel   : 'Non-Redelegated',
                                        sValue   : 'NON_REDELEGATED',
                                        bDefault : true
                                    }
                                ]
                            }
                        }
                    ]
                }
            },
            {
                sLabel  : 'Reappointment',
                sValue  : 'REAPPOINTMENT',
                oSelect : {
                    sLabel   : 'Option',
                    sTitle   : 'Reappointment Option',
                    sName    : 'actionType',
                    aOptions : [
                        {
                            sLabel   : 'Pre-Six Unit 18',
                            sTitle   : 'Reappointment: Pre-Six Unit 18',
                            sValue   : 'REAPPOINTMENT',
                            bDefault : true,
                            oSelect  : {
                                sLabel   : 'Delegation Authority',
                                sTitle   : 'Reappointment Delegation Authority',
                                sName    : 'delegationAuthority',
                                aOptions : [
                                    {
                                        sLabel : 'Redelegated',
                                        sValue : 'REDELEGATED',
                                        bDefault : true
                                    },
                                    {
                                        sLabel   : 'Non-Redelegated',
                                        sValue   : 'NON_REDELEGATED'
                                    }
                                ]
                            }
                        },
                        {
                            sLabel  : 'Endowed Chair',
                            sTitle  : 'Endowed Chair Reappointment',
                            sValue  : 'REAPPOINTMENT_ENDOWED_CHAIR',
                            oSelect : {
                                sLabel   : 'Delegation Authority',
                                sTitle   : 'Endowed Chair Reappointment Delegation Authority',
                                sName    : 'delegationAuthority',
                                aOptions : [
                                    {
                                        sLabel   : 'Non-Redelegated',
                                        sValue   : 'NON_REDELEGATED',
                                        bDefault : true
                                    }
                                ]
                            }
                        },
                        {
                            sLabel  : 'Endowed Professorship',
                            sTitle  : 'Endowed Professorship Reappointment',
                            sValue  : 'REAPPOINTMENT_ENDOWED_PROFESSORSHIP',
                            oSelect : {
                                sLabel   : 'Delegation Authority',
                                sTitle   : 'Endowed Professorship Reappointment Delegation Authority',
                                sName    : 'delegationAuthority',
                                aOptions : [
                                    {
                                        sLabel   : 'Non-Redelegated',
                                        sValue   : 'NON_REDELEGATED',
                                        bDefault : true
                                    }
                                ]
                            }
                        },
                        {
                            sLabel  : 'Endowed Specialist in CE',
                            sTitle  : 'Endowed Specialist in CE Reappointment',
                            sValue  : 'REAPPOINTMENT_ENDOWED_SPECIALIST',
                            oSelect : {
                                sLabel   : 'Delegation Authority',
                                sTitle   : 'Endowed Specialist in CE Reappointment Delegation Authority',
                                sName    : 'delegationAuthority',
                                aOptions : [
                                    {
                                        sLabel   : 'Non-Redelegated',
                                        sValue   : 'NON_REDELEGATED',
                                        bDefault : true
                                    }
                                ]
                            }
                        }
                    ]
                }
            }
        ]
    };

    // non-JavaScript selection fieldset
    var actionFieldset = $('fieldset.selection');

    // contains chosen action description
    var actionDescription = $('div.description');

    // option map for selection widget
    var selectionOptions = {
        oSelect      : oSelect,
        aSelected    : [// actionType and delegationAuthority name-value pairs
            {
                name  : 'actionType',
               /*
                * Using pop() as val() returns an array of strings
                * regardless if select field is a 'multiple' type.
                */
                value : $('select[name=actionType]').val().pop()
            },
            {
                name  : 'delegationAuthority',
                value : $('select[name=delegationAuthority]').val().pop()
            }
        ],
        sInstruction : $('legend', actionFieldset).text(),
        change       : function(event, ui) {
            // hide the chosen action description
            actionDescription.hide('slide', 200, function() {
                // title for the
                var submitTitle = ui.sDescription.length ?
                                  ui.sDescription :
                                  $(event.currentTarget).prop('title');

                // change the description text after hiding, then show
                $(this).text(ui.sDescription).show('slide', 200);

                //
                $('input[name=_eventId_select]').prop('title', 'Select ' + submitTitle)
                                                .prop('disabled', !ui.sDescription.length);
            });
        }
    };

    // call selection widget with options
    actionFieldset.selection(selectionOptions);
});
