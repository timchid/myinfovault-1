'use strict';
/* global fnInitDataTable */
//error notification
var bError = false;
var fnError = function(jqXHR, textStatus, errorThrown) {
     var bDebug = false;//in debug mode

     //if this is the first error and in debug mode
     if (!bError && bDebug) {
         //mark that an error has occurred
         bError = true;

         //notify the use that an error has occurred
         $('<div/>').attr('title', 'Error').text('An error has occurred:\n' +
                                                 'jqXHR:\n' + jqXHR +
                                                 ', textStatus:\n' + textStatus +
                                                 ', errorThrown:\n' + errorThrown).dialog({modal : true});
     }
};

/*
 * Initialization for grouplist.jsp
 */
$(document).ready(function() {
    fnInitDataTable('grouplist', {
        'aoColumnDefs'   : [{
                               'bSortable' : false,
                               'aTargets'  : [4]
                           }],
        'bHelpBar'       : true,
        'bAutoWidth'     : false,
        'aFilterColumns' : [0,1,2,3],
        'oLanguage'      : {
                               'sZeroRecords'    : 'No groups found',
                               'sLoadingRecords' : 'Loading...'
                           },
        'fnRowCallback'  : function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                               var self = this;

                               //the form associated with this row
                               var form = $('form', nRow);

                               //bind confirmation handler to the delete buttons
                               $('input[name=_eventId_delete]', nRow).unbind().click(function(event) {
                                   //stop form submission
                                   event.preventDefault();

                                   //add deleting class to row to highlight during confirmation
                                   $(nRow).addClass('deleting');

                                   //get the group name and ID associated with the selected button
                                   var sGroupName = $(nRow).children().first().text();
                                   var nGroupId = $('input[name=groupId]', form).val();

                                   //create confirmation dialog
                                   $('<div/>').attr('title', 'Please confirm').html('Delete group <em>' + sGroupName + '</em>?').dialog({
                                       modal         : true,
                                       closeOnEscape : false,
                                       buttons       : [
                                                         {
                                                             text  : 'Confirm',
                                                             click : function() {
                                                                         //close this dialog and re-trigger event
                                                                         $(this).dialog('close');

                                                                         var aoData = [];

                                                                         //add delete event to post data
                                                                         aoData.push({'name' : '_eventId', 'value' : 'delete'});
                                                                         aoData.push({'name' : 'groupId', 'value' : nGroupId});

                                                                         $.ajax({
                                                                             'dataType' : 'html',
                                                                             'type'     : 'POST',
                                                                             'url'      : form.attr('action'),
                                                                             'data'     : aoData,
                                                                             'success'  : function() {
                                                                                              //message for confirmation to display and its CSS classes
                                                                                              var sConfirmationMessage = '<div>Group <em>' + sGroupName + '</em> was deleted</div>';

                                                                                              var confirmationCell = $('<td/>').attr('colspan','5').html(sConfirmationMessage).addClass('deleteConfirmation');

                                                                                              $(nRow).html(confirmationCell).delay(3000).hide('fade', 1000, function() {
                                                                                                  self.fnDeleteRow(nRow);
                                                                                              });
                                                                                          },
                                                                             'error'    : fnError
                                                                         });
                                                                     }
                                                         },
                                                         {
                                                             text  : 'Cancel',
                                                             click : function() {
                                                                        //remove deleting class as delete is abandoned
                                                                        $(nRow).removeClass('deleting');

                                                                        //close this dialog
                                                                        $(this).dialog('close');
                                                                     }
                                                         }
                                                       ]
                                   });
                               });

                               return nRow;
                           }
    });
});
