'use strict';
/* exported insertCharacter */
var globalElement = null;
var globalValue;
var globalStart;
var globalEnd;
var globalRange;

window.onload = addEventHandlers;

function addEventHandlers()
{
    var inputTags = document.getElementsByTagName('input');
    var textareaTags = document.getElementsByTagName('textarea');

    for (var i = 0; i < inputTags.length; i++)
    {
        if (inputTags[i].type !== 'button' || inputTags[i].type !== 'image')
        {
            if (inputTags[i].type === 'text')
            {
                inputTags[i].onfocus = setElement;
                inputTags[i].onclick = setElement;
                inputTags[i].onchange = setElement;
            }
            else if (inputTags[i].type === 'radio' || inputTags[i].type === 'checkbox')
            {
                inputTags[i].onfocus = clearElement;
                inputTags[i].onclick = clearElement;
            }
        }
    }

    for (var j = 0; j < textareaTags.length; j++)
    {
            textareaTags[j].onfocus = setElement;
            textareaTags[j].onclick = setElement;
            textareaTags[j].onchange = setElement;
    }
}

function setElement()
{
    globalElement = window;
    globalValue = window.value;

    if (document.selection)
    {
        globalRange = document.selection.createRange();
    }
    else
    {
        globalStart = window.selectionStart;
        globalEnd = window.selectionEnd;
    }
}

function clearElement()
{
    globalElement = null;
}

function insertCharacter(inCharacter)
{
    if (globalElement !== null &&
        globalElement !== undefined)
    {
        //  debugger;
        if (document.selection)
        {
            globalElement.focus();

            var range = document.selection.createRange();

            // Insert character at cursor
            globalRange.text = inCharacter;

            range.text = globalRange.text;

            globalElement.focus();
        }
        else
        {
            var startString = globalValue.substring(0, globalStart);
            var endString = globalValue.substring(globalEnd, globalValue.length);

            globalElement.value = startString + inCharacter + endString;

            globalElement.selectionStart = globalStart + 1;
            globalElement.selectionEnd = globalStart + 1;

            globalElement.focus();
        }

        //toggleCharacterMap();

        return;
    }
}
//
// function toggleCharacterMap(map)
// {
//     var characterMap = document.getElementById(map);
//
//     if (characterMap.style.display === "block")
//     {
//         characterMap.style.display = "none";
//
//         globalElement = null;
//     }
//     else
//     {
//         characterMap.style.cursor = "default";
//         characterMap.style.position = "absolute";
//         //characterMap.style.backgroundColor = "white";
//         characterMap.style.display = "block";
//     }
// }

/*<span class="vim">
 vim:ts=8 sw=4 et sm:
</span>*/
