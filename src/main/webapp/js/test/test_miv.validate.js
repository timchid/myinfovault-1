'use strict';
/* global QUnit, miv, Modernizr */

var state = {
    errors : [],
    originalErrorFn : miv.errors.error,
    originalGetConstraintsFn : miv.validate.getConstraints,
    callCount : 0,
    reset : function() {
        state.errors = [];
        state.callCount = 0;
    }
};

/*
 * Simulate required constraint for test inputs.
 */
function getConstraintsMock(element) {
    return $('.vtest').has(element).length === 1 ? ['required'] : [];
}

QUnit.module('The miv.validate library', {
    beforeEach : function() {
        var inputsDiv = $('<div class="vtest">\
            <label for="testText">TestText Label</label>\
            <input id="testText" name="testText" type="text">\
            <input name="testEmail" type="email" data-errormessage="Fail {test}" data-test="email">\
            <input name="typeNumber" type="number" data-field="Number Test">\
            <label>Label surrounding <select name="testSelect">\
                    <option value="">Nothing</option>\
                    <option value="1">1</option>\
                    <option value="1.3">1.5<option>\
                    <option value="2">2</option>\
                </select>select</label>\
            <fieldset>\
                <legend>First radio</legend>\
                <input name="testRadio1" type="radio" value="0">\
                <input name="testRadio1" type="radio" value="1">\
            </fieldset>\
            <input name="testRadio2" type="radio" value="A">\
            <input name="testRadio2" type="radio" value="B">\
        </div>');

        $('#main').prepend(inputsDiv);

        //stub out the miv errors function
        miv.errors.error = function(errorArray) {
            state.errors = errorArray;
        };
    },
    afterEach : function() {
        $('.vtest').remove();
        state.reset();
        miv.errors.error = state.originalErrorFn;
        miv.validate.getConstraints = state.originalGetConstraintsFn;
    }
});

QUnit.test('Api Completeness', function(assert) {
    assert.ok(miv.validate.validate instanceof Function,
              'has a function to initiate validation');
    assert.ok(miv.validate.registerValidator instanceof Function,
              'has a function for registering custom validators');
    assert.ok(miv.validate.getConstraints instanceof Function,
              'has a function to get the list of constraints which apply to an element');
    assert.ok(miv.validate.config instanceof Function,
              'has a function for configuring the validation instance.');
});

QUnit.test('Validating valid inputs', function(assert) {
    var inputs = $('.vtest input, .vtest select');
    miv.validate.getConstraints = getConstraintsMock;

    //set input values to valid values for required
    inputs.not('select, [type=radio]').val('1');
    inputs.filter('[type=email]').val('japorito@gmail.com');
    inputs.filter('[type=radio]:even').prop('checked', true);
    $('option:nth-of-type(2)', inputs).prop('selected', true);

    assert.ok(miv.validate.validate(),
              'All valid inputs are found to be valid when native support is not used');
});

QUnit.test('Validating invalid inputs and getting appropriate error messages', function(assert) {
    miv.validate.getConstraints = getConstraintsMock;
    assert.notOk(miv.validate.validate(),
              'All invalid inputs are found to be invalid when native support is not used');

    assert.deepEqual(state.errors, [
            'TestText Label is required.',
            'Fail email',
            'Number Test is required.',
            'Label surrounding select is required.',
            'First radio is required.',
            'Field is required.'
        ],
        'Error messages are what we expected.'
    );
});

QUnit.test('Registration and use of custom validators', function(assert) {
    assert.throws(function() {
            miv.validate.registerValidator('noValidator');
        },
        /Invalid Validator/,
        'Errors when validator is not specified'
    );
    assert.throws(function() {
            miv.validate.registerValidator('badValidator', true);
        },
        /Invalid Validator/,
        'Errors when specified validator is not a function'
    );

    miv.validate.registerValidator('returnTrue', function() {
        state.callCount++;
        return true;
    });
    miv.validate.registerValidator('returnFalse', function() {
        return false;
    });
    miv.validate.registerValidator('returnMessage', function() {
        return 'Validation Failed';
    });

    var inputs = $('.vtest input, .vtest select');
    inputs.data('constraints', 'returnTrue');

    assert.ok(miv.validate.validate(), 'Test custom validator validated successfully with valid inputs');
    assert.equal(state.callCount,
                 6,
                 'Custom validator was called on each input the appropriate number of times.\
                  Only called ONCE for each set of radio buttons.');

    state.reset();
    inputs.data('constraints', 'returnFalse');
    inputs.removeData('errormessage');
    inputs.removeAttr('data-errormessage');

    assert.notOk(miv.validate.validate(), 'Custom validators properly validated invalid inputs by returning false.');
    assert.deepEqual(state.errors,
        [
            'TestText Label is invalid.',
            'Field is invalid.',
            'Number Test is invalid.',
            'Label surrounding select is invalid.',
            'First radio is invalid.',
            'Field is invalid.'
        ],
        'Use default error message when custom validator returns false'
    );

    state.reset();
    inputs.data('constraints', 'returnMessage');

    assert.notOk(miv.validate.validate(),
                 'Custom validators properly validated invalid inputs by returning an error message.');
    assert.deepEqual(state.errors,
        [
            'Validation Failed',
            'Validation Failed',
            'Validation Failed',
            'Validation Failed',
            'Validation Failed',
            'Validation Failed'
        ],
        'Error message from validator is used when supplied.'
    );

    inputs.data('constraints', 'returnTrue');
    $('.vtest').addClass('validated')
               .data('constraints', 'returnTrue');
    miv.validate.validate();
    assert.equal(state.callCount,
                 7,
                 'Non-input elements with the "validated" class can be validated with custom validators');
});

QUnit.test('Getting the list of constraints', function(assert) {
    var input = $('.vtest input:first');
    input.prop('required', true);

    if (Modernizr.input.required) {
        assert.deepEqual(miv.validate.getConstraints(input),
                         [],
                         'Defer to native validation when native support is available');
    }

    miv.validate.config('favorNative', false);

    input.removeAttr('required');
    input.data('constraints', 'oneConstraint;twoConstraints   ; thirdConstraint(with , arguments,too)');

    assert.deepEqual(miv.validate.getConstraints(input),
                     ['oneConstraint', 'twoConstraints', 'thirdConstraint(with , arguments,too)'],
                     'Get semi-colon separated constraints from data attribute.');

    input.prop('required', true);
    assert.deepEqual(miv.validate.getConstraints(input),
                     [
                         'oneConstraint',
                         'twoConstraints',
                         'thirdConstraint(with , arguments,too)',
                         'required'
                     ],
                     'Get semi-colon separated constraints from both data attribute and html validation attributes.');

    miv.validate.config('favorNative', true);
});

QUnit.test('Built in validation functions', function(assert) {
    var input = $('.vtest input:first');
    var radioInput = $('.vtest input[name=testRadio1]');

// required
    input.data('constraints', 'required');
    assert.notOk(miv.validate.validate(), 'Required invalid');
    input.val('ABCDEFG');
    assert.ok(miv.validate.validate(), 'Required valid');

// numeric
    input.data('constraints', 'numeric');
    assert.notOk(miv.validate.validate(), 'Numeric invalid');
    input.val(123.5);
    assert.ok(miv.validate.validate(), 'Numeric valid');

// integer
    input.data('constraints', 'integer');
    assert.notOk(miv.validate.validate(), 'Integer invalid');
    input.val(1234);
    assert.ok(miv.validate.validate(), 'Integer valid');

// min
    input.data('constraints', 'min(2345)');
    assert.notOk(miv.validate.validate(), 'Min invalid');
    input.val(3456);
    assert.ok(miv.validate.validate(), 'Min valid');

// max
    input.data('constraints', 'max(2345)');
    assert.notOk(miv.validate.validate(), 'Max invalid');
    input.val(1234);
    assert.ok(miv.validate.validate(), 'Max valid');

// step
    input.data('constraints', 'step(1000,5)');
    assert.notOk(miv.validate.validate(), 'Step invalid');
    input.val(1010);
    assert.ok(miv.validate.validate(), 'Step valid');

// between - tested with radiobuttons
    input.data('constraints', '');
    radioInput.data('constraints', 'between(.5  ,  1.5)');
    radioInput[0].checked = true;
    assert.notOk(miv.validate.validate(), 'Between invalid');
    radioInput[0].checked = false;
    radioInput[1].checked = true;
    assert.ok(miv.validate.validate(), 'Between valid');

// email
    input.data('constraints', 'email');
    assert.notOk(miv.validate.validate(), 'Email invalid');
    input.val('japorito@gmail.com');
    assert.ok(miv.validate.validate(), 'Email valid');

// pattern
    input.data('constraints', 'pattern(^not the same$)');
    assert.notOk(miv.validate.validate(), 'Pattern invalid');
    input.val('not the same');
    assert.ok(miv.validate.validate(), 'Pattern valid');

// maxlength
    input.data('constraints', 'maxlength(5)');
    assert.notOk(miv.validate.validate(), 'Maxlength invalid');
    input.val('lucky');
    assert.ok(miv.validate.validate(), 'Maxlength valid');

// minlength
    input.data('constraints', 'minlength(10)');
    assert.notOk(miv.validate.validate(), 'Minlength invalid');
    input.val('lucky duck');
    assert.ok(miv.validate.validate(), 'Minlength valid');
});
