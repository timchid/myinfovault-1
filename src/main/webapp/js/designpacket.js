'use strict';
/* global showLoadingDiv, key, hideLoadingMessage, buttonElement,
   sectionServlet, trim, escape, isScrolledIntoView, showMessage,
   Packet, packetId, packetName, userId, miv, Modernizr, packetIsNew:true */
/* exported doReset */

//perform on load
$(document).ready(function() {
    showLoadingDiv();

    function updateHistory()
    {
        if (Modernizr.history) {
            // most modern browsers, but not IE9 or lower
            if (packetIsNew) {
                //
                window.history.replaceState(window.history.state, '', '/miv/packet/add?packetname=' + packetName);
            }
            else {
                window.history.replaceState(window.history.state, '', '/miv/packet/design?packetId=' + packetId);
            }
        }
        else {
            // IE9 or lower
            if (packetIsNew) {
                // new packet, but can't update url without reloading the page
                // instead we'll send people back to the packet page when we
                // get into an invalid state.
                if (packetId === '') {
                    window.location.href = '/miv/packet';
                }
            }
            else {
                if (!window.location.search.includes('packetId=')) {
                    window.location.search = 'packetId=' + packetId;
                }
            }
        }
    }

    updateHistory();

    var linkEvent;
    var modified = false;
    var annotationsLinks = $('.annotationslink, .formatlink, .record-links a');

    $('input[type=checkbox]').change(function () {
        modified=true;
    });

    function buildPacket()
    {
        /* build packet */
        var packet = new Packet((packetId * 1) === 0 ? undefined : packetId,
                                packetName,
                                userId,
                                []);
        var packetContentMap = {};
        $('.displaycheck:checked').each(function (_index, item) {
            item = $(item);
            var itemKey = item.data('sectionid') + ':' + item.data('recordid');
            if (packetContentMap[itemKey] === undefined) {
                packetContentMap[itemKey] = {
                    sectionId : item.data('sectionid'),
                    recordId : item.data('recordid'),
                    displayParts : [item.data('displaycode')]
                };
            }
            else {
                packetContentMap[itemKey].displayParts.push(item.data('displaycode'));
            }
        });

        $.each(packetContentMap, function(_propertyName, item) {
            packet.addPacketItem(item.sectionId, item.recordId, item.displayParts);
        });

        return packet;
    }

    function addDesignPacketHandlers()
    {
        var checkBoxes = $('input[type=checkbox]');

        var $form = $('form#designForm');
        var topSaveButton = $('#savetop');
        var bottomSaveButton = $('#savebottom');

        try {
            /*
             * Submit is happening in two steps.
             * First, the headers are saved. If that succeeds, it then saves the packet
             * options. This is because the headers are not part actually a part of the
             * packet object, so it cannot use the Packet API.
             */
            $form.submit(function( event ) {
                event.preventDefault();
                topSaveButton.prop('disabled', true);
                bottomSaveButton.prop('disabled', true);

                savePacket();
            });
        }
        catch (exp) {
            topSaveButton.prop('disabled', false);
            bottomSaveButton.prop('disabled', false);
        }

        $('.headeredit').click(doHeaderEdit);
        $('.addnheaderedit').click(doAddnHeaderEdit);

        checkBoxes.click(validateContribution);

        $('.savepacketoptions').click( function(e) {
            savePacketOptions(e, checkBoxes);
        });

        topSaveButton.prop('disabled', false);
        bottomSaveButton.prop('disabled', false);

        hideLoadingMessage();
    }

    function handleFirstSave()
    {
        // re-enable the 'Continue Without Saving' button
        $('.ui-dialog-buttonset button')[1].disabled=false;

        annotationsLinks.each(function(_index, element) {
            element.href = element.href.replace('packetid=0', 'packetid=' + packetId);
        });

        dialog.find('.newMessage').hide();

        packetIsNew = false;
    }

    function savePacket(successCallback)
    {
        var packet = buildPacket();

        /* build headers array */
        var headers = [];
        $('.sectionhead input[type=checkbox], .sectionsubhead input[type=checkbox]').each(function(_index, element) {
            element = $(element);
            /*
            * Go from the checkbox to its parent, down to the nearby headertext span
            * Clone the span, and remove children so that only the actual text content
            * of the header (and not other children) is gotten.
            */
            var label = element.closest('.sectionhead, .sectionsubhead')
                               .find('.headertext')
                               .clone()
                               .children()
                               .remove()
                               .end()
                               .text()
                               .trim();
            var header = {
                userId : userId,
                sectionId : element.data('sectionid'),
                additionalInfoHeaderId : element.data('recordid'),
                included: element.is(':checked'),
                label : label
            };

            headers.push(header);
        });

        $.ajax({
            url : '/miv/packet/design/headers',
            data: JSON.stringify(headers),
            headers : {
                'Content-Type' : 'application/json'
            },
            method : 'POST',
            success : function() {
                miv.api.config('errorHandling', false);
                packet.save({
                    complete: saveFinished,
                    success: function(data, status) {
                        packetId = data.packetId; //jshint ignore:line
                        modified = false;
                        if (packetIsNew)
                        {
                            handleFirstSave();
                            updateHistory();
                        }

                        if (successCallback) { successCallback(); }
                    }
                });
            }
        })
        .error(function() {
            $('#CompletedMessage').empty()
            .append('<div id="errorbox">Save Failed</div>');

            var curtop = 0;
            if (isScrolledIntoView('#savetop')) {
                curtop = $('#savetop').position().top;
            }
            else if (isScrolledIntoView('#savebottom')) {
                curtop = ($('#savebottom').position().top) - 100;
            }
            showMessage('CompletedMessage',null,curtop);

            $('#savetop').prop('disabled', false);
            $('#savebottom').prop('disabled', false);
        });
    }

    var dialogSaveCallback = function(event) {
        $(this).dialog('close');
        savePacket(function() {
            //Briefly show save message before continuing on.
            setTimeout(function() {
                window.location.href = $(linkEvent.target).attr('href');
            }, 1500);
        });
    };

    var dialog = $('#leaveConfirmDialog').dialog({
        title : 'Save Before Leaving?',
        modal : true,
        autoOpen : false,
        minWidth : 350,
        open : function() {
            $(this).parent().find('.ui-dialog-buttonpane button:eq(0)').focus();
        },
        buttons: [
            {
                text : 'Save',
                click : dialogSaveCallback
            },
            {
                text : 'Continue Without Saving',
                disabled : packetIsNew,
                click : function(event) {
                    $(this).dialog('close');
                    window.location.href = $(linkEvent.target).attr('href');
                }
            },
            {
                text : 'Cancel',
                click : function() {
                    $(this).dialog('close');
                }
            }
        ]
    });

    annotationsLinks.click(function(event) {
        linkEvent = event;

        if (packetIsNew || modified) {
            event.preventDefault();

            dialog.dialog('open');
        }
    });

    setTimeout(function() {
        addDesignPacketHandlers();

        setupCollapsingDocuments();

        //For new packets, we want to save it as soon as it is added
        if (packetIsNew) {
            $('#savetop').trigger('click');
        }
    }, 200);
});

var curtop = 0;

function disableSave(e)
{
    var evt = e || window.event;
    var textFieldId = evt.target.id;
    var value = $.trim(evt.target.value);
    var buttonId = 'B' + textFieldId.substring(2);
    var buttonElement = $('#'+buttonId);
    buttonElement.prop('disabled', value === '');
    buttonElement.prop('title', value === '' ? 'Header can not be blank' : 'Save Header');
}

function doAddnHeaderEdit(e)
{
    var evt = e || window.event;
    evt.preventDefault();
    var editheaderlinkid = evt.target.id;
    var recordNumber = editheaderlinkid.substring(2);
    var headingLabel = $('#HL' + recordNumber).text();
    // replace no-break spaces with regular spaces to do the editing.
    // MIV escapes upper case <SUB>/<SUP> tags but honors lower case <sub>/<sup>
    // Since IE forces all HTML tags to upper case, replace upper case <SUB>/<SUP> tags with lower case.
    headingLabel = headingLabel.replace(/&nbsp;/gi, ' ').replace(/\xA0/g, ' ').replace(/\'/g, '&#39;').replace(/\"/g, '&quot;').replace(/<SUP>(.*?)<\/SUP>/g, '<sup>$1</sup>').replace(/<SUB>(.*?)<\/SUB>/g, '<sub>$1</sub>');

    var editHTML = '<span class="editheader"><input type="text" class="headerfield" id="TF'+recordNumber+'" ' +
            'value="'+headingLabel+'" size="50" title="Enter Header" maxlength="100"></span><input type="button" id="B'+recordNumber+'" value="Save" ' +
            'title="Save Header">' +
            '<input type="button" id="CB'+recordNumber+'" value="Cancel" title="Cancel Changes">';

    $('#T' + recordNumber).html(editHTML);
    $('#B'+recordNumber).click(saveAddHeader);

    doEdit(recordNumber);
}

function doHeaderEdit(e)
{
    var evt = e || window.event;
    evt.preventDefault();
    var editheaderlinkid = evt.target.id;
    var sectionid = editheaderlinkid.substring(2);
    var headingLabel = $('#HL' + sectionid).text();
    // replace no-break spaces with regular spaces to do the editing.
    // MIV escapes upper case <SUB>/<SUP> tags but honors lower case <sub>/<sup>
    // Since IE forces all HTML tags to upper case, replace upper case <SUB>/<SUP> tags with lower case.
    headingLabel = headingLabel.replace(/&nbsp;/gi, ' ').replace(/\xA0/g, ' ').replace(/\'/g, '&#39;').replace(/\"/g, '&quot;').replace(/<SUP>(.*?)<\/SUP>/g, '<sup>$1</sup>').replace(/<SUB>(.*?)<\/SUB>/g, '<sub>$1</sub>');
    var editHTML = '<span class="editheader"><input type="text" class="headerfield" ' +
              'id="TF'+sectionid+'" value="'+headingLabel+'" size="50" title="Enter Header" maxlength="100">' +
        '</span><input type="button" id="B'+sectionid+'" value="Save" title="Save Changed Header">' +
        '<input type="button" id="CB'+sectionid+'" value="Cancel" title="Cancel Changes">' +
        '<input type="button" id="RD'+sectionid+'" value="Restore Default Header" title="Restore Default Header">';

    $('#T' + sectionid).html(editHTML);
    $('#B'+sectionid).click(saveSectionHeader);
    $('#RD'+sectionid).click(restoreHeader);

    doEdit(sectionid);
}

function doEdit(sectionid)
{
    $('#TF'+sectionid).bind('selectionchange blur paste change keyup', disableSave);
    $('#CB'+sectionid).click(cancelHeaderChange);

    $('#H' + sectionid).hide();
    $('#T' + sectionid).show();
    $('#L' + sectionid).hide();
}

function doReset()
{
    $('#cbvalues').val('');
    window.location.reload();
    return;
}

function saveSectionHeader(e)
{
    var evt = e || window.event;
    if ($.trim(evt.target.value) === '')
    {
        buttonElement.disabled = true;
        return;
    }
    var savebuttonid = evt.target.id;
    var id = savebuttonid.substring(1);
    var displayOption = getDisplayOption(id);

    var mivsecheadServletURL = '/miv' + sectionServlet +
        '?action=save&sectionid=' + id + '&displayoption=' + displayOption;

    save(id, mivsecheadServletURL, true);
}

function getDisplayOption(id)
{
    return $('#sectionheader' + id + ':checkbox').exists() ? '1' : '0';
}

function restoreHeader(e)
{
    var evt = e || window.event;
    var resheadbuttonid = evt.target.id;
    var id = resheadbuttonid.substring(2);
    var displayOption = getDisplayOption(id);

    var mivsecheadServletURL = '/miv' + sectionServlet +
        '?action=restore&sectionid=' + id +'&displayoption=' + displayOption;

    save(id, mivsecheadServletURL, false);
}


function saveAddHeader(e)
{
    var evt = e || window.event;
    if (trim(evt.target.value) === '')
    {
        buttonElement.disabled = true;
        return;
    }
    var savebuttonid = evt.target.id;
    var id = savebuttonid.substring(1);

    var mivsecheadServletURL = '/miv' + sectionServlet +
        '?action=saveadditional&' + 'addheaderid=' + id;

    save(id, mivsecheadServletURL, true);
}

function cancelHeaderChange(e)
{
    var evt = e || window.event;
    var cancelbuttonid = evt.target.id;
    var id = cancelbuttonid.substring(2);

    $('#T' + id).hide().empty();
    $('#H' + id).show();
    $('#L' + id).show();
}

function save(id, requestURL, flag)
{
    var headertextField = $('#TF' + id);
    var newHeader = $.trim(headertextField.val());
    var headingLabelField = $('#HL' + id);
    if (flag)
    {
        headingLabelField.html(newHeader
            .replace(/&/g, '&amp;')
            .replace(/</g, '&lt;')
            .replace(/>/g, '&gt;')
            .replace(/ /g, '&nbsp;')
            .replace(/\"/g, '&quot;')
            .replace(/\'/g, '&#39;')
            .replace(/&lt;sup&gt;(.*?)&lt;\/sup&gt;/g, '<sup>$1</sup>')
            .replace(/&lt;sub&gt;(.*?)&lt;\/sub&gt;/g, '<sub>$1</sub>'));
    }

    $('#T' + id).hide().empty();
    $('#H' + id).show();
	$('#L' + id).show();

    newHeader = escape(newHeader).replace(/\+/g, '%2B').replace(/%20/g, '+');
    var mivsecheadServletURL = requestURL + '&header=' + newHeader;
    sendRequest(mivsecheadServletURL, id, flag);
}

function sendRequest(requestURL, id, flag)
{
    $.get(requestURL)
    .done(function(response) {
        if (!flag) {
            $('#HL' + id).empty().append(response);
        }
    });
}

function validateContribution(e)
{
    var evt = e || window.event;
    var targetID = evt.target.id;
    var clickedCBObj = $('#'+targetID);
    var contributionOrGoalInfoId = 'contributioninfo';
    var contributionorgoal = $('#' + targetID + 'contribution');

    if (!contributionorgoal.exists())
    {
        contributionorgoal = $('#' + targetID + 'goal');
        contributionOrGoalInfoId = 'goalinfo';
    }

    if (contributionorgoal.exists())
    {
        var contributionInfoObj = $('#' + targetID + contributionOrGoalInfoId);
        if (clickedCBObj.is(':checked'))
        {
            contributionorgoal.prop('disabled', false);
            contributionorgoal.prop('title', 'If checked, this contribution will appear in the packet');
            contributionInfoObj.removeClass('showcontribution');
            contributionInfoObj.addClass('hidecontribution');
        }
        else
        {
            contributionorgoal.prop('disabled', true);
            if (contributionorgoal.is(':checked'))
            {
                contributionorgoal.prop('title', 'This contribution will not appear in the packet');
                contributionInfoObj.removeClass('hidecontribution');
                contributionInfoObj.addClass('showcontribution');
            }
        }
    }
}

var designPacketList;// =  undefined;
function savePacketOptions(evt, list)
{
    var savebutton = evt.target;
    savebutton.disable = true;
    curtop = 0;
    if (savebutton.offsetParent) {
        do {
            curtop += savebutton.offsetTop;
            //alert("curtop (onclick) == " + curtop);
        } while (savebutton = savebutton.offsetParent); //jshint ignore:line
    }
    designPacketList = [];
    list.each(function(index, tag) {
            //if the checkbox is disabled, enable it
            if (tag.disabled) {
                tag.disabled = false;
                // maintain a list of disabled checkboxes to make those checkboxes disabled again
                // once the records are saved
                designPacketList.push(tag);
            }
        }
    );
}



function saveFinished(data, evt)
{
    var msgDiv = $('#CompletedMessage');
    msgDiv.empty();
    var resultObj = data.responseJSON;

    if (resultObj.errors)
    {
        for (var i = 0; i < resultObj.errors.length; i++) {
            msgDiv.append('<div id="errorbox">'+resultObj.errors[i]+'</div>');
        }
    }
    else
        {
        var successMsg = 'Your Packet Design options have been saved.';

        msgDiv.append('<div id="successbox">'+successMsg+'</div>');
    }

    var curtop = 0;
    if (isScrolledIntoView('#savetop')) {
        curtop = $('#savetop').position().top;
    }
    else if (isScrolledIntoView('#savebottom')) {
        curtop = ($('#savebottom').position().top) - 100;
    }
    showMessage('CompletedMessage',null,curtop);

    $('#savetop').prop('disabled', false);
    $('#savebottom').prop('disabled', false);
    $('#cbvalues').val(resultObj.hiddenValue);

    $.each(designPacketList,
        function(index, tag) {
            tag.disabled = true;
        }
    );
}

/*
 * Define design document body expand/collapse events and handlers
 */
function setupCollapsingDocuments() {
    //tooltip titles for document heads
    var sExpandTitle = 'Select to expand this document';
    var sCollapseTitle = 'Select to collapse this document';

    //design document headers
    var documentHeads = $('div.documenthead');

    //show document head's body
    var fnShow = function(documentHead) {
        var documentBody = documentHead.addClass('active').attr('title', sCollapseTitle).next().slideDown('slow');

        //add/remove hover class on list block inputs/links focusin/focusout
        documentBody.find('input,a').focusin(function(event) {
            $(event.currentTarget).closest('div.listblock').addClass('hover');
        }).focusout(function(event) {
            $(event.currentTarget).closest('div.listblock').removeClass('hover');
        });

        //reorder tab indexes
        $(':focusable').each(function(index, element) {
            $(element).attr('tabindex', index);
        });
    };

    //hide document head's body
    var fnHide = function(element) {
        element.removeClass('active').attr('title', sExpandTitle).next().slideUp('slow');
    };

    //toggle the selected document head's body
    var fnToggle = function(event) {
        var documentHead = $(event.currentTarget);

        if (event.target.nodeName !== 'INPUT')
        {
            //show document body if hidden, hide otherwise
            documentHead.next().is(':hidden') ?
                fnShow(documentHead) :
                fnHide(documentHead); //jshint ignore:line
        }
    };

    //on design document select
    documentHeads.click(fnToggle).attr('title', sExpandTitle//all initially hidden
        ).attr('tabindex', 100
        ).on('keydown', function(event) {
            if (event.keyCode === key.ENTER || event.keyCode === key.SPACE) {
                fnToggle(event);
            }
        });

    // Remove Clear/UnClear Button when no check boxes available
    $('div[id^="document"]').each(function( index ) {
        if ($('input[type=checkbox]:not(:disabled)',$(this)).size() === 0) {
            $('.buttonset', $(this)).remove();
        }
    });

    var documentsWithChanges = documentHeads.parent().has('.new, .changed');

    if (documentsWithChanges.length > 0) {
        documentsWithChanges.each(function(_index, element) {
            fnShow($(element).find('.documenthead'));
        });
    }
    else {
        //show first document and hideinitially
        fnShow(documentHeads.first());
    }
}

/*<span class="vim">
 vim:ts=8 sw=4 et sr:
</span>*/
