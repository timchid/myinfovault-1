'use strict';
/*
 * Combo-box widget that can be attached to an existing <select> control,
 * or attached to an <input> element to behave list jquery.ui.autocomplete
 */
(function($) {
    // Having the 2nd parameter (in the next line) extends autocomplete, but
    // we might want to follow the example more closely and use composition.
    // If we do that, we'll need the 'source', 'select', 'change' etc. options.
    var self;

    $.widget('miv.combobox', $.ui.autocomplete, {
        options : { something : '' },

        _create : function() {
            self = this;
            // var orig_element = this.element;
            // what we do now depends on if the original element is an <input> or a <select>
            // ...
        }
    });
})(jQuery);
