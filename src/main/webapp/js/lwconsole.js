'use strict';//Might not be able to use strict? Depends on what is referred to by 'this'
/* exported lw_console */
/*
 * Light-Weight Console (lw_console) to aid debugging.
 * This is especially useful for focus/blur event tracking, because alerts
 * and external consoles can affect the focus, interfering with what you're
 * trying to debug.
 *
 * Usage:
 *  Add a place in your page that will be the console output window, such as:
 *    <div id="my_console" class="console_win"></div>
 *  Create the console object, attaching it to your element:
 *    var my_con = new lw_console('my_console'); // Use the id of the div
 *  Write output to the console anywhere in your javascript:
 *    my_con.write('leaving method foo and returning n='+n);
 *  You can clear the current content of the console:
 *    my_con.clear();
 *  or clear the console and reset the line numbering:
 *    my_con.reset();
 *
 *  The console will also append a div to your page if you don't provide one:
 *    var my_con = new lw_console();
 *
 *  Classes are attached to the console and output so you can apply CSS styles.
 *  The structure is:
 *    <div class="lw_console">
 *      <p class="lw_line">
 *        <span class="lw_count">N: </span><span class="lw_message">output</span>
 *      </p>
 *    </div>
 *
 * See the example at http://psl-225.ucdavis.edu/console/events2.html
 */
 //lw_console is a constructor, so...
 /* jshint -W040 */
 //Constructors whose name start with a capital letter do not need that.
 //This now MUST be used as a constructor, because use strict does not allow
 //the 'this' variable to refer to the window object.
var __lw_console_count = 0;
function lw_console(id)
{
    /* Write a line to the console */
    this.write = function(text) {
        var pLine = document.createElement('p');
        var sCount = document.createElement('span');
        var sMsg  = document.createElement('span');

        pLine.style.margin = '0';
        // add classes to the line, the count span, and the message span
        pLine.setAttribute('class', 'lw_line');
        sCount.setAttribute('class', 'lw_count');
        sMsg.setAttribute('class', 'lw_message');

        sCount.appendChild(
            document.createTextNode('' + (++this.linecount) + ': ')
        );
        sMsg.appendChild(
            document.createTextNode(text)
        );

        pLine.appendChild(sCount);
        pLine.appendChild(document.createTextNode(' '));
        pLine.appendChild(sMsg);

        this.lwconsole.appendChild(pLine);
        pLine.scrollIntoView(false);
    };

    /* Clear all the current lines displayed in the console */
    this.clear = function() {
        var elem;
        do {
            elem = this.lwconsole.firstChild;
            if (elem) { this.lwconsole.removeChild(elem); }
        } while (elem);
    };

    /* Clear all lines from the console and reset the line numbering */
    this.reset = function() {
        this.clear();
        this.linecount = 0;
    };

    /* Append a CSS class to the console. Used internally. */
    this.appendClass = function(newClassName) {
        var orig_class = this.lwconsole.className;
        var class_string =
            orig_class.length > 0 ?  orig_class + ' ' : '';
        this.lwconsole.className = class_string + newClassName;
    };

    this.count = ++__lw_console_count;
    this.id = id;
    this.linecount = 0;
    this.lwconsole = document.getElementById(id);

    // If no ID was passed or getElementById returned nothing
    // we'll create a new element and make it the last child
    // of the body element.
    if (this.lwconsole === null || this.lwconsole === undefined)
    {
        this.lwconsole = document.createElement('div');
        this.lwconsole.id = '__lw_debug_console_' + this.count;
        document.body.appendChild(this.lwconsole);
    }

    this.appendClass('lw_console');

    // Set the height and width if the caller hasn't set them,
    // or if they are way too small.
    if (this.lwconsole.clientHeight < 10) {
        this.lwconsole.style.height = '12em';
    }
    if (this.lwconsole.clientWidth < 10) {
        this.lwconsole.style.width = '90%';
    }

    this.lwconsole.style.border = '1px solid black';
    this.lwconsole.style.overflow = 'auto';
    this.lwconsole.style['overflow-y'] = 'scroll'; // doesn't work
}
