'use strict';
/* exported getElementByName, getElementById, validateForm, getClickedButton, disable, enable, getLabel, setErrorLabel */
/* global miv, getErrorMessage */

/**
 * form validation script
 * Dependencies :
 * Order of the js files to include in jsp file
 *
 *   1. /js/mivcommon.js
 *   2. /js/error.messages.js
 *   3. /js/validation.js
 *   4. /js/form.validation.js
 *
 */

$(document).ready(function() {
    $('form input[type=submit], form button[type=submit]').click(function() {
        var $self = $(this);
        $('input[type=submit], button[type=submit]', $self.parents('form')).removeAttr('clicked');
        $self.attr('clicked', 'true');
    });

});

function validateForm( formObj, profile, errormessageObj, errorClass  )
{
    var successful = true;
    removeErrorClasses(errorClass);

    var errormessage = errormessageObj;
    if (errormessage.exists()) {
        errormessage.html('');
    }
    errormessage.css('display', 'none');

    if (typeof profile !== 'undefined') {
        var results = miv.validation.validate(formObj, profile);
        var fieldList = '';
        var s = '';

        successful = results.isSuccessful();
        if (!successful) {
            fieldList += listErrors(results.getOrderedErrorList(), profile.errormap);
        }

        if (results.hasInvalid() || results.hasMissing()) {
            s = results.getErrorCount() > 1 ?  'Errors: ' : 'Error: ';
            s = '<div id="errorbox">' + s + '<ul>' + fieldList + '</ul></div>';

            if (errormessage.exists()) {
                errormessage.html(s);
            }
            errormessage.css('display', 'block');
            scrollTo(errormessage.prop('id'));

            showErrorFields(results.getErrorFields(), errorClass);
            focusToErrorField(results.getFirstErrorField());
        }

    }

    return successful;
}


/**
 * get clicked button object
 */
function getClickedButton(formObj)
{
    return $('input[clicked=true], button[clicked=true]', formObj);
}
// getClickedButton


/**
 * Get jQuery element by ID. If none, by name.
 *
 * @param id ID of the element
 * @returns jQuery object
 */
function getElementById(id)
{
    if (jQuery.type(id) === 'string') {
        // These strings MUST be quoted with double quotes on the outside.
        // Otherwise, it breaks with a Syntax error, unrecognized exception
        // if a selector is handed in.
        var $element = $("*[id='"+id+"']"); // jshint ignore:line

        // return element for ID, otherwise try name
        return $element.exists() ? $element : $("*[name='"+id+"']"); //jshint ignore:line
    }

    return undefined;
}
// getElementById

/**
 * Get jQuery element by name. If none, by ID.
 *
 * @param name name of the element
 * @returns jQuery object
 */
function getElementByName(name)
{
    if (jQuery.type(name) === 'string') {
        // These strings MUST be quoted with double quotes on the outside.
        // Otherwise, it breaks with a Syntax error, unrecognized exception
        // if a selector is handed in.
        var $element = $("*[name='"+name+"']"); //jshint ignore:line

        // return element for name, otherwise try ID
        return $element.exists() ? $element : $("*[id='"+name+"']"); //jshint ignore:line
    }

    return undefined;
}
// getElementByName


/**
 * disable jquery object
 */
function disable(control) {
    if (control.exists()) {
        control.prop('disabled', true);
    }
}
// disable()


/**
 * enable jquery object
 */
function enable(control) {
    if (control.exists()) {
        control.removeProp('disabled');
    }
}
// enable()

/**
 * remove error classes from all error fields
 */
function removeErrorClasses(errorClass) {
    if (errorClass === undefined) {
        errorClass = 'errorfield';
    }

    $('.' + errorClass).removeClass(errorClass);
}
// removeErrorClasses()


/**
 * set error fields class
 */
function showErrorFields(errorFields, errorClass) {
    if (errorClass === undefined) {
        errorClass = 'errorfield';
    }
    removeErrorClasses(errorClass);

    for (var index=0; index < errorFields.length; index++) {
        var $field = errorFields[index];

        if ($field !== null && $field.exists()) {
            $field.addClass(errorClass);
        }
    }
}
// showErrorFields()


/**
 * set focus to first error field
 */
function focusToErrorField(firstErrorField) {
    var $self = firstErrorField.field_element;

    if ($self.exists()) {
        if (jQuery.type(customValidationResult) === 'function') {
            customValidationResult($self);
        }

        $self.focus();
    }
}
// focusToErrorField()

// this function must be override from other js
function customValidationResult(field) {} // jshint ignore:line

/**
 * Get the error label for an element
 * @param elemId : ID of the element
 * @return It returns errorlabel if not that label otherwise element id
 */
function getLabel(elemId)
{
    return getElementLabel(getElementById(elemId)) || elemId;
}
// getLabel
/**
 * Get a label associated with the jQuery element.
 *
 * @param element jQuery element selector
 * @return label for the given element
 */
function getElementLabel(element)
{
    // no label for non existing element
    if (!element.exists()) { return ''; }

    // get the label for given element
    var elementLabel;

    // if element represents multiple fields, find the closest legend
    if (element.size() > 1)
    {
        elementLabel = $('legend', element.closest('fieldset:has(legend)'));
    }
    // if a field set, use its own legend
    else if (element.is('fieldset'))
    {
        elementLabel = element.find('legend:first');
    }
    // otherwise, use its label tag
    else
    {
        // if element is nested in label
        elementLabel = element.closest('label');

        // otherwise, use the element ID
        if (!elementLabel.length) {
            elementLabel = $('label[for="' + element.attr('id') + '"]');
        }
    }

    /*
     * Get a string from the label:
     *  - Use the "errorlabel" attribute if present
     *  - else, use the label text
     *  - else, use the "errorLabel" data property
     *  - else, use the element ID
     */
    console.log(element.data('errorLabel'));
    var sLabel = elementLabel.attr('errorlabel') ||
                 elementLabel.text() ||
                 element.data('errorLabel') ||
                 element.attr('id');

    // Remove colon from label if present
    if (sLabel[sLabel.length-1] === ':') {
        sLabel = sLabel.slice(0, -1);
    }

    return sLabel;
}
// getElementLabel


/**
 * add error label attribute to label tag
 */
function setErrorLabel(parentElementObj, partialField,label, includelabel)
{
    if (!$(parentElementObj).exists()) { return; }

    var $element = $(parentElementObj).find('[id$="'+partialField+'"], [name$="'+partialField+'"]');

    if (!$element.exists()) { return; }

    var $elementLabel;// = undefined

    if ($element.is('fieldset')) {
        $elementLabel = $element.find( 'legend:first' );
    }
    else {
        $elementLabel = $('label[for="' + $element.prop('id') + '"]');
    }

    if (typeof $elementLabel !== 'undefined')
    {
        if (typeof includelabel === 'undefined') { includelabel = true; } // default to true

        if (includelabel && $.trim($elementLabel.text()).length > 0) {
            label = label + ' ' + $.trim($elementLabel.html());
        }

        $elementLabel.attr('errorlabel', label);
    }
}
// setErrorLabel


/**
 * list form errors
 */
function listErrors(errorList, formProfileErrormap)
{
    var errorItems = '';
    var mesg;
    for (var i=0; i<errorList.length; i++) {
        mesg = undefined;
        var $fieldname = errorList[i].field_element;

        if ($fieldname.exists() && $fieldname.prop('id') !== 'citation' ) {
            var fieldId = $fieldname.prop('id');
            var errorcode = errorList[i].error_code;

            if (errorcode.match(/CUSTOM_ERROR_MSG\s*:/) !== null) {
                errorcode = 'CUSTOM_ERROR_MSG';
                mesg = errorcode.replace(/CUSTOM_ERROR_MSG\s*:/,'');
            }
            else if (typeof formProfileErrormap !== 'undefined' &&
                    typeof formProfileErrormap[errorcode] !== 'undefined') {
                mesg = formProfileErrormap[errorcode];
            }

            if (typeof mesg === 'undefined' || (typeof mesg !== 'undefined' && mesg.trim() === 0)) {
                mesg = getErrorMessage(errorcode);
            }

            errorItems += '<li class="' + fieldId.toUpperCase() + ' ' + errorcode.toUpperCase() + '">' +
                          getElementLabel($fieldname) + ' : ' + mesg + ' </li>';
        }
    }

    return errorItems;
}
// listErrors()


/** This was called only in disclosureform.js but has been commented-out
 * Adding clicked button with form
 * by appending hidden field with form
 *
 * because Some forms need to send the information about clicked buttons
 * /
function addClickedButtonToForm(formObj, buttonObj)
{
    var id = buttonObj.prop('id');
    var name = buttonObj.prop('name');
    var value = buttonObj.val();

    if (typeof name === 'undefined' || $.trim(name).length === 0) {
        name = id;
    }

    if (typeof value === 'undefined' || $.trim(value).length === 0) {
        value = id;
    }

    formObj.append( $('<input/>').prop('type', 'hidden').prop('id', id).prop('name', name).val(value) );
}*/
// addClickedButtonToForm


/** I didn't find this called from anywhere ( find src/main -type f -exec fgrep printFormElemets {} + )
 * print the form elements
 * just for validation use
 * /
function printFormElemets(formObj)
{
    $.map(formObj.serializeArray(), function(map, index){
        alert(map.name +' == >'+map.value);
     });
}*/
//printFormElemets

/*<span class="vim">
 vim:ts=8 sw=4 et sr:
</span>*/
