'use strict';
/* global key */

$(function() {
    /**
     * @fileOverview Workflow widget.
     * @author Craig Gilmore
     * @version 1.0
     * @namespace workflow
     * @memberof jQuery.custom
     * @example
     * // instantiate workflow in a form (each of fieldsets will be treated as steps)
     * var form = $('form').workflow({
     *     beforeStep : function(event, ui) {
     *         // set legend for each step
     *         $('legend', ui.newStep).text('Step #' + ui.newCount);
     *     },
     * });
     *
     * // log current step index, initially 0
     * console.log('Currently at step #' + form.workflow('step'));
     *
     * // get workflow instance
     * var instance = form.data('custom-workflow');
     *
     * // increment to next step
     * console.log('Now on step #' + instance.next().step());
     *
     */
    $.widget('custom.workflow', {
        /**
         * @member workflow#options
         * @description Widget options
         * @property {String}          [stepSelector=fieldset]       Selector of elements to be considered steps in the workflow
         * @property {String}          [effect=blind]                Transition for moving to the next step. May be 'blind' or 'slide'
         * @property {String}          [nextButtonText=Next]         Default text of the "next" navigation button
         * @property {String}          [previousButtonText=Previous] Default text of the "previous" navigation button
         * @property {workflow#create} [create=$.noop]               Called after workflow is created
         */
        options  : {
            stepSelector        : 'fieldset',
            effect              : 'blind',
            nextButtonText      : 'Next',
            previousButtonText  : 'Previous',
            create              : $.noop
        },

        /**
         * @private
         * @function workflow#_create
         * @description Initialize this widget.
         */
        _create : function() {
            var self = this;

            // workflow steps
            var steps = $('> ' + self.options.stepSelector, this.element);

            /**
             * @typedef {Object} workflow#ui
             * @description State of the workflow user interface
             * @property {jQuery#Object} steps       All workflow steps
             * @property {jQuery#Object} first       First workflow step
             * @property {jQuery#Object} last        Last workflow step
             * @property {jQuery#Object} current     Current workflow step (equal to the former step for 'before' events, the new step otherwise)
             * @property {jQuery#Object} newStep     Workflow step to which we are transitioning
             * @property {jQuery#Object} formerStep  Workflow step from which we are transitioning
             * @property {Number}        newCount    Index of the step to which we are transitioning
             * @property {Number}        formerCount Index of the step from which we are transitioning
             */
            var ui = {
                steps   : steps,
                first   : steps.first(),
                last    : steps.last(),
                current : $()
            };

            // add step class and order to steps
            ui.steps.each(function(index, step) {
                $(step).addClass('step').data('order', index);
            });

            // Next button
            var nextButton = $('<button type="button">').text(self.options.nextButtonText).addClass('next').prependTo(this.element);

            // Previous button
            var previousButton = $('<button type="button">').text(self.options.previousButtonText).addClass('previous').prependTo(this.element);

            // move to new step
            var fnStep = function(newStep, event) {
                var newCount = steps.index(newStep);
                var formerCount = steps.index(ui.current);

                var bForward = newCount > formerCount;

                /*
                 * Step only if new step exists and is not current and current
                 * is not last/first if moving forward/backward.
                 */
                if (!ui.current.is(newStep) &&
                    !ui.current.is(bForward ? ui.last : ui.first) &&
                    newCount !== -1) {
                    // update UI new/former step references
                    ui.newStep = newStep;
                    ui.formerStep = ui.current;
                    ui.newCount = newCount;
                    ui.formerCount = formerCount;

                    var result;
                    if (bForward) {
                        /**
                         * @event workflow#workflowbeforeNext
                         * @description Called before moving to next step
                         * @param {jQuery#Event} event Workflow beforeNext event
                         * @param {workflow#ui}  ui
                         */
                        result = self._trigger('beforeNext', event, ui);
                    }
                    else {
                        /**
                         * @event workflow#workflowbeforePrevious
                         * @description Called before moving to previous step
                         * @param {jQuery#Event} event Workflow beforePreviou event
                         * @param {workflow#ui}  ui
                         */
                        result = self._trigger('beforePrevious', event, ui);
                    }

                    /**
                     * @event workflow#workflowbeforeStep
                     * @description Called before moving to new step
                     * @param {jQuery#Event} event Workflow beforeStep event
                     * @param {workflow#ui}  ui
                     */
                    if (result !== false &&
                        self._trigger('beforeStep', event, ui) !== false)
                    {
                        // update references and trigger callbacks
                        var fnComplete = function() {
                            // new step becomes the current step
                            ui.current = ui.newStep;

                            // disable/enable navigation buttons and reset text to default
                            previousButton.prop('disabled', ui.current.is(ui.first)).text(self.options.previousButtonText);
                            nextButton.prop('disabled', ui.current.is(ui.last)).text(self.options.nextButtonText);

                            if (bForward) {
                                /**
                                 * @event workflow#workflownext
                                 * @description Called after moving to next step
                                 * @param {jQuery#Event} event Workflow next event
                                 * @param {workflow#ui}  ui
                                 */
                                self._trigger('next', event, ui);
                            }
                            else {
                                /**
                                 * @event workflow#workflowprevious
                                 * @description Called after moving to previous step
                                 * @param {jQuery#Event} event Workflow previous event
                                 * @param {workflow#ui}  ui
                                 */
                                self._trigger('previous', event, ui);
                            }

                            /**
                             * @event workflow#workflowstep
                             * @description Called after moving to the new step
                             * @param {jQuery#Event} event Workflow step event
                             * @param {workflow#ui}  ui
                             */
                            self._trigger('step', event, ui);
                        };

                        // animate-hide current step if exists
                        if (ui.current.size()) {
                            ui.current.hide(self.options.effect, {
                                direction : self.options.effect === 'blind' ? null : (bForward ? 'left' : 'right'),
                                complete  : function() {
                                    // when moving forward, current step is now complete
                                    if (bForward) {
                                        ui.current.addClass('complete');
                                    }

                                    // new step is not complete until the next transition is completed
                                    ui.newStep.removeClass('complete');

                                    // animate-show new step
                                    ui.newStep.show(self.options.effect, {
                                        direction : self.options.effect === 'blind' ? null : (bForward ? 'right' : 'left'),
                                        complete  : fnComplete
                                    });
                                }
                            });
                        }
                        // otherwise, call complete function directly
                        else {
                            fnComplete();
                        }
                    }
                }

                return self;// this workflow for chaining
            };

            /**
             * @method workflow#step
             * @description Get the index of the current step
             * @returns {Number} Index of the current step
             */
            /**
             * @method workflow#step
             * @description Move to step
             * @param {Number} index Step index
             * @fires workflow#workflowbeforeStep
             * @fires workflow#workflowbeforeNext
             * @fires workflow#workflowbeforePrevious
             * @fires workflow#workflowstep
             * @fires workflow#workflownext
             * @fires workflow#workflowprevious
             */
            this.step = function(index) {
                // return current index if no argument
                return typeof index === 'undefined' ?
                       ui.steps.index(ui.current) :
                       fnStep($(ui.steps.get(index)));
            };

            /**
             * @method workflow#next
             * @listens jQuery#click
             * @description Move to next step
             * @param {jQuery#Event} [event] Event triggering the transition to the next step
             * @fires workflow#workflowbeforeStep
             * @fires workflow#workflowbeforeNext
             * @fires workflow#workflowstep
             * @fires workflow#workflownext
             */
            this.next = function(event) {
                return fnStep(ui.current.nextAll().not('.skip').first(), event);
            };

            /**
             * @method workflow#previous
             * @listens jQuery#click
             * @description Move to previous step
             * @param {jQuery#Event} [event] Event triggering the transition to the previous step
             * @fires workflow#workflowbeforeStep
             * @fires workflow#workflowbeforePrevious
             * @fires workflow#workflowstep
             * @fires workflow#workflowprevious
             */
            this.previous = function(event) {
                // previous is the last completed step
                return fnStep(ui.current.prevAll('.complete').first(), event);
            };


            // attach navigation events to buttons
            previousButton.on('click', this.previous);
            nextButton.on('click', this.next);

            /**
             * @function workflow~fnChange
             * @listens jQuery#change
             * @description Triggers workflow change event in response to form field change
             * @param {jQuery#event:change} event Form field change event
             * @fires workflow#workflowchange
             */
            var fnChange = function(event) {
                var current = $(event.currentTarget).closest('.step');

                /**
                 * @event workflow#workflowchange
                 * @description Called after form field value has changed within a workflow step
                 * @param {jQuery#Event}  event    Workflow change event
                 * @param {Object}        ui
                 * @param {jQuery#Object} ui.step  Step selector where change occurred
                 * @param {Number}        ui.count index of the step where change occurred
                 */
                self._trigger('change', event, {
                    step  : current,
                    count : ui.steps.index(current)
                });
            };

            /**
             * @function workflow~anonymous
             * @listens jQuery#keypress
             * @description Calls workflow~change and workflow#next on 'enter'
             * @param {jQuery#event:keypress} event Keypress event
             */
            $(':input', this.element).on('keypress', function(event) {
                if (event.which === key.ENTER) {
                    // fire input change event
                    fnChange(event);

                    // fire next event
                    self.next(event);
                    event.preventDefault();
                }
            });

            // attach change function to input change event
            $(':input', this.element).on('change', fnChange);

            // initial setup
            ui.steps.not(ui.first).hide();

            /**
             * @function workflow~fnCreate
             * @listens jQuery.widget#create
             * @description Moves to the first step of the workflow
             * @param {widget#event:create} event Widget instantiation event
             */
            var fnCreate = self.options.create;
            self.options.create = function(event) {
                // initiate workflow
                fnStep(ui.first, event);

                /**
                 * @callback workflow#create
                 * @description Called after workflow is created
                 * @param {jQuery#Event} event Workflow create event
                 * @param {workflow#ui}  ui
                 */
                fnCreate(event, ui);
            };
            /*
             * workflow~fnCreate will now handle the widget#create
             * event fired implicitly after _create returns.
             */
        },
        step : $.noop,
        next : $.noop,
        previous : $.noop,
        _destroy : function() {

        }
    });
});
