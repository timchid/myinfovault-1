'use strict';
/* global dojo */
//dojo.require("dojo.*"); /* for event.connect */
dojo.addOnLoad( function() { setTimeout(initCvSort, 200); } );

function initCvSort()
{
    init_controls();

    /* Connect to Checkbox */
    var cbHighlight = dojo.byId('highlightBox');
    var whichEvent = 'onchange';
    if (dojo.isIE) {
        whichEvent = 'onpropertychange';
    }
    dojo.connect( cbHighlight, whichEvent,
        function(evt) {
            toggle_hilite_choice(evt);
        }
    );

    var saveBtn = dojo.byId('save');
    dojo.connect( saveBtn, 'onclick', save_click );
    var resetBtn = dojo.byId('reset');
    dojo.connect( resetBtn, 'onclick', reset_click );

    cbHighlight.focus();
}

function init_controls()
{
    toggle_hilite_choice();
}


function toggle_hilite_choice(evt)
{
    var cbHighlight = dojo.byId('highlightBox'); // for checkbox
    var hlOpts = dojo.byId('hl_options');

    enable_section(hlOpts, cbHighlight.checked);
        hlOpts = dojo.byId('hl_all');
    enable_section(hlOpts, cbHighlight.checked);

    return false;
}

/* the Save button was clicked */
function save_click(evt)
{
    var hlOpts = dojo.byId('hl_options');
    enable_section(hlOpts, true);
        hlOpts = dojo.byId('hl_all');
    enable_section(hlOpts, true);
}

/* the Reset button was clicked */
function reset_click(evt)
{
    setTimeout(init_controls, 30);
}

function enable_control(el, abled)
{
    el.disabled = ! abled;
    //Why is this even here?
    var foo='bareak'; //jshint ignore:line
}

/* Enable/Disable a whole section of a form.
 *
 * Iterate through children of the provided element, enabling or disabling
 * any form controls (<input> elements) found.
 */
function enable_section(el, abled)
{
    var inputs = el.getElementsByTagName('input');
    /*alert("got inputs ["+inputs+"]");*/
    if (inputs.length > 0) {
        for (var i=0; i<inputs.length; i++) {
            enable_control(inputs[i], abled);
        }
    }
}


/*<span class="vim">
 vim:ts=8 sw=4 et sm:
 </span>*/
