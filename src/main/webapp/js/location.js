'use strict';
/* global htmlDecode */
/*
 * TODO: file description
 */
$(document).ready(function() {
    var province = $('#province');
    var country = $('#country');
    var cityblock = $('#cityblock');
    var city = $('#city');

    /* remove drop-down for State/Province and replace by text-box*/
    var divprovince = province.parent();
    var provinceVal = province.val();
    province.remove();
    province = $('<input />').attr({'type':'text', 'id': 'province', 'name': 'province' }).val(provinceVal).appendTo($(divprovince));

    country.change(function() {
        province.val('');
        city.val('');
    });

    cityblock.attr('title', cityblock.attr('title') + ' (Minimum 2 characters required to get type ahead suggestions.)');

    province.add(city).each(function(index, element) {
        var self = $(element);
        element.justblurred = false;

        self.focus(function(e) {
            //lwcon.write('got FOCUS event on ['+e+']');
            //console.log('got FOCUS event on ['+e+']');
            e.target.justblurred = false;
        });

        self.blur(function(e) {
            //lwcon.write('got BLUR event on ['+e+']');
            //console.log('got BLUR event on ['+e+']');
            $(e.target).autocomplete('close');
            e.target.justblurred = true;
            //lwcon.write('set blurred to true on ['+e.target.id+']');
        });

        self.autocomplete({
            minLength : 2,
            source    : function(request, response) {
                            var bCity = self.is(city);

                            var requestType = bCity ? 'citiesbyprovinceandcountrycode' : 'provincesbycountrycode';

                            var url = '/miv/location?requestType=' + requestType +
                                      '&country=' + country.val() +
                                      '&province=' + province.val();

                            if (bCity) { url += '&city=' + city.val(); }

                            $.get(url, function(data) {
                                response(data);
                            });
                        },
            focus     : function(event, ui) {
                            // this is fired when focus gets on one of the choices
                            self.val(htmlDecode(ui.item.id));
                            return false;
                        },
            select    : function(event, ui) {
                            //lwcon.write('select event: item ID is ['+ui.item.id+']');
                            self.val(htmlDecode(ui.item.id));
                            return false;
                        },
            open      : function(event, ui) {
                            //lwcon.write('open event on ['+event.target.id+']');
                            //lwcon.write('   justblurred is '+event.target.justblurred);
                            if (event.target.justblurred) {
                                event.target.justblurred = false;
                                $(event.target).autocomplete('close');
                            }
                        }
        })
        .data('ui-autocomplete')._renderItem = function(ul, item) {
            return $('<li/>')
                /*.data('item.autocomplete', item)*/
                .append('<a>' + htmlDecode(item.value) + '</a>')
                .appendTo(ul);
        };
    });
});
