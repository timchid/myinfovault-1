'use strict';
/* global dojo, dijit, tinyMCE, mivConfig, getLabel */
/* exported insertTag, insertGreek, insertCharacter */
dojo.require('dojo.parser');
dojo.require('dijit.layout.ContentPane');
dojo.require('dojox.layout.FloatingPane');

dojo.addOnLoad( function() { setTimeout(initSpecialCharacters, 200); } );

// ==========--------------------==========
//        Special Characters section
// ==========--------------------==========

var cm_globalElement = null;
var cm_globalStart;
var cm_globalEnd;
var cm_globalRange;

var globalIEStart;
var globalIEEnd;

var mceEditorId;

function initSpecialCharacters()
{
    if (typeof tinyMCE !== 'undefined')
    {
        initTinyMCE();
    }
    else
    {
        initCharacterPalette();
    }
}


function initTinyMCE()
{
    var tinyMceEditorArray = dojo.query('.mceEditor');

    if (tinyMceEditorArray.length > 0)
    {
       var tinyMceEditor = tinyMceEditorArray[0];
       mceEditorId = tinyMceEditor.id;
    }
    //var clicker = dojo.byId("palette_button").firstChild;
    var clicker = dojo.byId('palette_button').firstChild.firstChild;

    clicker.style.cursor = 'pointer';
    clicker.title = 'Show the Special Character Palette';
    dojo.connect(clicker,'onclick',showPalette);
    dojo.connect(window, 'onunload', closePalette);
    if (document.selection)
    {
        window.onunload=closePalette;
    }
}


function showPalette(e)
{
    //e.preventDefault();
    tinyMCE.execInstanceCommand('content','mceCharMap',false);
}


function closePalette()
{
    if (window.palwin) {
        window.palwin.close();
    }
}


function initCharacterPalette()
{
    //debugger;
    //if (djConfig) {
    //    djConfig.isDebug = true;
    //    djConfig.debugContainerId = "myDojoDebug";
    //}
    //else {
    //    djConfig = {
    //                 isDebug: true,
    //                 debugContainerId: "myDojoDebug"
    //               };
    //}

    var inputTags = document.getElementsByTagName('input');
    var textareaTags = document.getElementsByTagName('textarea');

    dojo.forEach(inputTags,
        function(tag, idx, ary) {
            if (tag.type !== 'button' && tag.type !== 'image')
            {
                if (tag.type === 'text')
                {
                    dojo.connect(tag, 'onfocus', cm_setElement);

                    // for IE we need to monitor changes to the selected text
                    if (document.selection) {
                        dojo.connect(tag, 'onselect', updateSelectRange);
                        dojo.connect(tag, 'onkeyup', updateSelectRange);
                        dojo.connect(tag, 'onselectionchange', updateSelectRange);
                        dojo.connect(tag, 'onclick', updateSelectRange);
                    }
                    else {
                        // but we *don't* want IE to see mouse ups
                        // -- it thinks the form is selected, not the text
                        dojo.connect(tag, 'onmouseup', cm_setElement);
                    }
                }
                else if (tag.type === 'radio' || tag.type === 'checkbox')
                {
                    dojo.connect(tag, 'onfocus', cm_clearElement);
                    dojo.connect(tag, 'onclick', cm_clearElement);
                }
            }
        }
    );

    dojo.forEach(textareaTags,
        function(el, idx, ary) {
            dojo.connect(el, 'onfocus', cm_setElement);

            // for IE we need to monitor changes to the selected text
            if (document.selection) {
                dojo.connect(el, 'onselect', updateSelectRange);
                dojo.connect(el, 'onkeyup', updateSelectRange);
                dojo.connect(el, 'onselectionchange', updateSelectRange);
                dojo.connect(el, 'onclick', updateSelectRange);
            }
            // but we *don't* want IE to see mouse ups
            // -- it thinks the form is selected, not the text
            else {
                dojo.connect(el, 'onmouseup', cm_setElement);
            }
        }
    );

    //var clicker = dojo.byId("palette_button").firstChild;
    var clicker = dojo.byId('palette_button').firstChild.firstChild;

    var palette = dojo.byId('char_palette');

    clicker.style.cursor = 'pointer';
    clicker.title = 'Show the Special Character Palette';

    dojo.connect( clicker, 'onclick',
        function(evt2x) {
            evt2x.preventDefault();
            palette.style.display = 'block';
            palette.style.height = '32.62em';
            palette.style.width = '28.1em';
            var dijitPalette = dijit.byId('char_palette');
            if (!dijitPalette)
            {
                alert('the character palette dijit has not been rendered!');
            }
            else
            {
                dijitPalette.show();
                dijitPalette.bringToTop();
                dijitPalette.close = dijitPalette.minimize;
            }

            if (mivConfig && mivConfig.isIE)
            {
                var pObj = dijit.byId('char_palette');
                var node = pObj.domNode;

                var w = node.clientWidth;
                var h = node.clientHeight;

                //pObj.resizeTo(w+1, h+1);			//resizeTo replace by resize
                pObj.resize({ w: w+1, h: h+1 });
            }
            // This next line would be nice -- It puts the focus back on
            // the field after clicking the Omega button.
            // But in IE it loses the highlight of the selected text. The
            // previously selected text *does* get replaced, and that is
            // visually confusing. Of course it works perfectly in Firefox.
            // Thanks IE.
            //if (cm_globalElement) cm_globalElement.focus();
        }
    );

    /*palette.style["display"] = "block";*/
    //palette.style.visibility = "visible";
}
//initCharacterPalette()


function cm_setElement(e)
{
    if (!e) { e = window.event; }

    /* Debugging Info ...
    dojo.log.info("");dojo.log.info("setElement, EVENT is ["+e.type+"]");
    dojo.log.info("     BEFORE: cm_globalElement is ["+cm_globalElement+"]");
    if (cm_globalElement) {
        dojo.log.info("setElement, before: type, id are ["+cm_globalElement.type+"],"+
                                                       "["+cm_globalElement.id+"]");
    }
    ... End Debugging Info */

    cm_globalElement = e.target;

    if (document.selection)
    {
        //dojo.log.info(" SETTING    cm_globalRange     RANGE");
        cm_globalRange = document.selection.createRange().duplicate();
        globalIEStart = Math.abs(document.selection.createRange().moveStart('character', -1000000));
        globalIEEnd = Math.abs(document.selection.createRange().moveEnd('character', -1000000));
    }
    else
    {
        cm_globalStart = cm_globalElement.selectionStart;
        cm_globalEnd = cm_globalElement.selectionEnd;
        //dojo.log.info("selectionStart:"+cm_globalStart+" End:"+cm_globalEnd);
    }

    /* Debugging Info ...
    dojo.log.info("     selectionStart:"+cm_globalStart+" End:"+cm_globalEnd+
                      " Range:"+cm_globalRange);
    if (cm_globalRange) {
        dojo.log.info("     testStart:"+globalIEStart+" testEnd:"+globalIEEnd+
                          " Range text: ["+cm_globalRange.text+"]");
    }
    dojo.log.info("      AFTER: cm_globalElement is ["+cm_globalElement+"]");
    if (cm_globalElement) {
        dojo.log.info("             type, id are ["+cm_globalElement.type+"],"+
                                                "["+cm_globalElement.id+"]");
    }
    dojo.log.info("Done with EVENT ["+e.type+"]");dojo.log.info("");
    ... End Debugging Info */
}
// cm_setElement()


function cm_clearElement()
{
    //dojo.log.info("clearElement, before: cm_globalElement is ["+cm_globalElement+"]");
    cm_globalElement = null;
    //dojo.log.info("clearElement, after: cm_globalElement is ["+cm_globalElement+"]");
}
// cm_clearElement()


/* This function is only hooked as a handler in IE and assumes that
 * the document.selection object exists, which is only true in IE.
 */
function updateSelectRange(e)
{
    if (!e) { e = window.event; }

    cm_globalRange = document.selection.createRange().duplicate();
    globalIEStart = Math.abs(document.selection.createRange().moveStart('character', -1000000));
    globalIEEnd = Math.abs(document.selection.createRange().moveEnd('character', -1000000));
}


function insertCharacter(inCharacter)
{
    insertString(inCharacter);
    closeCharPalette();
}


function insertGreek(inCharacter)
{
    insertString('<font face="symbol">'+inCharacter+'</font>');
    closeCharPalette();
}


/**
 * add the function to close the palette in IE
 */
function closeCharPalette()
{
    if (dojo.isIE)
    {
        var dijitPalette = dijit.byId('char_palette');
        if (dijitPalette)
        {
            dijitPalette.close();
        }
    }
}


function insertString(inCharacter)
{
    //dojo.log.info("");dojo.log.info("insertString: BEGIN");
    if (cm_globalElement !== null && cm_globalElement !== undefined)
    {
        var el = cm_globalElement;

        //cm_globalElement.focus(); // Removed this line -- causing IE to re-select??
        var insText = inCharacter;
        var maxlengthmsg;

        if (typeof document.selection !== 'undefined') // IE browser
        {
            //debugger;
            var localRange = cm_globalRange;//document.selection.createRange();

            /* Debugging Info ...
            dojo.log.info("     testStart:"+globalIEStart+
                                " testEnd:"+globalIEEnd+
                             " Range text: ["+cm_globalRange.text+"]");
            dojo.log.info("    local Range text: ["+localRange.text+"]");
            ... End Debugging Info */

            if (insText.length > 0)
            {
                if (el.type === 'textarea')
                {
                    localRange.text = insText;
                    setCursorPosition(el, globalIEStart+1);
                    if (typeof dijit.byId('errormessage') !== 'undefined') {
                        dijit.byId('errormessage').setContent('');
                    }
                }
                else
                {
                    if (el.maxLength > el.value.length - (globalIEEnd - globalIEStart))
                    {
                        var newText = el.value.substr(0,globalIEStart) +
                                      insText +
                                      el.value.substr(globalIEEnd, el.value.length);

                        if (el.maxLength > 0 && newText.length > el.maxLength)
                        {
                            newText = newText.substr(0, el.maxLength);
                        }

                        el.value = newText;
                        //Added the below line to make sure the old error message gets wiped away
                        if (typeof dijit.byId('errormessage') !== 'undefined') {
                            dijit.byId('errormessage').setContent('');
                        }
                        setCursorPosition(el, globalIEStart+1);
                    }
                    else
                    {
                        if (typeof dijit.byId('errormessage') !== 'undefined') {
                            maxlengthmsg = 'ERROR - Too many characters have been added to the following field<ul>';
                            maxlengthmsg += '<li>' + getLabel(el) + ' (' + el.maxLength + ' characters allowed).</li></ul>';
                            dijit.byId('errormessage').setContent(maxlengthmsg);
                        }
                    }
                }
            }
        }
        else if (typeof el.selectionStart !== 'undefined') // sane browsers
        {
            var start = el.selectionStart;
            var end = el.selectionEnd;
            if (insText.length > 0)
            {
                if (el.type === 'textarea' || el.maxLength > el.value.length - (end - start))
                {
                    var scrollPos = el.scrollTop;
                    var scrollLeftPos = el.scrollLeft;
                    el.value = el.value.substr(0, start) + insText + el.value.substr(end);
                    if (el.maxLength > 0 && el.value.length > el.maxLength)
                    {
                        el.value = el.value.substr(0, el.maxLength);
                    }
                    //Added the below line to make sure the old error message gets wiped away
                    if (typeof dijit.byId('errormessage') !== 'undefined') {
                        dijit.byId('errormessage').setContent('');
                    }

                    if (el.type === 'textarea')
                    {
                        el.scrollTop = scrollPos;
                        el.scrollLeft = scrollLeftPos;
                    }
                    var pos = start + insText.length;
                    setCursorPosition(el,pos);
                }
                else
                {
                    if (typeof dijit.byId('errormessage') !== 'undefined') {
                        maxlengthmsg = 'ERROR - Too many characters have been added to the following field<ul>';
                        maxlengthmsg += '<li>' + getLabel(el) + ' (' + el.maxLength + ' characters allowed).</li></ul>';
                        dijit.byId('errormessage').setContent(maxlengthmsg);
                    }
                }
            }
        }
        else // something not supported; inserted text is added at end
        {
            if (insText.length > 0)
            {
                el.value += insText;
            }
        }

        el.focus();

        //dojo.log.info("insertString: END");dojo.log.info("");

        return;
    }
}
// insertString()


function insertTag(tag)
{
    // only insert sup and sub tags.
    if (!tag || tag.length < 1 || (tag !== 'sup' && tag !== 'sub')) {
        return;
    }

    var startTag = '<'+tag+'>';
    var endTag = '</'+tag+'>';
    if (cm_globalElement !== null && cm_globalElement !== undefined)
    {
        var el = cm_globalElement;
        var curText, pos;

        if (typeof document.selection !== 'undefined') // IE browser
        {
            var localRange = cm_globalRange;
            curText = localRange.text;
            if (el.type === 'textarea')
            {
                localRange.text = startTag + curText + endTag;

                // Position the cursor between the tags if no text was selected,
                // or following the closing tag if there was a selection.
                pos = globalIEStart + startTag.length;
                if (curText.length > 0) {
                    pos += curText.length + endTag.length;
                }
                setCursorPosition(el, pos);
            }
            else
            {
                if (el.maxLength >= (el.value.length + startTag.length +  endTag.length))
                {
                    var newText = el.value.substr(0,globalIEStart) +
                                  startTag +
                                  el.value.substr(globalIEStart, globalIEEnd-globalIEStart) +
                                  endTag +
                                  el.value.substr(globalIEEnd, el.value.length);
                    el.value = newText;
                    //Added the below line to make sure the old error message gets wiped away
                    if (typeof dijit.byId('errormessage') !== 'undefined') {
                        dijit.byId('errormessage').setContent('');
                    }

                    // Position the cursor between the tags if no text was selected,
                    // or following the closing tag if there was a selection.
                    pos = globalIEStart + startTag.length;
                    if (curText.length > 0) {
                        pos += curText.length + endTag.length;
                    }
                    setCursorPosition(el, pos);
                }
                else
                {
                    if (typeof dijit.byId('errormessage') !== 'undefined') {
                        var maxlengthmsg = 'ERROR - Too many characters have been added to the following field<ul>';
                        maxlengthmsg += '<li>' + getLabel(el) + ' (' + el.maxLength + ' characters allowed).</li></ul>';
                        dijit.byId('errormessage').setContent(maxlengthmsg);
                    }
                }
            }

        }
        else if (typeof el.selectionStart !== 'undefined') // sane browsers
        {
            var start = el.selectionStart;
            var end = el.selectionEnd;
            curText = el.value.substr(start, end-start);
            if (el.type === 'textarea' || el.maxLength >= (el.value.length + startTag.length +  endTag.length))
            {
                var scrollPos = el.scrollTop;
                var scrollLeftPos = el.scrollLeft;
                el.value = el.value.substr(0, start) +
                              startTag +
                              curText +
                              endTag +
                              el.value.substr(end);

                //Added the below line to make sure the old error message gets wiped away
                if (typeof dijit.byId('errormessage') !== 'undefined') {
                    dijit.byId('errormessage').setContent('');
                }

                // Position the cursor between the tags if no text was selected,
                // or following the closing tag if there was a selection.
                var pos3 = start + startTag.length;
                if (curText.length > 0) {
                    pos3 += curText.length + endTag.length;
                }

                if (el.type === 'textarea')
                {
                    el.scrollTop = scrollPos;
                    el.scrollLeft = scrollLeftPos;
                }
                setCursorPosition(el, pos3);
            }
            else
            {
                if (typeof dijit.byId('errormessage') !== 'undefined') {
                    var maxlengthmsg3 = 'ERROR - Too many characters have been added to the following field<ul>';
                    maxlengthmsg3 += '<li>' + getLabel(el) + ' (' + el.maxLength + ' characters allowed).</li></ul>';
                    dijit.byId('errormessage').setContent(maxlengthmsg3);
                }
            }
        }
        else // something not supported; inserted text is added at end
        {
            if (tag.length > 0)
            {
                el.value += startTag;
                el.value += endTag;
            }
        }
        el.focus();
    }
}


function setCursorPosition(field, pos)
{
    if (dojo.isIE)
    {
        var range = field.createTextRange();
        range.collapse(true);

        range.move('character', 0);
        range.select();
        var start_caret_pos =
            Math.abs(document.selection.createRange().moveEnd('character', -1000000));

        range.moveEnd('character', pos - start_caret_pos);
        range.moveStart('character', pos - start_caret_pos);
        range.select();
    }
    else
    {
        field.selectionStart = pos;
        field.selectionEnd = pos;
    }
}

/*<span class="vim">
 vim:ts=8 sw=4 et sm:
</span>*/
