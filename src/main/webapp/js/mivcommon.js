'use strict';
/* exported htmlDecode, viewTime, key, scrollTo, showLoadingDiv, hideLoadingMessage, getLabel, trim */
/* exported isNumber, htmlEncode, highlight, changeInputType, focusToFirstErrorField, removeLayoutPanels */
/* exported leftPad, showMessage, isScrolledIntoView, checkUncheckAll, getListOfIds, getListOfValues */
/* exported formatAsMoneyFromString, stripCurrency */
/* global dojo, Modernizr */
/*
 * This javascript file contains common utility methods which are used across multiple JSP pages.
 */

/**
 * @namespace jQuery
 * @description Access via <code>$.</code>
 * @see {@link http://api.jquery.com/}
 */
/**
 * @typedef {string} jQuery#selector
 * @description A selector which can be passed to the jQuery function to select DOM elements.
 * @see {@link http://api.jquery.com/category/selectors/}
 */
/**
 * @typedef {Object} jQuery#Object
 * @description Jquery object
 * @see {@link http://api.jquery.com/Types/#jQuery}
 */
/**
 * @typedef {Object} jQuery#Element
 * @description An element in the Document Object Model (DOM)
 * @see {@link http://api.jquery.com/Types/#Element}
 */
/**
 * @typedef {Object} jQuery#Event
 * @description jQuery event
 * @property {jQuery#Element} currentTarget                   The current DOM element within the event bubbling phase.
 * @property {Object}         data                            An optional object of data passed to an event method when the current executing handler is bound.
 * @property {jQuery#Element} delegateTarget                  The element where the currently-called jQuery event handler was attached.
 * @property {Boolean}        metaKey                         Indicates whether the META key was pressed when the event fired.
 * @property {String}         namespace                       The namespace specified when the event was triggered.
 * @property {Number}         pageX                           The mouse position relative to the left edge of the document.
 * @property {Number}         pageY                           The mouse position relative to the top edge of the document.
 * @property {jQuery#Element} relatedTarget                   The other DOM element involved in the event, if any.
 * @property {Object}         result                          The last value returned by an event handler that was triggered by this event, unless the value was undefined.
 * @property {jQuery#Element} target                          The DOM element that initiated the event.
 * @property {Number}         timeStamp                       The difference in milliseconds between the time the browser created the event and January 1, 1970.
 * @property {String}         type                            Describes the nature of the event.
 * @property {Number}         which                           For key or mouse events, this property indicates the specific key or button that was pressed.
 * @property {Boolean}        isDefaultPrevented()            Returns whether event.preventDefault() was ever called on this event object.
 * @property {Boolean}        isImmediatePropagationStopped() Returns whether event.stopImmediatePropagation() was ever called on this event object.
 * @property {Boolean}        isPropagationStopped()          Returns whether event.stopPropagation() was ever called on this event object.
 * @property {Boolean}        preventDefault()                If this method is called, the default action of the event will not be triggered.
 * @property {Boolean}        stopImmediatePropagation()      Keeps the rest of the handlers from being executed and prevents the event from bubbling up the DOM tree.
 * @property {Boolean}        stopPropagation()               Prevents the event from bubbling up the DOM tree, preventing any parent handlers from being notified of the event.
 * @see {@link http://api.jquery.com/Types/#Event}
 */
/**
 * @event jQuery#click
 * @description Click event
 * @see {@link http://api.jquery.com/click/}
 */
/**
 * @event jQuery#change
 * @description Change event
 * @see {@link http://api.jquery.com/change/}
 */
/**
 * @event jQuery#keypress
 * @description Keypress event
 * @see {@link http://api.jquery.com/keypress/}
 */

/**
 * @namespace dataTables
 * @memberof jQuery
 * @description Data Tables jQuery plugin
 * @see {@link http://datatables.net/reference/api/}
 */
/**
 * @typedef {Object} jQuery.dataTables#Object
 * @description Data tables API.
 * @see {@link http://datatables.net/reference/api/}
 */
/**
 * @typedef {Object[]} jQuery.dataTables#columns
 * @description Data tables columns option
 * @see {@link http://datatables.net/reference/option/columns}
 */
/**
 * @typedef {integer|string|jQuery#Element|jQuery#Object} jQuery.dataTables#row-selector
 * @description Data tables row selector.
 * @see {@link http://datatables.net/reference/type/row-selector}
 */
/**
 * @typedef {integer|string|jQuery#Element|jQuery#Object} jQuery.dataTables#column-selector
 * @description Data tables column selector.
 * @see {@link http://datatables.net/reference/type/column-selector}
 */
/**
 * @event jQuery.dataTables#draw
 * @description Event fired when a datatable is drawn.
 * @see {@link http://datatables.net/reference/event/draw}
 */

/**
 * @namespace ui
 * @memberof jQuery
 * @description jQuery UI
 * @see {@link http://api.jqueryui.com/}
 */
/**
 * @member keyCode
 * @memberof jQuery.ui
 * @description A mapping of key code descriptions to their numeric values
 * @property {Number} BACKSPACE
 * @property {Number} COMMA
 * @property {Number} DELETE
 * @property {Number} DOWN
 * @property {Number} END
 * @property {Number} ENTER
 * @property {Number} ESCAPE
 * @property {Number} HOME
 * @property {Number} LEFT
 * @property {Number} PAGE_DOWN
 * @property {Number} PAGE_UP
 * @property {Number} PERIOD
 * @property {Number} RIGHT
 * @property {Number} SPACE
 * @property {Number} TAB
 * @property {Number} UP
 */

/**
 * @namespace autocomplete
 * @memberof jQuery.ui
 * @description jQuery UI Autocomplete widget
 * @see {@link http://api.jqueryui.com/autocomplete/}
 */

/**
 * @namespace widget
 * @memberof jQuery
 * @description jQuery UI Widget Factory
 * @see {@link http://jqueryui.com/widget/}
 */
/**
 * @event jQuery.widget#create
 * @description Widget instantiated event
 * @see {@link http://api.jqueryui.com/jquery.widget/}
 */

/**
 * @namespace custom
 * @memberof jQuery
 * @description MIV custom jQuery UI widgets
 * @see {@link https://ucdavis.jira.com/wiki/}
 */

var distanceFromTop = 300;
var animateDuration = 1000;
//how long the notice stays up: 2.0 seconds
var viewTime = 2000;

/**
 * @description JQuery event key codes
 * @todo Replace with jQuery UI keycodes
 * @see jQuery.ui.keyCode
 */
var key = {
    ENTER : 13,
    SPACE : 32
};

$(document).ready(function() {
	// initialize default focus
	setDefaultFocus();
});

/**
 * check the existence of element
 */
jQuery.fn.exists = function(){return ($(this).size() > 0);}; // jshint ignore:line

function scrollTo(node, distanceTop)
{
    if (distanceTop === undefined || isNaN(distanceTop)) {
        distanceTop = distanceFromTop;
    }
    var curtop = getPositionTop(node);
    // set focus to curtop position
    jQuery('html, body').animate({
        scrollTop : (curtop - distanceFromTop) + 'px'
    }, animateDuration, 'linear');
}


/**
 * Set the default focus on the first input field of the form
 */
function setDefaultFocus()
{
    var focusfield = $('*:input[autofocus]');

    // if autofocus not supported, select the first field and set focus to that field
    if (!Modernizr.input.autofocus || focusfield.length === 0) {
        focusfield = $('*:input:enabled:visible:first');
    }

    focusfield.focus();
}
// setDefaultFocus


/**
 * Show the loading div
 */
function showLoadingDiv()
{
    $('#loading').css('display', 'block');
}

/**
 * Hide the loading div
 */
function hideLoadingMessage()
{
    $('#loading').css('display', 'none');
}

/**
 * @description Get the error label for an element
 * @param {String} elemId ID of the element
 * @return {String} It returns errorlabel if not that label otherwise element id
 */
function getLabel(elemId)
{
    var element = $('#' + elemId);
    // Set element is as a label
    var label = elemId;
    // Check if element exists or not
    if (element.exists()) {
        // get the label for given element
        var elementLabel = $('label[for="' + elemId + '"]');

        // Use the errorlabel attribute if present
        if (elementLabel.attr('errorlabel') !== undefined) {
            label = elementLabel.attr('errorlabel');
        }
        // Otherwise get the label
        else {
            label = elementLabel.text();
        }

        // Remove colon from label if present
        if (label[label.length-1] === ':') {
            label = label.slice(0, -1);
        }
    }
    return label;
}


/**
 * @description Use this function to highlight an element for a certain amount of time (7 seconds)
 * @param {String} element ID
 */
function highlight(elemid)
{
    var node = $('#' + elemid);

    if (node.exists()) {
        node.effect('highlight', { color: '#D8FCDD' }, 7000);
    }
}

/**
 * @description This provides a function like the javascript "somestring.trim()" method
 * @param {String} str String to trim
 * @return {String} Given string trimmed of leading and trailing whitespace
 */
function trim(str) {
    return str !== undefined ? str.replace(/^\s\s*/, '').replace(/\s\s*$/, '') : str;
}
// Better -- this *adds* the .trim() method if it doesn't exist
(function() {
    if (! String.prototype.trim) {
        String.prototype.trim = function() { // jshint ignore:line
            return this.replace(/^\s+/, '').replace(/\s+$/, '');
        };
    }
})();

/**
 * @description Return if given value is a number
 * @param {String} s Value to check
 * @returns {Boolean} If given value is a number
 */
function isNumber(s)
{
    return !isNaN(s);
}

function htmlEncode(value) {
    if (value) {
        return jQuery('<div />').text(value).html();
    }
    else {
        return '';
    }
}

function htmlDecode(value) {
    if (value) {
        return $('<div />').html(value).text();
    }
    else {
        return '';
    }
}

/**
 * @description Change the type of an element
 * @param {jQuery#Element} oldObject
 * @param {String}         oType
 */
function changeInputType(oldObject, oType) {
    var newObject = document.createElement('input');
    newObject.type = oType;
    if (oldObject.size) { newObject.size = oldObject.size; }
    if (oldObject.value) { newObject.value = oldObject.value; }
    if (oldObject.name) { newObject.name = oldObject.name; }
    if (oldObject.id) { newObject.id = oldObject.id; }
    if (oldObject.className) { newObject.className = oldObject.className; }
    if (oldObject.onclick) { newObject.onclick = oldObject.onclick; }
    if (oldObject.title) { newObject.title = oldObject.title; }

    oldObject.parentNode.replaceChild(newObject, oldObject);
    return newObject;
}

/**
 * @description Set focus to first error field
 * @param {String} [errorClass=errorfield]
 */
function focusToFirstErrorField(errorClass) {
    if (errorClass === undefined) {
        errorClass = 'errorfield';
    }

    var errorFields = dojo.query('.' + errorClass);
    if (errorFields !== undefined &&
        errorFields !== null &&
        errorFields.length > 0) {
        if (dojo.byId(errorFields[0].id).tagName === 'DIV' || dojo.byId(errorFields[0].id).tagName === 'SPAN') {
            var elementsList = dojo.query('input,select,textarea', dojo.byId(errorFields[0].id));
            if (elementsList !== undefined && elementsList.length > 0) {
                elementsList[0].focus();
            }
        }
        else {
                dojo.byId(errorFields[0].id).focus();
        }
    }
}

/**
 * @description Remove all dijit layout panels
 * @param {String} layoutSelecter
 */
function removeLayoutPanels(layoutSelecter)
{
    // remove all dijit layout panel
    var layoutpanels = dojo.query(layoutSelecter);
    for (var i = 0; i < layoutpanels.length; i++) {
        if (typeof layoutpanels[i] !== 'undefined') {
            layoutpanels[i].innerHTML = '';
            layoutpanels[i].style.display = 'none';
        }
    }
}

/**
 * @description None
 */
function leftPad(number, targetLength) {
    var output = number + '';
    while (output.length < targetLength) {
        output = '0' + output;
    }
    return output;
}

/**
 * Show the message in the given messagediv with given message
 * @param messageDivID : ID of the div, for null or undefined id's a Dynamic Message div will be created.
 * @param message : set your custom message or set null to don't replace existing message.
 * @param curtop : set the current top position of message in numbers (default 160px).
 */
function showMessage(messageDivID, message, curtop) {
    if (curtop === null || typeof curtop === 'undefined' || curtop === 0) {
        curtop = 160;
    }

    // set focus to curtop position
    jQuery('html, body').animate({
        scrollTop : (curtop - distanceFromTop) + 'px'
    }, animateDuration, 'linear');

    var msgDiv = $('#' + messageDivID);

    var dynamicMessage = !msgDiv.exists();
    // create a dynamic message div
    if (dynamicMessage) {
        msgDiv = $('<div/>').addClass('DynamicMessage').appendTo('body');
    }

    msgDiv.removeAttr('style');

    if (message !== null && typeof message !== 'undefined') {
        msgDiv.html(message);
    }

    // set the style to the message div
    msgDiv.css('display', 'block');
    msgDiv.css('opacity', '1');
    msgDiv.css('position', 'absolute');
    if (curtop > 0) {
        msgDiv.css('top', curtop + 'px');
    }


    // create close link
    msgDiv.append('<a class="popuplink message-box ui-icon ui-icon-closethick ui-state-default ui-corner-all" title="close">&nbsp;</a>');
    var $closeLink = msgDiv.find('a.message-box');

    // click on message to close the message
    $closeLink.click(function() {
        hideMessage(msgDiv, dynamicMessage);
    });

    // how long the notice stays up: 3.0 seconds
    var viewTime = 3000;
    msgDiv.show('slow', setTimeout(function() {
        msgDiv.fadeOut('slow', function() {
            hideMessage(msgDiv, dynamicMessage);
        });
    }, viewTime));
}

/**
 * @description None
 */
function hideMessage(messageDiv, dynamicMessage) {
    messageDiv.removeAttr('style').hide();

    // clear the message if its available
    messageDiv.html('');

    // remove a dynamic message div
    if (dynamicMessage) {
        messageDiv.remove();
    }

    messageDiv.unbind( 'click' );
}

/**
 * Get the position top for an element
 * @param elementID : ID of the element.
 * @param message : set your custom message or set null to don't replace existing message.
 * @return  if element exist than returns positiontop otherwise default top which is 175.
 */
function getPositionTop(elementID)
{
    var top = 175;
    if ($('#' + elementID).exists()) {
        top = $('#' + elementID).position().top;
    }

    return top;
}

/**
 * Check if the element is visible on screen or not
 * @param elem : element selector.
 * @return  true when element is visible on screen
 */
function isScrolledIntoView(elem)
{
    var docViewTop = $(window).scrollTop();
    var docViewBottom = docViewTop + $(window).height();

    var elemTop = $(elem).offset().top;
    var elemBottom = elemTop + $(elem).height();

    return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
}

/**
 * Check/Uncheck All check boxes
 * @param elementCollection : container that holds check boxes.
 * @param checkAll : set TRUE is checked otherwise set FALSE
 */
function checkUncheckAll(elementCollection, checkAll)
{
    checkAll = !!checkAll; // guarantee it's an actual boolean

    var selector = 'input[type=checkbox]:not(.contribution, :disabled)' +
        (checkAll ? ':not(:checked)' : ':checked');

    // We need to click on the checkboxes to trigger any click handlers,
    // not just set the checked state.
    $(selector, elementCollection).click();
}

/**
 * Extract Ids and create Id List from selected elementCollection
 */
function getListOfIds(elementCollection)
{
	return $(elementCollection).map(function() {
	    return this.id;
	}).get();
}

/**
 * Extract Values and create Values List from selected elementCollection
 */
function getListOfValues(elementCollection)
{
	return $(elementCollection).map(function() {
	    return $(this).val();
	}).get();
}


/**
 * format string as currency format
 */
function formatAsMoneyFromString(nStr, upperlimit, useDecimal) {
    if (!isNaN(nStr) &&
        nStr !== '') {
        if (upperlimit !== null &&
            upperlimit !== undefined &&
            nStr * 1 <= upperlimit) {
            var amount = nStr * 1;
            nStr = amount.toString();
            nStr += '';
            var x = nStr.split('.');
            var x1 = x[0];
            var x2 = (typeof(x[1]) === 'undefined')?'00':x[1];
            x2 = (x2.length === 1)?x2+'0':x2;
            var rgx = /(\d+)(\d{3})/;

            while (rgx.test(x1)) {
                x1 = x1.replace(rgx, '$1' + ',' + '$2');
            }

            if (useDecimal) {
                return x1 + '.' + x2;
            }
            else {
                return x1;
            }
        }
    }

    return nStr;
}
// formatAsMoneyFromString


/**
 * strip commas and spaces from currency
 */
function stripCurrency(money) {
    var retval = '';

    if (money !== null && money !== undefined) {
        // Strip all commas and spaces, and a leading dollar-sign if present.
        retval = money.replace(/[,\s]/g, '').replace(/^\$/, '');
    }

    return retval;
}
