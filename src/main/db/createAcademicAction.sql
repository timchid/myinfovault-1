-- Author: Pradeep K Haldiya
-- Created: 2013-09-05

-- Create the AcademicAction
-- Dependencies: Holds the Action Information. The DossierActionType and DelegationAuthority tables must exist.

DROP TABLE IF EXISTS AcademicAction;

CREATE TABLE AcademicAction
(
   ID int primary key auto_increment,
   UserID int NOT NULL,
   ActionTypeID int NOT NULL,
   DelegationAuthorityID int NOT NULL,
   EffectiveDate date NOT NULL,
   RetroactiveDate date NULL,
   EndDate date NULL,
   AccelerationYears int NOT NULL DEFAULT 0,
   InsertTimestamp timestamp default current_timestamp,
   InsertUserID int NOT NULL,
   UpdateTimestamp datetime,
   UpdateUserID int,
   KEY `fk_academic_action_type` (`ActionTypeID`),
   CONSTRAINT `fk_academic_action_type` FOREIGN KEY (`ActionTypeID`) 
   REFERENCES `DossierActionType` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
   KEY `fk_delegation_authority_type` (`DelegationAuthorityID`),
   CONSTRAINT `fk_delegation_authority_type` FOREIGN KEY (`DelegationAuthorityID`) 
   REFERENCES `DelegationAuthority` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
)
engine = InnoDB
default character set utf8;
