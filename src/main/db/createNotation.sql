# Author: Lawrence Fyfe
# Date: 01/23/2007

create table Notation
(
   ID int primary key auto_increment,
   Symbol varchar(4),
   Value varchar(255),
   InsertTimestamp timestamp default current_timestamp,
   InsertUserID int,
   UpdateTimestamp datetime,
   UpdateUserID int
)
engine = InnoDB
default character set utf8;
