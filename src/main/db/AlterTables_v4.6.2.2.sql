-- disclosure certificate no longer need candidate ID, it's available from the dossier
ALTER TABLE DC_Forms DROP COLUMN CandidateID;
