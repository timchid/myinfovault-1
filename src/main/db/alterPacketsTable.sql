-- Author: Rick Hendricks
-- Date: 04/03/2009

-- Change from v2.3 to v3.0 for testing. The Packets table will likely be replaced.
ALTER TABLE `Packets`
 ADD COLUMN `DossierId` INT NULL DEFAULT 0,
 ADD COLUMN `ActionType` INT NULL DEFAULT 0,
 ADD COLUMN `DelegationAuthority` INT NULL DEFAULT 0,
 ADD COLUMN `RecommendedActionFormAdded` BOOLEAN NULL DEFAULT false
;

CREATE INDEX fx_attrib_dossier ON Packets (DossierId);
