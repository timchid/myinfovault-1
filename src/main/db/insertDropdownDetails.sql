-- Author: Pradeep Haldiya
-- Created: 2011-12-16
-- Updated: 2012-07-10

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

SELECT count(*) FROM DropdownDetails INTO @_ORIG_COUNT;

truncate DropdownDetails;
/*
 * DropdownDetailID - Key in the dropdown's option
 * DropdownTitle - Value in the dropdown's option
 * Description - tooltip of the dropdown's option
 */
INSERT INTO DropdownDetails (ID, DropdownDetailID, DropdownMasterID, DropdownTitle, Description, DisplayOrder, Active, InsertUserId)
VALUES
(1, 1, 1, 'Patent', 'Select ''Patent'' if this is an application for a patent or a granted patent', 1, true, 20720),
(2, 2, 1, 'Disclosure', 'Select ''Disclosure'' if this is an ROI disclosure filed with the campus technology office', 2, true, 20720),

/* Manage Format Options Data Category*/
(3,  6, 2, 'Publications', null, 1, true, 20720),
(4, 52, 2, 'Creative Activities', null, 2, true, 20720),
(5, 10, 2, 'Extending Knowledge', null, 3, true, 20720),

/* Manage Format Options Data Category Field Name - Publications*/
(51, 1, 6, 'Publications: Authors', 'author;Publications: Authors;false;true', 1, true, 20720),
(52, 2, 6, 'Publications: Editors', 'editor;Publications: Editors;false;true', 2, true, 20720),
(53, 3, 6, 'Publications: Contributions to Jointly Authored Works', 'contribution;Publications: Contributions to Jointly Authored Works;false;true', 3, true, 20720),
(54, 4, 6, 'Publications: Publication Name', 'journal;Publications: Publication Name;true;true', 4, true, 20720),
(55, 5, 6, 'Publications: Title', 'title;Publications: Title;true;true', 5, true, 20720),

/* Manage Format Options Data Category Field Name - Creative Activities*/
(71, 1, 52, 'Creative Activities: Creators', 'creators;Creative Activities: Creators or Authors;false;true', 1, true, 20720),
(72, 2, 52, 'Creative Activities: Contributions to Jointly Authored Works', 'contributors;Creative Activities: Contributions to Jointly Created Works;false;true', 2, true, 20720),
(73, 3, 52, 'Creative Activities: Publication Name', 'publication;Creative Activities: Publication Name;true;true', 3, true, 20720),
(74, 4, 52, 'Creative Activities: Title', 'title;Creative Activities: Title;true;true', 4, true, 20720),

(91, 1, 10, 'Extending Knowledge: Title', 'title;Extending Knowledge: Title;true;true', 1, true, 20720),
(92, 2, 10, 'Extending Knowledge: Publisher or Sponsor', 'publisher;Extending Knowledge: Publisher or Sponsor;false;true', 2, true, 20720)
;

SHOW WARNINGS;
commit;

SELECT count(*) FROM DropdownDetails INTO @_NEW_COUNT;
SELECT @_ORIG_COUNT AS 'Original Count', @_NEW_COUNT AS 'New Count';

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
