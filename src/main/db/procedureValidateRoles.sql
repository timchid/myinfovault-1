-- Author : Pradeep Kumar Haldiya
-- Created: 09/02/2011

DELIMITER $$

-- PROCEDURE to Validate User Role Assignments
-- @param vUserID
DROP PROCEDURE IF EXISTS `PROC_VALIDATE_ROLES`$$
CREATE PROCEDURE `PROC_VALIDATE_ROLES`(IN vUserID INT(11))
BEGIN

	/****************************************************************************/
	/* Declare Variables 													*/
	/****************************************************************************/
	DECLARE lPrimaryRoleId 	INT(11) DEFAULT -1; 
	DECLARE SYS_ADMIN 		INT(11) DEFAULT 1; 
	DECLARE DEAN 				INT(11) DEFAULT 3;
	DECLARE CANDIDATE 		INT(11) DEFAULT 6;
	DECLARE DEPT_CHAIR 		INT(11) DEFAULT 17; 

	CALL log_info(CONCAT('>>>>>>>>>>>> Coming Into PROC_VALIDATE_ROLES(',vUserID,')'));
	SET lPrimaryRoleId = getPrimaryRole(vUserID);

	IF lPrimaryRoleId = -1 OR lPrimaryRoleId = 0 THEN
		CALL log_errer(CONCAT('User with UserId=',vUserID,' has no primary role [Need to Fix]'));
	ELSE
		CASE lPrimaryRoleId 			
			WHEN DEAN THEN 
				CALL log_error(CONCAT('User with UserId=',vUserID,' have the role of DEAN as a primary role [Need to Fix]'));
			WHEN DEPT_CHAIR THEN 
				CALL log_error(CONCAT('User with UserId=',vUserID,' have the role of DEPT_CHAIR as a primary role [Need to Fix]'));
			ELSE
				CALL log_info(CONCAT('Skip PrimaryRoleId: ',lPrimaryRoleId));		
		END CASE;		
	END IF;
	
	/* If DEAN won't have primary role of CANDIDATE, delete the DEAN role from user's assignments*/
	IF isUserHaveRole(vUserID,DEAN) AND getPrimaryRole(vUserID) <> CANDIDATE THEN
		CALL log_error(CONCAT('Removed DEAN role for User with UserId=',vUserID,' beacuse there primary role is ',lPrimaryRoleId,'. [Fixed]'));
		CALL PROC_DELETE_ROLE_ASSIGNMENT(vUserID,DEAN);
	END IF;
	
	CALL log_info(CONCAT('>>>>>>>>>>>> Exit From PROC_VALIDATE_ROLES(',vUserID,')'));
END$$

DELIMITER ;
