-- Author: Rick Hendricks
-- Created: 2009-04-07
-- Updated: 2010-03-02

--
-- TODO: Documentation needed.
-- what is this table for?
--

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

USE `myinfovault`;

-- -----------------------------------------------------
-- Data for table `myinfovault`.`DossierAttributes`
-- -----------------------------------------------------
TRUNCATE `DossierAttributeType`;
-- DELETE FROM `DossierAttributeType`;
SET @OLD_AUTOCOMMIT=@@AUTOCOMMIT, AUTOCOMMIT=0;
INSERT INTO `DossierAttributeType` (`ID`, `Name`, `Description`, `Required`, `Optional`, `Display`, `ActionTypeIds`, `Type`, `DisplayProperty`, `Sequence`, `InsertTimestamp`, `InsertUserID`)
VALUES
(1, 'department_letter', 'Department Letter/Division Chief Letter', true, null, true, null, 'DOCUMENTUPLOAD', null, 10, current_timestamp, 18635),
(2, 'j_department_letter', 'Department Letter/Division Chief Letter', true, null, true, null, 'DOCUMENTUPLOAD', null,10, current_timestamp, 18635),
(3,'federationvote', 'Department Letter Federation Vote', false, null, true, null, 'DOCUMENTUPLOAD', null,20, current_timestamp, 18635),
(4,'j_federationvote', 'Department Letter Federation Vote', false, null, true, null, 'DOCUMENTUPLOAD', null,20, current_timestamp, 18635),
(5,'peerfederation', 'Department Peer Group Letter for Federation', false, null, true, null, 'DOCUMENTUPLOAD', null,30, current_timestamp, 18635),
(6,'j_peerfederation', 'Department Peer Group Letter for Federation', false, null, true, null, 'DOCUMENTUPLOAD', null,30, current_timestamp, 18635),
(7,'rebuttal', 'Rebuttal Letter', false, null, true, '1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20', 'DOCUMENTUPLOAD', null,40, current_timestamp, 18635),
(8,'rejoinder', 'Rejoinder Letter', false, null, true, '1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20', 'DOCUMENTUPLOAD', null,50, current_timestamp, 18635),
(9,'peerevaluationteaching', 'Peer Evaluation of Teaching Performance', false, null, true, '1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20', 'DOCUMENTUPLOAD', null,60, current_timestamp, 18635),
(10,'evalgraduatechairservice', 'Evaluation Letter of Graduate Group Chair Service', false, null, true, '1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20', 'DOCUMENTUPLOAD', null,70, current_timestamp, 18635),
(11,'ovcrevaluation', 'OVCR Evaluation Letter for Research Unit Director', false, null, true, '1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20', 'DOCUMENTUPLOAD', null,80, current_timestamp, 18635),
(12,'evalclincalactivities', 'Director Evaluation Letter of Clinical Activities', false, null, true, '1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20', 'DOCUMENTUPLOAD', null,90, current_timestamp, 18635),
(13,'evalacademicsenateservice', 'Evaluation Letter of Academic Senate Committee Service', false, null, true, '1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20', 'DOCUMENTUPLOAD', null,100, current_timestamp, 18635),
(14, 'disclosure_certificate', "Candidate's Disclosure Certificate", true, '17', true,'1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20', 'DOCUMENT', null,110, current_timestamp, 18635),
(15, 'j_disclosure_certificate', "Candidate's Disclosure Certificate", true, '17', true,'1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20', 'DOCUMENT', null,110, current_timestamp, 18635),
(16,'extramuralletter', 'Extramural Letter(s)', false, null, true, null, 'DOCUMENTUPLOAD', null,120, current_timestamp, 18635),
(17,'org_chart', 'Org Chart', false, null, true, null, 'DOCUMENTUPLOAD', null,130, current_timestamp, 18635),
(18,'review', 'Reviewing', false, null, true, null, 'ACTION', null,140, current_timestamp, 18635),
(19,'vote', 'Voting Period', false, null, true, null, 'ACTION', null,150, current_timestamp, 18635),
(20, 'fpc_recommendation', 'FPC Recommendation', false, null, true, null, 'DOCUMENTUPLOAD', null,160, current_timestamp,18635),
(21, 'j_fpc_recommendation', 'FPC Recommendation', false, null, true, null, 'DOCUMENTUPLOAD', null,160, current_timestamp,18635),
(22, 'deans_final_decision', "Dean's Final Decision", true, null, true, null, 'DOCUMENT', null,170, current_timestamp, 18635),
(23, 'deans_recommendation', "Dean's Recommendation", true, null, true, null, 'DOCUMENT', null,170, current_timestamp, 18635),
(24, 'j_deans_recommendation', "Dean's Recommendation", true, null, true, null, 'DOCUMENT', null,170, current_timestamp, 18635),
(25, 'cap_recommendation', 'CAP Recommendation', true, null, true, null, 'DOCUMENTUPLOAD', null,180, current_timestamp, 18635),
(26, 'capac_recommendation', 'CAPAC Recommendation', false, null, true, null, 'DOCUMENTUPLOAD', null,190, current_timestamp, 18635),
(27, 'aspc_recommendation', 'ASPC Recommendation', false, null, true, null, 'DOCUMENTUPLOAD', null,200, current_timestamp, 18635),
(28, 'afpc_recommendation', 'AFPC Recommendation', false, null, true, null, 'DOCUMENTUPLOAD', null,210, current_timestamp, 18635),
(29, 'jpc_recommendation', 'JPC Recommendation', false, null, true, null, 'DOCUMENTUPLOAD', null,220, current_timestamp, 18635),
(30, 'viceprovosts_final_decision', "Vice Provost's Final Decision", false, null, true, null, 'DOCUMENT', null,230, current_timestamp, 18635),
(31, 'raf', "Action Form", true, null, true, '1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20', 'DOCUMENT', null,5, current_timestamp, 18635),
(32, 'dean_release', "Release to Dean for Final Decision", true, null, true, null, 'ACTION', null,165, current_timestamp, 20001),
(33, 'vp_release', "Release To Vice Provost For Final Decision", true, null, true, null, 'ACTION', null,165, current_timestamp, 18635),
(34, 'deans_decision_comments', "Dean's Final Decision Comments", false, null, true, null, 'DOCUMENTUPLOAD', null,164, current_timestamp, 18635),
(35, 'j_deans_decision_comments', "Joint Dean's Decision Comments", false, null, true, null, 'DOCUMENTUPLOAD', null,164, current_timestamp, 18635),
(36, 'deans_recommendation_comments', "Dean's Recommendation Comments", false, null, true, null, 'DOCUMENTUPLOAD', null,164, current_timestamp, 18635),
(37, 'j_deans_recommendation_comments', "Joint Dean's Recommendation Comments", false, null, true, null, 'DOCUMENTUPLOAD', null,164, current_timestamp, 18635),
(38, 'adhoc_committee_recommendation', "Ad Hoc Committee Recommendation", false, null, true, null, 'DOCUMENTUPLOAD', null,162, current_timestamp, 18635),
(39, 'shadow_committee_recommendation', "Shadow Committee Recommendation", false, null, true, null, 'DOCUMENTUPLOAD', null,163, current_timestamp, 18635),
(40, 'joint_appointment_override', "Joint Appointment Override", false, null, false, null, 'ACTION', null,999, current_timestamp, 18635),
(41, 'deans_appeal_decision', "Dean's Appeal Decision", true, null, true, null, 'DOCUMENT', null,170, current_timestamp, 18635),
(42, 'j_deans_appeal_recommendation', "Dean's Appeal Recommendation", true, null, true, null, 'DOCUMENT', null,170, current_timestamp, 18635),
(43, 'deans_appeal_decision_comments', "Dean's Appeal Decision Comments", false, null, true, null, 'DOCUMENTUPLOAD', null,164, current_timestamp, 18635),
(44, 'j_deans_appeal_recommendation_comments', "Joint Dean's Appeal Recommendation Comments", false, null, true, null, 'DOCUMENTUPLOAD', null,164, current_timestamp, 18635),
(45, 'viceprovosts_appeal_decision', "Vice Provost's Appeal Decision", false, null, true, null, 'DOCUMENT', null,230, current_timestamp, 18635),
(46, 'viceprovosts_appeal_decision_comments', "Vice Provost's Appeal Decision Comments", false, null, true, null, 'DOCUMENTUPLOAD', null,164, current_timestamp, 18635),
(47, 'dean_appeal_release', "Release to Dean for Appeal Decision", true, null, true, null, 'ACTION', null,165, current_timestamp, 20001),
(48, 'vp_appeal_release', "Release To Vice Provost For Appeal Decision", true, null, true, null, 'ACTION', null,165, current_timestamp, 18635),
(49, 'appeal_request', 'Appeal Request', false, null, true, null, 'DOCUMENTUPLOAD', null,180, current_timestamp, 18635),
(50, 'cap_appeal_recommendation', 'CAP Recommendation', false, null, true, null, 'DOCUMENTUPLOAD', null,180, current_timestamp, 18635),
(51, 'capac_appeal_recommendation', 'CAPAC Recommendation', false, null, true, null, 'DOCUMENTUPLOAD', null,190, current_timestamp, 18635),
(52, 'aspc_appeal_recommendation', 'ASPC Recommendation', false, null, true, null, 'DOCUMENTUPLOAD', null,200, current_timestamp, 18635),
(53, 'afpc_appeal_recommendation', 'AFPC Recommendation', false, null, true, null, 'DOCUMENTUPLOAD', null,210, current_timestamp, 18635),
(54, 'jpc_appeal_recommendation', 'JPC Recommendation', false, null, true, null, 'DOCUMENTUPLOAD', null,220, current_timestamp, 18635),
(55, 'adhoc_committee_appeal_recommendation', 'Ad Hoc Committee Recommendation', false, null, true, null, 'DOCUMENTUPLOAD', null,230, current_timestamp, 18635),
(56, 'senate_shadow_committee_appeal_recommendation', 'Senate Shadow Committee Recommendation', false, null, true, null, 'DOCUMENTUPLOAD', null,240, current_timestamp, 18635),
(57, 'federation_shadow_committee_appeal_recommendation', 'Federation Shadow Committee Recommendation', false, null, true, null, 'DOCUMENTUPLOAD', null,250, current_timestamp, 18635),
(58, 'action_history_document', 'Action History Document', false, null, true, null, 'DOCUMENTUPLOAD', null,240, current_timestamp, 20729),
(59, 'viceprovosts_decision_comments', "Vice Provost's Final Decision Comments", false, null, true, null, 'DOCUMENTUPLOAD', null,164, current_timestamp, 19012),
(60,'biography_form',"Biography Form",true,null, true,'21','DOCUMENTUPLOAD', null,260,current_timestamp,18635),
(61,'evaluations',"List of Student Evaluations",false,null, true,'21','DOCUMENTUPLOAD', null,265,current_timestamp,18635),
(62,'service',"List of Service",false,null, true,'21','DOCUMENTUPLOAD', null,270,current_timestamp,18635),
(63,'publications',"Publications",false, null, true,'21','DOCUMENTUPLOAD', null,275,current_timestamp,18635),
(64,'contributions',"Contributions to Jointly Authored Works",false, null, true,'21','DOCUMENTUPLOAD', null,280,current_timestamp,18635),
(65,'honors',"Honors and Awards",false, null, true,'21','DOCUMENTUPLOAD', null,285,current_timestamp,18635),
(66,'grants',"Grants and Contracts",false, null, true,'21','DOCUMENTUPLOAD', null,290,current_timestamp,18635),
(67,'appointment_form',"Appointment Form",true, null, true,'21','DOCUMENT', null,295,current_timestamp,18635),
(68,'candidatestatement',"Candidate's Statement",false, null, true,'21','DOCUMENTUPLOAD', null,300,current_timestamp,18635),
(69,'candidatediversitystatement',"Candidate's Diversity Statement",false, null, true,'21','DOCUMENTUPLOAD', null,305,current_timestamp,18635),
(70,'positiondescription',"Position Description",false, null, true,'21','DOCUMENTUPLOAD', null,310,current_timestamp,18635),
(71,'teaching',"Teaching, Advising and Curricular Development",false, null, true,'21','DOCUMENTUPLOAD', null,315,current_timestamp,18635),
(72,'extendingknowledge',"Extending Knowledge",false, null, true,'21','DOCUMENTUPLOAD', null,320,current_timestamp,18635),
(73,'creativeactivities',"Creative Activities",false, null, true,'21','DOCUMENTUPLOAD', null,325,current_timestamp,18635),
(74,'workscontributions',"Contributions to Jointly Created Works",false, null, true,'21','DOCUMENTUPLOAD', null,330,current_timestamp,18635),
(75,'packet_header',"Candidate Packet:",false, null, true,'21','HEADER', null,340,current_timestamp,18635),
(76, 'vp_recommendation_release', "Release To Vice Provost For Recommendation", true, null, true, null, 'ACTION', null,164, current_timestamp, 18635),
(77, 'viceprovosts_recommendation', "Vice Provost's Recommendation", false, null, true, null, 'DOCUMENT', null,230, current_timestamp, 18635),
(78, 'p_recommendation_release', "Release To Provost For Recommendation", true, null, true, null, 'ACTION', null,164, current_timestamp, 18635),
(79, 'provosts_recommendation', "Provost's Recommendation", false, null, true, null, 'DOCUMENT', null,230, current_timestamp, 18635),
(80, 'p_tenure_release', "Release To Provost For Tenure Recommendation/Decision", true, null, true, null, 'ACTION', null,164, current_timestamp, 18635),
(81, 'provosts_tenure_recommendation', "Provost's Tenure Recommendation/Decision", false, null, false, null, 'DOCUMENT', 'separatorafter',230, current_timestamp, 20001),
(82, 'p_release', "Release To Provost For Final Decision", true, null, true, null, 'ACTION', null,164, current_timestamp, 18635),
(83, 'provosts_final_decision', "Provost's Final Decision", false, null, true, null, 'DOCUMENT', null,230, current_timestamp, 18635),
(84, 'provosts_decision_comments', "Provost's Final Decision Comments", false, null, true, null, 'DOCUMENTUPLOAD', null,164, current_timestamp, 19012),
(85, 'chancellor_release', "Release To Chancellor For Final Decision", true, null, true, null, 'ACTION', null,164, current_timestamp, 18635),
(86, 'chancellors_final_decision', "Chancellor's Final Decision", false, null, true, null, 'DOCUMENT', null,230, current_timestamp, 18635),
(87, 'chancellors_decision_comments', "Chancellor's Final Decision Comments", false, null, true, null, 'DOCUMENTUPLOAD', null,164, current_timestamp, 18635),
(88, 'vp_appeal_recommendation_release', "Release Appeal To Vice Provost For Recommendation", true, null, true, null, 'ACTION', null,164, current_timestamp, 18635),
(89, 'viceprovosts_appeal_recommendation', "Vice Provost's Appeal Recommendation", false, null, true, null, 'DOCUMENT', null,230, current_timestamp, 18635),
(90, 'p_appeal_recommendation_release', "Release To Provost For Appeal Recommendation", true, null, true, null, 'ACTION', null,164, current_timestamp, 18635),
(91, 'provosts_appeal_recommendation', "Provost's Appeal Recommendation", false, null, true, null, 'DOCUMENT', null,230, current_timestamp, 18635),
(92, 'p_tenure_appeal_release', "Release To Provost For Tenure Appeal Recommendation/Decision", true, null, true, null, 'ACTION', null,164, current_timestamp, 18635),
(93, 'provosts_tenure_appeal_recommendation', "Provost's Tenure Appeal Recommendation/Decision", false, null, false, null, 'DOCUMENT', 'separatorafter',230, current_timestamp, 20001),
(94, 'p_appeal_release', "Release To Provost For Appeal Final Decision", true, null, true, null, 'ACTION', null,164, current_timestamp, 18635),
(95, 'provosts_appeal_final_decision', "Provost's Appeal Final Decision", false, null, true, null, 'DOCUMENT', null,230, current_timestamp, 18635),
(96, 'provosts_appeal_decision_comments', "Provost's Appeal Decision Comments", false, null, true, null, 'DOCUMENTUPLOAD', null,164, current_timestamp, 19012),
(97, 'chancellor_appeal_release', "Release To Chancellor For Final Decision", true, null, true, null, 'ACTION', null,164, current_timestamp, 18635),
(98, 'chancellors_appeal_final_decision', "Chancellor's Appeal Decision", false, null, true, null, 'DOCUMENT', null,230, current_timestamp, 18635),
(99, 'chancellors_appeal_decision_comments', "Chancellor's Appeal Decision Comments", false, null, true, null, 'DOCUMENTUPLOAD', null,164, current_timestamp, 18635),
(100,'vp_header',"Vice Provost",false, null, true,null,'HEADER', null,340,current_timestamp,18635),
(101,'p_header',"Provost",false, null, true,null,'HEADER', null,340,current_timestamp,18635),
(102,'chancellor_header',"Chancellor",false, null, true,null,'HEADER', null,340,current_timestamp,18635),
(103, 'viceprovosts_recommendation_comments', "Vice Provost's Recommendation Comments", false, null, true, null, 'DOCUMENTUPLOAD', 'separatorafter',164, current_timestamp, 19012),
(104, 'viceprovosts_appeal_recommendation_comments', "Vice Provost's Appeal Recommendation Comments", false, null, true, null, 'DOCUMENTUPLOAD', 'separatorafter',164, current_timestamp, 18635),
(105, 'provosts_recommendation_comments', "Provost's Recommendation Comments", false, null, true, null, 'DOCUMENTUPLOAD', 'separatorafter',164, current_timestamp, 18635),
(106, 'provosts_appeal_recommendation_comments', "Provost's Appeal Recommendation Comments", false, null, true, null, 'DOCUMENTUPLOAD', 'separatorafter',164, current_timestamp, 18635),
(107, 'joint_dean_release', "Release to Dean for Recommendation", true, null, true, null, 'ACTION', null,165, current_timestamp, 20001),
(108, 'joint_dean_appeal_release', "Release to Dean for Appeal Recommendation", true, null, true, null, 'ACTION', null,165, current_timestamp, 20001),
(109, 'dean_recommendation_release', "Release to Dean for Recommendation", true, null, true, null, 'ACTION', null,165, current_timestamp, 20001),
(110, 'provosts_tenure_decision', "Provost's Tenure Recommendation/Decision", false, null, true, null, 'DOCUMENT', 'separatorafter',230, current_timestamp, 20001),
(111, 'provosts_tenure_appeal_decision', "Provost's Tenure Appeal Recommendation/Decision", false, null, true, null, 'DOCUMENT', 'separatorafter',230, current_timestamp, 20001),
(112, 'provosts_final_decision_wet', "Provost's Final Decision Wet Signature", false, null, false, null, 'DOCUMENTUPLOAD', null, 230, current_timestamp, 20001),
(113, 'provosts_recommendation_wet', "Provost's Recommendation Wet Signature", false, null, false, null, 'DOCUMENTUPLOAD', null, 230, current_timestamp, 20001),
(114, 'provosts_tenure_recommendation_wet', "Provost's Tenure Recommendation Wet Signature", false, null, false, null, 'DOCUMENTUPLOAD', null, 230, current_timestamp, 20001),
(115, 'provosts_tenure_decision_wet', "Provost's Tenure Decision Wet Signature", false, null, false, null, 'DOCUMENTUPLOAD', null, 230, current_timestamp, 20001),
(116, 'provosts_appeal_final_decision_wet', "Provost's Appeal Final Decision Wet Signature", false, null, false, null, 'DOCUMENTUPLOAD', null, 230, current_timestamp, 20001),
(117, 'provosts_appeal_recommendation_wet', "Provost's Appeal Recommendation Wet Signature", false, null, false, null, 'DOCUMENTUPLOAD', null, 230, current_timestamp, 20001),
(118, 'provosts_tenure_appeal_recommendation_wet', "Provost's Tenure Appeal Recommendation Wet Signature", false, null, false, null, 'DOCUMENTUPLOAD', null, 230, current_timestamp, 20001),
(119, 'provosts_tenure_appeal_decision_wet', "Provost's Tenure Appeal Decision Wet Signature", false, null, false, null, 'DOCUMENTUPLOAD', null, 230, current_timestamp, 20001),
(120, 'chancellors_final_decision_wet', "Chancellors's Final Decision Wet Signature", false, null, false, null, 'DOCUMENTUPLOAD', null, 230, current_timestamp, 20001),
(121, 'chancellors_appeal_final_decision_wet', "Chancellors's Appeal Decision Wet Signature", false, null, false, null, 'DOCUMENTUPLOAD', null, 230, current_timestamp, 20001),
(122, 'packet_request', 'Packet Request', false, null, true, null, 'ACTION', null,150, current_timestamp, 18635)
;


COMMIT;

SET AUTOCOMMIT=@OLD_AUTOCOMMIT;
SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
