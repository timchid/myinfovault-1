-- Author: Rick Hendricks
-- Date: 02/07/2011

-- Table updates for v3.8.0.


source createMivRole.sql;
source insertMivRole.sql;
source insertCollection.sql;
source insertCollectionDocument.sql;
source insertDossierView.sql;
source insertDossierAttributeType.sql;
source insertDelegationAuthorityAttributeCollection.sql;
source insertDocument.sql;






 
