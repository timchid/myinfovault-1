--  Author: Craig Gilmore
-- Created: 2012-03-16
-- Updated: 2012-04-02

-- Private groups now become confidential when assigned to a review
ALTER TABLE Groups
ADD COLUMN `Confidential` boolean null default false
AFTER `Private`;

UPDATE Groups SET
Confidential=Private
WHERE TypeID=1;

UPDATE Groups SET
Private=false
WHERE TypeID=1;

-- Group members can now be updated (not deleted and reinserted as before)
ALTER TABLE GroupMember
ADD COLUMN `UpdateUserID` int
AFTER `InsertUserID`;

ALTER TABLE GroupMember
ADD COLUMN `UpdateTimestamp` datetime
AFTER `InsertUserID`;


-- UserAccount PersonUUID index necessary for Groups foreign key
ALTER TABLE UserAccount
ADD INDEX(`PersonUUID`);


-- Change creator id from PrincipalId to PersonUUID
ALTER TABLE Groups
CHANGE CreatorPrincipalID
CreatorPersonUUID varchar(36) not null;

UPDATE Groups g
LEFT JOIN UserAccount u ON g.CreatorPersonUUID=u.PrincipalID
SET g.CreatorPersonUUID=u.PersonUUID;


-- Create foreign key relationship between Groups creator and UserAccount
ALTER TABLE Groups
ADD CONSTRAINT FOREIGN KEY (`CreatorPersonUUID`)
REFERENCES UserAccount(`PersonUUID`)
ON DELETE NO ACTION
ON UPDATE CASCADE;


-- Change member id from PrincipalId to PersonUUID
ALTER TABLE GroupMember
CHANGE PrincipalID
PersonUUID varchar(36) not null;

UPDATE GroupMember g
LEFT JOIN UserAccount u ON g.PersonUUID=u.PrincipalID
SET g.PersonUUID=u.PersonUUID;


-- Create foreign key relationship between Group member and UserAccount
ALTER TABLE GroupMember
ADD CONSTRAINT FOREIGN KEY (`PersonUUID`)
REFERENCES UserAccount(`PersonUUID`)
ON DELETE CASCADE
ON UPDATE CASCADE;

source insertSystemDepartment.sql;