# Author: Lawrence Fyfe
# Date: 10/11/2006

truncate PublicationStatus;

insert into PublicationStatus (ID,Description,InsertUserID)
values
(1,'Published',14021),
(2,'In Press',14021),
(3,'Submitted',14021);
#(4,'In Preparation',14021);
