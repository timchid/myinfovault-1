-- Author: Pradeep K Haldiya
-- Date: 09/04/2013

-- Table updates for v4.8.

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

-- Add Appointee Role
source insertMivRole.sql;

-- Drop Audit Columns From DelegationAuthority and DossierActionType tables
ALTER TABLE `DelegationAuthority` 
        DROP COLUMN `UpdateUserID` , 
        DROP COLUMN `UpdateTimestamp` , 
        DROP COLUMN `InsertUserID` , 
        DROP COLUMN `InsertTimestamp` ;
        
ALTER TABLE `DossierActionType` 
        DROP COLUMN `UpdateUserID` , 
        DROP COLUMN `UpdateTimestamp` , 
        DROP COLUMN `InsertUserID` , 
        DROP COLUMN `InsertTimestamp` ;

-- Create AcademicAction table and extract date from Dossier table : Start
-- create AcademicAction table
source createAcademicAction.sql;

-- Feed AcademicAction table from Dossier table
INSERT INTO AcademicAction (ID,UserID,ActionTypeID,DelegationAuthorityID,EffectiveDate,InsertTimestamp,InsertUserID,UpdateTimestamp,UpdateUserID) 
SELECT d.ID,d.UserID,
        dat.ID AS `ActionTypeID`, 
        da.ID AS `DelegationAuthorityID`,
        d.EffectiveDate,
        d.InsertTimestamp,
        d.InsertUserID, 
        d.UpdateTimestamp, 
        d.UpdateUserID 
FROM Dossier d, DossierActionType dat, DelegationAuthority da 
WHERE d.ActionType = dat.ActionType AND d.DelegationAuthority = da.DelegationAuthority;

-- drop DelegationAuthority, ActionType and UserID columns from dossier 
-- and add AcademicActionID and AcademicActionFormPresent columns
ALTER TABLE `Dossier` 
        DROP COLUMN `DelegationAuthority` , 
        DROP COLUMN `ActionType` , 
        DROP COLUMN `UserID` , 
        DROP COLUMN `InitiatorPrincipalId`,
        DROP COLUMN `EffectiveDate`,
        ADD COLUMN `AcademicActionID` INT NULL  AFTER `ID`,
        ADD COLUMN AcademicActionFormPresent boolean NOT NULL DEFAULT false AFTER RecommendedActionFormPresent;
                
-- Here we assume Dossier.AcademicActionID is same as Dossier.ID
UPDATE `Dossier` SET AcademicActionID = ID;

-- Update AcademicActionID column and create foreign key and index
ALTER TABLE `Dossier` CHANGE COLUMN `AcademicActionID` `AcademicActionID` INT NOT NULL  , 
  ADD CONSTRAINT `fk_dossier_academic_action`
  FOREIGN KEY (`AcademicActionID` )
  REFERENCES `AcademicAction` (`ID` )
  ON DELETE NO ACTION
  ON UPDATE NO ACTION
, ADD INDEX `fk_dossier_academic_action_idx` (`AcademicActionID` ASC);
-- Create AcademicAction table and extract date from Dossier table : End
        
-- Create tables for Appointment action type
source createAppointmentDetails.sql;
source createActionAssignment.sql;
source createAssignmentsTitle.sql;

ALTER TABLE `UserAccount` DROP COLUMN `PrincipalID`;

-- Droping PrincipalID cloumn from view
source createUserAssignmentView.sql;

-- Dossiers view for lookup only
source createDossiersView.sql;

ALTER TABLE `DossierActionType` CHANGE COLUMN `ActionType`
`ActionType` ENUM ('MERIT',
                    'PROMOTION',
                    'APPOINTMENT',
                    'APPOINTMENT_ENDOWED_CHAIR',
                    'APPOINTMENT_ENDOWED_PROFESSORSHIP',
                    'APPOINTMENT_ENDOWED_SPECIALIST',
                    'APPOINTMENT_INITIAL_CONTINUING',
                    'APPOINTMENT_CHANGE_DEPARTMENT',
                    'APPOINTMENT_CHANGE_TITLE',
                    'APPRAISAL',
                    'DEFERRAL_FIRST_YEAR',
                    'DEFERRAL_SECOND_YEAR',
                    'DEFERRAL_THIRD_YEAR',
                    'DEFERRAL_FOURTH_YEAR',
                    'EMERITUS_STATUS',
                    'FIVE_YEAR_REVIEW',
                    'FIVE_YEAR_REVIEW_DEPARTMENT_CHAIR',
                    'REAPPOINTMENT',
                    'REAPPOINTMENT_ENDOWED_CHAIR',
                    'REAPPOINTMENT_ENDOWED_PROFESSORSHIP',
                    'REAPPOINTMENT_ENDOWED_SPECIALIST') NOT NULL;

source insertDossierActionType.sql;

-- Updated configuration for new Biography Form and uploadable documents
source insertDocument.sql;
source insertCollectionDocument.sql;
alter table DossierAttributeType ADD COLUMN ActionTypeIds varchar(255) NULL DEFAULT NULL AFTER Display;
alter table DossierAttributeType modify column Type ENUM ('DOCUMENTUPLOAD','DOCUMENT','ACTION','HEADER') NOT NULL;
source insertDossierAttributeType.sql;
source insertDelegationAuthorityAttributeCollection.sql;
source insertDossierAttributeLocation.sql;
source insertRoutingPathDefinitions.sql;
source insertMivRole.sql;
source insertSection.sql;
source insertSystemSectionHeader.sql;

/*
 * Moving decision fields to single Decision table.
 */
source convertDecisions.sql;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
