-- Author: Rick Hendricks
-- Created: 4/07/2009
-- Updated: 2013-09-05

-- Create the main table to track Dossiers
-- Dependencies: The AcademicAction table must exist.

create table Dossier
(
   ID int primary key auto_increment,
   AcademicActionID int NOT null,
   DossierID int NOT null,
   SubmittedDate datetime not null,
   LastRoutedDate datetime,
   ArchivedDate datetime,
   CompletedDate datetime,
   PrimarySchoolID int NOT null,
   PrimaryDepartmentID int NOT null,
   ReviewType ENUM ('CRC_REVIEW') default null,
   RecommendedActionFormPresent boolean NOT NULL default false,
   AcademicActionFormPresent boolean NOT NULL default false,
   WorkflowLocations varchar(255) NOT null,
   WorkflowStatus varchar(1) null, 
   InsertTimestamp timestamp default current_timestamp,
   InsertUserID int NOT NULL,
   UpdateTimestamp datetime,
   UpdateUserID int,
   unique index (DossierID),
   KEY `fk_dossier_academic_action` (`AcademicActionID`),
   CONSTRAINT `fk_dossier_academic_action` FOREIGN KEY (`AcademicActionID`) 
   REFERENCES `AcademicAction` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
)
engine = InnoDB
default character set utf8;
