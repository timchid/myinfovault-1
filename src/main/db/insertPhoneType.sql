# Author: Lawrence Fyfe
# Date: 10/25/2006

truncate PhoneType;

insert into PhoneType (ID,Description,InsertUserID)
values
(1,'Permanent Home Phone',14021),
(2,'Permanent Fax',14021),
(3,'Current Home Phone',14021),
(4,'Current Fax',14021),
(5,'Office Phone',14021),
(6,'Office Fax',14021),
(7,'Cell Phone',14021);
