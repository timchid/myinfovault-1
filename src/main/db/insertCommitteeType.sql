# Author: Lawrence Fyfe
# Date: 8/20/2007

truncate CommitteeType;

insert into CommitteeType (ID,Description,Sequence,InsertUserID)
values
(1,'Department/Section',1,18470),
(2,'School/College/Division',2,18470),
(3,'Campus',3,18470),
(4,'Systemwide',4,18470),
(5,'Other University',5,18470),
(6,'Other Non-University',6,18470);
