# Author: Lawrence Fyfe
# Date: 10/18/2006

create table PositionDescription
(
   ID int primary key auto_increment,
   UserID int,
   PercentTime varchar(10),
   Content mediumtext,
   Sequence int,
   Display boolean NOT NULL default true,
   InsertTimestamp timestamp default current_timestamp,
   InsertUserID int,
   UpdateTimestamp datetime,
   UpdateUserID int,
   index (UserID)
)
engine = InnoDB
default character set utf8;
