-- Author: Rick Hendricks
-- Created: 2012-09-07
-- Note: Replace 18635 with your internal system ID

-- The DossierRoutingPathDefinitions table describes the next workflow node (NextWorkflowLocation) and
-- the previous workflow node (PreviousWorkflowLocation) that are available for routing to and from the
-- defined (CurrentWorkflowLocation) location. There is a definition by DelegationAuthority for each location (CurrentWorkflowLocation)
-- through which a dossier may be routed.
-- There may be multiple next workflow node locations defined for a particular current location, which 
-- implies that each of these defined locations is an optional next location which may be selected.
-- 
-- This table joined with the DossierActionType table to build a routing table map which contains entries by
-- DelegationAuthority, ActionType and Location. Each map entry carries information about the current workflow node
-- as well as the previous and next workflow nodes which are valid for the current location. 
--
--
-- The LocationPrerequisiteAttributes column contains data which defines the attributes their values which must 
-- be satisfied to enable routing *to* the defined location. Because there may be multiple definitions for a sing location, 
-- as described above, the LocationPrerequisites will be the same in each of the records defined. I'm sure there is a better
-- way to normalize the table to define a single LocationPrerequisiteAttributes value for a location, but for now this is
-- sufficient as the select and associated code to load the routing path definitions will handle it.   
-- 
-- AttributeName=Value:Value1:Value2:Value_N
-- 
-- Where AttributeName is a name from the Name column of the AttributeTypes table
-- Where Value:Value_1:Value_2:Value_N is a colon delimited list of different values which will satisfy the prerequisite for the named attribute 
-- 
-- There may be any number of comma separated attribute name and value pairs.
-- AttributeName=Value,AttributeName_1=Value,AttributeName_2=Value
-- 
-- It is possible to define a set prerequisite attributes in a manner that requires that only one in the set must be satisfied. Such definitions are
-- the same as outlined above with the exception that they must be enclosed in parenthesis:
-- (AttributeName_3=Value,AttributeName_4=Value,AttributeName_5=Value) 
-- 
-- As an example the below definition indicates that AttributeName and AttributeName_1 must be satisfied as well as any one of AttributeName_4, AttributeName_5 or AttributeName_6:
-- AttributeName=Value,AttributeName_1=Value,(AttributeName_4=Value,AttributeName_5=Value,AttributeName_6=Value) 
--
-- Query to use to check locations
-- select PreviousWorkflowLocation as PrevNodeName, CurrentWorkflowLocation as CurrentNodeName, NextWorkflowLocation as NextNodeName from 
-- DossierActionType at,
-- RoutingPathDefinitions rd
-- where
-- rd.RoutingPathType='REDELEGATED' and
-- at.ActionType='MERIT' and
-- CurrentNodeName='Archive'  
--  order by rd.Sequence;
-- 
-- ENHANCEMENT, MIV-5572
-- One may now define a routing path for an action type under a delegation
-- of authority. A definition with an action type of 'null' will apply to 
-- all action types (overridden by one with a specified action type).

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

TRUNCATE RoutingPathDefinitions;

INSERT INTO RoutingPathDefinitions (DelegationAuthority, ActionType, CurrentWorkflowLocation, WorkflowNodeType, NextWorkFlowLocation, PreviousWorkflowLocation, LocationPrerequisiteAttributes, Sequence, DisplayOnlyWhenAvailable, InsertUserID)
VALUES

-- redelegated routing type

-- PacketRequest(1)
-- Can go forward the Department, but there is no backward movement from this point. This is the first node
-- in the workflow.
('REDELEGATED', null, 'PACKETREQUEST', 'INITIAL', 'DEPARTMENT', 'UNKNOWN', null, 1, false, 18635),

-- Department(2)
-- Can go forward the School or back to the PacketRequest
('REDELEGATED', null, 'DEPARTMENT', 'ENROUTE', 'SCHOOL', 'PACKETREQUEST', 'packet_request=CLOSE', 2, true, 18635),

-- School(3) there are 4 possible locations to which an item can be routed from the school
-- Can go forward the Senate, Federation, SenateFederation or ReadyForPostReviewAudit or back to the Department
('REDELEGATED', null, 'SCHOOL', 'ENROUTE', 'SENATE', 'DEPARTMENT', 'packet_request=null:CLOSE:CANCEL,biography_form=ADDED,department_letter=ADDED,j_department_letter=ADDED,disclosure_certificate=SIGNED:SIGNEDIT,j_disclosure_certificate=SIGNED:SIGNEDIT,review=null:CLOSE,vote=null:CLOSE', 3, false, 18635),
('REDELEGATED', null, 'SCHOOL', 'ENROUTE', 'FEDERATION', 'DEPARTMENT', 'packet_request=null:CLOSE:CANCEL,biography_form=ADDED,department_letter=ADDED,j_department_letter=ADDED,disclosure_certificate=SIGNED:SIGNEDIT,j_disclosure_certificate=SIGNED:SIGNEDIT,review=null:CLOSE,vote=null:CLOSE', 4, false, 18635),
('REDELEGATED', null, 'SCHOOL', 'ENROUTE', 'SENATEFEDERATION', 'DEPARTMENT', 'packet_request=null:CLOSE:CANCEL,biography_form=ADDED,department_letter=ADDED,j_department_letter=ADDED,disclosure_certificate=SIGNED:SIGNEDIT,j_disclosure_certificate=SIGNED:SIGNEDIT,review=null:CLOSE,vote=null:CLOSE', 5, false, 18635),
('REDELEGATED', null, 'SCHOOL', 'ENROUTE', 'READYFORPOSTREVIEWAUDIT', 'DEPARTMENT', 'packet_request=null:CLOSE:CANCEL,biography_form=ADDED,department_letter=ADDED,j_department_letter=ADDED,disclosure_certificate=SIGNED:SIGNEDIT,j_disclosure_certificate=SIGNED:SIGNEDIT,review=null:CLOSE,vote=null:CLOSE', 6, false, 18635),

-- REAPPOINTMENT actions
-- School(3) there are 4 possible locations to which an item can be routed from the school
-- Can go forward the Senate, Federation, SenateFederation or ReadyForPostReviewAudit or back to the Department
('REDELEGATED', 'REAPPOINTMENT', 'SCHOOL', 'ENROUTE', 'SENATE', 'DEPARTMENT', 'packet_request=null:CLOSE:CANCEL,biography_form=ADDED,department_letter=ADDED,j_department_letter=ADDED,review=null:CLOSE,vote=null:CLOSE', 3, false, 18635),
('REDELEGATED', 'REAPPOINTMENT', 'SCHOOL', 'ENROUTE', 'FEDERATION', 'DEPARTMENT', 'packet_request=null:CLOSE:CANCEL,biography_form=ADDED,department_letter=ADDED,j_department_letter=ADDED,review=null:CLOSE,vote=null:CLOSE', 4, false, 18635),
('REDELEGATED', 'REAPPOINTMENT', 'SCHOOL', 'ENROUTE', 'SENATEFEDERATION', 'DEPARTMENT', 'packet_request=null:CLOSE:CANCEL,biography_form=ADDED,department_letter=ADDED,j_department_letter=ADDED,review=null:CLOSE,vote=null:CLOSE', 5, false, 18635),
('REDELEGATED', 'REAPPOINTMENT', 'SCHOOL', 'ENROUTE', 'READYFORPOSTREVIEWAUDIT', 'DEPARTMENT', 'packet_request=null:CLOSE:CANCEL,biography_form=ADDED,department_letter=ADDED,j_department_letter=ADDED,review=null:CLOSE,vote=null:CLOSE', 6, false, 18635),

-- Senate(9)
-- Can either go forward to the PostSenateSchool or back to the School
('REDELEGATED', null, 'SENATE', 'ENROUTE', 'POSTSENATESCHOOL', 'SCHOOL', 'review=null:CLOSE,dean_release=null:HOLD,joint_dean_release=null:HOLD,deans_final_decision=null,j_deans_final_decision=null', 7, false, 20001),

-- Federation(10)
-- Can either go forward to the PostSenateSchool or back to the School
('REDELEGATED', null, 'FEDERATION', 'ENROUTE', 'POSTSENATESCHOOL', 'SCHOOL', 'review=null:CLOSE,dean_release=null:HOLD,joint_dean_release=null:HOLD,deans_final_decision=null,j_deans_final_decision=null', 8, false, 20001),

-- SenateFederation(11)
-- Can either go forward to the PostSenateSchool or back to the School
('REDELEGATED', null, 'SENATEFEDERATION', 'ENROUTE', 'POSTSENATESCHOOL', 'SCHOOL', 'review=null:CLOSE,dean_release=null:HOLD,joint_dean_release=null:HOLD,deans_final_decision=null,j_deans_final_decision=null', 9, false, 20001),

-- PostSenateSchool (12)
-- Since arrival at this node can be from one of three optional locations, there are 3 possible locations to which
-- an item can be returned from the PostSenateSchool
-- Can either goward to the ReadyForPostReviewAudit or back to the Senate, Federation, or FederationSenate.
('REDELEGATED', null, 'POSTSENATESCHOOL',  'ENROUTE', 'READYFORPOSTREVIEWAUDIT', 'SENATE', 'review=null:CLOSE,cap_recommendation=ADDED,(aspc_recommendation=ADDED,afpc_recommendation=ADDED,jpc_recommendation=ADDED)', 10, false, 18635),
('REDELEGATED', null, 'POSTSENATESCHOOL', 'ENROUTE',  'READYFORPOSTREVIEWAUDIT', 'FEDERATION', 'review=null:CLOSE,cap_recommendation=ADDED,(aspc_recommendation=ADDED,afpc_recommendation=ADDED,jpc_recommendation=ADDED)', 11, false, 18635),
('REDELEGATED', null, 'POSTSENATESCHOOL', 'ENROUTE',  'READYFORPOSTREVIEWAUDIT', 'SENATEFEDERATION', 'review=null:CLOSE,cap_recommendation=ADDED,(aspc_recommendation=ADDED,afpc_recommendation=ADDED,jpc_recommendation=ADDED)', 12, false, 18635),

-- ReadyForPostReviewAudit(5)
-- Since arrival at this node can be from one of two optional locations, there are 2 possible locations to which
-- an item can be returned from the ReadyForPostReviewAudit.
('REDELEGATED', null, 'READYFORPOSTREVIEWAUDIT',  'ENROUTE', 'POSTAUDITREVIEW', 'SCHOOL', 'review=null:CLOSE,dean_release=null:RELEASE,joint_dean_release=null:RELEASE,deans_final_decision=ADDED,j_deans_recommendation=ADDED', 13, false, 20001),
('REDELEGATED', null, 'READYFORPOSTREVIEWAUDIT',  'ENROUTE', 'POSTAUDITREVIEW', 'POSTSENATESCHOOL', 'review=null:CLOSE,dean_release=null:RELEASE,joint_dean_release=null:RELEASE,deans_final_decision=ADDED,j_deans_recommendation=ADDED', 14, false, 20001),

-- PostAuditReview(14) there are 4 possible locations to which an item can be routed from post audit review
-- Can go forward the SenateAppeal, FederationAppeal, FederationSenateAppeal or Archive
-- No return is possible from this location 
('REDELEGATED', null, 'POSTAUDITREVIEW',  'ENROUTE', 'SENATEAPPEAL', 'UNKNOWN', null, 15, false, 18635),
('REDELEGATED', null, 'POSTAUDITREVIEW',  'ENROUTE', 'FEDERATIONAPPEAL', 'UNKNOWN', null, 16, false, 18635),
('REDELEGATED', null, 'POSTAUDITREVIEW',  'ENROUTE', 'FEDERATIONSENATEAPPEAL', 'UNKNOWN', null, 17, false, 18635),
('REDELEGATED', null, 'POSTAUDITREVIEW',  'ENROUTE', 'ARCHIVE', 'UNKNOWN', null, 18, false, 18635),

-- SenateAppeal(15)
-- Can either go forward to the PostAppealSchool or back to the PostAudit
('REDELEGATED', null, 'SENATEAPPEAL', 'ENROUTE', 'POSTAPPEALSCHOOL', 'POSTAUDITREVIEW', 'review=null:CLOSE,appeal_request=ADDED', 19, true, 18635),

-- FederationAppeal(16)
-- Can either go forward to the PostAppealSchool or back to the PostAudit
('REDELEGATED', null, 'FEDERATIONAPPEAL', 'ENROUTE', 'POSTAPPEALSCHOOL', 'POSTAUDITREVIEW', 'review=null:CLOSE,appeal_request=ADDED', 20, true, 18635),

-- SenateFederationAppeal(17)
-- Can either go forward to the PostAppealSchool or back to the PostAudit
('REDELEGATED', null, 'FEDERATIONSENATEAPPEAL', 'ENROUTE', 'POSTAPPEALSCHOOL', 'POSTAUDITREVIEW', 'review=null:CLOSE,appeal_request=ADDED', 21, true, 18635),

-- PostAppealSchool (18)
-- Since arrival at this node can be from one of three optional locations, there are 3 possible locations to which
-- an item can be returned from the PostSenateSchool
-- Can either go forward to the Archive or back to the SenateAppeal, FederationAppeal, or FederationSenateAppeal.
('REDELEGATED', null, 'POSTAPPEALSCHOOL', 'ENROUTE', 'ARCHIVE', 'SENATEAPPEAL', 'review=null:CLOSE,(cap_appeal_recommendation=ADDED,capac_appeal_recommendation=ADDED,senate_shadow_committee_appeal_recommendation=ADDED),(aspc_appeal_recommendation=ADDED,afpc_appeal_recommendation=ADDED,jpc_appeal_recommendation=ADDED,federation_shadow_committee_appeal_recommendation=ADDED)', 22, false, 18635),
('REDELEGATED', null, 'POSTAPPEALSCHOOL', 'ENROUTE', 'ARCHIVE', 'FEDERATIONAPPEAL', 'review=null:CLOSE,(cap_appeal_recommendation=ADDED,capac_appeal_recommendation=ADDED,senate_shadow_committee_appeal_recommendation=ADDED),(aspc_appeal_recommendation=ADDED,afpc_appeal_recommendation=ADDED,jpc_appeal_recommendation=ADDED,federation_shadow_committee_appeal_recommendation=ADDED)', 23, false, 18635),
('REDELEGATED', null, 'POSTAPPEALSCHOOL', 'ENROUTE', 'ARCHIVE', 'FEDERATIONSENATEAPPEAL', 'review=null:CLOSE,(cap_appeal_recommendation=ADDED,capac_appeal_recommendation=ADDED,senate_shadow_committee_appeal_recommendation=ADDED),(aspc_appeal_recommendation=ADDED,afpc_appeal_recommendation=ADDED,jpc_appeal_recommendation=ADDED,federation_shadow_committee_appeal_recommendation=ADDED)', 24, false, 18635),

-- Archive(6)
-- No movement allowed once at the Archive - No next and no previous location. This is the last node of the workflow. 
('REDELEGATED', null, 'ARCHIVE', 'FINAL', 'UNKNOWN', 'UNKNOWN', 'review=null:CLOSE,appeal_request=null,dean_appeal_release=RELEASE,deans_appeal_decision=ADDED,joint_dean_appeal_release=RELEASE,j_deans_appeal_decision=ADDED', 25, false, 20001),

-- non-reledelgated routing type

-- PacketRequest(1)
-- Can go forward the Department, but there is no backward movement from this point. This is the first node
-- in the workflow.
('NON_REDELEGATED', null, 'PACKETREQUEST', 'INITIAL', 'DEPARTMENT', 'UNKNOWN', null, 1, false, 18635),

-- Department(2)
-- Can go forward the School or back to the PacketRequest
('NON_REDELEGATED', null, 'DEPARTMENT', 'ENROUTE', 'SCHOOL', 'PACKETREQUEST', 'packet_request=CLOSE', 2, true, 18635),


-- School(3)
-- Can go forward the ViceProvost or back to the Department
('NON_REDELEGATED', null, 'SCHOOL', 'ENROUTE', 'VICEPROVOST', 'DEPARTMENT', 'packet_request=null:CLOSE:CANCEL,biography_form=ADDED,department_letter=ADDED,j_department_letter=ADDED,disclosure_certificate=SIGNED:SIGNEDIT,j_disclosure_certificate=SIGNED:SIGNEDIT,review=null:CLOSE,vote=null:CLOSE', 3, false, 18635),

-- REAPPOINTMENT actions
-- School(3)
-- Can go forward the ViceProvost or back to the Department
('NON_REDELEGATED', 'REAPPOINTMENT', 'SCHOOL', 'ENROUTE', 'VICEPROVOST', 'DEPARTMENT', 'packet_request=null:CLOSE:CANCEL,biography_form=ADDED,department_letter=ADDED,j_department_letter=ADDED,review=null:CLOSE,vote=null:CLOSE', 3, false, 18635),

-- ViceProvost(4) there are 4 possible locations to which an item can be routed from the viceprovost
-- Can go forward the Senate, Federation, SenateFederation or ReadyForPostReviewAudit or back to the School
('NON_REDELEGATED', null, 'VICEPROVOST', 'ENROUTE', 'SENATE',  'SCHOOL', 'review=null:CLOSE,dean_recommendation_release=RELEASE,deans_recommendation=ADDED,joint_dean_release=RELEASE,j_deans_recommendation=ADDED', 4, false, 20001),
('NON_REDELEGATED', null, 'VICEPROVOST', 'ENROUTE', 'FEDERATION', 'SCHOOL', 'review=null:CLOSE,dean_recommendation_release=RELEASE,dean_recommendation_release=RELEASE,deans_recommendation=ADDED,joint_dean_release=RELEASE,j_deans_recommendation=ADDED', 5, false, 20001),
('NON_REDELEGATED', null, 'VICEPROVOST', 'ENROUTE', 'SENATEFEDERATION', 'SCHOOL', 'review=null:CLOSE,dean_recommendation_release=RELEASE,dean_recommendation_release=RELEASE,deans_recommendation=ADDED,joint_dean_release=RELEASE,j_deans_recommendation=ADDED', 6, false, 20001),
('NON_REDELEGATED', null, 'VICEPROVOST', 'ENROUTE', 'POSTAUDITREVIEW', 'SCHOOL', 'review=null:CLOSE,dean_recommendation_release=RELEASE,dean_recommendation_release=RELEASE,deans_recommendation=ADDED,joint_dean_release=RELEASE,j_deans_recommendation=ADDED', 7, false, 20001),

-- Senate(9)
-- Can either go forward to the PostSenateViceProvost or back to the ViceProvost
('NON_REDELEGATED', null, 'SENATE', 'ENROUTE', 'POSTSENATEVICEPROVOST', 'VICEPROVOST', 'review=null:CLOSE,vp_release=null:HOLD,vp_recommendation_release=null:HOLD,p_recommendation_release=null:HOLD,p_tenure_release=null:HOLD,p_release=null:HOLD,chancellor_release=null:HOLD,vice_provosts_final_decision=null,viceprovosts_recommendation=null,provosts_recommendation=null,provosts_tenure_recommendation=null,provosts_tenure_decision=null,provosts_final_decision=null,chancellors_final_decision=null', 8, false, 20001),

-- Federation(10)
-- Can either go forward to the PostSenateViceProvost or back to the ViceProvost
('NON_REDELEGATED', null, 'FEDERATION', 'ENROUTE', 'POSTSENATEVICEPROVOST', 'VICEPROVOST', 'review=null:CLOSE,vp_release=null:HOLD,vp_recommendation_release=null:HOLD,p_recommendation_release=null:HOLD,p_tenure_release=null:HOLD,p_release=null:HOLD,chancellor_release=null:HOLD,vice_provosts_final_decision=null,viceprovosts_recommendation=null,provosts_recommendation=null,provosts_tenure_recommendation=null,provosts_tenure_decision=null,provosts_final_decision=null,chancellors_final_decision=null', 9, false, 20001),

-- SenateFederation(11)
-- Can either go forward to the PostSenateViceProvost or back to the ViceProvost
('NON_REDELEGATED', null, 'SENATEFEDERATION', 'ENROUTE', 'POSTSENATEVICEPROVOST', 'VICEPROVOST', 'review=null:CLOSE,vp_release=null:HOLD,vp_recommendation_release=null:HOLD,p_recommendation_release=null:HOLD,p_tenure_release=null:HOLD,p_release=null:HOLD,chancellor_release=null:HOLD,vice_provosts_final_decision=null,viceprovosts_recommendation=null,provosts_recommendation=null,provosts_tenure_recommendation=null,provosts_tenure_decision=null,provosts_final_decision=null,chancellors_final_decision=null', 10, false, 20001),

-- PostSenateViceProvost (13)
-- Since arrival at this node can be from one of three optional locations, there are 3 possible locations to which
-- an item can be returned from the PostSenateViceProvost
-- Can either go forward to the ReadyForPostReviewAudit or back to the Senate, Federation, or FederationSenate.
('NON_REDELEGATED', null, 'POSTSENATEVICEPROVOST', 'ENROUTE', 'POSTAUDITREVIEW', 'SENATE', 'review=null:CLOSE,cap_recommendation=ADDED,(aspc_recommendation=ADDED,afpc_recommendation=ADDED,jpc_recommendation=ADDED)', 11, false, 18635),
('NON_REDELEGATED', null, 'POSTSENATEVICEPROVOST', 'ENROUTE', 'POSTAUDITREVIEW', 'FEDERATION', 'review=null:CLOSE,cap_recommendation=ADDED,(aspc_recommendation=ADDED,afpc_recommendation=ADDED,jpc_recommendation=ADDED)', 12, false, 18635),
('NON_REDELEGATED', null, 'POSTSENATEVICEPROVOST', 'ENROUTE', 'POSTAUDITREVIEW', 'SENATEFEDERATION', 'review=null:CLOSE,cap_recommendation=ADDED,(aspc_recommendation=ADDED,afpc_recommendation=ADDED,jpc_recommendation=ADDED)', 13, false, 18635),

-- PostAuditReview(14) there are 4 possible locations to which an item can be routed from post audit review
-- Can go forward the SenateAppeal, FederationAppeal, SenateFederationAppeal or Archive
-- No return is possible from this location 
('NON_REDELEGATED', null, 'POSTAUDITREVIEW',  'ENROUTE', 'SENATEAPPEAL', 'UNKNOWN', 'review=null:CLOSE,(vp_release=RELEASE,vp_recommendation_release=RELEASE,p_recommendation_release=RELEASE,p_tenure_release=RELEASE,p_release=RELEASE,chancellor_release=RELEASE),(viceprovosts_final_decision=ADDED,provosts_final_decision=ADDED,provosts_tenure_decision=ADDED,chancellors_final_decision=ADDED)', 15, false, 20001),
('NON_REDELEGATED', null, 'POSTAUDITREVIEW',  'ENROUTE', 'FEDERATIONAPPEAL', 'UNKNOWN', 'review=null:CLOSE,(vp_release=RELEASE,vp_recommendation_release=RELEASE,p_recommendation_release=RELEASE,p_tenure_release=RELEASE,p_release=RELEASE,chancellor_release=RELEASE),(viceprovosts_final_decision=ADDED,provosts_final_decision=ADDED,provosts_tenure_decision=ADDED,chancellors_final_decision=ADDED)', 16, false, 20001),
('NON_REDELEGATED', null, 'POSTAUDITREVIEW',  'ENROUTE', 'FEDERATIONSENATEAPPEAL', 'UNKNOWN', 'review=null:CLOSE,(vp_release=RELEASE,vp_recommendation_release=RELEASE,p_recommendation_release=RELEASE,p_tenure_release=RELEASE,p_release=RELEASE,chancellor_release=RELEASE),(viceprovosts_final_decision=ADDED,provosts_final_decision=ADDED,provosts_tenure_decision=ADDED,chancellors_final_decision=ADDED)', 17, false, 20001),
('NON_REDELEGATED', null, 'POSTAUDITREVIEW',  'ENROUTE', 'ARCHIVE', 'UNKNOWN', 'review=null:CLOSE,(vp_release=RELEASE,vp_recommendation_release=RELEASE,p_recommendation_release=RELEASE,p_tenure_release=RELEASE,p_release=RELEASE,chancellor_release=RELEASE),(viceprovosts_final_decision=ADDED,provosts_final_decision=ADDED,provosts_tenure_decision=ADDED,chancellors_final_decision=ADDED)', 18, false, 20001),

-- SenateAppeal(15)
-- Can either go forward to the PostAppealSchool or back to the PostAudit
('NON_REDELEGATED', null, 'SENATEAPPEAL', 'ENROUTE', 'POSTAPPEALVICEPROVOST', 'POSTAUDITREVIEW', 'review=null:CLOSE,appeal_request=ADDED', 19, true, 18635),

-- FederationAppeal(16)
-- Can either go forward to the PostAppealSchool or back to the PostAudit
('NON_REDELEGATED', null, 'FEDERATIONAPPEAL', 'ENROUTE', 'POSTAPPEALVICEPROVOST', 'POSTAUDITREVIEW', 'review=null:CLOSE,appeal_request=ADDED', 20, true, 18635),

-- SenateFederationAppeal(17)
-- Can either go forward to the PostAppealSchool or back to the PostAudit
('NON_REDELEGATED', null, 'FEDERATIONSENATEAPPEAL', 'ENROUTE', 'POSTAPPEALVICEPROVOST', 'POSTAUDITREVIEW', 'review=null:CLOSE,appeal_request=ADDED', 21, true, 18635),

-- PostAppealViceProvost (19)
-- Since arrival at this node can be from one of three optional locations, there are 3 possible locations to which
-- an item can be returned from the PostAppealViceProvost
-- Can either go forward to the Archive or back to the SenateAppeal, FederationAppeal, or FederationSenateAppeal.
('NON_REDELEGATED', null, 'POSTAPPEALVICEPROVOST', 'ENROUTE', 'ARCHIVE', 'SENATEAPPEAL', 'review=null:CLOSE,(cap_appeal_recommendation=ADDED,capac_appeal_recommendation=ADDED,senate_shadow_committee_appeal_recommendation=ADDED),(aspc_appeal_recommendation=ADDED,afpc_appeal_recommendation=ADDED,jpc_appeal_recommendation=ADDED,federation_shadow_committee_appeal_recommendation=ADDED)', 22, false, 18635),
('NON_REDELEGATED', null, 'POSTAPPEALVICEPROVOST', 'ENROUTE', 'ARCHIVE', 'FEDERATIONAPPEAL', 'review=null:CLOSE,(cap_appeal_recommendation=ADDED,capac_appeal_recommendation=ADDED,senate_shadow_committee_appeal_recommendation=ADDED),(aspc_appeal_recommendation=ADDED,afpc_appeal_recommendation=ADDED,jpc_appeal_recommendation=ADDED,federation_shadow_committee_appeal_recommendation=ADDED)', 23, false, 18635),
('NON_REDELEGATED', null, 'POSTAPPEALVICEPROVOST', 'ENROUTE', 'ARCHIVE', 'FEDERATIONSENATEAPPEAL', 'review=null:CLOSE,(cap_appeal_recommendation=ADDED,capac_appeal_recommendation=ADDED,senate_shadow_committee_appeal_recommendation=ADDED),(aspc_appeal_recommendation=ADDED,afpc_appeal_recommendation=ADDED,jpc_appeal_recommendation=ADDED,federation_shadow_committee_appeal_recommendation=ADDED)', 24, false, 18635),

-- Archive(6)
-- No movement allowed once at the Archive - No next and no previous location. This is the last node of the workflow. 
('NON_REDELEGATED', null, 'ARCHIVE', 'FINAL', 'UNKNOWN', 'UNKNOWN', 'review=null:CLOSE,appeal_request=null,(vp_appeal_release=RELEASE,vp_appeal_recommendation_release=RELEASE,p_appeal_recommendation_release=RELEASE,p_tenure_appeal_release=RELEASE,p_appeal_release=RELEASE,chancellor_appeal_release=RELEASE),(viceprovosts_appeal_decision=ADDED,provosts_appeal_final_decision=ADDED,provosts_tenure_appeal_decision=ADDED,chancellors_appeal_final_decision=ADDED)', 25, false, 20001);

COMMIT;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
