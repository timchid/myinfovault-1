-- Author: Stephen Paulsen
-- Created: 2012-05-31
-- Updated: -

-- Table updates for removal of KEW workflow. MIV release version TBD.

source functionNextval.sql

source createSequenceData.sql
source insertSequenceData.sql

-- re-create adding 'Unknown' location to enum
source createWorkflowLocation.sql
source insertWorkflowLocation.sql

source createDossierActionType.sql
source insertDossierActionType.sql

source createWorkflowRoutingSequence.sql
source insertWorkflowRoutingSequence.sql

source insertBiosketchSectionLimits.sql
