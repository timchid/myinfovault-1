-- Author: Rick Hendricks
-- Date: 05/07/2010

-- Add column for v3.3.2.
ALTER TABLE `DossierSnapshot` ADD COLUMN `MivVersion` varchar(10) default "3.0" AFTER `SnapshotLocation`;
