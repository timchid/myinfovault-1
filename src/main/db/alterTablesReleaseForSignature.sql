-- v3.2 "Release for Signature"
-- Add "HOLD" and "RELEASE" to enum values
ALTER TABLE DossierAttributes CHANGE COLUMN Value Value ENUM ('ADDED','CLOSED','CLOSE','OPEN','COMPLETE','SIGNED','SIGNEDIT','REQUESTED','REQUESTEDIT','MISSING', 'HOLD', 'RELEASE') NULL;
UPDATE DossierAttributes SET Value='CLOSE' WHERE Value='CLOSED';
ALTER TABLE DossierAttributes CHANGE COLUMN Value Value ENUM ('ADDED','CLOSE','OPEN','COMPLETE','SIGNED','SIGNEDIT','REQUESTED','REQUESTEDIT','MISSING', 'HOLD', 'RELEASE') NULL;

-- The changes are also in createDossierAttributes.sql : this file can be deleted after the 3.2 release.
