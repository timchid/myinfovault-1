# Author: Lawrence Fyfe
# Date: 10/09/2006

DROP TABLE IF EXISTS PublicationType;
create table PublicationType
(
   ID int primary key auto_increment,
   Description varchar(255),
   InsertTimestamp timestamp default current_timestamp,
   InsertUserID int,
   UpdateTimestamp datetime,
   UpdateUserID int
)
engine = InnoDB
default character set utf8;
