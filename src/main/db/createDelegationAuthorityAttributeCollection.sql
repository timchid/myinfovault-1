# Author: Rick Hendricks
# Date: 07/23/2009
# This table defines a collection of DossierAttributeLocation items for a delegation authority.

drop table if exists DelegationAuthorityAttributeCollection;

create table DelegationAuthorityAttributeCollection
(
   ID int primary key auto_increment,
   DelegationAuthorityID int,
   DossierAttributeLocationID int,
   InsertTimestamp timestamp default current_timestamp,
   InsertUserID int,
   UpdateTimestamp datetime,
   UpdateUserID int
)
engine = InnoDB
default character set utf8;
