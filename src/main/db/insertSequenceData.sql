-- Author: Rick Hendricks
-- Date: 2011-12-02
--
-- This will create the dossier id sequence with an increment value of 1 and default max value.
-- It will set the current sequence value, which is the next value that will be used, to one
-- more than the current maximum DossierId in use.

INSERT INTO sequence_data
        (sequence_name, sequence_increment, sequence_cur_value, sequence_cycle)
VALUES
        ('dossierid_sequence', 1, (SELECT MAX(DossierId)+1 FROM Dossier), false);

