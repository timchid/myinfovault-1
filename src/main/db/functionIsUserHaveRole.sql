-- Author : Pradeep Kumar Haldiya
-- Created: 08/15/2011

DELIMITER $$

-- Function to check for User have give role or not
DROP FUNCTION IF EXISTS `isUserHaveRole`$$
CREATE FUNCTION `isUserHaveRole`(vUserId INT(11),vRoleId INT(11)) RETURNS tinyint(1)
BEGIN
	DECLARE lResult BOOLEAN DEFAULT FALSE;

	SELECT ifnull((SELECT true FROM `RoleAssignment`
			WHERE `UserID`=vUserId AND `RoleID`=vRoleId  LIMIT 1),false) INTO lResult;  

	RETURN lResult;

END$$

DELIMITER ;
