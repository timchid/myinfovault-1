--  Author: Stephen Paulsen
-- Created: 2009-03-03
-- Updated: 2011-03-18

-- Create the SystemDepartment table
-- Dependencies: SystemSchool table must exist

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

CREATE SCHEMA IF NOT EXISTS `myinfovault` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ;
USE `myinfovault`;

-- -----------------------------------------------------
-- Table `myinfovault`.`SystemDepartment`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `myinfovault`.`SystemDepartment` ;

CREATE TABLE `myinfovault`.`SystemDepartment` (
  `ID` INT NOT NULL AUTO_INCREMENT,
  `SchoolID` INT NOT NULL,
  `DepartmentID` INT NOT NULL,
  `Description` VARCHAR(255) NOT NULL,
  `Abbreviation` VARCHAR(20) DEFAULT '',
  `Active` BOOLEAN NOT NULL DEFAULT TRUE,
  `InsertTimestamp` timestamp DEFAULT current_timestamp,
  `InsertUserID` INT NOT NULL,
  `UpdateTimestamp` datetime DEFAULT NULL,
  `UpdateUserID` INT DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE INDEX `sch_dept` (`DepartmentID` ASC, `SchoolID` ASC),
  INDEX `SchoolID` (`SchoolID` ASC),
  CONSTRAINT `SchoolID`
    FOREIGN KEY (`SchoolID` )
    REFERENCES `myinfovault`.`SystemSchool` (`SchoolID` )
    ON DELETE NO ACTION
    ON UPDATE CASCADE)
ENGINE = InnoDB
default character set utf8
collate = utf8_general_ci;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;


--
-- create table SystemDepartment
-- (
--    ID int NOT NULL primary key auto_increment,
--    DepartmentID int NOT NULL,
--    SchoolID
--    Description varchar(255) NOT NULL,
--
--    InsertTimestamp timestamp default current_timestamp,
--    InsertUserID int,
--    UpdateTimestamp datetime,
--    UpdateUserID int,
-- )
-- engine = InnoDB
-- default character set utf8;
--
