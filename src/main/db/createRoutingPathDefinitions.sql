-- Author: Rick Hendricks
-- Date: 05/09/2013
-- This table defines the paths available at each of the workflow locations

drop table if exists RoutingPathDefinitions;

create table RoutingPathDefinitions
(
   DelegationAuthority ENUM ('REDELEGATED','NON_REDELEGATED'),
   ActionType ENUM ('MERIT',                                   
                    'PROMOTION',
                    'APPOINTMENT',
                    'APPOINTMENT_ENDOWED_CHAIR',           
                    'APPOINTMENT_ENDOWED_PROFESSORSHIP',   
                    'APPOINTMENT_ENDOWED_SPECIALIST',      
                    'APPOINTMENT_INITIAL_CONTINUING',      
                    'APPOINTMENT_CHANGE_DEPARTMENT',       
                    'APPOINTMENT_CHANGE_TITLE',            
                    'APPRAISAL',                           
                    'DEFERRAL_FIRST_YEAR',                 
                    'DEFERRAL_SECOND_YEAR',                
                    'DEFERRAL_THIRD_YEAR',                 
                    'DEFERRAL_FOURTH_YEAR',                
                    'EMERITUS_STATUS',                     
                    'FIVE_YEAR_REVIEW',                    
                    'FIVE_YEAR_REVIEW_DEPARTMENT_CHAIR',   
                    'REAPPOINTMENT',                       
                    'REAPPOINTMENT_ENDOWED_CHAIR',         
                    'REAPPOINTMENT_ENDOWED_PROFESSORSHIP', 
                    'REAPPOINTMENT_ENDOWED_SPECIALIST') NULL,
   CurrentWorkflowLocation  ENUM ('UNKNOWN',
                                  'PACKETREQUEST',
                                  'DEPARTMENT',
                                  'SCHOOL',
                                  'VICEPROVOST',
                                  'READYFORPOSTREVIEWAUDIT',
                                  'ARCHIVE',
                                  'FEDERATIONSCHOOL',
                                  'FEDERATIONVICEPROVOST',
                                  'SENATE',
                                  'FEDERATION',
                                  'SENATEFEDERATION',
                                  'POSTSENATESCHOOL',
                                  'POSTSENATEVICEPROVOST',
                                  'POSTAUDITREVIEW',
                                  'SENATEAPPEAL',
                                  'FEDERATIONAPPEAL',
                                  'FEDERATIONSENATEAPPEAL',
                                  'POSTAPPEALSCHOOL',
                                  'POSTAPPEALVICEPROVOST'),
   WorkflowNodeType ENUM ('INITIAL','ENROUTE','EXCEPTION', 'FINAL') NOT null COMMENT 'Describes the type of workflow node',
   PreviousWorkflowLocation  ENUM ('UNKNOWN',
                                  'PACKETREQUEST',
                                  'DEPARTMENT',
                                  'SCHOOL',
                                  'VICEPROVOST',
                                  'READYFORPOSTREVIEWAUDIT',
                                  'ARCHIVE',
                                  'FEDERATIONSCHOOL',
                                  'FEDERATIONVICEPROVOST',
                                  'SENATE',
                                  'FEDERATION',
                                  'SENATEFEDERATION',
                                  'POSTSENATESCHOOL',
                                  'POSTSENATEVICEPROVOST',
                                  'POSTAUDITREVIEW',
                                  'SENATEAPPEAL',
                                  'FEDERATIONAPPEAL',
                                  'FEDERATIONSENATEAPPEAL',
                                  'POSTAPPEALSCHOOL',
                                  'POSTAPPEALVICEPROVOST'),
  NextWorkflowLocation  ENUM ('UNKNOWN',
                                  'PACKETREQUEST',
                                  'DEPARTMENT',
                                  'SCHOOL',
                                  'VICEPROVOST',
                                  'READYFORPOSTREVIEWAUDIT',
                                  'ARCHIVE',
                                  'FEDERATIONSCHOOL',
                                  'FEDERATIONVICEPROVOST',
                                  'SENATE',
                                  'FEDERATION',
                                  'SENATEFEDERATION',
                                  'POSTSENATESCHOOL',
                                  'POSTSENATEVICEPROVOST',
                                  'POSTAUDITREVIEW',
                                  'SENATEAPPEAL',
                                  'FEDERATIONAPPEAL',
                                  'FEDERATIONSENATEAPPEAL',
                                  'POSTAPPEALSCHOOL',
                                  'POSTAPPEALVICEPROVOST'),
   LocationPrerequisiteAttributes varchar(1024) COMMENT 'Name from the DossierAttributeType table with value of the attributes required to move here from the previous workflow location',
   Sequence int,   
   DisplayOnlyWhenAvailable boolean DEFAULT FALSE COMMENT 'Make the routing location visible on the page only when available. The default value of FALSE will display the location, but it will not be selectable.',   
   UNIQUE(`DelegationAuthority`, `ActionType`, `CurrentWorkflowLocation`,`NextWorkflowLocation`,`PreviousWorkflowLocation`),
   InsertTimestamp timestamp default current_timestamp,
   InsertUserID int,
   UpdateTimestamp datetime,
   UpdateUserID int
)
engine = InnoDB
default character set utf8;
