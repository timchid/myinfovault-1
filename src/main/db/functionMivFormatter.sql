-- Author : Pradeep Kumar Haldiya
-- Created: 2012-01-05
-- Updated: 2012-01-19

DELIMITER $$

-- Function to format text
DROP FUNCTION IF EXISTS `mivNumberFormatter`$$
CREATE FUNCTION `mivNumberFormatter`(vText VARCHAR(1000)) RETURNS varchar(1000)
BEGIN
	DECLARE lResult VARCHAR(1000) DEFAULT null;
	DECLARE number BIGINT UNSIGNED DEFAULT 0;
	
  	IF vText IS NOT NULL THEN		
	    CASE 
	        WHEN vText REGEXP '^-?[0-9]+$' THEN
	            SET number = vText;
	            SET lResult = FORMAT(number,0);
	        ELSE 
	            SET lResult = vText;
	    END CASE;
	END IF;
	RETURN lResult;
END$$

DELIMITER ;

