# Author: Lawrence Fyfe
# Date: 10/11/2006

create table PublicationStatus
(
   ID int primary key auto_increment,
   Description varchar(255),
   InsertTimestamp timestamp default current_timestamp,
   InsertUserID int,
   UpdateTimestamp datetime,
   UpdateUserID int
)
engine = InnoDB
default character set utf8;
