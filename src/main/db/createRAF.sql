-- Author: Craig Gilmore
-- Created: 04/16/2009
-- Updated: 12/10/2009

-- Create all the RAF related tables
-- Dependencies: Dossier table must exist

use myinfovault;

drop table if exists RAF_StatusRanks;
drop table if exists RAF_FormStatuses;
drop table if exists RAF_FormDepartments;
drop table if exists RAF_Forms;

create table RAF_Forms (
        ID                  int                   primary key auto_increment,
        DossierID           int                   not null UNIQUE,
        CandidateName       varchar(255)          not null,
        AppointmentType9mo  boolean               not null,
        AppointmentType11mo boolean               not null,
        Acceleration        tinyint               not null,
        YearsAtRank         tinyint               not null,
        YearsAtStep         tinyint               not null,
        RecommendationDept  boolean               not null default true,
        RecommendationCSD   enum('YES','NO','NA') not null default 'NA',
        RecommendationJAS   enum('YES','NO','NA') not null default 'NA',
        RecommendationAFP   enum('YES','NO','NA') not null default 'NA',

        PrimarySchoolID     int                   not null,
        PrimaryDepartmentID int                   not null,

        InsertTimestamp timestamp default current_timestamp,
        InsertUserID    int,
        UpdateTimestamp datetime,
        UpdateUserID    int,

        constraint fk_rafforms_DossierID foreign key ind_DossierID (DossierID) references Dossier(DossierID) on delete cascade
) ENGINE = InnoDB default character set utf8 collate = utf8_general_ci;

create table RAF_FormDepartments (
        ID                  int                   primary key auto_increment,
        FormID              int                   not null,
        DepartmentType      enum('PRIMARY','JOINT') not null,
        PercentOfTime       varchar(3)            not null,
        SchoolID            int                   not null,
        DepartmentID        int                   not null,
        DepartmentName      varchar(255)          not null,
        SchoolName          varchar(255)          not null,

        InsertTimestamp timestamp default current_timestamp,
        InsertUserID    int,
        UpdateTimestamp datetime,
        UpdateUserID    int,

        constraint fk_rafformdepartments_FormID foreign key ind_FormID (FormID) references RAF_Forms(ID) on delete cascade
) ENGINE = InnoDB default character set utf8 collate = utf8_general_ci;

create table RAF_FormStatuses (
        ID              int                  primary key auto_increment,
        FormID          int                  not null,
        StatusType      enum('PRESENT', 'PROPOSED') not null,

        InsertTimestamp timestamp default current_timestamp,
        InsertUserID    int,
        UpdateTimestamp datetime,
        UpdateUserID    int,

        constraint fk_rafformstatuses_FormID foreign key ind_FormID (FormID) references RAF_Forms(ID) on delete cascade
) ENGINE = InnoDB default character set utf8 collate = utf8_general_ci;

create table RAF_StatusRanks (
        ID                  int                  primary key auto_increment,
        StatusID            int                  not null,
        RankAndStep         varchar(100)         not null,
        PercentOfTime       varchar(3)           not null,
        TitleCode           smallint(4)          not null,
        MonthlySalary       double(9,2)           not null,
        AnnualSalary        double(9,2)           not null,

        InsertTimestamp timestamp default current_timestamp,
        InsertUserID    int,
        UpdateTimestamp datetime,
        UpdateUserID    int,

        constraint fk_rafstatusranks_StatusID foreign key ind_StatusID (StatusID) references RAF_FormStatuses(ID) on delete cascade
) ENGINE = InnoDB default character set utf8 collate = utf8_general_ci;
