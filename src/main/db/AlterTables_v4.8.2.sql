source insertMivRole.sql;
source AlterTables_Annotations.sql;

ALTER TABLE DossierAttributeType ADD DisplayProperty varchar(50) DEFAULT NULL after Type;

source insertDocument.sql;
source insertCollectionDocument.sql;
source insertDossierView.sql;
source insertDossierAttributeType.sql;
source insertDelegationAuthorityAttributeCollection.sql;
source insertDossierAttributeLocation.sql;
source insertRoutingPathDefinitions.sql;

-- dossier description is now derived
ALTER TABLE Dossier DROP COLUMN Description;

-- Change 'dean_release' attribute to 'joint_dean_release' if attribute is not primary
UPDATE DossierAttributes a
LEFT JOIN Dossier d ON a.DossierID=d.DossierID
SET a.AttributeTypeID=107
WHERE a.AttributeTypeID=32
AND (a.SchoolID != d.PrimarySchoolID OR a.DepartmentID != d.PrimaryDepartmentID);

-- Change 'dean_appeal_release' attribute to 'joint_dean_appeal_release' if attribute is not primary
UPDATE DossierAttributes a
LEFT JOIN Dossier d ON a.DossierID=d.DossierID
SET a.AttributeTypeID=108
WHERE a.AttributeTypeID=47
AND (a.SchoolID != d.PrimarySchoolID OR a.DepartmentID != d.PrimaryDepartmentID);

-- Change 'dean_release' attribute to 'dean_recommendation_release' if attribute is primary and dossier is NON_REDELEGATED
UPDATE DossierAttributes a
LEFT JOIN Dossier d ON a.DossierID=d.DossierID
LEFT JOIN AcademicAction x ON d.AcademicActionID=x.ID
SET a.AttributeTypeID=109
WHERE a.AttributeTypeID=32
AND a.SchoolID=d.PrimarySchoolID
AND a.DepartmentID=d.PrimaryDepartmentID
AND x.DelegationAuthorityID=2;

-- Allows tomcat user to call stored procedures from Java, introspecting the parameter types to do proper conversions.
GRANT SELECT ON mysql.proc TO 'tomcat'@'%';

-- Dossier may not have multiple RAFs
ALTER TABLE RAF_Forms ADD UNIQUE(DossierID);


-- One or both of these view updates have been missed on at least some systems.
-- It doesn't hurt to run these again, and they can easily be run individually and independently
-- from this release script.  These should be the last thing done in this AlterTables_v4.8.2.
-- I recommend running these two in this order, since the second uses the first.
source createAppointmentAssignmentView.sql
source createUserAssignmentView.sql
