-- Author : Pradeep Kumar Haldiya
-- Created: 08/15/2011

DELIMITER $$

-- Create Logger Table
DROP TABLE IF EXISTS `logger`$$
CREATE TABLE `logger` (
  `idlogger` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(10) NOT NULL DEFAULT 'INFO',
  `msg` varchar(500) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `logtime` datetime DEFAULT NULL,
  PRIMARY KEY (`idlogger`)
)
engine = InnoDB
default character set utf8$$

DELIMITER ;
