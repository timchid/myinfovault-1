# Author: Stephen Paulsen
# Date: 2007-02-02

# Inserts on CvStation -- Ag Experiment Station

DELIMITER |

CREATE TRIGGER InsertCvStation AFTER INSERT on CvStation
FOR EACH ROW BEGIN
  IF NEW.UploadName IS NULL
  THEN
  INSERT INTO ReportSummary
    SET
       UserID=NEW.SSN,
       OldID=NEW.AESID,
       TypeID=1,
       ReportNumber=NEW.Snum,
       ReportTitle=NEW.Title,
       InvestigatorName=NEW.InvName,
       DateSpan=NEW.Dates,
       Sequence=NEW.Seq,
       Display=true,
       InsertTimestamp=now(),
       InsertUserID=NEW.SSN
    ;
  ELSE
  INSERT INTO UserUploadDocument
    SET
       UserID=NEW.SSN,
       DocumentID=9,
       UploadFile=NEW.UploadName,
       Year=NEW.Dates,
       Display=true,
       InsertTimestamp=now(),
       InsertUserID=NEW.SSN
    ;
  END IF;
END;
|

DELIMITER ;

