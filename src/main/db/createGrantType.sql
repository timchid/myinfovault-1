# Author: Lawrence Fyfe
# Date: 11/29/2006

create table GrantType
(
   ID int primary key auto_increment,
   Description varchar(40),
   Sequence int,
   InsertTimestamp timestamp default current_timestamp,
   InsertUserID int,
   UpdateTimestamp datetime,
   UpdateUserID int
)
engine = InnoDB
default character set utf8;
