# Author: Lawrence Fyfe
# Date: 11/01/2006

create table UserLink
(
   ID int primary key auto_increment,
   UserID int,
   Value varchar(255),
   Label varchar(50),
   Display boolean NOT NULL default true,
   InsertTimestamp timestamp default current_timestamp,
   InsertUserID int,
   UpdateTimestamp datetime,
   UpdateUserID int,
   index (UserID)
)
engine = InnoDB
default character set utf8;
