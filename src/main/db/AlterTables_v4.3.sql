--  Author: Craig Gilmore
-- Created: 2011-08-16
-- Updated: 2011-08-16

-- Table updates for v4.3

-- UserAccount PersonUUID index necessary for Groups foreign key
ALTER TABLE UserAccount
ADD INDEX(`PersonUUID`);


source createGroupType.sql;
source createGroups.sql;
source createGroupMember.sql;
source createGroupRoleAssignment.sql;

source insertGroupType.sql;

ALTER TABLE DossierReviewers CHANGE `UserID` `EntityID` INT NOT NULL;
ALTER TABLE DossierReviewers ADD COLUMN `EntityType` ENUM('GROUP', 'USER') NOT NULL default 'user' AFTER `EntityID`;

ALTER TABLE SystemSchool ADD COLUMN `Abbreviation` VARCHAR(15) DEFAULT '' AFTER `Description`;
ALTER TABLE SystemDepartment ADD COLUMN `Abbreviation` VARCHAR(20) DEFAULT '' AFTER `Description`;

source insertSystemSchool.sql;
source insertSystemDepartment.sql;
