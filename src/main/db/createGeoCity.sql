
DROP TABLE IF EXISTS `GeoCity`;
CREATE TABLE `GeoCity` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(100) COMMENT 'Name of city or town in ASCII encoding' DEFAULT NULL,
  `StateID` varchar(2) COMMENT 'FIPS 10-4 Region Code ID' DEFAULT NULL,
  `CountryCode` varchar(2) COMMENT 'ISO 3166 Country Code' NOT NULL,
  PRIMARY KEY (`ID`)
)
ENGINE=InnoDB AUTO_INCREMENT=1 
DEFAULT CHARSET=utf8
COLLATE = utf8_general_ci;
