--  Author: Rick Hendricks
-- Created: 2012-02-03
-- Updated: 2012-02-03

-- New tables for v4.4

-- Create these first
source createWorkType.sql;
source insertWorkType.sql;

source createEventType.sql;
source insertEventType.sql;

source createEventCategory.sql;
source insertEventCategory.sql;

-- Create Event and Work Status Tabel ans insert some initial data
source createWorkStatus.sql;
source insertWorkStatus.sql;

source createEventStatus.sql;
source insertEventStatus.sql;

source createPublicationEvents.sql;

source createWorks.sql;
source createEvents.sql;
source createReviews.sql;
source createWorksEventsAssociation.sql;
source createWorksPublicationEventsAssociation.sql
source createWorksReviewsAssociation.sql;
source createEventsReviewsAssociation.sql;

-- Table updates for v4.4
source insertDocument.sql;
source insertDocumentSection.sql;
source insertCollectionDocument.sql;

source createSection.sql;
source insertSection.sql;

source insertBiosketchSectionLimits.sql;
source insertSystemSectionHeader.sql;

source insertDropdownMaster.sql;
source insertDropdownDetails.sql;
