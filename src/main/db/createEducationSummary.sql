# Author: Lawrence Fyfe
# Date: 10/23/2006

create table EducationSummary
(
   ID int primary key auto_increment COMMENT 'Record unique identifier',
   UserID int COMMENT 'ID corresponding to the the user associated with this record', -- TODO: ref UserAccount
   Degree varchar(30) COMMENT 'Abbreviation of degree',
   Institution varchar(100) COMMENT 'Awarding institution',
   Location varchar(50) COMMENT 'Location of awarding institution',
   StartDate varchar(20) COMMENT 'Year started at instituion for degree',
   StartMonthId int COMMENT 'ID corresponding to the month started at the instution',
   EndDate varchar(20) COMMENT 'Year finishing at instituion for degree',
   EndMonthId int COMMENT 'ID corresponding to the month finishing at the instution',
   Field varchar(100) COMMENT 'Degree field of study',
   Sponsor varchar(50) COMMENT 'Degree sponsor/mentor',
   Sequence int COMMENT 'Relative order in which this record should appear when listed',
   Display boolean NOT NULL default true COMMENT 'Should the record be displayed?',
   Remark mediumtext COMMENT 'Additional remarks',
   DisplayRemark boolean NOT NULL default true COMMENT 'Should the remarks be displayed?',
   InsertTimestamp timestamp default current_timestamp COMMENT 'The date-time the record was inserted',
   InsertUserID int COMMENT 'ID corresponding to the real user that inserted the record',
   UpdateTimestamp datetime COMMENT 'The date-time the record was last updated',
   UpdateUserID int COMMENT 'ID corresponding to the real user that last updated the record',
   index (UserID)
)
engine = InnoDB
default character set utf8
COMMENT 'Education: Education and taining';
