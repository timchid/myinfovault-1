-- Author : Pradeep Kumar Haldiya
-- Created: 08/15/2011

DELIMITER $$

-- Procedure to Insert Role Assignments
-- @param vUserId
-- @param vRoleID
-- @param vScope
-- @param vPrimaryRole
DROP PROCEDURE IF EXISTS `PROC_INSERT_ROLE_ASSIGNMENT`$$
CREATE PROCEDURE `PROC_INSERT_ROLE_ASSIGNMENT`(	in vUserId INT(11), 
												in vRoleID INT(11),
												in vScope VARCHAR(255),
												in vPrimaryRole BOOLEAN)
BEGIN
	/****************************************************************************/
	/* Declare Variables 												    	*/
	/****************************************************************************/
	DECLARE sqlScript VARCHAR(4000) DEFAULT null;

	-- just for log
	SET sqlScript = CONCAT('INSERT INTO `RoleAssignment` (UserID,RoleID,Scope,PrimaryRole,InsertTimestamp) VALUES (?,?,?,?,?)');
	SET sqlScript = CONCAT(sqlScript, ' VALUES(', CONCAT_WS(',',vUserId,vRoleID,ifnull(vScope,'null'),vPrimaryRole,CAST(now() AS CHAR(25))),')');
	CALL log_sql(sqlScript);

	/* Insert appointment data into RoleAssignment */
	SET @_UserID = vUserId;
	SET @_RoleID = vRoleID;
	SET @_Scope = vScope;
	SET @_PrimaryRole = vPrimaryRole;
	SET @_InsertTimestamp = now();
	PREPARE stmt FROM 'INSERT INTO `RoleAssignment` (UserID,RoleID,Scope,PrimaryRole,InsertTimestamp) VALUES (?,?,?,?,?)';
	EXECUTE stmt USING @_UserID,@_RoleID,@_Scope,@_PrimaryRole,@_InsertTimestamp;	

END$$

DELIMITER ;
