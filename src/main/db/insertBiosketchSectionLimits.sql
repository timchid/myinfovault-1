-- -----------------------------------------------------
-- Data for table `myinfovault`.`BiosketchSectionLimits`
-- -----------------------------------------------------

select count(*) from BiosketchSectionLimits into @_ORIG_COUNT;

truncate BiosketchSectionLimits;

-- NIH sections

INSERT INTO `BiosketchSectionLimits` (`ID`, `BiosketchType`, `SectionID`, `SectionName`, `Availability`, `IsMainSection`, `InSelectList`, `MayHideHeader`, `InsertUserID`)
VALUES
(1, 3, 59,    'personal', 'MANDATORY', false, false, false, 19012),
(2, 3, 68,    'education', 'MANDATORY', true, true, false, 19012),
(3, 3, 10008, 'personalstatement-header', 'MANDATORY', false, false, false, 18635),
(4, 3, 10000, 'positions-header', 'MANDATORY', false, false, false, 19012),
(5, 3, 66,    'employment', 'MANDATORY', false, true, false, 19012),
(6, 3, 85,    'service-committee', 'MANDATORY', false, true, false, 19012),
(7, 3, 21,    'service-board', 'MANDATORY', false, true, false, 19012),
(8, 3, 53,    'honors', 'MANDATORY', false, true, false, 19012),
(9, 3, 10002, 'publications-header', 'OPTIONAL_ON', false, false, false, 19012),
(10, 3, 77,    'nih-publications', 'OPTIONAL_ON', true, false, false, 19012);



INSERT INTO `BiosketchSectionLimits` (`ID`, `BiosketchType`, `SectionID`, `SectionName`, `Availability`, `IsMainSection`, `InSelectList`, `MayHideHeader`, `DisplayInBiosketch`, `InsertUserID`)
VALUES
(11, 3, 78,   'abstracts', 'OPTIONAL_ON', false, true, false, 1, 19012),
(12, 3, 83,   'journals', 'OPTIONAL_ON', false, true, false, 2, 19012);

INSERT INTO `BiosketchSectionLimits` (`ID`, `BiosketchType`, `SectionID`, `SectionName`, `Availability`, `IsMainSection`, `InSelectList`, `MayHideHeader`, `InsertUserID`)
VALUES
(13, 3, 10001,'grants-header', 'OPTIONAL_ON', false, false, false, 19012),
(15, 3, 86,   'grants-ongoing-research', 'OPTIONAL_ON', false, true, false, 19012),
(16, 3, 87,   'grants-completed-research', 'OPTIONAL_ON', false, true, false, 19012),
(17, 3, 88,   'grants-other-research', 'OPTIONAL_ON', false, true, false, 19012);


-- CV sections
INSERT INTO `BiosketchSectionLimits` (`ID`, `BiosketchType`, `SectionID`, `SectionName`, `Availability`, `IsMainSection`, `InSelectList`, `MayHideHeader`, `DisplayInBiosketch`, `InsertUserID`)
VALUES
(101, 2, 59,   'personal', 'OPTIONAL_ON', true, true, true, 1000, 19012),
(102, 2, 60,   'personal-address', 'OPTIONAL_ON', false, false, true, 1000, 19012),
(103, 2, 61,   'personal-phone', 'OPTIONAL_ON', false, false, true, 1000, 19012),
(104, 2, 62,   'personal-email', 'OPTIONAL_ON', false, false, true, 1000, 19012),
(105, 2, 63,   'personal-link', 'OPTIONAL_ON', false, false, true, 1000, 19012);

INSERT INTO `BiosketchSectionLimits` (`ID`, `BiosketchType`, `SectionID`, `SectionName`, `Availability`, `IsMainSection`, `InSelectList`, `MayHideHeader`, `InsertUserID`)
VALUES
(106, 2, 64,   'personal-additional', 'OPTIONAL_ON', false, true, true, 19012),
(107, 2, 65,   'interest-area', 'OPTIONAL_ON', false, true, true, 19012),
(151, 2, 55,   'agstation-reports', 'OPTIONAL_ON', true, true, true, 19012),
(201, 2, 10006,'education-header', 'OPTIONAL_ON', true, false, true, 19012),
(202, 2, 68,   'education', 'OPTIONAL_ON', false, true, true, 19012),
(203, 2, 70,   'credential', 'OPTIONAL_ON', false, true, true, 19012),
(204, 2, 69,   'education-additional', 'OPTIONAL_ON', false, true, true, 19012),
(251, 2, 10007,'employment-header', 'OPTIONAL_ON', true, false, true, 19012),
(252, 2, 66,   'employment', 'OPTIONAL_ON', false, true, true, 19012),
(253, 2, 67,   'employment-additional', 'OPTIONAL_ON', false, true, true, 19012),
(301, 2, 10005, 'knowledge-header', 'OPTIONAL_ON', true, false, true, 19012),
(302, 2, 56,   'extending-knowledge-media', 'OPTIONAL_ON', false, true, true, 19012),
(303, 2, 57,   'extending-knowledge-gathering', 'OPTIONAL_ON', false, true, true, 19012),
(304, 2, 58,   'extending-knowledge-other', 'OPTIONAL_ON', false, true, true, 19012),
(351, 2, 10001, 'grants-header', 'OPTIONAL_ON', true, false, true, 19012),
(352, 2, 54,   'grants-active', 'OPTIONAL_ON', false, true, true, 19012),
(353, 2, 73,   'grants-pending', 'OPTIONAL_ON', false, true, true, 19012),
(354, 2, 74,   'grants-completed', 'OPTIONAL_ON', false, true, true, 19012),
(355, 2, 75,   'grants-notawarded', 'OPTIONAL_ON', false, true, true, 19012),
(356, 2, 76,   'grants-none', 'OPTIONAL_ON', false, true, true, 19012),
-- (41, 2, 10008, 'honors-header', 'OPTIONAL_ON', true, false, true, 19012),
(401, 2, 53,   'honors', 'OPTIONAL_ON', true, true, true, 19012),
(451, 2, 03,   'course-evaluations', 'OPTIONAL_ON', true, true, true, 19012),
(501, 2, 02,   'position-description', 'OPTIONAL_ON', true, true, true, 19012),
(551, 2, 10002, 'publications-header', 'OPTIONAL_ON', true, false, true, 19012),
(552, 2, 24,   'journals-published', 'OPTIONAL_ON', false, true, true, 19012),
(553, 2, 25,   'journals-in-press', 'OPTIONAL_ON', false, true, true, 19012),
(554, 2, 26,   'journals-submitted', 'OPTIONAL_ON', false, true, true, 19012),
(555, 2, 33,   'book-chapters-published', 'OPTIONAL_ON', false, true, true, 19012),
(556, 2, 34,   'book-chapters-in-press', 'OPTIONAL_ON', false, true, true, 19012),
(557, 2, 35,   'book-chapters-submitted', 'OPTIONAL_ON', false, true, true, 19012),
(558, 2, 42,   'editor-letters-published', 'OPTIONAL_ON', false, true, true, 19012),
(559, 2, 43,   'editor-letters-in-press', 'OPTIONAL_ON', false, true, true, 19012),
(560, 2, 44,   'editor-letters-submitted', 'OPTIONAL_ON', false, true, true, 19012),
(561, 2, 30,   'books-edited-published', 'OPTIONAL_ON', false, true, true, 19012),
(562, 2, 31,   'books-edited-in-press', 'OPTIONAL_ON', false, true, true, 19012),
(563, 2, 32,   'books-edited-submitted', 'OPTIONAL_ON', false, true, true, 19012),
(564, 2, 36,   'reviews-published', 'OPTIONAL_ON', false, true, true, 19012),
(565, 2, 37,   'reviews-in-press', 'OPTIONAL_ON', false, true, true, 19012),
(566, 2, 38,   'reviews-submitted', 'OPTIONAL_ON', false, true, true, 19012),
(567, 2, 27,   'books-authored-published', 'OPTIONAL_ON', false, true, true, 19012),
(568, 2, 28,   'books-authored-in-press', 'OPTIONAL_ON', false, true, true, 19012),
(569, 2, 29,   'books-authored-submitted', 'OPTIONAL_ON', false, true, true, 19012),
(570, 2, 45,   'limited-published', 'OPTIONAL_ON', false, true, true, 19012),
(571, 2, 46,   'limited-in-press', 'OPTIONAL_ON', false, true, true, 19012),
(572, 2, 47,   'limited-submitted', 'OPTIONAL_ON', false, true, true, 19012),
(573, 2, 39,   'media-published', 'OPTIONAL_ON', false, true, true, 19012),
(574, 2, 40,   'media-in-press', 'OPTIONAL_ON', false, true, true, 19012),
(575, 2, 41,   'media-submitted', 'OPTIONAL_ON', false, true, true, 19012),
(576, 2, 48,   'abstracts-published', 'OPTIONAL_ON', false, true, true, 19012),
(577, 2, 49,   'abstracts-in-press', 'OPTIONAL_ON', false, true, true, 19012),
(578, 2, 50,   'abstracts-submitted', 'OPTIONAL_ON', false, true, true, 19012),
(579, 2, 51,   'presentations', 'OPTIONAL_ON', false, true, true, 19012),

(580, 2, 92,   'patent-granted', 'OPTIONAL_ON', false, true, true, 20001),
(581, 2, 98,   'patent-filed', 'OPTIONAL_ON', false, true, true, 20001),
(582, 2, 93,   'disclosure', 'OPTIONAL_ON', false, true, true, 20720),

(583, 2, 52,   'publication-additional', 'OPTIONAL_ON', false, true, true, 19012),

(600, 2, 10020, 'creativeactivities-header', 'OPTIONAL_ON', true, false, true, 18635),
(601, 2, 94,'works-completed', 'OPTIONAL_ON', false, true, true, 18635),
(602, 2, 95,'works-scheduled', 'OPTIONAL_ON', false, true, true, 18635),
(603, 2, 96,'works-submitted', 'OPTIONAL_ON', false, true, true, 18635),

(901, 2, 301,'book-completed', 'OPTIONAL_ON', false, true, true, 18635),
(902, 2, 302,'book-scheduled', 'OPTIONAL_ON', false, true, true, 18635),
(903, 2, 303,'book-submitted', 'OPTIONAL_ON', false, true, true, 18635),
(904, 2, 304,'broadcast-completed', 'OPTIONAL_ON', false, true, true, 18635),
(905, 2, 305,'broadcast-scheduled', 'OPTIONAL_ON', false, true, true, 18635),
(906, 2, 306,'broadcast-submitted', 'OPTIONAL_ON', false, true, true, 18635),
(907, 2, 307,'catalogs-completed', 'OPTIONAL_ON', false, true, true, 18635),
(908, 2, 308,'catalogs-scheduled', 'OPTIONAL_ON', false, true, true, 18635),
(909, 2, 309,'catalogs-submitted', 'OPTIONAL_ON', false, true, true, 18635),
(910, 2, 310,'cd-dvd-video-completed', 'OPTIONAL_ON', false, true, true, 18635),
(911, 2, 311,'cd-dvd-video-scheduled', 'OPTIONAL_ON', false, true, true, 18635),
(912, 2, 312,'cd-dvd-video-submitted', 'OPTIONAL_ON', false, true, true, 18635),
(913, 2, 313,'citation-completed', 'OPTIONAL_ON', false, true, true, 18635),
(914, 2, 314,'citation-scheduled', 'OPTIONAL_ON', false, true, true, 18635),
(915, 2, 315,'citation-submitted', 'OPTIONAL_ON', false, true, true, 18635),
(916, 2, 316,'commission-completed', 'OPTIONAL_ON', false, true, true, 18635),
(917, 2, 317,'commission-scheduled', 'OPTIONAL_ON', false, true, true, 18635),
(918, 2, 318,'commission-submitted', 'OPTIONAL_ON', false, true, true, 18635),
(919, 2, 319,'concert-completed', 'OPTIONAL_ON', false, true, true, 18635),
(920, 2, 320,'concert-scheduled', 'OPTIONAL_ON', false, true, true, 18635),
(921, 2, 321,'concert-submitted', 'OPTIONAL_ON', false, true, true, 18635),
(922, 2, 322,'exhibitions-group-completed', 'OPTIONAL_ON', false, true, true, 18635),
(923, 2, 323,'exhibitions-group-scheduled', 'OPTIONAL_ON', false, true, true, 18635),
(924, 2, 324,'exhibitions-group-submitted', 'OPTIONAL_ON', false, true, true, 18635),
(925, 2, 325,'exhibitions-solo-completed', 'OPTIONAL_ON', false, true, true, 18635),
(926, 2, 326,'exhibitions-solo-scheduled', 'OPTIONAL_ON', false, true, true, 18635),
(927, 2, 327,'exhibitions-solo-submitted', 'OPTIONAL_ON', false, true, true, 18635),
(928, 2, 328,'structure-landscape-completed', 'OPTIONAL_ON', false, true, true, 18635),
(929, 2, 329,'structure-landscape-scheduled', 'OPTIONAL_ON', false, true, true, 18635),
(930, 2, 330,'structure-landscape-submitted', 'OPTIONAL_ON', false, true, true, 18635),
(931, 2, 331,'fashion-show-completed', 'OPTIONAL_ON', false, true, true, 18635),
(932, 2, 332,'fashion-show-scheduled', 'OPTIONAL_ON', false, true, true, 18635),
(933, 2, 333,'fashion-show-submitted', 'OPTIONAL_ON', false, true, true, 18635),
(934, 2, 334,'interview-commentary-completed', 'OPTIONAL_ON', false, true, true, 18635),
(935, 2, 335,'interview-commentary-scheduled', 'OPTIONAL_ON', false, true, true, 18635),
(936, 2, 336,'interview-commentary-submitted', 'OPTIONAL_ON', false, true, true, 18635),
(937, 2, 337,'performance-event-completed', 'OPTIONAL_ON', false, true, true, 18635),
(938, 2, 338,'performance-event-scheduled', 'OPTIONAL_ON', false, true, true, 18635),
(939, 2, 339,'performance-event-submitted', 'OPTIONAL_ON', false, true, true, 18635),
(940, 2, 340,'private-collection-completed', 'OPTIONAL_ON', false, true, true, 18635),
(941, 2, 341,'private-collection-scheduled', 'OPTIONAL_ON', false, true, true, 18635),
(942, 2, 342,'private-collection-submitted', 'OPTIONAL_ON', false, true, true, 18635),
(943, 2, 343,'product-completed', 'OPTIONAL_ON', false, true, true, 18635),
(944, 2, 344,'product-scheduled', 'OPTIONAL_ON', false, true, true, 18635),
(945, 2, 345,'product-submitted', 'OPTIONAL_ON', false, true, true, 18635),
(946, 2, 346,'program-notes-completed', 'OPTIONAL_ON', false, true, true, 18635),
(947, 2, 347,'program-notes-scheduled', 'OPTIONAL_ON', false, true, true, 18635),
(948, 2, 348,'program-notes-submitted', 'OPTIONAL_ON', false, true, true, 18635),
(949, 2, 349,'public-collection-completed', 'OPTIONAL_ON', false, true, true, 18635),
(950, 2, 350,'public-collection-scheduled', 'OPTIONAL_ON', false, true, true, 18635),
(951, 2, 351,'public-collection-submitted', 'OPTIONAL_ON', false, true, true, 18635),
(952, 2, 352,'reading-completed', 'OPTIONAL_ON', false, true, true, 18635),
(953, 2, 353,'reading-scheduled', 'OPTIONAL_ON', false, true, true, 18635),
(954, 2, 354,'reading-submitted', 'OPTIONAL_ON', false, true, true, 18635),
(955, 2, 355,'recordings-completed', 'OPTIONAL_ON', false, true, true, 18635),
(956, 2, 356,'recordings-scheduled', 'OPTIONAL_ON', false, true, true, 18635),
(957, 2, 357,'recordings-submitted', 'OPTIONAL_ON', false, true, true, 18635),
(958, 2, 358,'reproductions-completed', 'OPTIONAL_ON', false, true, true, 18635),
(959, 2, 359,'reproductions-scheduled', 'OPTIONAL_ON', false, true, true, 18635),
(960, 2, 360,'reproductions-submitted', 'OPTIONAL_ON', false, true, true, 18635),
(961, 2, 361,'screening-completed', 'OPTIONAL_ON', false, true, true, 18635),
(962, 2, 362,'screening-scheduled', 'OPTIONAL_ON', false, true, true, 18635),
(963, 2, 363,'screening-submitted', 'OPTIONAL_ON', false, true, true, 18635),
(964, 2, 364,'theatre-production-completed', 'OPTIONAL_ON', false, true, true, 18635),
(965, 2, 365,'theatre-production-scheduled', 'OPTIONAL_ON', false, true, true, 18635),
(966, 2, 366,'theatre-production-submitted', 'OPTIONAL_ON', false, true, true, 18635),
(967, 2, 367,'curated-exhibition-completed', 'OPTIONAL_ON', false, true, true, 18635),
(968, 2, 368,'curated-exhibition-scheduled', 'OPTIONAL_ON', false, true, true, 18635),
(969, 2, 369,'curated-exhibition-submitted', 'OPTIONAL_ON', false, true, true, 18635),

(1001, 2, 501,'publicationevents-published', 'OPTIONAL_ON', false, true, true, 18635),
(1002, 2, 502,'publicationevents-in-press', 'OPTIONAL_ON', false, true, true, 18635),
(1003, 2, 503,'publicationevents-submitted', 'OPTIONAL_ON', false, true, true, 18635),
(1004, 2, 504,'reviews', 'OPTIONAL_ON', false, true, true, 18635),
(1005, 2, 509,'creative-activities-additional', 'OPTIONAL_ON', false, true, true, 18635),

(2001, 2, 10004, 'service-header', 'OPTIONAL_ON', true, false, true, 19012),
(2002, 2, 22,   'service-administrative', 'OPTIONAL_ON', false, true, true, 19012),
(2003, 2, 15,   'service-department', 'OPTIONAL_ON', false, true, true, 19012),
(2004, 2, 16,   'service-school', 'OPTIONAL_ON', false, true, true, 19012),
(2005, 2, 17,   'service-campus', 'OPTIONAL_ON', false, true, true, 19012),
(2006, 2, 18,   'service-systemwide', 'OPTIONAL_ON', false, true, true, 19012),
(2007, 2, 19,   'service-other-university', 'OPTIONAL_ON', false, true, true, 19012),
(2008, 2, 20,   'service-other-nonuniversity', 'OPTIONAL_ON', false, true, true, 19012),
(2009, 2, 21,   'service-board', 'OPTIONAL_ON', false, true, true, 19012) ,
(2010, 2, 23,   'service-additional', 'OPTIONAL_ON', false, true, true, 19012),

(3001, 2, 10003, 'teaching-header', 'OPTIONAL_ON', true, false, true, 19012),
(3002, 2, 10,   'teaching-contact-hours', 'OPTIONAL_ON', false, true, true, 19012),
(3003, 2, 04,   'teaching-courses', 'OPTIONAL_ON', false, true, true, 19012),
(3004, 2, 09,   'teaching-curriculum', 'OPTIONAL_ON', false, true, true, 19012),
(3005, 2, 12,   'teaching-supplement-term', 'OPTIONAL_ON', false, true, true, 19012),
(3006, 2, 13,   'teaching-supplement-month', 'OPTIONAL_ON', false, true, true, 19012),
(3007, 2, 08,   'teaching-special-advising', 'OPTIONAL_ON', false, true, true, 19012),
(3008, 2, 07,   'teaching-student-advising', 'OPTIONAL_ON', false, true, true, 19012),
(3009, 2, 05,   'teaching-thesis', 'OPTIONAL_ON', false, true, true, 19012),
(3010, 2, 11,   'teaching-trainees', 'OPTIONAL_ON', false, true, true, 19012),
(3011, 2, 06,   'teaching-extension', 'OPTIONAL_ON', false, true, true, 19012),
(3012, 2, 14,   'teaching-additional', 'OPTIONAL_ON', false, true, true, 19012);

SHOW WARNINGS;
commit;

select count(*) from BiosketchSectionLimits into @_NEW_COUNT;
select @_ORIG_COUNT as 'Original Count', @_NEW_COUNT as 'New Count';
