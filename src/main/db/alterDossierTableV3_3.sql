-- Author: Rick Hendricks
-- Date: 03/30/2010

-- Change column name for v3.3.0.
ALTER TABLE `Dossier`
CHANGE `InitiatorPrincipalName` `InitiatorPrincipalId` varchar(50) NOT null
;

