-- Author: Stephen Paulsen
-- Date: 2015-03-23
-- Updated: yyyy-mm-dd (initials)
--
-- Create the Packet and Content tables.

use `myinfovault`;

-- Set lenient modes for table mods.
SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';



CREATE TABLE IF NOT EXISTS `myinfovault`.`Packet` (
  `ID` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `UserId` INT NOT NULL COMMENT 'Owner of packet',
  `Name` VARCHAR(255) NOT NULL COMMENT 'File name',
  `InsertTimestamp` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Packet creation date/time, like file [ctime]',
  `InsertUserID` INT NOT NULL COMMENT 'User who created this packet',
  `UpdateTimestamp` DATETIME NULL COMMENT 'Last modified date/time, like file [mtime]',
  `UpdateUserID` INT COMMENT 'User who last modified this packet',
  INDEX `user_idx` (`UserId` ASC),
  PRIMARY KEY (`ID`),
  CONSTRAINT `fk_userid`
    FOREIGN KEY (`UserId`)
    REFERENCES `myinfovault`.`UserAccount` (`UserID`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;



CREATE TABLE IF NOT EXISTS `myinfovault`.`PacketContent` (
  `ID` INT NULL AUTO_INCREMENT,
  `PacketID` INT UNSIGNED NOT NULL,
  `UserID` INT NOT NULL,
  `SectionID` INT NOT NULL,
  `RecordID` INT NOT NULL,
  `DisplayParts` VARCHAR(255) DEFAULT 'display',
  PRIMARY KEY (`ID`),
  INDEX `packet_id` (`PacketID` ASC),
  CONSTRAINT `packet_id`
    FOREIGN KEY (`PacketID`)
    REFERENCES `myinfovault`.`Packet` (`ID`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;




-- Restore normal modes.
SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
