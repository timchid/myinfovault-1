# Author: Lawrence Fyfe
# Date: 01/22/2007

truncate MediaType;

insert into MediaType (ID,Description,InsertUserID)
values
(1,'Leaflets',14021),
(2,'Fact Sheets',14021),
(3,'Newspaper Article',14021),
(4,'Magazine Article',14021),
(5,'Television Interview',14021),
(6,'CD ROM',14021),
(7,'Website',14021),
(8,'Video',14021),
(9,'Other',14021);
