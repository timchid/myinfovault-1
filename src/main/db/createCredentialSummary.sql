# Author: Lawrence Fyfe
# Date: 10/23/2006

create table CredentialSummary
(
   ID int primary key auto_increment COMMENT 'Record unique identifier',
   UserID int COMMENT 'ID corresponding to the the user associated with this record', -- TODO: ref UserAccount
   YearSpan varchar(20) COMMENT 'Range of years ',
   Content mediumtext COMMENT 'Description of this license/certification',
   Sequence int COMMENT 'Relative order in which this record should appear when listed',
   Display boolean NOT NULL default true COMMENT 'Should the record be displayed?',
   Remark mediumtext COMMENT 'Additional remarks to use in the CV',
   DisplayRemark boolean NOT NULL default true COMMENT 'Should the remarks be displayed?',
   InsertTimestamp timestamp default current_timestamp COMMENT 'The date-time the record was inserted',
   InsertUserID int COMMENT 'ID corresponding to the real user that inserted the record',
   UpdateTimestamp datetime COMMENT 'The date-time the record was last updated',
   UpdateUserID int COMMENT 'ID corresponding to the real user that last updated the record',
   index (UserID)
)
engine = InnoDB
default character set utf8
COMMENT 'Education: Licenses and Certifications';
