-- Author: Rick Hendricks
-- Created: 2012-02-03
-- Updated: 2012-02-03

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

SELECT count(*) FROM WorkType WHERE ID <= 500 INTO @_ORIG_COUNT;

DELETE FROM WorkType WHERE ID <= 500;

INSERT into WorkType (ID, Description, ShortDescription, InsertTimestamp, InsertUserID) values 
( 1, 'CD-ROM', 'CD-ROM', CURRENT_TIMESTAMP, 18635),
( 2, 'Art: Drawing ', 'Drawing ', CURRENT_TIMESTAMP, 18635),
( 3, 'Art: Film/Video', 'Film/Video', CURRENT_TIMESTAMP, 18635),
( 4, 'Art: Illustration', 'Illustration', CURRENT_TIMESTAMP, 18635),
( 5, 'Art: Mural', 'Mural', CURRENT_TIMESTAMP, 18635),
( 6, 'Art: Painting', 'Painting', CURRENT_TIMESTAMP, 18635),
( 7, 'Art: Photography', 'Photography', CURRENT_TIMESTAMP, 18635),
( 8, 'Art: Print', 'Print', CURRENT_TIMESTAMP, 18635),
( 9, 'Art: Sculpture', 'Sculpture', CURRENT_TIMESTAMP, 18635),
(10, 'Art: Silk Screen', 'Silk Screen', CURRENT_TIMESTAMP, 18635),
(11, 'Art: Textile', 'Textile', CURRENT_TIMESTAMP, 18635),
(12, 'Choreography', 'Choreography', CURRENT_TIMESTAMP, 18635),
(13, 'Composition', 'Composition', CURRENT_TIMESTAMP, 18635),
(14, 'Curatorial Activity', 'Curatorial Activity', CURRENT_TIMESTAMP, 18635),
(15, 'Design: Architecture', 'Architecture', CURRENT_TIMESTAMP, 18635),
(16, 'Design: Costume', 'Costume', CURRENT_TIMESTAMP, 18635),
(17, 'Design: Digital Media', 'Digital Media', CURRENT_TIMESTAMP, 18635),
(18, 'Design: Environmental', 'Environmental', CURRENT_TIMESTAMP, 18635),
(19, 'Design: Exhibition', 'Exhibition', CURRENT_TIMESTAMP, 18635),
(20, 'Design: Fashion', 'Fashion', CURRENT_TIMESTAMP, 18635),
(21, 'Design: Industrial/product', 'Industrial/product', CURRENT_TIMESTAMP, 18635),
(22, 'Design: Information', 'Information', CURRENT_TIMESTAMP, 18635),
(23, 'Design: Interaction', 'Interaction', CURRENT_TIMESTAMP, 18635),
(24, 'Design: Lighting', 'Lighting', CURRENT_TIMESTAMP, 18635),
(25, 'Design: Publication', 'Publication', CURRENT_TIMESTAMP, 18635),
(26, 'Design: Set', 'Set', CURRENT_TIMESTAMP, 18635),
(27, 'Design: Textile', 'Textile', CURRENT_TIMESTAMP, 18635),
(28, 'Design: Website', 'Website', CURRENT_TIMESTAMP, 18635),
(29, 'Directing', 'Directing', CURRENT_TIMESTAMP, 18635),
(30, 'Dramaturgy', 'Dramaturgy', CURRENT_TIMESTAMP, 18635),
(31, 'Installation', 'Installation', CURRENT_TIMESTAMP, 18635),
(32, 'Performance (as a work)', 'Performance', CURRENT_TIMESTAMP, 18635),
(33, 'Playwriting', 'Playwriting', CURRENT_TIMESTAMP, 18635),
(34, 'Poetry', 'Poetry', CURRENT_TIMESTAMP, 18635);


SHOW WARNINGS;
commit;

SELECT count(*) FROM WorkType WHERE ID <= 500 INTO @_NEW_COUNT;
SELECT @_ORIG_COUNT AS 'Original Count', @_NEW_COUNT AS 'New Count';


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
