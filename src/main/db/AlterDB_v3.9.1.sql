-- Author: Stephen Paulsen
-- Date: 2011-05-31
--
-- Drop the old, long-obsolete tables from the myinfovault database.
-- Run for the MIV v3.9.1 deployment.
--

DROP TABLE CvAbstracts;
DROP TABLE CvAddDtl;
DROP TABLE CvAddDtl2;
DROP TABLE CvAudit;
DROP TABLE CvBoards;
DROP TABLE CvBookCh;
DROP TABLE CvBooks;
DROP TABLE CvBooksEd;
DROP TABLE CvComm;
DROP TABLE CvComment;
DROP TABLE CvCothers;
DROP TABLE CvDept;
DROP TABLE CvDisclose;
DROP TABLE CvEKbpem;
DROP TABLE CvEKother;
DROP TABLE CvEKwcpsc;
DROP TABLE CvEditor;
DROP TABLE CvEdu;
DROP TABLE CvEmploy;
DROP TABLE CvEva;
DROP TABLE CvHeaders;
DROP TABLE CvHonors;
DROP TABLE CvHours;
DROP TABLE CvInterest;
DROP TABLE CvJournals;
DROP TABLE CvLec;
DROP TABLE CvLetter;
DROP TABLE CvLicenses;
DROP TABLE CvLimited;
DROP TABLE CvLocks;
DROP TABLE CvMaps;
DROP TABLE CvMedia;
-- CvMem -- keeping this so we have a record of the oldest accounts
DROP TABLE CvMemOld;
DROP TABLE CvOtherLetters;
DROP TABLE CvOvetLet;
DROP TABLE CvPersonal;
DROP TABLE CvPosDesc;
DROP TABLE CvRactgrants;
DROP TABLE CvRcomgrants;
DROP TABLE CvReviews;
DROP TABLE CvRgiftgrants;
DROP TABLE CvRothers;
DROP TABLE CvRothgrants;
DROP TABLE CvRpendgrants;
DROP TABLE CvSch;
DROP TABLE CvSeq;
DROP TABLE CvSignature;
DROP TABLE CvSort;
DROP TABLE CvStatement;
DROP TABLE CvStation;
DROP TABLE CvTadvise;
-- CvTadvise2 -- Keep this new v2 table, derived from CvTadvise which we are dropping
DROP TABLE CvTdev;
DROP TABLE CvText;
DROP TABLE CvTgrants;
DROP TABLE CvTior;
DROP TABLE CvTothers;
DROP TABLE CvTrainee;
DROP TABLE CvTspec;
DROP TABLE CvTthesis;
DROP TABLE CvUnawarded;
DROP TABLE CvUser;
DROP TABLE CvVetLet;
DROP TABLE CvWeb;
