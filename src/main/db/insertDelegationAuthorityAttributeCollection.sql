-- Author: Rick Hendricks
-- Created: 2009-07-24
-- Updated: 2010-03-02

-- The DELEGATIONAUTHORITYATTRIBUTECOLLECTION table defines the relationship 
-- between the dossier delegation authority and the attributes defined for a
-- dossier's work flow location, as defined in the DOSSIERATTRIBUTELOCATION table.
-- 
-- COLUMN                      TYPE        FUNCION
-- _________________________________________________
-- ID                          int         Unique ID.
-- DelegationAuthorityID       int         Delegation Authority ID from the DELEGATIONAUTHORITY table.
-- DossierAttributeLocationID  int         Dossier Attribute Location ID from the DOSSIERATTRIBUTELOCATION table. 
-- InsertTimestamp             timestamp   The insert timestamp of the record.
-- InsertUserID                int         The user inserting the record.
-- UpdateTimestamp             datetime    The update timestamp of the record.
-- UpdateUserID                int         The user updating the record.

-- Example query to retrive attibutes for a specified delegation authority, workflow location and appointment type.
-- SELECT dloc.DossierAttributeID, dtype.Description, dtype.Name
--             FROM
--             DossierAttributeType dtype,
--             DelegationAuthorityAttributeCollection dcol,
--             DossierAttributeLocation dloc,
--             DelegationAuthority dauth,
--             WorkflowLocation wfl
--             WHERE
--             dauth.DelegationAuthority='NON_REDELEGATED' AND
--             dauth.ID=dcol.DelegationAuthorityID AND
--             wfl.WorkflowNodeName='School' AND
--             dloc.LocationID = wfl.ID AND
--             dloc.AppointmentType='JOINT' AND
--             dcol.DossierAttributeLocationID=dloc.ID AND
--             dtype.ID=dloc.DossierAttributeID
--             ORDER BY dloc.Sequence;


SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

USE `myinfovault`;

-- -----------------------------------------------------
-- Data for table `myinfovault`.`DelegationAuthorityAttributeCollection`
-- -----------------------------------------------------
TRUNCATE `DelegationAuthorityAttributeCollection`;

SET @OLD_AUTOCOMMIT=@@AUTOCOMMIT, AUTOCOMMIT=0;
INSERT INTO `DelegationAuthorityAttributeCollection` (`ID`, `DelegationAuthorityID`,`DossierAttributeLocationID`,`InsertTimestamp`, `InsertUserID`)
VALUES

-- Define the DossierAttributeLocation values valid for each DelegationAuthority

-- Redelegated
-- Packet Request
(0, 1, 1, current_timestamp, 18635),
(0, 1, 2, current_timestamp, 18635),
(0, 1, 3, current_timestamp, 18635),
(0, 1, 4, current_timestamp, 18635),
(0, 1, 5, current_timestamp, 18635),
(0, 1, 6, current_timestamp, 18635),
(0, 1, 7, current_timestamp, 18635),
(0, 1, 8, current_timestamp, 18635),
(0, 1, 9, current_timestamp, 18635),
(0, 1, 10, current_timestamp, 18635),
(0, 1, 11, current_timestamp, 18635),
(0, 1, 12, current_timestamp, 18635),
(0, 1, 13, current_timestamp, 18635),
(0, 1, 14, current_timestamp, 18635),
(0, 1, 15, current_timestamp, 18635),
(0, 1, 16, current_timestamp, 18635),
(0, 1, 17, current_timestamp, 18635),
(0, 1, 18, current_timestamp, 18635),
(0, 1, 19, current_timestamp, 18635),
(0, 1, 20, current_timestamp, 18635),
(0, 1, 21, current_timestamp, 18635),
(0, 1, 22, current_timestamp, 18635),
(0, 1, 35, current_timestamp, 18635),
(0, 1, 36, current_timestamp, 18635),
(0, 1, 222, current_timestamp, 18635),
(0, 1, 223, current_timestamp, 18635),
(0, 1, 224, current_timestamp, 18635),
(0, 1, 225, current_timestamp, 18635),
(0, 1, 226, current_timestamp, 18635),
(0, 1, 227, current_timestamp, 18635),
(0, 1, 228, current_timestamp, 18635),
(0, 1, 229, current_timestamp, 18635),
(0, 1, 241, current_timestamp, 18635),
(0, 1, 242, current_timestamp, 18635),
(0, 1, 243, current_timestamp, 18635),
(0, 1, 244, current_timestamp, 18635),
(0, 1, 245, current_timestamp, 18635),
(0, 1, 246, current_timestamp, 18635),
(0, 1, 247, current_timestamp, 18635),
(0, 1, 248, current_timestamp, 18635),
(0, 1, 320, current_timestamp, 18635),

-- Department
(0, 1, 23, current_timestamp, 18635),
(0, 1, 24, current_timestamp, 18635),
(0, 1, 25, current_timestamp, 18635),
(0, 1, 26, current_timestamp, 18635),
(0, 1, 27, current_timestamp, 18635),
(0, 1, 28, current_timestamp, 18635),
(0, 1, 29, current_timestamp, 18635),
(0, 1, 30, current_timestamp, 18635),
(0, 1, 31, current_timestamp, 18635),
(0, 1, 32, current_timestamp, 18635),
(0, 1, 33, current_timestamp, 18635),
(0, 1, 34, current_timestamp, 18635),
(0, 1, 35, current_timestamp, 18635),
(0, 1, 36, current_timestamp, 18635),
(0, 1, 37, current_timestamp, 18635),
(0, 1, 38, current_timestamp, 18635),
(0, 1, 39, current_timestamp, 18635),
(0, 1, 40, current_timestamp, 18635),
(0, 1, 41, current_timestamp, 18635),
(0, 1, 42, current_timestamp, 18635),
(0, 1, 43, current_timestamp, 18635),
(0, 1, 44, current_timestamp, 18635),
(0, 1, 214, current_timestamp, 18635),
(0, 1, 215, current_timestamp, 18635),
(0, 1, 216, current_timestamp, 18635),
(0, 1, 217, current_timestamp, 18635),
(0, 1, 218, current_timestamp, 18635),
(0, 1, 219, current_timestamp, 18635),
(0, 1, 220, current_timestamp, 18635),
(0, 1, 221, current_timestamp, 18635),
(0, 1, 234, current_timestamp, 18635),
(0, 1, 235, current_timestamp, 18635),
(0, 1, 236, current_timestamp, 18635),
(0, 1, 237, current_timestamp, 18635),
(0, 1, 238, current_timestamp, 18635),
(0, 1, 239, current_timestamp, 18635),
(0, 1, 240, current_timestamp, 18635),
(0, 1, 249, current_timestamp, 18635),
(0, 1, 321, current_timestamp, 18635),

-- School
(0, 1, 45, current_timestamp, 18635),
(0, 1, 46, current_timestamp, 18635),
(0, 1, 47, current_timestamp, 18635),
(0, 1, 48, current_timestamp, 18635),
(0, 1, 50, current_timestamp, 18635),
(0, 1, 51, current_timestamp, 18635),
(0, 1, 52, current_timestamp, 18635),
(0, 1, 61, current_timestamp, 18635),
(0, 1, 62, current_timestamp, 18635),
(0, 1, 64, current_timestamp, 18635),
(0, 1, 66, current_timestamp, 18635),
(0, 1, 171, current_timestamp, 18635),
(0, 1, 230, current_timestamp, 18635),

-- Senate
(0, 1, 116, current_timestamp, 18635),
(0, 1, 117, current_timestamp, 18635),
-- (0, 1, 118, current_timestamp, 18635),
-- (0, 1, 119, current_timestamp, 18635),
-- (0, 1, 120, current_timestamp, 18635),
-- (0, 1, 121, current_timestamp, 18635),
(0, 1, 122, current_timestamp, 18635),

-- Federation
(0, 1, 126, current_timestamp, 18635),
-- (0, 1, 127, current_timestamp, 18635),
-- (0, 1, 128, current_timestamp, 18635),
(0, 1, 129, current_timestamp, 18635),
(0, 1, 130, current_timestamp, 18635),
(0, 1, 131, current_timestamp, 18635),
(0, 1, 132, current_timestamp, 18635),

-- SenateFederation
(0, 1, 136, current_timestamp, 18635),
(0, 1, 137, current_timestamp, 18635),
-- (0, 1, 138, current_timestamp, 18635),
(0, 1, 139, current_timestamp, 18635),
(0, 1, 140, current_timestamp, 18635),
(0, 1, 141, current_timestamp, 18635),
(0, 1, 142, current_timestamp, 18635),

-- PostSenateSchool
(0, 1, 146, current_timestamp, 18635),
(0, 1, 147, current_timestamp, 18635),
(0, 1, 148, current_timestamp, 18635),
(0, 1, 150, current_timestamp, 18635),
(0, 1, 151, current_timestamp, 18635),
(0, 1, 152, current_timestamp, 18635),
(0, 1, 154, current_timestamp, 18635),
(0, 1, 155, current_timestamp, 18635),
(0, 1, 156, current_timestamp, 18635),
(0, 1, 158, current_timestamp, 18635),
(0, 1, 159, current_timestamp, 18635),
(0, 1, 199, current_timestamp, 18635),
(0, 1, 232, current_timestamp, 18635),

-- PostAuditReview
(0, 1, 172, current_timestamp, 18635),
(0, 1, 173, current_timestamp, 18635),

-- SenateAppeal
(0, 1, 174, current_timestamp, 18635),
(0, 1, 175, current_timestamp, 18635),
(0, 1, 176, current_timestamp, 18635),
(0, 1, 200, current_timestamp, 18635),
(0, 1, 201, current_timestamp, 18635),

-- FederationAppeal
(0, 1, 177, current_timestamp, 18635),
(0, 1, 178, current_timestamp, 18635),
(0, 1, 179, current_timestamp, 18635),
(0, 1, 180, current_timestamp, 18635),
(0, 1, 202, current_timestamp, 18635),
(0, 1, 203, current_timestamp, 18635),

-- FederationSenateAppeal
(0, 1, 181, current_timestamp, 18635),
(0, 1, 182, current_timestamp, 18635),
(0, 1, 183, current_timestamp, 18635),
(0, 1, 184, current_timestamp, 18635),
(0, 1, 185, current_timestamp, 18635),
(0, 1, 186, current_timestamp, 18635),
(0, 1, 204, current_timestamp, 18635),
(0, 1, 205, current_timestamp, 18635),
(0, 1, 206, current_timestamp, 18635),

-- PostAppealSchool
(0, 1, 187, current_timestamp, 18635),
(0, 1, 188, current_timestamp, 18635),
(0, 1, 189, current_timestamp, 18635),
(0, 1, 190, current_timestamp, 18635),
(0, 1, 191, current_timestamp, 18635),
(0, 1, 192, current_timestamp, 18635),
(0, 1, 193, current_timestamp, 18635),
(0, 1, 194, current_timestamp, 18635),

-- Non-Redelegated
-- Packet Request
(0, 2, 1, current_timestamp, 18635),
(0, 2, 2, current_timestamp, 18635),
(0, 2, 3, current_timestamp, 18635),
(0, 2, 4, current_timestamp, 18635),
(0, 2, 5, current_timestamp, 18635),
(0, 2, 6, current_timestamp, 18635),
(0, 2, 7, current_timestamp, 18635),
(0, 2, 8, current_timestamp, 18635),
(0, 2, 9, current_timestamp, 18635),
(0, 2, 10, current_timestamp, 18635),
(0, 2, 11, current_timestamp, 18635),
(0, 2, 12, current_timestamp, 18635),
(0, 2, 13, current_timestamp, 18635),
(0, 2, 14, current_timestamp, 18635),
(0, 2, 15, current_timestamp, 18635),
(0, 2, 16, current_timestamp, 18635),
(0, 2, 17, current_timestamp, 18635),
(0, 2, 18, current_timestamp, 18635),
(0, 2, 19, current_timestamp, 18635),
(0, 2, 20, current_timestamp, 18635),
(0, 2, 21, current_timestamp, 18635),
(0, 2, 22, current_timestamp, 18635),
(0, 2, 222, current_timestamp, 18635),
(0, 2, 223, current_timestamp, 18635),
(0, 2, 224, current_timestamp, 18635),
(0, 2, 225, current_timestamp, 18635),
(0, 2, 226, current_timestamp, 18635),
(0, 2, 227, current_timestamp, 18635),
(0, 2, 228, current_timestamp, 18635),
(0, 2, 229, current_timestamp, 18635),
(0, 2, 241, current_timestamp, 18635),
(0, 2, 242, current_timestamp, 18635),
(0, 2, 243, current_timestamp, 18635),
(0, 2, 244, current_timestamp, 18635),
(0, 2, 245, current_timestamp, 18635),
(0, 2, 246, current_timestamp, 18635),
(0, 2, 247, current_timestamp, 18635),
(0, 2, 248, current_timestamp, 18635),
(0, 2, 320, current_timestamp, 18635),

-- Department
(0, 2, 23, current_timestamp, 18635),
(0, 2, 24, current_timestamp, 18635),
(0, 2, 25, current_timestamp, 18635),
(0, 2, 26, current_timestamp, 18635),
(0, 2, 27, current_timestamp, 18635),
(0, 2, 28, current_timestamp, 18635),
(0, 2, 29, current_timestamp, 18635),
(0, 2, 30, current_timestamp, 18635),
(0, 2, 31, current_timestamp, 18635),
(0, 2, 32, current_timestamp, 18635),
(0, 2, 33, current_timestamp, 18635),
(0, 2, 34, current_timestamp, 18635),
(0, 2, 35, current_timestamp, 18635),
(0, 2, 36, current_timestamp, 18635),
(0, 2, 37, current_timestamp, 18635),
(0, 2, 38, current_timestamp, 18635),
(0, 2, 39, current_timestamp, 18635),
(0, 2, 40, current_timestamp, 18635),
(0, 2, 41, current_timestamp, 18635),
(0, 2, 42, current_timestamp, 18635),
(0, 2, 43, current_timestamp, 18635),
(0, 2, 44, current_timestamp, 18635),
(0, 2, 214, current_timestamp, 18635),
(0, 2, 215, current_timestamp, 18635),
(0, 2, 216, current_timestamp, 18635),
(0, 2, 217, current_timestamp, 18635),
(0, 2, 218, current_timestamp, 18635),
(0, 2, 219, current_timestamp, 18635),
(0, 2, 220, current_timestamp, 18635),
(0, 2, 221, current_timestamp, 18635),
(0, 2, 234, current_timestamp, 18635),
(0, 2, 235, current_timestamp, 18635),
(0, 2, 236, current_timestamp, 18635),
(0, 2, 237, current_timestamp, 18635),
(0, 2, 238, current_timestamp, 18635),
(0, 2, 239, current_timestamp, 18635),
(0, 2, 240, current_timestamp, 18635),
(0, 2, 249, current_timestamp, 18635),
(0, 2, 321, current_timestamp, 18635),

-- School
(0, 2, 45, current_timestamp, 18635),
(0, 2, 46, current_timestamp, 18635),
(0, 2, 47, current_timestamp, 18635),
(0, 2, 49, current_timestamp, 18635),
(0, 2, 50, current_timestamp, 18635),
(0, 2, 51, current_timestamp, 18635),
(0, 2, 52, current_timestamp, 18635),
(0, 2, 53, current_timestamp, 18635),
(0, 2, 59, current_timestamp, 18635),
(0, 2, 60, current_timestamp, 18635),
(0, 2, 301, current_timestamp, 20001),
(0, 2, 63, current_timestamp, 18635),
(0, 2, 64, current_timestamp, 18635),
(0, 2, 66, current_timestamp, 18635),
(0, 2, 171, current_timestamp, 18635),
(0, 2, 230, current_timestamp, 18635),

-- ViceProvost
(0, 2, 67, current_timestamp, 18635),
(0, 2, 68, current_timestamp, 18635),
(0, 2, 69, current_timestamp, 18635),
(0, 2, 208, current_timestamp, 19012),
(0, 2, 250, current_timestamp, 18635),
(0, 2, 251, current_timestamp, 18635),
(0, 2, 252, current_timestamp, 18635),
(0, 2, 253, current_timestamp, 18635),
(0, 2, 305, current_timestamp, 20001),
(0, 2, 254, current_timestamp, 18635),
(0, 2, 255, current_timestamp, 18635),
(0, 2, 306, current_timestamp, 20001),
(0, 2, 302, current_timestamp, 20001),
(0, 2, 307, current_timestamp, 20001),
(0, 2, 256, current_timestamp, 18635),
(0, 2, 257, current_timestamp, 18635),
(0, 2, 308, current_timestamp, 20001),
(0, 2, 258, current_timestamp, 18635),
(0, 2, 259, current_timestamp, 18635),
(0, 2, 260, current_timestamp, 18635),
(0, 2, 309, current_timestamp, 20001),
(0, 2, 261, current_timestamp, 18635),
(0, 2, 231, current_timestamp, 19012),
(0, 2, 207, current_timestamp, 20508),
(0, 2, 286, current_timestamp, 18635),
(0, 2, 287, current_timestamp, 18635),
(0, 2, 288, current_timestamp, 18635),
(0, 2, 295, current_timestamp, 18635),
(0, 2, 296, current_timestamp, 18635),


-- Senate
(0, 2, 116, current_timestamp, 18635),
(0, 2, 117, current_timestamp, 18635),
-- (0, 2, 118, current_timestamp, 18635),
-- (0, 2, 119, current_timestamp, 18635),
-- (0, 2, 120, current_timestamp, 18635),
-- (0, 2, 121, current_timestamp, 18635),
(0, 2, 122, current_timestamp, 18635),

-- Federation
(0, 2, 126, current_timestamp, 18635),
-- (0, 2, 127, current_timestamp, 18635),
-- (0, 2, 128, current_timestamp, 18635),
(0, 2, 129, current_timestamp, 18635),
(0, 2, 130, current_timestamp, 18635),
(0, 2, 131, current_timestamp, 18635),
(0, 2, 132, current_timestamp, 18635),

-- SenateFederation
(0, 2, 136, current_timestamp, 18635),
(0, 2, 137, current_timestamp, 18635),
-- (0, 2, 138, current_timestamp, 18635),
(0, 2, 139, current_timestamp, 18635),
(0, 2, 140, current_timestamp, 18635),
(0, 2, 141, current_timestamp, 18635),
(0, 2, 142, current_timestamp, 18635),

-- PostSenateViceProvost
(0, 2, 160, current_timestamp, 18635),
(0, 2, 161, current_timestamp, 18635),
(0, 2, 169, current_timestamp, 18635),
(0, 2, 170, current_timestamp, 18635),
(0, 2, 213, current_timestamp, 18635),
(0, 2, 233, current_timestamp, 18635),
(0, 2, 262, current_timestamp, 18635),
(0, 2, 263, current_timestamp, 18635),
(0, 2, 264, current_timestamp, 18635),
(0, 2, 265, current_timestamp, 18635),
(0, 2, 310, current_timestamp, 20001),
(0, 2, 266, current_timestamp, 18635),
(0, 2, 267, current_timestamp, 18635),
(0, 2, 311, current_timestamp, 20001),
(0, 2, 303, current_timestamp, 20001),
(0, 2, 312, current_timestamp, 20001),
(0, 2, 268, current_timestamp, 18635),
(0, 2, 269, current_timestamp, 18635),
(0, 2, 313, current_timestamp, 20001),
(0, 2, 270, current_timestamp, 18635),
(0, 2, 271, current_timestamp, 18635),
(0, 2, 272, current_timestamp, 18635),
(0, 2, 314, current_timestamp, 20001),
(0, 2, 273, current_timestamp, 18635),
(0, 2, 289, current_timestamp, 18635),
(0, 2, 290, current_timestamp, 18635),
(0, 2, 291, current_timestamp, 18635),
(0, 2, 297, current_timestamp, 18635),
(0, 2, 298, current_timestamp, 18635),

-- PostAuditReview
(0, 2, 172, current_timestamp, 18635),
(0, 2, 173, current_timestamp, 18635),

-- SenateAppeal
(0, 2, 174, current_timestamp, 18635),
(0, 2, 175, current_timestamp, 18635),
(0, 2, 176, current_timestamp, 18635),
(0, 2, 200, current_timestamp, 18635),
(0, 2, 201, current_timestamp, 18635),

-- FederationAppeal
(0, 2, 177, current_timestamp, 18635),
(0, 2, 178, current_timestamp, 18635),
(0, 2, 179, current_timestamp, 18635),
(0, 2, 180, current_timestamp, 18635),
(0, 2, 202, current_timestamp, 18635),
(0, 2, 203, current_timestamp, 18635),

-- FederationSenateAppeal
(0, 2, 181, current_timestamp, 18635),
(0, 2, 182, current_timestamp, 18635),
(0, 2, 183, current_timestamp, 18635),
(0, 2, 184, current_timestamp, 18635),
(0, 2, 185, current_timestamp, 18635),
(0, 2, 186, current_timestamp, 18635),
(0, 2, 204, current_timestamp, 18635),
(0, 2, 205, current_timestamp, 18635),
(0, 2, 206, current_timestamp, 18635),

-- PostAppealViceProvost
(0, 2, 195, current_timestamp, 18635),
(0, 2, 196, current_timestamp, 18635),
(0, 2, 197, current_timestamp, 18635),
(0, 2, 198, current_timestamp, 18635),
(0, 2, 274, current_timestamp, 18635),
(0, 2, 275, current_timestamp, 18635),
(0, 2, 276, current_timestamp, 18635),
(0, 2, 277, current_timestamp, 18635),
(0, 2, 315, current_timestamp, 20001),
(0, 2, 278, current_timestamp, 18635),
(0, 2, 279, current_timestamp, 18635),
(0, 2, 316, current_timestamp, 20001),
(0, 2, 304, current_timestamp, 20001),
(0, 2, 317, current_timestamp, 20001),
(0, 2, 280, current_timestamp, 18635),
(0, 2, 281, current_timestamp, 18635),
(0, 2, 318, current_timestamp, 20001),
(0, 2, 282, current_timestamp, 18635),
(0, 2, 283, current_timestamp, 18635),
(0, 2, 284, current_timestamp, 18635),
(0, 2, 319, current_timestamp, 20001),
(0, 2, 285, current_timestamp, 18635),
(0, 2, 292, current_timestamp, 18635),
(0, 2, 293, current_timestamp, 18635),
(0, 2, 294, current_timestamp, 18635),
(0, 2, 299, current_timestamp, 18635),
(0, 2, 300, current_timestamp, 18635),

-- Redelegated Federation
(0, 3, 1, current_timestamp, 18635),
(0, 3, 2, current_timestamp, 18635),
(0, 3, 3, current_timestamp, 18635),
(0, 3, 4, current_timestamp, 18635),
(0, 3, 5, current_timestamp, 18635),
(0, 3, 6, current_timestamp, 18635),
(0, 3, 7, current_timestamp, 18635),
(0, 3, 8, current_timestamp, 18635),
(0, 3, 9, current_timestamp, 18635),
(0, 3, 10, current_timestamp, 18635),
(0, 3, 11, current_timestamp, 18635),
(0, 3, 12, current_timestamp, 18635),
(0, 3, 13, current_timestamp, 18635),
(0, 3, 14, current_timestamp, 18635),
(0, 3, 15, current_timestamp, 18635),
(0, 3, 16, current_timestamp, 18635),
(0, 3, 17, current_timestamp, 18635),
(0, 3, 18, current_timestamp, 18635),
(0, 3, 19, current_timestamp, 18635),
(0, 3, 20, current_timestamp, 18635),
(0, 3, 21, current_timestamp, 18635),
(0, 3, 22, current_timestamp, 18635),
(0, 3, 23, current_timestamp, 18635),
(0, 3, 24, current_timestamp, 18635),
(0, 3, 25, current_timestamp, 18635),
(0, 3, 26, current_timestamp, 18635),
(0, 3, 27, current_timestamp, 18635),
(0, 3, 28, current_timestamp, 18635),
(0, 3, 29, current_timestamp, 18635),
(0, 3, 30, current_timestamp, 18635),
(0, 3, 31, current_timestamp, 18635),
(0, 3, 32, current_timestamp, 18635),
(0, 3, 33, current_timestamp, 18635),
(0, 3, 34, current_timestamp, 18635),
(0, 3, 35, current_timestamp, 18635),
(0, 3, 36, current_timestamp, 18635),
(0, 3, 37, current_timestamp, 18635),
(0, 3, 38, current_timestamp, 18635),
(0, 3, 39, current_timestamp, 18635),
(0, 3, 40, current_timestamp, 18635),
(0, 3, 41, current_timestamp, 18635),
(0, 3, 42, current_timestamp, 18635),
(0, 3, 43, current_timestamp, 18635),
(0, 3, 44, current_timestamp, 18635),
(0, 3, 45, current_timestamp, 18635),

(0, 3, 92, current_timestamp, 18635),
(0, 3, 93, current_timestamp, 18635),
(0, 3, 94, current_timestamp, 18635),
(0, 3, 96, current_timestamp, 18635),
(0, 3, 97, current_timestamp, 18635),
(0, 3, 98, current_timestamp, 18635),
(0, 3, 100, current_timestamp, 18635),
(0, 3, 101, current_timestamp, 18635),
(0, 3, 103, current_timestamp, 18635),
(0, 3, 104, current_timestamp, 18635),
(0, 3, 105, current_timestamp, 18635),
(0, 3, 106, current_timestamp, 18635),
(0, 3, 107, current_timestamp, 18635),
(0, 3, 108, current_timestamp, 18635),
(0, 3, 109, current_timestamp, 18635),
(0, 3, 110, current_timestamp, 18635),
(0, 3, 111, current_timestamp, 18635),
(0, 3, 112, current_timestamp, 18635),
(0, 3, 113, current_timestamp, 18635),

-- (0, 3, 114, current_timestamp, 18635),  Uncomment for Vice Provost release
-- (0, 3, 115, current_timestamp, 18635)   Uncomment fot Vice Provost's Final Decision

(0, 3, 116, current_timestamp, 18635)
 ;

COMMIT;

SET AUTOCOMMIT=@OLD_AUTOCOMMIT;
SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
