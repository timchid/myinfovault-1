-- Create back-up table for document signature records to alter
CREATE TABLE DocumentSignatureBackup LIKE DocumentSignature;

-- Back-up said records
INSERT INTO DocumentSignatureBackup
SELECT s.*
FROM DocumentSignature s
LEFT JOIN Dossier d ON s.DossierID=d.DossierID
LEFT JOIN AcademicAction a ON d.AcademicActionID=a.ID
WHERE s.Signed
AND s.DocumentType=20
AND s.DocumentVersion=2
AND a.ActionTypeID=21;

-- Update to version three all signature records that are:
--  - Signed
--  - represent a Dean's Final Decision
--  - For new appointment actions; and
--  - version two
UPDATE DocumentSignature s
LEFT JOIN Dossier d ON s.DossierID=d.DossierID
LEFT JOIN AcademicAction a ON d.AcademicActionID=a.ID
SET s.DocumentVersion=3
WHERE s.Signed
AND s.DocumentType=20
AND s.DocumentVersion=2
AND a.ActionTypeID=21;