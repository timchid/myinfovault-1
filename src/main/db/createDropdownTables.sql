-- Author: Pradeep Haldiya
-- Created: 2011-12-16
-- Updated: 2012-01-03

-- Create the DropdownMaster and DropdownDetails
-- Dependencies: none

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

DROP TABLE IF EXISTS DropdownMaster;

CREATE TABLE DropdownMaster (
   ID INT primary key auto_increment,
   Description VARCHAR(500) NOT NULL,
   Active BOOLEAN DEFAULT false NOT NULL,
   InsertTimestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
   InsertUserID INT DEFAULT 0 NOT NULL,
   UpdateTimestamp TIMESTAMP NULL,
   UpdateUserID INT,
   index (ID)
)
engine = InnoDB
default character set utf8;

DROP TABLE IF EXISTS DropdownDetails;

CREATE TABLE DropdownDetails (
   ID int primary key auto_increment,
   DropdownDetailID INT NOT NULL,
   DropdownMasterID INT NOT NULL,
   DropdownTitle VARCHAR(500) NOT NULL,
   Description VARCHAR(500) NULL,
   DisplayOrder INT,
   Active BOOLEAN DEFAULT false NOT NULL,
   InsertTimestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
   InsertUserID INT DEFAULT 0 NOT NULL,
   UpdateTimestamp TIMESTAMP NULL,
   UpdateUserID INT,
   index (DropdownDetailID),
   index (DropdownMasterID)
)
engine = InnoDB
default character set utf8;



SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
