--  Author: Pradeep Haldiya
-- Created: 2011-11-22
-- Updated: 2012-01-03

-- Table and Data updates for v4.2

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

/* Just for verification */
-- Select ID,UserID,getUserSortNameByUserID(UserID)`UserName`,Length(Content) From AdditionalInformation where Length(Content) > 65536  Order By Length(Content) Desc;

ALTER TABLE `AdditionalInformation`
  DROP COLUMN `ColumnPercent`,
  DROP COLUMN `ColumnTwo`,
  DROP COLUMN `ColumnOne`,
  CHANGE COLUMN `Content` `Content` TEXT NULL DEFAULT NULL;

ALTER TABLE `Section` ADD COLUMN `SectionPrimaryKeyID` VARCHAR(500) AFTER `WhereClause`;

source functionMivFormatter.sql
source functionMivPatentDateFormatter.sql

source createPatentJurisdiction.sql
source insertPatentJurisdiction.sql

source createPatentStatus.sql
source insertPatentStatus.sql

source createPatentSummary.sql

source insertSection.sql
source insertDocumentSection.sql
source insertSystemSectionHeader.sql

source createDropdownTables.sql
source insertDropdownMaster.sql
source insertDropdownDetails.sql

source insertBiosketchSectionLimits.sql

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
