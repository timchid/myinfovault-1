--  Author: Pradeep K Haldiya
-- Created: 2012-07-02

-- Table and Data updates for v4.5

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

-- add format options to Extending Knowledge forms
source insertDropdownMaster.sql;
source insertDropdownDetails.sql;

-- correct the Creative Activities: Contributions to Jointly Created Works column name into UserFormatPattern table
Update UserFormatPattern Set FieldName='contributors' where DocumentID=52 And FieldName='contribution';

-- change the size of FieldName column to store more than one fields to be formated as same style
ALTER TABLE `UserFormatPattern` CHANGE COLUMN `FieldName` `FieldName` VARCHAR(255) NULL DEFAULT NULL;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

