
DROP TABLE IF EXISTS `GeoState`;
CREATE TABLE `GeoState` (
  `CountryCode` varchar(2) COMMENT 'ISO 3166 Country Code' NOT NULL,  
  `StateID` varchar(2) COMMENT 'FIPS 10-4 Region Code' NOT NULL,
  `Name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`CountryCode`,`StateID`)
) 
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE = utf8_general_ci;
