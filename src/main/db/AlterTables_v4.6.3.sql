-- run 4.6.2.2 alter table script (wasn't run on the 4.6.2.2 emergency patch)
source AlterTables_v4.6.2.2.sql;

-- to change title column length from 100 to 225 characters.
ALTER TABLE `Reviews` CHANGE COLUMN `Title` `Title` VARCHAR(255) NULL DEFAULT NULL  ;
ALTER TABLE `PublicationEvents` CHANGE COLUMN `Title` `Title` VARCHAR(255) NULL DEFAULT NULL  ;

-- remove depts that are not used.
source insertSystemDepartment.sql;

-- Canidate name is used in RAF document representations; must be preserved in case name is changed in whitepages
ALTER TABLE `RAF_Forms` ADD COLUMN `CandidateName` VARCHAR(255) NOT NULL;

-- Initially set RAFs to dossier candidate's user account preferred name
UPDATE RAF_Forms r
LEFT JOIN Dossier d ON r.DossierID=d.DossierID
LEFT JOIN UserAccount u ON d.UserID=u.UserID
SET r.CandidateName=u.PreferredName
WHERE r.CandidateName='';
