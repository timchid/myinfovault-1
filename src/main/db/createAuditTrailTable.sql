-- Author : Pradeep Kumar Haldiya
-- Created: 09/07/2011

DELIMITER $$

-- Create AuditTrail Table
DROP TABLE IF EXISTS `AuditTrail`$$
CREATE TABLE `AuditTrail` (
  `ID`				int(11)		NOT NULL AUTO_INCREMENT,
  `Actor`			varchar(100) DEFAULT NULL COMMENT 'PrincipalID of the User',
  `ActionType` 		INT(11) NOT NULL COMMENT 'Reffers to ActionType[ActionID]',
  `EntityType` 		INT(11) NOT NULL COMMENT 'Reffers to EntityType[EntityID]',
  `EntityObjectID`	varchar(100) NOT NULL COMMENT 'It contains UserID,DossierID,DocumentID etc.',
  `Description`		varchar(1000) DEFAULT NULL COMMENT 'Brief description about action performed on entity',
  `Remarks`			text DEFAULT NULL COMMENT 'Full description about action performed on entity',
  `ActionDate`		timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,  
  PRIMARY KEY (`ID`)
)
engine = InnoDB
default character set utf8$$

ALTER TABLE `AuditTrail`  
  ADD CONSTRAINT `fk_audittrail_actiontype`
  FOREIGN KEY (`ActionType` )
  REFERENCES `ActionType` (`ActionID` )
  ON DELETE RESTRICT
  ON UPDATE NO ACTION
, ADD INDEX `fk_audittrail_actiontype` (`ActionType` ASC)
, ADD CONSTRAINT `fk_audittrail_entitytype`
  FOREIGN KEY (`EntityType` )
  REFERENCES `EntityType` (`EntityID` )
  ON DELETE RESTRICT
  ON UPDATE NO ACTION
, ADD INDEX `fk_audittrail_entitytype` (`EntityType` ASC)$$

DELIMITER ;
