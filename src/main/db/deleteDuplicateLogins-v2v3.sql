-- Author: Stephen Paulsen
-- Date: 2009-10-05
-- Updated: 2009-10-05

-- Updates according to the "DuplicateLogins" spreadsheet attached to MIV-2745



-- Philip Schneider has 2 accounts, both with login='philip.schneider', 2nd account has no UserLogin or data
DELETE FROM CvMem WHERE Uid=10313;

-- Thoan Tran, 2 accounts with login='thoan.tran', UserLogin is for 10359 login='txtran'
DELETE FROM CvMem WHERE Uid=4185;
UPDATE CvMem SET Login='txtran' WHERE Uid=10359;
DELETE FROM UserLogin WHERE UserID IN (4185,10359);
INSERT INTO UserLogin (UserID,Login,InsertUser,InsertTimestamp) VALUES (10359,'txtran',18099,now()),(10359,'thoan.tran',18099,now());

-- James Liau, 2 accounts with login='james.liau', not in LDAP, no UserLogin entries
DELETE FROM CvMem WHERE Login='james.liau' AND Uid IN (10863,11018);

-- John Owings, 2 accounts with login='john.owings'
DELETE FROM CvMem WHERE Uid=10305;
UPDATE CvMem SET Login='jowings' WHERE Uid=11043;
DELETE FROM UserLogin WHERE UserID IN (10305,11043);
INSERT INTO UserLogin (UserID,Login,InsertUser,InsertTimestamp) VALUES (11043,'jowings',18099,now()),(11043,'john.owings',18099,now());


-- Emily Harris, 2 accounts with login='emily.harris', not in LDAP
-- delete 11138, remove login entries
DELETE FROM CvMem WHERE Uid=11138 AND Login='emily.harris';
DELETE FROM UserLogin WHERE UserID=11138;


-- Mark Flanagan, 2 accounts with=login 'mark.flanagan', not in LDAP, delete both
DELETE FROM CvMem WHERE Login='mark.flanagan' AND Uid in (10990,11618);
DELETE FROM UserLogin WHERE Login='mark.flanagan' AND UserID IN (10990,11618);

-- Jason Rasmussen, 2 accounts with login='jason.rasmussen', in LDAP but not People Search
-- 2 real uids are jason.rasmussen, jjrasmus
DELETE FROM CvMem WHERE Login='jason.rasmussen' AND Uid IN (10856,12233);
DELETE FROM UserLogin WHERE Login='jason.rasmussen' AND UserID IN (10856,12233);


-- Pauline Mysliwiec, 2 accounts with login='pauline.mysliwiec' - also has 'pamysliw' in UserLogin
-- Both have data; Older account is SOM/Internal Medicine, newer is SOM/Gastroenterology
-- UserLogin points to new account 12237
-- TODO!
-- for now, just going to "disable' so the login isn't duplicated, and remove from UserLogin
UPDATE CvMem SET Login='pauline.mysliwiec-diable1' WHERE Uid=11365;
UPDATE CvMem SET Login='pauline.mysliwiec-diable2' WHERE Uid=12237;
DELETE FROM UserLogin WHERE UserID=12237;


-- Mary Christopher, 2 accounts with login='mary.christopher'
-- 2 real uids are fzmarymc, mary.christopher - both are in UserLogin
DELETE FROM CvMem WHERE Uid=12899;
UPDATE CvMem SET Login='fzmarymc' WHERE Uid=13078 AND Login='mary.christopher';


-- peter.moore ... There are 2 Peter Moore accounts,
-- Peter G Moore,  8145 : SOM/ANESTHESIOLOGY & PAIN MEDICINE : peter.moore@ucdmc.ucdavis.edu : pgmoore
-- Peter F Moore, 13090 : VETMED/PATHOLOGY, MICROBIOLOGY AND IMMUNOLOGY : pfmoore@ucdavis.edu : pfmoore, peter.moore
-- UserLogin points only to 13090, the VetMed account
UPDATE CvMem SET Login='pgmoore', EmailAddress='peter.moore@ucdmc.ucdavis.edu' WHERE Uid=8145;
UPDATE CvMem SET Login='pfmoore', Fname='Peter', Lname='Moore' WHERE Uid=13090;
-- UserLogin for Peter F Moore in VetMed is correct; put Peter G Moore SOM in the table
INSERT INTO UserLogin (UserID,Login,InsertUser,InsertTimestamp) VALUES (8145,'pgmoore',18099,now());


-- Danny Enepekides, 2 accounts with login='danny.enepekides'
DELETE FROM CvMem WHERE Uid=13938;
DELETE FROM UserLogin WHERE UserID IN (12673,13938);

-- David Asmuth, 2 accounts with login='david.asmuth', also has 'dasmuth' in UserLogin
UPDATE CvMem SET Login='david.asmuth-disable1' WHERE Uid=14168;
UPDATE CvMem SET Login='dasmuth', EmailAddress='david.asmuth@ucdmc.ucdavis.edu' WHERE Uid=10519;
DELETE FROM UserLogin WHERE UserID=14168;

-- Alison Van Eenennaam, 2 accounts with login='alvane'
-- logged into 14884 at 10pm Aug 29 2009
DELETE FROM CvMem WHERE Uid=19611 AND Login='alvane';

-- Lili Scheiber (nee Castillo), 2 accounts with login='cabowabo', one is inactive
DELETE FROM CvMem WHERE Uid=16860 AND Login='cabowabo';


