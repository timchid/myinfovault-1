-- Author: Rick Hendricks
-- Created: 2011-12-01
-- Updated: 2012-08-08
-- Note: Replace 18635 with your internal system ID

-- The routing sequence rows define the workflow location routing sequence for the DelegationAuthority
-- and DossierActionType of a dossier. The routing sequence rows are built using the foreign key ID's
-- from the DelegationAuthority, DossierActionType and WorkflowLocation tables.
-- The current routing sequences are shown below:
--
-- reledelgated merit sequence = Candidate, Department, School, ReadyToArchive, Archive
-- reledelgated promotion sequence = Candidate, Department, School, ReadyToArchive, Archive
-- non-reledelgated merit sequence = Candidate, Department, School, ViceProvost, ReadyToArchive, Archive
-- non-reledelgated promotion sequence = Candidate, Department, School, ViceProvost, ReadyToArchive, Archive
-- reledelgatedfederation promotion (CRC Req) sequence = Candidate, Department, School, Academic Senate, School (CRC Complete), ReadyToArchive, Archive
-- reledelgatedfederation merit (CRC Req) sequence = Candidate, Department, School, Academic Senate, School (CRC Complete), ReadyToArchive, Archive
--
-- The following query may be used to check the routing sequence for a given delegation authority and
-- action type by replacing the values for the da.DelegationAuthority and at.ActionType as needed:
--
-- select wl.WorkflowNodeName, rs.RoutingSequence from WorkflowRoutingSequence rs,
--  DelegationAuthority da,
--  DossierActionType at,
--  WorkflowLocation wl where
--  da.DelegationAuthority='REDELEGATED_FEDERATION' and
--  at.ActionType='PROMOTION' and
-- da.ID = rs.DelegationAuthorityID and
-- at.ID = rs.ActionTypeID and
-- wl.ID = rs.WorkflowLocationID order by rs.RoutingSequence;


SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

TRUNCATE WorkflowRoutingSequence;

INSERT INTO WorkflowRoutingSequence (ID, DelegationAuthorityID, ActionTypeID, WorkflowLocationID, RoutingSequence, InsertUserID)
VALUES

-- reledelgated(1) merit(1) sequence = Candidate(1), Department(2), School(3), ReadyToArchive(5), Archive(6)

(0, 1, 1, 0, 0, 18099),
(0, 1, 1, 1, 1, 18635),
(0, 1, 1, 2, 2, 18635),
(0, 1, 1, 3, 3, 18635),
(0, 1, 1, 5, 4, 18635),
(0, 1, 1, 6, 5, 18635),

-- reledelgated(1) promotion(2) sequence = Candidate(1), Department(2), School(3), ReadyToArchive(5), Archive(6)

(0, 1, 2, 0, 0, 18099),
(0, 1, 2, 1, 1, 18635),
(0, 1, 2, 2, 2, 18635),
(0, 1, 2, 3, 3, 18635),
(0, 1, 2, 5, 4, 18635),
(0, 1, 2, 6, 5, 18635),

-- non-reledelgated(2) merit(1) sequence = Candidate(1), Department(2), School(3), ViceProvost(4), ReadyToArchive(5), Archive(6)

(0, 2, 1, 0, 0, 18099),
(0, 2, 1, 1, 1, 18635),
(0, 2, 1, 2, 2, 18635),
(0, 2, 1, 3, 3, 18635),
(0, 2, 1, 4, 4, 18635),
(0, 2, 1, 5, 5, 18635),
(0, 2, 1, 6, 6, 18635),

-- non-reledelgated(2) promotion(2) sequence = Candidate(1), Department(2), School(3), ViceProvost(4), ReadyToArchive(5), Archive(6)

(0, 2, 2, 0, 0, 18099),
(0, 2, 2, 1, 1, 18635),
(0, 2, 2, 2, 2, 18635),
(0, 2, 2, 3, 3, 18635),
(0, 2, 2, 4, 4, 18635),
(0, 2, 2, 5, 5, 18635),
(0, 2, 2, 6, 6, 18635),

-- reledelgatedfederation promotion sequence = Candidate(1), Department(2), School(3), Academic Senate(8), School (CRC Complete)(7), ReadyToArchive(5), Archive(6)

(0, 3, 2, 0, 0, 18099),
(0, 3, 2, 1, 1, 18635),
(0, 3, 2, 2, 2, 18635),
(0, 3, 2, 3, 3, 18635),
(0, 3, 2, 8, 4, 18635),
(0, 3, 2, 7, 5, 18635),
(0, 3, 2, 5, 6, 18635),
(0, 3, 2, 6, 7, 18635),

-- reledelgatedfederation merit sequence = Candidate(1), Department(2), School(3), Academic Senate(8), School (CRC Complete)(7), ReadyToArchive(5), Archive(6)

(0, 3, 1, 0, 0, 18099),
(0, 3, 1, 1, 1, 18635),
(0, 3, 1, 2, 2, 18635),
(0, 3, 1, 3, 3, 18635),
(0, 3, 1, 8, 4, 18635),
(0, 3, 1, 7, 5, 18635),
(0, 3, 1, 5, 6, 18635),
(0, 3, 1, 6, 7, 18635);

COMMIT;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
