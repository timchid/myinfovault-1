SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

USE `myinfovault`;

-- -----------------------------------------------------
-- Data for table `myinfovault`.`Fonts`
-- -----------------------------------------------------
truncate Fonts;
SET AUTOCOMMIT=0;
INSERT INTO `Fonts` (`ID`, `Name`, `Font`, `InsertUserID`)
VALUES
(1, 'Default', 'Helvetica', 18099),
(2, 'Helvetica', 'Helvetica', 18099),
(3, 'Arial', 'Bitstream Vera Sans', 18099),
(4, 'Times Roman', 'Times New Roman', 18099);

COMMIT;

-- -----------------------------------------------------
-- Data for table `myinfovault`.`SystemCitationFormat`
-- -----------------------------------------------------
truncate SystemCitationFormat;
SET AUTOCOMMIT=0;
INSERT INTO `SystemCitationFormat` (`ID`, `Name`, `Implementation`, `InsertUserID`)
VALUES
(1, 'Default', 'miv', 18099),
(2, 'MIV', 'miv', 18099),
(3, 'CV', 'cv', 18099),
(4, 'NIH', 'nih', 18099);

COMMIT;

-- -----------------------------------------------------
-- Data for table `myinfovault`.`BiosketchStyle`
-- -----------------------------------------------------
DELETE FROM `BiosketchStyle` WHERE ID<100;
SET AUTOCOMMIT=0;
INSERT INTO `BiosketchStyle` (`ID`, `UserID`, `StyleName`, `PageWidth`, `PageHeight`,`CitationFormatID`,`InsertUserID`)
VALUES
(1, 0, 'System Default', 612, 792, 1, 18099),
(2, 0, 'CV Default', 612, 792, 3, 18099),
(3, 0, 'NIH Default', 612, 792, 4, 18099),
(99, 0, 'delete me', 612, 792, 1, 18099);

DELETE FROM `BiosketchStyle` WHERE ID=99;       -- forces next auto_increment value to be == 100

COMMIT;

-- -----------------------------------------------------
-- Data for table `myinfovault`.`Biosketch`
-- -----------------------------------------------------
DELETE FROM `Biosketch` WHERE ID<100;
SET AUTOCOMMIT=0;
INSERT INTO `Biosketch` (`ID`, `UserID`, `Name`, `Title`, `BiosketchType`, `StyleID`, `InsertUserID`)
VALUES
(2, 0, 'CV_DEFAULT', '', 2, 0, 18099),
(3, 0, 'NIH_DEFAULT', '', 3, 0, 18099),
(99, 0, 'delete me', '', 1, 0, 18099);

DELETE FROM `Biosketch` WHERE ID=99;       -- forces next auto_increment value to be == 100

COMMIT;

-- -----------------------------------------------------
-- Data for table `myinfovault`.`BiosketchFormatLimits`
-- -----------------------------------------------------
truncate BiosketchFormatLimits;
SET AUTOCOMMIT=0;
INSERT INTO `BiosketchFormatLimits` (`ID`, `BiosketchType`, `PageWidthMin`, `PageWidthMax`, `PageHeightMin`, `PageHeightMax`, `MarginMin`, `MarginMax`, `HeaderSizeMin`, `HeaderSizeMax`, `HeaderIndentMin`, `HeaderIndentMax`, `BodySizeMin`, `BodySizeMax`, `BodyIndentMin`, `BodyIndentMax`, `InsertUserID`)
VALUES
(0, 2, 612, 612, 792, 792,  4, 72,  9, 22, 0, 72,  9, 16, 0, 72, 18635),
(0, 3, 612, 612, 792, 792, 36, 54, 12, 12, 0, 72, 11, 11, 0, 72, 18635);

COMMIT;

-- -----------------------------------------------------
-- Data for table `myinfovault`.`BiosketchSectionLimits`
-- -----------------------------------------------------
truncate BiosketchSectionLimits;
SET AUTOCOMMIT=0;

-- NIH sections

INSERT INTO `BiosketchSectionLimits` (`ID`, `BiosketchType`, `SectionID`, `SectionName`, `Availability`, `IsMainSection`, `InSelectList`, `MayHideHeader`, `InsertUserID`)
VALUES
(1, 3, 59,    'personal', 'MANDATORY', false, false, false, 19012),
(2, 3, 68,    'education', 'MANDATORY', true, true, false, 19012),
(3, 3, 10008, 'personalstatement-header', 'MANDATORY', false, false, false, 18635),
(4, 3, 10000, 'positions-header', 'MANDATORY', false, false, false, 19012),
(5, 3, 66,    'employment', 'MANDATORY', false, true, false, 19012),
(6, 3, 85,    'service-committee', 'MANDATORY', false, true, false, 19012),
(7, 3, 21,    'service-board', 'MANDATORY', false, true, false, 19012),
(8, 3, 53,    'honors', 'MANDATORY', false, true, false, 19012),
(9, 3, 10002, 'publications-header', 'OPTIONAL_ON', false, false, false, 19012),
(10, 3, 77,    'nih-publications', 'OPTIONAL_ON', true, false, false, 19012);



INSERT INTO `BiosketchSectionLimits` (`ID`, `BiosketchType`, `SectionID`, `SectionName`, `Availability`, `IsMainSection`, `InSelectList`, `MayHideHeader`, `DisplayInBiosketch`, `InsertUserID`)
VALUES
(11, 3, 78,   'abstracts', 'OPTIONAL_ON', false, true, false, 1, 19012),
(12, 3, 83,   'journals', 'OPTIONAL_ON', false, true, false, 2, 19012);

INSERT INTO `BiosketchSectionLimits` (`ID`, `BiosketchType`, `SectionID`, `SectionName`, `Availability`, `IsMainSection`, `InSelectList`, `MayHideHeader`, `InsertUserID`)
VALUES
(13, 3, 10001,'grants-header', 'OPTIONAL_ON', false, false, false, 19012),
(15, 3, 86,   'grants-ongoing-research', 'OPTIONAL_ON', false, true, false, 19012),
(16, 3, 87,   'grants-completed-research', 'OPTIONAL_ON', false, true, false, 19012),
(17, 3, 88,   'grants-other-research', 'OPTIONAL_ON', false, true, false, 19012);


-- CV sections
INSERT INTO `BiosketchSectionLimits` (`ID`, `BiosketchType`, `SectionID`, `SectionName`, `Availability`, `IsMainSection`, `InSelectList`, `MayHideHeader`, `DisplayInBiosketch`, `InsertUserID`)
VALUES
(18, 2, 59,   'personal', 'OPTIONAL_ON', true, true, true, 1000, 19012),
(19, 2, 60,   'personal-address', 'OPTIONAL_ON', false, false, true, 1000, 19012),
(20, 2, 61,   'personal-phone', 'OPTIONAL_ON', false, false, true, 1000, 19012),
(21, 2, 62,   'personal-email', 'OPTIONAL_ON', false, false, true, 1000, 19012),
(22, 2, 63,   'personal-link', 'OPTIONAL_ON', false, false, true, 1000, 19012);

INSERT INTO `BiosketchSectionLimits` (`ID`, `BiosketchType`, `SectionID`, `SectionName`, `Availability`, `IsMainSection`, `InSelectList`, `MayHideHeader`, `InsertUserID`)
VALUES
(23, 2, 64,   'personal-additional', 'OPTIONAL_ON', false, true, true, 19012),
(24, 2, 65,   'interest-area', 'OPTIONAL_ON', false, true, true, 19012),
(25, 2, 55,   'agstation-reports', 'OPTIONAL_ON', true, true, true, 19012),
(26, 2, 10006,'education-header', 'OPTIONAL_ON', true, false, true, 19012),
(27, 2, 68,   'education', 'OPTIONAL_ON', false, true, true, 19012),
(28, 2, 70,   'credential', 'OPTIONAL_ON', false, true, true, 19012),
(29, 2, 69,   'education-additional', 'OPTIONAL_ON', false, true, true, 19012),
(30, 2, 10007,'employment-header', 'OPTIONAL_ON', true, false, true, 19012),
(31, 2, 66,   'employment', 'OPTIONAL_ON', false, true, true, 19012),
(32, 2, 67,   'employment-additional', 'OPTIONAL_ON', false, true, true, 19012),
(33, 2, 10005, 'knowledge-header', 'OPTIONAL_ON', true, false, true, 19012),
(34, 2, 56,   'extending-knowledge-media', 'OPTIONAL_ON', false, true, true, 19012),
(35, 2, 57,   'extending-knowledge-gathering', 'OPTIONAL_ON', false, true, true, 19012),
(36, 2, 58,   'extending-knowledge-other', 'OPTIONAL_ON', false, true, true, 19012),
(37, 2, 10001, 'grants-header', 'OPTIONAL_ON', true, false, true, 19012),
(38, 2, 54,   'grants-active', 'OPTIONAL_ON', false, true, true, 19012),
(39, 2, 73,   'grants-pending', 'OPTIONAL_ON', false, true, true, 19012),
(40, 2, 74,   'grants-completed', 'OPTIONAL_ON', false, true, true, 19012),
(41, 2, 75,   'grants-notawarded', 'OPTIONAL_ON', false, true, true, 19012),
(42, 2, 76,   'grants-none', 'OPTIONAL_ON', false, true, true, 19012),
-- (41, 2, 10008, 'honors-header', 'OPTIONAL_ON', true, false, true, 19012),
(43, 2, 53,   'honors', 'OPTIONAL_ON', true, true, true, 19012),
(44, 2, 03,   'course-evaluations', 'OPTIONAL_ON', true, true, true, 19012),
(45, 2, 02,   'position-description', 'OPTIONAL_ON', true, true, true, 19012),
(46, 2, 10002, 'publications-header', 'OPTIONAL_ON', true, false, true, 19012),
(47, 2, 24,   'journals-published', 'OPTIONAL_ON', false, true, true, 19012),
(48, 2, 25,   'journals-in-press', 'OPTIONAL_ON', false, true, true, 19012),
(49, 2, 26,   'journals-submitted', 'OPTIONAL_ON', false, true, true, 19012),
(50, 2, 33,   'book-chapters-published', 'OPTIONAL_ON', false, true, true, 19012),
(51, 2, 34,   'book-chapters-in-press', 'OPTIONAL_ON', false, true, true, 19012),
(52, 2, 35,   'book-chapters-submitted', 'OPTIONAL_ON', false, true, true, 19012),
(53, 2, 42,   'editor-letters-published', 'OPTIONAL_ON', false, true, true, 19012),
(54, 2, 43,   'editor-letters-in-press', 'OPTIONAL_ON', false, true, true, 19012),
(55, 2, 44,   'editor-letters-submitted', 'OPTIONAL_ON', false, true, true, 19012),
(56, 2, 30,   'books-edited-published', 'OPTIONAL_ON', false, true, true, 19012),
(57, 2, 31,   'books-edited-in-press', 'OPTIONAL_ON', false, true, true, 19012),
(58, 2, 32,   'books-edited-submitted', 'OPTIONAL_ON', false, true, true, 19012),
(59, 2, 36,   'reviews-published', 'OPTIONAL_ON', false, true, true, 19012),
(60, 2, 37,   'reviews-in-press', 'OPTIONAL_ON', false, true, true, 19012),
(61, 2, 38,   'reviews-submitted', 'OPTIONAL_ON', false, true, true, 19012),
(62, 2, 27,   'books-authored-published', 'OPTIONAL_ON', false, true, true, 19012),
(63, 2, 28,   'books-authored-in-press', 'OPTIONAL_ON', false, true, true, 19012),
(64, 2, 29,   'books-authored-submitted', 'OPTIONAL_ON', false, true, true, 19012),
(65, 2, 45,   'limited-published', 'OPTIONAL_ON', false, true, true, 19012),
(66, 2, 46,   'limited-in-press', 'OPTIONAL_ON', false, true, true, 19012),
(67, 2, 47,   'limited-submitted', 'OPTIONAL_ON', false, true, true, 19012),
(68, 2, 39,   'media-published', 'OPTIONAL_ON', false, true, true, 19012),
(69, 2, 40,   'media-in-press', 'OPTIONAL_ON', false, true, true, 19012),
(70, 2, 41,   'media-submitted', 'OPTIONAL_ON', false, true, true, 19012),
(71, 2, 48,   'abstracts-published', 'OPTIONAL_ON', false, true, true, 19012),
(72, 2, 49,   'abstracts-in-press', 'OPTIONAL_ON', false, true, true, 19012),
(73, 2, 50,   'abstracts-submitted', 'OPTIONAL_ON', false, true, true, 19012),
(74, 2, 51,   'presentations', 'OPTIONAL_ON', false, true, true, 19012),
/*(98, 2, 92,   'patent', 'OPTIONAL_ON', false, true, true, 20720),
(99, 2, 93,   'disclosure', 'OPTIONAL_ON', false, true, true, 20720),*/
(75, 2, 52,   'publication-additional', 'OPTIONAL_ON', false, true, true, 19012),
(76, 2, 10004, 'service-header', 'OPTIONAL_ON', true, false, true, 19012),
(77, 2, 22,   'service-administrative', 'OPTIONAL_ON', false, true, true, 19012),
(78, 2, 15,   'service-department', 'OPTIONAL_ON', false, true, true, 19012),
(79, 2, 16,   'service-school', 'OPTIONAL_ON', false, true, true, 19012),
(80, 2, 17,   'service-campus', 'OPTIONAL_ON', false, true, true, 19012),
(81, 2, 18,   'service-systemwide', 'OPTIONAL_ON', false, true, true, 19012),
(82, 2, 19,   'service-other-university', 'OPTIONAL_ON', false, true, true, 19012),
(83, 2, 20,   'service-other-nonuniversity', 'OPTIONAL_ON', false, true, true, 19012),
(84, 2, 21,   'service-board', 'OPTIONAL_ON', false, true, true, 19012) ,
(85, 2, 23,   'service-additional', 'OPTIONAL_ON', false, true, true, 19012),
(86, 2, 10003, 'teaching-header', 'OPTIONAL_ON', true, false, true, 19012),
(87, 2, 10,   'teaching-contact-hours', 'OPTIONAL_ON', false, true, true, 19012),
(88, 2, 04,   'teaching-courses', 'OPTIONAL_ON', false, true, true, 19012),
(89, 2, 09,   'teaching-curriculum', 'OPTIONAL_ON', false, true, true, 19012),
(90, 2, 12,   'teaching-supplement-term', 'OPTIONAL_ON', false, true, true, 19012),
(91, 2, 13,   'teaching-supplement-month', 'OPTIONAL_ON', false, true, true, 19012),
(92, 2, 08,   'teaching-special-advising', 'OPTIONAL_ON', false, true, true, 19012),
(93, 2, 07,   'teaching-student-advising', 'OPTIONAL_ON', false, true, true, 19012),
(94, 2, 05,   'teaching-thesis', 'OPTIONAL_ON', false, true, true, 19012),
(95, 2, 11,   'teaching-trainees', 'OPTIONAL_ON', false, true, true, 19012),
(96, 2, 06,   'teaching-extension', 'OPTIONAL_ON', false, true, true, 19012),
(97, 2, 14,   'teaching-additional', 'OPTIONAL_ON', false, true, true, 19012);

COMMIT;

-- -----------------------------------------------------
-- Data for table `myinfovault`.`PaperSize`
-- -----------------------------------------------------
truncate PaperSize;
SET AUTOCOMMIT=0;
INSERT INTO `PaperSize` (`ID`, `Name`, `Description`, `Width`, `Height`, `InsertUserID`)
VALUES
(1, 'Letter', '8.5 x 11 in.', 612, 792, 18099),
(2, 'Legal', '8.5 x 14 in.', 612, 1008, 18099),
(3, 'A4', '210 x 290 mm', 595, 842, 18099);

COMMIT;

-- -----------------------------------------------------
-- Data for table `myinfovault`.`BiosketchAttributes`
-- -----------------------------------------------------
DELETE FROM `BiosketchAttributes` WHERE BiosketchID<100;
SET AUTOCOMMIT=0;
INSERT INTO `BiosketchAttributes` (`ID`, `BiosketchID`, `Name`, `Value`, `Sequence`, `Display`, `InsertUserID`)
VALUES
(0, 2, 'full_name', '', 45, true, 18099),
(0, 2, 'permanent_address', '', 50, true, 18099),
(0, 2, 'permanent_phone', '', 60, true, 18099),
(0, 2, 'permanent_fax', '', 70, true, 18099),
(0, 2, 'current_address', '', 80, true, 18099),
(0, 2, 'current_phone', '', 90, true, 18099),
(0, 2, 'current_fax', '', 100, true, 18099),
(0, 2, 'office_address', '', 110, true, 18099),
(0, 2, 'office_phone', '', 120, true, 18099),
(0, 2, 'office_fax', '', 130, true, 18099),
(0, 2, 'cell_phone', '', 140, true, 18099),
(0, 2, 'email', '', 150, true, 18099),
(0, 2, 'link', '', 160, true, 18099),
(0, 2, 'birthDate', '', 170, true, 18099),
(0, 2, 'citizen', '', 180, true, 18099),
(0, 2, 'date_entered', '', 190, true, 18099),
(0, 2, 'visa_type', '', 200, true, 18099),

(0, 3, 'programdirector_name', '', 5, true, 18635),
(0, 3, 'full_name', '', 10, true, 18099),
(0, 3, 'era_username', '', 20, true, 18099),
(0, 3, 'position_title1', '', 30, true, 18099),
(0, 3, 'position_title2', '', 40, true, 18099);

COMMIT;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
