-- Author: Rick Hendricks
-- Created: 2013-01-23
-- Updated: 2013-02-11 (SDP)


use myinfovault;

-- Rename the ReadyToArchive Location to ReadyForPostReviewAudit 
update Dossier set  WorkflowLocations='ReadyForPostReviewAudit' where WorkflowLocations='ReadyToArchive';

-- Table updates for addition of the Post Audit and Appeals locations.

source createWorkflowLocation.sql;
source insertWorkflowLocation.sql;

-- Change size of the DossierAttributeType Name column from 40 to 255
ALTER TABLE `DossierAttributeType` CHANGE COLUMN `Name` `Name` VARCHAR(255);

-- Change the size of the Filename and attribute names in the document table
ALTER TABLE `Document` CHANGE COLUMN `DossierFileName` `DossierFileName` VARCHAR(255);
ALTER TABLE `Document` CHANGE COLUMN `FileName` `FileName` VARCHAR(255);
ALTER TABLE `Document` CHANGE COLUMN `AttributeName` `AttributeName` VARCHAR(255);

source insertDossierAttributeLocation.sql;
source insertDossierAttributeType.sql;
source insertDossierRoutingPathDefinitions.sql;
source insertDelegationAuthorityAttributeCollection.sql;

source insertDocument.sql;
source insertDocumentSection.sql;
source insertSection.sql;
source insertSystemSectionHeader.sql;

source insertCollection.sql;
source insertCollectionDocument.sql;
source insertDossierView.sql;
source insertMivRole.sql;

-- Add index on Publication 'year' field to improve DSS view performance.
CREATE INDEX `IDX_PUB_YEAR` ON `PublicationSummary`(Year);

-- vp, primary/joint dean appeal decisions/recommendations
source createAppealDecision.sql;

-- MIV-4782 - Allow for differing text base on whether a Decision or recommendation is being signed.
ALTER TABLE `RAF_Forms` CHANGE COLUMN `Decision` `Decision` enum('APPROVED','DENIED','OTHER','RECOMMENDAPPROVAL','RECOMMENDDENIAL','RECOMMENDOTHER','NONE');
ALTER TABLE `RAF_FormDepartments` CHANGE COLUMN `Decision` `Decision` enum('APPROVED','DENIED','OTHER','RECOMMENDAPPROVAL','RECOMMENDDENIAL','RECOMMENDOTHER','NONE');
ALTER TABLE `AppealDecision` CHANGE COLUMN `Decision` `Decision` enum('APPROVED','DENIED','OTHER','RECOMMENDAPPROVAL','RECOMMENDDENIAL','RECOMMENDOTHER','NONE');

-- not needed as action type is available from the dossier
ALTER TABLE `DC_Forms` DROP COLUMN `ActionPromotion`;
ALTER TABLE `DC_Forms` DROP COLUMN `ActionMerit`;
