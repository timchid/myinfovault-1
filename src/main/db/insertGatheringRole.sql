# Author: Lawrence Fyfe
# Date: 01/22/2007

truncate GatheringRole;

insert into GatheringRole (ID,Name,InsertUserID)
values
(1,'Organizer',14021),
(2,'Presenter',14021),
(3,'Participant',14021),
(4,'Invited Speaker',14021),
(5,'Other',14021),
(6,'Keynote Speaker',14021);
