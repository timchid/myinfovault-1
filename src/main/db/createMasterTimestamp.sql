--  Author: Stephen Paulsen
-- Created: 2015-06-22
-- Updated: 2015-06-22

-- Create the table to maintain a timestamp for when each user's data was last touched.
-- Dependencies: UserAccoungt table must exist


SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';



CREATE TABLE IF NOT EXISTS `myinfovault`.`MasterTimestamp` (
    `UserID` INT NOT NULL,
    UpdateTimestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`UserID`),
    CONSTRAINT `fk_data_userid`
    FOREIGN KEY (`UserID`) REFERENCES `myinfovault`.`UserAccount`(`UserID`)
        ON UPDATE CASCADE
        ON DELETE CASCADE
    )
ENGINE = InnoDB default character set utf8;



-- Initialize records for everyone.
INSERT INTO MasterTimestamp (UserID) SELECT UserID FROM `myinfovault`.`UserAccount`;



SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
