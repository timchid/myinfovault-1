-- Author : Pradeep Kumar Haldiya
-- Created: 08/15/2011

DELIMITER $$

-- Procedure to delete Appointments by Id and UserId
-- @param vId
-- @param vUserId
DROP PROCEDURE IF EXISTS `PROC_DELETE_APPOINTMENT`$$
CREATE PROCEDURE `PROC_DELETE_APPOINTMENT`(in vId INT(11), in vUserId INT(11))
BEGIN
	/****************************************************************************/
	/* Declare Variables 												    	*/
	/****************************************************************************/
	DECLARE sqlScript VARCHAR(4000) DEFAULT null;

	-- just for log	
	SET sqlScript = CONCAT('DELETE FROM `Appointment` WHERE `ID`=',vId,' AND `UserID`=',vUserId);
	CALL log_sql(sqlScript);

	DELETE FROM `Appointment` WHERE `ID`=vId AND `UserID`=vUserId;
	
END$$

DELIMITER ;
