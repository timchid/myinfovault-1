-- Author: Pradeep Haldiya
-- Created: 2012-08-13
-- Updated: 2014-06-30
-- View to get all publication records with resolved ids with their corresponding names.

DROP VIEW IF EXISTS `DssPublication`;
CREATE VIEW `DssPublication` AS
SELECT
	CONCAT(u.Surname, ', ', u.GivenName) AS Fullname,
	u.Surname,
	u.GivenName,
	u.PersonUUID,
	d.Description AS Department,
        d.DepartmentID AS DepartmentID,
	s.Description AS School,
        s.SchoolID AS SchoolID,

	t.Description AS Type,
	ps.Description AS Status,
	-- lpad(if(trim(p.Year+0)<1900,1899,p.Year+0),4,'0') as Year,
	p.Year,
	p.Author,
	p.Title,
	p.Editor,
	p.Journal,
	p.Volume,
	p.Issue,
	p.Pages,
	p.Publisher,
	p.City,
	p.ISBN,
	p.Link,
	p.Contribution,
	p.Significance,

	CONVERT((CASE
		WHEN aa.PrimaryAppointment = true THEN 'Primary'
		WHEN aa.PrimaryAppointment = false THEN 'Joint'
		ELSE 'None' END) USING utf8) COLLATE utf8_unicode_ci as AppointmentType,
	u.Active AS UserActive

FROM myinfovault.PublicationSummary p

  LEFT JOIN myinfovault.UserAccount u ON p.UserID=u.UserID
  LEFT JOIN myinfovault.Appointment aa ON aa.DepartmentID IS NOT NULL AND aa.SchoolID IS NOT NULL AND aa.UserID=u.UserID
  LEFT JOIN myinfovault.SystemDepartment d ON d.DepartmentID=ifnull(aa.DepartmentID,u.DepartmentID) AND d.SchoolID=ifnull(aa.SchoolID,u.SchoolID)
  LEFT JOIN myinfovault.SystemSchool s ON s.Demo=false AND s.SchoolID=ifnull(aa.SchoolID,u.SchoolID)

  LEFT JOIN myinfovault.PublicationType t ON p.TypeID=t.ID
  LEFT JOIN myinfovault.PublicationStatus ps ON p.StatusID=ps.ID

WHERE u.PersonUUID is not null;
