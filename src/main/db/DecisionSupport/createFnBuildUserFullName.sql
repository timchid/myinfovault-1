-- Author: Pradeep Haldiya
-- Created: 2014-02-21
-- create function to build user full name for GivenName, MiddleName and Surname

DELIMITER $$

-- Function to build User Full Name
DROP FUNCTION IF EXISTS `buildUserFullName`$$
CREATE FUNCTION `buildUserFullName`(vGivenName VARCHAR(50), vMiddleName VARCHAR(50), vSurname VARCHAR(50)) RETURNS varchar(500) CHARSET utf8
BEGIN
	DECLARE userName VARCHAR(500) DEFAULT NULL;

	IF vSurname IS NOT NULL and vSurname <> '' THEN
		SET userName = concat(vSurname,', ');
	END IF;

	IF vGivenName IS NOT NULL and vGivenName <> '' THEN
		SET userName = concat(userName,vGivenName);
	END IF;

	IF vMiddleName IS NOT NULL and vMiddleName <> '' THEN
		SET userName = concat(userName,' ',vMiddleName);
	END IF;

	RETURN userName; 
END$$


DELIMITER ;
