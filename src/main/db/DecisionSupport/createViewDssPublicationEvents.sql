-- Author: Pradeep Haldiya
-- Created: 2014-02-20
-- Updated: 2014-06-27
-- List of creative activities publication events 

DROP VIEW IF EXISTS `DssPublicationEvents`;
CREATE VIEW `DssPublicationEvents` AS 
SELECT 
	e.ID, 

	CONCAT(u.Surname, ', ', u.GivenName) AS Fullname,
	u.Surname,
	u.GivenName,
	u.PersonUUID,
	d.Description AS Department,
        d.DepartmentID AS DepartmentID,
	s.Description AS School,
        s.SchoolID AS SchoolID,

	e.Year, 
	e.StatusID, 
	ps.Description AS `StatusDescription`, 
	e.Title, 
	e.Publication, 
	e.Publisher,
	e.Creators,
	e.Editors,
	e.Volume,
	e.Issue,
	e.Pages,
	e.Country, 
	e.Province,
	e.City,
	e.URL, 

	CONVERT((CASE
		WHEN aa.PrimaryAppointment = true THEN 'Primary'
		WHEN aa.PrimaryAppointment = false THEN 'Joint'
		ELSE 'None' END) USING utf8) COLLATE utf8_unicode_ci as AppointmentType,
	u.Active AS UserActive

FROM `myinfovault`.`PublicationEvents` AS e

  LEFT JOIN myinfovault.UserAccount u ON e.UserID=u.UserID
  LEFT JOIN myinfovault.Appointment aa ON aa.DepartmentID IS NOT NULL AND aa.SchoolID IS NOT NULL AND aa.UserID=u.UserID
  LEFT JOIN myinfovault.SystemDepartment d ON d.DepartmentID=ifnull(aa.DepartmentID,u.DepartmentID) AND d.SchoolID=ifnull(aa.SchoolID,u.SchoolID)
  LEFT JOIN myinfovault.SystemSchool s ON s.Demo=false AND s.SchoolID=ifnull(aa.SchoolID,u.SchoolID)

  LEFT JOIN `myinfovault`.`PublicationStatus` ps ON e.StatusID=ps.ID

WHERE u.PersonUUID is not null;
