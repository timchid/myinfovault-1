-- Author: Pradeep Haldiya
-- Created: 2014-02-20
-- create views for creative activities reports

source createBuildUserFullName.sql;
source createRoutineDssWorks.sql;
source createRoutineDssEvents.sql;
source createRoutineDssPublicationEvents.sql;
source createRoutineDssReviews.sql;
source createRoutineDssCreativeActivitiesAssociations.sql;
source createRoutineDssCreativeActivitiesMasterTables.sql;
source createRoutineDssUserAccount.sql;
