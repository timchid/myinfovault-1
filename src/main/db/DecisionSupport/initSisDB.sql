-- Author: Stephen Paulsen
-- Created: 2014-06-25
-- Updated:

-- 
-- Create the Decision Support schema 'sisdb' and populate it with
-- all of the Views and routines needed for DSS reporting.
-- 


-- Create the Schema...
source createDecisionSupportDataBase.sql

-- ...and switch to it for the rest of the work.
USE sisdb;



-- (create general stuff)
source createFnBuildUserFullName.sql

source createViewDssUserAccount.sql

source createViewDssSchool.sql

source createViewDssDepartment.sql

source createViewDssAccessRights.sql



-- (next do Publications stuff)
source createViewDssPublication.sql
source createViewDssPublicationYear.sql



-- (then do Creative Activities stuff)

source createDssCreativeActivitiesMasterTables.sql

-- DDL_CreativeActivities.sql
	source createViewDssWorks.sql
	source createViewDssEvents.sql
	source createViewDssPublicationEvents.sql
	source createViewDssReviews.sql

source createDssAssociationViews.sql



-- (last, set up the privs)
source createUserGrants.sql

/*
 vim:se ts=8 sw=8 sr noet:
*/
