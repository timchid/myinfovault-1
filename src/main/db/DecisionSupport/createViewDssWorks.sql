-- Author: Pradeep Haldiya
-- Created: 2014-02-19
-- Updated: 2014-06-25
-- List of creative activities works

DROP VIEW IF EXISTS `DssWorks`;
CREATE VIEW `DssWorks` AS
SELECT
	w.ID,

	CONCAT(u.Surname, ', ', u.GivenName) AS Fullname,
	u.Surname,
	u.GivenName,
	u.PersonUUID,
	d.Description AS Department,
        d.DepartmentID AS DepartmentID,
	s.Description AS School,
        s.SchoolID AS SchoolID,

	wt.Description AS `Type`,
		w.WorkTypeID,
		wt.Description AS `WorkTypeDescription`,
	ws.Status,
		w.StatusID,
		ws.Status AS `StatusDescription`,

	w.WorkYear AS Year,
		w.WorkYear,
	w.Creators,
	w.Title,
	w.Description,
	w.URL,
	w.Contributors,

	CONVERT((CASE
		WHEN aa.PrimaryAppointment = true THEN 'Primary'
		WHEN aa.PrimaryAppointment = false THEN 'Joint'
		ELSE 'None' END) USING utf8) COLLATE utf8_unicode_ci as AppointmentType,
	u.Active AS UserActive

FROM `myinfovault`.`Works` AS w

  LEFT JOIN myinfovault.UserAccount u ON w.UserID=u.UserID
  LEFT JOIN myinfovault.Appointment aa ON aa.DepartmentID IS NOT NULL AND aa.SchoolID IS NOT NULL AND aa.UserID=u.UserID
  LEFT JOIN myinfovault.SystemDepartment d ON d.DepartmentID=ifnull(aa.DepartmentID,u.DepartmentID) AND d.SchoolID=ifnull(aa.SchoolID,u.SchoolID)
  LEFT JOIN myinfovault.SystemSchool s ON s.Demo=false AND s.SchoolID=ifnull(aa.SchoolID,u.SchoolID)

  LEFT JOIN `myinfovault`.`WorkType` wt ON w.WorkTypeID=wt.ID
  LEFT JOIN `myinfovault`.`WorkStatus` ws ON w.StatusID=ws.ID

WHERE u.PersonUUID is not null;
