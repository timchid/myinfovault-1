# Author: Venkat
# Date: 11/26/2007

create table CommitteeSummary
(
    ID int primary key auto_increment COMMENT 'Record unique identifier',
    UserID int COMMENT 'ID corresponding to the the user associated with this record', -- TODO: ref UserAccount
    TypeID int COMMENT 'ID corresponding to the committee/group type', -- TODO: ref CommitteeType
    Year varchar(20) COMMENT 'Year range of the committee/group',
    Description text COMMENT 'Description of the committee/group',
    Role varchar(255) COMMENT 'Committee role associated with the user for this record',
    Sequence int COMMENT 'Relative order in which this record should appear when listed',
    Display boolean NOT NULL default true COMMENT 'Should the record be displayed?',
    InsertTimestamp timestamp default current_timestamp COMMENT 'The date-time the record was inserted',
    InsertUserID int COMMENT 'ID corresponding to the real user that inserted the record',
    UpdateTimestamp datetime COMMENT 'The date-time the record was last updated',
    UpdateUserID int COMMENT 'ID corresponding to the real user that last updated the record',
    index (UserID)
)
engine = InnoDB
default character set utf8
COMMENT 'Service: Committees';
