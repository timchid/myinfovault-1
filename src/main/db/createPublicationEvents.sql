-- Author: Pradeep Haldiya
-- Created: 2012-04-12


-- Create the Publication Events Table
-- Dependencies: The PublicationStatus tables must exist.

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

DROP TABLE IF EXISTS PublicationEvents;

create table PublicationEvents
(
   ID int auto_increment,
   UserID int NOT NULL,
   Year int NOT NULL,
   StatusID int NOT NULL,
   Title varchar (255) NULL,
   Publication varchar (255) NULL,
   Publisher varchar (255) NULL,
   Creators text NOT NULL COMMENT 'Publication author or authors', -- max is 5524
   Editors varchar(255) COMMENT 'Publication editor or editors',
   Volume varchar(20) NULL,
   Issue varchar (20) NULL,
   Pages varchar (50) NULL,
   Country varchar(3) NOT NULL DEFAULT '',
   Province varchar(30) NOT NULL DEFAULT '',
   City varchar(50) NOT NULL DEFAULT '',
   URL text NULL,
   Sequence int,
   Display boolean NOT NULL default true, 
   InsertTimestamp timestamp DEFAULT CURRENT_TIMESTAMP,
   InsertUserID int NOT NULL,
   UpdateTimestamp timestamp NULL,
   UpdateUserID int NULL,
   PRIMARY KEY (`ID`),
   INDEX(`UserID`),
   CONSTRAINT `fk_publicationeventstatusid` FOREIGN KEY (`StatusID`) REFERENCES `PublicationStatus`(`ID`) ON DELETE NO ACTION ON UPDATE CASCADE
)ENGINE=INNODB 
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
