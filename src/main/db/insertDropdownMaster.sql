-- Author: Pradeep Haldiya
-- Created: 2011-12-16
-- Updated: 2012-01-03

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

SELECT count(*) FROM DropdownMaster INTO @_ORIG_COUNT;

truncate DropdownMaster;

INSERT INTO DropdownMaster (ID, Description, Active, InsertUserId)
VALUES
(1, 'Patent Types', true, 20720),
(2, 'Manage Format Options Data Category', true, 20720),

(6, 'Publications', true, 20720),
(52, 'Creative Activities', true, 20720),
(10, 'Extending Knowledge', true, 20720);

SHOW WARNINGS;
commit;

SELECT count(*) FROM DropdownMaster INTO @_NEW_COUNT;
SELECT @_ORIG_COUNT AS 'Original Count', @_NEW_COUNT AS 'New Count';

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
