# Author: Lawrence Fyfe
# Date: 10/09/2006

truncate ServiceType;

insert into ServiceType (ID,Description,InsertUserID)
values
(1,'Committee',14021),
(2,'Editorial or Advisory Board',14021),
(3,'Administrative Activity',14021);
