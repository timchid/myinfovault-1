-- Author : Pradeep Kumar Haldiya
-- Created: 09/19/2011
-- Description: Drop all tables
-- 'Drop scripts for appointments to roles conversion. (MIV-3956)'  

-- Drop logger table and _temp tables
drop table if exists `myinfovault`.`logger`;
drop table if exists `myinfovault`.`roleassignment_temp`;
drop table if exists `myinfovault`.`appointment_temp`;
