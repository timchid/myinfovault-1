-- Author: Pradeep K Haldiya
-- Created: 2013-04-01
-- Updated: 2013-04-17(Rick Hendricks)

use myinfovault;

source insertDocument.sql
source insertCollection.sql
source insertCollectionDocument.sql
source insertDelegationAuthorityAttributeCollection.sql
source insertDocumentSection.sql
source insertDossierAttributeLocation.sql
source insertDossierAttributeType.sql
source insertDossierView.sql
source insertMivRole.sql;

source createDossierActionType.sql;
source insertDossierActionType.sql;
source createRoutingPathDefinitions.sql;
source insertRoutingPathDefinitions.sql;
source insertSystemDepartment.sql;


-- MIV-4735
-- Adding document representation version column
-- All signature records default to version zero (invalid, null version)
-- However, all currently signed signature records are version one
ALTER TABLE `DocumentSignature` ADD COLUMN `DocumentVersion` INT NOT NULL DEFAULT 0 AFTER `UserID`;
UPDATE `DocumentSignature` SET DocumentVersion=1 WHERE Signed=true;
-- update signature table views
DROP VIEW IF EXISTS sigs;
CREATE VIEW sigs AS
  SELECT ID, Signed, UserId, DocumentId AS DocId, DocumentType AS DocType, DocumentVersion AS DocRepVer, SchoolID AS SchId, DepartmentID AS DeptId, SignTime,
  left(hex(DocumentHash),64) as Signature, DigestType AS Digest,
  CompatibleVersion AS Compat, InsertUserID, InsertTimestamp, UpdateUserID AS Signer, UpdateTimestamp
FROM DocumentSignature;
DROP VIEW IF EXISTS sigtable;
CREATE VIEW sigtable AS
  SELECT
        ID,
        InsertUserID AS Requestor,
        UserId AS RequestedSigner,
        UpdateUserID AS Signer,
        Signed,
        SignTime,
        DocumentType AS DocType,
        DocumentId AS DocId,
        DocumentVersion AS DocRepVer,
        SchoolID AS SchId,
        DepartmentID AS DeptId,
        left(hex(DocumentHash),16) as Signature,
        DigestType AS Digest,
        CompatibleVersion AS Compat,
        InsertTimestamp,
        UpdateTimestamp
 FROM DocumentSignature
 ORDER BY ID ASC;

-- no need to delete columns they may used in signature computation
-- ALTER TABLE `DC_Forms` DROP COLUMN `CandidateName`, DROP COLUMN `SchoolName`, DROP COLUMN `DepartmentName`, DROP COLUMN `ActionOther` , DROP COLUMN `ActionDeferral` , DROP COLUMN `ActionAcceleration` , DROP COLUMN `ActionAppraisal` , DROP COLUMN `EnteredDate`;
ALTER TABLE `DC_Forms`
CHANGE COLUMN `ActionAppraisal` `ActionAppraisal` BOOLEAN NOT NULL DEFAULT 0 ,
CHANGE COLUMN `ActionAcceleration` `ActionAcceleration` BOOLEAN NOT NULL DEFAULT 0,
CHANGE COLUMN `ActionDeferral` `ActionDeferral` BOOLEAN NOT NULL DEFAULT 0,
CHANGE COLUMN `ActionOther` `ActionOther` BOOLEAN NOT NULL DEFAULT 0;

ALTER TABLE `DC_Ranks` ADD COLUMN `PresentPercentOfTime` VARCHAR(3) NULL DEFAULT ''  AFTER `PresentRankAndStep` , ADD COLUMN `ProposedPercentOfTime` VARCHAR(3) NULL DEFAULT ''  AFTER `ProposedRankAndStep` ;

-- MIV-4833, new action types
ALTER TABLE `Dossier` CHANGE COLUMN `ActionType`
`ActionType` ENUM ('APPOINTMENT_ENDOWED_CHAIR',
                   'APPOINTMENT_ENDOWED_PROFESSORSHIP',
                   'APPOINTMENT_ENDOWED_SPECIALIST',
                   'APPOINTMENT_INITIAL_CONTINUING',
                   'APPOINTMENT_CHANGE_DEPARTMENT',
                   'APPOINTMENT_CHANGE_TITLE',
                   'APPRAISAL',
                   'DEFERRAL_FIRST_YEAR',
                   'DEFERRAL_SECOND_YEAR',
                   'DEFERRAL_THIRD_YEAR',
                   'DEFERRAL_FOURTH_YEAR',
                   'EMERITUS_STATUS',
                   'FIVE_YEAR_REVIEW',
                   'FIVE_YEAR_REVIEW_DEPARTMENT_CHAIR',
                   'MERIT',
                   'PROMOTION',
                   'REAPPOINTMENT',
                   'REAPPOINTMENT_ENDOWED_CHAIR',
                   'REAPPOINTMENT_ENDOWED_PROFESSORSHIP',
                   'REAPPOINTMENT_ENDOWED_SPECIALIST') NOT null;

-- MIV-4833; Widen the Description columns to accommodate the longer action type names
ALTER TABLE `Dossier` CHANGE COLUMN `Description` `Description` VARCHAR(100) NOT NULL;
ALTER TABLE `DossierSnapshot` CHANGE COLUMN `Description` `Description` VARCHAR(100) NOT NULL;

-- MIV-4835; recommendation fields will be maintained in database, but removed from inserts and update statements; providing defaults
ALTER TABLE `RAF_Forms`
CHANGE COLUMN `RecommendationDept` `RecommendationDept` boolean not null default true,
CHANGE COLUMN `RecommendationCSD` `RecommendationCSD` enum('YES','NO','NA') not null default 'NA',
CHANGE COLUMN `RecommendationJAS` `RecommendationJAS` enum('YES','NO','NA') not null default 'NA',
CHANGE COLUMN `RecommendationAFP` `RecommendationAFP` enum('YES','NO','NA') not null default 'NA';


-- Dropping cvlevel and devteam from user account
ALTER TABLE `UserAccount` DROP COLUMN `CvLevel`;
ALTER TABLE `UserAccount` DROP COLUMN `DevTeam`;
DROP VIEW IF EXISTS `myinfovault`.`accounts`;
CREATE  VIEW `myinfovault`.`accounts` AS
 SELECT
    UserID, Login, PersonUUID AS UUID, SchoolID AS School, DepartmentID AS Dept,
    Surname AS Surname, GivenName AS Given, MiddleName AS Middle, PreferredName AS Preferred,
    Email, Active AS Actv
 FROM UserAccount;

-- Give Maureen Stanton the VICE_PROVOST role
INSERT INTO RoleAssignment (UserID, RoleID, PrimaryRole, InsertUserID) VALUES (17748, 10, false, 18099);
