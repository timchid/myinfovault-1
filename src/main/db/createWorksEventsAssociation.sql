-- Author: Rick Hendricks
-- Created: 2012-02-03
-- Updated: 2012-05-23

-- Create the WorksEventsAssociation Table
-- Dependencies: The Works and Events tables must exist.

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

DROP TABLE IF EXISTS WorksEventsAssociation;

create table WorksEventsAssociation
(
   UserID int NOT NULL,
   WorkID int NOT NULL,
   EventID int NOT NULL,
   InsertTimestamp timestamp DEFAULT CURRENT_TIMESTAMP,
   InsertUserID int NOT NULL,
   UpdateTimestamp timestamp NULL,
   UpdateUserID int NULL,
   PRIMARY KEY (`WorkID`,`EventID`),
   INDEX(`WorkID`,`EventID`),
   CONSTRAINT  `fk_we_workid` FOREIGN KEY (`WorkID`) REFERENCES `Works`(`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
   CONSTRAINT `fk_we_eventid` FOREIGN KEY (`EventID`) REFERENCES `Events`(`ID`) ON DELETE CASCADE ON UPDATE CASCADE
)ENGINE=INNODB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
