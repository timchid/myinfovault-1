-- Author: Stephen Paulsen
-- Created: 07/15/2009
-- Updated: 07/15/2009

-- Create the Person-to-Appointment table
-- Dependencies: UserAccount, SystemSchool, SystemDepartment

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

USE `myinfovault`;

DROP TABLE IF EXISTS `myinfovault`.`Appointment`;

CREATE TABLE `myinfovault`.`Appointment` (
    `ID` INT NOT NULL AUTO_INCREMENT,
    `UserID` INT NOT NULL,
    `SchoolID` INT NOT NULL,
    `DepartmentID` INT NOT NULL,
    `PrimaryAppointment` BOOLEAN NOT NULL DEFAULT false COMMENT 'true if this is the primary appointment',
    `Sequence` INT NOT NULL DEFAULT 0,
    `InsertTimestamp` timestamp default current_timestamp,
    `InsertUserID` int,
    `UpdateTimestamp` datetime,
    `UpdateUserID` int,
    PRIMARY KEY (`ID`),
    INDEX(`UserID`),
    CONSTRAINT
      FOREIGN KEY (`SchoolID`)
      REFERENCES `myinfovault`.`SystemSchool` (`SchoolID`)
      ON DELETE NO ACTION
      ON UPDATE CASCADE,
    CONSTRAINT
      FOREIGN KEY (`DepartmentID`)
      REFERENCES `myinfovault`.`SystemDepartment` (`DepartmentID`)
      ON DELETE NO ACTION
      ON UPDATE CASCADE
)
engine = InnoDB
default character set utf8;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
