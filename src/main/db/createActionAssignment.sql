-- Author: Pradeep Kumar Haldiya
-- Created: 2013-09-13

-- Create the main table ActionAssignment
-- Dependencies: The AcademicAction table must exist.

DROP TABLE IF EXISTS ActionAssignment;

CREATE TABLE ActionAssignment
(
   ID INT PRIMARY KEY auto_increment,
   AcademicActionID INT NOT NULL,
   SchoolID INT NOT NULL,
   SchoolName VARCHAR(225) NOT NULL,
   DepartmentID INT NOT NULL,
   DepartmentName VARCHAR(225) NOT NULL,
   Status ENUM ('PRESENT','PROPOSED') default NULL,
   PrimaryAssignment BOOLEAN,
   PercentageOfTime DECIMAL(4,3) NOT NULL,
   InsertTimestamp TIMESTAMP DEFAULT current_timestamp,
   InsertUserID INT NOT NULL,
   UpdateTimestamp datetime,
   UpdateUserID INT,
   KEY `fk_actionassignment_academic_action` (`AcademicActionID`),
   CONSTRAINT `fk_actionassignment_academic_action` FOREIGN KEY (`AcademicActionID`) 
   REFERENCES `AcademicAction` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
)
AUTO_INCREMENT=500000
engine = InnoDB
default character set utf8;
