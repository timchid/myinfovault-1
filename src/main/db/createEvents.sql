-- Author: Rick Hendricks
-- Created: 2012-02-03
-- Updated: 2012-02-03

-- Create the Events Table
-- Dependencies: The EventType and EventCategory tables must exist.

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

DROP TABLE IF EXISTS Events;

create table Events
(
   ID int auto_increment,
   UserID int NOT NULL,
   EventTypeID int NOT NULL,
   EventStartDate date,
   EventEndDate date,
   StatusID int NOT NULL,
   Venue varchar (255) NULL,
   VenueDescription text NULL,
   EventDescription text NULL,
   Country varchar(3) NOT NULL DEFAULT '',
   Province varchar(30) NOT NULL DEFAULT '',
   City varchar(50) NOT NULL DEFAULT '',
   URL text NULL,
   Sequence int,
   Display boolean NOT NULL default true, 
   InsertTimestamp timestamp DEFAULT CURRENT_TIMESTAMP,
   InsertUserID int NOT NULL,
   UpdateTimestamp timestamp NULL,
   UpdateUserID int NULL,
   PRIMARY KEY (`ID`),
   INDEX(`UserID`),
   CONSTRAINT `fk_eventtypeid` FOREIGN KEY (`EventTypeID`) REFERENCES `EventType`(`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
   CONSTRAINT `fk_eventstatusid` FOREIGN KEY (`StatusID`) REFERENCES `EventStatus`(`ID`) ON DELETE NO ACTION ON UPDATE CASCADE
)ENGINE=INNODB 
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
