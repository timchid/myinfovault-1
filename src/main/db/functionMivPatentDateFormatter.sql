-- Author : Pradeep Kumar Haldiya
-- Created: 2012-01-09
-- Updated: 2012-01-11

DELIMITER $$

-- Function to format patent dates
DROP FUNCTION IF EXISTS `mivPatentDateFormatter`$$
CREATE FUNCTION `mivPatentDateFormatter`(vDate VARCHAR(10)) RETURNS varchar(12)
BEGIN
	DECLARE lResult VARCHAR(12) DEFAULT null;
	
  	IF vDate IS NOT NULL THEN	
	    SET lResult = DATE_FORMAT(STR_TO_DATE(vDate,'%Y-%m-%d'),'%b %e, %Y');	    
  	END IF;
	RETURN lResult;
END$$

DELIMITER ;

