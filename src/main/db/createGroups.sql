-- Author: Rick Hendricks
-- Date: 08/05/2011
-- This table defines groups.

-- Create the Groups table
-- Dependencies: GroupType, UserAccount 

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

drop table if exists `Groups`;

create table `Groups`
(
   `ID` int not null auto_increment,
   `Name` varchar(25) NOT null,
   `Description` varchar(100),
   `TypeID` int not null,
   `CreatorPersonUUID` varchar(100) not null,
   `ReadOnly` boolean null default false,
   `Private` boolean null default false,
   `Confidential` boolean null default false,
   `Active` boolean null default true,
   `InsertTimestamp` timestamp default current_timestamp,
   `InsertUserID` int,
   `UpdateTimestamp` datetime,
   `UpdateUserID` int,
    PRIMARY KEY (`ID`),
    INDEX(`Name`),
    CONSTRAINT
      FOREIGN KEY (`TypeID`)
      REFERENCES GroupType(`ID`)
      ON DELETE NO ACTION
      ON UPDATE NO ACTION,
    CONSTRAINT FOREIGN KEY (`CreatorPersonUUID`)
      REFERENCES UserAccount(`PersonUUID`)
      ON DELETE NO ACTION
      ON UPDATE CASCADE
)
engine = InnoDB
default character set utf8;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

