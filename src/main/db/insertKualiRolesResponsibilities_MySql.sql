-- note: for role population, search for FILL ME IN USER!!! and replace with proper MIV user ids ; for data population, search for FILL ME IN DATA!!! and replace with proper MIV school codes, department codes, etc.

-- start fresh
delete from krim_role_mbr_attr_data_t where attr_data_id like 'MIV%'
;
delete from krim_role_mbr_t where role_mbr_id like 'MIV%'
;
delete from krim_role_rsp_actn_t where role_rsp_actn_id like 'MIV%'
;
delete from krim_role_rsp_t where role_rsp_id like 'MIV%'
;
delete from krim_role_rsp_t where role_rsp_id in ('10001','20001','30001','40001', '50001')
;
delete from krim_role_t where role_id like 'MIV%'
;
delete from krim_rsp_attr_data_t where attr_data_id like 'MIV%'
;
delete from krim_rsp_t where rsp_id like 'MIV%'
;
delete from krim_rsp_t where rsp_id in ('10001','20001','30001','40001','50001')
;
delete from krim_attr_defn_t where kim_attr_defn_id like 'MIV%'
;
delete from krim_typ_attr_t where kim_typ_attr_id like 'MIV%'
;
delete from krim_typ_t where kim_typ_id  like 'MIV%'
;
delete from krns_nmspc_t where nmspc_cd = 'UCD-MIV'
;

-- add namespace *
INSERT INTO KRNS_NMSPC_T(NMSPC_CD, OBJ_ID, VER_NBR, NM, ACTV_IND) 
    VALUES('UCD-MIV', UUID(), 1, 'MyInfoVault', 'Y')
;

-- add custom attribute definitions for school and department
INSERT INTO KRIM_ATTR_DEFN_T(KIM_ATTR_DEFN_ID, OBJ_ID, VER_NBR, NM, ACTV_IND, LBL, NMSPC_CD, CMPNT_NM, APPL_URL) 
    VALUES('MIV-SCH', UUID(), 1, 'schoolCode', 'Y', 'School Code', 'UCD-MIV', null, null)
;
INSERT INTO KRIM_ATTR_DEFN_T(KIM_ATTR_DEFN_ID, OBJ_ID, VER_NBR, NM, ACTV_IND, LBL, NMSPC_CD, CMPNT_NM, APPL_URL) 
    VALUES('MIV-DEPT', UUID(), 1, 'departmentCode', 'Y', 'Department Code', 'UCD-MIV', null, null)
;

-- add department role type
INSERT INTO KRIM_TYP_T(KIM_TYP_ID, OBJ_ID, VER_NBR, NM, SRVC_NM, ACTV_IND, NMSPC_CD) 
    VALUES('MIV-DEPT', UUID(), 1, 'Department', 'departmentRoleTypeService', 'Y', 'UCD-MIV')
;
INSERT INTO KRIM_TYP_ATTR_T(KIM_TYP_ATTR_ID, OBJ_ID, VER_NBR, KIM_TYP_ID, KIM_ATTR_DEFN_ID, ACTV_IND, SORT_CD) 
    VALUES('MIV-DEPT-SCH', UUID(), 1, 'MIV-DEPT', 'MIV-SCH', 'Y', 'a')
;
INSERT INTO KRIM_TYP_ATTR_T(KIM_TYP_ATTR_ID, OBJ_ID, VER_NBR, KIM_TYP_ID, KIM_ATTR_DEFN_ID, ACTV_IND, SORT_CD) 
    VALUES('MIV-DEPT', UUID(), 1, 'MIV-DEPT', 'MIV-DEPT', 'Y', 'b')
;

-- add department assistant role type
INSERT INTO KRIM_TYP_T(KIM_TYP_ID, OBJ_ID, VER_NBR, NM, SRVC_NM, ACTV_IND, NMSPC_CD) 
    VALUES('MIV-DEPT-ASST', UUID(), 1, 'Department Assistant', 'departmentRoleTypeService', 'Y', 'UCD-MIV')
;


-- add school role type
INSERT INTO KRIM_TYP_T(KIM_TYP_ID, OBJ_ID, VER_NBR, NM, SRVC_NM, ACTV_IND, NMSPC_CD) 
    VALUES('MIV-SCH', UUID(), 1, 'School', 'schoolRoleTypeService', 'Y', 'UCD-MIV')
;
INSERT INTO KRIM_TYP_ATTR_T(KIM_TYP_ATTR_ID, OBJ_ID, VER_NBR, KIM_TYP_ID, KIM_ATTR_DEFN_ID, ACTV_IND, SORT_CD) 
    VALUES('MIV-SCH', UUID(), 1, 'MIV-SCH', 'MIV-SCH', 'Y', 'a')
;


-- add dossier role type (42 is delivered attribute definition for document number - could define your own new attribute for dosssier id instead)
INSERT INTO KRIM_TYP_T(KIM_TYP_ID, OBJ_ID, VER_NBR, NM, SRVC_NM, ACTV_IND, NMSPC_CD) 
    VALUES('MIV-DOSSIER', UUID(), 1, 'Derived Role: Dossier', 'dossierRoleTypeService', 'Y', 'UCD-MIV')
;
INSERT INTO KRIM_TYP_ATTR_T(KIM_TYP_ATTR_ID, OBJ_ID, VER_NBR, KIM_TYP_ID, KIM_ATTR_DEFN_ID, ACTV_IND, SORT_CD) 
    VALUES('MIV-DOSSIER', UUID(), 1, 'MIV-DOSSIER', '42', 'Y', 'a')
;

-- add ; populate candidate role of default type
INSERT INTO KRIM_ROLE_T(ROLE_ID, OBJ_ID, VER_NBR, ROLE_NM, NMSPC_CD, DESC_TXT, KIM_TYP_ID, ACTV_IND, LAST_UPDT_DT) 
    VALUES('MIV-CAND', UUID(), 1, 'Candidate', 'UCD-MIV', null, '1', 'Y', NOW())
;
--INSERT INTO KRIM_ROLE_MBR_T(ROLE_MBR_ID, VER_NBR, OBJ_ID, ROLE_ID, MBR_ID, MBR_TYP_CD, ACTV_FRM_DT, ACTV_TO_DT, LAST_UPDT_DT) 
--    VALUES('MIV-CANDIDATE-18635', 1, UUID(), 'MIV-CANDIDATE', '18635', 'P', null, null, NOW())
--;

--INSERT INTO KRIM_ROLE_MBR_T(ROLE_MBR_ID, VER_NBR, OBJ_ID, ROLE_ID, MBR_ID, MBR_TYP_CD, ACTV_FRM_DT, ACTV_TO_DT, LAST_UPDT_DT) 
--    VALUES('MIV-CANDIDATE-18099', 1, UUID(), 'MIV-CANDIDATE', '18099', 'P', null, null, NOW())
--;

--INSERT INTO KRIM_ROLE_MBR_T(ROLE_MBR_ID, VER_NBR, OBJ_ID, ROLE_ID, MBR_ID, MBR_TYP_CD, ACTV_FRM_DT, ACTV_TO_DT, LAST_UPDT_DT) 
--    VALUES('MIV-CANDIDATE-19012', 1, UUID(), 'MIV-CANDIDATE', '19012', 'P', null, null, NOW())
--;


-- add ; populate MIV-DEPARTMENT role of MIV-DEPARTMENT type
INSERT INTO KRIM_ROLE_T(ROLE_ID, OBJ_ID, VER_NBR, ROLE_NM, NMSPC_CD, DESC_TXT, KIM_TYP_ID, ACTV_IND, LAST_UPDT_DT) 
    VALUES('MIV-DEPT', UUID(), 1, 'Department Administrator', 'UCD-MIV', null, 'MIV-DEPT', 'Y', NOW())
;

-- add ; populate MIV-DEPARTMENT-ASSISTANT role of MIV-DEPARTMENT-ASSISTANT type
INSERT INTO KRIM_ROLE_T(ROLE_ID, OBJ_ID, VER_NBR, ROLE_NM, NMSPC_CD, DESC_TXT, KIM_TYP_ID, ACTV_IND, LAST_UPDT_DT) 
    VALUES('MIV-DEPT-ASST', UUID(), 1, 'Department Assistant', 'UCD-MIV', null, 'MIV-DEPT-ASST', 'Y', NOW())
;



-- ***** add one for each userid who is a member member of this  role
--INSERT INTO KRIM_ROLE_MBR_T(ROLE_MBR_ID, VER_NBR, OBJ_ID, ROLE_ID, MBR_ID, MBR_TYP_CD, ACTV_FRM_DT, ACTV_TO_DT, LAST_UPDT_DT) 
--    VALUES('MIV-DEPARTMENT-1423', 1, UUID(), 'MIV-DEPARTMENT', '1423', 'P', null, null, NOW())
--;

--INSERT INTO KRIM_ROLE_MBR_ATTR_DATA_T(ATTR_DATA_ID, OBJ_ID, VER_NBR, RSP_ID, KIM_TYP_ID, KIM_ATTR_DEFN_ID, ATTR_VAL) 
--    VALUES('MIV-DEPARTMENT-1423-SCHOOL', UUID(), 1, 'MIV-DEPARTMENT-1423', 'MIV-DEPARTMENT', 'MIV-SCHOOL', '1')-
--;

-- ***** add one for each departmentid
--INSERT INTO KRIM_ROLE_MBR_ATTR_DATA_T(ATTR_DATA_ID, OBJ_ID, VER_NBR, RSP_ID, KIM_TYP_ID, KIM_ATTR_DEFN_ID, ATTR_VAL) 
--    VALUES('MIV-DEPARTMENT-1423-DEPARTMENT', UUID(), 1, 'MIV-DEPARTMENT-1423', 'MIV-DEPARTMENT', 'MIV-DEPARTMENT', '244')
--;




-- add MIV-DOSSIER-DEPARTMENT role of MIV-DOSSIER type - not populating because MIV screens will populate
INSERT INTO KRIM_ROLE_T(ROLE_ID, OBJ_ID, VER_NBR, ROLE_NM, NMSPC_CD, DESC_TXT, KIM_TYP_ID, ACTV_IND, LAST_UPDT_DT) 
    VALUES('MIV-DOSSIER-DEPARTMENT', UUID(), 1, 'Department Dossier Reviewer', 'UCD-MIV', null, 'MIV-DOSSIER', 'Y', NOW())
;





-- add ; populate MIV-SCHOOL role of MIV-SCHOOL type
INSERT INTO KRIM_ROLE_T(ROLE_ID, OBJ_ID, VER_NBR, ROLE_NM, NMSPC_CD, DESC_TXT, KIM_TYP_ID, ACTV_IND, LAST_UPDT_DT) 
    VALUES('MIV-SCH', UUID(), 1, 'School Administrator', 'UCD-MIV', null, 'MIV-SCH', 'Y', NOW())
;

-- ***** add one for each userid who is a member member of this  role
--INSERT INTO KRIM_ROLE_MBR_T(ROLE_MBR_ID, VER_NBR, OBJ_ID, ROLE_ID, MBR_ID, MBR_TYP_CD, ACTV_FRM_DT, ACTV_TO_DT, LAST_UPDT_DT) 
--    VALUES('MIV-SCHOOL-1800', 1, UUID(), 'MIV-SCHOOL', '1800', 'P', null, null, NOW())
--;

-- ***** add one for each schoolid
--INSERT INTO KRIM_ROLE_MBR_ATTR_DATA_T(ATTR_DATA_ID, OBJ_ID, VER_NBR, RSP_ID, KIM_TYP_ID, KIM_ATTR_DEFN_ID, ATTR_VAL) 
--    VALUES('MIV-SCHOOL-1800-SCHOOL', UUID(), 1, 'MIV-SCHOOL-1800', 'MIV-SCHOOL', 'MIV-SCHOOL', '1')
--;

-- add MIV-DOSSIER-SCHOOL role of MIV-DOSSIER type - not populating because MIV screens will populate
INSERT INTO KRIM_ROLE_T(ROLE_ID, OBJ_ID, VER_NBR, ROLE_NM, NMSPC_CD, DESC_TXT, KIM_TYP_ID, ACTV_IND, LAST_UPDT_DT) 
    VALUES('MIV-DOSSIER-SCHOOL', UUID(), 1, 'School Dossier Reviewer', 'UCD-MIV', null, 'MIV-DOSSIER', 'Y', NOW())
;



-- add ; populate vice provost of default type
INSERT INTO KRIM_ROLE_T(ROLE_ID, OBJ_ID, VER_NBR, ROLE_NM, NMSPC_CD, DESC_TXT, KIM_TYP_ID, ACTV_IND, LAST_UPDT_DT) 
    VALUES('MIV-VP', UUID(), 1, 'Vice Provost', 'UCD-MIV', null, '1', 'Y', NOW())
;


-- add ; miv archive of default type
INSERT INTO KRIM_ROLE_T(ROLE_ID, OBJ_ID, VER_NBR, ROLE_NM, NMSPC_CD, DESC_TXT, KIM_TYP_ID, ACTV_IND, LAST_UPDT_DT) 
    VALUES('MIV-ARCH', UUID(), 1, 'MIV Archive', 'UCD-MIV', null, '1', 'Y', NOW())
;



-- ***** add one for each userid who is a member member of this  role
--INSERT INTO KRIM_ROLE_MBR_T(ROLE_MBR_ID, VER_NBR, OBJ_ID, ROLE_ID, MBR_ID, MBR_TYP_CD, ACTV_FRM_DT, ACTV_TO_DT, LAST_UPDT_DT) 
--    VALUES('MIV-VICEPROVOST-12848', 1, UUID(), 'MIV-VICEPROVOST', '12848', 'P', null, null, NOW())
--;

--INSERT INTO KRIM_ROLE_MBR_T(ROLE_MBR_ID, VER_NBR, OBJ_ID, ROLE_ID, MBR_ID, MBR_TYP_CD, ACTV_FRM_DT, ACTV_TO_DT, LAST_UPDT_DT) 
 --   VALUES('MIV-VICEPROVOST-18185', 1, UUID(), 'MIV-VICEPROVOST', '18185', 'P', null, null, NOW())
--;

--INSERT INTO KRIM_ROLE_MBR_T(ROLE_MBR_ID, VER_NBR, OBJ_ID, ROLE_ID, MBR_ID, MBR_TYP_CD, ACTV_FRM_DT, ACTV_TO_DT, LAST_UPDT_DT) 
--    VALUES('MIV-VICEPROVOST-18099', 1, UUID(), 'MIV-VICEPROVOST', '18099', 'P', null, null, NOW())
--;


-- add ; populate administrator role of default type
INSERT INTO KRIM_ROLE_T(ROLE_ID, OBJ_ID, VER_NBR, ROLE_NM, NMSPC_CD, DESC_TXT, KIM_TYP_ID, ACTV_IND, LAST_UPDT_DT) 
    VALUES('MIV-ADMIN', UUID(), 1, 'MyInfoVault Administrator', 'UCD-MIV', null, '1', 'Y', NOW())
;
--INSERT INTO KRIM_ROLE_MBR_T(ROLE_MBR_ID, VER_NBR, OBJ_ID, ROLE_ID, MBR_ID, MBR_TYP_CD, ACTV_FRM_DT, ACTV_TO_DT, LAST_UPDT_DT) 
--    VALUES('MIV-ADMINISTRATOR-18635', 1, UUID(), 'MIV-ADMINISTRATOR', '18635', 'P', null, null, NOW())
--;

--INSERT INTO KRIM_ROLE_MBR_T(ROLE_MBR_ID, VER_NBR, OBJ_ID, ROLE_ID, MBR_ID, MBR_TYP_CD, ACTV_FRM_DT, ACTV_TO_DT, LAST_UPDT_DT) 
--    VALUES('MIV-ADMINISTRATOR-18099', 1, UUID(), 'MIV-ADMINISTRATOR', '18099', 'P', null, null, NOW())
--;
--INSERT INTO KRIM_ROLE_MBR_T(ROLE_MBR_ID, VER_NBR, OBJ_ID, ROLE_ID, MBR_ID, MBR_TYP_CD, ACTV_FRM_DT, ACTV_TO_DT, LAST_UPDT_DT) 
--    VALUES('MIV-ADMINISTRATOR-18185', 1, UUID(), 'MIV-ADMINISTRATOR', '18185', 'P', null, null, NOW())
--;
--INSERT INTO KRIM_ROLE_MBR_T(ROLE_MBR_ID, VER_NBR, OBJ_ID, ROLE_ID, MBR_ID, MBR_TYP_CD, ACTV_FRM_DT, ACTV_TO_DT, LAST_UPDT_DT) 
--    VALUES('MIV-ADMINISTRATOR-19012', 1, UUID(), 'MIV-ADMINISTRATOR', '19012', 'P', null, null, NOW())
--;

-- give administrator role the KR-SYS Technical Administrator role (to solve the type of access issues you're getting now
INSERT INTO KRIM_ROLE_MBR_T(ROLE_MBR_ID, VER_NBR, OBJ_ID, ROLE_ID, MBR_ID, MBR_TYP_CD, ACTV_FRM_DT, ACTV_TO_DT, LAST_UPDT_DT) 
    VALUES('MIV-RICE-ADMIN-M1', 1, UUID(), '63', 'MIV-ADMIN', 'R', null, null, NOW())
;

-- note: taking advantage of delivered responsibility templates ; types ; attributes - should have no need to define your own

-- add exception routing responsibility for DossierDocuments to send to administrator role
INSERT INTO KRIM_RSP_T(RSP_ID, OBJ_ID, VER_NBR, RSP_TMPL_ID, NM, DESC_TXT, ACTV_IND, NMSPC_CD) 
    VALUES('10001', UUID(), 1, '2', null, null, 'Y', 'UCD-MIV')
;
INSERT INTO KRIM_RSP_ATTR_DATA_T(ATTR_DATA_ID, OBJ_ID, VER_NBR, RSP_ID, KIM_TYP_ID, KIM_ATTR_DEFN_ID, ATTR_VAL) 
    VALUES('MIV-EXCEPTION', UUID(), 1, '10001', '54', '13', 'DossierDocument')
;
INSERT INTO KRIM_ROLE_RSP_T(ROLE_RSP_ID, OBJ_ID, VER_NBR, ROLE_ID, RSP_ID, ACTV_IND) 
    VALUES('10001', UUID(), 1, 'MIV-ADMIN', '10001', 'Y')
;
INSERT INTO KRIM_ROLE_RSP_ACTN_T(ROLE_RSP_ACTN_ID, OBJ_ID, VER_NBR, ACTN_TYP_CD, PRIORITY_NBR, ACTN_PLCY_CD, ROLE_MBR_ID, ROLE_RSP_ID, FRC_ACTN) 
    VALUES('MIV-EXCEPTION', UUID(), 1, 'A', null, 'F', '*', '10001', 'Y')
;



-- add workflow to department role
INSERT INTO KRIM_RSP_T(RSP_ID, OBJ_ID, VER_NBR, RSP_TMPL_ID, NM, DESC_TXT, ACTV_IND, NMSPC_CD) 
    VALUES('20001', UUID(), 1, '1', null, null, 'Y', 'UCD-MIV')
;
INSERT INTO KRIM_RSP_ATTR_DATA_T(ATTR_DATA_ID, OBJ_ID, VER_NBR, RSP_ID, KIM_TYP_ID, KIM_ATTR_DEFN_ID, ATTR_VAL) 
    VALUES('MIV-DEPT-DOCTYPE', UUID(), 1, '20001', '7', '13', 'DossierDocument')
;
INSERT INTO KRIM_RSP_ATTR_DATA_T(ATTR_DATA_ID, OBJ_ID, VER_NBR, RSP_ID, KIM_TYP_ID, KIM_ATTR_DEFN_ID, ATTR_VAL) 
    VALUES('MIV-DEPT-NODE', UUID(), 1, '20001', '7', '16', 'Department')
;
INSERT INTO KRIM_RSP_ATTR_DATA_T(ATTR_DATA_ID, OBJ_ID, VER_NBR, RSP_ID, KIM_TYP_ID, KIM_ATTR_DEFN_ID, ATTR_VAL) 
    VALUES('MIV-DEPT-REQRD', UUID(), 1, '20001', '7', '40', 'true')
;
INSERT INTO KRIM_RSP_ATTR_DATA_T(ATTR_DATA_ID, OBJ_ID, VER_NBR, RSP_ID, KIM_TYP_ID, KIM_ATTR_DEFN_ID, ATTR_VAL) 
    VALUES('MIV-DEPT-ADARML', UUID(), 1, '20001', '7', '41', 'false')
;
INSERT INTO KRIM_ROLE_RSP_T(ROLE_RSP_ID, OBJ_ID, VER_NBR, ROLE_ID, RSP_ID, ACTV_IND) 
    VALUES('20001', UUID(), 1, 'MIV-DEPT', '20001', 'Y')
;
INSERT INTO KRIM_ROLE_RSP_ACTN_T(ROLE_RSP_ACTN_ID, OBJ_ID, VER_NBR, ACTN_TYP_CD, PRIORITY_NBR, ACTN_PLCY_CD, ROLE_MBR_ID, ROLE_RSP_ID, FRC_ACTN) 
    VALUES('MIV-DEPT', UUID(), 1, 'A', null, 'F', '*', '20001', 'Y')
;

-- there is no workflow for the department assistant role


-- add workflow to school role
INSERT INTO KRIM_RSP_T(RSP_ID, OBJ_ID, VER_NBR, RSP_TMPL_ID, NM, DESC_TXT, ACTV_IND, NMSPC_CD) 
    VALUES('30001', UUID(), 1, '1', null, null, 'Y', 'UCD-MIV')
;
INSERT INTO KRIM_RSP_ATTR_DATA_T(ATTR_DATA_ID, OBJ_ID, VER_NBR, RSP_ID, KIM_TYP_ID, KIM_ATTR_DEFN_ID, ATTR_VAL) 
    VALUES('MIV-SCH-DOCTYPE', UUID(), 1, '30001', '7', '13', 'DossierDocument')
;
INSERT INTO KRIM_RSP_ATTR_DATA_T(ATTR_DATA_ID, OBJ_ID, VER_NBR, RSP_ID, KIM_TYP_ID, KIM_ATTR_DEFN_ID, ATTR_VAL) 
    VALUES('MIV-SCH-NODE', UUID(), 1, '30001', '7', '16', 'School')
;
INSERT INTO KRIM_RSP_ATTR_DATA_T(ATTR_DATA_ID, OBJ_ID, VER_NBR, RSP_ID, KIM_TYP_ID, KIM_ATTR_DEFN_ID, ATTR_VAL) 
    VALUES('MIV-SCH-REQRD', UUID(), 1, '30001', '7', '40', 'true')
;
INSERT INTO KRIM_RSP_ATTR_DATA_T(ATTR_DATA_ID, OBJ_ID, VER_NBR, RSP_ID, KIM_TYP_ID, KIM_ATTR_DEFN_ID, ATTR_VAL) 
    VALUES('MIV-SCH-ADARML', UUID(), 1, '30001', '7', '41', 'false')
;
INSERT INTO KRIM_ROLE_RSP_T(ROLE_RSP_ID, OBJ_ID, VER_NBR, ROLE_ID, RSP_ID, ACTV_IND) 
    VALUES('30001', UUID(), 1, 'MIV-SCH', '30001', 'Y')
;
INSERT INTO KRIM_ROLE_RSP_ACTN_T(ROLE_RSP_ACTN_ID, OBJ_ID, VER_NBR, ACTN_TYP_CD, PRIORITY_NBR, ACTN_PLCY_CD, ROLE_MBR_ID, ROLE_RSP_ID, FRC_ACTN) 
    VALUES('MIV-SCH', UUID(), 1, 'A', null, 'F', '*', '30001', 'Y')
;

-- add workflow to vice provost role
INSERT INTO KRIM_RSP_T(RSP_ID, OBJ_ID, VER_NBR, RSP_TMPL_ID, NM, DESC_TXT, ACTV_IND, NMSPC_CD) 
    VALUES('40001', UUID(), 1, '1', null, null, 'Y', 'UCD-MIV')
;
INSERT INTO KRIM_RSP_ATTR_DATA_T(ATTR_DATA_ID, OBJ_ID, VER_NBR, RSP_ID, KIM_TYP_ID, KIM_ATTR_DEFN_ID, ATTR_VAL) 
    VALUES('MIV-VP-DOCTYPE', UUID(), 1, '40001', '7', '13', 'DossierDocument')
;
INSERT INTO KRIM_RSP_ATTR_DATA_T(ATTR_DATA_ID, OBJ_ID, VER_NBR, RSP_ID, KIM_TYP_ID, KIM_ATTR_DEFN_ID, ATTR_VAL) 
    VALUES('MIV-VP-NODE', UUID(), 1, '40001', '7', '16', 'ViceProvost')
;
INSERT INTO KRIM_RSP_ATTR_DATA_T(ATTR_DATA_ID, OBJ_ID, VER_NBR, RSP_ID, KIM_TYP_ID, KIM_ATTR_DEFN_ID, ATTR_VAL) 
    VALUES('MIV-VP-REQRD', UUID(), 1, '40001', '7', '40', 'true')
;
INSERT INTO KRIM_RSP_ATTR_DATA_T(ATTR_DATA_ID, OBJ_ID, VER_NBR, RSP_ID, KIM_TYP_ID, KIM_ATTR_DEFN_ID, ATTR_VAL) 
    VALUES('MIV-VP-ADARML', UUID(), 1, '40001', '7', '41', 'false')
;
INSERT INTO KRIM_ROLE_RSP_T(ROLE_RSP_ID, OBJ_ID, VER_NBR, ROLE_ID, RSP_ID, ACTV_IND) 
    VALUES('40001', UUID(), 1, 'MIV-VP', '40001', 'Y')
;
INSERT INTO KRIM_ROLE_RSP_ACTN_T(ROLE_RSP_ACTN_ID, OBJ_ID, VER_NBR, ACTN_TYP_CD, PRIORITY_NBR, ACTN_PLCY_CD, ROLE_MBR_ID, ROLE_RSP_ID, FRC_ACTN) 
    VALUES('MIV-VP', UUID(), 1, 'A', null, 'F', '*', '40001', 'Y')
;

-- add workflow to miv archive role
INSERT INTO KRIM_RSP_T(RSP_ID, OBJ_ID, VER_NBR, RSP_TMPL_ID, NM, DESC_TXT, ACTV_IND, NMSPC_CD) 
    VALUES('50001', UUID(), 1, '1', null, null, 'Y', 'UCD-MIV')
;
INSERT INTO KRIM_RSP_ATTR_DATA_T(ATTR_DATA_ID, OBJ_ID, VER_NBR, RSP_ID, KIM_TYP_ID, KIM_ATTR_DEFN_ID, ATTR_VAL) 
    VALUES('MIV-ARCH-DOCTYPE', UUID(), 1, '50001', '7', '13', 'DossierDocument')
;
INSERT INTO KRIM_RSP_ATTR_DATA_T(ATTR_DATA_ID, OBJ_ID, VER_NBR, RSP_ID, KIM_TYP_ID, KIM_ATTR_DEFN_ID, ATTR_VAL) 
    VALUES('MIV-ARCH-NODE', UUID(), 1, '50001', '7', '16', 'ReadyToArchive')
;
INSERT INTO KRIM_RSP_ATTR_DATA_T(ATTR_DATA_ID, OBJ_ID, VER_NBR, RSP_ID, KIM_TYP_ID, KIM_ATTR_DEFN_ID, ATTR_VAL) 
    VALUES('MIV-ARCH-REQRD', UUID(), 1, '50001', '7', '40', 'true')
;
INSERT INTO KRIM_RSP_ATTR_DATA_T(ATTR_DATA_ID, OBJ_ID, VER_NBR, RSP_ID, KIM_TYP_ID, KIM_ATTR_DEFN_ID, ATTR_VAL) 
    VALUES('MIV-ARCH-ADARML', UUID(), 1, '50001', '7', '41', 'false')
;
INSERT INTO KRIM_ROLE_RSP_T(ROLE_RSP_ID, OBJ_ID, VER_NBR, ROLE_ID, RSP_ID, ACTV_IND) 
    VALUES('50001', UUID(), 1, 'MIV-ARCH', '50001', 'Y')
;
INSERT INTO KRIM_ROLE_RSP_ACTN_T(ROLE_RSP_ACTN_ID, OBJ_ID, VER_NBR, ACTN_TYP_CD, PRIORITY_NBR, ACTN_PLCY_CD, ROLE_MBR_ID, ROLE_RSP_ID, FRC_ACTN) 
    VALUES('MIV-ARCH', UUID(), 1, 'A', null, 'F', '*', '50001', 'Y')
;

-- note: no responsibilities for department and school dossier reviewer roles, because those will be handled by ad hoc routing

-- Assign Technical Administrator role
INSERT INTO KRICEDEV.KRIM_ROLE_MBR_T
( ROLE_MBR_ID
, VER_NBR
, OBJ_ID
, ROLE_ID
, MBR_ID
, MBR_TYP_CD
, ACTV_FRM_DT
, ACTV_TO_DT
, LAST_UPDT_DT)
VALUES
( 'MIV-TECH-ADMIN-rhendric'
, 1
, UUID()
, (select ROLE_ID from KRICEDEV.KRIM_ROLE_T where ROLE_NM = 'Technical Administrator')
, 'rhendric'
, 'P'
, null
, null
, NOW());

-- Assign Rice role
INSERT INTO kricedev.KRIM_ROLE_MBR_T
( ROLE_MBR_ID
, VER_NBR
, OBJ_ID
, ROLE_ID
, MBR_ID
, MBR_TYP_CD
, ACTV_FRM_DT
, ACTV_TO_DT
, LAST_UPDT_DT)
VALUES
( 'MIV-RICE-ADMIN-rhendric'
, 1
, UUID()
, (select ROLE_ID from kricedev.KRIM_ROLE_T where ROLE_NM = 'Rice')
, 'rhendric'
, 'P'
, null
, null
, NOW());

INSERT INTO KRICEDEV.KRIM_ROLE_MBR_T
( ROLE_MBR_ID
, VER_NBR
, OBJ_ID
, ROLE_ID
, MBR_ID
, MBR_TYP_CD
, ACTV_FRM_DT
, ACTV_TO_DT
, LAST_UPDT_DT)
VALUES
( 'MIV-TECH-ADMIN-mnorthup'
, 1
, UUID()
, (select ROLE_ID from KRICEDEV.KRIM_ROLE_T where ROLE_NM = 'Technical Administrator')
, 'mnorthup'
, 'P'
, null
, null
, NOW());

-- Assign Rice role
INSERT INTO kricedev.KRIM_ROLE_MBR_T
( ROLE_MBR_ID
, VER_NBR
, OBJ_ID
, ROLE_ID
, MBR_ID
, MBR_TYP_CD
, ACTV_FRM_DT
, ACTV_TO_DT
, LAST_UPDT_DT)
VALUES
( 'MIV-RICE-ADMIN-mnorthup'
, 1
, UUID()
, (select ROLE_ID from kricedev.KRIM_ROLE_T where ROLE_NM = 'Rice')
, 'mnorthup'
, 'P'
, null
, null
, NOW());

INSERT INTO KRICEDEV.KRIM_ROLE_MBR_T
( ROLE_MBR_ID
, VER_NBR
, OBJ_ID
, ROLE_ID
, MBR_ID
, MBR_TYP_CD
, ACTV_FRM_DT
, ACTV_TO_DT
, LAST_UPDT_DT)
VALUES
( 'MIV-TECH-ADMIN-stephenp'
, 1
, UUID()
, (select ROLE_ID from KRICEDEV.KRIM_ROLE_T where ROLE_NM = 'Technical Administrator')
, 'stephenp'
, 'P'
, null
, null
, NOW());

-- Assign Rice role
INSERT INTO kricedev.KRIM_ROLE_MBR_T
( ROLE_MBR_ID
, VER_NBR
, OBJ_ID
, ROLE_ID
, MBR_ID
, MBR_TYP_CD
, ACTV_FRM_DT
, ACTV_TO_DT
, LAST_UPDT_DT)
VALUES
( 'MIV-RICE-ADMIN-stephenp'
, 1
, UUID()
, (select ROLE_ID from kricedev.KRIM_ROLE_T where ROLE_NM = 'Rice')
, 'stephenp'
, 'P'
, null
, null
, NOW());

INSERT INTO KRICEDEV.KRIM_ROLE_MBR_T
( ROLE_MBR_ID
, VER_NBR
, OBJ_ID
, ROLE_ID
, MBR_ID
, MBR_TYP_CD
, ACTV_FRM_DT
, ACTV_TO_DT
, LAST_UPDT_DT)
VALUES
( 'MIV-TECH-ADMIN-lgjohnst'
, 1
, UUID()
, (select ROLE_ID from KRICEDEV.KRIM_ROLE_T where ROLE_NM = 'Technical Administrator')
, 'lgjohnst'
, 'P'
, null
, null
, NOW());

-- Assign Rice role
INSERT INTO kricedev.KRIM_ROLE_MBR_T
( ROLE_MBR_ID
, VER_NBR
, OBJ_ID
, ROLE_ID
, MBR_ID
, MBR_TYP_CD
, ACTV_FRM_DT
, ACTV_TO_DT
, LAST_UPDT_DT)
VALUES
( 'MIV-RICE-ADMIN-lgjohnst'
, 1
, UUID()
, (select ROLE_ID from kricedev.KRIM_ROLE_T where ROLE_NM = 'Rice')
, 'lgjohnst'
, 'P'
, null
, null
, NOW());


commit
;
