--  Author: Stephen Paulsen
-- Created: 2009-03-04
-- Updated: 2014-06-25

-- Populate the SystemDepartment table
-- Dependencies: SystemSchool table must exist and be populated
--
-- Department number 500 is reserved for the "blank" department and is used mainly
-- to represent direct employees of the school. For example, the libraries employ
-- people directly, not within different departments.  A department number of 500
-- should not be used for other purposes.
--
-- Department nuber 1000 is reserved for moving users into an "inactive" state.
-- There is now an active/inactive flag set on the user's account record and
-- the "Inactive" department shouldn't be needed any more.
--
-- A few other departments have standard numbers that should be used:
--   123 : *Other - Department not listed
--   214 : Administration
--   232 : Academic Affairs
--   239 : Dean's Office
--   257 : Academic Personnel[ Services]
--         (UCI has 302 for this but it's a "demo" so we'll leave it alone)
--

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

truncate SystemDepartment;

insert into SystemDepartment (SchoolID, DepartmentID, Description, Abbreviation, Active, InsertUserID)
values

-- School of Medicine (SOM)
( 1,    1, 'Internal Medicine', '', true, 18099),
( 1,    2, 'Pathology & Laboratory Medicine', '', true, 18099),
( 1,    4, 'Family & Community Medicine', '', true, 18099),
( 1,    5, 'Surgery', '', true, 18099),
( 1,    6, 'Psychiatry and Behavioral Sciences', '', true, 18099),
( 1,    7, 'Dermatology', '', true, 18099),
( 1,    8, 'Pediatrics', '', true, 18099),
( 1,    9, 'Radiology', '', true, 18099),
( 1,   10, 'Obstetrics & Gynecology', '', true, 18099),
( 1,   12, 'Hematology/Oncology', '', false, 20729),
( 1,   15, 'Pulmonary & Critical Care Medicine', '', false, 20729),
( 1,   28, 'Infect & Immun Dis: Med', '', false, 20729),
( 1,   29, 'Anesthesiology & Pain Medicine', '', true, 18099),
( 1,   32, 'Medical Informatics', '', false, 20729),
( 1,   35, 'Otolaryngology', '', true, 18099),
( 1,   38, 'Emergency Medicine', '', true, 18099),
( 1,   44, 'Center for Health & Technology', '', false, 20729),
( 1,   53, 'Physical Medicine & Rehabilitation', '', true, 18099),
( 1,   60, 'Public Health Science', '', true, 18099),
( 1,   67, 'Cardiovascular Medicine', '', false, 20729),
( 1,   69, 'Ophthalmology and Visual Science', '', true, 18099),
( 1,   70, 'Gastroenterology', '', false, 20729),
( 1,   71, 'Radiation Oncology', '', true, 18099),
( 1,   82, 'Urology', '', true, 18099),
( 1,   88, 'Neurology/Neuroscience', '', false, 20729),
( 1,  103, 'Orthopaedic Surgery', '', true, 18099),
( 1,  112, 'Cell Biology and Human Anatomy', '', true, 18099),
( 1,  115, 'Nephrology: Med', '', false, 20729),
( 1,  123, '*Other - Department not listed', '', true, 18099),
( 1,  202, 'Microbiology & Immunology', '', true, 18099),
( 1,  204, 'Physiology and Membrane Biology', '', true, 18099),
( 1,  207, 'Bioethics', '', false, 20729),
( 1,  208, 'Allergy and Clinical Immunology', '', false, 20729),
( 1,  212, 'Cancer Center', '', true, 18099),
( 1,  214, 'Administration', '', false, 20729),
( 1,  216, 'M.I.N.D. Institute', '', false, 20729),
( 1,  219, 'Neurological Surgery', '', true, 18099),
( 1,  221, 'Endocrinology', '', false, 20729),
( 1,  226, 'Biochemistry and Molecular Medicine', '', true, 18099),
( 1,  229, 'Center for Biophotonics', '', false, 18099), -- "C4B"
( 1,  230, 'Pharmacology', '', true, 18099),
( 1,  231, 'Office of Medical Education', '', true, 20729), -- MIV-4277 2012-04-10
( 1,  232, 'Academic Affairs', '', true, 18099),
( 1,  241, 'Office of Sponsored Programs', '', true, 18099),
( 1,  242, 'General Medicine', '', false, 20729),
( 1,  243, 'Rheumatology', '', false, 20729),
( 1,  290, 'Neurology', '', true, 18099),
( 1,  301, 'Health Sciences Library', '', false, 20729),
( 1,  315, 'Center for Comparative Medicine', '', false, 20729),
( 1,  326, 'Center for Heathly Aging', '', false, 20729),

-- Veterinary Medicine (Vetmed)
(18,   68, 'Biochemistry & Molecular Biology', '', false, 20729),
(18,  214, 'Administration', '', false, 20729),
(18,  232, 'Academic Affairs', '', true, 18099),
(18,  233, 'Anatomy, Physiology & Cell Biology', '', true, 18099),
(18,  234, 'Molecular Biosciences', '', true, 18099),
(18,  235, 'Medicine & Epidemiology', '', true, 18099),
(18,  236, 'Pathology, Microbiology & Immunology', '', true, 18099),
(18,  237, 'Population Health & Reproduction', '', true, 18099),
(18,  238, 'Surgical & Radiological Sciences', '', true, 18099),
(18,  239, 'Dean''s Office', '', true, 20001),
(18,  250, 'California Health and Food Safety System', '', true, 18099),
(18,  312, 'Veterinary Genetics Laboratory', '', true, 18099),
(18,  314, 'Center for Comparative Medicine', '', true, 18099),
(18,  316, 'Veterinary Medicine Extension', '', true, 18099),
(18,  317, 'International Lab of Molecular Biology for Tropical Diseases', '', false, 20729),
(18,  329, 'Western Institute for Food Safety and Security', '', true, 18099),
(18,  333, 'Wildlife Health Center', '', true, 18099),

-- UCSF (San Francisco)
-- (21,    1, 'Internal Medicine', '', true, 18099),
-- (21,  214, 'Administration', '', true, 18099),
-- (21,  232, 'Academic Affairs', '', true, 18099),
-- Now "Sample Dean's Office"
(21,    1, 'Sample Department 1', '', true, 18099),
(21,  214, 'Sample Department 2', '', true, 18099),
(21,  232, 'Sample Academic Affairs', '', true, 18099),

-- College of Biological Sciences
(23,   68, 'Biochemistry & Molecular Biology', '', false, 20729),
(23,  214, 'Administration', '', false, 20729),
(23,  232, 'Academic Affairs', '', true, 18099),
(23,  239, 'Dean''s Office', '', false, 20729),
(23,  240, 'Molecular and Cellular Biology', '', true, 18099),
(23,  258, 'Neurobiology, Physiology and Behavior', '', true, 18099),
(23,  287, 'Plant Biology', '', true, 18099),
(23,  292, 'Microbiology and Molecular Genetics', '', true, 20729),
(23,  293, 'Evolution and Ecology', '', true, 18099),
(23,  294, 'Center for Neuroscience', '', true, 18099),
(23,  295, 'Genetics and Development', '', false, 20729),
(23,  296, 'Genome Center', '', true, 18099),
(23,  297, 'Population Biology', '', false, 20729),

-- Provost's Office
(24,   23, 'Public Affairs', '', true, 18099),
(24,  123, '*Other - Department not listed', '', true, 18099),
(24,  214, 'Administration', '', true, 18099),
(24,  232, 'Academic Affairs', '', true, 18099),
(24, 1000, 'Inactive', '', true, 18099),

-- College of Agricultural & Environmental Sciences (CAES)
-- (27,   54, 'Environmental Design', '', false, 20729), /* Drop the department because none of the dossiers belongs to this department */
(27,  102, 'Environmental Toxicology', '', true, 18099),
(27,  123, '*Other - Department not listed', '', true, 18099),
(27,  225, 'Nutrition', '', true, 18099),
(27,  232, 'Academic Affairs', '', true, 18099),
(27,  240, 'Molecular and Cellular Biology', '', false, 20729),
(27,  246, 'Human Ecology', '', true, 20729),
(27,  247, 'Animal Science', '', true, 18099),
(27,  248, 'Environmental Science & Policy', '', true, 18099),
(27,  249, 'Landscape Architecture', '', false, 20729),
(27,  251, 'Agricultural & Resource Economics', '', true, 18099),
(27,  253, 'Plant Pathology', '', true, 18099),
(27,  255, 'Land, Air and Water Resources', 'LAWR', true, 18099),
(27,  256, 'Entomology & Nematology', '', true, 20729),
(27,  262, 'Plant Sciences', '', true, 18099),
(27,  271, 'Biological and Agricultural', '', true, 18099), -- used for joint appointments with matching department in Engineering (school 34)
(27,  285, 'Food Science and Technology', '', true, 20729),
(27,  286, 'Technology', '', false, 20729),
(27,  303, 'Textiles and Clothing', '', true, 18099),
(27,  304, 'Viticulture & Enology', '', true, 18099),
(27,  311, 'Wildlife, Fish and Conservation Biology', '', true, 18099),
(27,  323, 'Nematology', '', false, 20729),
(27,  324, 'International Programs Office', '', true, 18099),
(27,  325, 'Agricultural Sustainability Institute', '', true, 18099),
(27,  326, 'Contained Research Facility', '', true, 20729),
(27,  327, 'Agricultural Issues Center', '', true, 20729),
(27,  328, 'Arboretum', '', true, 18099),

-- UCSD (San Diego)
-- (29,  119, 'Center for Health Services Research', '', true, 18099),
-- (29,  209, 'Biotechnology', '', true, 18099),
-- (29,  213, 'Adult Infusion Center', '', true, 18099),
-- (29,  257, 'Academic Personnel Services', '', true, 18099),

-- LS: MPS [formerly "Letters and Science (L&S)"]
(30,  203, 'Chemistry', '', true, 18099),
(30,  214, 'Administration', '', false, 20729),
(30,  232, 'Academic Affairs', '', true, 18099),
(30,  263, 'Mathematics', '', true, 18099),
(30,  269, 'Statistics', '', true, 18099),
-- (30,  279, 'Geology', '', true, 18099), -- MIV-5161 rename Geology dept.
(30,  279, 'Earth and Planetary Sciences', '', true, 18099),
(30,  281, 'Physics', '', true, 18099),
(30,  282, 'Nanomaterials in the Environment, Agriculture and Technology', 'NEAT', true, 20729),
-- School of Education
(31,  500, '', '', true, 18099),

-- Mathematics
-- (32,  257, 'Academic Personnel Services', '', true, 18099),

-- Engineering
(34,  257, 'Academic Personnel Services', '', true, 18099),
(34,  270, 'Applied Science', '', false, 20729),
(34,  271, 'Biological and Agricultural', '', true, 18099), -- used for joint appointments with matching department in CAES (school 27)
(34,  272, 'Biomedical Engineering', '', true, 18099),
(34,  273, 'Chemical Engineering and Materials Science', '', true, 18099),
(34,  274, 'Civil and Environmental', '', true, 18099),
(34,  275, 'Computer Science', '', true, 18099),
(34,  276, 'Electrical and Computer', '', true, 18099),
(34,  277, 'Mechanical and Aerospace', '', true, 18099),
(34,  330, 'Center for Molecular & Genome Imaging', '', false, 20729),

-- UCI (Irvine)
-- (35,  302, 'Academic Personnel', '', true, 18099),

-- Office of Research (OVCR)
(36,  305, 'Crocker Nuclear Laboratory', '', true, 18099),
(36,  306, 'John Muir Institute of the Environment', '', true, 18099),
(36,  308, 'Vice Chancellor''s Office', '', true, 18099),
(36,  309, 'Center for Health and the Environment', '', true, 18099),
(36,  310, 'Bodega Marine Laboratory', '', true, 18099),
(36,  311, 'Nuclear Magnetic Resonance Facility', '', true, 18099),
(36,  312, 'Campus Mass Spectrometry Facilities', '', true, 18099),
(36,  313, 'Interdisciplinary Center for Plasma Mass Spectrometry', '', true, 18099),
(36,  314, 'Biotechnology Program', '', true, 18099),
(36,  315, 'Air Quality Research Center', '', true, 18099),
(36,  316, 'Institute for Transportation Studies', '', true, 18099),
(36,  229, 'Center for Biophotonics', '', true, 18099), -- "C4B"
(36,  317, 'McClellan Nuclear Research Center', '', true, 20729), /* MIV-4071 */
(36,  318, 'Primate Center', '', true, 18635), /* MIV-4114 */
(36,  319, 'Center for Watershed Sciences', '', true, 18099),
(36,  320, 'Natural Reserve System', '', true, 18099),
(36,  321, 'Tahoe Environmental Research Center', '', true, 18099),

-- Graduate School of Management (GSM)
(37,  500, '', '', true, 18099),

-- Information and Educational Technology (IET)
(38,  313, 'Application Development', '', true, 18099),
(38, 1000, 'Inactive', '', true, 18099),

-- General Library
(39,  500, '', '', true, 18099),

-- Law Library
(40,  500, '', '', false, 18099),

-- Offices of the Chancellor and Provost
(41,  500, '', '', true, 18099),

-- Academic Senate
(42,  500, '', '', true, 18099),

-- UCR (Riverside)
-- (43,  232, 'Academic Affairs', '', true, 18099),

-- UCLA
(44,  257, 'Academic Personnel Services', '', true, 18099),

-- LS: HArCS (Letters and Science: Humanities, Arts, Comparative Studies)
(45,  232, 'Academic Affairs', '', true, 19012),
(45,  214, 'Administration', '', false, 20729),
(45,    3, 'African American and African Studies', '', true, 19012),
(45,    4, 'American Studies', '', true, 19012),
(45,    5, 'Art History', '', true, 19012),
(45,    6, 'Art Studio', '', true, 19012),
(45,    7, 'Asian American Studies', '', true, 19012),
(45,    8, 'Chicana/Chicano Studies', '', true, 19012),
(45,    9, 'Classics', '', true, 19012),
(45,   10, 'Comparative Literature', '', true, 19012),
(45,   11, 'Consortium for Language Learning and Teaching', '', false, 20729),
(45,   12, 'Critical Theory', '', false, 20729),
(45,   13, 'Cultural Studies Graduate Program', '', false, 20729),
(45,   14, 'Design', '', true, 19012),
(45,   15, 'East Asian Languages and Cultures (Chinese & Japanese)', '', true, 19012),
(45,   16, 'English', '', true, 19012),
(45,   17, 'Film Studies', '', false, 20729),
(45,   18, 'French and Italian', '', true, 19012),
(45,   19, 'German and Russian', '', true, 19012),
(45,   20, 'Humanities', '', true, 19012),
(45,   22, 'Davis Language Center', '', true, 19012),
(45,   23, 'Medieval Studies', '', false, 20729),
(45,   24, 'Music', '', true, 19012),
(45,   25, 'Native American Studies', '', true, 19012),
(45,   26, 'Nature and Culture', '', false, 20729),
(45,   27, 'Religious Studies', '', true, 19012),
(45,   29, 'Second Language Acquisition Institute', '', false, 20729),
(45,   30, 'Spanish and Portuguese', '', true, 20729),
(45,   31, 'Cinema and Technocultural Studies', '', true, 19012),
(45,   32, 'Theatre and Dance', '', true, 19012),
(45,   33, 'University Writing Program', '', true, 19012),
(45,   34, 'Women and Gender Studies', '', true, 19012),
(45,   35, 'Center for Transnational Health', 'CTH', true, 18099),

-- LS: Social Sciences
(46,  232, 'Academic Affairs', '', true, 18099),
(46,  214, 'Administration', '', false, 20729),
(46,    1, 'Anthropology', '', true, 19012),
(46,    2, 'Center for History, Society, and Culture', '', true, 19012),
(46,    3, 'Center for Mind and Brain', '', true, 19012),
(46,    4, 'Communication', '', true, 19012),
(46,    5, 'East Asian Studies', '', true, 19012),
(46,    6, 'Economics', '', true, 19012),
(46,    7, 'Economy, Justice, and Society', '', true, 19012),
(46,    8, 'English as a Second Language', '', true, 19012),
(46,    9, 'Hemispheric Institute on the Americas', '', true, 19012),
(46,   10, 'History', '', true, 19012),
(46,   11, 'International Relations', '', true, 19012),
(46,   12, 'Jewish Studies', '', true, 19012),
(46,   13, 'Linguistics', '', true, 19012),
(46,   14, 'Middle East/South Asia Studies', '', true, 19012),
(46,   15, 'Military Science', '', true, 19012),
(46,   16, 'Philosophy', '', true, 19012),
(46,   17, 'Intercollegiate Athletics/Physical Education', '', true, 19012),
(46,   18, 'Political Science', '', true, 19012),
(46,   19, 'Psychology', '', true, 19012),
(46,   20, 'Science and Technology Studies', '', true, 19012),
(46,   21, 'Sociology', '', true, 19012),
(46,   22, 'Physical Education', '', true, 20729),
(46,   23, 'Institute for Social Sciences', '', true, 18099),

-- "LS: MPS" is School 30, above - renamed from "Letters and Science"

-- SUNY
-- (50,  232, 'Academic Affairs', '', true, 18099),

-- Iowa State University
-- (51,  232, 'Academic Affairs', '', true, 18099),

-- School of Law
(52,  500, '', '', true, 18099),
(52,  501, 'Law Library', '', true, 18099),

-- University Extension
(53,  500, '', '', true, 18099),

-- Graduate Studies
(54,  500, '', '', true, 18099),

-- Betty Irene Moore School of Nursing (BIMSON)
(55,  500, '', '', true, 18099)
;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

