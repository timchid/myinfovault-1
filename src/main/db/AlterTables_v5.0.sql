-- Author: Stephen Paulsen
-- Date: 2015-03-19
-- Updated: yyyy-mm-dd (initials)
--
-- Table updates for v5.0

use `myinfovault`;

-- Set lenient modes for table mods.
SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

-- Begin Updates

-- Drop a lot of old and temp tables.
DROP TABLE `myinfovault`.`PacketDocumentType`;
DROP TABLE `myinfovault`.`PacketReviewer`;
DROP TABLE `myinfovault`.`OtherSchDept`;
DROP TABLE `myinfovault`.`Packets`;

DROP TABLE `myinfovault`.`appointment_temp`;
DROP TABLE `myinfovault`.`logger`;
DROP TABLE `myinfovault`.`mothra_no_zlogin`;
DROP TABLE `myinfovault`.`mothra_login`;
DROP TABLE `myinfovault`.`NIH`;
DROP TABLE `myinfovault`.`NIHparms`;
DROP TABLE `myinfovault`.`PkParms`;
DROP TABLE `myinfovault`.`Quarantine`;
DROP TABLE `myinfovault`.`roleassignment_temp`;
DROP TABLE `myinfovault`.`StatsLog`;
DROP TABLE `myinfovault`.`SessionLog`;
DROP TABLE `myinfovault`.`Tagline`;
DROP TABLE `myinfovault`.`TempDocument`;
DROP TABLE `myinfovault`.`TempOtherLetters`;
DROP TABLE `myinfovault`.`Training`;
DROP TABLE `myinfovault`.`uploadbackup`;
DROP TABLE IF EXISTS `myinfovault`.`UserRecordAnnotationTemp`;

-- DROP TABLE `myinfovault`.`9`;
-- DROP TABLE IF EXISTS `myinfovault`.`9`;

-- Set up Packet tables and Annotations reference to packet.
source createPacketTables.sql
source insertPacketTables.sql

ALTER TABLE Annotations
  ADD COLUMN `PacketID` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT 'zero (0) for master annotations'
  AFTER `SectionBaseTable`;
ALTER TABLE Annotations
  ADD CONSTRAINT fk_packetid FOREIGN KEY (PacketID) REFERENCES Packet(ID);

-- Add new events
ALTER TABLE EventLogDetails MODIFY COLUMN
Category ENUM('DOSSIER',
              'SIGNATURE',
              'REQUEST',
              'HOLD',
              'RELEASE',
              'DECISION',
              'DISCLOSURE',
              'RAF',
              'VOTE',
              'UPLOAD',
              'ROUTE',
              'REVIEW',
              'USER',
              'RECORDCHG',
              'PACKET',
              'ERROR') NOT NULL;

-- Add new attribute status
ALTER TABLE DossierAttributes MODIFY COLUMN
Value ENUM ('ADDED',
            'CLOSE',
            'OPEN',
            'COMPLETE',
            'SIGNED',
            'SIGNEDIT',
            'REQUESTED',
            'REQUESTEDIT',
            'INVALID',
            'INVALIDEDIT',
            'MISSING',
            'HOLD',
            'RELEASE',
            'CANCEL') NULL;

-- New PacketRequest table
source createPacketRequest.sql;

-- Add new packet_request attribute
source insertDossierAttributeType.sql;
source insertDossierAttributeLocation.sql;
source insertDelegationAuthorityAttributeCollection.sql;

-- Updated routing path definitions and workflow locations
source createWorkflowLocation.sql;
source insertWorkflowLocation.sql;

source createRoutingPathDefinitions.sql;
source insertRoutingPathDefinitions.sql;

-- Replace Candidate location with PacketRequest in all WorkflowLocations in the Dossier table
UPDATE Dossier SET WorkflowLocations = REPLACE(WorkflowLocations,'Candidate','Department');

-- Update the PreviousWorkflowLocations to 'PacketRequest' for those records now at the 'Department' in the Dossier table
UPDATE Dossier SET PreviousWorkflowLocations = 'PacketRequest' where WorkflowLocations = 'Department';

-- Replace the Candidate location with PacketRequest in all PreviousWorkflowLocations in the Dossier table
UPDATE Dossier SET PreviousWorkflowLocations = REPLACE(PreviousWorkflowLocations,'Candidate','PacketRequest');

-- Replace the CANDIDATE location with PACKETREQUEST in all Detail columns of the EventLogDetails table
UPDATE EventLogDetails SET Detail = REPLACE(Detail,'CANDIDATE','PACKETREQUEST');


-- Update the SnapshotLocation from Candidate to PacketRequest
UPDATE DossierSnapshot SET SnapshotLocation='PacketRequest' WHERE SnapshotLocation='Candidate';

-- Create ApiEntity tables
source createApiEntity.sql
source createApiEntityScope.sql

-- Function for generating random string
DELIMITER $$

DROP FUNCTION IF EXISTS randString$$

CREATE FUNCTION randString(in_length INT) RETURNS VARCHAR(512)
BEGIN
  SET @len = 0;
  SET @returnValue = "";
  WHILE (@len < in_length) DO
    SET @randChar = (
      SELECT CHAR(FLOOR(RAND()*10)+48) as chr
      UNION SELECT CHAR(FLOOR(RAND()*26)+65)
      UNION SELECT CHAR(FLOOR(RAND()*26)+97)
      ORDER BY rand() LIMIT 1);
    SET @returnValue = CONCAT(@returnValue,@randChar);
    SET @len = @len + 1;
  END WHILE;
  RETURN @returnValue;
END$$

DELIMITER ;


-- Insert
INSERT INTO ApiEntity (`ID`, `EntityName`, `KeyHash`, `Salt`, `SecurityScheme`)
  VALUES (1, "miv-frontend", "Not used for this scheme", randString(512), 'INTERNAL');
INSERT INTO ApiEntityScope
  (`EntityID`, `SchoolID`, `DepartmentID`, `UserID`, `Create`, `Read`, `Update`, `Delete`, `Resource`)
  VALUES (1, -1, -1, -1, TRUE, TRUE, TRUE, TRUE, 'ALL');

-- Pull Timestamps with dossier data.
source insertSection.sql

-- Save Timestamp every time user data entry is changed
source createMasterTimestamp.sql

-- Fix possible collation issue.
source functionNextVal.sql

-- procedure to cleanup documentSignature records with bad (no longer exists) school and department combinations
source procedureFixDecisions.sql

CALL fixDecisions(27, 54);
CALL fixDecisions(30, 51);
CALL fixDecisions(30, 117);
CALL fixDecisions(30, 264);
CALL fixDecisions(30, 278);

-- End Updates

-- Restore normal modes.
SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
