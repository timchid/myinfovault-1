-- Author: Lawrence Fyfe
-- Created: 10/17/2006
-- Updated: 09/24/2009

create table UserUploadDocument
(
   ID int primary key auto_increment,
   UserID int,
   DocumentID int,
   UploadName tinytext,
   UploadFile tinytext,
   Year varchar(4),
   Sequence int,
   AccessLevel varchar(2),
   PacketID int NULL default 0,
   Display boolean NOT NULL default true,
   UploadLocation varchar(40) NOT NULL,
   Redacted boolean NOT NULL default false,
   Confidential boolean NOT NULL default false,
   UploadFirst boolean NOT NULL default false,
   InsertTimestamp timestamp default current_timestamp,
   InsertUserID int,
   UpdateTimestamp datetime,
   UpdateUserID int,
   index (UserID)
)
engine = InnoDB
default character set utf8;
