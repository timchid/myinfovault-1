-- Author: Rick Hendricks
-- Date: 08/05/2011
-- This table defines the group types for a group (Review, etc).

-- Create the Group Type table
-- Dependencies: None 

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

drop table if exists `GroupType`;

create table `GroupType`
(
   `ID` int not null,
   `Description` varchar(40) not null,
   `InsertTimestamp` timestamp default current_timestamp,
   `InsertUserID` int,
   `UpdateTimestamp` datetime,
   `UpdateUserID` int,
   PRIMARY KEY (`ID`)
)
engine = InnoDB
default character set utf8;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
