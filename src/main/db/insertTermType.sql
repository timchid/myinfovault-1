# Author: Lawrence Fyfe
# Date: 10/4/2006

truncate TermType;

insert into TermType (ID,Code,GroupCode,Description,Sequence,DropDownSequence,InsertUserID)
values
(1,'01','01','Winter Quarter',1,2,14021),
(2,'02','02','Spring Semester',3,8,14021),
(3,'03','01','Spring Quarter',2,3,14021),
(4,'04','02','Extra Session',5,9,14021),
(5,'05','01','Summer Session 1',6,5,14021),
(6,'06','02','Summer Special Session',8,10,14021),
(7,'07','01','Summer Session 2',7,6,14021),
(8,'08','01','Summer Quarter',4,4,14021),
(9,'09','02','Fall Semester',10,7,14021),
(10,'10','01','Fall Quarter',9,1,14021),
(11,'00','01','None',0,0,14021);
update TermType set ID=0 where ID=11;

