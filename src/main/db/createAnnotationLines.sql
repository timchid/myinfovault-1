-- Author: Pradeep K Haldiya
-- Created: 2013-09-23

-- Create the AnnotationLines Table
-- Dependencies: The UserRecordAnnotation table must exist.

DROP TABLE IF EXISTS AnnotationLines;

create table AnnotationLines
(
   ID int primary key auto_increment,
   AnnotationID int,
   Label mediumtext,
   RightJustify boolean,
   DisplayBefore boolean,
   InsertTimestamp timestamp default current_timestamp,
   InsertUserID int,
   UpdateTimestamp datetime,
   UpdateUserID int,
   CONSTRAINT `fk_annotationid` FOREIGN KEY (`AnnotationID`) 
   REFERENCES `Annotations`(`ID`) ON DELETE CASCADE ON UPDATE NO ACTION
)
engine = InnoDB
default character set utf8;
