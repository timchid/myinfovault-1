-- Author: Rick Hendricks
-- Created: 2012-02-03
-- Updated: 2012-02-03

-- Create the Works Table
-- Dependencies: The WorkType table must exist.

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

DROP TABLE IF EXISTS Works;

create table Works
(
   ID int auto_increment,
   UserID int NOT NULL,
   WorkTypeID int NOT NULL,
   WorkYear varchar(4) NOT NULL,
   StatusID int NOT NULL,
   Creators text NOT NULL,
   Title text NOT NULL,
   Description text NOT NULL,
   URL text NULL,
   Contributors text NULL,
   Sequence int,
   Display boolean NOT NULL default true, 
   DisplayContribution boolean NOT NULL default true, 
   InsertTimestamp timestamp DEFAULT CURRENT_TIMESTAMP,
   InsertUserID int NOT NULL,
   UpdateTimestamp timestamp NULL,
   UpdateUserID int NULL,
   PRIMARY KEY (`ID`),
   INDEX(`UserID`),
   CONSTRAINT `fk_worktypeid` FOREIGN KEY (`WorkTypeID`) REFERENCES `WorkType`(`ID`) ON DELETE NO ACTION ON UPDATE CASCADE,
   CONSTRAINT `fk_workstatusid` FOREIGN KEY (`StatusID`) REFERENCES `WorkStatus`(`ID`) ON DELETE NO ACTION ON UPDATE CASCADE
)ENGINE=INNODB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
