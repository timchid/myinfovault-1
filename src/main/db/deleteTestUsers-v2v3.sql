-- Author: Stephen Paulsen
-- Date: 2009-09-22
-- Updated: 2009-09-22

-- Delete test users as specified in MIV-2891
DELETE FROM CvMem WHERE Uid IN (12209, 16172, 11779, 14704, 13351, 13353, 13354, 13359, 13358, 13357, 13420, 13355, 14350, 14703, 13175, 16518, 13361, 15674, 12595);

DELETE FROM UserLogin WHERE UserID IN (12209, 16172, 11779, 14704, 13351, 13353, 13354, 13359, 13358, 13357, 13420, 13355, 14350, 14703, 13175, 16518, 13361, 15674, 12595);



-- Raw list of user ID numbers dumped from "column B" from spreadsheet attached to MIV-2891
-- 12209
-- 16172
-- 11779
-- 14704
-- 13351
-- 13353
-- 13354
-- 13359
-- 13358
-- 13357
-- 13420
-- 13355
-- 14350
-- 14703
-- 13175
-- 16518
-- 13361
-- 15674
-- 12595

-- Very troublesome users, cvlevels of 3, 3, 3 and 2
-- Francine Plummer fhplumme, Peggy Janzen peggy.janzen, Tom Mezzanares tjmezzan, Cynthia VanSchaemelhout cva
-- none of these users are in LDAP
DELETE FROM UserLogin WHERE UserID IN
(  9778,                      10072,                     17255,                   18874);
DELETE FROM CvMem WHERE Uid IN
(  9778,                      10072,                     17255,                   18874);
