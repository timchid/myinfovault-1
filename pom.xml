<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <!-- PROJECT SPECIFIC PROPERTIES -->
    <groupId>edu.ucdavis.myinfovault</groupId>
    <artifactId>miv</artifactId>
    <version>5.0.1</version>
    <packaging>war</packaging>
    <name>MyInfoVault</name>
    <url>http://myinfovault.ucdavis.edu</url>

    <!-- Artifactory -->
    <dependencyManagement>
        <dependencies>
            <dependency>
                <groupId>xml-apis</groupId>
                <artifactId>xml-apis</artifactId>
                <version>1.3.04</version>
                <type>jar</type>
            </dependency>
            <dependency>
                <groupId>xml-apis</groupId>
                <artifactId>xml-apis-ext</artifactId>
                <version>1.3.04</version>
                <type>jar</type>
            </dependency>
            <dependency>
                <groupId>xalan</groupId>
                <artifactId>xalan</artifactId>
                <version>2.7.0</version>
                <type>jar</type>
            </dependency>
            <!-- SPRING BILL OF MATERIALS -->
            <dependency>
                <groupId>org.springframework</groupId>
                <artifactId>spring-framework-bom</artifactId>
                <version>4.1.5.RELEASE</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>
        </dependencies>
    </dependencyManagement>
    <distributionManagement>
        <repository>
            <id>maven.ucdavis.edu</id>
            <name>maven.ucdavis.edu-releases</name>
            <url>http://maven.ucdavis.edu/libs-releases-local</url>
        </repository>
        <snapshotRepository>
            <id>maven.ucdavis.edu</id>
            <name>maven.ucdavis.edu-snapshots</name>
            <url>http://maven.ucdavis.edu/libs-snapshots-local</url>
            <uniqueVersion>false</uniqueVersion>
        </snapshotRepository>
    </distributionManagement>

    <!-- DEFAULT PROPERTIES -->
    <properties>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <!-- The timestamp format is ignored...maven bug -->
        <maven.build.timestamp.format>yyyy-MMM-dd HH:mm zzz</maven.build.timestamp.format>
        <machine></machine>
        <miv.skipCompress>true</miv.skipCompress> <!-- To minify set to false -->
        <miv.warExcludes></miv.warExcludes> <!-- To minify set to **/*.css,**/*.js -->
    </properties>

    <!-- SVN CONNECTION -->
    <issueManagement>
        <system>Jira</system>
        <url>https://ucdavis.jira.com/browse/MIV</url>
    </issueManagement>
    <scm>
        <connection>scm:git:https://mivteam@bitbucket.org/mivteam/myinfovault.git</connection>
        <tag>HEAD</tag>
        <url>https://bitbucket.org/mivteam/myinfovault/commits/branch/master</url>
    </scm>

    <!-- WEBAPP BUILD -->
    <organization>
        <name>UC Davis - EAIS</name>
    </organization>
    <build>
        <!-- WAR FILE NAME -->
        <finalName>${project.artifactId}</finalName>
        <!-- finalName>${project.artifactId}##${project.version}</finalName -->

        <!-- PLUGIN CUSTOMIZATIONS -->
        <plugins>
            
            <!-- Java Compiler Plugin -->
            <plugin>
                <artifactId>maven-compiler-plugin</artifactId>
                <configuration>
                    <source>1.7</source>
                    <target>1.7</target>
                </configuration>
            </plugin>

            <!-- Have maven get javadoc and sources for dependencies -->
            <!--<plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-eclipse-plugin</artifactId>
                <configuration>
                    <downloadSources>true</downloadSources>
                    <downloadJavadocs>true</downloadJavadocs>
                </configuration>
            </plugin>-->

           <!-- Javascript minification -->
           <plugin>
                <groupId>net.alchim31.maven</groupId>
                <artifactId>yuicompressor-maven-plugin</artifactId>
                <executions>
                    <execution>
                        <goals>
                           <goal>compress</goal>
                        </goals>
                    </execution>
                </executions>
                <configuration>
                    <nosuffix>true</nosuffix>
                    <skip>${miv.skipCompress}</skip>
                </configuration>
           </plugin>
            <!-- JSP PRECOMPILE -->
            <plugin>
                <groupId>org.jasig.mojo.jspc</groupId>
                <artifactId>jspc-maven-plugin</artifactId>
                <version>2.0.0</version>

                <executions>
                    <execution>
                        <id>jspc</id>

                        <goals>
                             <goal>compile</goal>
                        </goals>
                    </execution>
                </executions>

                <dependencies>
                    <dependency>
                        <groupId>org.jasig.mojo.jspc</groupId>
                        <artifactId>jspc-compiler-tomcat7</artifactId>
                        <version>2.0.0</version>
                    </dependency>
                </dependencies>

                <configuration>
                    <!-- Turn off compiling and don't include in the war file.
                         This still runs the JSPC compiler and does a syntax check,
                         and the build will fail if there are errors in a JSP file,
                         but this does not create xxx_jsp.java and xxx_jsp.class files
                         and doesn't output the jspweb.xml file. -->
                    <compile>false</compile>
                    <includeInProject>false</includeInProject>
                    <trimSpaces>false</trimSpaces>
                </configuration>
            </plugin>

            <!-- WAR PACKAGING -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-war-plugin</artifactId>
                <version>2.2</version>

                <configuration>
                    <!-- No longer outputting the jspweb.xml file, so get the webXml from our source web.xml file. -->
                    <!--webXml>${project.build.directory}/jspweb.xml</webXml-->
                    <webXml>src/main/webapp/WEB-INF/web.xml</webXml>
                    <containerConfigXML>src/main/webapp/META-INF/context.xml</containerConfigXML>
                    <filteringDeploymentDescriptors>true</filteringDeploymentDescriptors>
                    <archive>
                        <manifest>
                            <addDefaultImplementationEntries>true</addDefaultImplementationEntries>
                        </manifest>

                        <manifestEntries>

                            <!-- The timestamp of the build -->                
                            <Build-Date>${timestamp}</Build-Date>
                            
                            <!-- Full descriptive version of current build. Includes closest tag, tag delta, and abbreviated commit hash.
                             Example: 1.2.0-SNAPSHOT-2-c154712 -->
                            <Build-Version>${build-version}</Build-Version>
                 
                            <!-- Full hash of commit -->
                            <Build-Commit>${build-commit}</Build-Commit>
                            
                            <!-- Abbreviated hash of commit -->
                            <Build-Revision>${build-commit-abbrev}</Build-Revision>
                            
                            <!-- Closest repository tag (in commit history following "first parents"). NOTE: Only tags starting with v are considered, and the v is stripped.
                             Example: 1.2.0-SNAPSHOT (tag on git: v1.2.0-SNAPSHOT). -->
                            <Build-Tag>${build-tag}</Build-Tag>
                           
                            <!-- Number of commits since the closest tag until HEAD. Example: 2 -->
                            <Build-Tag-Delta>${build-tag-delta}</Build-Tag-Delta>
                           
                            <!--  A date and time stamp of the current commit (HEAD). -->
                            <Last-Commit>${build-tstamp}</Last-Commit>
                            
                        </manifestEntries>
                    </archive>

                    <!-- webapp filtered resources -->
                    <webResources>
                        <!-- apache front-end -->
                        <resource>
                            <directory>src/main/webapp/html</directory>
                            <targetPath>html</targetPath>
                            <filtering>true</filtering>
                            <includes>
                                <include>**/*.html</include>
                            </includes>
                        </resource>

                        <!-- jsp files -->
                        <resource>
                            <directory>src/main/webapp/jsp</directory>
                            <targetPath>jsp</targetPath>
                            <filtering>true</filtering>
                            <includes>
                                <include>miv*footer.jsp</include>
                            </includes>
                        </resource>

                        <!-- help files -->
                        <resource>
                            <directory>src/main/webapp/help</directory>
                            <targetPath>help</targetPath>
                            <filtering>true</filtering>
                            <includes>
                                <include>**/*.html</include>
                            </includes>
                        </resource>

                        <!-- xslt files -->
                        <resource>
                            <directory>src/main/webapp/xslt</directory>
                            <targetPath>xslt</targetPath>
                            <filtering>true</filtering>
                            <includes>
                                <include>**/*.xslt</include>
                            </includes>
                        </resource>

                        <!-- webapp configuration -->
                        <resource>
                            <directory>src/main/resources</directory>
                            <targetPath>WEB-INF/classes</targetPath>
                            <filtering>true</filtering>
                        </resource>
                    </webResources>
                    <warSourceExcludes>${miv.warExcludes}</warSourceExcludes>
                </configuration>
            </plugin>

            <!-- TOMCAT DEPLOYMENT -->
            <plugin>
                <groupId>org.apache.tomcat.maven</groupId>
                <artifactId>tomcat7-maven-plugin</artifactId>
                <version>2.2</version>

                <configuration>
                    <server>${machine}</server>
                    <url>http://${machine}.ucdavis.edu:8080/manager</url>
                </configuration>
            </plugin>
           
           <!-- BUILD NUMBER PLUGIN-->
           <!-- Keeping this in place to use the ${timestamp} property rather than the ${build-tstamp} in -->
           <!-- the git build version plugin -->
            <plugin>
                <groupId>org.codehaus.mojo</groupId>
                <artifactId>buildnumber-maven-plugin</artifactId>
                <version>1.1</version>
                <executions>
                    <execution>
                        <phase>validate</phase>
                        <goals>
                            <goal>create</goal>
                        </goals>
                    </execution>
                </executions>
                <configuration>
                    <doCheck>false</doCheck>
                    <doUpdate>false</doUpdate>
                </configuration>
            </plugin>
            
            <!-- git BUILD VERSION PLUGIN -->
            <plugin>
              <groupId>com.code54.mojo</groupId>
              <artifactId>buildversion-plugin</artifactId>
              <version>1.0.2</version>
              <executions>
                <execution>
                  <goals>
                    <goal>set-properties</goal>
                  </goals>
                  <configuration>
                    <gitCmd>git</gitCmd>
                    <!-- This is the timstamp of the last commit rather than the timestamp of the build, so we will still use -->
                    <!-- the timstamp value from the buildnumber-maven-plugin as well. -->
                    <!-- SimplDateFormat formatting options -->
                    <tstampFormat>EEE, d MMM yyyy, HH:mm:ss z</tstampFormat>
          
                    <!-- Example:  Define a new project property 'build-tag-lowercase', based 
                      on 'build-tag' Note how 'build-tag' is available to the script as a local 
                      variable. -->
                    <customProperties>
                      { :build-tag-lowercase
                      (clojure.string/lower-case build-tag) }
                    </customProperties>
                  </configuration>
                </execution>
              </executions>
            </plugin>

            <!-- JAVADOC -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-javadoc-plugin</artifactId>
                <version>2.8.1</version>

                <executions>
                    <execution>
                        <id>attach-sources</id>
                        <goals>
                            <goal>jar</goal>
                        </goals>
                    </execution>
                </executions>

                <configuration>
                    <stylesheet>java</stylesheet>
                    <footer>${project.name} v${project.version} rev: ${buildNumber}, Generated ${maven.build.timestamp}</footer>
                    <show>package</show>
                    <failOnError>false</failOnError>
                </configuration>
            </plugin>

            <!-- JSDOC -->
            <plugin>
                <groupId>com.phasebash.jsdoc</groupId>
                <artifactId>jsdoc3-maven-plugin</artifactId>
                <version>1.1.0</version>
                <configuration>
<!--                     <recursive>true</recursive> -->
<!--                     <directoryRoots> -->
<!--                         <directoryRoot>src/main/webapp/js</directoryRoot> -->
<!--                     </directoryRoots> -->
                    <sourceFiles>
                        <sourceFile>src/main/webapp/js/mivcommon.js</sourceFile>
                        <sourceFile>src/main/webapp/js/widget-datatables-serverside.js</sourceFile>
                        <sourceFile>src/main/webapp/js/widget-selection.js</sourceFile>
                        <sourceFile>src/main/webapp/js/widget-workflow.js</sourceFile>
                        <sourceFile>src/main/webapp/js/widget-suggestion.js</sourceFile>
                        <sourceFile>src/main/webapp/js/ldapselect.js</sourceFile>
                        <sourceFile>src/main/webapp/js/newuseredit.js</sourceFile>
                        <sourceFile>src/main/webapp/js/manageusers-common.js</sourceFile>
                        <sourceFile>src/main/webapp/js/widget-draganddrop.js</sourceFile>
                        <sourceFile>src/main/webapp/js/miv.validate.js</sourceFile>
                        <sourceFile>src/main/webapp/js/miv.errors.js</sourceFile>
                        <sourceFile>src/main/webapp/js/miv.dom.js</sourceFile>
                        <sourceFile>src/main/webapp/js/miv.api.js</sourceFile>
                    </sourceFiles>
                </configuration>
            </plugin>

            <!-- GRINDER -->
            <plugin>
                <groupId>com.fides</groupId>
                <artifactId>grinderplugin</artifactId>
                <version>0.0.1-SNAPSHOT</version>

                <configuration>
                    <path>src/test/config/grinder.properties</path>
                    <pathTest>src/test/jython</pathTest>
                </configuration>
            </plugin>

            <!-- XML TRANSFORMS -->
            <plugin>
                <groupId>org.codehaus.mojo</groupId>
                <artifactId>xml-maven-plugin</artifactId>
                <version>1.0</version>

                <executions>
                    <execution>
                        <goals>
                            <goal>transform</goal>
                        </goals>
                    </execution>
                </executions>

                <configuration>
                    <transformationSets>
                        <!-- MENU PAGE TRANSFORM -->
                        <transformationSet>
                            <dir>src/main/resources</dir>
                            <stylesheet>src/main/webapp/xslt/menu/menu.xslt</stylesheet>
                            <outputDir>src/main/webapp/jsp</outputDir>

                            <includes>
                                <include>menu.xml</include>
                            </includes>

                            <fileMappers>
                                <fileMapper implementation="org.codehaus.plexus.components.io.filemappers.FileExtensionMapper">
                                    <targetExtension>.jsp</targetExtension>
                                </fileMapper>
                            </fileMappers>
                        </transformationSet>

                        <!-- MENU TRANSFORM -->
                        <transformationSet>
                            <dir>src/main/resources</dir>
                            <stylesheet>src/main/webapp/xslt/menu/mivmenu.xslt</stylesheet>
                            <outputDir>src/main/webapp/jsp</outputDir>

                            <includes>
                                <include>menu.xml</include>
                            </includes>

                            <fileMappers>
                                <fileMapper implementation="org.codehaus.plexus.components.io.filemappers.RegExpFileMapper">
                                    <pattern>^.*$</pattern>
                                    <replacement>mivmenu.html</replacement>
                                </fileMapper>
                            </fileMappers>
                        </transformationSet>
                    </transformationSets>
                </configuration>
            </plugin>
        </plugins>

        <!-- TOKEN FILTER PROPERTIES-->
        <filters>
            <filter>src/main/filters/ldap.properties</filter>
            <filter>src/main/filters/application.properties</filter>
        </filters>
    </build>

    <!-- MAVEN REPOSITORIES -->
    <repositories>
        <repository>
            <id>maven.ucdavis.edu</id>
            <url>http://maven.ucdavis.edu/nexus/content/groups/libs-releases</url>
        </repository>

        <repository>
            <id>repo1.maven.org</id>
            <url>http://repo1.maven.org/maven2</url>
        </repository>
        
        <repository>
            <id>spring-json.sourceforge.net</id>
            <url>http://spring-json.sourceforge.net/repository</url>
        </repository>

        <repository>
            <id>repository.apache.org</id>
            <url>http://repository.apache.org/snapshots</url>
        </repository>
     
        <repository>
            <id>eaio.com</id>
            <url>http://eaio.com/maven2</url>
        </repository>
        
        
    </repositories>
    
    <!-- git PLUGIN REPOSITORY -->
    <pluginRepositories>
       <pluginRepository>
         <id>sonatype-releases</id>
         <url>http://oss.sonatype.org/content/repositories/releases</url>
       </pluginRepository>
       <pluginRepository>
         <id>clojars.org</id>
         <url>http://clojars.org/repo</url>
       </pluginRepository>

       <!-- If you want to try SNAPSHOT versions, you need Sonatype's snapshots repo: -->
       <pluginRepository>
         <id>sonatype-snapshots</id>
         <url>http://oss.sonatype.org/content/repositories/snapshots</url>
       </pluginRepository>
    </pluginRepositories>

    <!-- PROJECT DEPENDENCIES -->
    <dependencies>
        <!-- JUNIT TESTING -->
        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <version>4.10</version>
            <scope>test</scope>
        </dependency>

        <!-- SERVLET RUNTIME -->
        <dependency>
            <groupId>javax.servlet</groupId>
            <artifactId>javax.servlet-api</artifactId>
            <version>3.0.1</version>
            <scope>provided</scope>
        </dependency>
        
        <dependency>
            <groupId>javax.validation</groupId>
            <artifactId>validation-api</artifactId>
            <version>1.1.0.Final</version>
        </dependency>


        <!-- JSP COMPLIATION -->
        <dependency>
            <groupId>javax.servlet.jsp</groupId>
            <artifactId>jsp-api</artifactId>
            <version>2.2</version>
            <scope>provided</scope>
        </dependency>

        <dependency>
            <groupId>org.apache.tomcat</groupId>
            <artifactId>tomcat-jasper</artifactId>
            <version>7.0.61</version>
            <scope>provided</scope>
        </dependency>

        <dependency>
            <groupId>javax.el</groupId>
            <artifactId>el-api</artifactId>
            <version>2.2</version>
            <scope>provided</scope>
        </dependency>
        
        <dependency>
            <groupId>com.google.code.gson</groupId>
            <artifactId>gson</artifactId>
            <version>2.2.4</version>
        </dependency>

        <!-- JAVA MAIL -->
        <dependency>
            <groupId>javax.mail</groupId>
            <artifactId>mail</artifactId>
            <version>1.4.4</version>
            <!-- <version>1.5.1</version> -->
        </dependency>


        <!-- LOGGING -->
        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-api</artifactId>
            <version>1.6.6</version>
        </dependency>

        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-log4j12</artifactId>
            <version>1.6.6</version>
            <type>jar</type>
            <scope>runtime</scope>
        </dependency>

        <dependency>
            <groupId>log4j</groupId>
            <artifactId>log4j</artifactId>
            <version>1.2.14</version>
        </dependency>


        <!-- APACHE -->
        <dependency>
           <groupId>commons-lang</groupId>
           <artifactId>commons-lang</artifactId>
           <version>2.3</version>
        </dependency>
        
        <dependency>
            <groupId>commons-collections</groupId>
            <artifactId>commons-collections</artifactId>
            <version>3.2.1</version>
        </dependency>
        
        <dependency>
            <groupId>commons-fileupload</groupId>
            <artifactId>commons-fileupload</artifactId>
            <version>1.3.1</version>
        </dependency>
        
        <dependency>
            <groupId>commons-beanutils</groupId>
            <artifactId>commons-beanutils</artifactId>
            <version>1.9.2</version>
        </dependency>
        
        <dependency>
            <groupId>commons-dbcp</groupId>
            <artifactId>commons-dbcp</artifactId>
            <version>1.2.2</version>
        </dependency>

        <!--
            FOP 1.1 pom has the wrong avalon dependencies
            see https://mail-archives.apache.org/mod_mbox/xmlgraphics-fop-dev/201211.mbox/%3Cbug-54089-38666@https.issues.apache.org/bugzilla/%3E
        -->
        <dependency>
            <groupId>org.apache.xmlgraphics</groupId>
            <artifactId>fop</artifactId>
            <version>1.1</version>
            <exclusions>
                <exclusion>
                    <artifactId>avalon-framework-api</artifactId>
                    <groupId>org.apache.avalon.framework</groupId>
                </exclusion>
                <exclusion>
                    <artifactId>avalon-framework-impl</artifactId>
                    <groupId>org.apache.avalon.framework</groupId>
                </exclusion>
            </exclusions>
        </dependency>
        <!--  these two are to correct issues in  fop dependency -->
        <dependency>
            <groupId>avalon-framework</groupId>
            <artifactId>avalon-framework-api</artifactId>
            <version>4.2.0</version>
        </dependency>
        <dependency>
            <groupId>avalon-framework</groupId>
            <artifactId>avalon-framework-impl</artifactId>
            <version>4.2.0</version>
        </dependency>


        <!-- GOOGLE -->
        <dependency>
            <groupId>com.google.guava</groupId>
            <artifactId>guava</artifactId>
            <version>12.0</version>
        </dependency>


        <!-- ITEXT -->
        <dependency>
            <groupId>com.itextpdf</groupId>
            <artifactId>itextpdf</artifactId>
            <version>5.0.6</version>
        </dependency>


        <!-- OSTERMILLER -->
        <dependency>
            <groupId>org.ostermiller</groupId>
            <artifactId>utils</artifactId>
            <version>1.07.00</version>
        </dependency>


        <!-- HTML TIDY -->
        <dependency>
            <groupId>net.sf.jtidy</groupId>
            <artifactId>jtidy</artifactId>
            <version>r938</version>
        </dependency>

        <!-- SPRING BILL OF MATERIALS -->
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-jdbc</artifactId>
        </dependency>
        
        <dependency>
             <groupId>org.springframework</groupId>
             <artifactId>spring-test</artifactId>            
        </dependency>
        
        <dependency>
            <groupId>org.springframework</groupId>            
            <artifactId>spring-aspects</artifactId>
        </dependency>
      
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-tx</artifactId>
        </dependency>

        <dependency> 
            <groupId>org.springframework</groupId> 
            <artifactId>spring-web</artifactId>
        </dependency>

        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-webmvc</artifactId>
        </dependency>
        
        <!-- SPRING -->
        
        <dependency>
            <groupId>org.springframework.webflow</groupId>
            <artifactId>spring-webflow</artifactId>
            <version>2.4.0.RELEASE</version>
        </dependency>

        <dependency>
            <groupId>org.springframework.ldap</groupId>
            <artifactId>spring-ldap-core</artifactId>
            <version>1.3.1.RELEASE</version>
        </dependency>

        <dependency>
            <groupId>org.springframework.ldap</groupId>
            <artifactId>spring-ldap-core-tiger</artifactId>
            <version>1.3.1.RELEASE</version>
        </dependency>

        <dependency>
            <groupId>org.springframework.ldap</groupId>
            <artifactId>spring-ldap-odm</artifactId>
            <version>1.3.1.RELEASE</version>
        </dependency>

        <dependency>
            <groupId>org.springframework.ldap</groupId>
            <artifactId>spring-ldap-ldif-core</artifactId>
            <version>1.3.1.RELEASE</version>
        </dependency>

        <dependency>
            <groupId>org.springframework.ldap</groupId>
            <artifactId>spring-ldap-ldif-batch</artifactId>
            <version>1.3.1.RELEASE</version>
        </dependency>

        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-test</artifactId>
            <version>1.2.3.RELEASE</version>
            <scope>test</scope>
        </dependency>
        
        <dependency>
            <groupId>com.eaio.uuid</groupId>
            <artifactId>uuid</artifactId>
            <version>3.4</version>
        </dependency>
 
        <!-- IET -->
        <dependency>
            <groupId>edu.ucdavis.iet.commons</groupId>
            <artifactId>iet-commons-ldap</artifactId>
            <version>1.2.1.3</version>
            <scope>compile</scope>
            <exclusions>
            <exclusion>
                <groupId>org.directwebremoting</groupId>
                <artifactId>dwr</artifactId>
            </exclusion>    
            </exclusions>    
        </dependency>
        
        <dependency>
            <groupId>edu.ucdavis.iet.commons</groupId>
            <artifactId>iet-commons-utils</artifactId>
            <version>2.0.0</version>
            <scope>compile</scope>
        </dependency>
        
        <dependency>
            <groupId>org.jdom</groupId>
            <artifactId>jdom2</artifactId>
            <version>2.0.5</version>
        </dependency>

        <dependency>
            <groupId>org.aspectj</groupId>
            <artifactId>aspectjrt</artifactId>
            <version>1.8.2</version>
        </dependency>

        <dependency>
            <groupId>javax.ws.rs</groupId>
            <artifactId>javax.ws.rs-api</artifactId>
            <version>2.0.1</version>
        </dependency>
        
        <dependency>
            <groupId>xerces</groupId>
            <artifactId>xercesImpl</artifactId>
            <version>2.11.0</version>
            <type>jar</type>
        </dependency>
        
        <!-- JSTL -->
        <dependency>
             <groupId>jstl</groupId>
             <artifactId>jstl</artifactId>
             <version>1.2</version>
        </dependency>

        <dependency>
            <groupId>opensymphony</groupId>
            <artifactId>oscache</artifactId>
            <version>2.3</version>
            <scope>runtime</scope>
        </dependency>

        <!-- ORACLE -->
        <dependency>
            <groupId>com.oracle</groupId>
            <artifactId>ojdbc6</artifactId>
            <version>11.2.0.1.0</version>
        </dependency>


        <!-- MySQL -->
        <dependency>
            <groupId>mysql</groupId>
            <artifactId>mysql-connector-java</artifactId>
            <version>5.1.22</version>
            <type>jar</type>
            <scope>runtime</scope>
        </dependency>


        <!-- TOMCAT -->
        <dependency>
            <groupId>org.apache.tomcat.maven</groupId>
            <artifactId>tomcat7-maven-plugin</artifactId>
            <version>2.2</version>
            <scope>provided</scope>
        </dependency>

        <dependency>
            <groupId>org.apache.tomcat</groupId>
            <artifactId>tomcat-dbcp</artifactId>
            <version>7.0.61</version>
            <scope>runtime</scope>
        </dependency>


        <!-- XMLMind XFC -->
        <dependency>
            <groupId>com.xmlmind</groupId>
            <artifactId>xfc</artifactId>
            <version>4.4</version>
            <scope>runtime</scope>
        </dependency>

        <!-- JavaScript Bundling -->
        <dependency>
            <groupId>net.jawr</groupId>
            <artifactId>jawr-core</artifactId>
            <version>[3,]</version>
        </dependency>

        <!-- JACKSON DATA-BINDING -->
        <dependency>
            <groupId>com.fasterxml.jackson.core</groupId>
            <artifactId>jackson-databind</artifactId>
            <version>2.5.3</version>
        </dependency>

        <!-- JavaScript Documentation -->
        <dependency>
            <groupId>com.phasebash.jsdoc</groupId>
            <artifactId>jsdoc3-maven-plugin</artifactId>
            <version>1.1.0</version>
        </dependency>

        <dependency>
	        <groupId>org.springframework.hateoas</groupId>
	        <artifactId>spring-hateoas</artifactId>
	        <version>0.17.0.RELEASE</version>
	    </dependency>
    </dependencies>
</project>
